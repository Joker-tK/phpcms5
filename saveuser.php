<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2011.09.11
 * @Id         单页
**/
session_start();
require_once 'source/core/run.php';

$action		= Core_Fun::rec_post("action");

switch($action){
    case 'add';
	    Adduser();
		break;
	default;
	    volist();
		break;
}
function Adduser(){
	//Core_Auth::checkauth("adminadd");
	global $db;
	$loginname	= Core_Fun::rec_post('loginname',1);
	$password	= Core_Fun::rec_post('password',1);
	$usergroupid= 1;
	$relname	= Core_Fun::rec_post('relname',1);
	$flag		= 1;
	$sex		= Core_Fun::rec_post('Sex',1);
	$email		= Core_Fun::rec_post('email',1);
	$address	= Core_Fun::rec_post('address',1);
	$telno		= Core_Fun::rec_post('telno',1);
	$brothday	= Core_Fun::rec_post('brothday',1);
	$memo		= Core_Fun::strip_post('memo',1);
	$founderr	= false;
	if(!Core_Fun::ischar($loginname)){
	    $founderr	= true;
		$errmsg	   .="登录帐号不能为空.<br />";
	}else{
		if(!Core_Fun::check_userstr($loginname)){
			$founderr	= true;
			$errmsg	   .="帐号格式不正确，只能由中文，字母、数字和下横下组成.<br />";
		}
	}
	if(!Core_Fun::ischar($password)){
		$founderr	= true;
		$errmsg	   .= "登录密码不能为空.<br />";	
	}

	/*if(!Core_Fun::ischar($sex)){
		$founderr	= true;
		$errmsg	   .= "性别不能为空.<br />";	
	}

	if(!Core_Fun::ischar($email)){
		$founderr	= true;
		$errmsg	   .= "E-mail不能为空.<br />";	
	}
	if(!Core_Fun::ischar($telno)){
		$founderr	= true;
		$errmsg	   .= "联系电话不能为空.<br />";	
	}

	if($founderr == true){
	    Core_Fun::halt($errmsg,"",1);
	}*/
	else 
		{


if(!($db->checkdata("SELECT userid FROM ".DB_PREFIX."user WHERE lower(loginname)='".strtolower($loginname)."'"))){
		$userid	= $db->fetch_newid("SELECT MAX(userid) FROM ".DB_PREFIX."user",1);
		$password	= md5($password);
		$array	= array(
			'userid'=>$userid,
			'loginname'=>$loginname,
			'password'=>$password,
			'usergroupid'=>$usergroupid,
			'flag'=>$flag,
			'realname'=>$relname,
			'regdate'=>time(),
			'sex'=>$sex,
			'email'=>$email,
			'address'=>$address,
			'telno'=>$telno,
			'brothday'=>$brothday,
			'memo'=>$memo,
	    );
		$result = $db->insert(DB_PREFIX."user",$array);
		if($result){
			Core_Command::runlog("","注册会员成功[$loginname]",1);
			Core_Fun::halt("注册会员成功","index.php",0);
		}else{
			Core_Fun::halt("注册失败","",1);
		}
	}else{
		Core_Fun::halt("该帐号已存在，请填写另外一个。","",1);
	}

	}




}

	function userlogin($username,$password,$ajax=0){
		global $db;
		$username = Core_Fun::replacebadchar($username);
		$password = Core_Fun::replacebadchar($password);
		$md5password = md5($password);
		$sql  = "SELECT a.*,g.grupname,g.level".
			    " FROM ".DB_PREFIX."user AS a".
			    " LEFT JOIN ".DB_PREFIX."usergroup AS g ON a.usergroupid=g.usergroupid".
			    " WHERE lower(a.loginname)='".strtolower($username)."' AND a.password='$md5password'";
		$rows = $db->fetch_first($sql);
		if($rows){
			if($rows['flag']==0){
				Core_Fun::halt("对不起，该帐号被禁止！","login.php",4);
			}else{
			$_SESSION["USERNAME"]=$username;
			$_SESSION["usergroupname"]=$rows['grupname'];
			$_SESSION["USERLEVEL"]=$rows['level'];
			$_SESSION["pointnum"]=$rows['pointnum'];
			$_SESSION["lastlogindate"]=$rows['lastlogindate'];
		
				$array  = array(
					'lastlogindate'=>time(),
					'pointnum'=>'[[pointnum+1]]',
					'lastloginip'=>Core_Fun::getip(),
				);
				$db->update(DB_PREFIX."user",$array,"loginname='$username'");
				Core_Command::runlog($username,"登录成功.",1);
				if($ajax==1){
					return true;
				}else{
		Core_Fun::halt("登录成功","login.php",0);
				}
			}
		}else{
			Core_Command::runlog($username,"登录后台失败.",1);
			if($ajax==1){
				return false;
			}else{
				Core_Fun::halt("对不起，帐号或者密码不正确！","login.php",4);
			}
		}
	}




?>




