<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.01.15
 * @Id         首页
**/
if(!file_exists('./data/install.lock')){
	if(file_exists('./install/index.php')){
		header("location:./install/index.php");exit;
	}
	else{
		header("Content-type: text/html;charset=utf-8");
		echo "安装文件不存在，请上传安装文件。如已安装过，请新建data/install.lock文件。";
		die();
	}
}
define('ALLOWGUEST',true);
require_once './source/core/run.php';
$tplfile = INDEX_TEMPLATE."index.".$tplext;
$widgetfile = "./source/widget/index.php";
if(!Core_Fun::fileexists($tplfile)){
	Core_Fun::halt("对不起，模板文件“".$tplfile."”不存在，请检查！","",1);
}
if(!Core_Fun::fileexists($widgetfile)){
	Core_Fun::halt("对不起，部件文件“".$widgetfile."”不存在，请检查！","",1);
}

/* 缓存,模板处理 */
if($config['cachstatus']==1){
	$cache_seconds = $config['cachtime']*60;
	$tpl->setCaching(true); 
	$tpl->setCacheLifetime($cache_seconds);	
}
$cacheid = md5($_SERVER["REQUEST_URI"]);
if(!$tpl->isCached($tplfile,$cacheid)){
	require_once './source/module/app.php';
	require_once './source/widget/index.php';
}
$tpl->assign("runtime",Core_Fun::runtime());
$tpl->display($tplfile,$cacheid);

//echo(date('m'));

?>