<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
**/
session_start();
require_once '../source/core/run.php';
$action	= Core_Fun::rec_post("action",1);
if($action=="loginpost"){
	$username	= Core_Fun::rec_post("username",1);
	$password	= Core_Fun::rec_post("password",1);
	$adminlogincode = Core_Fun::rec_post("AdminLoginCode",1);
	$checkcode	= Core_Fun::rec_post("checkcode",1);
	$founderr   = false;
	if(!Core_Fun::ischar($username)){
		$founderr	= true;
		$errmsg	   .= "管理员帐号不能为空.<br />";
	}else{
		if(!Core_Fun::check_userstr($username)){
			$founderr	= true;
			$errmsg	   .= "帐号格式不正确！.<br />";
		}
	}

	if(!Core_Fun::ischar($adminlogincode)){
		$founderr	= true;
		$errmsg	   .= "认证不能为空.<br />";
	}
	else 
	{
	if(trim($adminlogincode)<>"admin"){
			$founderr	= true;
			$errmsg	   .= "你输入的管理员认证码不对！.<br />";
		}
	}
	if(!Core_Fun::ischar($password)){
		$founderr	= true;
		$errmsg	   .= "密码不能为空.<br />";
	}
	if(!Core_Fun::ischar($checkcode)){
		$founderr	= true;
		$errmsg	   .= "验证码不能为空.<br />";
	}else{
		if($checkcode != $_SESSION["verifycode"]){
			$founderr	= true;
			$errmsg	   .= "验证码不正确.<br />";
		}
	}
	if($founderr == true){
		Core_Fun::halt($errmsg,"",1);
	}else{
		$libadmin->login($username,$password);
	}	
}else{
	$tpl->display(ADMIN_TEMPLATE."login.tpl");
}

?>