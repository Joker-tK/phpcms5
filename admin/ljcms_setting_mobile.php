<?php
require_once '../source/core/run.php';
require_once 'admin.inc.php';
$action	= Core_Fun::rec_post("action");
Core_Auth::checkauth("config");
switch($action){
    case 'saveabout';
	    saveabout();
		break;
	case 'savecontact';
	    savecontact();
		break;
	case 'saverewrite';
	    saverewrite();
		break;
	case 'savecache';
	    savecache();
		break;
	case 'saveseo';
	    saveseo();
		break;
	case 'saveconfig';
	    saveconfig();
		break;
	case 'saveupload';
	    saveupload();
		break;
	case 'clearcache';
	    clearcache();
		break;
	case 'clearcompiled';
	    clearcompiled();
		break;
	case 'savesetting';
	    savesetting();
		break;
	default;
	    setting();
		break;
}


function savesetting(){
	$sitename		= Core_Fun::strip_post("sitename",1);
	$siteurl		= Core_Fun::strip_post("siteurl",1);
	$icpcode		= Core_Fun::strip_post("icpcode",1);
	$tjcode			= Core_Fun::strip_post("tjcode",1);
	$indexvideo		= Core_Fun::strip_post("indexvideo",1);
	$downsearchtop	= Core_Fun::strip_post("downsearchtop",1);
	$downsearchbottom = Core_Fun::strip_post("downsearchbottom",1);
	$nocontent		= Core_Fun::strip_post("nocontent",1);
	$sitecopyright	= Core_Fun::strip_post("sitecopyright",1);
	$logoimg		= Core_Fun::rec_post("logoimg",1);
	$adsyspic		= Core_Fun::rec_post("adsyspic",1);
	$adstatus	    = Core_Fun::detect_number(Core_Fun::rec_post("adstatus",1));
	$adlink		    = Core_Fun::rec_post("adlink",1);
	$logowidth		= Core_Fun::detect_number(Core_Fun::rec_post("logowidth",1));
	$logoheight		= Core_Fun::detect_number(Core_Fun::rec_post("logoheight",1));
	$array	= array(
		'sitename'=>$sitename,
		'siteurl'=>$siteurl,
		'icpcode'=>$icpcode,
		'tjcode'=>$tjcode,
		'downsearchtop'=>$downsearchtop,
		'downsearchbottom'=>$downsearchbottom,
	    'nocontent'=>$nocontent,
		'indexvideo'=>$indexvideo,
		'sitecopyright'=>$sitecopyright,
		'logoimg'=>$logoimg,
		'logowidth'=>$logowidth,
		'logoheight'=>$logoheight,
		'adsyspic'=>$adsyspic,
		'adstatus'=>$adstatus,
		'adlink'=>$adlink,
	);
	$result = $GLOBALS['db']->update(DB_PREFIX."config",$array,"");
	if(!$result){
		Core_Fun::halt("更新失败","",1);
	}else{
		Core_Command::runlog("","更新站点设置成功");
		Core_Fun::halt("更新成功","ljcms_setting.php",0);
	}
}

function saveabout(){
	$content = Core_Fun::strip_post("content",1);
	$result = $GLOBALS['db']->update(DB_PREFIX."config",array('about'=>$content,),"");
	if(!$result){
		Core_Fun::halt("更新失败","",1);
	}else{
		Core_Command::runlog("","更新关于我们成功");
		Core_Fun::halt("更新成功","ljcms_setting.php?action=about",0);
	}
}

function savecontact(){
	$content = Core_Fun::strip_post("content",1);
	$result = $GLOBALS['db']->update(DB_PREFIX."config",array('contact'=>$content,),"");
	if(!$result){
		Core_Fun::halt("更新失败","",1);
	}else{
		Core_Command::runlog("","更新联系我们成功");
		Core_Fun::halt("更新成功","ljcms_setting.php?action=contact",0);
	}
}




function saveconfig(){
	$newspagesize		= Core_Fun::detect_number(Core_Fun::rec_post("newspagesize",1),15);
	$newsnum			= Core_Fun::detect_number(Core_Fun::rec_post("newsnum",1),10);
	$newslen			= Core_Fun::detect_number(Core_Fun::rec_post("newslen",1),10);
	$articlepagesize	= Core_Fun::detect_number(Core_Fun::rec_post("articlepagesize",1),15);
	$articlenum			= Core_Fun::detect_number(Core_Fun::rec_post("articlenum",1),10);
	$articlelen			= Core_Fun::detect_number(Core_Fun::rec_post("articlelen",1),10);
	$productpagesize	= Core_Fun::detect_number(Core_Fun::rec_post("productpagesize",1),15);
	$productnum			= Core_Fun::detect_number(Core_Fun::rec_post("productnum",1),10);
	$productlen			= Core_Fun::detect_number(Core_Fun::rec_post("productlen",1),10);
	$casepagesize		= Core_Fun::detect_number(Core_Fun::rec_post("casepagesize",1),15);
	$casenum			= Core_Fun::detect_number(Core_Fun::rec_post("casenum",1),10);
	$caselen			= Core_Fun::detect_number(Core_Fun::rec_post("caselen",1),10);
	$jobpagesize		= Core_Fun::detect_number(Core_Fun::rec_post("jobpagesize",1),15);
	$jobnum				= Core_Fun::detect_number(Core_Fun::rec_post("jobnum",1),10);
	$joblen				= Core_Fun::detect_number(Core_Fun::rec_post("joblen",1),10);
	$downpagesize		= Core_Fun::detect_number(Core_Fun::rec_post("downpagesize",1),15);
	$downnum			= Core_Fun::detect_number(Core_Fun::rec_post("downnum",1),10);
	$downlen			= Core_Fun::detect_number(Core_Fun::rec_post("downlen",1),10);
	$solutionpagesize	= Core_Fun::detect_number(Core_Fun::rec_post("solutionpagesize",1),15);
	$solutionnum		= Core_Fun::detect_number(Core_Fun::rec_post("solutionnum",1),10);
	$solutionlen		= Core_Fun::detect_number(Core_Fun::rec_post("solutionlen",1),10);
	$eliteproductnum	= Core_Fun::detect_number(Core_Fun::rec_post("eliteproductnum",1),15);
	$eliteproductlen	= Core_Fun::detect_number(Core_Fun::rec_post("eliteproductlen",1),10);
	$qqstatus			= Core_Fun::detect_number(Core_Fun::rec_post("qqstatus",1));
	$array	= array(
		'newspagesize'=>$newspagesize,
		'newsnum'=>$newsnum,
		'newslen'=>$newslen,
		'articlepagesize'=>$articlepagesize,
		'articlenum'=>$articlenum,
		'articlelen'=>$articlelen,
		'productpagesize'=>$productpagesize,
		'productnum'=>$productnum,
		'productlen'=>$productlen,
		'casepagesize'=>$casepagesize,
		'casenum'=>$casenum,
		'caselen'=>$caselen,
		'jobpagesize'=>$jobpagesize,
		'jobnum'=>$jobnum,
		'joblen'=>$joblen,
		'downpagesize'=>$downpagesize,
		'downnum'=>$downnum,
		'downlen'=>$downlen,
		'solutionpagesize'=>$solutionpagesize,
		'solutionnum'=>$solutionnum,
		'solutionlen'=>$solutionlen,
		'eliteproductnum'=>$eliteproductnum,
		'eliteproductlen'=>$eliteproductlen,
		'qqstatus'=>$qqstatus,
	);
	$result = $GLOBALS['db']->update(DB_PREFIX."config",$array,"");
	if(!$result){
		Core_Fun::halt("更新失败","",1);
	}else{
		Core_Command::runlog("","更新站点参数设置成功");
		Core_Fun::halt("更新成功","ljcms_setting.php?action=config",0);
	}
}



function saveseo(){
	$sitetitle			= Core_Fun::strip_post("sitetitle",1);
	$metadescription	= Core_Fun::rec_post("metadescription",1);
	$metakeyword		= Core_Fun::rec_post("metakeyword",1);
	$array	= array(
		'sitetitle'=>$sitetitle,
		'metadescription'=>$metadescription,
		'metakeyword'=>$metakeyword,
	);
	$result = $GLOBALS['db']->update(DB_PREFIX."config",$array,"");
	if(!$result){
		Core_Fun::halt("更新失败","",1);
	}else{
		Core_Command::runlog("","更新站点SEO成功");
		Core_Fun::halt("更新成功","ljcms_setting.php?action=seo",0);
	}
}



function saverewrite(){
	$htmltype		= Core_Fun::rec_post("htmltype",1);
	$routeurltype	= Core_Fun::detect_number(Core_Fun::rec_post("routeurltype",1),1);
	$array	= array(
		'htmltype'=>$htmltype,
		'routeurltype'=>$routeurltype,
	);
	$result = $GLOBALS['db']->update(DB_PREFIX."config",$array,"");
	if(!$result){
		Core_Fun::halt("更新失败","",1);
	}else{
		Core_Command::runlog("","设置站点伪静态成功");
		Core_Fun::halt("更新成功","ljcms_setting.php?action=rewrite",0);
	}
}
$tpl->assign("action",$action);
$tpl->display(ADMIN_TEMPLATE."b.tpl");
$tpl->assign("runtime",runtime());
$tpl->assign("copyright",$libadmin->copyright());
?>