<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
**/
require_once '../source/core/run.php';
require_once 'admin.inc.php';
$action		= Core_Fun::rec_post("action");
$page		= Core_Fun::detect_number(Core_Fun::rec_post("page"),1);
if($page<1){$page=1;}
$comeurl	= "page=$page";

if(Core_Fun::rec_post('act')=='update'){
    updateajax(Core_Fun::rec_post('id'),Core_Fun::rec_post('action'));
}
switch($action){
    case 'add';
	    add();
		break;
	case 'saveadd';
	    saveadd();
		break;
	case 'edit';
	    edit();
		break;
	case 'saveedit';
	    saveedit();
		break;
	case 'del';
	    del();
		break;
	default;
	    volist();
		break;
}

function volist(){
	Core_Auth::checkauth("linkvolist");
	global $db,$tpl,$page;
    $pagesize	= 30;
	$searchsql	= " WHERE 1=1";
	$countsql	= "SELECT COUNT(linkid) FROM ".DB_PREFIX."link".$searchsql;
    $total		= $db->fetch_count($countsql);
    $pagecount	= ceil($total/$pagesize);
	$nextpage	= $page+1;
	$prepage	= $page-1;
	$start		= ($page-1)*$pagesize;
	$sql		= "SELECT * FROM ".DB_PREFIX."link".
		          $searchsql." ORDER BY linktype ASC,orders ASC LIMIT $start, $pagesize";
	$link		= $db->getall($sql);
	$url		= $_SERVER['PHP_SELF'];
	$showpage	= Core_Page::adminpage($total,$pagesize,$page,$url,10);
	$tpl->assign("total",$total);
	$tpl->assign("pagecount",$pagecount);
	$tpl->assign("page",$page);
	$tpl->assign("showpage",$showpage);
	$tpl->assign("link",$link);
}

function add(){
	Core_Auth::checkauth("linkadd");
	global $tpl,$db;
	$orders = $db->fetch_newid("SELECT MAX(orders) FROM ".DB_PREFIX."link",1);
	$tpl->assign("flag_checkbox",Core_Mod::checkbox("1","flag","审核"));
	$tpl->assign("orders",$orders);
}

function edit(){
	Core_Auth::checkauth("linkedit");
	global $db,$tpl;
    $id = Core_Fun::rec_post('id');
	if(!Core_Fun::isnumber($id)){
		Core_Fun::halt("ID丢失","",2);
	}
	$sql	= "SELECT * FROM ".DB_PREFIX."link WHERE linkid=$id";
	$link	= $db->fetch_first($sql);
	if(!$link){
		Core_Fun::halt("数据不存在","",2);
	}else{
		$link['logopicname'] = Core_Mod::getpicname($link['logoimg']);
		$tpl->assign("flag_checkbox",Core_Mod::checkbox($link['flag'],"flag","审核"));
		$tpl->assign("id",$id);
		$tpl->assign("comeurl",$GLOBALS['comeurl']);
	    $tpl->assign("link",$link);
	}
}

function saveadd(){
	Core_Auth::checkauth("linkadd");
	global $db;
	$linktitle	= Core_Fun::rec_post('linktitle',1);
	$fontcolor	= Core_Fun::strip_post('fontcolor',1);
	$linkurl	= Core_Fun::strip_post('linkurl',1);
	$linktype	= Core_Fun::detect_number(Core_Fun::rec_post('linktype',1),1);
	$logoimg	= Core_Fun::strip_post('logoimg',1);
	$flag		= Core_Fun::detect_number(Core_Fun::rec_post('flag',1));
	$orders		= Core_Fun::detect_number(Core_Fun::rec_post('orders',1));
	$intro		= Core_Fun::rec_post('intro',1);
	$founderr	= false;
	if(!Core_Fun::ischar($linktitle)){
	    $founderr	= true;
		$errmsg	   .="网站名称不能为空.<br />";
	}
	if(!Core_Fun::ischar($linkurl)){
	    $founderr	= true;
		$errmsg	   .="网站URL链接不能为空.<br />";
	}
	if($founderr == true){
	    Core_Fun::halt($errmsg,"",1);
	}
	$linkid	= $db->fetch_newid("SELECT MAX(linkid) FROM ".DB_PREFIX."link",1);
	$array	= array(
		'linkid'=>$linkid,
		'linktitle'=>$linktitle,
		'fontcolor'=>$fontcolor,
		'linkurl'=>$linkurl,
		'linktype'=>$linktype,
		'logoimg'=>$logoimg,
		'flag'=>$flag,
		'intro'=>$intro,
		'orders'=>$orders,
		'timeline'=>time(),
	);
	$result = $db->insert(DB_PREFIX."link",$array);
	if($result){
		Core_Command::runlog("","添加友情链接成功[$linktitle]",1);
		Core_Fun::halt("保存成功","ljcms_link.php",0);
	}else{
		Core_Fun::halt("保存失败","",1);
	}
}

function saveedit(){
	Core_Auth::checkauth("linkedit");
	global $db;
	$id			= Core_Fun::rec_post('id',1);
	$linktitle	= Core_Fun::rec_post('linktitle',1);
	$fontcolor	= Core_Fun::strip_post('fontcolor',1);
	$linkurl	= Core_Fun::strip_post('linkurl',1);
	$linktype	= Core_Fun::detect_number(Core_Fun::rec_post('linktype',1),1);
	$logoimg	= Core_Fun::strip_post('logoimg',1);
	$flag		= Core_Fun::detect_number(Core_Fun::rec_post('flag',1));
	$orders		= Core_Fun::detect_number(Core_Fun::rec_post('orders',1));
	$intro		= Core_Fun::rec_post('intro',1);
	$founderr	= false;
	if(!Core_Fun::isnumber($id)){
	    $founderr	= true;
		$errmsg	   .= "ID丢失.<br />";
	}
	if(!Core_Fun::ischar($linktitle)){
	    $founderr	= true;
		$errmsg	   .="网站名称不能为空.<br />";
	}
	if(!Core_Fun::ischar($linkurl)){
	    $founderr	= true;
		$errmsg	   .="网站URL链接不能为空.<br />";
	}
	if($founderr == true){
	    Core_Fun::halt($errmsg,"",1);
	}
	$array = array(
		'linktitle'=>$linktitle,
		'fontcolor'=>$fontcolor,
		'linkurl'=>$linkurl,
		'linktype'=>$linktype,
		'logoimg'=>$logoimg,
		'flag'=>$flag,
		'intro'=>$intro,
		'orders'=>$orders,
	);
	$result = $db->update(DB_PREFIX."link",$array,"linkid=$id");
	if($result){
		Core_Command::runlog("","编辑友情链接成功[id=$id]");
		Core_Fun::halt("编辑成功","ljcms_link.php?".$GLOBALS['comeurl']."",0);
	}else{
		Core_Fun::halt("编辑失败","",2);
	}
}

function del(){
	Core_Auth::checkauth("linkdel");
	$arrid  = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
	if($arrid=="" || is_null($arrid)){
		Core_Fun::halt("请选择要删除的数据","",1);
	}
	global $db;
	for($ii=0;$ii<count($arrid);$ii++){
        $id = Core_Fun::replacebadchar(trim($arrid[$ii]));
		if(Core_Fun::isnumber($id)){

			$sql	= "SELECT logoimg FROM ".DB_PREFIX."link WHERE linkid=$id";
			$rows	= $GLOBALS['db']->fetch_first($sql);
			if($rows){
				if(Core_Fun::ischar($rows['logoimg'])){
					Core_Fun::deletefile("../".$rows['logoimg']);
				}
				$GLOBALS['db']->query("DELETE FROM ".DB_PREFIX."link WHERE linkid=$id");
			}
		}
	}
	Core_Command::runlog("","删除友情链接成功[id=$arrid]");
	Core_Fun::halt("删除成功","ljcms_link.php",0);
}

function updateajax($_id,$_action){
	Core_Auth::checkauth("linkedit");
    if(Core_Fun::isnumber($_id)){
		global $db;
		switch($_action){
			case 'flagopen';
			$db->query("UPDATE ".DB_PREFIX."link SET flag=1 WHERE linkid=$_id");
			break;
			case 'flagclose';
			$db->query("UPDATE ".DB_PREFIX."link SET flag=0 WHERE linkid=$_id");
			break;
			default;
			break;
		}
	}
}

$tpl->assign("action",$action);
$tpl->display(ADMIN_TEMPLATE."link.tpl");
$tpl->assign("runtime",runtime());
$tpl->assign("copyright",$libadmin->copyright());
?>