<?php



require_once '../source/core/run.php';
require_once 'admin.inc.php';
$action		= Core_Fun::rec_post("action");
$page		= Core_Fun::detect_number(Core_Fun::rec_post("page"),1);
$szoneid    = Core_Fun::detect_number(Core_Fun::rec_post("szoneid"));
$sname      = Core_Fun::rec_post("sname",1);
if($page<1){$page=1;}
$comeurl	= "page=$page&sskinid=$sskinid&sname=".urlencode($sname)."";

if(Core_Fun::rec_post('act')=='update'){
    updateajax(Core_Fun::rec_post('id'),Core_Fun::rec_post('action'));
}
switch($action){
    case 'add';
	    add();
		break;
	case 'saveadd';
	    saveadd();
		break;
	case 'edit';
	    edit();
		break;
	case 'saveedit';
	    saveedit();
		break;
	case 'del';
	    del();
		break;
	default;
	    volist();
		break;
}



function saveadd(){
	Core_Auth::checkauth("adsadd");
	global $db;
	$zoneid			= Core_Fun::detect_number(Core_Fun::rec_post('zoneid',1));
	$adsname		= Core_Fun::rec_post('adsname',1);
	$uploadfiles	= Core_Fun::strip_post('uploadfiles',1);
	$url			= Core_Fun::strip_post('url',1);
	$flag			= Core_Fun::detect_number(Core_Fun::rec_post('flag',1));
	$orders			= Core_Fun::detect_number(Core_Fun::rec_post('orders',1));
	$width			= Core_Fun::detect_number(Core_Fun::rec_post('width',1));
	$height			= Core_Fun::detect_number(Core_Fun::rec_post('height',1));
	$content		= Core_Fun::strip_post('content',1);
	$founderr		= false;
	if($zoneid<1){
	    $founderr	= true;
		$errmsg	   .="请选择所属广告标签.<br />";
	}
	if(!Core_Fun::ischar($uploadfiles)){
	    $founderr	= true;
		$errmsg	   .="图片附件不能为空.<br />";
	}
	if($founderr == true){
	    Core_Fun::halt($errmsg,"",1);
	}
	$adsid	= $db->fetch_newid("SELECT MAX(adsid) FROM ".DB_PREFIX."adsfigure",1);
	$array	= array(
		'adsid'=>$adsid,
		'adsname'=>$adsname,
		'zoneid'=>$zoneid,
		'uploadfiles'=>$uploadfiles,
		'url'=>$url,
		'width'=>$width,
		'height'=>$height,
		'orders'=>$orders,
		'flag'=>$flag,
		'content'=>$content,
		'timeline'=>time(),
	);
	$result = $db->insert(DB_PREFIX."adsfigure",$array);
	if($result){
		Core_Command::runlog("","添加广告图片成功[$adsname]",1);
		Core_Fun::halt("保存成功","ljcms_ads.php",0);
	}else{
		Core_Fun::halt("保存失败","",1);
	}
}



function del(){
	Core_Auth::checkauth("adsdel");
	$arrid  = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
	if($arrid=="" || is_null($arrid)){
		Core_Fun::halt("请选择要删除的数据","",1);
	}
	global $db;
	for($ii=0;$ii<count($arrid);$ii++){
        $id = Core_Fun::replacebadchar(trim($arrid[$ii]));
		if(Core_Fun::isnumber($id)){

			$sql	= "SELECT uploadfiles FROM ".DB_PREFIX."adsfigure WHERE adsid=$id";
			$rows	= $GLOBALS['db']->fetch_first($sql);
			if($rows){
				if(Core_Fun::ischar($rows['uploadfiles'])){
					Core_Fun::deletefile("../".$rows['uploadfiles']);
				}
				$GLOBALS['db']->query("DELETE FROM ".DB_PREFIX."adsfigure WHERE adsid=$id");
			}
		}
	}
	Core_Command::runlog("","删除广告图片成功[id=$arrid]");
	Core_Fun::halt("删除成功","ljcms_ads.php",0);
}

function updateajax($_id,$_action){
	Core_Auth::checkauth("adsedit");
    if(Core_Fun::isnumber($_id)){
		global $db;
		switch($_action){
			case 'flagopen';
			$db->query("UPDATE ".DB_PREFIX."adsfigure SET flag=1 WHERE adsid=$_id");
			break;
			case 'flagclose';
			$db->query("UPDATE ".DB_PREFIX."adsfigure SET flag=0 WHERE adsid=$_id");
			break;
			default;
			break;
		}
	}
}

$tpl->assign("action",$action);
$tpl->display(ADMIN_TEMPLATE."a.tpl");

?>