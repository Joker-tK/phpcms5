<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
**/
require_once '../source/core/run.php';
require_once 'admin.inc.php';
$action		= Core_Fun::rec_post("action");
$page		= Core_Fun::detect_number(Core_Fun::rec_post("page"),1);
if($page<1){$page=1;}
$comeurl	= "page=$page";

if(Core_Fun::rec_post('act')=='update'){
    updateajax(Core_Fun::rec_post('id'),Core_Fun::rec_post('action'));
}
switch($action){
    case 'add';
	    add();
		break;
	case 'saveadd';
	    saveadd();
		break;
	case 'edit';
	    edit();
		break;
	case 'saveedit';
	    saveedit();
		break;
	case 'del';
	    del();
		break;
	default;
	    volist();
		break;
}

function volist(){
	Core_Auth::checkauth("jobcatevolist");
	global $db,$tpl,$page;
    $pagesize	= 30;
	$searchsql	= " WHERE 1=1";
	$countsql	= "SELECT COUNT(cateid) FROM ".DB_PREFIX."jobcate".$searchsql;
    $total		= $db->fetch_count($countsql);
    $pagecount	= ceil($total/$pagesize);
	$nextpage	= $page+1;
	$prepage	= $page-1;
	$start		= ($page-1)*$pagesize;
	$sql		= "SELECT * FROM ".DB_PREFIX."jobcate".
		          $searchsql." ORDER BY orders ASC LIMIT $start, $pagesize";
	$cate		= $db->getall($sql);
	foreach($cate as $key=>$value){
		$cate[$key]['jobcount'] = $db->fetch_count("SELECT COUNT(jobid) FROM ".DB_PREFIX."job WHERE cateid=".$value['cateid']."");
	}
	$url		= $_SERVER['PHP_SELF'];
	$showpage	= Core_Page::adminpage($total,$pagesize,$page,$url,10);
	$tpl->assign("total",$total);
	$tpl->assign("pagecount",$pagecount);
	$tpl->assign("page",$page);
	$tpl->assign("showpage",$showpage);
	$tpl->assign("cate",$cate);
}

function add(){
	Core_Auth::checkauth("jobcateadd");
	global $tpl,$db;
	$orders  = $db->fetch_newid("SELECT MAX(orders) FROM ".DB_PREFIX."jobcate",1);
	$tpl->assign("flag_checkbox",Core_Mod::checkbox("1","flag","审核"));
	$tpl->assign("elite_checkbox",Core_Mod::checkbox("1","elite","推荐"));
	$tpl->assign("orders",$orders);
}

function edit(){
	Core_Auth::checkauth("jobcateedit");
	global $db,$tpl;
    $id = Core_Fun::rec_post('id');
	if(!Core_Fun::isnumber($id)){
		Core_Fun::halt("ID丢失","",2);
	}
	$sql	= "SELECT * FROM ".DB_PREFIX."jobcate WHERE cateid=$id";
	$cate	= $db->fetch_first($sql);
	if(!$cate){
		Core_Fun::halt("数据不存在","",2);
	}else{
		$cate['imgname'] = Core_Mod::getpicname($cate['logoimg']);
		$tpl->assign("flag_checkbox",Core_Mod::checkbox($cate['flag'],"flag","审核"));
		$tpl->assign("elite_checkbox",Core_Mod::checkbox($cate['elite'],"elite","推荐"));
		$tpl->assign("id",$id);
		$tpl->assign("comeurl",$GLOBALS['comeurl']);
	    $tpl->assign("cate",$cate);
	}
}

function saveadd(){
	Core_Auth::checkauth("jobcateadd");
	global $db;
	$catename			= Core_Fun::rec_post('catename',1);
	$metatitle			= Core_Fun::rec_post('metatitle',1);
	$metakeyword		= Core_Fun::rec_post('metakeyword',1);
	$metadescription	= Core_Fun::rec_post('metadescription',1);
	$pathname			= Core_Fun::rec_post('pathname',1);
	$intro				= Core_Fun::strip_post('intro',1);
	$orders				= Core_Fun::detect_number(Core_Fun::rec_post('orders',1));
	$flag				= Core_Fun::detect_number(Core_Fun::rec_post('flag',1));
	$elite				= Core_Fun::detect_number(Core_Fun::rec_post('elite',1));
	$cssname			= Core_Fun::rec_post('cssname',1);
	$img				= Core_Fun::rec_post('img',1);
	$target				= Core_Fun::detect_number(Core_Fun::rec_post('target',1),1);
	$linktype			= Core_Fun::detect_number(Core_Fun::rec_post('linktype',1),1);
	$linkurl			= Core_Fun::strip_post('linkurl',1);
	$founderr			= false;
	if(!Core_Fun::ischar($catename)){
	    $founderr	= true;
		$errmsg	   .="分类名称不能为空.<br />";
	}
	if($founderr == true){
	    Core_Fun::halt($errmsg,"",1);
	}
	$cateid	= $db->fetch_newid("SELECT MAX(cateid) FROM ".DB_PREFIX."jobcate",1);
	$array	= array(
		'cateid'=>$cateid,
		'catename'=>$catename,
		'metatitle'=>$metatitle,
		'metakeyword'=>$metakeyword,
		'metadescription'=>$metadescription,
		'pathname'=>$pathname,
		'orders'=>$orders,
		'flag'=>$flag,
		'intro'=>$intro,
		'timeline'=>time(),
		'elite'=>$elite,
		'cssname'=>$cssname,
		'img'=>$img,
		'target'=>$target,
		'linktype'=>$linktype,
		'linkurl'=>$linkurl,
	);
	$result = $db->insert(DB_PREFIX."jobcate",$array);
	if($result){
		Core_Command::runlog("","添加招聘分类成功[$catename]",1);
		Core_Fun::halt("保存成功","ljcms_jobcate.php",0);
	}else{
		Core_Fun::halt("保存失败","",1);
	}
}

function saveedit(){
	Core_Auth::checkauth("jobcateedit");
	global $db;
	$id					= Core_Fun::rec_post('id',1);
	$catename			= Core_Fun::rec_post('catename',1);
	$metatitle			= Core_Fun::rec_post('metatitle',1);
	$metakeyword		= Core_Fun::rec_post('metakeyword',1);
	$metadescription	= Core_Fun::rec_post('metadescription',1);
	$pathname			= Core_Fun::rec_post('pathname',1);
	$intro				= Core_Fun::strip_post('intro',1);
	$orders				= Core_Fun::detect_number(Core_Fun::rec_post('orders',1));
	$flag				= Core_Fun::detect_number(Core_Fun::rec_post('flag',1));
	$elite				= Core_Fun::detect_number(Core_Fun::rec_post('elite',1));
	$cssname			= Core_Fun::rec_post('cssname',1);
	$img				= Core_Fun::rec_post('img',1);
	$target				= Core_Fun::detect_number(Core_Fun::rec_post('target',1),1);
	$linktype			= Core_Fun::detect_number(Core_Fun::rec_post('linktype',1),1);
	$linkurl			= Core_Fun::strip_post('linkurl',1);
	$founderr	= false;
	if(!Core_Fun::isnumber($id)){
	    $founderr	= true;
		$errmsg	   .= "ID丢失.<br />";
	}
	if(!Core_Fun::ischar($catename)){
	    $founderr	= true;
		$errmsg	   .="分类名称不能为空.<br />";
	}
	if($founderr == true){
	    Core_Fun::halt($errmsg,"",1);
	}
	$array = array(
		'catename'=>$catename,
		'metatitle'=>$metatitle,
		'metakeyword'=>$metakeyword,
		'metadescription'=>$metadescription,
		'pathname'=>$pathname,
		'orders'=>$orders,
		'flag'=>$flag,
		'intro'=>$intro,
		'elite'=>$elite,
		'cssname'=>$cssname,
		'img'=>$img,
		'target'=>$target,
		'linktype'=>$linktype,
		'linkurl'=>$linkurl,
	);
	$result = $db->update(DB_PREFIX."jobcate",$array,"cateid=$id");
	if($result){
		Core_Command::runlog("","编辑招聘分类成功[id=$id]");
		Core_Fun::halt("编辑成功","ljcms_jobcate.php?".$GLOBALS['comeurl']."",0);
	}else{
		Core_Fun::halt("编辑失败","",2);
	}
}

function del(){
	Core_Auth::checkauth("jobcatedel");
	$arrid  = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
	if($arrid=="" || is_null($arrid)){
		Core_Fun::halt("请选择要删除的数据","",1);
	}
	global $db;
	for($ii=0;$ii<count($arrid);$ii++){
        $id = Core_Fun::replacebadchar(trim($arrid[$ii]));
		if(Core_Fun::isnumber($id)){
			$sql	= "SELECT img FROM ".DB_PREFIX."jobcate WHERE cateid=$id";
			$rows	= $db->fetch_first($sql);
			if($rows){
				if(Core_Fun::ischar($rows['img'])){
					Core_Fun::deletefile("../".$rows['img']);
				}
				$db->query("DELETE FROM ".DB_PREFIX."jobcate WHERE cateid=$id");
				//$db->query("DELETE FROM ".DB_PREFIX."job WHERE cateid=$id");
			}
		}
	}
	Core_Command::runlog("","删除招聘分类成功[id=$arrid]");
	Core_Fun::halt("删除成功","ljcms_jobcate.php",0);
}

function updateajax($_id,$_action){
	Core_Auth::checkauth("jobcateedit");
    if(Core_Fun::isnumber($_id)){
		global $db;
		switch($_action){
			case 'flagopen';
			$db->query("UPDATE ".DB_PREFIX."jobcate SET flag=1 WHERE cateid=$_id");
			break;
			case 'flagclose';
			$db->query("UPDATE ".DB_PREFIX."jobcate SET flag=0 WHERE cateid=$_id");
			break;
			case 'eliteopen';
			$db->query("UPDATE ".DB_PREFIX."jobcate SET elite=1 WHERE cateid=$_id");
			break;
			case 'eliteclose';
			$db->query("UPDATE ".DB_PREFIX."jobcate SET elite=0 WHERE cateid=$_id");
			break;
			default;
			break;
		}
	}
}

$tpl->assign("action",$action);
$tpl->display(ADMIN_TEMPLATE."jobcate.tpl");
$tpl->assign("runtime",runtime());
$tpl->assign("copyright",$libadmin->copyright());
?>