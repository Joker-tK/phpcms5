<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
**/
require_once '../source/core/run.php';
require_once 'admin.inc.php';
$action	= Core_Fun::rec_post("action");
$dir	= Core_Fun::strip_post('dir');
switch($action){
	case 'edit';
	    edit();
		break;
	case 'saveedit';
	    saveedit();
		break;
	case 'del';
	    del();
		break;
	default;
	    volist();
		break;
}

function volist(){
	Core_Auth::checkauth("templatevolist");
	global $dir,$tpl;
	if(!Core_Fun::ischar($dir)){
		$dir = "tpl";
	}

	if(substr($dir,0,3)!="tpl"){
		Core_Fun::halt("对不起，模板管理只允许读取“tpl”目录下的文件！","",1);
	}

	$dirpath = "../".$dir;
	/* 判断是否存在目录 */
	if(!is_dir($dirpath)){
		Core_Fun::halt("对不起，模板目录不存在！","",1);
	}else{
		if(!file_exists($dirpath)){
			Core_Fun::halt("对不起，模板目录不存在！","",1);
		}
	}

	$handle= opendir($dirpath);
	$template = array();
	$i = 0;
	$ii = 1;
	if($handle){
		while(false!==($files=readdir($handle))){
			if($files!="."&&$files!=".."){
				$url_strs = $dir."/".$files;

				if(is_dir($dirpath."/".$files)){
					/* 文件夹 */
					$template = $template + array(
						$i=>array(
						    'i'=>$ii,
						    'type'=>'1',
							'filename'=>$files,
							'size'=>get_filesize($dirpath."/".$files),
							'timeline'=>get_filetime($dirpath."/".$files),
						    'filepath'=>$url_strs,
						)
					);
				}else {
					/* 文件名 */
					$template = $template + array(
						$i=>array(
						    'i'=>$ii,
						    'type'=>'2',
							'filename'=>$files,
							'size'=>get_filesize($dirpath."/".$files),
							'timeline'=>get_filetime($dirpath."/".$files),
						    'filepath'=>$url_strs,
						)
					);
				}
			}
			$i = $i+1;
			$ii = ($ii+1);
		}
	}
	closedir($handle);
	$tpl->assign("dirpath",$dirpath);
	$tpl->assign("dir",$dir);
	$tpl->assign("template",$template);
}

function edit(){
	Core_Auth::checkauth("templateedit");
	global $tpl;
    $urlstrs = Core_Fun::strip_post('urlstrs');
	if(!Core_Fun::ischar($urlstrs)){
		Core_Fun::halt("对不起，请指点要修改的模板文件","",1);
	}
	/* 限制目录 */
	if(substr($urlstrs,0,3)!="tpl"){
		Core_Fun::halt("对不起，只能修改“tpl”目录下的文件！","",1);
	}
	if(!Core_Fun::fileexists("../".$urlstrs)){
		Core_Fun::halt("对不起，模板文件不存在！","",1);
	}

	/* 扩展名 */
	$allow_exts = "tpl|html|htm|js|css";
	$array_file = explode(".",$urlstrs);
	$array_count = count($array_file);
	$file_ext = $array_file[$array_count-1];
	if(!Core_Fun::foundinarr($allow_exts,strtolower($file_ext),"|")){
		Core_Fun::halt("对不起，只允许修改后缀为.tpl,.html,.htm,.js和.css的文件！","",1);
	}

	/* 文件名和目录名 */
	$arrays = explode("/",$urlstrs);
	$counts = count($arrays);
	$filename = $arrays[$counts-1];
	$filepath = str_replace($filename,"",$urlstrs);

	/* 读取文件信息 */
	$content = "";
	$handle = @fopen("../".$urlstrs,"r");
	if(!$handle){
		Core_Fun::halt("对不起，读取文件内容失败！","",1);
	}else{
		while(!feof($handle)){
			$content = $content.tpl_encode(fgets($handle));
		}
		fclose($handle);
	}
	$tpl->assign("content",$content);
	$tpl->assign("dir",substr($filepath,0,(strlen($filepath)-1)));
	$tpl->assign("urlstrs",$urlstrs);
}

function saveedit(){
	Core_Auth::checkauth("templateedit");
    $urlstrs = Core_Fun::strip_post('urlstrs',1);
	$content = Core_Fun::strip_post("content",1);
	if(!Core_Fun::ischar($urlstrs)){
		Core_Fun::halt("文件不存在或者丢失！","",1);
	}
	if(!Core_Fun::ischar($content)){
		Core_Fun::halt("文件内容不能为空","",1);
	}
    
	if(!Core_Fun::fileexists("../".$urlstrs)){
		Core_Fun::halt("对不起，模板文件不存在！","",1);
	}
	/* 限制目录 */
	if(substr($urlstrs,0,3)!="tpl"){
		Core_Fun::halt("对不起，只能修改“tpl”目录下的文件！","",1);
	}

	/* 扩展名 */
	$allow_exts = "tpl|html|htm|js|css";
	$array_file = explode(".",$urlstrs);
	$array_count = count($array_file);
	$file_ext = $array_file[$array_count-1];
	if(!Core_Fun::foundinarr($allow_exts,strtolower($file_ext),"|")){
		Core_Fun::halt("对不起，只允许修改后缀为.tpl,.html,.htm,.js和.css的文件！","",1);
	}

	/* 文件名和目录名 */
	$arrays = explode("/",$urlstrs);
	$counts = count($arrays);
	$filename = $arrays[$counts-1];
	$filepath = str_replace($filename,"",$urlstrs);

	$content = tpl_decode($content);

	/* 检测文件 */
	if(!is_writeable("../".$urlstrs)){
		Core_Fun::halt("对不起，该文件没有修改的权限！请设置tpl目录权限后再试！","",1);
	}else{
		$handle = fopen("../".$urlstrs,"wb");
		if(!$handle){
			Core_Fun::halt("对不起，不能打开该文件！","",1);
		}else{
			if(@fwrite($handle,$content)===FALSE){
				Core_Fun::halt("对不起，文件修改失败，请检查该文件是否使用中！","",1);
			}else{
				Core_Command::runlog("","编辑文件成功[".$urlstrs."]",1);
				Core_Fun::halt("模板文件修改成功","ljcms_template.php?dir=".urlencode(substr($filepath,0,(strlen($filepath)-1)))."",0);
			}
		}
		fclose($handle);
	}
}

function del(){
	Core_Auth::checkauth("templatedel");
    $urlstrs = Core_Fun::strip_post('urlstrs');

	if(!Core_Fun::ischar($urlstrs)){
		Core_Fun::halt("请选择要删除的文件！","",1);
	}
	if(!Core_Fun::fileexists("../".$urlstrs)){
		Core_Fun::halt("对不起，模板文件不存在！","",1);
	}
	if(substr($urlstrs,0,3)!="tpl"){
		Core_Fun::halt("对不起，只能删除“tpl”目录下的文件！","",1);
	}
	/* 文件名和目录名 */
	$arrays = explode("/",$urlstrs);
	$counts = count($arrays);
	$filename = $arrays[$counts-1];
	$filepath = str_replace($filename,"",$urlstrs);

	Core_Fun::deletefile("../".$urlstrs);
	Core_Command::runlog("","删除文件成功[".$urlstrs."]",1);
	Core_Fun::halt("文件删除成功","ljcms_template.php?dir=".urlencode(substr($filepath,0,(strlen($filepath)-1)))."",0);
}

/* 获取文件大小 */
function get_filesize($a){
	if(file_exists($a)){
		return fileSize($a);
	}
}

/* 获取修改时间 */
function get_filetime($a){
	if(file_exists($a)){
		return filemTime($a);
	}
}

function tpl_encode($str){
	$str=str_replace("<","&lt;",$str);
	$str=str_replace(">","&gt;",$str);
	return $str;
}

function tpl_decode($str){
	$str=str_replace("&lt;","<",$str);
	$str=str_replace("&gt;",">",$str);
	return $str;
}

$tpl->assign("action",$action);
$tpl->display(ADMIN_TEMPLATE."template.tpl");
$tpl->assign("runtime",runtime());
$tpl->assign("copyright",$libadmin->copyright());
?>