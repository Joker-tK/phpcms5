<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
**/
require_once '../source/core/run.php';
require_once 'admin.inc.php';
$action		= Core_Fun::rec_post("action");
$page		= Core_Fun::detect_number(Core_Fun::rec_post("page"),1);
if($page<1){$page=1;}
$comeurl	= "page=$page";

if(Core_Fun::rec_post('act')=='update'){
    updateajax(Core_Fun::rec_post('id'),Core_Fun::rec_post('action'));
}
switch($action){
    case 'add';
	    add();
		break;
	case 'saveadd';
	    saveadd();
		break;
	case 'edit';
	    edit();
		break;
	case 'saveedit';
	    saveedit();
		break;
	case 'del';
	    del();
		break;
	case 'setting';
	    setting();
		break;
	case 'savesetting';
	    savesetting();
		break;
	default;
	    volist();
		break;
}

function volist(){
	Core_Auth::checkauth("modulevolist");
	global $db,$tpl,$page;
    $pagesize	= 30;
	$searchsql	= " WHERE 1=1";
	$countsql	= "SELECT COUNT(modid) FROM ".DB_PREFIX."module".$searchsql;
    $total		= $db->fetch_count($countsql);
    $pagecount	= ceil($total/$pagesize);
	$nextpage	= $page+1;
	$prepage	= $page-1;
	$start		= ($page-1)*$pagesize;
	$sql		= "SELECT * FROM ".DB_PREFIX."module".
		          $searchsql." ORDER BY modid ASC LIMIT $start, $pagesize";
	$module		= $db->getall($sql);
//	foreach($cate as $key=>$value){
//		$cate[$key]['casecount'] = $db->fetch_count("SELECT COUNT(modid) FROM ".DB_PREFIX."case WHERE cateid=".$value['cateid']."");
//	}
	$url		= $_SERVER['PHP_SELF'];
	$showpage	= Core_Page::adminpage($total,$pagesize,$page,$url,10);
	$tpl->assign("total",$total);
	$tpl->assign("pagecount",$pagecount);
	$tpl->assign("page",$page);
	$tpl->assign("showpage",$showpage);
	$tpl->assign("module",$module);
}

function setting(){
	Core_Auth::checkauth("moduleedit");
	global $db,$tpl,$page;
    $pagesize	= 30;
	$searchsql	= " WHERE 1=1";
	$countsql	= "SELECT COUNT(modid) FROM ".DB_PREFIX."module".$searchsql;
    $total		= $db->fetch_count($countsql);
    $pagecount	= ceil($total/$pagesize);
	$nextpage	= $page+1;
	$prepage	= $page-1;
	$start		= ($page-1)*$pagesize;
	$sql		= "SELECT * FROM ".DB_PREFIX."module".
		          $searchsql." ORDER BY modid ASC LIMIT $start, $pagesize";
	$module		= $db->getall($sql);
	$url		= $_SERVER['PHP_SELF'];
	$showpage	= Core_Page::adminpage($total,$pagesize,$page,$url,10);
	$tpl->assign("total",$total);
	$tpl->assign("pagecount",$pagecount);
	$tpl->assign("page",$page);
	$tpl->assign("showpage",$showpage);
	$tpl->assign("module",$module);
}

function add(){
	Core_Auth::checkauth("moduleadd");
	global $tpl,$db;
	$orders  = $db->fetch_newid("SELECT MAX(modid) FROM ".DB_PREFIX."module",1);
	//$tpl->assign("flag_checkbox",Core_Mod::checkbox("1","flag","审核"));
	//$tpl->assign("elite_checkbox",Core_Mod::checkbox("1","elite","推荐"));
	$tpl->assign("orders",$orders);
}

function edit(){
	Core_Auth::checkauth("moduleedit");
	global $db,$tpl;
    $id = Core_Fun::rec_post('id');
	if(!Core_Fun::isnumber($id)){
		Core_Fun::halt("ID丢失","",2);
	}
	$sql	= "SELECT * FROM ".DB_PREFIX."module WHERE modid=$id";
	$module	= $db->fetch_first($sql);
	if(!$module){
		Core_Fun::halt("数据不存在","",2);
	}else{
		//$cate['imgname'] = Core_Mod::getpicname($cate['logoimg']);
		$tpl->assign("flag_checkbox",Core_Mod::checkbox($cate['enabled'],"enabled","状态"));
	//.	$tpl->assign("elite_checkbox",Core_Mod::checkbox($cate['elite'],"elite","推荐"));
		$tpl->assign("id",$id);
		$tpl->assign("comeurl",$GLOBALS['comeurl']);
	    $tpl->assign("module",$module);
	}
}

function saveadd(){
	Core_Auth::checkauth("moduleadd");
	global $db;
	$modname		= Core_Fun::rec_post('modname',1);
	$alias			= Core_Fun::rec_post('alias',1);
	$color		    = Core_Fun::rec_post('color',1);
	$tplindex	    = Core_Fun::rec_post('tplindex',1);
	$tpllist	    = Core_Fun::rec_post('tpllist',1);
	$tpldetail	    = Core_Fun::rec_post('tpldetail',1);
	$posts			= Core_Fun::detect_number(Core_Fun::rec_post('posts',1));
	$comments		= Core_Fun::detect_number(Core_Fun::rec_post('comments',1));
	$pv				= Core_Fun::detect_number(Core_Fun::rec_post('pv',1),1);
	$sort			= Core_Fun::detect_number(Core_Fun::rec_post('sort',1),1);
	$enabled		= Core_Fun::detect_number(Core_Fun::rec_post('enabled',1));
	$intro			= Core_Fun::strip_post('intro',1);
	$founderr			= false;
	if(!Core_Fun::ischar($modname)){
	    $founderr	= true;
		$errmsg	   .="模块名称不能为空.<br />";
	}
	if($founderr == true){
	    Core_Fun::halt($errmsg,"",1);
	}
	$cateid	= $db->fetch_newid("SELECT MAX(modid) FROM ".DB_PREFIX."module",1);
	$array	= array(
		'modid'=>$modid,
		'modname'=>$modname,
		'alias'=>$alias,
		'color'=>$color,
		'tplindex'=>$tplindex,
		'tpllist'=>$tpllist,
		'tpldetail'=>$tpldetail,
		'posts'=>$posts,
		'comments'=>$comments,
		'pv'=>$pv,
		'sort'=>$sort,
		'enabled'=>$enabled,
		'intro'=>$intro,
	);
	$result = $db->insert(DB_PREFIX."module",$array);
	if($result){
		Core_Command::runlog("","添加模块成功[$modname]",1);
		Core_Fun::halt("保存成功","ljcms_module.php",0);
	}else{
		Core_Fun::halt("保存失败","",1);
	}
}

function saveedit(){
	Core_Auth::checkauth("modidedit");
	global $db;
	$id			= Core_Fun::rec_post('id',1);
	$modname		= Core_Fun::rec_post('modname',1);
	$alias			= Core_Fun::rec_post('alias',1);
	$color		    = Core_Fun::rec_post('color',1);
	$tplindex	    = Core_Fun::rec_post('tplindex',1);
	$tpllist	    = Core_Fun::rec_post('tpllist',1);
	$tpldetail	    = Core_Fun::rec_post('tpldetail',1);
	$posts			= Core_Fun::detect_number(Core_Fun::rec_post('posts',1));
	$comments		= Core_Fun::detect_number(Core_Fun::rec_post('comments',1));
	$pv				= Core_Fun::detect_number(Core_Fun::rec_post('pv',1),1);
	$sort			= Core_Fun::detect_number(Core_Fun::rec_post('sort',1),1);
	$enabled		= Core_Fun::detect_number(Core_Fun::rec_post('enabled',1));
	$intro			= Core_Fun::strip_post('intro',1);

	$founderr	= false;
	if(!Core_Fun::isnumber($id)){
	    $founderr	= true;
		$errmsg	   .= "ID丢失.<br />";
	}
	if(!Core_Fun::ischar($modname)){
	    $founderr	= true;
		$errmsg	   .="模块名称不能为空.<br />";
	}
	if($founderr == true){
	    Core_Fun::halt($errmsg,"",1);
	}
	$array = array(
		'modname'=>$modname,
		'alias'=>$alias,
		'color'=>$color,
		'tplindex'=>$tplindex,
		'tpllist'=>$tpllist,
		'tpldetail'=>$tpldetail,
		'posts'=>$posts,
		'comments'=>$comments,
		'pv'=>$pv,
		'sort'=>$sort,
		'enabled'=>$enabled,
		'intro'=>$intro,
	);
	$result = $db->update(DB_PREFIX."module",$array,"modid=$id");
	if($result){
		Core_Command::runlog("","编辑案例分类成功[id=$id]");
		Core_Fun::halt("编辑成功","ljcms_module.php?".$GLOBALS['comeurl']."",0);
	}else{
		Core_Fun::halt("编辑失败","",2);
	}
}

function del(){
	Core_Auth::checkauth("modiddel");
	$arrid  = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
	if($arrid=="" || is_null($arrid)){
		Core_Fun::halt("请选择要删除的数据","",1);
	}
	global $db;
	for($ii=0;$ii<count($arrid);$ii++){
        $id = Core_Fun::replacebadchar(trim($arrid[$ii]));
		if(Core_Fun::isnumber($id)){
			$sql	= "SELECT modid FROM ".DB_PREFIX."module WHERE modid=$id";
			$rows	= $db->fetch_first($sql);
			if($rows){
				$db->query("DELETE FROM ".DB_PREFIX."module WHERE modid=$id");
			}
		}
	}
	Core_Command::runlog("","删除案例分类成功[id=$arrid]");
	Core_Fun::halt("删除成功","ljcms_module.php",0);
}

function updateajax($_id,$_action){
	Core_Auth::checkauth("modidedit");
    if(Core_Fun::isnumber($_id)){
		global $db;
		switch($_action){
			case 'flagopen';
			$db->query("UPDATE ".DB_PREFIX."module SET enabled=1 WHERE modid=$_id");
			break;
			case 'flagclose';
			$db->query("UPDATE ".DB_PREFIX."module SET enabled=0 WHERE modid=$_id");
			break;
			case 'eliteopen';
			$db->query("UPDATE ".DB_PREFIX."module SET enabled=1 WHERE modid=$_id");
			break;
			case 'eliteclose';
			$db->query("UPDATE ".DB_PREFIX."module SET enabled=0 WHERE modid=$_id");
			break;
			default;
			break;
		}
	}
}

function savesetting(){
	Core_Auth::checkauth("modidedit");
	$arrid  = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
	if($arrid=="" || is_null($arrid)){
		Core_Fun::halt("请选择要更新的数据","",1);
	}
	global $db;
	for($ii=0;$ii<count($arrid);$ii++){
        $id					= (int)($arrid[$ii]);
		$modname	= Core_Fun::rec_post("modname_".$id,1);
		$color		= Core_Fun::rec_post("color_".$id,1);
		$tplindex	= Core_Fun::rec_post("tplindex_".$id,1);
		$tpllist	= Core_Fun::rec_post("tpllist_".$id,1);
	    $tpldetail	= Core_Fun::rec_post("tpldetailt_".$id,1);
	    $enabled	= Core_Fun::rec_post("enabled_".$id,1);

		if(Core_Fun::isnumber($id)){
			$array = array(
				'modname'=>$modname,
				'color'=>$color,
				'tplindex'=>$tplindex,
				'tpllist'=>$tpllist,
				'tpldetail'=>$tpldetail,
				'enabled'=>$enabled,
			);
			$db->update(DB_PREFIX."module",$array,"modid=$id");
		}
	}
	Core_Fun::halt("批量更新成功","ljcms_module.php",0);
}

$tpl->assign("action",$action);
$tpl->display(ADMIN_TEMPLATE."module.tpl");
$tpl->assign("runtime",runtime());
$tpl->assign("copyright",$libadmin->copyright());
?>