<?php
require_once '../source/core/run.php';
require_once 'admin.inc.php';
$action		= Core_Fun::rec_post("action");
$page		= Core_Fun::detect_number(Core_Fun::rec_post("page"),1);
if($page<1){$page=1;}
$comeurl	= "page=$page";

if(Core_Fun::rec_post('act')=='update'){
    updateajax(Core_Fun::rec_post('id'),Core_Fun::rec_post('action'));
}
switch($action){
    case 'add';
	    add();
		break;
	case 'saveadd';
	    saveadd();
		break;
	case 'show';
	    show();
		break;
	case 'saveedit';
	    saveedit();
		break;
	case 'del';
	    del();
		break;
	default;
	    volist();
		break;
}

function volist(){
///	Core_Auth::checkauth("applyjobvolist");
	global $db,$tpl,$page;
    $pagesize	= 30;
	$searchsql	= " WHERE 1=1";
	$countsql	= "SELECT COUNT(aid) FROM ".DB_PREFIX."applyjob".$searchsql;
    $total		= $db->fetch_count($countsql);
    $pagecount	= ceil($total/$pagesize);
	$nextpage	= $page+1;
	$prepage	= $page-1;
	$start		= ($page-1)*$pagesize;
	$sql		= "SELECT a.*,j.title AS jobtitle".
	             " FROM ".DB_PREFIX."applyjob AS a".
		         " LEFT JOIN ".DB_PREFIX."job  AS j ON a.jobid=j.jobid".
		         $searchsql." ORDER BY a.aid ASC LIMIT $start, $pagesize";
	$ajob		= $db->getall($sql);
	$url		= $_SERVER['PHP_SELF'];
	$showpage	= Core_Page::adminpage($total,$pagesize,$page,$url,10);
	$tpl->assign("total",$total);
	$tpl->assign("pagecount",$pagecount);
	$tpl->assign("page",$page);
	$tpl->assign("showpage",$showpage);
	$tpl->assign("ajob",$ajob);
}

function add(){
	Core_Auth::checkauth("applyjobadd");
	global $tpl;
	$tpl->assign("flag_checkbox",Core_Mod::checkbox("1","flag","审核"));
}

function show(){
	//Core_Auth::checkauth("applyjobedit");
	global $db,$tpl;
    $id = Core_Fun::rec_post('id');
	if(!Core_Fun::isnumber($id)){
		Core_Fun::halt("ID丢失","",2);
	}
	$sql		= "SELECT a.*,j.title AS jobtitle".
	             " FROM ".DB_PREFIX."applyjob AS a".
		         " LEFT JOIN ".DB_PREFIX."job  AS j ON a.jobid=j.jobid WHERE a.aid=$id";
	$ajob	= $db->fetch_first($sql);
	if(!$ajob){
		Core_Fun::halt("数据不存在","",2);
	}else{
		$tpl->assign("flag_checkbox",Core_Mod::checkbox($ajob['flag'],"flag","审核"));
		$tpl->assign("id",$id);
		$tpl->assign("comeurl",$GLOBALS['comeurl']);
	    $tpl->assign("ajob",$ajob);
	}
}

function saveadd(){
	Core_Auth::checkauth("applyjobadd");
	global $db;
	$title			= Core_Fun::rec_post('title',1);
	$ajobuser		= Core_Fun::rec_post('ajobuser',1);
	$gender			= Core_Fun::detect_number(Core_Fun::rec_post('gender',1));
	$jobs			= Core_Fun::rec_post('jobs',1);
	$telephone		= Core_Fun::rec_post('telephone',1);
	$fax			= Core_Fun::rec_post('fax',1);
	$mobile			= Core_Fun::rec_post('mobile',1);
	$email			= Core_Fun::rec_post('email',1);
	$qqmsn			= Core_Fun::rec_post('qqmsn',1);
	$companyname	= Core_Fun::rec_post('companyname',1);
	$address		= Core_Fun::rec_post('address',1);
	$trade			= Core_Fun::rec_post('trade',1);
	$content		= Core_Fun::strip_post('content',1);
	$replyuser		= Core_Fun::rec_post('replyuser',1);
	$replycontent	= Core_Fun::strip_post('replycontent',1);
	$flag			= Core_Fun::detect_number(Core_Fun::rec_post('flag',1));
	$founderr		= false;
	if(!Core_Fun::ischar($ajobuser)){
	    $founderr	= true;
		$errmsg	   .="留言者不能为空.<br />";
	}
	if(!Core_Fun::ischar($content)){
	    $founderr	= true;
		$errmsg	   .="留言内容不能为空.<br />";
	}
	if($founderr == true){
	    Core_Fun::halt($errmsg,"",1);
	}
	$aid	= $db->fetch_newid("SELECT MAX(aid) FROM ".DB_PREFIX."applyjob",1);
	$array	= array(
		'aid'=>$aid,
		'title'=>$title,
		'ajobuser'=>$ajobuser,
		'gender'=>$gender,
		'jobs'=>$jobs,
		'telephone'=>$telephone,
		'fax'=>$fax,
		'mobile'=>$mobile,
		'email'=>$email,
		'qqmsn'=>$qqmsn,
		'companyname'=>$companyname,
		'address'=>$address,
		'trade'=>$trade,
		'homepage'=>$homepage,
		'content'=>$content,
		'ajobtimeline'=>time(),
		'ip'=>Core_Fun::getip(),
		'flag'=>$flag,
	);
	$result = $db->insert(DB_PREFIX."applyjob",$array);
	if($result){
		Core_Command::runlog("","添加留言成功",1);
		Core_Fun::halt("保存成功","ljcms_applyjob.php",0);
	}else{
		Core_Fun::halt("保存失败","",1);
	}
}

function saveedit(){
	Core_Auth::checkauth("applyjobedit");
	global $db;
	$id				= Core_Fun::rec_post('id',1);
	$title			= Core_Fun::rec_post('title',1);
	$ajobuser		= Core_Fun::rec_post('ajobuser',1);
	$gender			= Core_Fun::detect_number(Core_Fun::rec_post('gender',1));
	$jobs			= Core_Fun::rec_post('jobs',1);
	$telephone		= Core_Fun::rec_post('telephone',1);
	$fax			= Core_Fun::rec_post('fax',1);
	$mobile			= Core_Fun::rec_post('mobile',1);
	$email			= Core_Fun::rec_post('email',1);
	$qqmsn			= Core_Fun::rec_post('qqmsn',1);
	$companyname	= Core_Fun::rec_post('companyname',1);
	$address		= Core_Fun::rec_post('address',1);
	$trade			= Core_Fun::rec_post('trade',1);
	$content		= Core_Fun::strip_post('content',1);
	$replyuser		= Core_Fun::rec_post('replyuser',1);
	$replycontent	= Core_Fun::strip_post('replycontent',1);
	$flag			= Core_Fun::detect_number(Core_Fun::rec_post('flag',1));
	$founderr		= false;
	if(!Core_Fun::isnumber($id)){
	    $founderr	= true;
		$errmsg	   .= "ID丢失.<br />";
	}
	if(!Core_Fun::ischar($ajobuser)){
	    $founderr	= true;
		$errmsg	   .="留言者不能为空.<br />";
	}
	if(!Core_Fun::ischar($content)){
	    $founderr	= true;
		$errmsg	   .="留言内容不能为空.<br />";
	}
	if($founderr == true){
	    Core_Fun::halt($errmsg,"",1);
	}
	$array = array(
		'title'=>$title,
		'ajobuser'=>$ajobuser,
		'gender'=>$gender,
		'jobs'=>$jobs,
		'telephone'=>$telephone,
		'fax'=>$fax,
		'mobile'=>$mobile,
		'email'=>$email,
		'qqmsn'=>$qqmsn,
		'companyname'=>$companyname,
		'address'=>$address,
		'trade'=>$trade,
		'homepage'=>$homepage,
		'content'=>$content,
		'flag'=>$flag,
		'replyuser'=>$replyuser,
		'replycontent'=>$replycontent,
	);
	if(Core_Fun::ischar($replycontent)){
		$array = $array + array('replytimeline'=>time());
	}
	$result = $db->update(DB_PREFIX."applyjob",$array,"aid=$id");
	if($result){
		Core_Command::runlog("","编辑/回复留言成功[id=$id]");
		Core_Fun::halt("编辑成功","ljcms_applyjob.php?".$GLOBALS['comeurl']."",0);
	}else{
		Core_Fun::halt("编辑失败","",2);
	}
}

function del(){
	Core_Auth::checkauth("applyjobdel");
	$arrid  = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
	if($arrid=="" || is_null($arrid)){
		Core_Fun::halt("请选择要删除的数据","",1);
	}
	global $db;
	for($ii=0;$ii<count($arrid);$ii++){
        $id = Core_Fun::replacebadchar(trim($arrid[$ii]));
		if(Core_Fun::isnumber($id)){
			$db->query("DELETE FROM ".DB_PREFIX."applyjob WHERE aid=$id");
		}
	}
	Core_Command::runlog("","删除留言成功[id=$arrid]");
	Core_Fun::halt("删除成功","ljcms_applyjob.php",0);
}

function updateajax($_id,$_action){
	Core_Auth::checkauth("applyjobedit");
    if(Core_Fun::isnumber($_id)){
		global $db;
		switch($_action){
			case 'flagopen';
			$db->query("UPDATE ".DB_PREFIX."applyjob SET flag=1 WHERE aid=$_id");
			break;
			case 'flagclose';
			$db->query("UPDATE ".DB_PREFIX."applyjob SET flag=0 WHERE aid=$_id");
			break;
			default;
			break;
		}
	}
}

$tpl->assign("action",$action);
$tpl->display(ADMIN_TEMPLATE."applyjobddd.tpl");
$tpl->assign("runtime",runtime());
$tpl->assign("copyright",$libadmin->copyright());
?>