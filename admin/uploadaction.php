<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.01.15
**/
require_once '../source/core/run.php';
require_once 'admin.inc.php';
echo"<meta http-equiv='Content-Type' content='text/html;charset=".LJCMS_CHARSET."' />";
$uptypes        = array('image/jpg','image/jpeg','image/pjpeg','image/png','image/gif','image/bmp','image/x-png');
$allowextension = array('jpg','jpeg','png','gif','bmp','png');
$max_file_size  = 5000000;
$comeform		= isset($_REQUEST['comeform']) ? trim($_REQUEST['comeform']) : "";
$picfolder		= isset($_REQUEST['picfolder']) ? trim($_REQUEST['picfolder']) : "";
$inputid		= isset($_REQUEST['inputid']) ? trim($_REQUEST['inputid']) : "";
$thumbinput	    = isset($_REQUEST['thumbinput']) ? trim($_REQUEST['thumbinput']) : "";
$channel		= isset($_REQUEST['channel']) ? trim($_REQUEST['channel']) : "";
$thumbflag		= isset($_REQUEST['thumbflag']) ? intval($_REQUEST['thumbflag']) : 0;
$thumbflag2		= isset($_REQUEST['thumbflag2']) ? intval($_REQUEST['thumbflag2']) : 0;
$thumbwidth		= isset($_REQUEST['thumbwidth']) ? intval($_REQUEST['thumbwidth']) : 0;
$thumbheight	= isset($_REQUEST['thumbheight']) ? intval($_REQUEST['thumbheight']) : 0;
$waterflag		= isset($_REQUEST['waterflag']) ? intval($_REQUEST['waterflag']) : 0;

$attachmentdir = "data/attachment/";
if(Core_Fun::ischar($picfolder)){
	if(substr($picfolder,-1,1)=="/"){
	}else{
	    $picfolder .= "/";
	}
}else{
	$yfolder = date('Y').date('m')."/";
	if(!file_exists("../".$attachmentdir.$yfolder)){
		mkdir("../".$attachmentdir.$yfolder);
	}
	$dfolder = date('d')."/";
	if(!file_exists("../".$attachmentdir.$yfolder.$dfolder)){
		mkdir("../".$attachmentdir.$yfolder.$dfolder);
	}
	$picfolder = $yfolder.$dfolder;
}
$uploadfolder =$attachmentdir.$picfolder;
if(!file_exists("../".$uploadfolder)){
	mkdir("../".$uploadfolder);
}

if ($_SERVER['REQUEST_METHOD'] == 'POST'){
	/* 本地图片处理 */
    if (!is_uploaded_file($_FILES["sf_upfile"]['tmp_name'])){
		echo "<font color='red'>文件不存在！</font>";
		exit;
	}
    $file	= $_FILES["sf_upfile"];
    if($max_file_size < $file["size"]){
		echo "<font color='red'>文件太大！超过了5M.</font>";
		exit;
	}
	if(!in_array($file["type"], $uptypes)){
		echo "<font color='red'>对不起，只能上传图片格式,JPG,GIF,JPEG,PNG,BMP</font>";
		exit;
	}
    $image_filename	= $file["tmp_name"];
    $image_size		= getimagesize($image_filename); 
    $image_pinfo	= pathinfo($file["name"]);
    $image_ftype	= $image_pinfo['extension'];
	if(!in_array(strtolower($image_ftype),$allowextension)){
		echo "<font color='red'>对不起，图片后缀只能接受JPG,GIF,JPEG,PNG,BMP</font>";
		die();
	}
	/* 本地图片处理 end */
	$time					= time();
	$rndnum					= Core_Fun::get_rndchar(6);
	$mkname					= md5($time.$rndnum);
    $upload_filename		= $mkname.".".$image_ftype; //新的文件名

	$uploadfiles			= $uploadfolder.$upload_filename;
	$thumbfiles				= $uploadfiles.".thumb.jpg";
    $uploaddestination		= "../".$uploadfiles;
    $thumbdestination		= "../".$thumbfiles;
    
	/* 保存图片 */
	if(!move_uploaded_file ($image_filename, $uploaddestination)){
		echo "<font color='red'>移动文件出错！</a>";
		exit;
	}
	/* 缩 略 图 */
	if($thumbflag==1){
		Core_Image::makethumb($uploaddestination,$channel,480,480);
		Core_Image::makethumb_max($uploaddestination,$channel,280,280);
	}
	
	
    /* 水 印 */
	if($waterflag==1){
        Core_Image::makewatermark($uploaddestination);
	}
	/* 提 示 */
	if($thumbinput!=""){
		echo("<script language='javascript' type='text/javascript'>");
		echo("parent.document.getElementById('".$inputid."').value='".$uploadfiles."';parent.document.getElementById('".$thumbinput."').value='".$thumbfiles."';");
		echo("</script>");
	}else{
		echo("<script language='javascript' type='text/javascript'>");
		echo("parent.document.getElementById('".$inputid."').value='".$uploadfiles."';");
		echo("</script>");
	}
	echo("<script language='javascript' type='text/javascript'>window.location.href='upload.php?action=show&comeform=".$myform."&picfolder=".$picfolder."&inputid=".$inputid."&thumbinput=".$thumbinput."&channel=".$channel."&thumbflag=".$thumbflag."&thumbwidth=".$thumbwidth."&thumbheight=".$thumbheight."&waterflag=$waterflag&picname=".$upload_filename."&picurl=".$uploadfiles."';</script>");
}
?>