<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
**/
require_once '../source/core/run.php';
require_once 'admin.inc.php';
$action		= Core_Fun::rec_post("action");
$page		= Core_Fun::detect_number(Core_Fun::rec_post("page"),1);
$scateid    = Core_Fun::detect_number(Core_Fun::rec_post("scateid"));
$sname      = Core_Fun::rec_post("sname",1);
if($page<1){$page=1;}
$comeurl	= "page=$page&scateid=$scateid&sname=".urlencode($sname)."";

if(Core_Fun::rec_post('act')=='update'){
    updateajax(Core_Fun::rec_post('id'),Core_Fun::rec_post('action'));
}
switch($action){
    case 'add';
	    add();
		break;
	case 'saveadd';
	    saveadd();
		break;
	case 'edit';
	    edit();
		break;
	case 'saveedit';
	    saveedit();
		break;
	case 'del';
	    del();
		break;
	default;
	    volist();
		break;
}

function volist(){
	Core_Auth::checkauth("jobvolist");
	global $db,$tpl,$page,$scateid,$sname;
    $pagesize	= 30;
	$searchsql	= " WHERE 1=1";
	if($scateid>0){
		$searchsql .= " AND j.cateid=$scateid";
	}
	if(Core_Fun::ischar($sname)){
		$searchsql .= " AND j.title LIKE '%".$sname."%'";
	}
	$countsql	= "SELECT COUNT(j.jobid) FROM ".DB_PREFIX."job AS j".$searchsql;
    $total		= $db->fetch_count($countsql);
    $pagecount	= ceil($total/$pagesize);
	$nextpage	= $page+1;
	$prepage	= $page-1;
	$start		= ($page-1)*$pagesize;
	$sql		= "SELECT j.*,c.catename".
		          " FROM ".DB_PREFIX."job AS j".
		          " LEFT JOIN ".DB_PREFIX."jobcate AS c ON j.cateid=c.cateid".
		          $searchsql." ORDER BY j.jobid DESC LIMIT $start, $pagesize";
	$job		= $db->getall($sql);
	$url		= $_SERVER['PHP_SELF'];
	$urlitem	= "scateid=$scateid&sname=".urlencode($sname)."";
	$url	   .= "?".$urlitem;
	$showpage	= Core_Page::adminpage($total,$pagesize,$page,$url,10);
	$tpl->assign("total",$total);
	$tpl->assign("pagecount",$pagecount);
	$tpl->assign("page",$page);
	$tpl->assign("showpage",$showpage);
	$tpl->assign("job",$job);
	$tpl->assign("urlitem",$urlitem);
	$tpl->assign("cate_search",Core_Mod::db_select($scateid,"scateid","jobcate"));
	$tpl->assign("sname",$sname);
}

function add(){
	Core_Auth::checkauth("jobadd");
	global $tpl;
	$tpl->assign("flag_checkbox",Core_Mod::checkbox("1","flag","审核"));
	$tpl->assign("cate_select",Core_Mod::db_select("","cateid","jobcate"));
}

function edit(){
	Core_Auth::checkauth("jobedit");
	global $db,$tpl;
    $id = Core_Fun::rec_post('id');
	if(!Core_Fun::isnumber($id)){
		Core_Fun::halt("ID丢失","",2);
	}
	$sql	= "SELECT * FROM ".DB_PREFIX."job WHERE jobid=$id";
	$job	= $db->fetch_first($sql);
	if(!$job){
		Core_Fun::halt("数据不存在","",2);
	}else{
		$tpl->assign("flag_checkbox",Core_Mod::checkbox($job['flag'],"flag","审核"));
		$tpl->assign("cate_select",Core_Mod::db_select($job['cateid'],"cateid","jobcate"));
		$tpl->assign("id",$id);
		$tpl->assign("comeurl",$GLOBALS['comeurl']);
	    $tpl->assign("job",$job);
	}
}

function saveadd(){
	Core_Auth::checkauth("jobadd");
	global $db;
	$cateid			= Core_Fun::detect_number(Core_Fun::rec_post('cateid',1));
	$title			= Core_Fun::rec_post('title',1);
	$workarea		= Core_Fun::strip_post('workarea',1);
	$number			= Core_Fun::detect_number(Core_Fun::rec_post('number',1));
	$jobdescription	= Core_Fun::strip_post('jobdescription',1);
	$jobrequest		= Core_Fun::strip_post('jobrequest',1);
	$jobotherrequest= Core_Fun::strip_post('jobotherrequest',1);
	$jobcontact		= Core_Fun::strip_post('jobcontact',1);
	$flag			= Core_Fun::detect_number(Core_Fun::rec_post('flag',1));
	$founderr		= false;
	if($cateid<1){
	    $founderr	= true;
		$errmsg	   .="请选择招聘分类.<br />";
	}
	if(!Core_Fun::ischar($title)){
	    $founderr	= true;
		$errmsg	   .="招聘职位不能为空.<br />";
	}
	if(!Core_Fun::ischar($jobdescription)){
	    $founderr	= true;
		$errmsg	   .="岗位职责不能为空.<br />";
	}
	if(!Core_Fun::ischar($jobrequest)){
	    $founderr	= true;
		$errmsg	   .="职位要求不能为空.<br />";
	}
	if($founderr == true){
	    Core_Fun::halt($errmsg,"",1);
	}
	$jobid	= $db->fetch_newid("SELECT MAX(jobid) FROM ".DB_PREFIX."job",1);
	$array	= array(
		'jobid'=>$jobid,
		'cateid'=>$cateid,
		'title'=>$title,
		'workarea'=>$workarea,
		'number'=>$number,
		'jobdescription'=>$jobdescription,
		'jobrequest'=>$jobrequest,
		'jobotherrequest'=>$jobotherrequest,
		'jobcontact'=>$jobcontact,
		'flag'=>$flag,
		'timeline'=>time(),
	);
	$result = $db->insert(DB_PREFIX."job",$array);
	if($result){
		Core_Command::runlog("","发布招聘信息成功[$adsname]",1);
		Core_Fun::halt("保存成功","ljcms_job.php",0);
	}else{
		Core_Fun::halt("保存失败","",1);
	}
}

function saveedit(){
	Core_Auth::checkauth("jobedit");
	global $db;
	$id				= Core_Fun::rec_post('id',1);
	$cateid			= Core_Fun::detect_number(Core_Fun::rec_post('cateid',1));
	$title			= Core_Fun::rec_post('title',1);
	$workarea		= Core_Fun::strip_post('workarea',1);
	$number			= Core_Fun::detect_number(Core_Fun::rec_post('number',1));
	$jobdescription	= Core_Fun::strip_post('jobdescription',1);
	$jobrequest		= Core_Fun::strip_post('jobrequest',1);
	$jobotherrequest= Core_Fun::strip_post('jobotherrequest',1);
	$jobcontact		= Core_Fun::strip_post('jobcontact',1);
	$flag			= Core_Fun::detect_number(Core_Fun::rec_post('flag',1));
	$founderr	= false;
	if(!Core_Fun::isnumber($id)){
	    $founderr	= true;
		$errmsg	   .= "ID丢失.<br />";
	}
	if($cateid<1){
	    $founderr	= true;
		$errmsg	   .="请选择招聘分类.<br />";
	}
	if(!Core_Fun::ischar($title)){
	    $founderr	= true;
		$errmsg	   .="招聘职位不能为空.<br />";
	}
	if(!Core_Fun::ischar($jobdescription)){
	    $founderr	= true;
		$errmsg	   .="岗位职责不能为空.<br />";
	}
	if(!Core_Fun::ischar($jobrequest)){
	    $founderr	= true;
		$errmsg	   .="职位要求不能为空.<br />";
	}
	if($founderr == true){
	    Core_Fun::halt($errmsg,"",1);
	}
	$array = array(
		'cateid'=>$cateid,
		'title'=>$title,
		'workarea'=>$workarea,
		'number'=>$number,
		'jobdescription'=>$jobdescription,
		'jobrequest'=>$jobrequest,
		'jobotherrequest'=>$jobotherrequest,
		'jobcontact'=>$jobcontact,
		'flag'=>$flag,
	);
	$result = $db->update(DB_PREFIX."job",$array,"jobid=$id");
	if($result){
		Core_Command::runlog("","编辑招聘信息成功[id=$id]");
		Core_Fun::halt("编辑成功","ljcms_job.php?".$GLOBALS['comeurl']."",0);
	}else{
		Core_Fun::halt("编辑失败","",2);
	}
}

function del(){
	Core_Auth::checkauth("jobdel");
	$arrid  = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
	if($arrid=="" || is_null($arrid)){
		Core_Fun::halt("请选择要删除的数据","",1);
	}
	global $db;
	for($ii=0;$ii<count($arrid);$ii++){
        $id = Core_Fun::replacebadchar(trim($arrid[$ii]));
		if(Core_Fun::isnumber($id)){
			$db->query("DELETE FROM ".DB_PREFIX."job WHERE jobid=$id");
		}
	}
	Core_Command::runlog("","删除招聘信息成功[id=$arrid]");
	Core_Fun::halt("删除成功","ljcms_job.php",0);
}

function updateajax($_id,$_action){
	Core_Auth::checkauth("jobedit");
    if(Core_Fun::isnumber($_id)){
		global $db;
		switch($_action){
			case 'flagopen';
			$db->query("UPDATE ".DB_PREFIX."job SET flag=1 WHERE jobid=$_id");
			break;
			case 'flagclose';
			$db->query("UPDATE ".DB_PREFIX."job SET flag=0 WHERE jobid=$_id");
			break;
			default;
			break;
		}
	}
}

$tpl->assign("action",$action);
$tpl->display(ADMIN_TEMPLATE."job.tpl");
$tpl->assign("runtime",runtime());
$tpl->assign("copyright",$libadmin->copyright());
?>