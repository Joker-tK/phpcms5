<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
**/
require_once '../source/core/run.php';
require_once 'admin.inc.php';
$action		= Core_Fun::rec_post("action");
$page		= Core_Fun::detect_number(Core_Fun::rec_post("page"),1);
if($page<1){$page=1;}
$comeurl	= "page=$page";

if(Core_Fun::rec_post('act')=='update'){
    updateajax(Core_Fun::rec_post('id'),Core_Fun::rec_post('action'));
}
switch($action){
    case 'add';
	    add();
		break;
	case 'saveadd';
	    saveadd();
		break;
	case 'edit';
	    edit();
		break;
	case 'saveedit';
	    saveedit();
		break;
	case 'del';
	    del();
		break;
	default;
	    volist();
		break;
}

function volist(){
	Core_Auth::checkauth("onlinechatvolist");
	global $db,$tpl,$page;
    $pagesize	= 30;
	$searchsql	= " WHERE 1=1";
	$countsql	= "SELECT COUNT(onid) FROM ".DB_PREFIX."onlinechat".$searchsql;
    $total		= $db->fetch_count($countsql);
    $pagecount	= ceil($total/$pagesize);
	$nextpage	= $page+1;
	$prepage	= $page-1;
	$start		= ($page-1)*$pagesize;
	$sql		= "SELECT * FROM ".DB_PREFIX."onlinechat".
		         $searchsql." ORDER BY orders ASC LIMIT $start, $pagesize";
	$onlinechat	= $db->getall($sql);
	$url		= $_SERVER['PHP_SELF'];
	$showpage	= Core_Page::adminpage($total,$pagesize,$page,$url,10);
	$tpl->assign("total",$total);
	$tpl->assign("pagecount",$pagecount);
	$tpl->assign("page",$page);
	$tpl->assign("showpage",$showpage);
	$tpl->assign("onlinechat",$onlinechat);
}

function add(){
	Core_Auth::checkauth("onlinechatadd");
	global $tpl,$db;
	$orders = $db->fetch_newid("SELECT MAX(orders) FROM ".DB_PREFIX."onlinechat",1);
	$tpl->assign("flag_checkbox",Core_Mod::checkbox("1","flag","审核"));
	$tpl->assign("orders",$orders);
}

function edit(){
	Core_Auth::checkauth("onlinechatedit");
	global $db,$tpl;
    $id = Core_Fun::rec_post('id');
	if(!Core_Fun::isnumber($id)){
		Core_Fun::halt("ID丢失","",2);
	}
	$sql		= "SELECT * FROM ".DB_PREFIX."onlinechat WHERE onid=$id";
	$onlinechat = $db->fetch_first($sql);
	if(!$onlinechat){
		Core_Fun::halt("数据不存在","",2);
	}else{
		$tpl->assign("flag_checkbox",Core_Mod::checkbox($onlinechat['flag'],"flag","审核"));
		$tpl->assign("id",$id);
		$tpl->assign("comeurl",$GLOBALS['comeurl']);
	    $tpl->assign("onlinechat",$onlinechat);
	}
}

function saveadd(){
	Core_Auth::checkauth("onlinechatadd");
	global $db;
	$ontype		= Core_Fun::detect_number(Core_Fun::rec_post('ontype',1));
	$title		= Core_Fun::rec_post('title',1);
	$number		= Core_Fun::rec_post('number',1);
	$sitetitle	= Core_Fun::rec_post('sitetitle',1);
	$orders		= Core_Fun::detect_number(Core_Fun::rec_post('orders',1));
	$flag		= Core_Fun::detect_number(Core_Fun::rec_post('flag',1));
	$founderr	= false;
	if(intval($ontype)<1){
		$founderr	= true;
		$errmsg	   .="请选择类型.<br />";
	}
	if(!Core_Fun::ischar($number)){
	    $founderr	= true;
		$errmsg	   .="号码不能为空.<br />";
	}
	if($founderr == true){
	    Core_Fun::halt($errmsg,"",1);
	}
	$onid	= $db->fetch_newid("SELECT MAX(onid) FROM ".DB_PREFIX."onlinechat",1);
	$array	= array(
		'onid'=>$onid,
		'ontype'=>$ontype,
		'title'=>$title,
		'number'=>$number,
		'sitetitle'=>$sitetitle,
		'timeline'=>time(),
		'flag'=>$flag,
		'orders'=>$orders,
	);
	$result = $db->insert(DB_PREFIX."onlinechat",$array);
	if($result){
		Core_Command::runlog("","添加客服信息成功",1);
		Core_Fun::halt("保存成功","ljcms_onlinechat.php",0);
	}else{
		Core_Fun::halt("保存失败","",1);
	}
}

function saveedit(){
	Core_Auth::checkauth("onlinechatedit");
	global $db;
	$id			= Core_Fun::rec_post('id',1);
	$ontype		= Core_Fun::detect_number(Core_Fun::rec_post('ontype',1));
	$title		= Core_Fun::rec_post('title',1);
	$number		= Core_Fun::rec_post('number',1);
	$sitetitle	= Core_Fun::rec_post('sitetitle',1);
	$orders		= Core_Fun::detect_number(Core_Fun::rec_post('orders',1));
	$flag		= Core_Fun::detect_number(Core_Fun::rec_post('flag',1));
	$founderr	= false;
	if(!Core_Fun::isnumber($id)){
	    $founderr	= true;
		$errmsg	   .= "ID丢失.<br />";
	}
	if(intval($ontype)<1){
		$founderr	= true;
		$errmsg	   .="请选择类型.<br />";
	}
	if(!Core_Fun::ischar($number)){
	    $founderr	= true;
		$errmsg	   .="号码不能为空.<br />";
	}
	if($founderr == true){
	    Core_Fun::halt($errmsg,"",1);
	}
	$array = array(
		'ontype'=>$ontype,
		'title'=>$title,
		'number'=>$number,
		'sitetitle'=>$sitetitle,
		'flag'=>$flag,
		'orders'=>$orders,
	);
	$result = $db->update(DB_PREFIX."onlinechat",$array,"onid=$id");
	if($result){
		Core_Command::runlog("","编辑客服信息成功[id=$id]");
		Core_Fun::halt("编辑成功","ljcms_onlinechat.php?".$GLOBALS['comeurl']."",0);
	}else{
		Core_Fun::halt("编辑失败","",2);
	}
}

function del(){
	Core_Auth::checkauth("onlinechatdel");
	$arrid  = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
	if($arrid=="" || is_null($arrid)){
		Core_Fun::halt("请选择要删除的数据","",1);
	}
	for($ii=0;$ii<count($arrid);$ii++){
        $id = Core_Fun::replacebadchar(trim($arrid[$ii]));
		if(Core_Fun::isnumber($id)){
			$GLOBALS['db']->query("DELETE FROM ".DB_PREFIX."onlinechat WHERE onid=$id");
		}
	}
	Core_Command::runlog("","删除客服信息成功[id=$arrid]");
	Core_Fun::halt("删除成功","ljcms_onlinechat.php",0);
}

function updateajax($_id,$_action){
	Core_Auth::checkauth("onlinechatedit");
    if(Core_Fun::isnumber($_id)){
		global $db;
		switch($_action){
			case 'flagopen';
			$db->query("UPDATE ".DB_PREFIX."onlinechat SET flag=1 WHERE onid=$_id");
			break;
			case 'flagclose';
			$db->query("UPDATE ".DB_PREFIX."onlinechat SET flag=0 WHERE onid=$_id");
			break;
			default;
			break;
		}
	}
}

$tpl->assign("action",$action);
$tpl->display(ADMIN_TEMPLATE."onlinechat.tpl");
$tpl->assign("runtime",runtime());
$tpl->assign("copyright",$libadmin->copyright());
?>