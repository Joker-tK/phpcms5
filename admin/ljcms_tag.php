<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
**/
require_once '../source/core/run.php';
require_once 'admin.inc.php';
$action		= Core_Fun::rec_post("action");
$page		= Core_Fun::detect_number(Core_Fun::rec_post("page"),1);
$schannel	= Core_Fun::detect_number(Core_Fun::rec_post("schannel"));
$sname      = Core_Fun::rec_post("sname",1);
if($page<1){$page=1;}
$comeurl	= "page=$page&schannel=$schannel&sname=".urlencode($sname)."";

if(Core_Fun::rec_post('act')=='update'){
    updateajax(Core_Fun::rec_post('id'),Core_Fun::rec_post('action'));
}
switch($action){
    case 'add';
	    add();
		break;
	case 'saveadd';
	    saveadd();
		break;
	case 'edit';
	    edit();
		break;
	case 'saveedit';
	    saveedit();
		break;
	case 'del';
	    del();
		break;
	default;
	    volist();
		break;
}

function volist(){
	Core_Auth::checkauth("tagvolist");
	global $db,$tpl,$page,$schannel,$sname;
    $pagesize	= 30;
	$searchsql	= " WHERE 1=1";
	if(Core_Fun::ischar($schannel)){
		$searchsql .= " AND channel='$schannel'";
	}
	if(Core_Fun::ischar($sname)){
		$searchsql .= " AND tag LIKE '%".$sname."%'";
	}
	$countsql	= "SELECT COUNT(tagid) FROM ".DB_PREFIX."tag".$searchsql;
    $total		= $db->fetch_count($countsql);
    $pagecount	= ceil($total/$pagesize);
	$nextpage	= $page+1;
	$prepage	= $page-1;
	$start		= ($page-1)*$pagesize;
	$sql		= "SELECT * FROM ".DB_PREFIX."tag".
		          $searchsql." ORDER BY tagid DESC LIMIT $start, $pagesize";
	$tag		= $db->getall($sql);
	$url		= $_SERVER['PHP_SELF'];
	$urlitem	= "schannel=$schannel&sname=".urlencode($sname)."";
	$url	   .= "?".$urlitem;
	$showpage	= Core_Page::adminpage($total,$pagesize,$page,$url,10);
	$tpl->assign("total",$total);
	$tpl->assign("pagecount",$pagecount);
	$tpl->assign("page",$page);
	$tpl->assign("showpage",$showpage);
	$tpl->assign("tag",$tag);
	$tpl->assign("urlitem",$urlitem);
	$tpl->assign("channel_search",Core_Mod::var_select($schannel,"schannel"));
	$tpl->assign("sname",$sname);
}

function add(){
	Core_Auth::checkauth("tagadd");
	global $tpl;
	$tpl->assign("flag_checkbox",Core_Mod::checkbox("1","flag","审核"));
	$tpl->assign("strong_checkbox",Core_Mod::checkbox("","strong","字体加粗"));
	$tpl->assign("channel_select",Core_Mod::var_select("","channel"));
	$tpl->assign("orders",$orders);
}

function edit(){
	Core_Auth::checkauth("tagedit");
	global $db,$tpl;
    $id = Core_Fun::rec_post('id');
	if(!Core_Fun::isnumber($id)){
		Core_Fun::halt("ID丢失","",2);
	}
	$sql	= "SELECT * FROM ".DB_PREFIX."tag WHERE tagid=$id";
	$tag	= $db->fetch_first($sql);
	if(!$tag){
		Core_Fun::halt("数据不存在","",2);
	}else{
		$tpl->assign("flag_checkbox",Core_Mod::checkbox($tag['flag'],"flag","审核"));
		$tpl->assign("strong_checkbox",Core_Mod::checkbox($tag['strong'],"strong","字体加粗"));
		$tpl->assign("channel_select",Core_Mod::var_select($tag['channel'],"channel"));
		$tpl->assign("id",$id);
		$tpl->assign("comeurl",$GLOBALS['comeurl']);
	    $tpl->assign("tag",$tag);
	}
}

function saveadd(){
	Core_Auth::checkauth("tagadd");
	global $db;
	$channel	= Core_Fun::rec_post('channel',1);
	$tag		= Core_Fun::strip_post('tag',1);
	$url		= Core_Fun::strip_post('url',1);
	$target		= Core_Fun::detect_number(Core_Fun::rec_post('target',1));
	$strong		= Core_Fun::detect_number(Core_Fun::rec_post('strong',1));
	$flag		= Core_Fun::detect_number(Core_Fun::rec_post('flag',1));
	$color		= Core_Fun::rec_post('color',1);
	$founderr	= false;
	if(!Core_Fun::ischar($tag)){
	    $founderr	= true;
		$errmsg	   .="Tag名称不能为空.<br />";
	}
	if($founderr == true){
	    Core_Fun::halt($errmsg,"",1);
	}
	$tagid	= $db->fetch_newid("SELECT MAX(tagid) FROM ".DB_PREFIX."tag",1);
	$array	= array(
		'tagid'=>$tagid,
		'tag'=>$tag,
		'channel'=>$channel,
		'url'=>$url,
		'flag'=>$flag,
		'timeline'=>time(),
		'target'=>$target,
		'color'=>$color,
		'strong'=>$strong,
	);
	$result = $db->insert(DB_PREFIX."tag",$array);
	if($result){
		Core_Command::runlog("","添加TAGS成功[$tag]",1);
		Core_Fun::halt("保存成功","ljcms_tag.php",0);
	}else{
		Core_Fun::halt("保存失败","",1);
	}
}

function saveedit(){
	Core_Auth::checkauth("pageedit");
	global $db;
	$id			= Core_Fun::rec_post('id',1);
	$channel	= Core_Fun::rec_post('channel',1);
	$tag		= Core_Fun::strip_post('tag',1);
	$url		= Core_Fun::strip_post('url',1);
	$target		= Core_Fun::detect_number(Core_Fun::rec_post('target',1));
	$strong		= Core_Fun::detect_number(Core_Fun::rec_post('strong',1));
	$flag		= Core_Fun::detect_number(Core_Fun::rec_post('flag',1));
	$color		= Core_Fun::rec_post('color',1);
	$founderr	= false;
	if(!Core_Fun::isnumber($id)){
	    $founderr	= true;
		$errmsg	   .= "ID丢失.<br />";
	}
	if(!Core_Fun::ischar($tag)){
	    $founderr	= true;
		$errmsg	   .="Tag名称不能为空.<br />";
	}
	if($founderr == true){
	    Core_Fun::halt($errmsg,"",1);
	}
	$array = array(
		'tag'=>$tag,
		'channel'=>$channel,
		'url'=>$url,
		'flag'=>$flag,
		'target'=>$target,
		'color'=>$color,
		'strong'=>$strong,
	);
	$result = $db->update(DB_PREFIX."tag",$array,"tagid=$id");
	if($result){
		Core_Command::runlog("","编辑TAGS成功[id=$id]");
		Core_Fun::halt("编辑成功","ljcms_tag.php?".$GLOBALS['comeurl']."",0);
	}else{
		Core_Fun::halt("编辑失败","",2);
	}
}

function del(){
	Core_Auth::checkauth("tagdel");
	$arrid  = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
	if($arrid=="" || is_null($arrid)){
		Core_Fun::halt("请选择要删除的数据","",1);
	}
	global $db;
	for($ii=0;$ii<count($arrid);$ii++){
        $id = Core_Fun::replacebadchar(trim($arrid[$ii]));
		if(Core_Fun::isnumber($id)){
			$db->query("DELETE FROM ".DB_PREFIX."tag WHERE tagid=$id");
		}
	}
	Core_Command::runlog("","删除TAGS成功[id=$arrid]");
	Core_Fun::halt("删除成功","ljcms_tag.php",0);
}

function updateajax($_id,$_action){
	Core_Auth::checkauth("tagedit");
    if(Core_Fun::isnumber($_id)){
		global $db;
		switch($_action){
			case 'flagopen';
			$db->query("UPDATE ".DB_PREFIX."tag SET flag=1 WHERE tagid=$_id");
			break;
			case 'flagclose';
			$db->query("UPDATE ".DB_PREFIX."tag SET flag=0 WHERE tagid=$_id");
			break;
			default;
			break;
		}
	}
}

$tpl->assign("action",$action);
$tpl->display(ADMIN_TEMPLATE."tag.tpl");
$tpl->assign("runtime",runtime());
$tpl->assign("copyright",$libadmin->copyright());
?>