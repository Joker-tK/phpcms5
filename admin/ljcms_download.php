<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
**/
require_once '../source/core/run.php';
require_once 'admin.inc.php';
$action		= Core_Fun::rec_post("action");
$page		= Core_Fun::detect_number(Core_Fun::rec_post("page"),1);
$scateid    = Core_Fun::detect_number(Core_Fun::rec_post("scateid"));
$sname      = Core_Fun::rec_post("sname",1);
if($page<1){$page=1;}
$comeurl	= "page=$page&scateid=$scateid&sname=".urlencode($sname)."";

if(Core_Fun::rec_post('act')=='update'){
    updateajax(Core_Fun::rec_post('id'),Core_Fun::rec_post('action'));
}
switch($action){
    case 'add';
	    add();
		break;
	case 'saveadd';
	    saveadd();
		break;
	case 'edit';
	    edit();
		break;
	case 'saveedit';
	    saveedit();
		break;
	case 'setting';
	    setting();
		break;
	case 'savesetting';
	    savesetting();
		break;
	case 'del';
	    del();
		break;
	default;
	    volist();
		break;
}

function volist(){
	Core_Auth::checkauth("downloadvolist");
	global $db,$tpl,$page,$scateid,$sname;
    $pagesize	= 30;
	$searchsql	= " WHERE 1=1";
	if($scateid>0){
		$searchsql .= " AND d.cateid=$scateid";
	}
	if(Core_Fun::ischar($sname)){
		$searchsql .= " AND d.title LIKE '%".$sname."%'";
	}
	$countsql	= "SELECT COUNT(d.downid) FROM ".DB_PREFIX."download AS d".$searchsql;
    $total		= $db->fetch_count($countsql);
    $pagecount	= ceil($total/$pagesize);
	$nextpage	= $page+1;
	$prepage	= $page-1;
	$start		= ($page-1)*$pagesize;
	$sql		= "SELECT d.*,c.catename".
		          " FROM ".DB_PREFIX."download AS d".
		          " LEFT JOIN ".DB_PREFIX."downloadcate AS c ON d.cateid=c.cateid".
		          $searchsql." ORDER BY d.downid DESC LIMIT $start, $pagesize";
	$download	= $db->getall($sql);
	$url		= $_SERVER['PHP_SELF'];
	$urlitem	= "scateid=$scateid&sname=".urlencode($sname)."";
	$url	   .= "?".$urlitem;
	$showpage	= Core_Page::adminpage($total,$pagesize,$page,$url,10);
	$tpl->assign("total",$total);
	$tpl->assign("pagecount",$pagecount);
	$tpl->assign("page",$page);
	$tpl->assign("showpage",$showpage);
	$tpl->assign("download",$download);
	$tpl->assign("urlitem",$urlitem);
	$tpl->assign("cate_search",Core_Mod::db_select($scateid,"scateid","downloadcate"));
	$tpl->assign("sname",$sname);
}

function setting(){
	Core_Auth::checkauth("downloadedit");
	global $db,$tpl,$page;
    $pagesize	= 30;
	$searchsql	= " WHERE 1=1";
	$countsql	= "SELECT COUNT(downid) FROM ".DB_PREFIX."download".$searchsql;
    $total		= $db->fetch_count($countsql);
    $pagecount	= ceil($total/$pagesize);
	$nextpage	= $page+1;
	$prepage	= $page-1;
	$start		= ($page-1)*$pagesize;
	$sql		= "SELECT * FROM ".DB_PREFIX."download".
		          $searchsql." ORDER BY downid DESC LIMIT $start, $pagesize";
	$download	= $db->getall($sql);
	$url		= $_SERVER['PHP_SELF'];
	$showpage	= Core_Page::adminpage($total,$pagesize,$page,$url,10);
	$tpl->assign("total",$total);
	$tpl->assign("pagecount",$pagecount);
	$tpl->assign("page",$page);
	$tpl->assign("showpage",$showpage);
	$tpl->assign("download",$download);
}

function add(){
	Core_Auth::checkauth("downloadadd");
	global $tpl;
	$tpl->assign("flag_checkbox",Core_Mod::checkbox("1","flag","审核"));
	$tpl->assign("elite_checkbox",Core_Mod::checkbox("","elite","推荐"));
	$tpl->assign("cate_select",Core_Mod::db_select("","cateid","downloadcate"));
	$tpl->assign("time",time());
}

function edit(){
	Core_Auth::checkauth("downloadedit");
	global $db,$tpl;
    $id = Core_Fun::rec_post('id');
	if(!Core_Fun::isnumber($id)){
		Core_Fun::halt("ID丢失","",2);
	}
	$sql		= "SELECT * FROM ".DB_PREFIX."download WHERE downid=$id";
	$download	= $db->fetch_first($sql);
	if(!$download){
		Core_Fun::halt("数据不存在","",2);
	}else{
		$download['uploadname'] = Core_Mod::getpicname($download['uploadfiles']);
		$tpl->assign("flag_checkbox",Core_Mod::checkbox($download['flag'],"flag","审核"));
		$tpl->assign("elite_checkbox",Core_Mod::checkbox($download['elite'],"elite","推荐"));
		$tpl->assign("cate_select",Core_Mod::db_select($download['cateid'],"cateid","downloadcate"));
		$tpl->assign("ugroupid_select",Core_Mod::db_usergroulive($download['ugroupid'],"groupid","usergroup"));
		$tpl->assign("id",$id);
		$tpl->assign("comeurl",$GLOBALS['comeurl']);
	    $tpl->assign("download",$download);
	}
}

function saveadd(){
	Core_Auth::checkauth("downloadadd");
	global $db;
	$cateid			= Core_Fun::detect_number(Core_Fun::rec_post('cateid',1));
	$title			= Core_Fun::rec_post('title',1);
	$uploadfiles	= Core_Fun::strip_post('uploadfiles',1);
	$barcode	    = Core_Fun::strip_post('barcode',1);
	$filesize		= Core_Fun::strip_post('filesize',1);
	$intro			= Core_Fun::strip_post('intro',1);
	$content		= Core_Fun::strip_post('content',1);
	$downs			= Core_Fun::detect_number(Core_Fun::rec_post('downs',1));
	$flag			= Core_Fun::detect_number(Core_Fun::rec_post('flag',1));
	$groupid		= Core_Fun::strip_post('groupid',1);
	$exclusive		= Core_Fun::strip_post('exclusive',1);
	$elite			= Core_Fun::detect_number(Core_Fun::rec_post('elite',1));
	$metatitle		= Core_Fun::rec_post('metatitle',1);
	$metakeyword	= Core_Fun::rec_post('metakeyword',1);
	$metadescription= Core_Fun::rec_post('metadescription',1);
	$dateline		= Core_Fun::strip_post('dateline',1);
	$founderr		= false;
	if($cateid<1){
	    $founderr	= true;
		$errmsg	   .="请选择分类.<br />";
	}
	if(!Core_Fun::ischar($title)){
	    $founderr	= true;
		$errmsg	   .="下载标题不能为空.<br />";
	}
	if(!Core_Fun::ischar($uploadfiles)){
	    $founderr	= true;
		$errmsg	   .="下载地址不能为空.<br />";
	}
	if($founderr == true){
	    Core_Fun::halt($errmsg,"",1);
	}
	$dateline = strtotime($dateline);

	$downid	= $db->fetch_newid("SELECT MAX(downid) FROM ".DB_PREFIX."download",1);
	$array	= array(
		'downid'=>$downid,
		'cateid'=>$cateid,
		'title'=>$title,
		'filesize'=>$filesize,
		'uploadfiles'=>$uploadfiles,
		'barcode'=>$barcode,
		'intro'=>$intro,
		'content'=>$content,
		'timeline'=>time(),
		'elite'=>$elite,
		'flag'=>$flag,
		'ugroupid'=>$groupid,
		'exclusive'=>$exclusive,
		'metatitle'=>$metatitle,
		'metakeyword'=>$metakeyword,
		'metadescription'=>$metadescription,
		'downs'=>$downs,
		'dateline'=>$dateline,
	);
	$result = $db->insert(DB_PREFIX."download",$array);
	if($result){
		Core_Command::runlog("","发布下载成功[$title]",1);
		Core_Fun::halt("保存成功","ljcms_download.php",0);
	}else{
		Core_Fun::halt("保存失败","",1);
	}
}

function saveedit(){
	Core_Auth::checkauth("downloadedit");
	global $db;
	$id				= Core_Fun::rec_post('id',1);
	$cateid			= Core_Fun::detect_number(Core_Fun::rec_post('cateid',1));
	$title			= Core_Fun::rec_post('title',1);
	$uploadfiles	= Core_Fun::strip_post('uploadfiles',1);
	$barcode	= Core_Fun::strip_post('barcode',1);
	$filesize		= Core_Fun::rec_post('filesize',1);
	$intro			= Core_Fun::strip_post('intro',1);
	$content		= Core_Fun::strip_post('content',1);
	$downs			= Core_Fun::detect_number(Core_Fun::rec_post('downs',1));
	$flag			= Core_Fun::detect_number(Core_Fun::rec_post('flag',1));
	$groupid		= Core_Fun::strip_post('groupid',1);
	$exclusive		= Core_Fun::strip_post('exclusive',1);
	$elite			= Core_Fun::detect_number(Core_Fun::rec_post('elite',1));
	$metatitle		= Core_Fun::rec_post('metatitle',1);
	$metakeyword	= Core_Fun::rec_post('metakeyword',1);
	$metadescription= Core_Fun::rec_post('metadescription',1);
	$dateline		= Core_Fun::strip_post('dateline',1);
	$founderr	= false;
	if(!Core_Fun::isnumber($id)){
	    $founderr	= true;
		$errmsg	   .= "ID丢失.<br />";
	}
	if($cateid<1){
	    $founderr	= true;
		$errmsg	   .="请选择分类.<br />";
	}
	if(!Core_Fun::ischar($title)){
	    $founderr	= true;
		$errmsg	   .="下载标题不能为空.<br />";
	}
	if(!Core_Fun::ischar($uploadfiles)){
	    $founderr	= true;
		$errmsg	   .="下载地址不能为空.<br />";
	}
	if($founderr == true){
	    Core_Fun::halt($errmsg,"",1);
	}
	$dateline = strtotime($dateline);

	$array = array(
		'cateid'=>$cateid,
		'title'=>$title,
		'filesize'=>$filesize,
		'uploadfiles'=>$uploadfiles,
		'barcode'=>$barcode,
		'intro'=>$intro,
		'content'=>$content,
		'elite'=>$elite,
		'flag'=>$flag,
		'ugroupid'=>$groupid,
		'exclusive'=>$exclusive,
		'metatitle'=>$metatitle,
		'metakeyword'=>$metakeyword,
		'metadescription'=>$metadescription,
		'downs'=>$downs,
		'dateline'=>$dateline,
	);
	$result = $db->update(DB_PREFIX."download",$array,"downid=$id");
	if($result){
		Core_Command::runlog("","编辑下载成功[id=$id]");
		Core_Fun::halt("编辑成功","ljcms_download.php?".$GLOBALS['comeurl']."",0);
	}else{
		Core_Fun::halt("编辑失败","",2);
	}
}

function del(){
	Core_Auth::checkauth("downloaddel");
	$arrid  = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
	if($arrid=="" || is_null($arrid)){
		Core_Fun::halt("请选择要删除的数据","",1);
	}
	global $db;
	for($ii=0;$ii<count($arrid);$ii++){
        $id = Core_Fun::replacebadchar(trim($arrid[$ii]));
		if(Core_Fun::isnumber($id)){
			$sql	= "SELECT uploadfiles FROM ".DB_PREFIX."download WHERE downid=$id";
			$rows	= $db->fetch_first($sql);
			if($rows){
				if(Core_Fun::ischar($rows['uploadfiles'])){
					if(strtolower(substr($rows['uploadfiles'],0,4))=='http'){
					}else{
						Core_Fun::deletefile("../".$rows['uploadfiles']);
					}
				}
			}
			$db->query("DELETE FROM ".DB_PREFIX."download WHERE downid=$id");
		}
	}
	Core_Command::runlog("","删除下载成功[id=$arrid]");
	Core_Fun::halt("删除成功","ljcms_download.php",0);
}

function updateajax($_id,$_action){
	Core_Auth::checkauth("downloadedit");
    if(Core_Fun::isnumber($_id)){
		global $db;
		switch($_action){
			case 'flagopen';
			$db->query("UPDATE ".DB_PREFIX."download SET flag=1 WHERE downid=$_id");
			break;
			case 'flagclose';
			$db->query("UPDATE ".DB_PREFIX."download SET flag=0 WHERE downid=$_id");
			break;
			case 'eliteopen';
			$db->query("UPDATE ".DB_PREFIX."download SET elite=1 WHERE downid=$_id");
			break;
			case 'eliteclose';
			$db->query("UPDATE ".DB_PREFIX."download SET elite=0 WHERE downid=$_id");
			break;
			default;
			break;
		}
	}
}

function savesetting(){
	Core_Auth::checkauth("downloadedit");
	$arrid  = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
	if($arrid=="" || is_null($arrid)){
		Core_Fun::halt("请选择要更新的数据","",1);
	}
	global $db;
	for($ii=0;$ii<count($arrid);$ii++){
        $id					= (int)($arrid[$ii]);
		$title				= Core_Fun::rec_post("title_".$id,1);
		$metatitle			= Core_Fun::rec_post("metatitle_".$id,1);
		$metakeyword		= Core_Fun::rec_post("metakeyword_".$id,1);
		$metadescription	= Core_Fun::rec_post("metadescription_".$id,1);
		if(Core_Fun::isnumber($id)){
			$array = array(
				'title'=>$title,
				'metatitle'=>$metatitle,
				'metakeyword'=>$metakeyword,
				'metadescription'=>$metadescription,
			);
			$db->update(DB_PREFIX."download",$array,"downid=$id");
		}
	}
	Core_Fun::halt("批量更新成功","ljcms_download.php?action=setting",0);
}

$tpl->assign("action",$action);
$tpl->display(ADMIN_TEMPLATE."download.tpl");
$tpl->assign("runtime",runtime());
$tpl->assign("copyright",$libadmin->copyright());
?>