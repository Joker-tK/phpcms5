<?php


if (!defined('PHP_KINGFISHER')){
    die('Hacking attempt');
}
$libadmin->checklogin();
$tpl->assign("uc_adminname",$libadmin->uc_adminname);
$tpl->assign("uc_super",$libadmin->uc_super);
$tpl->assign("uc_groupname",$libadmin->uc_groupname);

$tpl->assign("info_count",Core_Mod::infoCount());
$tpl->assign("article_count",Core_Mod::articleCount());
$tpl->assign("product_count",Core_Mod::productCount());
$tpl->assign("case_count",Core_Mod::caseCount());
$tpl->assign("solution_count",Core_Mod::solutionCount());
$tpl->assign("download_count",Core_Mod::downloadCount());
$tpl->assign("guestbook_count",Core_Mod::guestbookCount());
$tpl->assign("user_count",Core_Mod::userCount());
$tpl->assign("order_count",Core_Mod::proorderCount());
$tpl->assign("myversion",PHP_VERSION);
$tpl->assign("serverversion",$_SERVER['SERVER_SOFTWARE']);


echo $_SERVER['SERVER_SIGNATURE'] ;

function runtime(){
	echo("<div style='clear:both'></div><div align='center' style='overflow:hidden;'><font color='#999999'>Processed in ".Core_Timer::display()." second(s) , ".$GLOBALS['db']->querynum." queries<br /></font></div>");
}
?>