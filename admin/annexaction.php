<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
**/
require_once '../source/core/run.php';
require_once 'admin.inc.php';
echo"<meta http-equiv='Content-Type' content='text/html;charset=".LJCMS_CHARSET."' />";
$forbidtype		= "asp|aspx|asax|asa|jsp|cer|cdx|asa|htr|php|php3|cgi|html|htm|shtml";
$max_file_size	= 20000000;
$comeform		= Core_Fun::rec_post('comeform');
$inputname		= Core_Fun::rec_post('inputname');

$attachmentdir = "data/download/";
$yfolder = date('Y').date('m')."/";
if(!file_exists("../".$attachmentdir.$yfolder)){
	mkdir("../".$attachmentdir.$yfolder);
}
$dfolder = date('d')."/";
if(!file_exists("../".$attachmentdir.$yfolder.$dfolder)){
	mkdir("../".$attachmentdir.$yfolder.$dfolder);
}
$picfolder	= $yfolder.$dfolder;
$filepath	= $attachmentdir.$picfolder;
if(!file_exists("../".$filepath)){
	mkdir("../".$filepath);
}

if ($_SERVER['REQUEST_METHOD'] == 'POST'){
    if (!is_uploaded_file($_FILES["upfile"]['tmp_name'])){
		echo "<font color='red'>文件不存在！</font>";
		exit;
	}
    $file		= $_FILES["upfile"];
	$file_size	= $file["size"];
    if($max_file_size < $file_size){
		echo "<font color='red'>文件太大了！超过了20M.</font>";
		exit;
	}
    $tmpname		= $file["tmp_name"];
    $image_size		= getimagesize($tmpname); 
    $pinfo			= pathinfo($file["name"]);
    $file_type		= $pinfo['extension'];
	if(Core_Fun::foundinarr($forbidtype,strtolower($file_type),"|")){
		echo "<font color='red'>禁止上传".$forbidtype."类型文件.</font>";
		die();
	}
	$md5name		= md5(time());
	$newfilename	= $md5name.".".$file_type;
	$uploadfiles	= $filepath.$newfilename;
    $fulluploadfiles	= "../".$uploadfiles;
	if (file_exists($fulluploadfiles)){
		echo "<font color='red'>同名文件已经存在了！</font>";
		exit;
	}
	if(!move_uploaded_file ($tmpname, $fulluploadfiles)){
		echo "<font color='red'>文件上传失败！</a>";
		exit;
	}
    if($comeform!=""){
		echo("<script language='javascript'>window.opener.document.".$comeform.".".$inputname.".value='".$uploadfiles."';</script>");
		echo("<script language=\"javascript\">window.close();</script>");
	}else{
		openner("");
	}
}
?>