<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
**/
require_once '../source/core/run.php';
require_once 'admin.inc.php';
$action		= Core_Fun::rec_post("action");
$page		= Core_Fun::detect_number(Core_Fun::rec_post("page"),1);
$scateid    = Core_Fun::detect_number(Core_Fun::rec_post("scateid"));
$sname      = Core_Fun::rec_post("sname",1);
if($page<1){$page=1;}
$comeurl	= "page=$page&scateid=$scateid&sname=".urlencode($sname)."";

if(Core_Fun::rec_post('act')=='update'){
    updateajax(Core_Fun::rec_post('id'),Core_Fun::rec_post('action'));
}
switch($action){
    case 'add';
	    add();
		break;
	case 'saveadd';
	    saveadd();
		break;
	case 'edit';
	    edit();
		break;
	case 'saveedit';
	    saveedit();
		break;
	case 'setting';
	    setting();
		break;
	case 'savesetting';
	    savesetting();
		break;
	case 'del';
	    del();
		break;
	default;
	    volist();
		break;
}

function volist(){
	Core_Auth::checkauth("productvolist");
	global $db,$tpl,$page,$scateid,$sname;
    $pagesize	= 30;
	$searchsql	= " WHERE 1=1";
	if($scateid>0){
		$childs_sql = Core_Mod::build_childsql("productcate","a",$scateid,"");
		if(Core_Fun::ischar($childs_sql)){
			$searchsql .= " AND (a.cateid=$scateid".$childs_sql.")";
		}else{
			$searchsql .= " AND a.cateid=$scateid";
		}
	}
	if(Core_Fun::ischar($sname)){
		$searchsql .= " AND a.productname LIKE '%".$sname."%'";
	}
	$countsql	= "SELECT COUNT(a.productid) FROM ".DB_PREFIX."product AS a".$searchsql;
    $total		= $db->fetch_count($countsql);
    $pagecount	= ceil($total/$pagesize);
	$nextpage	= $page+1;
	$prepage	= $page-1;
	$start		= ($page-1)*$pagesize;
	$sql		= "SELECT a.*,c.catename".
		          " FROM ".DB_PREFIX."product AS a".
		          " LEFT JOIN ".DB_PREFIX."productcate AS c ON a.cateid=c.cateid".
		          $searchsql." ORDER BY a.productid DESC LIMIT $start, $pagesize";
	$product	= $db->getall($sql);
	$url		= $_SERVER['PHP_SELF'];
	$urlitem	= "scateid=$scateid&sname=".urlencode($sname)."";
	$url	   .= "?".$urlitem;
	$showpage	= Core_Page::adminpage($total,$pagesize,$page,$url,10);
	$tpl->assign("total",$total);
	$tpl->assign("pagecount",$pagecount);
	$tpl->assign("page",$page);
	$tpl->assign("showpage",$showpage);
	$tpl->assign("product",$product);
	$tpl->assign("urlitem",$urlitem);
	$tpl->assign("cate_search",Core_Mod::tree_select("productcate",$scateid,"scateid"));
	$tpl->assign("sname",$sname);
}

function setting(){
	Core_Auth::checkauth("productedit");
	global $db,$tpl,$page;
    $pagesize	= 30;
	$searchsql	= " WHERE 1=1";
	$countsql	= "SELECT COUNT(productid) FROM ".DB_PREFIX."product".$searchsql;
    $total		= $db->fetch_count($countsql);
    $pagecount	= ceil($total/$pagesize);
	$nextpage	= $page+1;
	$prepage	= $page-1;
	$start		= ($page-1)*$pagesize;
	$sql		= "SELECT * FROM ".DB_PREFIX."product".
		          $searchsql." ORDER BY productid DESC LIMIT $start, $pagesize";
	$product	= $db->getall($sql);
	$url		= $_SERVER['PHP_SELF'];
	$showpage	= Core_Page::adminpage($total,$pagesize,$page,$url,10);
	$tpl->assign("total",$total);
	$tpl->assign("pagecount",$pagecount);
	$tpl->assign("page",$page);
	$tpl->assign("showpage",$showpage);
	$tpl->assign("product",$product);
}

function add(){
	Core_Auth::checkauth("productadd");
	global $tpl;
	$tpl->assign("flag_checkbox",Core_Mod::checkbox("1","flag","审核"));
	$tpl->assign("elite_checkbox",Core_Mod::checkbox("","elite","推荐"));
	$tpl->assign("isnew_checkbox",Core_Mod::checkbox("","isnew","新品"));
	$tpl->assign("hots_checkbox",Core_Mod::checkbox("","hots","热门"));
	$tpl->assign("ismiao_checkbox",Core_Mod::checkbox("","ismiao","秒杀"));
	$tpl->assign("cate_select",Core_Mod::tree_select("productcate","","cateid"));
}

function edit(){
	Core_Auth::checkauth("productedit");
	global $db,$tpl;
    $id = Core_Fun::rec_post('id');
	if(!Core_Fun::isnumber($id)){
		Core_Fun::halt("ID丢失","",2);
	}
	$sql		= "SELECT * FROM ".DB_PREFIX."product WHERE productid=$id";
	$product	= $db->fetch_first($sql);
	if(!$product){
		Core_Fun::halt("数据不存在","",2);
	}else{
		$product['uploadpicname'] = Core_Mod::getpicname($product['uploadfiles']);
		$product['thumbpicname'] = Core_Mod::getpicname($product['thumbfiles']);
		$tpl->assign("flag_checkbox",Core_Mod::checkbox($product['flag'],"flag","审核"));
		$tpl->assign("elite_checkbox",Core_Mod::checkbox($product['elite'],"elite","推荐"));
		$tpl->assign("isnew_checkbox",Core_Mod::checkbox($product['isnew'],"isnew","新品"));
		$tpl->assign("hots_checkbox",Core_Mod::checkbox($product['hots'],"hots","热门"));
	    $tpl->assign("ismiao_checkbox",Core_Mod::checkbox($product['ismiao'],"ismiao","秒杀"));
		$tpl->assign("cate_select",Core_Mod::tree_select("productcate",$product['cateid'],"cateid"));
		$tpl->assign("ugroupid_select",Core_Mod::db_usergroulive($product['ugroupid'],"groupid","usergroup"));
		$tpl->assign("id",$id);
		$tpl->assign("comeurl",$GLOBALS['comeurl']);
	    $tpl->assign("product",$product);
	}
}

function saveadd(){
	Core_Auth::checkauth("productadd");
	global $db;
	$cateid			= Core_Fun::detect_number(Core_Fun::rec_post('cateid',1));
	$productnum		= Core_Fun::rec_post('productnum',1);
	$productname	= Core_Fun::rec_post('productname',1);
	$thumbfiles		= Core_Fun::strip_post('thumbfiles',1);
	$uploadfiles	= Core_Fun::strip_post('uploadfiles',1);
	$listpicurl		= Core_Fun::strip_post('listpicurl',1);
	$lsitthumburl	= Core_Fun::strip_post('lsitthumburl',1);
	$img1			= Core_Fun::strip_post('img1',1);
	$img1thumb			= Core_Fun::strip_post('img1thumb',1);
	$img2thumb			= Core_Fun::strip_post('img2thumb',1);
	$img2			= Core_Fun::strip_post('img2',1);
	$img3			= Core_Fun::strip_post('img3',1);
	$img4			= Core_Fun::strip_post('img4',1);
	$img5			= Core_Fun::strip_post('img5',1);
	$img6			= Core_Fun::strip_post('img6',1);
	$img7			= Core_Fun::strip_post('img7',1);
	$img8			= Core_Fun::strip_post('img8',1);
	$intro			= Core_Fun::strip_post('intro',1);
	$content		= Core_Fun::strip_post('content',1);
	$nprice			= Core_Fun::detect_number(Core_Fun::rec_post('nprice',1));
	$price			= Core_Fun::detect_number(Core_Fun::rec_post('price',1));
	$hits			= Core_Fun::detect_number(Core_Fun::rec_post('hits',1));
	$flag			= Core_Fun::detect_number(Core_Fun::rec_post('flag',1));
	$groupid		= Core_Fun::strip_post('groupid',1);
	$exclusive		= Core_Fun::strip_post('exclusive',1);
	$elite			= Core_Fun::detect_number(Core_Fun::rec_post('elite',1));
	$isnew			= Core_Fun::detect_number(Core_Fun::rec_post('isnew',1));
	$hots			= Core_Fun::detect_number(Core_Fun::rec_post('hots',1));
	$ismiao			= Core_Fun::detect_number(Core_Fun::rec_post('ismiao',1));
    $miaotime       = Core_Fun::rec_post('miaotime',1);
    $delimitname	= Core_Fun::rec_post('delimitname',1);
	$metatitle		= Core_Fun::rec_post('metatitle',1);
	$metakeyword	= Core_Fun::rec_post('metakeyword',1);
	$metadescription= Core_Fun::rec_post('metadescription',1);
	$tag			= Core_Fun::rec_post('tag',1);
	$founderr		= false;
	if(!Core_Fun::ischar($productname)){
	    $founderr	= true;
		$errmsg	   .="产品名称不能为空.<br />";
	}
	if(!Core_Fun::ischar($content)){
	    $founderr	= true;
		$errmsg	   .="产品详细介绍不能为空.<br />";
	}

if($ismiao==1)
{
if (!Core_Fun::ischar($miaotime))
{
	    $founderr	= true;
		$errmsg	   .="你选择了秒杀请填写截至时间.<br />";
}

}


	if($founderr == true){
	    Core_Fun::halt($errmsg,"",1);
	}
	if(Core_Fun::ischar($thumbfiles)){
		if(!file_exists("../".$thumbfiles)){
			$thumbfiles = $uploadfiles;
		}
	}
	if(!Core_Fun::ischar($productnum)){
		$productnum = time();
	}
	$productid	= $db->fetch_newid("SELECT MAX(productid) FROM ".DB_PREFIX."product",1);
	$array	= array(
		'productid'=>$productid,
		'cateid'=>$cateid,
		'productnum'=>$productnum,
		'productname'=>$productname,
		'thumbfiles'=>$thumbfiles,
		'uploadfiles'=>$uploadfiles,
		'listpicurl'=>$listpicurl,		
		'lsitthumburl'=>$lsitthumburl,
		'img1'=>$img1,
		'img1thumb'=>$img1thumb,
		'img2'=>$img2,
		'img3'=>$img3,
		'img4'=>$img4,
		'img5'=>$img5,
		'img6'=>$img6,
		'img7'=>$img7,
		'img8'=>$img8,
		'intro'=>$intro,
		'content'=>$content,
		'nprice'=>$nprice,
		'price'=>$price,
		'timeline'=>time(),
		'elite'=>$elite,
		'flag'=>$flag,
		'ismiao'=>$ismiao,
		'miaotime'=>$miaotime,
		'ugroupid'=>$groupid,
		'exclusive'=>$exclusive,
		'isnew'=>$isnew,
		'hots'=>$hots,
		'metatitle'=>$metatitle,
		'metakeyword'=>$metakeyword,
		'metadescription'=>$metadescription,
		'delimitname'=>$delimitname,
		'tag'=>$tag,
		'hits'=>$hits,
	);
/*	echo '<pre>';
	print_r($array);
	die;*/
	$result = $db->insert(DB_PREFIX."product",$array);
	if($result){
		Core_Command::command_savetag("product",$tag);
		Core_Command::runlog("","发布产品成功[$productname]",1);
		Core_Fun::halt("保存成功","ljcms_product.php",0);
	}else{
		Core_Fun::halt("保存失败","",1);
	}
}

function saveedit(){
	Core_Auth::checkauth("productedit");
	global $db;
	$id				= Core_Fun::rec_post('id',1);
	$cateid			= Core_Fun::detect_number(Core_Fun::rec_post('cateid',1));
	$productnum		= Core_Fun::rec_post('productnum',1);
	$productname	= Core_Fun::rec_post('productname',1);
	$thumbfiles		= Core_Fun::strip_post('thumbfiles',1);
	$uploadfiles	= Core_Fun::strip_post('uploadfiles',1);
	$listpicurl	= Core_Fun::strip_post('listpicurl',1);
	$lsitthumburl	= Core_Fun::strip_post('lsitthumburl',1);
	$img1			= Core_Fun::strip_post('img1',1);
	$img2			= Core_Fun::strip_post('img2',1);
	$img3			= Core_Fun::strip_post('img3',1);
	$img4			= Core_Fun::strip_post('img4',1);
	$img5			= Core_Fun::strip_post('img5',1);
	$img6			= Core_Fun::strip_post('img6',1);
	$img7			= Core_Fun::strip_post('img7',1);
	$img8			= Core_Fun::strip_post('img8',1);
	$intro			= Core_Fun::strip_post('intro',1);
	$content		= Core_Fun::strip_post('content',1);
	$nprice			= Core_Fun::detect_number(Core_Fun::rec_post('nprice',1));
	$price			= Core_Fun::detect_number(Core_Fun::rec_post('price',1));
	$hits			= Core_Fun::detect_number(Core_Fun::rec_post('hits',1));
	$flag			= Core_Fun::detect_number(Core_Fun::rec_post('flag',1));
	$groupid		= Core_Fun::strip_post('groupid',1);
	$exclusive		= Core_Fun::strip_post('exclusive',1);
	$elite			= Core_Fun::detect_number(Core_Fun::rec_post('elite',1));
	$isnew			= Core_Fun::detect_number(Core_Fun::rec_post('isnew',1));
	$hots			= Core_Fun::detect_number(Core_Fun::rec_post('hots',1));
	$ismiao			= Core_Fun::detect_number(Core_Fun::rec_post('ismiao',1));
	$miaotime       = Core_Fun::rec_post('miaotime',1);
    $delimitname	= Core_Fun::rec_post('delimitname',1);
	$metatitle		= Core_Fun::rec_post('metatitle',1);
	$metakeyword	= Core_Fun::rec_post('metakeyword',1);
	$metadescription= Core_Fun::rec_post('metadescription',1);
	$tag			= Core_Fun::rec_post('tag',1);
	$founderr	= false;
	if(!Core_Fun::isnumber($id)){
	    $founderr	= true;
		$errmsg	   .= "ID丢失.<br />";
	}
	if(!Core_Fun::ischar($productname)){
	    $founderr	= true;
		$errmsg	   .="产品名称不能为空.<br />";
	}
	if(!Core_Fun::ischar($content)){
	    $founderr	= true;
		$errmsg	   .="产品详细介绍不能为空.<br />";
	}

    if($ismiao==1)
   {
     if (!Core_Fun::ischar($miaotime))
      {
	    $founderr	= true;
		$errmsg	   .="你选择了秒杀请填写截至时间.<br />";
       }
    }

	if($founderr == true){
	    Core_Fun::halt($errmsg,"",1);
	}
	if(Core_Fun::ischar($thumbfiles)){
		if(!file_exists("../".$thumbfiles)){
			$thumbfiles = $uploadfiles;
		}
	}
	if(!Core_Fun::ischar($productnum)){
		$productnum = time();
	}
	$array = array(
		'cateid'=>$cateid,
		'productnum'=>$productnum,
		'productname'=>$productname,
		'thumbfiles'=>$thumbfiles,
		'uploadfiles'=>$uploadfiles,
		'listpicurl'=>$listpicurl,
		'lsitthumburl'=>$lsitthumburl,
		'img1'=>$img1,
		'img1thumb'=>$img1thub,
		'img2'=>$img2,
		'img3'=>$img3,
		'img4'=>$img4,
		'img5'=>$img5,
		'img6'=>$img6,
		'img7'=>$img7,
		'img8'=>$img8,
		'intro'=>$intro,
		'content'=>$content,
		'nprice'=>$nprice,
		'price'=>$price,
		'elite'=>$elite,
		'flag'=>$flag,
		'ugroupid'=>$groupid,
		'exclusive'=>$exclusive,
		'isnew'=>$isnew,
		'hots'=>$hots,
		'ismiao'=>$ismiao,
	    'miaotime'=>$miaotime,
		'metatitle'=>$metatitle,
		'metakeyword'=>$metakeyword,
		'metadescription'=>$metadescription,
		'delimitname'=>$delimitname,
		'tag'=>$tag,
		'hits'=>$hits,
	);
	/*echo '<pre>';
	pprint_r($array);
	die;*/
	$result = $db->update(DB_PREFIX."product",$array,"productid=$id");
	if($result){
		Core_Command::command_savetag("product",$tag);
		Core_Command::runlog("","编辑产品成功[id=$id]");
		Core_Fun::halt("编辑成功","ljcms_product.php?".$GLOBALS['comeurl']."",0);
	}else{
		Core_Fun::halt("编辑失败","",2);
	}
}

function del(){
	Core_Auth::checkauth("productdel");
	$arrid  = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
	if($arrid=="" || is_null($arrid)){
		Core_Fun::halt("请选择要删除的数据","",1);
	}
	global $db;
	for($ii=0;$ii<count($arrid);$ii++){
        $id = Core_Fun::replacebadchar(trim($arrid[$ii]));
		if(Core_Fun::isnumber($id)){
			$sql	= "SELECT thumbfiles,uploadfiles,img1,img2,img3,img4,img5,img6,img7,img8".
			         " FROM ".DB_PREFIX."product WHERE productid=$id";
			$rows	= $db->fetch_first($sql);
			if($rows){
				if(Core_Fun::ischar($rows['thumbfiles'])){
					Core_Fun::deletefile("../".$rows['thumbfiles']);
				}
				if(Core_Fun::ischar($rows['uploadfiles'])){
					Core_Fun::deletefile("../".$rows['uploadfiles']);
				}
				if(Core_Fun::ischar($rows['img1'])){
					Core_Fun::deletefile("../".$rows['img1']);
				}
				if(Core_Fun::ischar($rows['img2'])){
					Core_Fun::deletefile("../".$rows['img2']);
				}
				if(Core_Fun::ischar($rows['img3'])){
					Core_Fun::deletefile("../".$rows['img3']);
				}
				if(Core_Fun::ischar($rows['img4'])){
					Core_Fun::deletefile("../".$rows['img4']);
				}
				if(Core_Fun::ischar($rows['img5'])){
					Core_Fun::deletefile("../".$rows['img5']);
				}
				if(Core_Fun::ischar($rows['img6'])){
					Core_Fun::deletefile("../".$rows['img6']);
				}
				if(Core_Fun::ischar($rows['img7'])){
					Core_Fun::deletefile("../".$rows['img7']);
				}
				if(Core_Fun::ischar($rows['img8'])){
					Core_Fun::deletefile("../".$rows['img8']);
				}
			}
			$db->query("DELETE FROM ".DB_PREFIX."product WHERE productid=$id");
		}
	}
	Core_Command::runlog("","删除产品成功[id=$arrid]");
	Core_Fun::halt("删除成功","ljcms_product.php",0);
}

function updateajax($_id,$_action){
	Core_Auth::checkauth("productedit");
    if(Core_Fun::isnumber($_id)){
		global $db;
		switch($_action){
			case 'flagopen';
			$db->query("UPDATE ".DB_PREFIX."product SET flag=1 WHERE productid=$_id");
			break;
			case 'flagclose';
			$db->query("UPDATE ".DB_PREFIX."product SET flag=0 WHERE productid=$_id");
			break;
			case 'eliteopen';
			$db->query("UPDATE ".DB_PREFIX."product SET elite=1 WHERE productid=$_id");
			break;
			case 'eliteclose';
			$db->query("UPDATE ".DB_PREFIX."product SET elite=0 WHERE productid=$_id");
			break;
			default;
			break;
		}
	}
}

function savesetting(){
	Core_Auth::checkauth("productedit");
	$arrid  = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
	if($arrid=="" || is_null($arrid)){
		Core_Fun::halt("请选择要更新的数据","",1);
	}
	global $db;
	for($ii=0;$ii<count($arrid);$ii++){
        $id					= (int)($arrid[$ii]);
		$productname		= Core_Fun::rec_post("productname_".$id,1);
		$metatitle			= Core_Fun::rec_post("metatitle_".$id,1);
		$metakeyword		= Core_Fun::rec_post("metakeyword_".$id,1);
		$metadescription	= Core_Fun::rec_post("metadescription_".$id,1);
		if(Core_Fun::isnumber($id)){
			$array = array(
				'productname'=>$productname,
				'metatitle'=>$metatitle,
				'metakeyword'=>$metakeyword,
				'metadescription'=>$metadescription,
			);
			$db->update(DB_PREFIX."product",$array,"productid=$id");
		}
	}
	Core_Fun::halt("批量更新成功","ljcms_product.php?action=setting",0);
}

$tpl->assign("action",$action);
$tpl->display(ADMIN_TEMPLATE."product.tpl");
$tpl->assign("runtime",runtime());
$tpl->assign("copyright",$libadmin->copyright());
?>