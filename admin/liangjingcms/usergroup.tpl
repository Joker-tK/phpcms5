<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title>会员组组别</title>
<meta name="author" content="<!--{$copyright_author}-->" />
<link type="text/css" rel="stylesheet" href="liangjingcms/css/admin_style.css" />
<script type='text/javascript' src='js/jquery-1.4.4.min.js'></script>
<script type='text/javascript' src='js/command.js'></script>
<script type="text/javascript" src='../data/editor/kindeditor.js'></script>
</head>
<body>
<!--{if $action eq ""}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：系统设置<span>&gt;&gt;</span>会员级别设置</p></div>
  <div class="main-cont">
    <h3 class="title"><a href="ljcms_usergroup.php?action=add" class="btn-general"><span>添加会员级别</span></a>管理会员级别</h3>
	<form action="ljcms_usergroup.php" method="post" name="myform" id="myform" style="margin:0">
	<input type="hidden" name="action" id="action" value="del" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <thead class="tb-tit-bg">
	  <tr>
	    <th width="10%"><div class="th-gap-u">编号</div></th>
		<th width="15%"><div class="th-gap-u">组名</div></th>
		<th width="8%"><div class="th-gap-u">状态</div></th>
		<th width="18%"><div class="th-gap-u">录入时间</div></th>
		<th><div class="th-gap-u">备注说明</div></th>
		<th width="15%"><div class="th-gap-u">操作</div></th>
	  </tr>
	  </thead>
	  <tfoot class="tb-foot-bg"></tfoot>
	  <!--{foreach $usergroup as $volist}-->

	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">

	    <td align="center"><input name="id[]" type="checkbox" value="<!--{$volist.usergroupid}-->" onClick="checkItem(this, 'chkAll')"></td>
		<td><!--{$volist.grupname}--></td>
		<td align="center">
		<!--{if $volist.flag==0}-->
			<input type="hidden" id="attr_flag<!--{$volist.usergroupid}-->" value="flagopen" />
			<img id="flag<!--{$volist.usergroupid}-->" src="liangjingcms/images/no.gif" onClick="javascript:fetch_ajax('flag','<!--{$volist.usergroupid}-->');" style="cursor:pointer;">
		<!--{else}-->
			<input type="hidden" id="attr_flag<!--{$volist.usergroupid}-->" value="flagclose" />
			<img id="flag<!--{$volist.usergroupid}-->" src="liangjingcms/images/yes.gif" onClick="javascript:fetch_ajax('flag','<!--{$volist.usergroupid}-->');" style="cursor:pointer;">	
		<!--{/if}-->
		</td>
		<td><!--{$volist.addtime|date_format:"%Y/%m/%d %H:%M:%S"}--></td>
		<td align="left"><!--{$volist.intro}--></td>
		<td align="center"><a href="ljcms_usergroup.php?action=edit&id=<!--{$volist.usergroupid}-->&page=<!--{$page}-->" class="icon-set">设置</a>&nbsp;&nbsp;<a href="ljcms_usergroup.php?action=del&id[]=<!--{$volist.usergroupid}-->" onClick="{if(confirm('确定要删除该信息?')){return true;} return false;}" class="icon-del">删除</a></td>
	  </tr>
	  <!--{foreachelse}-->
      <tr>
	<td colspan="6" align="center">暂无信息</td>
      </tr>
	  <!--{/foreach}-->
	  <!--{if $total>0}-->
	  <tr>
		<td align="center"><input name="chkAll" type="checkbox" id="chkAll" onClick="checkAll(this, 'id[]')" value="checkbox"></td>
		<td class="hback" colspan="5"><input class="button" name="btn_del" type="button" value="删 除" onClick="{if(confirm('确定删除选定信息吗!?')){$('#action').val('del');$('#myform').submit();return true;}return false;}" class="button">&nbsp;&nbsp;共[ <b><!--{$total}--></b> ]条记录</td>
	  </tr>
	  <!--{/if}-->
	</table>
	</form>
	<!--{if $pagecount>1}-->
	<table width='95%' border='0' cellspacing='0' cellpadding='0' align='center' style="margin-top:10px;">
	  <tr>
		<td align='center'><!--{$showpage}--></td>
	  </tr>
	</table>
	<!--{/if}-->
  </div>
</div>
<!--{/if}-->




<!--{if $action eq "add"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：系统设置<span>&gt;&gt;</span>添加会员级别</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_usergroup.php" class="btn-general"><span>返回列表</span></a>添加会员级别</h3>
    <form name="myform" id="myform" method="post" action="ljcms_usergroup.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveadd" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">级别名称：<span class='f_red'>*</span></td>
		<td class='hback' width="85%"><input type="text" name="grupname" id="grupname" class="input-txt" /> <span class='f_red' id="dgroupname"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>级别号：<span class='f_red'>*</span></td>
		<td class='hback'><input type="text" name="lives" id="lives" class="input-txt" value="<!--{$lives}-->" /></td>
	  </tr>

	  <tr>
		<td class='hback_1'>权限值：<span class='f_red'>*</span></td>
		<td class='hback'><input type="text" name="gpurview" id="gpurview" class="input-txt" value="" /></td>
	  </tr>

	  <tr>
		<td class='hback_1'>备注说明： </td>
		<td class='hback'><textarea name="intro" id='intro' style='width:60%;height:65px;overflow:auto;color:#444444;'></textarea></td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="添加保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<!--{/if}-->

<!--{if $action eq "edit"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：系统设置<span>&gt;&gt;</span>编辑会员级别</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_usergroup.php?<!--{$comeurl}-->" class="btn-general"><span>返回列表</span></a>编辑管理组</h3>
    <form name="myform" id="myform" method="post" action="ljcms_usergroup.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveedit" />
	<input type="hidden" name="id" value="<!--{$id}-->" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">组名：<span class='f_red'>*</span></td>
		<td class='hback' width="85%"><input type="text" name="groupname" id="groupname" class="input-txt" value="<!--{$usergroup.grupname}-->" /> <span class='f_red' id="dgroupname"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>级别号：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="lives" id="lives" class="input-txt" value="<!--{$usergroup.level}-->" /></td>
	  </tr>
	  <tr>
		<td class='hback_1'>权限值：<span class='f_red'>*</span></td>
		<td class='hback'><input type="text" name="gpurview" id="gpurview" class="input-txt" value="<!--{$usergroup.gpurview}-->" /></td>
	  </tr>
	  <tr>
		<td class='hback_1'>备注说明： </td>
		<td class='hback'><textarea name="intro" id='intro' style='width:60%;height:65px;overflow:auto;color:#444444;'><!--{$usergroup.intro}--></textarea></td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="更新保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<!--{/if}-->

</body>
</html>
<script type="text/javascript">
function checkform() {
	var t = "";
	var v = "";

	t = "groupname";
	v = $("#"+t).val();
	if(v=="") {
		dmsg("管理组名不能为空！", t);
		return false;
	}
	return true;
}
</script>
