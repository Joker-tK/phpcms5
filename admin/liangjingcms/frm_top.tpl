<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title>top</title>
<link rel="stylesheet" type="text/css" href="liangjingcms/css/top.css" />
<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="js/top.js"></script>
<!--[if lte IE 6]>
<script type='text/javascript' src='js/DD_belatedPNG-min.js'></script>
<script language="javascript">DD_belatedPNG.fix('.logo');</script>
<![endif]-->
</head>
<body>
<div id="top">
  <div class="logo"></div>
  <div id="navs">
	<ul>
	  <li><a href="frm_left.php?mod=index">管理首页</a></li>
	  <li><a href="frm_left.php?mod=system">系统设置</a></li>
	  <li><a href="frm_left.php?mod=content">内容管理</a></li>
	  <li><a href="frm_left.php?mod=product">产品管理</a></li>
	  <li><a href="frm_left.php?mod=skin">模版风格</a></li>
	  <li><a href="frm_left.php?mod=seo">优化管理</a></li>
	  <li><a href="frm_left.php?mod=other">其他扩展</a></li>
	</ul>  
  </div>
  <div id="right">
    <p>欢迎回来：<!--{$uc_adminname}-->&nbsp;&nbsp;<font color="#999999">|</font>&nbsp;&nbsp;<a href="../" target="_blank">网站首页</a>&nbsp;&nbsp;<font color="#999999">|</font>&nbsp;&nbsp;<a href="logout.php" target="_top">退出登录</a></p>
  </div>
  <div style="clear:both;"></div>
</div>
</body>
</html>