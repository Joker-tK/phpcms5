<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title>在线客服</title>
<meta name="author" content="<!--{$copyright_author}-->" />
<link type="text/css" rel="stylesheet" href="liangjingcms/css/admin_style.css" media="screen" />
<script type='text/javascript' src='js/jquery-1.4.4.min.js'></script>
<script type='text/javascript' src='js/command.js'></script>
<script type="text/javascript" src='../data/editor/kindeditor.js'></script>
</head>
<body>
<table class="tableBorder" width="95%" border="0" align="center" cellpadding="5" cellspacing="1">
<tr>
<td width="100%" class="leftrow">
<!--{if $action eq ""}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：系统设置<span>&gt;&gt;</span>在线客服</p></div>
  <div class="main-cont">
    <h3 class="title"><a href="ljcms_onlinechat.php?action=add" class="btn-general"><span>添加客服</span></a>在线客服</h3>
	<form action="ljcms_onlinechat.php" method="post" name="myform" id="myform" style="margin:0">
	<input type="hidden" name="action" id="action" value="del" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <thead class="tb-tit-bg">
	  <tr>
	    <th width="8%"><div class="th-gap">选择</div></th>
		<th width="10%"><div class="th-gap">类型</div></th>
		<th width="15%"><div class="th-gap">帐号</div></th>
		<th width="12%"><div class="th-gap">文字说明</div></th>
		<th width="12%"><div class="th-gap">网站名称</div></th>
		<th width="8%"><div class="th-gap">排序</div></th>
		<th width="8%"><div class="th-gap">状态</div></th>
		<th width="12%"><div class="th-gap">录入时间</div></th>
		<th><div class="th-gap">操作</div></th>
	  </tr>
	  </thead>
	  <tfoot class="tb-foot-bg"></tfoot>
	  <!--{foreach $onlinechat as $volist}-->
	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">
	    <td align="center"><input name="id[]" type="checkbox" value="<!--{$volist.onid}-->" onClick="checkItem(this, 'chkAll')"></td>
		<td align="center">
		<!--{if $volist.ontype==1}-->
		<font color="green">QQ</font>
		<!--{else if $volist.ontype==2}-->
		<font color="blue">淘宝旺旺</font>
		<!--{else}-->
		<font color="orange">MSN</font>
		<!--{/if}-->
		</td>
		<td align="left"><!--{$volist.number}--></td>
		<td align="left"><!--{$volist.title}--></td>
		<td align="left"><!--{$volist.sitetitle}--></td>
		<td align="left"><!--{$volist.orders}--></td>
		<td align="center">
		<!--{if $volist.flag==0}-->
			<input type="hidden" id="attr_flag<!--{$volist.onid}-->" value="flagopen" />
			<img id="flag<!--{$volist.onid}-->" src="liangjingcms/images/no.gif" onClick="javascript:fetch_ajax('flag','<!--{$volist.onid}-->');" style="cursor:pointer;">
		<!--{else}-->
			<input type="hidden" id="attr_flag<!--{$volist.onid}-->" value="flagclose" />
			<img id="flag<!--{$volist.onid}-->" src="liangjingcms/images/yes.gif" onClick="javascript:fetch_ajax('flag','<!--{$volist.onid}-->');" style="cursor:pointer;">	
		<!--{/if}-->
        </td>
		<td align="center"><!--{$volist.timeline|date_format:"%Y/%m/%d"}--></td>
		<td align="center"><a href="ljcms_onlinechat.php?action=edit&id=<!--{$volist.onid}-->&page=<!--{$page}-->" class="icon-edit">编辑</a>&nbsp;&nbsp;<a href="ljcms_onlinechat.php?action=del&id[]=<!--{$volist.onid}-->" onClick="{if(confirm('确定要删除该信息?')){return true;} return false;}" class="icon-del">删除</a></td>
	  </tr>
	  <!--{foreachelse}-->
      <tr>
	    <td colspan="9" align="center">暂无信息</td>
	  </tr>
	  <!--{/foreach}-->
	  <!--{if $total>0}-->
	  <tr>
		<td align="center"><input name="chkAll" type="checkbox" id="chkAll" onClick="checkAll(this, 'id[]')" value="checkbox"></td>
		<td class="hback" colspan="8"><input class="button" name="btn_del" type="button" value="删 除" onClick="{if(confirm('确定删除选定信息吗!?')){$('#action').val('del');$('#myform').submit();return true;}return false;}" class="button">&nbsp;&nbsp;共[ <b><!--{$total}--></b> ]条记录</td>
	  </tr>
	  <!--{/if}-->
	</table>
	</form>
	<!--{if $pagecount>1}-->
	<table width='95%' border='0' cellspacing='0' cellpadding='0' align='center' style="margin-top:10px;">
	  <tr>
		<td align='center'><!--{$showpage}--></td>
	  </tr>
	</table>
	<!--{/if}-->
  </div>
</div>
<!--{/if}-->

<!--{if $action eq "add"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：系统设置<span>&gt;&gt;</span>在线客服</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_onlinechat.php" class="btn-general"><span>返回列表</span></a>添加客服</h3>
    <form name="myform" id="myform" method="post" action="ljcms_onlinechat.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveadd" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">客服类型 <span class='f_red'>*</span></td>
		<td class='hback' width="85%"><select name="ontype" id="ontype"><option value="1">QQ</option><option value="2">淘宝旺旺</option><option value="3">MSN</option></select> <span id="dontype" class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>帐号/号码 <span class="f_red">*</span> </td>
		<td class='hback'><input type="text" name="number" id="number" class="input-txt" /> <span id='dnumber' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>文字说明 <span class="f_red"></span> </td>
		<td class='hback'><input type="text" name="title" id="title" class="input-txt" /> <span id='dtitle' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>网站名称 <span class="f_red"></span> </td>
		<td class='hback'><input type="text" name="sitetitle" id="sitetitle" class="input-txt" /> <span id='dsitetitle' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>状态设置 </td>
		<td class='hback'><!--{$flag_checkbox}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>排序 <span class="f_red"></span> </td>
		<td class='hback'><input type="text" name="orders" id="orders" class="input-s" value="<!--{$orders}-->" /> <span id='dorders' class='f_red'></span></td>
	  </tr>

	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="添加保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<!--{/if}-->

<!--{if $action eq "edit"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：系统设置<span>&gt;&gt;</span>在线客服</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_onlinechat.php?<!--{$comeurl}-->" class="btn-general"><span>返回列表</span></a>编辑客服</h3>
    <form name="myform" id="myform" method="post" action="ljcms_onlinechat.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveedit" />
	<input type="hidden" name="id" value="<!--{$id}-->" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">客服类型 <span class='f_red'>*</span></td>
		<td class='hback' width="85%"><select name="ontype" id="ontype"><option value="1"<!--{if $onlinechat.ontype==1}--> selected<!--{/if}-->>QQ</option><option value="2"<!--{if $onlinechat.ontype==2}--> selected<!--{/if}-->>淘宝旺旺</option><option value="3"<!--{if $onlinechat.ontype==3}--> selected<!--{/if}-->>MSN</option></select> <span id="dontype" class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>帐号/号码 <span class="f_red">*</span> </td>
		<td class='hback'><input type="text" name="number" id="number" class="input-txt" value="<!--{$onlinechat.number}-->" /> <span id='dnumber' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>文字说明 <span class="f_red"></span> </td>
		<td class='hback'><input type="text" name="title" id="title" class="input-txt" value="<!--{$onlinechat.title}-->" /> <span id='dtitle' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>网站名称 <span class="f_red"></span> </td>
		<td class='hback'><input type="text" name="sitetitle" id="sitetitle" class="input-txt" value="<!--{$onlinechat.sitetitle}-->" /> <span id='dsitetitle' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>状态设置 </td>
		<td class='hback'><!--{$flag_checkbox}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>排序 <span class="f_red"></span> </td>
		<td class='hback'><input type="text" name="orders" id="orders" class="input-s" value="<!--{$onlinechat.orders}-->" /> <span id='dorders' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="更新保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<!--{/if}-->
</td></tr></table>
</body>
</html>
<script type="text/javascript">
function checkform() {
	var t = "";
	var v = "";

	t = "ontype";
	v = $("#"+t).val();
	if(v=="") {
		dmsg("请选择类型", t);
		return false;
	}
	t = "number";
	v = $("#"+t).val();
	if(v=="") {
		dmsg("帐号/号码不能为空", t);
		return false;
	}

	return true;
}
</script>