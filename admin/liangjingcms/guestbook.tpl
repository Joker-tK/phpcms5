<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title>留言管理</title>
<meta name="author" content="<!--{$copyright_author}-->" />
<link type="text/css" rel="stylesheet" href="liangjingcms/css/admin_style.css" media="screen" />
<script type='text/javascript' src='js/jquery-1.4.4.min.js'></script>
<script type='text/javascript' src='js/command.js'></script>
<script type="text/javascript" src='../data/editor/kindeditor.js'></script>
</head>
<body>
<!--{if $action eq ""}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：其他扩展<span>&gt;&gt;</span>留言管理</p></div>
  <div class="main-cont">
    <h3 class="title"><a href="ljcms_guestbook.php?action=add" class="btn-general"><span>添加留言</span></a>留言管理</h3>
	<form action="ljcms_guestbook.php" method="post" name="myform" id="myform" style="margin:0">
	<input type="hidden" name="action" id="action" value="del" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <thead class="tb-tit-bg">
	  <tr>
	    <th width="8%"><div class="th-gap">选择</div></th>
		<th width="10%"><div class="th-gap">留言人</div></th>
		<th width="8%"><div class="th-gap">性别</div></th>
		<th width="20%"><div class="th-gap">联系方式</div></th>
		<th width="16%"><div class="th-gap">留言时间</div></th>
		<th width="8%"><div class="th-gap">审核</div></th>
		<th width="15%"><div class="th-gap">回复状态</div></th>
		<th><div class="th-gap">操作</div></th>
	  </tr>
	  </thead>
	  <tfoot class="tb-foot-bg"></tfoot>
	  <!--{foreach $book as $volist}-->
	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">
	    <td align="center"><input name="id[]" type="checkbox" value="<!--{$volist.bookid}-->" onClick="checkItem(this, 'chkAll')"></td>
		<td align="center"><!--{$volist.bookuser}--></td>
		<td align="center"><!--{if $volist.gender==1}-->男<!--{else}-->女<!--{/if}--></td>
		<td align="left"><!--{$volist.telephone}--> <!--{$volist.mobile}--><!--{if $volist.email!=''}--><br /><!--{$volist.email}--><!--{/if}--></td>
		<td align="center"><!--{$volist.booktimeline|date_format:"%Y/%m/%d %H:%M:%S"}--></td>
		<td align="center">
		<!--{if $volist.flag==0}-->
			<input type="hidden" id="attr_flag<!--{$volist.bookid}-->" value="flagopen" />
			<img id="flag<!--{$volist.bookid}-->" src="liangjingcms/images/no.gif" onClick="javascript:fetch_ajax('flag','<!--{$volist.bookid}-->');" style="cursor:pointer;">
		<!--{else}-->
			<input type="hidden" id="attr_flag<!--{$volist.bookid}-->" value="flagclose" />
			<img id="flag<!--{$volist.bookid}-->" src="liangjingcms/images/yes.gif" onClick="javascript:fetch_ajax('flag','<!--{$volist.bookid}-->');" style="cursor:pointer;">	
		<!--{/if}-->
		</td>
		<td align="center"><!--{if $volist.replycontent!=''}--><font color="green">已回复</font><br /><!--{$volist.replytimeline|date_format:"%Y/%m/%d"}--><!--{else}--><font color="#999999">未回复</font><!--{/if}--></td>
		<td align="center"><a href="ljcms_guestbook.php?action=edit&id=<!--{$volist.bookid}-->&page=<!--{$page}-->" class="icon-edit">回复</a>&nbsp;&nbsp;<a href="ljcms_guestbook.php?action=del&id[]=<!--{$volist.bookid}-->" onClick="{if(confirm('确定要删除该信息?')){return true;} return false;}" class="icon-del">删除</a></td>
	  </tr>
	  <!--{foreachelse}-->
      <tr>
	    <td colspan="8" align="center">暂无信息</td>
	  </tr>
	  <!--{/foreach}-->
	  <!--{if $total>0}-->
	  <tr>
		<td align="center"><input name="chkAll" type="checkbox" id="chkAll" onClick="checkAll(this, 'id[]')" value="checkbox"></td>
		<td class="hback" colspan="7"><input class="button" name="btn_del" type="button" value="删 除" onClick="{if(confirm('确定删除选定信息吗!?')){$('#action').val('del');$('#myform').submit();return true;}return false;}" class="button">&nbsp;&nbsp;共[ <b><!--{$total}--></b> ]条记录</td>
	  </tr>
	  <!--{/if}-->
	</table>
	</form>
	<!--{if $pagecount>1}-->
	<table width='95%' border='0' cellspacing='0' cellpadding='0' align='center' style="margin-top:10px;">
	  <tr>
		<td align='center'><!--{$showpage}--></td>
	  </tr>
	</table>
	<!--{/if}-->
  </div>
</div>
<!--{/if}-->

<!--{if $action eq "add"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：其他扩展<span>&gt;&gt;</span>添加留言</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_guestbook.php" class="btn-general"><span>返回列表</span></a>添加留言</h3>
    <form name="myform" id="myform" method="post" action="ljcms_guestbook.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveadd" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">留言人：<span class='f_red'>*</span></td>
		<td class='hback' width="85%"><input type="text" name="bookuser" id="bookuser" class="input-s" /> <span class='f_red' id="dbookuser"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>性别：<span class='f_red'></span></td>
		<td class='hback'><input type="radio" name="gender" id="gender" value="1" checked />男，<input type="radio" name="gender" id="gender" value="2" />女</td>
	  </tr>

	  <tr>
		<td class='hback_1'>职位：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="jobs" id="jobs" class="input-s" /> <span class='f_red' id="djobs"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>电话：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="telephone" id="telephone" class="input-txt" /> <span class='f_red' id="dtelephone"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>传真：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="fax" id="fax" class="input-txt" /> <span class='f_red' id="dfax"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>手机：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="mobile" id="mobile" class="input-txt" /> <span class='f_red' id="dmobile"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Email：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="email" id="email" class="input-txt" /> <span class='f_red' id="demail"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>QQ/MSN：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="qqmsn" id="qqmsn" class="input-txt" /> <span class='f_red' id="dqqmsn"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>公司名称：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="companyname" id="companyname" class="input-txt" /> <span class='f_red' id="dcompanyname"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>公司地址：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="address" id="address" class="input-txt" /> <span class='f_red' id="daddress"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>公司行业：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="trade" id="trade" class="input-s" /> <span class='f_red' id="dtrade"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>公司主页：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="homepage" id="homepage" class="input-txt" /> <span class='f_red' id="dhomepage"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>审核设置：<span class='f_red'></span></td>
		<td class='hback'><!--{$flag_checkbox}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>留言内容： <span class='f_red'>*</span></td>
		<td class='hback'><textarea name="content" id="content" style='width:60%;height:100px;overflow:auto;color:#444444;'></textarea> <span class='f_red' id="dcontent"></span></td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="添加保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<!--{/if}-->

<!--{if $action eq "edit"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：其他扩展<span>&gt;&gt;</span>回复留言</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_guestbook.php?<!--{$comeurl}-->" class="btn-general"><span>返回列表</span></a>回复留言</h3>
    <form name="myform" id="myform" method="post" action="ljcms_guestbook.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveedit" />
	<input type="hidden" name="id" value="<!--{$id}-->" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">留言人：<span class='f_red'>*</span></td>
		<td class='hback' width="85%"><input type="text" name="bookuser" id="bookuser" class="input-s" value="<!--{$book.bookuser}-->" /> <span class='f_red' id="dbookuser"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>性别：<span class='f_red'></span></td>
		<td class='hback'><input type="radio" name="gender" id="gender" value="1"<!--{if $book.gender==1}--> checked<!--{/if}--> />男，<input type="radio" name="gender" id="gender" value="2"<!--{if $book.gender==2}--> checked<!--{/if}--> />女</td>
	  </tr>

	  <tr>
		<td class='hback_1'>职位：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="jobs" id="jobs" class="input-s" value="<!--{$book.jobs}-->" /> <span class='f_red' id="djobs"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>电话：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="telephone" id="telephone" class="input-txt" value="<!--{$book.telephone}-->" /> <span class='f_red' id="dtelephone"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>传真：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="fax" id="fax" class="input-txt" value="<!--{$book.fax}-->" /> <span class='f_red' id="dfax"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>手机：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="mobile" id="mobile" class="input-txt" value="<!--{$book.mobile}-->" /> <span class='f_red' id="dmobile"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Email：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="email" id="email" class="input-txt" value="<!--{$book.email}-->" /> <span class='f_red' id="demail"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>QQ/MSN：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="qqmsn" id="qqmsn" class="input-txt" value="<!--{$book.qqmsn}-->" /> <span class='f_red' id="dqqmsn"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>公司名称：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="companyname" id="companyname" class="input-txt" value="<!--{$book.companyname}-->" /> <span class='f_red' id="dcompanyname"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>公司地址：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="address" id="address" class="input-txt" value="<!--{$book.address}-->" /> <span class='f_red' id="daddress"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>公司行业：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="trade" id="trade" class="input-s" value="<!--{$book.trade}-->" /> <span class='f_red' id="dtrade"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>公司主页：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="homepage" id="homepage" class="input-txt" value="<!--{$book.homepage}-->" /> <span class='f_red' id="dhomepage"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>审核设置：<span class='f_red'></span></td>
		<td class='hback'><!--{$flag_checkbox}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>留言内容： <span class='f_red'>*</span></td>
		<td class='hback'><textarea name="content" id="content" style='width:60%;height:100px;overflow:auto;color:#444444;'><!--{$book.content}--></textarea> <span class='f_red' id="dcontent"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>留言时间：<span class='f_red'></span></td>
		<td class='hback'><!--{$book.booktimeline|date_format:"%Y/%m/%d %H:%M:%S"}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>留言IP：<span class='f_red'></span></td>
		<td class='hback'><!--{$book.ip}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>回复人：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="replyuser" id="replyuser" class="input-s" value="<!--{$book.replyuser}-->" /> <span class='f_red' id="dreplyuser"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>回复内容： <span class='f_red'></span></td>
		<td class='hback'><textarea name="replycontent" id="replycontent" style='width:60%;height:100px;overflow:auto;color:#444444;'><!--{$book.replycontent}--></textarea>  <span class='f_red' id="dreplycontent"></span></td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="更新保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<!--{/if}-->

</body>
</html>
<script type="text/javascript">
function checkform() {
	var t = "";
	var v = "";

	t = "bookuser";
	v = $("#"+t).val();
	if(v=="") {
		dmsg("留言人不能为空", t);
		return false;
	}

	t = "content";
	v = $("#"+t).val();
	if(v=="") {
		dmsg("留言内容不能为空", t);
		return false;
	}

	return true;
}
</script>
