<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title>模块类型</title>
<meta name="author" content="<!--{$copyright_author}-->" />
<link type="text/css" rel="stylesheet" href="liangjingcms/css/admin_style.css" media="screen" />
<script type='text/javascript' src='js/jquery-1.4.4.min.js'></script>
<script type='text/javascript' src='js/command.js'></script>
<script type="text/javascript" src='../data/editor/kindeditor.js'></script>
</head>
<body>
<table class="tableBorder" width="95%" border="0" align="center" cellpadding="5" cellspacing="1">
<tr>
<td width="100%" class="leftrow">
<!--{if $action eq ""}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：内容管理<span>&gt;&gt;</span>模块类型</p></div>
  <div class="main-cont">
<form action="ljcms_module.php" method="post" name="myform" id="myform" style="margin:0">
<input type="hidden" name="action" id="action" value="savesetting" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <thead class="tb-tit-bg">
	  <tr>
	     <th width="5%"><div class="th-gap">选择</div></th>
	      <th width="8%"><div class="th-gap">ID</div></th>
		<th width="12%"><div class="th-gap">别名</div></th>
		<th width="14%"><div class="th-gap">名称</div></th>
		<th width="10%"><div class="th-gap">颜色</div></th>
		<th width="14%"><div class="th-gap">默认首页模板</div></th>
		<th width="14%"><div class="th-gap">默认列表模板</div></th>
		<th width="13%"><div class="th-gap">默认内容模板</div></th>
		<th><div class="th-gap">状态</div></th>
	  </tr>
	  </thead>
	  <tfoot class="tb-foot-bg"></tfoot>

	  <!--{foreach $module as $volist}-->
	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">
	        <td align="center"><input name="id[]" type="checkbox" value="<!--{$volist.modid}-->" onClick="checkItem(this, 'chkAll')"></td>
	        <td align="center"><!--{$volist.modid}--></td>
		<td><!--{$volist.alias}--></td>
		<td align="left"><input type="text" name="modname_<!--{$volist.modid}-->" value="<!--{$volist.modname}-->" class="input-100" /></td>
		<td align="left"><input type="text" name="color_<!--{$volist.modid}-->" value="<!--{$volist.color}-->" class="input-s" /></td>
		<td align="left"><input type="text" name="tplindex_<!--{$volist.modid}-->" value="<!--{$volist.tplindex}-->" class="input-100" /></td>
		<td align="left"><input type="text" name="tpllist_<!--{$volist.modid}-->" value="<!--{$volist.tpllist}-->" class="input-100" /></td>
		<td align="left"><input type="text" name="tpldetail_<!--{$volist.modid}-->" value="<!--{$volist.tpldetail}-->" class="input-100" /></td>
		<td align="center">
		<select name="enabled_<!--{$volist.modid}-->">
		<option value="1"<!--{if $volist.enabled=='1'}--> selected<!--{/if}-->>开启</option>
		<option value="0"<!--{if $volist.enabled=='0'}--> selected<!--{/if}-->>关闭</option>
		</select>
		</td>
	  </tr>
	  <!--{foreachelse}-->
      <tr>
	    <td colspan="9" align="center">暂无信息</td>
	  </tr>
	  <!--{/foreach}-->
	  <!--{if $total>0}-->
	  <tr>
		<td align="center"><input name="chkAll" type="checkbox" id="chkAll" onClick="checkAll(this, 'id[]')" value="checkbox"></td>
		<td class="hback" colspan="8"><input class="button" name="btn_del" type="button" value="批量更新" onClick="{if(confirm('确定更新选定信息吗!?')){$('#action').val('savesetting');$('#myform').submit();return true;}return false;}" class="button">
		默认模板，不需要填写当前风格路径
		</td>
	  </tr>
	  <!--{/if}-->
	</table>
	</form>
	<!--{if $pagecount>1}-->
	<table width='95%' border='0' cellspacing='0' cellpadding='0' align='center' style="margin-top:10px;">
	  <tr>
		<td align='center'><!--{$showpage}--></td>
	  </tr>
	</table>
	<!--{/if}-->
  </div>
</div>
<!--{/if}-->

<!--{if $action eq "setting"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：内容管理<span>&gt;&gt;</span>设置案例分类</p></div>
  <div class="main-cont">
    <h3 class="title"><a href="ljcms_casecate.php?action=add" class="btn-general"><span>添加分类</span></a>设置案例分类</h3>
	<form action="ljcms_casecate.php" method="post" name="myform" id="myform" style="margin:0">
	<input type="hidden" name="action" id="action" value="savesetting" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <thead class="tb-tit-bg">
	  <tr>
	    <th width="8%"><div class="th-gap">选择</div></th>
		<th width="20%"><div class="th-gap">分类名称</div></th>
		<th width="20%"><div class="th-gap">Meta标题</div></th>
		<th width="20%"><div class="th-gap">Meta关键字</div></th>
		<th width="20%"><div class="th-gap">Meta描述</div></th>
		<th><div class="th-gap">排序</div></th>
	  </tr>
	  </thead>
	  <tfoot class="tb-foot-bg"></tfoot>
	  <!--{foreach $cate as $volist}-->
	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">
	    <td align="center"><input name="id[]" type="checkbox" value="<!--{$volist.cateid}-->" onClick="checkItem(this, 'chkAll')"></td>
		<td align="center"><input type="text" name="catename_<!--{$volist.cateid}-->" value="<!--{$volist.catename}-->" size="18" /></td>
		<td align="center"><input type="text" name="metatitle_<!--{$volist.cateid}-->" value="<!--{$volist.metatitle}-->" size="18" /></td>
		<td align="center"><input type="text" name="metakeyword_<!--{$volist.cateid}-->" value="<!--{$volist.metakeyword}-->" size="18" /></td>
		<td align="center"><input type="text" name="metadescription_<!--{$volist.cateid}-->" value="<!--{$volist.metadescription}-->" size="18" /></td>
		<td align="center"><input type="text" name="orders_<!--{$volist.cateid}-->" value="<!--{$volist.orders}-->" size="6" /></td>
	  </tr>
	  <!--{foreachelse}-->
      <tr>
	    <td colspan="6" align="center">暂无信息</td>
	  </tr>
	  <!--{/foreach}-->
	  <!--{if $total>0}-->
	  <tr>
		<td align="center"><input name="chkAll" type="checkbox" id="chkAll" onClick="checkAll(this, 'id[]')" value="checkbox"></td>
		<td class="hback" colspan="5"><input class="button" name="btn_del" type="button" value="批量更新" onClick="{if(confirm('确定更新选定信息吗!?')){$('#action').val('savesetting');$('#myform').submit();return true;}return false;}" class="button"></td>
	  </tr>
	  <!--{/if}-->
	</table>
	</form>
	<!--{if $pagecount>1}-->
	<table width='95%' border='0' cellspacing='0' cellpadding='0' align='center' style="margin-top:10px;">
	  <tr>
		<td align='center'><!--{$showpage}--></td>
	  </tr>
	</table>
	<!--{/if}-->
  </div>
</div>
<!--{/if}-->


<!--{if $action eq "add"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：内容管理<span>&gt;&gt;</span>添加案例分类</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_casecate.php" class="btn-general"><span>返回列表</span></a>添加案例分类</h3>
    <form name="myform" id="myform" method="post" action="ljcms_casecate.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveadd" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">分类名称：<span class='f_red'>*</span></td>
		<td class='hback' width="85%"><input type="text" name="catename" id="catename" class="input-txt" /> <span class='f_red' id="dcatename"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta标题 </td>
		<td class='hback'><input type="text" name="metatitle" id="metatitle" class="input-txt" />  <span id='dmetatitle' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta关键字 <span class="f_red"></span></td>
		<td class='hback'><textarea name="metakeyword" id="metakeyword" style="width:60%;height:45px;display:;overflow:auto;"></textarea> <span id='dmetakeyword' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta描述 <span class="f_red"></span></td>
		<td class='hback'><textarea name="metadescription" id="metadescription" style="width:60%;height:45px;display:;overflow:auto;"></textarea> <span id='dmetadescription' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>分类排序：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="orders" id="orders" class="input-s" value="<!--{$orders}-->" /> <span class='f_red' id="dorders"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>参数设置：<span class='f_red'></span></td>
		<td class='hback'><!--{$flag_checkbox}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>CCS样式：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="cssname" id="cssname" class="input-s" /> <span class='f_red' id="dcssname"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>样式图标 <span class="f_red"></span> </td>
		<td class='hback'>
		  <input type="hidden" name="img" id="img" /><span id='dimg' class='f_red'></span>
		  <iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?comeform=myform&inputid=img&thumbflag=0'></iframe>
		</td>
	  </tr>
	  <tr>
		<td class='hback_1'>打开方式：<span class='f_red'></span></td>
		<td class='hback'><select name="target" id="target"><option value="1">本页面</option><option value="2">新页面</option></select></td>
	  </tr>
	  <tr>
		<td class='hback_1'>链接类型：<span class='f_red'></span></td>
		<td class='hback'><input type="radio" name="linktype" value="1" checked />内部链接，<input type="radio" name="linktype" value="2" />外部链接</td>
	  </tr>
	  <tr>
		<td class='hback_1'>外部URL：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="linkurl" id="linkurl" class="input-txt" /> <span class='f_red' id="dlinkurl"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>分类简介： </td>
		<td class='hback'><textarea name="intro" id="intro" style='width:60%;height:60px;overflow:auto;color:#444444;'></textarea><br />（500字符以内）</td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="添加保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<!--{/if}-->

<!--{if $action eq "edit"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：内容管理<span>&gt;&gt;</span>编辑案例分类</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_casecate.php?<!--{$comeurl}-->" class="btn-general"><span>返回列表</span></a>编辑案例分类</h3>
    <form name="myform" id="myform" method="post" action="ljcms_casecate.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveedit" />
	<input type="hidden" name="id" value="<!--{$id}-->" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">分类名称：<span class='f_red'>*</span></td>
		<td class='hback' width="85%"><input type="text" name="catename" id="catename" class="input-txt" value="<!--{$cate.catename}-->" /> <span class='f_red' id="dcatename"></span></td>
	  </tr>

	  <tr>
		<td class='hback_1'>Meta标题 </td>
		<td class='hback'><input type="text" name="metatitle" id="metatitle" class="input-txt" value="<!--{$cate.metatitle}-->" />  <span id='dmetatitle' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta关键字 <span class="f_red"></span></td>
		<td class='hback'>
		  <textarea name="metakeyword" id="metakeyword" style="width:60%;height:45px;display:;overflow:auto;"><!--{$cate.metakeyword}--></textarea> <span id='dmetakeyword' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta描述 <span class="f_red"></span></td>
		<td class='hback'>
		  <textarea name="metadescription" id="metadescription" style="width:60%;height:45px;display:;overflow:auto;"><!--{$cate.metadescription}--></textarea> <span id='dmetadescription' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>分类排序：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="orders" id="orders" class="input-s" value="<!--{$cate.orders}-->" /> <span class='f_red' id="dorders"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>参数设置：<span class='f_red'></span></td>
		<td class='hback'><!--{$flag_checkbox}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>CCS样式：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="cssname" id="cssname" class="input-s" value="<!--{$cate.cssname}-->" /> <span class='f_red' id="dcssname"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>样式图标 <span class="f_red"></span> </td>
		<td class='hback'>
		  <input type="hidden" name="img" id="img" value="<!--{$cate.img}-->" /><span id='dimg' class='f_red'></span>
		    <!--{if $cate.img!=""}-->
			<iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?action=show&comeform=myform&inputid=img&thumbflag=0&picname=<!--{$cate.imgname}-->&picurl=<!--{$cate.img}-->'></iframe>
			<!--{else}-->
			<iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?comeform=myform&inputid=img&thumbflag=0'></iframe>
			<!--{/if}-->
		</td>
	  </tr>
	  <tr>
		<td class='hback_1'>打开方式：<span class='f_red'></span></td>
		<td class='hback'><select name="target" id="target"><option value="1"<!--{if $cate.target==1}--> selected<!--{/if}-->>本页面</option><option value="2"<!--{if $cate.target==2}--> selected<!--{/if}-->>新页面</option></select></td>
	  </tr>
	  <tr>
		<td class='hback_1'>链接类型：<span class='f_red'></span></td>
		<td class='hback'><input type="radio" name="linktype" value="1"<!--{if $cate.linktype==1}--> checked<!--{/if}--> />内部链接，<input type="radio" name="linktype" value="2"<!--{if $cate.linktype==2}--> checked<!--{/if}--> />外部链接</td>
	  </tr>
	  <tr>
		<td class='hback_1'>外部URL：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="linkurl" id="linkurl" class="input-txt" value="<!--{$cate.linkurl}-->" /> <span class='f_red' id="dlinkurl"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>分类简介： </td>
		<td class='hback'><textarea name="intro" id="intro" style='width:60%;height:60px;overflow:auto;color:#444444;'><!--{$cate.intro}--></textarea><br />（500字符以内）</td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="更新保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<!--{/if}-->
</td></tr></table>
</body>
</html>
<script type="text/javascript">
function checkform() {
	var t = "";
	var v = "";

	t = "catename";
	v = $("#"+t).val();
	if(v=="") {
		dmsg("分类名称不能为空", t);
		return false;
	}
	return true;
}
</script>
