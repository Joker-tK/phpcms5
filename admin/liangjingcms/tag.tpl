<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title>TAGS</title>
<meta name="author" content="<!--{$copyright_author}-->" />
<link type="text/css" rel="stylesheet" href="liangjingcms/css/admin_style.css" media="screen" />
<script type='text/javascript' src='js/jquery-1.4.4.min.js'></script>
<script type='text/javascript' src='js/command.js'></script>
<script type="text/javascript" src='../data/editor/kindeditor.js'></script>
</head>
<body>
<!--{if $action eq ""}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：其他扩展<span>&gt;&gt;</span>TAGS</p></div>
  <div class="main-cont">
    <h3 class="title"><a href="ljcms_tag.php?action=add" class="btn-general"><span>添加TAGS</span></a>TAGS</h3>
	<div class="search-area ">
	  <form method="post" id="search_form" action="ljcms_tag.php" />
	  <div class="item">
	    <label>所在频道：</label><!--{$channel_search}-->&nbsp;&nbsp;
		<label>TAGS名：</label><input type="text" id="sname" name="sname" size="15" class="input" value="<!--{$sname}-->" />&nbsp;&nbsp;&nbsp;
		<input type="submit" class="button_s" value="搜 索" />
	  </div>
	  </form>
	</div>
	<form action="ljcms_tag.php" method="post" name="myform" id="myform" style="margin:0">
	<input type="hidden" name="action" id="action" value="del" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <thead class="tb-tit-bg">
	  <tr>
	    <th width="8%"><div class="th-gap">选择</div></th>
		<th width="13%"><div class="th-gap">所在频道</div></th>
		<th width="15%"><div class="th-gap">TAGS名称</div></th>
		<th width="8%"><div class="th-gap">状态</div></th>
		<th width="10%"><div class="th-gap">是否加粗</div></th>
		<th width="10%"><div class="th-gap">打开方式</div></th>
		<th width="20%"><div class="th-gap">URL</div></th>
		<th><div class="th-gap">操作</div></th>
	  </tr>
	  </thead>
	  <tfoot class="tb-foot-bg"></tfoot>
	  <!--{foreach $tag as $volist}-->
	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">
	    <td align="center"><input name="id[]" type="checkbox" value="<!--{$volist.tagid}-->" onClick="checkItem(this, 'chkAll')"></td>
		<td align="center"><!--{$volist.channel}--></td>
		<td align="center"><!--{$volist.tag}--></td>
		<td align="center">
		<!--{if $volist.flag==0}-->
			<input type="hidden" id="attr_flag<!--{$volist.tagid}-->" value="flagopen" />
			<img id="flag<!--{$volist.tagid}-->" src="liangjingcms/images/no.gif" onClick="javascript:fetch_ajax('flag','<!--{$volist.tagid}-->');" style="cursor:pointer;">
		<!--{else}-->
			<input type="hidden" id="attr_flag<!--{$volist.tagid}-->" value="flagclose" />
			<img id="flag<!--{$volist.tagid}-->" src="liangjingcms/images/yes.gif" onClick="javascript:fetch_ajax('flag','<!--{$volist.tagid}-->');" style="cursor:pointer;">	
		<!--{/if}-->
        </td>
		<td align="center">
		<!--{if $volist.strong==0}-->
			<input type="hidden" id="attr_strong<!--{$volist.tagid}-->" value="strongopen" />
			<img id="strong<!--{$volist.tagid}-->" src="liangjingcms/images/no.gif" onClick="javascript:fetch_ajax('strong','<!--{$volist.tagid}-->');" style="cursor:pointer;">
		<!--{else}-->
			<input type="hidden" id="attr_strong<!--{$volist.tagid}-->" value="strongclose" />
			<img id="strong<!--{$volist.tagid}-->" src="liangjingcms/images/yes.gif" onClick="javascript:fetch_ajax('strong','<!--{$volist.tagid}-->');" style="cursor:pointer;">	
		<!--{/if}-->
        </td>
		<td align="center"><!--{if $volist.target==1}--><font color="green">本页</font><!--{else}--><font color="blue">新页面</font><!--{/if}--></td>
		<td align="left"><!--{$volist.url}--></td>
		<td align="center"><a href="ljcms_tag.php?action=edit&id=<!--{$volist.tagid}-->&page=<!--{$page}-->&<!--{$urlitem}-->" class="icon-edit">编辑</a>&nbsp;&nbsp;<a href="ljcms_tag.php?action=del&id[]=<!--{$volist.tagid}-->" onClick="{if(confirm('确定要删除该信息?')){return true;} return false;}" class="icon-del">删除</a></td>
	  </tr>
	  <!--{foreachelse}-->
      <tr>
	    <td colspan="8" align="center">暂无信息</td>
	  </tr>
	  <!--{/foreach}-->
	  <!--{if $total>0}-->
	  <tr>
		<td align="center"><input name="chkAll" type="checkbox" id="chkAll" onClick="checkAll(this, 'id[]')" value="checkbox"></td>
		<td class="hback" colspan="7"><input class="button" name="btn_del" type="button" value="删 除" onClick="{if(confirm('确定删除选定信息吗!?')){$('#action').val('del');$('#myform').submit();return true;}return false;}" class="button">&nbsp;&nbsp;共[ <b><!--{$total}--></b> ]条记录</td>
	  </tr>
	  <!--{/if}-->
	</table>
	</form>
	<!--{if $pagecount>1}-->
	<table width='95%' border='0' cellspacing='0' cellpadding='0' align='center' style="margin-top:10px;">
	  <tr>
		<td align='center'><!--{$showpage}--></td>
	  </tr>
	</table>
	<!--{/if}-->
  </div>
</div>
<!--{/if}-->

<!--{if $action eq "add"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：其他扩展<span>&gt;&gt;</span>TAGS</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_tag.php" class="btn-general"><span>返回列表</span></a>添加TAGS</h3>
    <form name="myform" id="myform" method="post" action="ljcms_tag.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveadd" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">所在频道 <span class='f_red'>*</span></td>
		<td class='hback' width="85%"><!--{$channel_select}--> <span id="dchannel" class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Tags名称 <span class="f_red">*</span> </td>
		<td class='hback'><input type="text" name="tag" id="tag" class="input-txt" /> <span id='dtag' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>打开方式 <span class="f_red"></span> </td>
		<td class='hback'><input type="radio" name="target" id="target" value="1" checked />本页，<input type="radio" name="target" id="target" value="2" />新页面</td>
	  </tr>
	  <tr>
		<td class='hback_1'>URL链接 <span class="f_red"></span> </td>
		<td class='hback'><input type="text" name="url" id="url" class="input-txt" />（以“http://”开头） <span id='durl' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>参数设置 </td>
		<td class='hback'><!--{$flag_checkbox}-->，<!--{$strong_checkbox}--></td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="添加保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<!--{/if}-->

<!--{if $action eq "edit"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：其他扩展<span>&gt;&gt;</span>编辑TAGS</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_tag.php?<!--{$comeurl}-->" class="btn-general"><span>返回列表</span></a>编辑TAGS</h3>
    <form name="myform" id="myform" method="post" action="ljcms_tag.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveedit" />
	<input type="hidden" name="id" value="<!--{$id}-->" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">所在频道 <span class='f_red'>*</span></td>
		<td class='hback' width="85%"><!--{$channel_select}--> <span id="dchannel" class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Tags名称 <span class="f_red">*</span> </td>
		<td class='hback'><input type="text" name="tag" id="tag" class="input-txt" value="<!--{$tag.tag}-->" /> <span id='dtag' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>打开方式 <span class="f_red"></span> </td>
		<td class='hback'><input type="radio" name="target" id="target" value="1"<!--{if $tag.target==1}--> checked<!--{/if}--> />本页，<input type="radio" name="target" id="target" value="2"<!--{if $tag.target==2}--> checked<!--{/if}--> />新页面</td>
	  </tr>
	  <tr>
		<td class='hback_1'>URL链接 <span class="f_red"></span> </td>
		<td class='hback'><input type="text" name="url" id="url" class="input-txt" value="<!--{$tag.url}-->" />（以“http://”开头） <span id='durl' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>参数设置 </td>
		<td class='hback'><!--{$flag_checkbox}-->，<!--{$strong_checkbox}--></td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="更新保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<!--{/if}-->
</body>
</html>
<script type="text/javascript">
function checkform() {
	var t = "";
	var v = "";

	t = "channel";
	v = $("#"+t).val();
	if(v=="") {
		dmsg("请选择所在频道", t);
		return false;
	}
	t = "tag";
	v = $("#"+t).val();
	if(v=="") {
		dmsg("Tags名称不能为空", t);
		return false;
	}
    
	return true;
}
</script>