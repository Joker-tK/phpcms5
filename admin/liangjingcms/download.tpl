<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title>下载内容</title>
<meta name="author" content="<!--{$copyright_author}-->" />
<link type="text/css" rel="stylesheet" href="liangjingcms/css/admin_style.css" media="screen" />
<script type='text/javascript' src='js/jquery-1.4.4.min.js'></script>
<script type='text/javascript' src='js/command.js'></script>
<script type="text/javascript" src='../data/editor/kindeditor.js'></script>
<script type="text/javascript" src='../tpl/static/js/WdatePicker.js'></script>
</head>
<body>
<table class="tableBorder" width="95%" border="0" align="center" cellpadding="5" cellspacing="1">
<tr>
<td width="100%" class="leftrow">
<!--{if $action eq ""}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：内容管理<span>&gt;&gt;</span>下载内容</p></div>
  <div class="main-cont">
    <h3 class="title"><a href="ljcms_download.php?action=add" class="btn-general"><span>发布下载</span></a>下载内容</h3>
	<div class="search-area ">
	  <form method="post" id="search_form" action="ljcms_download.php" />
	  <div class="item">
	    <label>下载分类：</label><!--{$cate_search}-->&nbsp;&nbsp;
		<label>下载名称：</label><input type="text" id="sname" name="sname" size="20" class="input" value="<!--{$sname}-->" />&nbsp;&nbsp;&nbsp;
		<input type="submit" class="button_s" value="搜 索" />
	  </div>
	  </form>
	</div>
	<form action="ljcms_download.php" method="post" name="myform" id="myform" style="margin:0">
	<input type="hidden" name="action" id="action" value="del" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <thead class="tb-tit-bg">
	  <tr>
	        <th width="8%"><div class="th-gap">选择</div></th>
		<th width="10%"><div class="th-gap">所在分类</div></th>
		<th width="18%"><div class="th-gap">资源名称</div></th>
		<th width="10%"><div class="th-gap">文件大小</div></th>
		<th width="8%"><div class="th-gap">状态</div></th>
		<th width="12%"><div class="th-gap">机器条码</div></th>
		<th width="8%"><div class="th-gap">下载次数</div></th>
		<th width="12%"><div class="th-gap">发布时间</div></th>
		<th><div class="th-gap">操作</div></th>
	  </tr>
	  </thead>
	  <tfoot class="tb-foot-bg"></tfoot>
	  <!--{foreach $download as $volist}-->
	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">
	    <td align="center"><input name="id[]" type="checkbox" value="<!--{$volist.downid}-->" onClick="checkItem(this, 'chkAll')"></td>
		<td align="center"><!--{$volist.catename}--></td>
		<td align="left"><a href="../download.php?mod=detail&id=<!--{$volist.downid}-->" target="_blank"><!--{$volist.title}--></a></td>
		<td align="left"><!--{$volist.filesize}--></td>
		<td align="center">
		<!--{if $volist.flag==0}-->
			<input type="hidden" id="attr_flag<!--{$volist.downid}-->" value="flagopen" />
			<img id="flag<!--{$volist.downid}-->" src="liangjingcms/images/no.gif" onClick="javascript:fetch_ajax('flag','<!--{$volist.downid}-->');" style="cursor:pointer;">
		<!--{else}-->
			<input type="hidden" id="attr_flag<!--{$volist.downid}-->" value="flagclose" />
			<img id="flag<!--{$volist.downid}-->" src="liangjingcms/images/yes.gif" onClick="javascript:fetch_ajax('flag','<!--{$volist.downid}-->');" style="cursor:pointer;">	
		<!--{/if}-->
        </td>
		<td align="center"><!--{$volist.barcode}--></td>
		<td align="center"><!--{$volist.downs}--></td>
		<td align="center"><!--{$volist.timeline|date_format:"%Y/%m/%d"}--></td>
		<td align="center"><a href="ljcms_download.php?action=edit&id=<!--{$volist.downid}-->&page=<!--{$page}-->&<!--{$urlitem}-->" class="icon-edit">编辑</a>&nbsp;&nbsp;<a href="ljcms_download.php?action=del&id[]=<!--{$volist.downid}-->" onClick="{if(confirm('确定要删除该信息?')){return true;} return false;}" class="icon-del">删除</a></td>
	  </tr>
	  <!--{foreachelse}-->
      <tr>
	    <td colspan="8" align="center">暂无信息</td>
	  </tr>
	  <!--{/foreach}-->
	  <!--{if $total>0}-->
	  <tr>
		<td align="center"><input name="chkAll" type="checkbox" id="chkAll" onClick="checkAll(this, 'id[]')" value="checkbox"></td>
		<td class="hback" colspan="7"><input class="button" name="btn_del" type="button" value="删 除" onClick="{if(confirm('确定删除选定信息吗!?')){$('#action').val('del');$('#myform').submit();return true;}return false;}" class="button">&nbsp;&nbsp;共[ <b><!--{$total}--></b> ]条记录</td>
	  </tr>
	  <!--{/if}-->
	</table>
	</form>
	<!--{if $pagecount>1}-->
	<table width='95%' border='0' cellspacing='0' cellpadding='0' align='center' style="margin-top:10px;">
	  <tr>
		<td align='center'><!--{$showpage}--></td>
	  </tr>
	</table>
	<!--{/if}-->
  </div>
</div>
<!--{/if}-->

<!--{if $action eq "setting"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：内容管理<span>&gt;&gt;</span>设置下载内容</p></div>
  <div class="main-cont">
    <h3 class="title"><a href="ljcms_download.php?action=add" class="btn-general"><span>发布下载</span></a>设置下载内容</h3>
	<form action="ljcms_download.php" method="post" name="myform" id="myform" style="margin:0">
	<input type="hidden" name="action" id="action" value="savesetting" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <thead class="tb-tit-bg">
	  <tr>
	    <th width="8%"><div class="th-gap">选择</div></th>
		<th width="20%"><div class="th-gap">标题</div></th>
		<th width="20%"><div class="th-gap">Meta标题</div></th>
		<th width="20%"><div class="th-gap">Meta关键字</div></th>
		<th width="20%"><div class="th-gap">Meta描述</div></th>
	  </tr>
	  </thead>
	  <tfoot class="tb-foot-bg"></tfoot>
	  <!--{foreach $download as $volist}-->
	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">
	    <td align="center"><input name="id[]" type="checkbox" value="<!--{$volist.downid}-->" onClick="checkItem(this, 'chkAll')"><!--{$volist.downid}--></td>
		<td align="center"><input type="text" name="title_<!--{$volist.downid}-->" value="<!--{$volist.title}-->" size="22" /></td>
		<td align="center"><input type="text" name="metatitle_<!--{$volist.downid}-->" value="<!--{$volist.metatitle}-->" size="20" /></td>
		<td align="center"><input type="text" name="metakeyword_<!--{$volist.downid}-->" value="<!--{$volist.metakeyword}-->" size="20" /></td>
		<td align="center"><input type="text" name="metadescription_<!--{$volist.downid}-->" value="<!--{$volist.metadescription}-->" size="20" /></td>
	  </tr>
	  <!--{foreachelse}-->
      <tr>
	    <td colspan="5" align="center">暂无信息</td>
	  </tr>
	  <!--{/foreach}-->
	  <!--{if $total>0}-->
	  <tr>
		<td align="center"><input name="chkAll" type="checkbox" id="chkAll" onClick="checkAll(this, 'id[]')" value="checkbox"></td>
		<td class="hback" colspan="4"><input class="button" name="btn_del" type="button" value="批量更新" onClick="{if(confirm('确定更新选定信息吗!?')){$('#action').val('savesetting');$('#myform').submit();return true;}return false;}" class="button"></td>
	  </tr>
	  <!--{/if}-->
	</table>
	</form>
	<!--{if $pagecount>1}-->
	<table width='95%' border='0' cellspacing='0' cellpadding='0' align='center' style="margin-top:10px;">
	  <tr>
		<td align='center'><!--{$showpage}--></td>
	  </tr>
	</table>
	<!--{/if}-->
  </div>
</div>
<!--{/if}-->


<!--{if $action eq "add"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：内容管理<span>&gt;&gt;</span>发布下载</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_download.php" class="btn-general"><span>返回列表</span></a>发布下载</h3>
    <form name="myform" id="myform" method="post" action="ljcms_download.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveadd" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">下载分类 <span class='f_red'>*</span></td>
		<td class='hback' width="85%"><!--{$cate_select}--> <span id="dcateid" class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>下载名称 <span class="f_red">*</span> </td>
		<td class='hback'><input type="text" name="title" id="title" class="input-txt" /> <span id='dtitle' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta标题 </td>
		<td class='hback'><input type="text" name="metatitle" id="metatitle" class="input-txt" />  <span id='dmetatitle' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta关键字 <span class="f_red"></span></td>
		<td class='hback'><textarea name="metakeyword" id="metakeyword" style="width:60%;height:45px;display:;overflow:auto;"></textarea> <span id='dmetakeyword' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta描述 <span class="f_red"></span></td>
		<td class='hback'><textarea name="metadescription" id="metadescription" style="width:60%;height:45px;display:;overflow:auto;"></textarea> <span id='dmetadescription' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>下载地址 <span class="f_red">*</span> </td>
		<td class='hback'><input type='text' name='uploadfiles' id="uploadfiles" class="input-txt" />  <a href="javascript:upload('myform','uploadfiles')">点击上传资源</a> <span id='duploadfiles' class='f_red'></span></td>
	  </tr>

	  <tr>
		<td class='hback_1'>机器条码 <span class="f_red">*</span> </td>
		<td class='hback'><input type="text" name="barcode" id="barcode" class="input-txt" />  <span id='dbarcode' class='f_red'></span></td>
	  </tr>

	  <tr>
		<td class='hback_1'>发布日期 <span class="f_red"></span> </td>
		<td class='hback'><input type="text" name="dateline" id="dateline" class="input-s" value="<!--{$time|date_format:"%Y-%m-%d"}-->" readonly onClick="WdatePicker();" /> <span id='ddateline' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>文件大小 <span class="f_red"></span> </td>
		<td class='hback'><input type="text" name="filesize" id="filesize" class="input-s" /> <span id='dfilesize' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>下载次数 <span class="f_red"></span> </td>
		<td class='hback'><input type="text" name="downs" id="downs" class="input-s" /> <span id='ddowns' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>状态设置 </td>
		<td class='hback'><!--{$flag_checkbox}-->，<!--{$elite_checkbox}--></td>
	  </tr>
         <tr>
	<td class='hback_1'>浏览权限</td>
	<td class='hback'>
       <!--{$ugroupid_select}-->

        <input name="exclusive" type="radio" value="&gt;=" checked>
        隶属
        <input name="exclusive"  type="radio"  value="=">
        专属（隶属：权限值≥可查看，专属：权限值＝可查看）	
		</td>
	  </tr>

	  <tr>
		<td class='hback_1'>资源简介： </td>
		<td class='hback'><textarea name="intro" id="intro" style='width:60%;height:65px;overflow:auto;color:#444444;'></textarea></td>
	  </tr>
	  <tr>
		<td class='hback_1'>详细介绍 <span class="f_red"></span></td>
		<td class='hback'>
		  <textarea name="content" id="content" style="width:99%;height:200px;display:none;"></textarea>
		  <script>KE.show({id : 'content' });</script>  <span id='dcontent' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="添加保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<!--{/if}-->

<!--{if $action eq "edit"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：内容管理<span>&gt;&gt;</span>编辑下载</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_download.php?<!--{$comeurl}-->" class="btn-general"><span>返回列表</span></a>编辑下载</h3>
    <form name="myform" id="myform" method="post" action="ljcms_download.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveedit" />
	<input type="hidden" name="id" value="<!--{$id}-->" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">下载分类 <span class='f_red'>*</span></td>
		<td class='hback' width="85%"><!--{$cate_select}--> <span id="dcateid" class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>下载名称 <span class="f_red">*</span> </td>
		<td class='hback'><input type="text" name="title" id="title" class="input-txt" value="<!--{$download.title}-->" /> <span id='dtitle' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta标题 </td>
		<td class='hback'><input type="text" name="metatitle" id="metatitle" class="input-txt" value="<!--{$download.metatitle}-->" />  <span id='dmetatitle' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta关键字 <span class="f_red"></span></td>
		<td class='hback'><textarea name="metakeyword" id="metakeyword" style="width:60%;height:45px;display:;overflow:auto;"><!--{$download.metakeyword}--></textarea> <span id='dmetakeyword' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta描述 <span class="f_red"></span></td>
		<td class='hback'><textarea name="metadescription" id="metadescription" style="width:60%;height:45px;display:;overflow:auto;"><!--{$download.metadescription}--></textarea> <span id='dmetadescription' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>下载地址 <span class="f_red">*</span> </td>
		<td class='hback'><input type="text" name="uploadfiles" id="uploadfiles" class="input-txt" value="<!--{$download.uploadfiles}-->" />  <a href="javascript:upload('myform','uploadfiles')">点击上传资源</a> <span id='duploadfiles' class='f_red'></span></td>
	  </tr>

	  <tr>
		<td class='hback_1'>机器条码 <span class="f_red">*</span> </td>
		<td class='hback'><input type="text" name="barcode" id="barcode" class="input-txt" value="<!--{$download.barcode}-->" />  <span id='dbarcode' class='f_red'></span></td>
	  </tr>

	  <tr>
		<td class='hback_1'>发布日期 <span class="f_red"></span> </td>
		<td class='hback'><input type="text" name="dateline" id="dateline" class="input-s" value="<!--{$download.dateline|date_format:"%Y-%m-%d"}-->" readonly onClick="WdatePicker();" /> <span id='ddateline' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>文件大小 <span class="f_red"></span> </td>
		<td class='hback'><input type="text" name="filesize" id="filesize" class="input-s" value="<!--{$download.filesize}-->" /> <span id='dfilesize' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>下载次数 <span class="f_red"></span> </td>
		<td class='hback'><input type="text" name="downs" id="downs" class="input-s" value="<!--{$download.downs}-->" /> <span id='ddowns' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>状态设置 </td>
		<td class='hback'><!--{$flag_checkbox}-->，<!--{$elite_checkbox}--></td>
	  </tr>
	  	  <tr>
		<td class='hback_1'>浏览权限</td>
	<td class='hback'>
       <!--{$ugroupid_select}-->

        <input name="exclusive" type="radio" value="&gt;=" <!--{if $download.exclusive==">="}--> checked<!--{/if}-->>
        隶属
        <input name="exclusive"  type="radio"  value="=" <!--{if $download.exclusive=="="}--> checked<!--{/if}-->>
        专属（隶属：权限值≥可查看，专属：权限值＝可查看）	
		</td>
	  </tr>
	  <tr>
		<td class='hback_1'>资源简介： </td>
		<td class='hback'><textarea name="intro" id="intro" style='width:60%;height:65px;overflow:auto;color:#444444;'><!--{$download.intro}--></textarea></td>
	  </tr>
	  <tr>
		<td class='hback_1'>详细介绍 <span class="f_red"></span></td>
		<td class='hback'>
		  <textarea name="content" id="content" style="width:99%;height:200px;display:none;"><!--{$download.content}--></textarea>
		  <script>KE.show({id : 'content' });</script>  <span id='dcontent' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="更新保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<!--{/if}-->
</td></tr></table>
</body>
</html>
<script type="text/javascript">
function checkform() {
	var t = "";
	var v = "";

	t = "cateid";
	v = $("#"+t).val();
	if(v=="") {
		dmsg("请选择分类", t);
		return false;
	}
	t = "title";
	v = $("#"+t).val();
	if(v=="") {
		dmsg("下载名称不能为空", t);
		return false;
	}

	t = "uploadfiles";
	v = $("#"+t).val();
	if(v=="") {
		dmsg("下载地址不能为空", t);
		return false;
	}

	return true;
}

function upload(s_formname,s_inputname){
    window.open("annexform.php?comeform="+s_formname+"&inputname="+s_inputname+"","load_annexup","status=no,scrollbars=no,top=200,left=310,width=420,height=165");
}
</script>