<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title>自定义HTML标签</title>
<meta name="author" content="<!--{$copyright_author}-->" />
<link type="text/css" rel="stylesheet" href="liangjingcms/css/admin_style.css" media="screen" />
<script type='text/javascript' src='js/jquery-1.4.4.min.js'></script>
<script type='text/javascript' src='js/command.js'></script>
<script type="text/javascript" src='../data/editor/kindeditor.js'></script>
</head>
<body>
<!--{if $action eq ""}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：其他扩展<span>&gt;&gt;</span>自定义HTML标签</p></div>
  <div class="main-cont">
    <h3 class="title"><a href="ljcms_delimitlabel.php?action=add" class="btn-general"><span>添加标签</span></a>自定义HTML标签</h3>
	<div class="search-area ">
	  <form method="post" id="search_form" action="ljcms_delimitlabel.php" />
	  <div class="item">
	    <label>所属风格模板：</label><!--{$skin_search}-->&nbsp;&nbsp;
		<label>标签描述：</label><input type="text" id="sname" name="sname" size="15" class="input" value="<!--{$sname}-->" />&nbsp;&nbsp;&nbsp;
		<input type="submit" class="button_s" value="搜 索" />
	  </div>
	  </form>
	</div>
	<form action="ljcms_delimitlabel.php" method="post" name="myform" id="myform" style="margin:0">
	<input type="hidden" name="action" id="action" value="del" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <thead class="tb-tit-bg">
	  <tr>
	    <th width="10%"><div class="th-gap">选择</div></th>
		<th width="15%"><div class="th-gap">所属风格</div></th>
		<th width="20%"><div class="th-gap">标签名</div></th>
		<th width="15%"><div class="th-gap">标签描述</div></th>
		<th width="8%"><div class="th-gap">状态</div></th>
		<th width="18%"><div class="th-gap">录入时间</div></th>
		<th><div class="th-gap">操作</div></th>
	  </tr>
	  </thead>
	  <tfoot class="tb-foot-bg"></tfoot>
	  <!--{foreach $delimitlabel as $volist}-->
	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">
	    <td align="center"><input name="id[]" type="checkbox" value="<!--{$volist.labelid}-->" onClick="checkItem(this, 'chkAll')"></td>
		<td align="center"><!--{$volist.skinname}--></td>
		<td align="left">&lt;!--{$delimit_<!--{$volist.labelname}-->}--&gt;</td>
		<td align="left"><!--{$volist.labeltitle}--></td>
		<td align="center">
		<!--{if $volist.flag==0}-->
			<input type="hidden" id="attr_flag<!--{$volist.labelid}-->" value="flagopen" />
			<img id="flag<!--{$volist.labelid}-->" src="liangjingcms/images/no.gif" onClick="javascript:fetch_ajax('flag','<!--{$volist.labelid}-->');" style="cursor:pointer;">
		<!--{else}-->
			<input type="hidden" id="attr_flag<!--{$volist.labelid}-->" value="flagclose" />
			<img id="flag<!--{$volist.labelid}-->" src="liangjingcms/images/yes.gif" onClick="javascript:fetch_ajax('flag','<!--{$volist.labelid}-->');" style="cursor:pointer;">	
		<!--{/if}-->
        </td>
		<td><!--{$volist.timeline|date_format:"%Y/%m/%d %H:%M:%S"}--></td>
		<td align="center"><a href="ljcms_delimitlabel.php?action=edit&id=<!--{$volist.labelid}-->&page=<!--{$page}-->" class="icon-edit">编辑</a>&nbsp;&nbsp;<a href="ljcms_delimitlabel.php?action=del&id[]=<!--{$volist.labelid}-->" onClick="{if(confirm('确定要删除该信息?')){return true;} return false;}" class="icon-del">删除</a></td>
	  </tr>
	  <!--{foreachelse}-->
      <tr>
	    <td colspan="7" align="center">暂无信息</td>
	  </tr>
	  <!--{/foreach}-->
	  <!--{if $total>0}-->
	  <tr>
		<td align="center"><input name="chkAll" type="checkbox" id="chkAll" onClick="checkAll(this, 'id[]')" value="checkbox"></td>
		<td class="hback" colspan="6"><input class="button" name="btn_del" type="button" value="删 除" onClick="{if(confirm('确定删除选定信息吗!?')){$('#action').val('del');$('#myform').submit();return true;}return false;}" class="button">&nbsp;&nbsp;共[ <b><!--{$total}--></b> ]条记录</td>
	  </tr>
	  <!--{/if}-->
	</table>
	</form>
	<!--{if $pagecount>1}-->
	<table width='95%' border='0' cellspacing='0' cellpadding='0' align='center' style="margin-top:10px;">
	  <tr>
		<td align='center'><!--{$showpage}--></td>
	  </tr>
	</table>
	<!--{/if}-->
  </div>
</div>
<!--{/if}-->

<!--{if $action eq "add"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：其他扩展<span>&gt;&gt;</span>添加标签</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_delimitlabel.php" class="btn-general"><span>返回列表</span></a>添加标签</h3>
    <form name="myform" id="myform" method="post" action="ljcms_delimitlabel.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveadd" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">所属风格模板 <span class='f_red'></span></td>
		<td class='hback' width="85%"><!--{$skin_select}--> <span id="dskinid" class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>标签名称 <span class="f_red">*</span> </td>
		<td class='hback'>&lt!--{$delimit_<input type="text" name="labelname" id="labelname" class="input-txt" />}--&gt;  <span id='dlabelname' class='f_red'></span> (标签名只能字母、数字组成)</td>
	  </tr>
	  <tr>
		<td class='hback_1'>标签描述 <span class="f_red">*</span> </td>
		<td class='hback'><input type="text" name="labeltitle" id="labeltitle" class="input-txt" />  <span id='dlabeltitle' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>状态设置 </td>
		<td class='hback'><!--{$flag_checkbox}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>备注说明</td>
		<td class='hback'><textarea name="intro" id="intro" style='width:60%;height:45px;display:;overflow:auto;'></textarea>  <span id='dintro' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>标签内容</td>
		<td class='hback'>
		  <textarea name="labelcontent" id="labelcontent" style="width:98%;height:280px;display:none;"></textarea>
		  <script>KE.show({id : 'labelcontent' });</script>
		  <br />1、支持config标签，调用config标签时不需要使用起始符“&lt!--”和结束符“--&gt;”<br />
		           如：调用网站名称只需在内容里填写  {$config.sitetitle}<br />
		        2、支持{$skinpath}、{$urlpath}、{$url_频道名}标签。
		</td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="添加保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<!--{/if}-->

<!--{if $action eq "edit"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：其他扩展<span>&gt;&gt;</span>编辑标签</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_delimitlabel.php?<!--{$comeurl}-->" class="btn-general"><span>返回列表</span></a>编辑标签</h3>
    <form name="myform" id="myform" method="post" action="ljcms_delimitlabel.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveedit" />
	<input type="hidden" name="id" value="<!--{$id}-->" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">所属风格模板 <span class='f_red'></span></td>
		<td class='hback' width="85%"><!--{$skin_select}--> <span id="dskinid" class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>标签名称 <span class="f_red">*</span> </td>
		<td class='hback'>{$delimit_<input type="text" name="labelname" id="labelname" class="input-txt" value="<!--{$delimitlabel.labelname}-->" />}  <span id='dlabelname' class='f_red'></span> (标签名只能字母、数字组成)</td>
	  </tr>
	  <tr>
		<td class='hback_1'>标签描述 <span class="f_red">*</span> </td>
		<td class='hback'><input type="text" name="labeltitle" id="labeltitle" class="input-txt" value="<!--{$delimitlabel.labeltitle}-->" />  <span id='dlabeltitle' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>状态设置 </td>
		<td class='hback'><!--{$flag_checkbox}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>备注说明</td>
		<td class='hback'><textarea name="intro" id="intro" style='width:60%;height:45px;display:;overflow:auto;'><!--{$delimitlabel.intro}--></textarea>  <span id='dintro' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>标签内容</td>
		<td class='hback'>
		  <textarea name="labelcontent" id="labelcontent" style="width:98%;height:280px;display:none;"><!--{$delimitlabel.labelcontent}--></textarea>
		  <script>KE.show({id : 'labelcontent' });</script>
		  <br />1、支持config标签，调用config标签时不需要使用起始符“&lt!--”和结束符“--&gt;”<br />
		           如：调用网站名称只需在内容里填写  {$config.sitetitle}<br />
		        2、支持{$skinpath}、{$urlpath}、{$url_频道名}标签。
		</td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="更新保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<!--{/if}-->
</body>
</html>
<script type="text/javascript">
function checkform() {
	var t = "";
	var v = "";

	t = "labelname";
	v = $("#"+t).val();
	if(v=="") {
		dmsg("标签名称不能为空", t);
		return false;
	}
	t = "labeltitle";
	v = $("#"+t).val();
	if(v=="") {
		dmsg("标签描述不能为空", t);
		return false;
	}
	return true;
}
</script>
