<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title>自定义单页</title>
<meta name="author" content="<!--{$copyright_author}-->" />
<link type="text/css" rel="stylesheet" href="liangjingcms/css/admin_style.css" media="screen" />
<script type='text/javascript' src='js/jquery-1.4.4.min.js'></script>
<script type='text/javascript' src='js/command.js'></script>
<script type="text/javascript" src='../data/editor/kindeditor.js'></script>
</head>
<body>
<table class="tableBorder" width="95%" border="0" align="center" cellpadding="5" cellspacing="1">
<tr>
<td width="100%" class="leftrow">
<!--{if $action eq ""}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：其他扩展<span>&gt;&gt;</span>自定义单页</p></div>
  <div class="main-cont">
    <h3 class="title"><a href="ljcms_page.php?action=add" class="btn-general"><span>添加单页</span></a>自定义单页</h3>
	<div class="search-area ">
	  <form method="post" id="search_form" action="ljcms_page.php" />
	  <div class="item">
	    <label>单页分类：</label><!--{$cate_search}-->&nbsp;&nbsp;
		<label>单页名：</label><input type="text" id="sname" name="sname" size="15" class="input" value="<!--{$sname}-->" />&nbsp;&nbsp;&nbsp;
		<input type="submit" class="button_s" value="搜 索" />
	  </div>
	  </form>
	</div>
	<form action="ljcms_page.php" method="post" name="myform" id="myform" style="margin:0">
	<input type="hidden" name="action" id="action" value="del" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <thead class="tb-tit-bg">
	  <tr>
	    <th width="8%"><div class="th-gap">选择</div></th>
		<th width="13%"><div class="th-gap">所属分类</div></th>
		<th width="15%"><div class="th-gap">单页名称</div></th>
		<th width="8%"><div class="th-gap">排序</div></th>
		<th width="6%"><div class="th-gap">状态</div></th>
		<th width="10%"><div class="th-gap">打开方式</div></th>
		<th width="10%"><div class="th-gap">链接类型</div></th>
		<th width="15%"><div class="th-gap">URL</div></th>
		<th><div class="th-gap">操作</div></th>
	  </tr>
	  </thead>
	  <tfoot class="tb-foot-bg"></tfoot>
	  <!--{foreach $volpage as $volist}-->
	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">
	    <td align="center"><input name="id[]" type="checkbox" value="<!--{$volist.pageid}-->" onClick="checkItem(this, 'chkAll')"></td>
		<td align="center"><!--{$volist.catename}--></td>
		<td align="center"><a href="../page.php?mod=detail&id=<!--{$volist.pageid}-->" target="_blank"><!--{$volist.title}--></a></td>
		<td align="center"><!--{$volist.orders}--></td>
		<td align="center">
		<!--{if $volist.flag==0}-->
			<input type="hidden" id="attr_flag<!--{$volist.pageid}-->" value="flagopen" />
			<img id="flag<!--{$volist.pageid}-->" src="liangjingcms/images/no.gif" onClick="javascript:fetch_ajax('flag','<!--{$volist.pageid}-->');" style="cursor:pointer;">
		<!--{else}-->
			<input type="hidden" id="attr_flag<!--{$volist.pageid}-->" value="flagclose" />
			<img id="flag<!--{$volist.pageid}-->" src="liangjingcms/images/yes.gif" onClick="javascript:fetch_ajax('flag','<!--{$volist.pageid}-->');" style="cursor:pointer;">	
		<!--{/if}-->
        </td>
		<td align="center"><!--{if $volist.target==1}--><font color="green">本页</font><!--{else}--><font color="blue">新页面</font><!--{/if}--></td>
		<td align="center"><!--{if $volist.linktype==1}--><font color="green">站内</font><!--{else}--><font color="blue">站外</font><!--{/if}--></td>
		<td align="left"><!--{$volist.linkurl}--></td>
		<td align="center"><a href="ljcms_page.php?action=edit&id=<!--{$volist.pageid}-->&page=<!--{$page}-->&<!--{$urlitem}-->" class="icon-edit">编辑</a>&nbsp;&nbsp;<a href="ljcms_page.php?action=del&id[]=<!--{$volist.pageid}-->" onClick="{if(confirm('确定要删除该信息?')){return true;} return false;}" class="icon-del">删除</a></td>
	  </tr>
	  <!--{foreachelse}-->
      <tr>
	    <td colspan="9" align="center">暂无信息</td>
	  </tr>
	  <!--{/foreach}-->
	  <!--{if $total>0}-->
	  <tr>
		<td align="center"><input name="chkAll" type="checkbox" id="chkAll" onClick="checkAll(this, 'id[]')" value="checkbox"></td>
		<td class="hback" colspan="8"><input class="button" name="btn_del" type="button" value="删 除" onClick="{if(confirm('确定删除选定信息吗!?')){$('#action').val('del');$('#myform').submit();return true;}return false;}" class="button">&nbsp;&nbsp;共[ <b><!--{$total}--></b> ]条记录</td>
	  </tr>
	  <!--{/if}-->
	</table>
	</form>
	<!--{if $pagecount>1}-->
	<table width='95%' border='0' cellspacing='0' cellpadding='0' align='center' style="margin-top:10px;">
	  <tr>
		<td align='center'><!--{$showpage}--></td>
	  </tr>
	</table>
	<!--{/if}-->
  </div>
</div>
<!--{/if}-->

<!--{if $action eq "add"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：其他扩展<span>&gt;&gt;</span>自定义单页</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_page.php" class="btn-general"><span>返回列表</span></a>添加单页</h3>
    <form name="myform" id="myform" method="post" action="ljcms_page.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveadd" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">单页分类 <span class='f_red'>*</span></td>
		<td class='hback' width="85%"><!--{$cate_select}--> <span id="dcateid" class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>单页名称 <span class="f_red">*</span> </td>
		<td class='hback'><input type="text" name="title" id="title" class="input-txt" /> <span id='dtitle' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta标题 </td>
		<td class='hback'><input type="text" name="metatitle" id="metatitle" class="input-txt" />  <span id='dmetatitle' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta关键字 <span class="f_red"></span></td>
		<td class='hback'>
		  <textarea name="metakeyword" id="metakeyword" style="width:70%;height:65px;display:;overflow:auto;"></textarea> <span id='dmetakeyword' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta描述 <span class="f_red"></span></td>
		<td class='hback'>
		  <textarea name="metadescription" id="metadescription" style="width:70%;height:65px;display:;overflow:auto;"></textarea> <span id='dmetakeyword' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>打开方式 <span class="f_red"></span> </td>
		<td class='hback'><input type="radio" name="target" id="target" value="1" checked />本页，<input type="radio" name="target" id="target" value="2" />新页面</td>
	  </tr>
	  <tr>
		<td class='hback_1'>链接类型 <span class="f_red"></span> </td>
		<td class='hback'><input type="radio" name="linktype" id="linktype" value="1" checked />站内，<input type="radio" name="linktype" id="linktype" value="2" />站外</td>
	  </tr>
	  <tr>
		<td class='hback_1'>站外URL <span class="f_red"></span> </td>
		<td class='hback'><input type="text" name="linkurl" id="linkurl" class="input-txt" /> <span id='dlinkurl' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>页面排序 <span class="f_red"></span> </td>
		<td class='hback'><input type="text" name="orders" id="orders" class="input-s" value="<!--{$orders}-->" /> <span id='dorders' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>状态设置 </td>
		<td class='hback'><!--{$flag_checkbox}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>页面内容 <span class="f_red"></span></td>
		<td class='hback'>
		  <textarea name="content" id="content" style="width:98%;height:230px;display:none;"></textarea>
		  <script>KE.show({id : 'content' });</script>  <span id='dcontent' class='f_red'></span>
		  <br />1、支持config标签，调用config标签时不需要使用起始符“&lt!--”和结束符“--&gt;”<br />
		           如：调用网站名称只需在内容里填写  {$config.sitetitle}<br />
		        2、支持{$skinpath}、{$urlpath}、{$url_频道名}标签。		  
		  
		  </td>
	  </tr>

	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="添加保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<!--{/if}-->

<!--{if $action eq "edit"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：其他扩展<span>&gt;&gt;</span>编辑自定义单页</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_page.php?<!--{$comeurl}-->" class="btn-general"><span>返回列表</span></a>编辑单页</h3>
    <form name="myform" id="myform" method="post" action="ljcms_page.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveedit" />
	<input type="hidden" name="id" value="<!--{$id}-->" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">单页分类 <span class='f_red'>*</span></td>
		<td class='hback' width="85%"><!--{$cate_select}--> <span id="dcateid" class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>单页名称 <span class="f_red">*</span> </td>
		<td class='hback'><input type="text" name="title" id="title" class="input-txt" value="<!--{$volpage.title}-->" /> <span id='dtitle' class='f_red'></span></td>
	  </tr>

	  <tr>
		<td class='hback_1'>Meta标题 </td>
		<td class='hback'><input type="text" name="metatitle" id="metatitle" class="input-txt" value="<!--{$volpage.metatitle}-->" />  <span id='dmetatitle' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta关键字 <span class="f_red"></span></td>
		<td class='hback'>
		  <textarea name="metakeyword" id="metakeyword" style="width:70%;height:65px;display:;overflow:auto;"><!--{$volpage.metakeyword}--></textarea> <span id='dmetakeyword' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta描述 <span class="f_red"></span></td>
		<td class='hback'>
		  <textarea name="metadescription" id="metadescription" style="width:70%;height:65px;display:;overflow:auto;"><!--{$volpage.metadescription}--></textarea> <span id='dmetakeyword' class='f_red'></span></td>
	  </tr>

	  <tr>
		<td class='hback_1'>打开方式 <span class="f_red"></span> </td>
		<td class='hback'><input type="radio" name="target" id="target" value="1"<!--{if $volpage.target==1}--> checked<!--{/if}--> />本页，<input type="radio" name="target" id="target" value="2"<!--{if $volpage.target==2}--> checked<!--{/if}--> />新页面</td>
	  </tr>
	  <tr>
		<td class='hback_1'>链接类型 <span class="f_red"></span> </td>
		<td class='hback'><input type="radio" name="linktype" id="linktype" value="1"<!--{if $volpage.linktype==1}--> checked<!--{/if}--> />站内，<input type="radio" name="linktype" id="linktype" value="2"<!--{if $volpage.linktype==2}--> checked<!--{/if}--> />站外</td>
	  </tr>
	  <tr>
		<td class='hback_1'>站外URL <span class="f_red"></span> </td>
		<td class='hback'><input type="text" name="linkurl" id="linkurl" class="input-txt" value="<!--{$volpage.linkurl}-->" /> <span id='dlinkurl' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>页面排序 <span class="f_red"></span> </td>
		<td class='hback'><input type="text" name="orders" id="orders" class="input-s" value="<!--{$volpage.orders}-->" /> <span id='dorders' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>状态设置 </td>
		<td class='hback'><!--{$flag_checkbox}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>页面内容 <span class="f_red"></span></td>
		<td class='hback'>
		  <textarea name="content" id="content" style="width:98%;height:230px;display:none;"><!--{$volpage.content}--></textarea>
		  <script>KE.show({id : 'content' });</script>  <span id='dcontent' class='f_red'></span>
		  
		  <br />1、支持config标签，调用config标签时不需要使用起始符“&lt!--”和结束符“--&gt;”<br />
		           如：调用网站名称只需在内容里填写  {$config.sitetitle}<br />
		        2、支持{$skinpath}、{$urlpath}、{$url_频道名}标签。
				
		  </td>
	  </tr>

	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="更新保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<!--{/if}-->
</td></tr></table>
</body>
</html>
<script type="text/javascript">
function checkform() {
	var t = "";
	var v = "";

	t = "cateid";
	v = $("#"+t).val();
	if(v=="") {
		dmsg("请选择单页分类", t);
		return false;
	}
	t = "title";
	v = $("#"+t).val();
	if(v=="") {
		dmsg("单页名称不能为空", t);
		return false;
	}
    
	/*
	t = 'content';
	v = KE.html(t).length;
	if(v=="") {
		dmsg("页面不能为空", t);
		return false;
	}
	*/
	return true;
}
</script>