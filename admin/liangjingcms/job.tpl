<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title>招聘信息</title>
<meta name="author" content="<!--{$copyright_author}-->" />
<link type="text/css" rel="stylesheet" href="liangjingcms/css/admin_style.css" media="screen" />
<script type='text/javascript' src='js/jquery-1.4.4.min.js'></script>
<script type='text/javascript' src='js/command.js'></script>
<script type="text/javascript" src='../data/editor/kindeditor.js'></script>
</head>
<body>
<table class="tableBorder" width="95%" border="0" align="center" cellpadding="5" cellspacing="1">
<tr>
<td width="100%" class="leftrow">
<!--{if $action eq ""}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：其他扩展<span>&gt;&gt;</span>招聘信息</p></div>
  <div class="main-cont">
    <h3 class="title"><a href="ljcms_job.php?action=add" class="btn-general"><span>发布招聘</span></a>招聘信息</h3>
	<div class="search-area ">
	  <form method="post" id="search_form" action="ljcms_job.php" />
	  <div class="item">
	    <label>招聘分类：</label><!--{$cate_search}-->&nbsp;&nbsp;
		<label>职位名：</label><input type="text" id="sname" name="sname" size="15" class="input" value="<!--{$sname}-->" />&nbsp;&nbsp;&nbsp;
		<input type="submit" class="button_s" value="搜 索" />
	  </div>
	  </form>
	</div>
	<form action="ljcms_job.php" method="post" name="myform" id="myform" style="margin:0">
	<input type="hidden" name="action" id="action" value="del" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <thead class="tb-tit-bg">
	  <tr>
	    <th width="8%"><div class="th-gap">选择</div></th>
		<th width="15%"><div class="th-gap">所在分类</div></th>
		<th width="20%"><div class="th-gap">招聘职位</div></th>
		<th width="10%"><div class="th-gap">工作地点</div></th>
		<th width="10%"><div class="th-gap">招聘人数</div></th>
		<th width="10%"><div class="th-gap">状态</div></th>
		<th width="12%"><div class="th-gap">录入时间</div></th>
		<th><div class="th-gap">操作</div></th>
	  </tr>
	  </thead>
	  <tfoot class="tb-foot-bg"></tfoot>
	  <!--{foreach $job as $volist}-->
	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">
	    <td align="center"><input name="id[]" type="checkbox" value="<!--{$volist.jobid}-->" onClick="checkItem(this, 'chkAll')"></td>
		<td align="center"><!--{$volist.catename}--></td>
		<td align="left"><a href="../job.php?mod=detail&id=<!--{$volist.jobid}-->" target="_blank"><!--{$volist.title}--></a></td>
		<td align="center"><!--{$volist.workarea}--></td>
		<td align="center"><!--{$volist.number}--></td>
		<td align="center">
		<!--{if $volist.flag==0}-->
			<input type="hidden" id="attr_flag<!--{$volist.jobid}-->" value="flagopen" />
			<img id="flag<!--{$volist.jobid}-->" src="liangjingcms/images/no.gif" onClick="javascript:fetch_ajax('flag','<!--{$volist.jobid}-->');" style="cursor:pointer;">
		<!--{else}-->
			<input type="hidden" id="attr_flag<!--{$volist.jobid}-->" value="flagclose" />
			<img id="flag<!--{$volist.jobid}-->" src="liangjingcms/images/yes.gif" onClick="javascript:fetch_ajax('flag','<!--{$volist.jobid}-->');" style="cursor:pointer;">	
		<!--{/if}-->
        </td>
		<td><!--{$volist.timeline|date_format:"%Y/%m/%d"}--></td>
		<td align="center"><a href="ljcms_job.php?action=edit&id=<!--{$volist.jobid}-->&page=<!--{$page}-->&<!--{$urlitem}-->" class="icon-edit">编辑</a>&nbsp;&nbsp;<a href="ljcms_job.php?action=del&id[]=<!--{$volist.jobid}-->" onClick="{if(confirm('确定要删除该信息?')){return true;} return false;}" class="icon-del">删除</a></td>
	  </tr>
	  <!--{foreachelse}-->
      <tr>
	    <td colspan="8" align="center">暂无信息</td>
	  </tr>
	  <!--{/foreach}-->
	  <!--{if $total>0}-->
	  <tr>
		<td align="center"><input name="chkAll" type="checkbox" id="chkAll" onClick="checkAll(this, 'id[]')" value="checkbox"></td>
		<td class="hback" colspan="7"><input class="button" name="btn_del" type="button" value="删 除" onClick="{if(confirm('确定删除选定信息吗!?')){$('#action').val('del');$('#myform').submit();return true;}return false;}" class="button">&nbsp;&nbsp;共[ <b><!--{$total}--></b> ]条记录</td>
	  </tr>
	  <!--{/if}-->
	</table>
	</form>
	<!--{if $pagecount>1}-->
	<table width='95%' border='0' cellspacing='0' cellpadding='0' align='center' style="margin-top:10px;">
	  <tr>
		<td align='center'><!--{$showpage}--></td>
	  </tr>
	</table>
	<!--{/if}-->
  </div>
</div>
<!--{/if}-->

<!--{if $action eq "add"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：其他扩展<span>&gt;&gt;</span>发布招聘</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_job.php" class="btn-general"><span>返回列表</span></a>发布招聘</h3>
    <form name="myform" id="myform" method="post" action="ljcms_job.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveadd" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">招聘分类 <span class='f_red'>*</span></td>
		<td class='hback' width="85%"><!--{$cate_select}--> <span id="dcateid" class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>招聘职位 <span class="f_red">*</span> </td>
		<td class='hback'><input type="text" name="title" id="title" class="input-txt" /> <span id='dtitle' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>招聘人数 <span class="f_red"></span> </td>
		<td class='hback'><input type="text" name="number" id="number" class="input-s" /> （填写数字） 人 <span id='dnumber' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>工作地点 <span class="f_red"></span> </td>
		<td class='hback'><input type="text" name="workarea" id="workarea" class="input-s" /> <span id='dworkarea' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>状态设置 </td>
		<td class='hback'><!--{$flag_checkbox}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>岗位职责 <span class="f_red">*</span></td>
		<td class='hback'>
		  <textarea name="jobdescription" id="jobdescription" style="width:98%;height:200px;display:none;"></textarea>
		  <script>KE.show({id : 'jobdescription' });</script>  <span id='djobdescription' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>职位要求 <span class="f_red">*</span></td>
		<td class='hback'>
		  <textarea name="jobrequest" id="jobrequest" style="width:98%;height:200px;display:none;"></textarea>
		  <script>KE.show({id : 'jobrequest' });</script>  <span id='djobrequest' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>其他要求 <span class="f_red"></span></td>
		<td class='hback'>
		  <textarea name="jobotherrequest" id="jobotherrequest" style="width:98%;height:200px;display:none;"></textarea>
		  <script>KE.show({id : 'jobotherrequest' });</script>  <span id='djobotherrequest' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>联系方式 </td>
		<td class='hback'><input type="text" name="jobcontact" id="jobcontact" class="input-txt" />  <span id='djobcontact' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="添加保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<!--{/if}-->

<!--{if $action eq "edit"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：其他扩展<span>&gt;&gt;</span>编辑招聘信息</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_job.php?<!--{$comeurl}-->" class="btn-general"><span>返回列表</span></a>编辑招聘信息</h3>
    <form name="myform" id="myform" method="post" action="ljcms_job.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveedit" />
	<input type="hidden" name="id" value="<!--{$id}-->" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">招聘分类 <span class='f_red'>*</span></td>
		<td class='hback' width="85%"><!--{$cate_select}--> <span id="dcateid" class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>招聘职位 <span class="f_red">*</span> </td>
		<td class='hback'><input type="text" name="title" id="title" class="input-txt" value="<!--{$job.title}-->" /> <span id='dtitle' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>招聘人数 <span class="f_red"></span> </td>
		<td class='hback'><input type="text" name="number" id="number" class="input-s" value="<!--{$job.number}-->" /> （填写数字） 人 <span id='dnumber' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>工作地点 <span class="f_red"></span> </td>
		<td class='hback'><input type="text" name="workarea" id="workarea" class="input-s" value="<!--{$job.workarea}-->" /> <span id='dworkarea' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>状态设置 </td>
		<td class='hback'><!--{$flag_checkbox}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>岗位职责 <span class="f_red">*</span></td>
		<td class='hback'>
		  <textarea name="jobdescription" id="jobdescription" style="width:98%;height:200px;display:none;"><!--{$job.jobdescription}--></textarea>
		  <script>KE.show({id : 'jobdescription' });</script>  <span id='djobdescription' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>职位要求 <span class="f_red">*</span></td>
		<td class='hback'>
		  <textarea name="jobrequest" id="jobrequest" style="width:98%;height:200px;display:none;"><!--{$job.jobrequest}--></textarea>
		  <script>KE.show({id : 'jobrequest' });</script>  <span id='djobrequest' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>其他要求 <span class="f_red"></span></td>
		<td class='hback'>
		  <textarea name="jobotherrequest" id="jobotherrequest" style="width:98%;height:200px;display:none;"><!--{$job.jobotherrequest}--></textarea>
		  <script>KE.show({id : 'jobotherrequest' });</script>  <span id='djobotherrequest' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>联系方式 </td>
		<td class='hback'><input type="text" name="jobcontact" id="jobcontact" class="input-txt" value="<!--{$job.jobcontact}-->" />  <span id='djobcontact' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="更新保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<!--{/if}-->
</td></tr></table>
</body>
</html>
<script type="text/javascript">
function checkform() {
	var t = "";
	var v = "";

	t = "cateid";
	v = $("#"+t).val();
	if(v=="") {
		dmsg("请选择招聘分类", t);
		return false;
	}
	t = "title";
	v = $("#"+t).val();
	if(v=="") {
		dmsg("招聘职位不能为空", t);
		return false;
	}

	t = 'jobdescription';
	v = KE.html(t).length;
	if(v=="") {
		dmsg("岗位职责不能为空", t);
		return false;
	}

	t = 'jobrequest';
	v = KE.html(t).length;
	if(v=="") {
		dmsg("职位要求不能为空", t);
		return false;
	}
	return true;
}
</script>