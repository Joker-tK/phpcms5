<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title>站点设置</title>
<meta name="author" content="<!--{$copyright_author}-->" />
<link type="text/css" rel="stylesheet" href="liangjingcms/css/admin_style.css" />
<script type='text/javascript' src='js/jquery-1.4.4.min.js'></script>
<script type='text/javascript' src='js/command.js'></script>
<script type="text/javascript" src='../data/editor/kindeditor.js'></script>
</head>
<body>
<table class="tableBorder" width="95%" border="0" align="center" cellpadding="5" cellspacing="1">
<tr>
<td width="100%" class="leftrow">
<!--{if $action eq ""}-->

<div class="main-wrap">
  <form name="myform" method="post" action="ljcms_setting.php" />
  <input type="hidden" name="action" value="savesetting" />
  <div class="path"><p>当前位置：系统设置<span>&gt;&gt;</span>站点设置</p></div>
  <div class="main-cont">
	<h3 class="title">站点信息设置</h3>
	<div class="set-area">
	  <div class="form web-info-form">
		<div class="form-row">
		<label class="form-field">网站名称</label>
		<div class="form-cont"><input name="sitename" id="sitename" class="input-txt" type="text" value="<!--{$config.sitename}-->" /><span id="dsitename"></span></div>
		</div>
		<div class="form-row">
		  <label class="form-field">网站地址</label>
		  <div class="form-cont"><input name="siteurl" id="siteurl" class="input-txt" type="text" value="<!--{$config.siteurl}-->" /><span id="dsiteurl"></span><p class="form-tips">（以“http://”开头，“/”结束）</p></div>
		</div>
		<div class="form-row">
		  <label class="form-field">备案号码</label>
		  <div class="form-cont"><input name="icpcode" id="icpcode" class="input-txt" type="text" value="<!--{$config.icpcode}-->" /><span id="dicpcode"></span><p class="form-tips">（网站备案信息将显示在页面底部）</p></div>
		</div>

		<div class="form-row">
		  <label for="declare" class="form-field">首页视频代码</label>
		  <div class="form-cont"><textarea name="indexvideo" id="indexvideo" class="input-area area-s4 code-area" style="background-color:#ffffff;width:500px;height:90px;"><!--{$config.indexvideo}--></textarea></div>
		</div>
		<div class="form-row">
		  <label for="declare" class="form-field">流量统计代码</label>
		  <div class="form-cont"><textarea name="tjcode" id="tjcode" class="input-area area-s4 code-area" style="background-color:#ffffff;width:500px;height:60px;"><!--{$config.tjcode}--></textarea></div>
		</div>
		<div class="form-row">
		  <label for="declare" class="form-field">下载搜索顶部代码</label>
		  <div class="form-cont"><textarea name="downsearchtop" id="downsearchtop" class="input-area area-s4 code-area" style="background-color:#ffffff;width:500px;height:60px;"><!--{$config.downsearchtop}--></textarea></div>
		</div>
		<div class="form-row">
		  <label for="declare" class="form-field">下载搜索底部代码</label>
		  <div class="form-cont"><textarea name="downsearchbottom" id="downsearchbottom" class="input-area area-s4 code-area" style="background-color:#ffffff;width:500px;height:60px;"><!--{$config.downsearchbottom}--></textarea></div>
		</div>

		<div class="form-row">
		  <label class="form-field">没有找到下载搜索信息</label>
		  <div class="form-cont"><input name="nocontent" id="nocontent" class="input-txt" type="text" value="<!--{$config.nocontent}-->" /><span id="nocontent"></span></div>
		</div>
		<div class="form-row">
		  <label for="declare" class="form-field" style="height:200px;">网站版权信息</label>
		  <div class="form-cont" style="width:736px;float:left;" ><textarea name="sitecopyright" id="sitecopyright" style="width:85%;height:200px;display:none;">
		  <!--{$config.sitecopyright}--></textarea><script>KE.show({id : 'sitecopyright' });</script></div>
		</div>
		<div class="form-row">
		  <label for="declare" class="form-field" >左侧广告图是否开启</label>
		  <div class="form-cont" style="width:736px;float:left;" >
<input type="radio" name="adstatus" id="adstatus" value="1"<!--{if $config.adstatus==1}--> checked<!--{/if}--> />开启，<input type="radio" name="adstatus" id="adstatus" value="0"<!--{if $config.adstatus==0}--> checked<!--{/if}--> />关闭
		  </div>
		</div>
		<div class="form-row">
		 
		  <label class="form-field">左侧广告图片</label>
		  <div class="form-cont">
		  	<input type="hidden" name="adsyspic" id="adsyspic" value="<!--{$config.adsyspic}-->" /><span id='dadsyspic' class='f_red'></span>
			<!--{if $config.adsyspic!=""}-->
			<iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?action=show&comeform=myform&inputid=adsyspic&thumbflag=0&picname=<!--{$adimgname}-->&picurl=<!--{$config.adsyspic}-->'></iframe>
			<!--{else}-->
			<iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?comeform=myform&inputid=adsyspic&thumbflag=0'></iframe>
			<!--{/if}-->
		  </div>
		</div>
		<div class="form-row">
		  <label class="form-field">左侧广告连接地址</label>
		  <div class="form-cont"><input name="adlink" id="adlink" class="input-txt" type="text" value="<!--{$config.adlink}-->" /><span id="dadlink"></span></div>
		</div>
	  </div>
	</div>
    
	<h3 class="title">请选择需要在网站中使用的LOGO图案</h3>
	<div class="set-area">
	  <div class="form web-info-form" style="background:#bfe8f2">
	    <div class="form-row">
		  <label class="form-field">选择图片</label>
		  <div class="form-cont">
		  	<input type="hidden" name="logoimg" id="logoimg" value="<!--{$config.logoimg}-->" /><span id='dlogoimg' class='f_red'></span>
			<!--{if $config.logoimg!=""}-->
			<iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?action=show&comeform=myform&inputid=logoimg&thumbflag=0&picname=<!--{$logoimgname}-->&picurl=<!--{$config.logoimg}-->'></iframe>
			<!--{else}-->
			<iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?comeform=myform&inputid=logoimg&thumbflag=0'></iframe>
			<!--{/if}-->
		  </div>
		</div>
		<div class="form-row">
		  <label class="form-field">图片大小</label>
		  <div class="form-cont">宽：<input name="logowidth" id="logowidth" type="text" size="5" value="<!--{$config.logowidth}-->" />px；高：<input name="logoheight" id="logoheight" type="text" size="5" value="<!--{$config.logoheight}-->" />px</div>
		</div>
		<div class="form-row logo_preview">
		  <label for="upload_file" class="form-field">效果预览</label>
		  <div class="form-cont"><!--{if $config.logoimg!=""}--><img id="logo_preview" src="../<!--{$config.logoimg}-->" /><!--{/if}--></div>
		</div>
		<div class="btn-area" style="background:#bfe8f2"><input type="submit" name="btn_save" class="button" value="更新保存" /></div>
	  </div>
	</div>
  </div>
  </form>
</div>
<!--{/if}-->

<!--{if $action eq "about"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：系统设置<span>&gt;&gt;</span>关于我们</p></div>
  <div class="main-cont">
	<h3 class="title">关于我们</h3>
    <form name="myform" method="post" action="ljcms_setting.php" />
    <input type="hidden" name="action" value="saveabout" />
	<table cellpadding='2' cellspacing='1' class='tab'>
	  <tr>
		<td class='hback_none' width='15%'>内容介绍：</td>
		<td class='hback_none' width='85%'>
		  <textarea name="content" id="content" style="width:98%;height:300px;display:none;"><!--{$config.about}--></textarea>
		  <script>KE.show({id : 'content' });</script>
		</td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="更新保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<!--{/if}-->

<!--{if $action eq "contact"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：系统设置<span>&gt;&gt;</span>联系我们</p></div>
  <div class="main-cont">
	<h3 class="title">联系我们</h3>
    <form name="myform" method="post" action="ljcms_setting.php" />
    <input type="hidden" name="action" value="savecontact" />
	<table cellpadding='2' cellspacing='1' class='tab'>
	  <tr>
		<td class='hback_none' width='15%'>内容介绍：</td>
		<td class='hback_none' width='85%'>
		  <textarea name="content" id="content" style="width:98%;height:300px;display:none;"><!--{$config.contact}--></textarea>
		  <script>KE.show({id : 'content' });</script>
		</td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="更新保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<!--{/if}-->

<!--{if $action eq "config"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：系统设置<span>&gt;&gt;</span>参数设置</p></div>
  <div class="main-cont">
	<h3 class="title">页面显示参数设置</h3>
    <form name="myform" method="post" action="ljcms_setting.php" />
    <input type="hidden" name="action" value="saveconfig" />
	<table cellpadding='5' cellspacing='5' class='tab'>
	  <tr>
		<td class='hback_none' width="10%">新闻资讯>></td>
		<td class='hback_none' width="90%">每页 <input type='text' name='newspagesize' id='newspagesize' size='5' value="<!--{$config.newspagesize}-->" /> <span id='dnewspagesize' class='f_red'></span>&nbsp;&nbsp;&nbsp;&nbsp;最新：<input type='text' name='newsnum' id='newsnum' size='5' value="<!--{$config.newsnum}-->" /> <span id='dnewsnum' class='f_red'></span>&nbsp;&nbsp;标题长度：<input type='text' name='newslen' id='newslen' size='5' value="<!--{$config.newslen}-->" /> <span id='dnewslen' class='f_red'></span></td>
	  </tr>

	  <tr>
		<td class='hback_none'>产品展示>> </td>
		<td class='hback_none'>每页 <input type='text' name='productpagesize' id='productpagesize' size='5' value="<!--{$config.productpagesize}-->" />  &nbsp;&nbsp;&nbsp;&nbsp;最新：<input type='text' name='productnum' id='productnum' size='5' value="<!--{$config.productnum}-->" /> &nbsp;&nbsp;标题长度：<input type='text' name='productlen' id='productlen' size='5' value="<!--{$config.productlen}-->" /> </td>
	  </tr>
	  <tr>
		<td class='hback_none'>成功案例>> </td>
		<td class='hback_none'>每页 <input type='text' name='casepagesize' id='casepagesize' size='5' value="<!--{$config.casepagesize}-->" />  &nbsp;&nbsp;&nbsp;&nbsp;最新：<input type='text' name='casenum' id='casenum' size='5' value="<!--{$config.casenum}-->" /> &nbsp;&nbsp;标题长度：<input type='text' name='caselen' id='caselen' size='5' value="<!--{$config.caselen}-->" /></td>
	  </tr>
	  <tr>
		<td class='hback_none'>解决方案>> </td>
		<td class='hback_none'>每页 <input type='text' name='solutionpagesize' id='solutionpagesize' size='5' value="<!--{$config.solutionpagesize}-->" />  &nbsp;&nbsp;&nbsp;&nbsp;最新：<input type='text' name='solutionnum' id='solutionnum' size='5' value="<!--{$config.solutionnum}-->" /> &nbsp;&nbsp;标题长度：<input type='text' name='solutionlen' id='solutionlen' size='5' value="<!--{$config.solutionlen}-->" /></td>
	  </tr>
	  <tr>
		<td class='hback_none'>下载中心>> </td>
		<td class='hback_none'>每页 <input type='text' name='downpagesize' id='downpagesize' size='5' value="<!--{$config.downpagesize}-->" />  &nbsp;&nbsp;&nbsp;&nbsp;最新：<input type='text' name='downnum' id='downnum' size='5' value="<!--{$config.downnum}-->" /> &nbsp;&nbsp;标题长度：<input type='text' name='downlen' id='downlen' size='5' value="<!--{$config.downlen}-->" /></td>
	  </tr>
	  <tr>
		<td class='hback_none'>人才招聘>> </td>
		<td class='hback_none'>每页 <input type='text' name='jobpagesize' id='jobpagesize' size='5' value="<!--{$config.jobpagesize}-->" />  &nbsp;&nbsp;&nbsp;&nbsp;最新：<input type='text' name='jobnum' id='jobnum' size='5' value="<!--{$config.jobnum}-->" /> &nbsp;&nbsp;标题长度：<input type='text' name='joblen' id='joblen' size='5' value="<!--{$config.joblen}-->" /></td>
	  </tr>
	  <tr>
		<td class='hback_none'>推荐产品>> </td>
		<td class='hback_none'>推荐数量：<input type='text' name='eliteproductnum' id='eliteproductnum' size='5' value="<!--{$config.eliteproductnum}-->" /> &nbsp;&nbsp;标题长度：<input type='text' name='eliteproductlen' id='eliteproductlen' size='5' value="<!--{$config.eliteproductlen}-->" /></td>
	  </tr>
	</table>
	<h3 class="title">在线QQ客服</h3>
	<table cellpadding='5' cellspacing='5' class='tab'>
	  <tr>
		<td class='hback_none' width="10%"></td>
		<td class='hback_none' width="90%"><input type="radio" name="qqstatus" id="qqstatus" value="1"<!--{if $config.qqstatus==1}--> checked<!--{/if}--> />开启，<input type="radio" name="qqstatus" id="qqstatus" value="0"<!--{if $config.qqstatus==0}--> checked<!--{/if}--> />关闭</td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="更新保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<!--{/if}-->

<!--{if $action eq "upload"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：系统设置<span>&gt;&gt;</span>图片设置</p></div>
  <div class="main-cont">
	<h3 class="title">上传图片设置，本功能需要PHP环境支持GD才生效
缩略图按原图比例缩小，宽高不会超过本设定，但都不能小于60px</h3>
    <form name="myform" method="post" action="ljcms_setting.php" />
    <input type="hidden" name="action" value="saveupload" />
	<table cellpadding='5' cellspacing='5' class='tab'>
	  <tr>
		<td class='hback_1' width="20%">图片最大尺寸： </td>
		<td class='hback' width="80%">宽：<input type="text" name="maxthumbwidth" id="maxthumbwidth" size="5" value="<!--{$config.maxthumbwidth}-->" /> 像素（px）  高：<input type="text" name="maxthumbheight" id="maxthumbheight" size="5" value="<!--{$config.maxthumbheight}-->" /> 像素（px）<br />
	如果用户上传一些尺寸很大的数码图片，则程序会按照本设置进行缩小该图片并显示，<br />比如可以设置为 宽：1024px，高：768px，但都不能小于300px。设置为0，则不做任何处理。</td>
	  </tr>

	  <tr>
		<td class='hback_1'>默认预览缩略图大小： </td>
		<td class='hback'>宽：<input type="text" name="thumbwidth" id="thumbwidth" size="5" value="<!--{$config.thumbwidth}-->" /> 像素（px） ， 高：<input type="text" name="thumbheight" id="thumbheight" size="5" value="<!--{$config.thumbheight}-->" /> 像素（px）</td>
	  </tr>
	  <tr>
		<td class='hback_1'>产品展示缩略图大小： </td>
		<td class='hback'>宽：<input type="text" name="productthumbwidth" id="productthumbwidth" size="5" value="<!--{$config.productthumbwidth}-->" /> 像素（px） ， 高：<input type="text" name="productthumbheight" id="productthumbheight" size="5" value="<!--{$config.productthumbheight}-->" /> 像素（px）</td>
	  </tr>
	  <tr>
		<td class='hback_1'>成功案例缩略图大小： </td>
		<td class='hback'>宽：<input type="text" name="casethumbwidth" id="casethumbwidth" size="5" value="<!--{$config.casethumbwidth}-->" /> 像素（px） ， 高：<input type="text" name="casethumbheight" id="casethumbheight" size="5" value="<!--{$config.casethumbheight}-->" /> 像素（px）</td>
	  </tr>
	  <tr>
		<td class='hback_1'>解决方案缩略图大小： </td>
		<td class='hback'>宽：<input type="text" name="solutionthumbwidth" id="solutionthumbwidth" size="5" value="<!--{$config.solutionthumbwidth}-->" /> 像素（px） ， 高：<input type="text" name="solutionthumbheight" id="solutionthumbheight" size="5" value="<!--{$config.solutionthumbheight}-->" /> 像素（px）</td>
	  </tr> 
	</table>
	<h3 class="title">图片水印</h3>
	<table cellpadding='5' cellspacing='5' class='tab'>
	  <tr>
		<td class="hback_1" width="20%">是否启用： </td>
		<td class="hback" width="80%"><input type="radio" name="watermarkflag" value="1"<!--{if $config.watermarkflag==1}--> checked<!--{/if}--> />是，<input type="radio" name="watermarkflag" value="0"<!--{if $config.watermarkflag==0}--> checked<!--{/if}--> />否</td>
	  </tr>
	  <tr>
		<td class="hback_1">水印图片地址： </td>
		<td class="hback"><input type="text" name="watermarkfile" id="watermarkfile" size="45" value="<!--{$config.watermarkfile}-->" /> <br />默认为tpl/static/images/watermark.png，只支持JPG/GIF/PNG格式，推荐用透明的png图片 </td>
	  </tr>
	  <tr>
		<td class="hback_1">水印位置： </td>
		<td class="hback"><input type="radio" name="watermarkpos" value="1"<!--{if $config.watermarkpos==1}--> checked<!--{/if}--> />顶端居左 <input type="radio" name="watermarkpos" value="2"<!--{if $config.watermarkpos==2}--> checked<!--{/if}--> />顶端居右 <input type="radio" name="watermarkpos" value="3"<!--{if $config.watermarkpos==3}--> checked<!--{/if}--> />底端居左 <input type="radio" name="watermarkpos" value="4"<!--{if $config.watermarkpos==4}--> checked<!--{/if}--> />底端居右  <input type="radio" name="watermarkpos" value="0"<!--{if $config.watermarkpos==0}--> checked<!--{/if}--> />随机</td>
	  </tr>

	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="更新保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<!--{/if}-->

<!--{if $action eq "seo"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：系统设置<span>&gt;&gt;</span>站点SEO设置</p></div>
  <div class="main-cont">
	<h3 class="title">站点SEO设置</h3>
    <form name="myform" method="post" action="ljcms_setting.php" />
    <input type="hidden" name="action" value="saveseo" />
	<table cellpadding='2' cellspacing='1' class='tab'>
	  <tr>
		<td class="hback_none" width="15%">SEO标题： </td>
		<td class="hback" width="85%">&nbsp;&nbsp;<input type="text" name="sitetitle" id="sitetitle" size="45" value="<!--{$config.sitetitle}-->" /></td>
	  </tr>
	  <tr>
		<td class='hback_none'>Meta描述：</td>
		<td class='hback_none'><textarea name="metadescription" id="metadescription" style="width:50%;height:80px;display:;overflow:auto;"><!--{$config.metadescription}--></textarea></td>
	  </tr>
	  <tr>
		<td class='hback_none'>Meta关键字：</td>
		<td class='hback_none'><textarea name="metakeyword" id="metakeyword" style="width:50%;height:80px;display:;overflow:auto;"><!--{$config.metakeyword}--></textarea></td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="更新保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<!--{/if}-->

<!--{if $action eq "cache"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：系统设置<span>&gt;&gt;</span>站点缓存优化</p></div>
  <div class="main-cont">
	<h3 class="title">站点缓存优化（如果网站不常更新，建议开启缓存，以提高站点访问速度）</h3>
    <form name="myform" method="post" action="ljcms_setting.php" />
    <input type="hidden" name="action" value="savecache" />
	<table cellpadding='5' cellspacing='5' class='tab'>
	  <tr>
		<td class="hback_none" width="15%">是否开启缓存： </td>
		<td class="hback" width="85%"><input type="radio" name="cachstatus" value="1"<!--{if $config.cachstatus==1}--> checked<!--{/if}--> />开启，<input type="radio" name="cachstatus" value="0"<!--{if $config.cachstatus==0}--> checked<!--{/if}--> />关闭</td>
	  </tr>
	  <tr>
		<td class='hback_none'>缓存持续时间：</td>
		<td class='hback_none'><input type="text" name="cachtime" id="cachtime" size="10" value="<!--{$config.cachtime}-->" /> 分钟</td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="更新保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<!--{/if}-->

<!--{if $action eq "rewrite"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：系统设置<span>&gt;&gt;</span>站点伪静态设置</p></div>
  <div class="main-cont">
	<h3 class="title">站点伪静态设置（开启Rewrite功能会将URL静态化，提高搜索引擎的抓取）</h3>
    <form name="myform" method="post" action="ljcms_setting.php" />
    <input type="hidden" name="action" value="saverewrite" />
	<table cellpadding='5' cellspacing='5' class='tab'>
	  <tr>
		<td class="hback_none" width="20%">页面访问方式： </td>
		<td class="hback_none" width="80%"><input type="radio" name="htmltype" value="php"<!--{if $config.htmltype=='php'}--> checked<!--{/if}--> />PHP动态页，<input type="radio" name="htmltype" value="rewrite"<!--{if $config.htmltype=='rewrite'}--> checked<!--{/if}--> />HTML伪静态 <br /><font color="red">(温馨提示：选择伪静态时，空间必须支持伪静态功能。)</font><br />
		Apache下的伪静态正则表达式请看网站目录下的.htaccess<br />IIS下的伪静态正则表达式请看网站目录下的httpd.ini。</td>
	  </tr>
	  <tr>
		<td class="hback_none">伪静态URL路由模式： </td>
		<td class="hback_none"><input type="radio" name="routeurltype" value="1"<!--{if $config.routeurltype==1}--> checked<!--{/if}--> />模式一，<input type="radio" name="routeurltype" value="2"<!--{if $config.routeurltype==2}--> checked<!--{/if}--> />模式二<br /><font color="#666666">模式一：URL路由显示一层目录；http://www.liangjing.org/product-123.html<br />模式二：URL路由包含二级目录；http://www.liangjing.org/product/123.html</font></td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="更新保存" /></td>
	  </tr>
	</table>
	<h3 class="title">Apache服务器配置Rewrite规则：</h3>
	<table cellpadding='5' cellspacing='5' class='tab'>
	  <td style="color:#666666;line-height:20px;">
		1、确保Apache安装了Rewite模块，并将Apache配置文件中的AllowOverride设置为All；<br />
		2、找到站点根目录下的.htaccess文件，将“RewriteEngine off”前面的#号去掉，并改为“RewriteEngine on”；<br />
		3、在本设置中开启该功能；<br />
		4、如果需要在本设置中关闭该功能，请将.htaccess文件中相关代码用#号注释掉；<br />
		5、为了避免报错的情况下请删除httpd.ini多的那于的那个模式<br/>
	  </td>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<!--{/if}-->
</td></tr></table>
</body>
</html>
