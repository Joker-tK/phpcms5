<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title>下载分类</title>
<meta name="author" content="<!--{$copyright_author}-->" />
<link type="text/css" rel="stylesheet" href="liangjingcms/css/admin_style.css" media="screen" />
<script type='text/javascript' src='js/jquery-1.4.4.min.js'></script>
<script type='text/javascript' src='js/command.js'></script>
<script type="text/javascript" src='../data/editor/kindeditor.js'></script>
</head>
<body>
<!--{if $action eq ""}-->
<div class="main-wrap">





  <div class="path"><p>当前位置：内容管理<span>&gt;&gt;</span>下载分类</p></div>
  <div class="main-cont">
    <h3 class="title"><a href="ljcms_downloadcate.php?action=add" class="btn-general"><span>添加分类</span></a>下载分类</h3>
	<form action="ljcms_downloadcate.php" method="post" name="myform" id="myform" style="margin:0">
	<input type="hidden" name="action" id="action" value="del" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <thead class="tb-tit-bg">
	  <tr>
	    <th width="8%"><div class="th-gap">选择</div></th>
		<th width="15%"><div class="th-gap">分类名称</div></th>
		<th width="10%"><div class="th-gap">图标</div></th>
		<th width="8%"><div class="th-gap">CSS</div></th>
		<th width="8%"><div class="th-gap">链接</div></th>
		<th width="8%"><div class="th-gap">排序</div></th>
		<th width="8%"><div class="th-gap">状态</div></th>
		<th width="10%"><div class="th-gap">下载</div></th>
		<th width="12%"><div class="th-gap">录入时间</div></th>
		<th><div class="th-gap">操作</div></th>
	  </tr>
	  </thead>
	  <tfoot class="tb-foot-bg"></tfoot>
	  <!--{foreach $cate as $volist}-->
	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">
	    <td align="center"><input name="id[]" type="checkbox" value="<!--{$volist.cateid}-->" onClick="checkItem(this, 'chkAll')"></td>
		<td><!--{$volist.catename}--></td>
		<td align="center"><!--{if $volist.img!=''}--><img src="../<!--{$volist.img}-->" border="0" /><!--{else}--><font color="#999999">无图标</font><!--{/if}--></td>
		<td align="center"><!--{$volist.cssname}--></td>
		<td align="center"><!--{if $volist.linktype==1}--><font color="green">内部</font><!--{else}--><font color="blue">外部</font><!--{/if}--></td>
		<td align="center"><!--{$volist.orders}--></td>
		<td align="center">
		<!--{if $volist.flag==0}-->
			<input type="hidden" id="attr_flag<!--{$volist.cateid}-->" value="flagopen" />
			<img id="flag<!--{$volist.cateid}-->" src="liangjingcms/images/no.gif" onClick="javascript:fetch_ajax('flag','<!--{$volist.cateid}-->');" style="cursor:pointer;">
		<!--{else}-->
			<input type="hidden" id="attr_flag<!--{$volist.cateid}-->" value="flagclose" />
			<img id="flag<!--{$volist.cateid}-->" src="liangjingcms/images/yes.gif" onClick="javascript:fetch_ajax('flag','<!--{$volist.cateid}-->');" style="cursor:pointer;">	
		<!--{/if}-->
		</td>
		<td align="center"><!--{$volist.downloadcount}--></td>
		<td align="center"><!--{$volist.timeline|date_format:"%Y/%m/%d"}--></td>
		<td align="center"><a href="ljcms_downloadcate.php?action=edit&id=<!--{$volist.cateid}-->&page=<!--{$page}-->" class="icon-set">设置</a>&nbsp;&nbsp;<a href="ljcms_downloadcate.php?action=del&id[]=<!--{$volist.cateid}-->" onClick="{if(confirm('确定要删除该信息?')){return true;} return false;}" class="icon-del">删除</a></td>
	  </tr>
	  <!--{foreachelse}-->
      <tr>
	    <td colspan="10" align="center">暂无信息</td>
	  </tr>
	  <!--{/foreach}-->
	  <!--{if $total>0}-->
	  <tr>
		<td align="center"><input name="chkAll" type="checkbox" id="chkAll" onClick="checkAll(this, 'id[]')" value="checkbox"></td>
		<td class="hback" colspan="9"><input class="button" name="btn_del" type="button" value="删 除" onClick="{if(confirm('确定删除选定信息吗!?')){$('#action').val('del');$('#myform').submit();return true;}return false;}" class="button">&nbsp;&nbsp;共[ <b><!--{$total}--></b> ]条记录</td>
	  </tr>
	  <!--{/if}-->
	</table>
	</form>
	<!--{if $pagecount>1}-->
	<table width='95%' border='0' cellspacing='0' cellpadding='0' align='center' style="margin-top:10px;">
	  <tr>
		<td align='center'><!--{$showpage}--></td>
	  </tr>
	</table>
	<!--{/if}-->
  </div>
</div>
<!--{/if}-->

<!--{if $action eq "setting"}-->



<div class="main-wrap">
  <div class="path"><p>当前位置：内容管理<span>&gt;&gt;</span>设置下载分类</p></div>
  <div class="main-cont">
    <h3 class="title"><a href="ljcms_downloadcate.php?action=add" class="btn-general"><span>添加分类</span></a>设置下载分类</h3>
	<form action="ljcms_downloadcate.php" method="post" name="myform" id="myform" style="margin:0">
	<input type="hidden" name="action" id="action" value="savesetting" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <thead class="tb-tit-bg">
	  <tr>
	    <th width="8%"><div class="th-gap">选择</div></th>
		<th width="20%"><div class="th-gap">分类名称</div></th>
		<th width="20%"><div class="th-gap">Meta标题</div></th>
		<th width="20%"><div class="th-gap">Meta关键字</div></th>
		<th width="20%"><div class="th-gap">Meta描述</div></th>
		<th><div class="th-gap">排序</div></th>
	  </tr>
	  </thead>
	  <tfoot class="tb-foot-bg"></tfoot>
	  <!--{foreach $cate as $volist}-->
	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">
	    <td align="center"><input name="id[]" type="checkbox" value="<!--{$volist.cateid}-->" onClick="checkItem(this, 'chkAll')"></td>
		<td align="center"><input type="text" name="catename_<!--{$volist.cateid}-->" value="<!--{$volist.catename}-->" size="18" /></td>
		<td align="center"><input type="text" name="metatitle_<!--{$volist.cateid}-->" value="<!--{$volist.metatitle}-->" size="18" /></td>
		<td align="center"><input type="text" name="metakeyword_<!--{$volist.cateid}-->" value="<!--{$volist.metakeyword}-->" size="18" /></td>
		<td align="center"><input type="text" name="metadescription_<!--{$volist.cateid}-->" value="<!--{$volist.metadescription}-->" size="18" /></td>
		<td align="center"><input type="text" name="orders_<!--{$volist.cateid}-->" value="<!--{$volist.orders}-->" size="6" /></td>
	  </tr>
	  <!--{foreachelse}-->
      <tr>
	    <td colspan="6" align="center">暂无信息</td>
	  </tr>
	  <!--{/foreach}-->
	  <!--{if $total>0}-->
	  <tr>
		<td align="center"><input name="chkAll" type="checkbox" id="chkAll" onClick="checkAll(this, 'id[]')" value="checkbox"></td>
		<td class="hback" colspan="5"><input class="button" name="btn_del" type="button" value="批量更新" onClick="{if(confirm('确定更新选定信息吗!?')){$('#action').val('savesetting');$('#myform').submit();return true;}return false;}" class="button"></td>
	  </tr>
	  <!--{/if}-->
	</table>
	</form>
	<!--{if $pagecount>1}-->
	<table width='95%' border='0' cellspacing='0' cellpadding='0' align='center' style="margin-top:10px;">
	  <tr>
		<td align='center'><!--{$showpage}--></td>
	  </tr>
	</table>
	<!--{/if}-->
  </div>
</div>








<!--{/if}-->


<!--{if $action eq "add"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：内容管理<span>&gt;&gt;</span>下载分类</p></div>
  <div class="main-cont">
    <h3 class="title"><a href="ljcms_downloadcate.php?action=add" class="btn-general"><span>添加分类</span></a>下载分类</h3>
	<form action="ljcms_downloadcate.php" method="post" name="myform" id="myform" style="margin:0">
	<input type="hidden" name="action" id="action" value="del" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <thead class="tb-tit-bg">
	  <tr>
	    <th width="8%"><div class="th-gap">选择</div></th>
		<th width="15%"><div class="th-gap">分类名称</div></th>
		<th width="10%"><div class="th-gap">图标</div></th>
		<th width="8%"><div class="th-gap">CSS</div></th>
		<th width="8%"><div class="th-gap">链接</div></th>
		<th width="8%"><div class="th-gap">排序</div></th>
		<th width="8%"><div class="th-gap">状态</div></th>
		<th width="10%"><div class="th-gap">下载</div></th>
		<th width="12%"><div class="th-gap">录入时间</div></th>
		<th><div class="th-gap">操作</div></th>
	  </tr>
	  </thead>
	  <tfoot class="tb-foot-bg"></tfoot>
	  <!--{foreach $cate as $volist}-->
	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">
	    <td align="center"><input name="id[]" type="checkbox" value="<!--{$volist.cateid}-->" onClick="checkItem(this, 'chkAll')"></td>
		<td><!--{$volist.catename}--></td>
		<td align="center"><!--{if $volist.img!=''}--><img src="../<!--{$volist.img}-->" border="0" /><!--{else}--><font color="#999999">无图标</font><!--{/if}--></td>
		<td align="center"><!--{$volist.cssname}--></td>
		<td align="center"><!--{if $volist.linktype==1}--><font color="green">内部</font><!--{else}--><font color="blue">外部</font><!--{/if}--></td>
		<td align="center"><!--{$volist.orders}--></td>
		<td align="center">
		<!--{if $volist.flag==0}-->
			<input type="hidden" id="attr_flag<!--{$volist.cateid}-->" value="flagopen" />
			<img id="flag<!--{$volist.cateid}-->" src="liangjingcms/images/no.gif" onClick="javascript:fetch_ajax('flag','<!--{$volist.cateid}-->');" style="cursor:pointer;">
		<!--{else}-->
			<input type="hidden" id="attr_flag<!--{$volist.cateid}-->" value="flagclose" />
			<img id="flag<!--{$volist.cateid}-->" src="liangjingcms/images/yes.gif" onClick="javascript:fetch_ajax('flag','<!--{$volist.cateid}-->');" style="cursor:pointer;">	
		<!--{/if}-->
		</td>
		<td align="center"><!--{$volist.downloadcount}--></td>
		<td align="center"><!--{$volist.timeline|date_format:"%Y/%m/%d"}--></td>
		<td align="center"><a href="ljcms_downloadcate.php?action=edit&id=<!--{$volist.cateid}-->&page=<!--{$page}-->" class="icon-set">设置</a>&nbsp;&nbsp;<a href="ljcms_downloadcate.php?action=del&id[]=<!--{$volist.cateid}-->" onClick="{if(confirm('确定要删除该信息?')){return true;} return false;}" class="icon-del">删除</a></td>
	  </tr>
	  <!--{foreachelse}-->
      <tr>
	    <td colspan="10" align="center">暂无信息</td>
	  </tr>
	  <!--{/foreach}-->
	  <!--{if $total>0}-->
	  <tr>
		<td align="center"><input name="chkAll" type="checkbox" id="chkAll" onClick="checkAll(this, 'id[]')" value="checkbox"></td>
		<td class="hback" colspan="9"><input class="button" name="btn_del" type="button" value="删 除" onClick="{if(confirm('确定删除选定信息吗!?')){$('#action').val('del');$('#myform').submit();return true;}return false;}" class="button">&nbsp;&nbsp;共[ <b><!--{$total}--></b> ]条记录</td>
	  </tr>
	  <!--{/if}-->
	</table>
	</form>
	<!--{if $pagecount>1}-->
	<table width='95%' border='0' cellspacing='0' cellpadding='0' align='center' style="margin-top:10px;">
	  <tr>
		<td align='center'><!--{$showpage}--></td>
	  </tr>
	</table>
	<!--{/if}-->
  </div>
</div>
<!--{/if}-->

<!--{if $action eq "setting"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：内容管理<span>&gt;&gt;</span>设置下载分类</p></div>
  <div class="main-cont">
    <h3 class="title"><a href="ljcms_downloadcate.php?action=add" class="btn-general"><span>添加分类</span></a>设置下载分类</h3>
	<form action="ljcms_downloadcate.php" method="post" name="myform" id="myform" style="margin:0">
	<input type="hidden" name="action" id="action" value="savesetting" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <thead class="tb-tit-bg">
	  <tr>
	    <th width="8%"><div class="th-gap">选择</div></th>
		<th width="20%"><div class="th-gap">分类名称</div></th>
		<th width="20%"><div class="th-gap">Meta标题</div></th>
		<th width="20%"><div class="th-gap">Meta关键字</div></th>
		<th width="20%"><div class="th-gap">Meta描述</div></th>
		<th><div class="th-gap">排序</div></th>
	  </tr>
	  </thead>
	  <tfoot class="tb-foot-bg"></tfoot>
	  <!--{foreach $cate as $volist}-->
	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">
	    <td align="center"><input name="id[]" type="checkbox" value="<!--{$volist.cateid}-->" onClick="checkItem(this, 'chkAll')"></td>
		<td align="center"><input type="text" name="catename_<!--{$volist.cateid}-->" value="<!--{$volist.catename}-->" size="18" /></td>
		<td align="center"><input type="text" name="metatitle_<!--{$volist.cateid}-->" value="<!--{$volist.metatitle}-->" size="18" /></td>
		<td align="center"><input type="text" name="metakeyword_<!--{$volist.cateid}-->" value="<!--{$volist.metakeyword}-->" size="18" /></td>
		<td align="center"><input type="text" name="metadescription_<!--{$volist.cateid}-->" value="<!--{$volist.metadescription}-->" size="18" /></td>
		<td align="center"><input type="text" name="orders_<!--{$volist.cateid}-->" value="<!--{$volist.orders}-->" size="6" /></td>
	  </tr>
	  <!--{foreachelse}-->
      <tr>
	    <td colspan="6" align="center">暂无信息</td>
	  </tr>
	  <!--{/foreach}-->
	  <!--{if $total>0}-->
	  <tr>
		<td align="center"><input name="chkAll" type="checkbox" id="chkAll" onClick="checkAll(this, 'id[]')" value="checkbox"></td>
		<td class="hback" colspan="5"><input class="button" name="btn_del" type="button" value="批量更新" onClick="{if(confirm('确定更新选定信息吗!?')){$('#action').val('savesetting');$('#myform').submit();return true;}return false;}" class="button"></td>
	  </tr>
	  <!--{/if}-->
	</table>
	</form>
	<!--{if $pagecount>1}-->
	<table width='95%' border='0' cellspacing='0' cellpadding='0' align='center' style="margin-top:10px;">
	  <tr>
		<td align='center'><!--{$showpage}--></td>
	  </tr>
	</table>
	<!--{/if}-->
  </div>
</div>








<!--{/if}-->


<!--{if $action eq "add"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：内容管理<span>&gt;&gt;</span>添加下载分类</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_downloadcate.php" class="btn-general"><span>返回列表</span></a>添加下载分类</h3>
    <form name="myform" id="myform" method="post" action="ljcms_downloadcate.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveadd" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">分类名称：<span class='f_red'>*</span></td>
		<td class='hback' width="85%"><input type="text" name="catename" id="catename" class="input-txt" /> <span class='f_red' id="dcatename"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta标题 </td>
		<td class='hback'><input type="text" name="metatitle" id="metatitle" class="input-txt" />  <span id='dmetatitle' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta关键字 <span class="f_red"></span></td>
		<td class='hback'><textarea name="metakeyword" id="metakeyword" style="width:60%;height:45px;display:;overflow:auto;"></textarea> <span id='dmetakeyword' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta描述 <span class="f_red"></span></td>
		<td class='hback'><textarea name="metadescription" id="metadescription" style="width:60%;height:45px;display:;overflow:auto;"></textarea> <span id='dmetadescription' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>分类排序：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="orders" id="orders" class="input-s" value="<!--{$orders}-->" /> <span class='f_red' id="dorders"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>参数设置：<span class='f_red'></span></td>
		<td class='hback'><!--{$flag_checkbox}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>CCS样式：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="cssname" id="cssname" class="input-s" /> <span class='f_red' id="dcssname"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>样式图标 <span class="f_red"></span> </td>
		<td class='hback'>
		  <input type="hidden" name="img" id="img" /><span id='dimg' class='f_red'></span>
		  <iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?comeform=myform&inputid=img&thumbflag=0'></iframe>
		</td>
	  </tr>
	  <tr>
		<td class='hback_1'>打开方式：<span class='f_red'></span></td>
		<td class='hback'><select name="target" id="target"><option value="1">本页面</option><option value="2">新页面</option></select></td>
	  </tr>
	  <tr>
		<td class='hback_1'>链接类型：<span class='f_red'></span></td>
		<td class='hback'><input type="radio" name="linktype" value="1" checked />内部链接，<input type="radio" name="linktype" value="2" />外部链接</td>
	  </tr>
	  <tr>
		<td class='hback_1'>外部URL：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="linkurl" id="linkurl" class="input-txt" /> <span class='f_red' id="dlinkurl"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>分类简介： </td>
		<td class='hback'><textarea name="intro" id="intro" style='width:60%;height:60px;overflow:auto;color:#444444;'></textarea><br />（500字符以内）</td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="添加保存" /></td>
	  </tr>
	</table>
	</form>
  </div>



  <div style="clear:both;"></div>
</div>
<!--{/if}-->

<!--{if $action eq "edit"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：内容管理<span>&gt;&gt;</span>编辑下载分类</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_downloadcate.php?<!--{$comeurl}-->" class="btn-general"><span>返回列表</span></a>编辑下载分类</h3>
    <form name="myform" id="myform" method="post" action="ljcms_downloadcate.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveedit" />
	<input type="hidden" name="id" value="<!--{$id}-->" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">分类名称：<span class='f_red'>*</span></td>
		<td class='hback' width="85%"><input type="text" name="catename" id="catename" class="input-txt" value="<!--{$cate.catename}-->" /> <span class='f_red' id="dcatename"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta标题 </td>
		<td class='hback'><input type="text" name="metatitle" id="metatitle" class="input-txt" value="<!--{$cate.metatitle}-->" />  <span id='dmetatitle' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta关键字 <span class="f_red"></span></td>
		<td class='hback'>
		  <textarea name="metakeyword" id="metakeyword" style="width:60%;height:45px;display:;overflow:auto;"><!--{$cate.metakeyword}--></textarea> <span id='dmetakeyword' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta描述 <span class="f_red"></span></td>
		<td class='hback'>
		  <textarea name="metadescription" id="metadescription" style="width:60%;height:45px;display:;overflow:auto;"><!--{$cate.metadescription}--></textarea> <span id='dmetadescription' class='f_red'></span></td>
	  </tr>

	  <tr>
		<td class='hback_1'>分类排序：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="orders" id="orders" class="input-s" value="<!--{$cate.orders}-->" /> <span class='f_red' id="dorders"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>参数设置：<span class='f_red'></span></td>
		<td class='hback'><!--{$flag_checkbox}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>CCS样式：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="cssname" id="cssname" class="input-s" value="<!--{$cate.cssname}-->" /> <span class='f_red' id="dcssname"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>样式图标 <span class="f_red"></span> </td>
		<td class='hback'>
		  <input type="hidden" name="img" id="img" value="<!--{$cate.img}-->" /><span id='dimg' class='f_red'></span>
		    <!--{if $cate.img!=""}-->
			<iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?action=show&comeform=myform&inputid=img&thumbflag=0&picname=<!--{$cate.imgname}-->&picurl=<!--{$cate.img}-->'></iframe>
			<!--{else}-->
			<iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?comeform=myform&inputid=img&thumbflag=0'></iframe>
			<!--{/if}-->
		</td>
	  </tr>
	  <tr>
		<td class='hback_1'>打开方式：<span class='f_red'></span></td>
		<td class='hback'><select name="target" id="target"><option value="1"<!--{if $cate.target==1}--> selected<!--{/if}-->>本页面</option><option value="2"<!--{if $cate.target==2}--> selected<!--{/if}-->>新页面</option></select></td>
	  </tr>
	  <tr>
		<td class='hback_1'>链接类型：<span class='f_red'></span></td>
		<td class='hback'><input type="radio" name="linktype" value="1"<!--{if $cate.linktype==1}--> checked<!--{/if}--> />内部链接，<input type="radio" name="linktype" value="2"<!--{if $cate.linktype==2}--> checked<!--{/if}--> />外部链接</td>
	  </tr>
	  <tr>
		<td class='hback_1'>外部URL：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="linkurl" id="linkurl" class="input-txt" value="<!--{$cate.linkurl}-->" /> <span class='f_red' id="dlinkurl"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>分类简介： </td>
		<td class='hback'><textarea name="intro" id="intro" style='width:60%;height:60px;overflow:auto;color:#444444;'><!--{$cate.intro}--></textarea><br />（500字符以内）</td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="更新保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>


  <div style="clear:both;"></div>
</div>
<!--{/if}-->

<!--{if $action eq "edit"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：内容管理<span>&gt;&gt;</span>编辑下载分类</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_downloadcate.php?<!--{$comeurl}-->" class="btn-general"><span>返回列表</span></a>编辑下载分类</h3>
    <form name="myform" id="myform" method="post" action="ljcms_downloadcate.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveedit" />
	<input type="hidden" name="id" value="<!--{$id}-->" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">分类名称：<span class='f_red'>*</span></td>
		<td class='hback' width="85%"><input type="text" name="catename" id="catename" class="input-txt" value="<!--{$cate.catename}-->" /> <span class='f_red' id="dcatename"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta标题 </td>
		<td class='hback'><input type="text" name="metatitle" id="metatitle" class="input-txt" value="<!--{$cate.metatitle}-->" />  <span id='dmetatitle' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta关键字 <span class="f_red"></span></td>
		<td class='hback'>
		  <textarea name="metakeyword" id="metakeyword" style="width:60%;height:45px;display:;overflow:auto;"><!--{$cate.metakeyword}--></textarea> <span id='dmetakeyword' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta描述 <span class="f_red"></span></td>
		<td class='hback'>
		  <textarea name="metadescription" id="metadescription" style="width:60%;height:45px;display:;overflow:auto;"><!--{$cate.metadescription}--></textarea> <span id='dmetadescription' class='f_red'></span></td>
	  </tr>

	  <tr>
		<td class='hback_1'>分类排序：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="orders" id="orders" class="input-s" value="<!--{$cate.orders}-->" /> <span class='f_red' id="dorders"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>参数设置：<span class='f_red'></span></td>
		<td class='hback'><!--{$flag_checkbox}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>CCS样式：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="cssname" id="cssname" class="input-s" value="<!--{$cate.cssname}-->" /> <span class='f_red' id="dcssname"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>样式图标 <span class="f_red"></span> </td>
		<td class='hback'>
		  <input type="hidden" name="img" id="img" value="<!--{$cate.img}-->" /><span id='dimg' class='f_red'></span>
		    <!--{if $cate.img!=""}-->
			<iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?action=show&comeform=myform&inputid=img&thumbflag=0&picname=<!--{$cate.imgname}-->&picurl=<!--{$cate.img}-->'></iframe>
			<!--{else}-->
			<iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?comeform=myform&inputid=img&thumbflag=0'></iframe>
			<!--{/if}-->
		</td>
	  </tr>
	  <tr>
		<td class='hback_1'>打开方式：<span class='f_red'></span></td>
		<td class='hback'><select name="target" id="target"><option value="1"<!--{if $cate.target==1}--> selected<!--{/if}-->>本页面</option><option value="2"<!--{if $cate.target==2}--> selected<!--{/if}-->>新页面</option></select></td>
	  </tr>
	  <tr>
		<td class='hback_1'>链接类型：<span class='f_red'></span></td>
		<td class='hback'><input type="radio" name="linktype" value="1"<!--{if $cate.linktype==1}--> checked<!--{/if}--> />内部链接，<input type="radio" name="linktype" value="2"<!--{if $cate.linktype==2}--> checked<!--{/if}--> />外部链接</td>
	  </tr>
	  <tr>
		<td class='hback_1'>外部URL：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="linkurl" id="linkurl" class="input-txt" value="<!--{$cate.linkurl}-->" /> <span class='f_red' id="dlinkurl"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>分类简介： </td>
		<td class='hback'><textarea name="intro" id="intro" style='width:60%;height:60px;overflow:auto;color:#444444;'><!--{$cate.intro}--></textarea><br />（500字符以内）</td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="更新保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<!--{/if}-->

</body>
</html>
<script type="text/javascript">
function checkform() {
	var t = "";
	var v = "";

	t = "catename";
	v = $("#"+t).val();
	if(v=="") {
		dmsg("分类名称不能为空", t);
		return false;
	}

	return true;
}
</script>






























<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- saved from url=(0050)http://localhost/douphp/admin/mobile.php?rec=theme -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>DouPHP 管理中心 - 手机版模板 </title>
<meta name="Copyright" content="Douco Design.">
<link href="img/public.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="img/jquery.min.js"></script>
<script type="text/javascript" src="img/global.js"></script>
<script type="text/javascript" src="img/jquery.tab.js"></script>
</head>
<body>


   <div id="mMain">
    <div class="mainBox" style="height:auto!important;height:550px;min-height:550px;">
      
      
      
          <div id="theme">
      <h3>手机版模板</h3>
      <ul class="tab">
       <li><a href="img/DouPHP 管理中心 - 手机版模板.html" class="selected">管理模板</a></li>
       <li><a href="http://localhost/douphp/admin/mobile.php?rec=theme&act=install">获取更多模板</a></li>
      </ul>
            <div class="enable">
       <h2>当前启用的模板</h2>
       <p><img src="img/screenshot.png" width="170" height="230"></p>
       <dl>
        <dt>DouPHP Default</dt>
        <dd>版本：1.0</dd>
        <dd>作者：<a href="http://www.douco.com/" target="_blank">DouCo Co.,Ltd.</a></dd>
        <dd>简介：DouPHP 默认模板</dd>
       </dl>
      </div>
      <div class="themeList">
       <h2>可用模板</h2>
             </div>
                 </div>
      
    </div>
   </div>
  </div>
 





</body>
</html>