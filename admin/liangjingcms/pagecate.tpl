<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title>单页分类</title>
<meta name="author" content="<!--{$copyright_author}-->" />
<link type="text/css" rel="stylesheet" href="liangjingcms/css/admin_style.css" media="screen" />
<script type='text/javascript' src='js/jquery-1.4.4.min.js'></script>
<script type='text/javascript' src='js/command.js'></script>
<script type="text/javascript" src='../data/editor/kindeditor.js'></script>
</head>
<body>
<!--{if $action eq ""}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：其他扩展<span>&gt;&gt;</span>单页分类</p></div>
  <div class="main-cont">
    <h3 class="title"><a href="ljcms_pagecate.php?action=add" class="btn-general"><span>添加分类</span></a>单页分类</h3>
	<form action="ljcms_pagecate.php" method="post" name="myform" id="myform" style="margin:0">
	<input type="hidden" name="action" id="action" value="del" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <thead class="tb-tit-bg">
	  <tr>
	    <th width="8%"><div class="th-gap">选择</div></th>
		<th width="15%"><div class="th-gap">分类名称</div></th>
		<th width="15%"><div class="th-gap">调用标签</div></th>
		<th width="10%"><div class="th-gap">单页</div></th>
		<th width="10%"><div class="th-gap">分类图标</div></th>
		<th width="10%"><div class="th-gap">CSS样式</div></th>
		<th width="10%"><div class="th-gap">排序</div></th>
		<th width="10%"><div class="th-gap">状态</div></th>
		<th><div class="th-gap">操作</div></th>
	  </tr>
	  </thead>
	  <tfoot class="tb-foot-bg"></tfoot>
	  <!--{foreach $cate as $volist}-->
	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">
	    <td align="center"><input name="id[]" type="checkbox" value="<!--{$volist.cateid}-->" onClick="checkItem(this, 'chkAll')"></td>
		<td><!--{$volist.catename}--></td>
		<td align="center">&lt;!--{$page_block<!--{$volist.cateid}-->}--&gt;</td>
		<td align="center"><!--{$volist.pagecount}--></td>
		<td align="center"><!--{if $volist.img!=''}--><img src="../<!--{$volist.img}-->" border="0" /><!--{else}--><font color="#999999">无图标</font><!--{/if}--></td>
		<td align="center"><!--{$volist.cssname}--></td>
		<td align="center"><!--{$volist.orders}--></td>
		<td align="center">
		<!--{if $volist.flag==0}-->
			<input type="hidden" id="attr_flag<!--{$volist.cateid}-->" value="flagopen" />
			<img id="flag<!--{$volist.cateid}-->" src="liangjingcms/images/no.gif" onClick="javascript:fetch_ajax('flag','<!--{$volist.cateid}-->');" style="cursor:pointer;">
		<!--{else}-->
			<input type="hidden" id="attr_flag<!--{$volist.cateid}-->" value="flagclose" />
			<img id="flag<!--{$volist.cateid}-->" src="liangjingcms/images/yes.gif" onClick="javascript:fetch_ajax('flag','<!--{$volist.cateid}-->');" style="cursor:pointer;">	
		<!--{/if}-->
		</td>
		<td align="center"><a href="ljcms_pagecate.php?action=edit&id=<!--{$volist.cateid}-->&page=<!--{$page}-->" class="icon-set">设置</a>&nbsp;&nbsp;<a href="ljcms_pagecate.php?action=del&id[]=<!--{$volist.cateid}-->" onClick="{if(confirm('确定要删除该信息?')){return true;} return false;}" class="icon-del">删除</a></td>
	  </tr>
	  <!--{foreachelse}-->
      <tr>
	    <td colspan="9" align="center">暂无信息</td>
	  </tr>
	  <!--{/foreach}-->
	  <!--{if $total>0}-->
	  <tr>
		<td align="center"><input name="chkAll" type="checkbox" id="chkAll" onClick="checkAll(this, 'id[]')" value="checkbox"></td>
		<td class="hback" colspan="8"><input class="button" name="btn_del" type="button" value="删 除" onClick="{if(confirm('确定删除选定信息吗!?')){$('#action').val('del');$('#myform').submit();return true;}return false;}" class="button">&nbsp;&nbsp;共[ <b><!--{$total}--></b> ]条记录</td>
	  </tr>
	  <!--{/if}-->
	</table>
	</form>
	<!--{if $pagecount>1}-->
	<table width='95%' border='0' cellspacing='0' cellpadding='0' align='center' style="margin-top:10px;">
	  <tr>
		<td align='center'><!--{$showpage}--></td>
	  </tr>
	</table>
	<!--{/if}-->
  </div>
</div>
<!--{/if}-->

<!--{if $action eq "add"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：其他扩展<span>&gt;&gt;</span>添加单页分类</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_pagecate.php" class="btn-general"><span>返回列表</span></a>添加单页分类</h3>
    <form name="myform" id="myform" method="post" action="ljcms_pagecate.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveadd" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">分类名称：<span class='f_red'>*</span></td>
		<td class='hback' width="85%"><input type="text" name="catename" id="catename" class="input-txt" /> <span class='f_red' id="dcatename"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>分类排序：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="orders" id="orders" class="input-s" value="<!--{$orders}-->" /> <span class='f_red' id="dorders"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>参数设置：<span class='f_red'></span></td>
		<td class='hback'><!--{$flag_checkbox}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>CSS样式：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="cssname" id="cssname" class="input-s" /> <span class='f_red' id="dcssname"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>样式图标 <span class="f_red"></span> </td>
		<td class='hback'>
		  <input type="hidden" name="img" id="img" /><span id='dimg' class='f_red'></span>
		  <iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?comeform=myform&inputid=img&thumbflag=0'></iframe>
		</td>
	  </tr>
	  <tr>
		<td class='hback_1'>备注说明： </td>
		<td class='hback'><textarea name="intro" id="intro" style='width:60%;height:65px;overflow:auto;color:#444444;'></textarea></td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="添加保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<!--{/if}-->

<!--{if $action eq "edit"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：其他扩展<span>&gt;&gt;</span>编辑单页分类</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_pagecate.php?<!--{$comeurl}-->" class="btn-general"><span>返回列表</span></a>编辑单页分类</h3>
    <form name="myform" id="myform" method="post" action="ljcms_pagecate.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveedit" />
	<input type="hidden" name="id" value="<!--{$id}-->" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">分类名称：<span class='f_red'>*</span></td>
		<td class='hback' width="85%"><input type="text" name="catename" id="catename" class="input-txt" value="<!--{$cate.catename}-->" /> <span class='f_red' id="dcatename"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>分类排序：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="orders" id="orders" class="input-s" value="<!--{$cate.orders}-->" /> <span class='f_red' id="dorders"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>参数设置：<span class='f_red'></span></td>
		<td class='hback'><!--{$flag_checkbox}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>CSS样式：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="cssname" id="cssname" class="input-s" value="<!--{$cate.cssname}-->" /> <span class='f_red' id="dcssname"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>样式图标 <span class="f_red"></span> </td>
		<td class='hback'>
		  <input type="hidden" name="img" id="img" value="<!--{$cate.img}-->" /><span id='dimg' class='f_red'></span>
		    <!--{if $cate.img!=""}-->
			<iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?action=show&comeform=myform&inputid=img&thumbflag=0&picname=<!--{$cate.imgname}-->&picurl=<!--{$cate.img}-->'></iframe>
			<!--{else}-->
			<iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?comeform=myform&inputid=img&thumbflag=0'></iframe>
			<!--{/if}-->
		</td>
	  </tr>
	  <tr>
		<td class='hback_1'>备注说明： </td>
		<td class='hback'><textarea name="intro" id="intro" style='width:60%;height:65px;overflow:auto;color:#444444;'><!--{$cate.intro}--></textarea></td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="更新保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<!--{/if}-->

</body>
</html>
<script type="text/javascript">
function checkform() {
	var t = "";
	var v = "";

	t = "catename";
	v = $("#"+t).val();
	if(v=="") {
		dmsg("分类名称不能为空", t);
		return false;
	}

	return true;
}
</script>
