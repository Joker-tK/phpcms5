<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title>广告图片</title>
<meta name="author" content="<!--{$copyright_author}-->" />
<link type="text/css" rel="stylesheet" href="liangjingcms/css/admin_style.css" media="screen" />
<script type='text/javascript' src='js/jquery-1.4.4.min.js'></script>
<script type='text/javascript' src='js/command.js'></script>
<script type="text/javascript" src='../data/editor/kindeditor.js'></script>
</head>
<body>
<!--{if $action eq ""}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：手机版系统设置<span>&gt;&gt;</span></p></div>
  <div class="main-cont">
    <h3 class="title"><a href="ljcms_ads.php?action=add" class="btn-general"><span>添加图片</span></a>广告图片</h3>
	<div class="search-area ">
	  <form method="post" id="search_form" action="ljcms_ads.php" />
	  <div class="item">
	    <label>所在广告标签：</label><!--{$adszone_search}-->&nbsp;&nbsp;
		<label>图片描述：</label><input type="text" id="sname" name="sname" size="15" class="input" value="<!--{$sname}-->" />&nbsp;&nbsp;&nbsp;
		<input type="submit" class="button_s" value="搜 索" />
	  </div>
	  </form>
	</div>
	<form action="ljcms_ads.php" method="post" name="myform" id="myform" style="margin:0">
	<input type="hidden" name="action" id="action" value="del" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <thead class="tb-tit-bg">
	  <tr>
	    <th width="8%"><div class="th-gap">选择</div></th>
		<th width="15%"><div class="th-gap">广告标签</div></th>
		<th width="15%"><div class="th-gap">图片描述</div></th>
		<th width="15%"><div class="th-gap">图片预览</div></th>
		<th width="8%"><div class="th-gap">排序</div></th>
		<th width="8%"><div class="th-gap">显示</div></th>
		<th width="18%"><div class="th-gap">URL</div></th>
		<th><div class="th-gap">操作</div></th>
	  </tr>
	  </thead>
	  <tfoot class="tb-foot-bg"></tfoot>
	  <!--{foreach $ads as $volist}-->
	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">
	    <td align="center"><input name="id[]" type="checkbox" value="<!--{$volist.adsid}-->" onClick="checkItem(this, 'chkAll')"></td>
		<td align="center"><!--{$volist.zonename}--><!--{if $volist.zone_slide==1}--><font color="green">幻灯片</font><!--{/if}--><br />&lt!--{$ads_zone<!--{$volist.zoneid}-->}--&gt;<br /><!--{$volist.zone_width}-->x<!--{$volist.zone_height}-->像素</td>
		<td align="left"><!--{$volist.adsname}--></td>
		<td align="center"><a href="../<!--{$volist.uploadfiles}-->" target="_blank"><img src="../<!--{$volist.uploadfiles}-->" width="135" height="68" border="0" /></a></td>
		<td align="center"><!--{$volist.orders}--></td>
		<td align="center">
		<!--{$volist.showtime}-->
        </td>
		<td><!--{$volist.url}--></td>
		<td align="center"><a href="ljcms_mobile_huandengpian.php?action=edit&id=<!--{$volist.adsid}-->&page=<!--{$page}-->&<!--{$urlitem}-->" class="icon-edit">编辑</a>&nbsp;&nbsp;<a href="ljcms_mobile_huandengpian.php?action=del&id[]=<!--{$volist.adsid}-->" onClick="{if(confirm('确定要删除该信息?')){return true;} return false;}" class="icon-del">删除</a></td>
	  </tr>
	  <!--{foreachelse}-->
      <tr>
	    <td colspan="8" align="center">暂无信息</td>
	  </tr>
	  <!--{/foreach}-->
	  <!--{if $total>0}-->
	  <tr>
		<td align="center"><input name="chkAll" type="checkbox" id="chkAll" onClick="checkAll(this, 'id[]')" value="checkbox"></td>
		<td class="hback" colspan="7"><input class="button" name="btn_del" type="button" value="删 除" onClick="{if(confirm('确定删除选定信息吗!?')){$('#action').val('del');$('#myform').submit();return true;}return false;}" class="button">&nbsp;&nbsp;共[ <b><!--{$total}--></b> ]条记录</td>
	  </tr>
	  <!--{/if}-->
	</table>
	</form>
	<!--{if $pagecount>1}-->
	<table width='95%' border='0' cellspacing='0' cellpadding='0' align='center' style="margin-top:10px;">
	  <tr>
		<td align='center'><!--{$showpage}--></td>
	  </tr>
	</table>
	<!--{/if}-->
  </div>
</div>
<!--{/if}-->

<!--{if $action eq "add"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：其他扩展<span>&gt;&gt;</span>添加广告图片</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_ads.php" class="btn-general"><span>返回列表</span></a>添加广告图片</h3>
    <form name="myform" id="myform" method="post" action="ljcms_ads.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveadd" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">广告标签 <span class='f_red'>*</span></td>
		<td class='hback' width="85%"><!--{$adszone_select}--> <span id="dzoneid" class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>图片描述 <span class="f_red">*</span> </td>
		<td class='hback'><input type="text" name="adsname" id="adsname" class="input-txt" /> <span id='dadsname' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>上传图片 <span class="f_red">*</span> </td>
		<td class='hback'>
		  <input type="hidden" name="uploadfiles" id="uploadfiles" /><span id='duploadfiles' class='f_red'></span>
		  <iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?comeform=myform&inputid=uploadfiles&thumbflag=0'></iframe>
		</td>
	  </tr>
	  <tr>
		<td class='hback_1'>图片排序 </td>
		<td class='hback'><input type="text" name="orders" id="orders" class="input-s" value="<!--{$orders}-->" />  <span id='dorders' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>链接地址 </td>
		<td class='hback'><input type="text" name="url" id="url" class="input-txt" value="" />  <span id='durl' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>状态设置 </td>
		<td class='hback'><!--{$flag_checkbox}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>备注说明</td>
		<td class='hback'><textarea name="content" id="content" style='width:60%;height:65px;display:;overflow:auto;'></textarea>  <span id='dintro' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="添加保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<!--{/if}-->

<!--{if $action eq "edit"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：其他扩展<span>&gt;&gt;</span>编辑广告图片</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_ads.php?<!--{$comeurl}-->" class="btn-general"><span>返回列表</span></a>编辑广告图片</h3>
    <form name="myform" id="myform" method="post" action="ljcms_mobile_huandengpian.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveedit" />
	<input type="hidden" name="id" value="<!--{$id}-->" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">广告标签 <span class='f_red'>*</span></td>
		<td class='hback' width="85%"><!--{$adszone_select}--> <span id="dzoneid" class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>图片描述 <span class="f_red">*</span> </td>
		<td class='hback'><input type="text" name="adsname" id="adsname" class="input-txt" value="<!--{$ads.adsname}-->" /> <span id='dadsname' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>上传图片 <span class="f_red">*</span> </td>
		<td class='hback'>
		  <input type="hidden" name="uploadfiles" id="uploadfiles" value="<!--{$ads.uploadfiles}-->" /><span id='duploadfiles' class='f_red'></span>
		    <!--{if $ads.uploadfiles!=""}-->
			<iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?action=show&comeform=myform&inputid=uploadfiles&thumbflag=0&picname=<!--{$ads.uploadpicname}-->&picurl=<!--{$ads.uploadfiles}-->'></iframe>
			<!--{else}-->
			<iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?comeform=myform&inputid=uploadfiles&thumbflag=0'></iframe>
			<!--{/if}-->
		</td>
	  </tr>
	  <tr>
		<td class='hback_1'>图片排序 </td>
		<td class='hback'><input type="text" name="orders" id="orders" class="input-s" value="<!--{$ads.orders}-->" />  <span id='dorders' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>链接地址 </td>
		<td class='hback'><input type="text" name="url" id="url" class="input-txt" value="<!--{$ads.url}-->" />  <span id='durl' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>显示与否</td>
		<td class='hback'>显示<input type="radio" value="1"  name="showtime" <!--{if $ads.showtime==1}--> checked <!--{/if}-->  />  
		不显示<input type="radio" value="0"  name="showtime" <!--{if $ads.showtime==0}--> checked <!--{/if}-->  /> 
		</td>
	  </tr>
	  <tr>
		<td class='hback_1'>备注说明</td>
		<td class='hback'><textarea name="content" id="content" style='width:60%;height:65px;display:;overflow:auto;'><!--{$ads.content}--></textarea>  <span id='dintro' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="更新保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<!--{/if}-->
</body>
</html>
<script type="text/javascript">
function checkform() {
	var t = "";
	var v = "";

	t = "zoneid";
	v = $("#"+t).val();
	if(v=="") {
		dmsg("请选择所在广告标签", t);
		return false;
	}
	t = "adsname";
	v = $("#"+t).val();
	if(v=="") {
		dmsg("图片描述不能为空", t);
		return false;
	}
	t = "uploadfiles";
	v = $("#"+t).val();
	if(v=="") {
		dmsg("请上传图片", t);
		return false;
	}
	return true;
}
</script>