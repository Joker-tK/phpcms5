<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title>资讯内容</title>
<meta name="author" content="<!--{$copyright_author}-->" />
<link type="text/css" rel="stylesheet" href="liangjingcms/css/admin_style.css" media="screen" />
<script type='text/javascript' src='js/jquery-1.4.4.min.js'></script>
<script type='text/javascript' src='js/command.js'></script>
<script type="text/javascript" src='../data/editor/kindeditor.js'></script>
</head>
<body>
<table class="tableBorder" width="95%" border="0" align="center" cellpadding="5" cellspacing="1">
<tr>
<td width="100%" class="leftrow">
<!--{if $action eq ""}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：内容管理<span>&gt;&gt;</span>资讯内容</p></div>
  <div class="main-cont">
    <h3 class="title"><a href="ljcms_info.php?action=add" class="btn-general"><span>发布资讯</span></a>资讯内容</h3>
	<div class="search-area ">
	  <form method="post" id="search_form" action="ljcms_info.php" />
	  <div class="item">
	    <label>资讯分类：</label><!--{$cate_search}-->&nbsp;&nbsp;
		<label>标题：</label><input type="text" id="sname" name="sname" size="20" class="input" value="<!--{$sname}-->" />&nbsp;&nbsp;&nbsp;
		<input type="submit" class="button_s" value="搜 索" />
	  </div>
	  </form>
	</div>
	<form action="ljcms_info.php" method="post" name="myform" id="myform" style="margin:0">
	<input type="hidden" name="action" id="action" value="del" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <thead class="tb-tit-bg">
	  <tr>
	    <th width="8%"><div class="th-gap">选择</div></th>
		<th width="15%"><div class="th-gap">所在分类</div></th>
		<th width="23%"><div class="th-gap">标题</div></th>
		<th width="9%"><div class="th-gap">浏览</div></th>
		<th width="9%"><div class="th-gap">状态</div></th>
		<th width="9%"><div class="th-gap">推荐</div></th>
		<th width="12%"><div class="th-gap">录入时间</div></th>
		<th><div class="th-gap">操作</div></th>
	  </tr>
	  </thead>
	  <tfoot class="tb-foot-bg"></tfoot>
	  <!--{foreach $info as $volist}-->
	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">
	    <td align="center"><input name="id[]" type="checkbox" value="<!--{$volist.infoid}-->" onClick="checkItem(this, 'chkAll')"></td>
		<td align="center"><!--{$volist.catename}--></td>
		<td align="left"><a href="../info.php?mod=detail&id=<!--{$volist.infoid}-->" target="_blank"><!--{$volist.title}--></a> <!--{if $volist.thumbfiles!=''}--><font color="blue">[图文]</font><!--{/if}--></td>
		<td align="center"><!--{$volist.hits}--></td>
		<td align="center">
		<!--{if $volist.flag==0}-->
			<input type="hidden" id="attr_flag<!--{$volist.infoid}-->" value="flagopen" />
			<img id="flag<!--{$volist.infoid}-->" src="liangjingcms/images/no.gif" onClick="javascript:fetch_ajax('flag','<!--{$volist.infoid}-->');" style="cursor:pointer;">
		<!--{else}-->
			<input type="hidden" id="attr_flag<!--{$volist.infoid}-->" value="flagclose" />
			<img id="flag<!--{$volist.infoid}-->" src="liangjingcms/images/yes.gif" onClick="javascript:fetch_ajax('flag','<!--{$volist.infoid}-->');" style="cursor:pointer;">	
		<!--{/if}-->
        </td>
		<td align="center">
		<!--{if $volist.elite==0}-->
			<input type="hidden" id="attr_elite<!--{$volist.infoid}-->" value="eliteopen" />
			<img id="elite<!--{$volist.infoid}-->" src="liangjingcms/images/no.gif" onClick="javascript:fetch_ajax('elite','<!--{$volist.infoid}-->');" style="cursor:pointer;">
		<!--{else}-->
			<input type="hidden" id="attr_elite<!--{$volist.infoid}-->" value="eliteclose" />
			<img id="elite<!--{$volist.infoid}-->" src="liangjingcms/images/yes.gif" onClick="javascript:fetch_ajax('elite','<!--{$volist.infoid}-->');" style="cursor:pointer;">	
		<!--{/if}-->
        </td>
		<td align="center"><!--{$volist.timeline|date_format:"%Y/%m/%d"}--></td>
		<td align="center"><a href="ljcms_info.php?action=edit&id=<!--{$volist.infoid}-->&page=<!--{$page}-->&<!--{$urlitem}-->" class="icon-edit">编辑</a>&nbsp;&nbsp;<a href="ljcms_info.php?action=del&id[]=<!--{$volist.infoid}-->" onClick="{if(confirm('确定要删除该信息?')){return true;} return false;}" class="icon-del">删除</a></td>
	  </tr>
	  <!--{foreachelse}-->
      <tr>
	    <td colspan="8" align="center">暂无信息</td>
	  </tr>
	  <!--{/foreach}-->
	  <!--{if $total>0}-->
	  <tr>
		<td align="center"><input name="chkAll" type="checkbox" id="chkAll" onClick="checkAll(this, 'id[]')" value="checkbox"></td>
		<td class="hback" colspan="7"><input class="button" name="btn_del" type="button" value="删 除" onClick="{if(confirm('确定删除选定信息吗!?')){$('#action').val('del');$('#myform').submit();return true;}return false;}" class="button">&nbsp;&nbsp;共[ <b><!--{$total}--></b> ]条记录</td>
	  </tr>
	  <!--{/if}-->
	</table>
	</form>
	<!--{if $pagecount>1}-->
	<table width='95%' border='0' cellspacing='0' cellpadding='0' align='center' style="margin-top:10px;">
	  <tr>
		<td align='center'><!--{$showpage}--></td>
	  </tr>
	</table>
	<!--{/if}-->
  </div>
</div>
<!--{/if}-->

<!--{if $action eq "setting"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：内容管理<span>&gt;&gt;</span>设置资讯内容</p></div>
  <div class="main-cont">
    <h3 class="title"><a href="ljcms_info.php?action=add" class="btn-general"><span>发布资讯</span></a>设置资讯内容</h3>
	<form action="ljcms_info.php" method="post" name="myform" id="myform" style="margin:0">
	<input type="hidden" name="action" id="action" value="savesetting" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <thead class="tb-tit-bg">
	  <tr>
	    <th width="8%"><div class="th-gap">选择</div></th>
		<th width="20%"><div class="th-gap">标题</div></th>
		<th width="20%"><div class="th-gap">Meta标题</div></th>
		<th width="20%"><div class="th-gap">Meta关键字</div></th>
		<th width="20%"><div class="th-gap">Meta描述</div></th>
	  </tr>
	  </thead>
	  <tfoot class="tb-foot-bg"></tfoot>
	  <!--{foreach $info as $volist}-->
	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">
	    <td align="center"><input name="id[]" type="checkbox" value="<!--{$volist.infoid}-->" onClick="checkItem(this, 'chkAll')"><!--{$volist.infoid}--></td>
		<td align="center"><input type="text" name="title_<!--{$volist.infoid}-->" value="<!--{$volist.title}-->" size="22" /></td>
		<td align="center"><input type="text" name="metatitle_<!--{$volist.infoid}-->" value="<!--{$volist.metatitle}-->" size="20" /></td>
		<td align="center"><input type="text" name="metakeyword_<!--{$volist.infoid}-->" value="<!--{$volist.metakeyword}-->" size="20" /></td>
		<td align="center"><input type="text" name="metadescription_<!--{$volist.infoid}-->" value="<!--{$volist.metadescription}-->" size="20" /></td>
	  </tr>
	  <!--{foreachelse}-->
      <tr>
	    <td colspan="5" align="center">暂无信息</td>
	  </tr>
	  <!--{/foreach}-->
	  <!--{if $total>0}-->
	  <tr>
		<td align="center"><input name="chkAll" type="checkbox" id="chkAll" onClick="checkAll(this, 'id[]')" value="checkbox"></td>
		<td class="hback" colspan="4"><input class="button" name="btn_del" type="button" value="批量更新" onClick="{if(confirm('确定更新选定信息吗!?')){$('#action').val('savesetting');$('#myform').submit();return true;}return false;}" class="button"></td>
	  </tr>
	  <!--{/if}-->
	</table>
	</form>
	<!--{if $pagecount>1}-->
	<table width='95%' border='0' cellspacing='0' cellpadding='0' align='center' style="margin-top:10px;">
	  <tr>
		<td align='center'><!--{$showpage}--></td>
	  </tr>
	</table>
	<!--{/if}-->
  </div>
</div>
<!--{/if}-->


<!--{if $action eq "add"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：内容管理<span>&gt;&gt;</span>发布资讯</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_info.php" class="btn-general"><span>返回列表</span></a>发布资讯</h3>
    <form name="myform" id="myform" method="post" action="ljcms_info.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveadd" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">资讯分类 <span class='f_red'>*</span></td>
		<td class='hback' width="85%"><!--{$cate_select}--> <span id="dcateid" class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>资讯标题 <span class="f_red">*</span> </td>
		<td class='hback'><input type="text" name="title" id="title" class="input-txt" /> <span id='dtitle' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta标题 </td>
		<td class='hback'><input type="text" name="metatitle" id="metatitle" class="input-txt" />  <span id='dmetatitle' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta关键字 <span class="f_red"></span></td>
		<td class='hback'><textarea name="metakeyword" id="metakeyword" style="width:60%;height:45px;display:;overflow:auto;"></textarea> <span id='dmetakeyword' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta描述 <span class="f_red"></span></td>
		<td class='hback'><textarea name="metadescription" id="metadescription" style="width:60%;height:45px;display:;overflow:auto;"></textarea> <span id='dmetadescription' class='f_red'></span></td>
	  </tr>

	  <tr>
		<td class='hback_1'>资讯图文 <span class="f_red"></span> </td>
		<td class='hback'>
		  <input type="hidden" name="uploadfiles" id="uploadfiles" /><input type="hidden" name="thumbfiles" id="thumbfiles" /><span id='duploadfiles' class='f_red'></span>
		  <iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?comeform=myform&inputid=uploadfiles&thumbflag=1&thumbinput=thumbfiles&channel=&waterflag=<!--{$config.watermarkflag}-->'></iframe>
		</td>
	  </tr>
	  <tr>
		<td class='hback_1'>浏览次数 <span class="f_red"></span> </td>
		<td class='hback'><input type="text" name="hits" id="hits" class="input-s" /> <span id='dhits' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>TAGS </td>
		<td class='hback'><input type="text" name="tag" id="tag" class="input-txt" />  <span id='dtag' class='f_red'></span> (多个请用英文,号隔开) <a href="ljcms_tag.php">管理Tags链接</a></td>
	  </tr>
	  <tr>
		<td class='hback_1'>状态设置 </td>
		<td class='hback'><!--{$flag_checkbox}-->，<!--{$elite_checkbox}--></td>
	  </tr>

	  <tr>
		<td class='hback_1'>浏览权限</td>
	<td class='hback'>
       <!--{$ugroupid_select}-->

        <input name="exclusive" type="radio" value="&gt;=" checked>
        隶属
        <input name="exclusive"  type="radio"  value="=">
        专属（隶属：权限值≥可查看，专属：权限值＝可查看）	
		</td>
	  </tr>


	  <tr>
		<td class='hback_1'>资讯摘要： </td>
		<td class='hback'><textarea name="summary" id="summary" style='width:60%;height:65px;overflow:auto;color:#444444;'></textarea></td>
	  </tr>
	  <tr>
		<td class='hback_1'>资讯内容 <span class="f_red">*</span></td>
		<td class='hback'>
		  <textarea name="content" id="content" style="width:99%;height:300px;display:none;"></textarea>
		  <script>KE.show({id : 'content' });</script>  <span id='dcontent' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="添加保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<!--{/if}-->

<!--{if $action eq "edit"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：内容管理<span>&gt;&gt;</span>编辑资讯</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_info.php?<!--{$comeurl}-->" class="btn-general"><span>返回列表</span></a>编辑资讯</h3>
    <form name="myform" id="myform" method="post" action="ljcms_info.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveedit" />
	<input type="hidden" name="id" value="<!--{$id}-->" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">资讯分类 <span class='f_red'>*</span></td>
		<td class='hback' width="85%"><!--{$cate_select}--> <span id="dcateid" class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>资讯标题 <span class="f_red">*</span> </td>
		<td class='hback'><input type="text" name="title" id="title" class="input-txt" value="<!--{$info.title}-->" /> <span id='dtitle' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta标题 </td>
		<td class='hback'><input type="text" name="metatitle" id="metatitle" class="input-txt" value="<!--{$info.metatitle}-->" />  <span id='dmetatitle' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta关键字 <span class="f_red"></span></td>
		<td class='hback'><textarea name="metakeyword" id="metakeyword" style="width:60%;height:45px;display:;overflow:auto;"><!--{$info.metakeyword}--></textarea> <span id='dmetakeyword' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta描述 <span class="f_red"></span></td>
		<td class='hback'><textarea name="metadescription" id="metadescription" style="width:60%;height:45px;display:;overflow:auto;"><!--{$info.metadescription}--></textarea> <span id='dmetadescription' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>资讯图文 <span class="f_red"></span> </td>
		<td class='hback'>
		  <input type="hidden" name="uploadfiles" id="uploadfiles" value="<!--{$info.uploadfiles}-->" /><input type="hidden" name="thumbfiles" id="thumbfiles" value="<!--{$info.thumbfiles}-->" /><span id='duploadfiles' class='f_red'></span>
		    <!--{if $info.thumbfiles!=""}-->
			<iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?action=show&comeform=myform&inputid=uploadfiles&thumbflag=1&thumbinput=thumbfiles&channel=&waterflag=<!--{$config.watermarkflag}-->&picname=<!--{$info.uploadpicname}-->&picurl=<!--{$info.uploadfiles}-->'></iframe>
			<!--{else}-->
			<iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?comeform=myform&inputid=uploadfiles&thumbflag=1&thumbinput=thumbfiles&channel=&waterflag=<!--{$config.watermarkflag}-->'></iframe>
			<!--{/if}-->
		</td>
	  </tr>
	  <tr>
		<td class='hback_1'>浏览次数 <span class="f_red"></span> </td>
		<td class='hback'><input type="text" name="hits" id="hits" class="input-s" value="<!--{$info.hits}-->" /> <span id='dhits' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>TAGS </td>
		<td class='hback'><input type="text" name="tag" id="tag" class="input-txt" value="<!--{$info.tag}-->" />  <span id='dtag' class='f_red'></span> (多个请用英文,号隔开) <a href="ljcms_tag.php">管理Tags链接</a></td>
	  </tr>
	  <tr>
		<td class='hback_1'>状态设置 </td>
		<td class='hback'><!--{$flag_checkbox}-->，<!--{$elite_checkbox}--></td>
	  </tr>

	  <tr>
		<td class='hback_1'>浏览权限</td>
	<td class='hback'>
       <!--{$ugroupid_select}-->

        <input name="exclusive" type="radio" value="&gt;=" <!--{if $info.exclusive==">="}--> checked<!--{/if}-->>
        隶属
        <input name="exclusive"  type="radio"  value="=" <!--{if $info.exclusive=="="}--> checked<!--{/if}-->>
        专属（隶属：权限值≥可查看，专属：权限值＝可查看）	
		</td>
	  </tr>
	  <tr>
		<td class='hback_1'>资讯摘要： </td>
		<td class='hback'><textarea name="summary" id="summary" style='width:60%;height:65px;overflow:auto;color:#444444;'><!--{$info.summary}--></textarea></td>
	  </tr>
	  <tr>
		<td class='hback_1'>资讯内容 <span class="f_red">*</span></td>
		<td class='hback'>
		  <textarea name="content" id="content" style="width:99%;height:300px;display:none;"><!--{$info.content}--></textarea>
		  <script>KE.show({id : 'content' });</script>  <span id='dcontent' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="更新保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<!--{/if}-->
</td></tr></table>
</body>
</html>
<script type="text/javascript">
function checkform() {
	var t = "";
	var v = "";

	t = "cateid";
	v = $("#"+t).val();
	if(v=="") {
		dmsg("请选择分类", t);
		return false;
	}
	t = "title";
	v = $("#"+t).val();
	if(v=="") {
		dmsg("标题不能为空", t);
		return false;
	}

	t = 'content';
	v = KE.html(t).length;
	if(v=="") {
		dmsg("内容不能为空", t);
		return false;
	}

	return true;
}
</script>