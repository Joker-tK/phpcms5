<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>首页</title>
<link type="text/css" rel="stylesheet" href="liangjingcms/css/admin_style.css" />
</head>
<body>
<br />
<table class="tableBorder" width="95%" align="center" cellpadding="5" cellspacing="1">
  <tr>
    <th height="22" colspan="2" sytle="line-height:150%">系统信息</th>
  </tr>
  <tr>
    <td width="47%" class="leftrow">
  <p>系统版本：<!--{$copyright_version}--><!--{$copyright_release}--></p> 
      </td>
    <td width="53%" class="forumrowhighlight"> 
    <a class="btn-general" href="http://www.liangjing.org/" target="_blank"><span>良精官方</span></a>
    <a class="btn-general" href="mailto:asp3721@hotmail.com"><span>意见反馈</span></a>
     </td>
     </tr>
</table>


<br />
<table class="tableBorder" width="95%" border="0" align="center" cellpadding="5" cellspacing="1">
  <tr>
    <th height="22" colspan="2" sytle="line-height:150%">运行环境信息</th>
  </tr>
  <tr>
    <td width="47%" class="leftrow">

	PHP版本信息：<span><!--{$myversion}--></span>
      
      </td>
    <td width="53%" class="forumrowhighlight">
    	服务器信息：<span><!--{$serverversion}--></span>
     </td>
     </tr>
</table>


<br />
<table class="tableBorder" width="95%" border="0" align="center" cellpadding="5" cellspacing="1">
  <tr>
    <th height="22" colspan="2" sytle="line-height:150%">网站基本数据</th>
  </tr>

  <tr>
    <td width="47%" class="leftrow">
新闻资讯数：<span><!--{$info_count}--></span>
      </td>
    <td width="53%" class="forumrowhighlight">
解决方案数：<span><!--{$solution_count}--></span>
     </td>
     </tr>

  <tr>
    <td width="47%" class="leftrow">
产品展示数：<span><!--{$product_count}--></span>
      </td>
    <td width="53%" class="forumrowhighlight">
资源下载数：<span><!--{$download_count}--></span>
     </td>
     </tr>

  <tr>
    <td width="47%" class="leftrow">
成功案例数：<span><!--{$case_count}--></span>
      </td>
    <td width="53%" class="forumrowhighlight">
客户留言数：<span><!--{$guestbook_count}--></span>
     </td>
     </tr>

  <tr>
    <td width="47%" class="leftrow">
注册会员数：<span><!--{$user_count}--></span>
      </td>
    <td width="53%" class="forumrowhighlight">

产品订单数：<!--{$order_count}-->

     </td>
     </tr>
</table>
</body>
</html>
