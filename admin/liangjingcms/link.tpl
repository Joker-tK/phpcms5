<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title>友情链接</title>
<meta name="author" content="<!--{$copyright_author}-->" />
<link type="text/css" rel="stylesheet" href="liangjingcms/css/admin_style.css" media="screen" />
<script type='text/javascript' src='js/jquery-1.4.4.min.js'></script>
<script type='text/javascript' src='js/command.js'></script>
<script type="text/javascript" src='../data/editor/kindeditor.js'></script>
</head>
<body>
<!--{if $action eq ""}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：其他扩展<span>&gt;&gt;</span>友情链接</p></div>
  <div class="main-cont">
    <h3 class="title"><a href="ljcms_link.php?action=add" class="btn-general"><span>添加友情链接</span></a>友情链接</h3>
	<form action="ljcms_link.php" method="post" name="myform" id="myform" style="margin:0">
	<input type="hidden" name="action" id="action" value="del" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <thead class="tb-tit-bg">
	  <tr>
	    <th width="8%"><div class="th-gap">选择</div></th>
		<th width="15%"><div class="th-gap">网站名称</div></th>
		<th width="15%"><div class="th-gap">LOGO预览</div></th>
		<th width="20%"><div class="th-gap">网站URL</div></th>
		<th width="8%"><div class="th-gap">排序</div></th>
		<th width="8%"><div class="th-gap">状态</div></th>
		<th width="12%"><div class="th-gap">录入时间</div></th>
		<th><div class="th-gap">操作</div></th>
	  </tr>
	  </thead>
	  <tfoot class="tb-foot-bg"></tfoot>
	  <!--{foreach $link as $volist}-->
	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">
	    <td align="center"><input name="id[]" type="checkbox" value="<!--{$volist.linkid}-->" onClick="checkItem(this, 'chkAll')"></td>
		<td align="left"><!--{$volist.linktitle}--></td>
		<td align="center"><!--{if $volist.linktype==1}-->文字链接<!--{else}--><a href="../<!--{$volist.logoimg}-->" target="_blank"><img src="../<!--{$volist.logoimg}-->" width="88" height="31" /></a><!--{/if}--></td>
		<td align="left"><!--{$volist.linkurl}--></td>
		<td align="center"><!--{$volist.orders}--></td>
		<td align="center">
		<!--{if $volist.flag==0}-->
			<input type="hidden" id="attr_flag<!--{$volist.linkid}-->" value="flagopen" />
			<img id="flag<!--{$volist.linkid}-->" src="liangjingcms/images/no.gif" onClick="javascript:fetch_ajax('flag','<!--{$volist.linkid}-->');" style="cursor:pointer;">
		<!--{else}-->
			<input type="hidden" id="attr_flag<!--{$volist.linkid}-->" value="flagclose" />
			<img id="flag<!--{$volist.linkid}-->" src="liangjingcms/images/yes.gif" onClick="javascript:fetch_ajax('flag','<!--{$volist.linkid}-->');" style="cursor:pointer;">	
		<!--{/if}-->
        </td>
		<td><!--{$volist.timeline|date_format:"%Y/%m/%d"}--></td>
		<td align="center"><a href="ljcms_link.php?action=edit&id=<!--{$volist.linkid}-->&page=<!--{$page}-->" class="icon-edit">编辑</a>&nbsp;&nbsp;<a href="ljcms_link.php?action=del&id[]=<!--{$volist.linkid}-->" onClick="{if(confirm('确定要删除该信息?')){return true;} return false;}" class="icon-del">删除</a></td>
	  </tr>
	  <!--{foreachelse}-->
      <tr>
	    <td colspan="8" align="center">暂无信息</td>
	  </tr>
	  <!--{/foreach}-->
	  <!--{if $total>0}-->
	  <tr>
		<td align="center"><input name="chkAll" type="checkbox" id="chkAll" onClick="checkAll(this, 'id[]')" value="checkbox"></td>
		<td class="hback" colspan="7"><input class="button" name="btn_del" type="button" value="删 除" onClick="{if(confirm('确定删除选定信息吗!?')){$('#action').val('del');$('#myform').submit();return true;}return false;}" class="button"></td>
	  </tr>
	  <!--{/if}-->
	</table>
	</form>
	<!--{if $pagecount>1}-->
	<table width='95%' border='0' cellspacing='0' cellpadding='0' align='center' style="margin-top:10px;">
	  <tr>
		<td align='center'><!--{$showpage}--></td>
	  </tr>
	</table>
	<!--{/if}-->
  </div>
</div>
<!--{/if}-->

<!--{if $action eq "add"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：其他扩展<span>&gt;&gt;</span>添加友情链接</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_link.php" class="btn-general"><span>返回列表</span></a>添加友情链接</h3>
    <form name="myform" id="myform" method="post" action="ljcms_link.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveadd" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">网站名称 <span class='f_red'>*</span></td>
		<td class='hback' width="85%"><input type="text" name="linktitle" id="linktitle" class="input-txt" /> <span id='dlinktitle' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>网站URL <span class="f_red">*</span> </td>
		<td class='hback'><input type="text" name="linkurl" id="linkurl" class="input-txt" /> （以 http:// 开头） <span id='dlinkurl' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>链接类型 <span class="f_red">*</span></td>
		<td class='hback'><select name="linktype" id="linktype"><option value="">==请选择==</option><option value="1">文字链接</option><option value="2">LOGO链接</option></select> <span id='dlinktype' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>LOGO图片 <span class="f_red"></span> </td>
		<td class='hback'>
		  <input type="hidden" name="logoimg" id="logoimg" /><span id='dlogoimg' class='f_red'></span>
		  <iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?comeform=myform&inputid=logoimg&thumbflag=0'></iframe>
		</td>
	  </tr>
	  <tr>
		<td class='hback_1'>网站排序 </td>
		<td class='hback'><input type="text" name="orders" id="orders" class="input-s" value="<!--{$orders}-->" />  <span id='dorders' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>状态设置 </td>
		<td class='hback'><!--{$flag_checkbox}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>网站描述</td>
		<td class='hback'><textarea name="intro" id="intro" style='width:60%;height:65px;display:;overflow:auto;'></textarea>  <span id='dintro' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="添加保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<!--{/if}-->

<!--{if $action eq "edit"}-->
<div class="main-wrap">
  <div class="path"><p>当前位置：其他扩展<span>&gt;&gt;</span>编辑友情链接</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_link.php?<!--{$comeurl}-->" class="btn-general"><span>返回列表</span></a>编辑友情链接</h3>
    <form name="myform" id="myform" method="post" action="ljcms_link.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveedit" />
	<input type="hidden" name="id" value="<!--{$id}-->" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">网站名称 <span class='f_red'>*</span></td>
		<td class='hback' width="85%"><input type="text" name="linktitle" id="linktitle" class="input-txt" value="<!--{$link.linktitle}-->" /> <span id='dlinktitle' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>网站URL <span class="f_red">*</span> </td>
		<td class='hback'><input type="text" name="linkurl" id="linkurl" class="input-txt" value="<!--{$link.linkurl}-->" /> （以 http:// 开头） <span id='dlinkurl' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>链接类型 <span class="f_red">*</span></td>
		<td class='hback'><select name="linktype" id="linktype"><option value="">==请选择==</option><option value="1"<!--{if $link.linktype==1}--> selected<!--{/if}-->>文字链接</option><option value="2"<!--{if $link.linktype==2}--> selected<!--{/if}-->>LOGO链接</option></select> <span id='dlinktype' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>LOGO图片 <span class="f_red"></span> </td>
		<td class='hback'>
		  <input type="hidden" name="logoimg" id="logoimg" value="<!--{$link.logoimg}-->" /><span id='dlogoimg' class='f_red'></span>
		    <!--{if $link.logoimg!=""}-->
			<iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?action=show&comeform=myform&inputid=logoimg&thumbflag=0&picname=<!--{$link.logopicname}-->&picurl=<!--{$link.logoimg}-->'></iframe>
			<!--{else}-->
			<iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?comeform=myform&inputid=logoimg&thumbflag=0'></iframe>
			<!--{/if}-->
		</td>
	  </tr>
	  <tr>
		<td class='hback_1'>网站排序 </td>
		<td class='hback'><input type="text" name="orders" id="orders" class="input-s" value="<!--{$link.orders}-->" />  <span id='dorders' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>状态设置 </td>
		<td class='hback'><!--{$flag_checkbox}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>网站描述</td>
		<td class='hback'><textarea name="intro" id="intro" style='width:60%;height:65px;display:;overflow:auto;'><!--{$link.intro}--></textarea>  <span id='dintro' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="更新保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<!--{/if}-->
</body>
</html>
<script type="text/javascript">
function checkform() {
	var t = "";
	var v = "";

	t = "linktitle";
	v = $("#"+t).val();
	if(v=="") {
		dmsg("网站名称不能为空", t);
		return false;
	}
	t = "linkurl";
	v = $("#"+t).val();
	if(v=="") {
		dmsg("网站URL不能为空", t);
		return false;
	}
	t = "linktype";
	v = $("#"+t).val();
	if(v=="") {
		dmsg("请选择链接类型", t);
		return false;
	}
	return true;
}
</script>