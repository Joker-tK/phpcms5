<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
**/

require_once '../source/core/run.php';
require_once 'admin.inc.php';
$action		= Core_Fun::rec_post("action");
$page		= Core_Fun::detect_number(Core_Fun::rec_post("page"),1);
$scateid    = Core_Fun::detect_number(Core_Fun::rec_post("scateid"));
$sname      = Core_Fun::rec_post("sname",1);
if($page<1){$page=1;}
$comeurl	= "page=$page&scateid=$scateid&sname=".urlencode($sname)."";

if(Core_Fun::rec_post('act')=='update'){
    updateajax(Core_Fun::rec_post('id'),Core_Fun::rec_post('action'));
}
switch($action){
    case 'add';
	    add();
		break;
	case 'saveadd';
	    saveadd();
		break;
	case 'edit';
	    edit();
		break;
	case 'saveedit';
	    saveedit();
		break;
	case 'setting';
	    setting();
		break;
	case 'savesetting';
	    savesetting();
		break;
	case 'del';
	    del();
		break;
	default;
	    volist();
		break;
}

function volist(){
	Core_Auth::checkauth("casevolist");
	global $db,$tpl,$page,$scateid,$sname;
    $pagesize	= 30;
	$searchsql	= " WHERE 1=1";
	if($scateid>0){
		$searchsql .= " AND i.cateid=$scateid";
	}
	if(Core_Fun::ischar($sname)){
		$searchsql .= " AND i.title LIKE '%".$sname."%'";
	}
	$countsql	= "SELECT COUNT(i.caseid) FROM ".DB_PREFIX."case AS i".$searchsql;
    $total		= $db->fetch_count($countsql);
    $pagecount	= ceil($total/$pagesize);
	$nextpage	= $page+1;
	$prepage	= $page-1;
	$start		= ($page-1)*$pagesize;
	$sql		= "SELECT i.*,c.catename".
		          " FROM ".DB_PREFIX."case AS i".
		          " LEFT JOIN ".DB_PREFIX."casecate AS c ON i.cateid=c.cateid".
		          $searchsql." ORDER BY i.caseid DESC LIMIT $start, $pagesize";
	$case		= $db->getall($sql);
	$url		= $_SERVER['PHP_SELF'];
	$urlitem	= "scateid=$scateid&sname=".urlencode($sname)."";
	$url	   .= "?".$urlitem;
	$showpage	= Core_Page::adminpage($total,$pagesize,$page,$url,10);
	$tpl->assign("total",$total);
	$tpl->assign("pagecount",$pagecount);
	$tpl->assign("page",$page);
	$tpl->assign("showpage",$showpage);
	$tpl->assign("case",$case);
	$tpl->assign("urlitem",$urlitem);
	$tpl->assign("cate_search",Core_Mod::db_select($scateid,"scateid","casecate"));
	$tpl->assign("sname",$sname);
}

function setting(){
	global $db,$tpl;
	$sql		= "SELECT * FROM ".DB_PREFIX."re";
	$result		= $db->getall($sql);
	/*if(isset($_GET['id'])){
		$id = $_GET['id'];
		var_dump($id);
	}*/
	/*if(isset($_GET['showtime'])){
		$id = $_GET['showtime'];
		var_dump($_GET['showtime']);
	}*/
	$tpl->assign("result",$result);
	/*if(isset($_GET['id']) && isset($_GET['showtime'])){
	if($showtime==0){
		$result = $db->update(DB_PREFIX."re",array("showtime"=>"1"),"id=$id");	
		echo 1;
	}
	if($showtime==1){
		$result = $db->update(DB_PREFIX."re",array("showtime"=>"0"),"id=$id");
		echo 2;
	}
	}*/
}
function change(){
	var_dump($_POST);
}


function edit(){
	Core_Auth::checkauth("caseedit");
	global $db,$tpl;
    $id = Core_Fun::rec_post('id');
	if(!Core_Fun::isnumber($id)){
		Core_Fun::halt("ID丢失","",2);
	}
	$sql	= "SELECT * FROM ".DB_PREFIX."case WHERE caseid=$id";
	$case	= $db->fetch_first($sql);
	if(!$case){
		Core_Fun::halt("数据不存在","",2);
	}else{
		$case['uploadpicname'] = Core_Mod::getpicname($case['uploadfiles']);
		$case['thumbpicname'] = Core_Mod::getpicname($case['thumbfiles']);
		$tpl->assign("flag_checkbox",Core_Mod::checkbox($case['flag'],"flag","审核"));
		$tpl->assign("elite_checkbox",Core_Mod::checkbox($case['elite'],"elite","推荐"));
		$tpl->assign("cate_select",Core_Mod::db_select($case['cateid'],"cateid","casecate"));
		$tpl->assign("ugroupid_select",Core_Mod::db_usergroulive($case['ugroupid'],"groupid","usergroup"));
		$tpl->assign("id",$id);
		$tpl->assign("comeurl",$GLOBALS['comeurl']);
	    $tpl->assign("case",$case);
	}
}

function saveadd(){
	Core_Auth::checkauth("caseadd");
	global $db;
	$cateid			= Core_Fun::detect_number(Core_Fun::rec_post('cateid',1));
	$title			= Core_Fun::rec_post('title',1);
	$thumbfiles		= Core_Fun::strip_post('thumbfiles',1);
	$uploadfiles	= Core_Fun::strip_post('uploadfiles',1);
	$hits			= Core_Fun::detect_number(Core_Fun::rec_post('hits',1));
	$intro			= Core_Fun::ltCode(Core_Fun::strip_post('intro',1));
	$content		= Core_Fun::strip_post('content',1);
	$groupid		= Core_Fun::strip_post('groupid',1);
	$exclusive		= Core_Fun::strip_post('exclusive',1);
	$flag			= Core_Fun::detect_number(Core_Fun::rec_post('flag',1));
	$elite			= Core_Fun::detect_number(Core_Fun::rec_post('elite',1));
    $delimitname	= Core_Fun::rec_post('delimitname',1);
	$metatitle		= Core_Fun::rec_post('metatitle',1);
	$metakeyword	= Core_Fun::rec_post('metakeyword',1);
	$metadescription= Core_Fun::rec_post('metadescription',1);
	$tag			= Core_Fun::rec_post('tag',1);
	$founderr		= false;
	if($cateid<1){
	    $founderr	= true;
		$errmsg	   .="请选择分类.<br />";
	}
	if(!Core_Fun::ischar($title)){
	    $founderr	= true;
		$errmsg	   .="标题不能为空.<br />";
	}
	if(!Core_Fun::ischar($content)){
	    $founderr	= true;
		$errmsg	   .="内容不能为空.<br />";
	}
	if($founderr == true){
	    Core_Fun::halt($errmsg,"",1);
	}
	if(Core_Fun::ischar($thumbfiles)){
		if(!file_exists("../".$thumbfiles)){
			$thumbfiles = $uploadfiles;
		}
	}
	$caseid	= $db->fetch_newid("SELECT MAX(caseid) FROM ".DB_PREFIX."case",1);
	$array	= array(
		'caseid'=>$caseid,
		'cateid'=>$cateid,
		'title'=>$title,
		'thumbfiles'=>$thumbfiles,
		'uploadfiles'=>$uploadfiles,
		'intro'=>$intro,
		'content'=>$content,
		'timeline'=>time(),
		'elite'=>$elite,
		'ugroupid'=>$groupid,
		'exclusive'=>$exclusive,
		'flag'=>$flag,
		'metatitle'=>$metatitle,
		'metakeyword'=>$metakeyword,
		'metadescription'=>$metadescription,
		'delimitname'=>$delimitname,
		'tag'=>$tag,
		'hits'=>$hits,
	);
	$result = $db->insert(DB_PREFIX."case",$array);
	if($result){
		Core_Command::command_savetag("case",$tag);
		Core_Command::runlog("","发布成功案例成功[$title]",1);
		Core_Fun::halt("保存成功","ljcms_case.php",0);
	}else{
		Core_Fun::halt("保存失败","",1);
	}
}

function saveedit(){
	global $db;
	if($_POST['showtime1'] ==1){
		$result1 = $db->update(DB_PREFIX."re",array('showtime'=>1),"id=1");
	}else{
		$result1 = $db->update(DB_PREFIX."re",array('showtime'=>0),"id=1");
	}
	if($_POST['showtime2'] ==1){
		$result = $db->update(DB_PREFIX."re",array('showtime'=>1),"id=2");
	}else{
		$result = $db->update(DB_PREFIX."re",array('showtime'=>0),"id=2");
	}
	if($_POST['showtime3'] ==1){
		$result = $db->update(DB_PREFIX."re",array('showtime'=>1),"id=3");
	}else{
		$result = $db->update(DB_PREFIX."re",array('showtime'=>0),"id=3");
	}
	if($_POST['showtime4'] ==1){
		$result = $db->update(DB_PREFIX."re",array('showtime'=>1),"id=4");
	}else{
		$result = $db->update(DB_PREFIX."re",array('showtime'=>0),"id=4");
	}
	if($_POST['showtime5'] ==1){
		$result = $db->update(DB_PREFIX."re",array('showtime'=>1),"id=5");
	}else{
		$result = $db->update(DB_PREFIX."re",array('showtime'=>0),"id=5");
	}
	if($_POST['showtime6'] ==1){
		$result = $db->update(DB_PREFIX."re",array('showtime'=>1),"id=6");
	}else{
		$result = $db->update(DB_PREFIX."re",array('showtime'=>0),"id=6");
	}
	if($result1){
		Core_Fun::halt("编辑成功","register_setting.php?action=setting",0);
	}else{
		Core_Fun::halt("编辑失败","",2);
	}
}


$tpl->assign("action",$action);
$tpl->display(ADMIN_TEMPLATE."register_setting.tpl");
$tpl->assign("runtime",runtime());
$tpl->assign("copyright",$libadmin->copyright());
?>