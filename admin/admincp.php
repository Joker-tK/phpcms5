<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
**/
require_once '../source/core/run.php';
require_once 'admin.inc.php';
$mod	= Core_Fun::rec_get("mod");
if($mod == "drag"){
	$tpl->display(ADMIN_TEMPLATE."frm_drag.tpl");
} elseif($mod == "left"){
	$tpl->display(ADMIN_TEMPLATE."frm_left.tpl");
} elseif($mod == "top"){
	$tpl->display(ADMIN_TEMPLATE."frm_top.tpl");
} elseif($mod == "footer"){
	$tpl->display(ADMIN_TEMPLATE."frm_footer.tpl");
} elseif($mod == "main"){
	$tpl->display(ADMIN_TEMPLATE."frm_main.tpl");
}else{
	$tpl->display(ADMIN_TEMPLATE."admincp.tpl");
}

$tpl->assign("runtime",runtime());
$tpl->assign("copyright",$libadmin->copyright());

?>