<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
**/
require_once '../source/core/run.php';
require_once 'admin.inc.php';
$action		= Core_Fun::rec_post("action");
$page		= Core_Fun::detect_number(Core_Fun::rec_post("page"),1);
$scateid    = Core_Fun::detect_number(Core_Fun::rec_post("scateid"));
$sname      = Core_Fun::rec_post("sname",1);
if($page<1){$page=1;}
$comeurl	= "page=$page&scateid=$scateid&sname=".urlencode($sname)."";

if(Core_Fun::rec_post('act')=='update'){
    updateajax(Core_Fun::rec_post('id'),Core_Fun::rec_post('action'));
}
switch($action){
    case 'add';
	    add();
		break;
	case 'saveadd';
	    saveadd();
		break;
	case 'edit';
	    edit();
		break;
	case 'saveedit';
	    saveedit();
		break;
	case 'del';
	    del();
		break;
	default;
	    volist();
		break;
}

function volist(){
	Core_Auth::checkauth("pagevolist");
	global $db,$tpl,$page,$scateid,$sname;
    $pagesize	= 30;
	$searchsql	= " WHERE 1=1";
	if($scateid>0){
		$searchsql .= " AND p.cateid=$scateid";
	}
	if(Core_Fun::ischar($sname)){
		$searchsql .= " AND p.title LIKE '%".$sname."%'";
	}
	$countsql	= "SELECT COUNT(p.pageid) FROM ".DB_PREFIX."page AS p".$searchsql;
    $total		= $db->fetch_count($countsql);
    $pagecount	= ceil($total/$pagesize);
	$nextpage	= $page+1;
	$prepage	= $page-1;
	$start		= ($page-1)*$pagesize;
	$sql		= "SELECT p.*,c.catename".
		          " FROM ".DB_PREFIX."page AS p".
		          " LEFT JOIN ".DB_PREFIX."pagecate AS c ON p.cateid=c.cateid".
		          $searchsql." ORDER BY p.orders ASC LIMIT $start, $pagesize";
	$volpage	= $db->getall($sql);
	$url		= $_SERVER['PHP_SELF'];
	$urlitem	= "scateid=$scateid&sname=".urlencode($sname)."";
	$url	   .= "?".$urlitem;
	$showpage	= Core_Page::adminpage($total,$pagesize,$page,$url,10);
	$tpl->assign("total",$total);
	$tpl->assign("pagecount",$pagecount);
	$tpl->assign("page",$page);
	$tpl->assign("showpage",$showpage);
	$tpl->assign("volpage",$volpage);
	$tpl->assign("urlitem",$urlitem);
	$tpl->assign("cate_search",Core_Mod::db_select($scateid,"scateid","pagecate"));
	$tpl->assign("sname",$sname);
}

function add(){
	Core_Auth::checkauth("pageadd");
	global $tpl,$db;
	$orders = $db->fetch_newid("SELECT MAX(orders) FROM ".DB_PREFIX."page",1);
	$tpl->assign("flag_checkbox",Core_Mod::checkbox("1","flag","审核"));
	$tpl->assign("navshow_checkbox",Core_Mod::checkbox("","navshow","导航显示"));
	$tpl->assign("cate_select",Core_Mod::db_select("","cateid","pagecate"));
	$tpl->assign("orders",$orders);
}

function edit(){
	Core_Auth::checkauth("pageedit");
	global $db,$tpl;
    $id = Core_Fun::rec_post('id');
	if(!Core_Fun::isnumber($id)){
		Core_Fun::halt("ID丢失","",2);
	}
	$sql		= "SELECT * FROM ".DB_PREFIX."page WHERE pageid=$id";
	$volpage	= $db->fetch_first($sql);
	if(!$volpage){
		Core_Fun::halt("数据不存在","",2);
	}else{
		$tpl->assign("flag_checkbox",Core_Mod::checkbox($volpage['flag'],"flag","审核"));
		$tpl->assign("navshow_checkbox",Core_Mod::checkbox($volpage['navshow'],"navshow","导航显示"));
		$tpl->assign("cate_select",Core_Mod::db_select($volpage['cateid'],"cateid","pagecate"));
		$tpl->assign("id",$id);
		$tpl->assign("comeurl",$GLOBALS['comeurl']);
	    $tpl->assign("volpage",$volpage);
	}
}

function saveadd(){
	Core_Auth::checkauth("pageadd");
	global $db;
	$cateid			= Core_Fun::detect_number(Core_Fun::rec_post('cateid',1));
	$linktype		= Core_Fun::detect_number(Core_Fun::rec_post('linktype',1));
	$linkurl		= Core_Fun::strip_post('linkurl',1);
	$target			= Core_Fun::detect_number(Core_Fun::rec_post('target',1));
	$title			= Core_Fun::rec_post('title',1);
	$content		= Core_Fun::strip_post('content',1);
	$flag			= Core_Fun::detect_number(Core_Fun::rec_post('flag',1));
	$navshow		= Core_Fun::detect_number(Core_Fun::rec_post('navshow',1));
	$orders			= Core_Fun::detect_number(Core_Fun::rec_post('orders',1));
	$metatitle		= Core_Fun::rec_post('metatitle',1);
	$metakeyword	= Core_Fun::rec_post('metakeyword',1);
	$metadescription= Core_Fun::rec_post('metadescription',1);
	$delimitname	= Core_Fun::rec_post('delimitname',1);
	$founderr		= false;
	if($cateid<1){
	    $founderr	= true;
		$errmsg	   .="请选择所在分类.<br />";
	}
	if(!Core_Fun::ischar($title)){
	    $founderr	= true;
		$errmsg	   .="单页名称不能为空.<br />";
	}
	if($founderr == true){
	    Core_Fun::halt($errmsg,"",1);
	}
	$pageid	= $db->fetch_newid("SELECT MAX(pageid) FROM ".DB_PREFIX."page",1);
	$array	= array(
		'pageid'=>$pageid,
		'cateid'=>$cateid,
		'linktype'=>$linktype,
		'linkurl'=>$linkurl,
		'target'=>$target,
		'title'=>$title,
		'content'=>$content,
		'flag'=>$flag,
		'navshow'=>$navshow,
		'orders'=>$orders,
		'timeline'=>time(),
		'metatitle'=>$metatitle,
		'metakeyword'=>$metakeyword,
		'metadescription'=>$metadescription,
		'delimitname'=>$delimitname,
	);
	$result = $db->insert(DB_PREFIX."page",$array);
	if($result){
		Core_Command::runlog("","添加单页成功[$title]",1);
		Core_Fun::halt("保存成功","ljcms_page.php",0);
	}else{
		Core_Fun::halt("保存失败","",1);
	}
}

function saveedit(){
	Core_Auth::checkauth("pageedit");
	global $db;
	$id				= Core_Fun::rec_post('id',1);
	$cateid			= Core_Fun::detect_number(Core_Fun::rec_post('cateid',1));
	$linktype		= Core_Fun::detect_number(Core_Fun::rec_post('linktype',1));
	$linkurl		= Core_Fun::strip_post('linkurl',1);
	$target			= Core_Fun::detect_number(Core_Fun::rec_post('target',1));
	$title			= Core_Fun::rec_post('title',1);
	$content		= Core_Fun::strip_post('content',1);
	$flag			= Core_Fun::detect_number(Core_Fun::rec_post('flag',1));
	$navshow		= Core_Fun::detect_number(Core_Fun::rec_post('navshow',1));
	$orders			= Core_Fun::detect_number(Core_Fun::rec_post('orders',1));
	$metatitle		= Core_Fun::rec_post('metatitle',1);
	$metakeyword	= Core_Fun::rec_post('metakeyword',1);
	$metadescription= Core_Fun::rec_post('metadescription',1);
	$delimitname	= Core_Fun::rec_post('delimitname',1);
	$founderr	= false;
	if(!Core_Fun::isnumber($id)){
	    $founderr	= true;
		$errmsg	   .= "ID丢失.<br />";
	}
	if($cateid<1){
	    $founderr	= true;
		$errmsg	   .="请选择所在分类.<br />";
	}
	if(!Core_Fun::ischar($title)){
	    $founderr	= true;
		$errmsg	   .="单页名称不能为空.<br />";
	}
	if($founderr == true){
	    Core_Fun::halt($errmsg,"",1);
	}
	$array = array(
		'cateid'=>$cateid,
		'linktype'=>$linktype,
		'linkurl'=>$linkurl,
		'target'=>$target,
		'title'=>$title,
		'content'=>$content,
		'flag'=>$flag,
		'navshow'=>$navshow,
		'orders'=>$orders,
		'metatitle'=>$metatitle,
		'metakeyword'=>$metakeyword,
		'metadescription'=>$metadescription,
		'delimitname'=>$delimitname,
	);
	$result = $db->update(DB_PREFIX."page",$array,"pageid=$id");
	if($result){
		Core_Command::runlog("","编辑单页成功[id=$id]");
		Core_Fun::halt("编辑成功","ljcms_page.php?".$GLOBALS['comeurl']."",0);
	}else{
		Core_Fun::halt("编辑失败","",2);
	}
}

function del(){
	Core_Auth::checkauth("pagedel");
	$arrid  = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
	if($arrid=="" || is_null($arrid)){
		Core_Fun::halt("请选择要删除的数据","",1);
	}
	global $db;
	for($ii=0;$ii<count($arrid);$ii++){
        $id = Core_Fun::replacebadchar(trim($arrid[$ii]));
		if(Core_Fun::isnumber($id)){
			$db->query("DELETE FROM ".DB_PREFIX."page WHERE pageid=$id");
		}
	}
	Core_Command::runlog("","删除单页成功[id=$arrid]");
	Core_Fun::halt("删除成功","ljcms_page.php",0);
}

function updateajax($_id,$_action){
	Core_Auth::checkauth("pageedit");
    if(Core_Fun::isnumber($_id)){
		global $db;
		switch($_action){
			case 'flagopen';
			$db->query("UPDATE ".DB_PREFIX."page SET flag=1 WHERE pageid=$_id");
			break;
			case 'flagclose';
			$db->query("UPDATE ".DB_PREFIX."page SET flag=0 WHERE pageid=$_id");
			break;
			default;
			break;
		}
	}
}

$tpl->assign("action",$action);
$tpl->display(ADMIN_TEMPLATE."page.tpl");
$tpl->assign("runtime",runtime());
$tpl->assign("copyright",$libadmin->copyright());
?>