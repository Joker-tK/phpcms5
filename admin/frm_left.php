<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
**/
require_once '../source/core/run.php';
require_once 'admin.inc.php';
$mod	= Core_Fun::rec_get("mod");
$tpl->assign("mod",$mod);
$tpl->display(ADMIN_TEMPLATE."frm_left.tpl");
?>