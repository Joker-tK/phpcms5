<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
**/
require_once '../source/core/run.php';
require_once 'admin.inc.php';
$action		= Core_Fun::rec_post("action");
$page		= Core_Fun::detect_number(Core_Fun::rec_post("page"),1);
if($page<1){$page=1;}
$comeurl	= "page=$page";

if(Core_Fun::rec_post('act')=='update'){
    updateajax(Core_Fun::rec_post('id'),Core_Fun::rec_post('action'));
}
switch($action){
    case 'add';
	    add();
		break;
	case 'saveadd';
	    saveadd();
		break;
	case 'edit';
	    edit();
		break;
	case 'saveedit';
	    saveedit();
		break;
	case 'del';
	    del();
		break;
	case 'setting';
	    setting();
		break;
	case 'savesetting';
	    savesetting();
		break;
	default;
	    volist();
		break;
}

function volist(){
	Core_Auth::checkauth("productcatevolist");
	global $db,$tpl;
	$cat_id = 0;
	$sql	= "SELECT c.*,COUNT(s.cateid) AS has_children,COUNT(a.productid) AS content_count".
		  " FROM ".DB_PREFIX."productcate AS c".
		  " LEFT JOIN ".DB_PREFIX."productcate AS s ON c.cateid=s.parentid".
		  " LEFT JOIN ".DB_PREFIX."product AS a ON a.cateid=c.cateid".
		  " GROUP BY c.cateid ORDER BY parentid,orders ASC";
	$rows	= $db->getall($sql);
	$cate	= Core_Mod::orders_cate_array($cat_id,$rows);
	foreach($cate as $key => $value){
		if($value['depth']==0){
			$cate[$key]['tree_catename'] = $value['catename'];
		}else{
			$tree = "";
			if($value['depth']==1){
				$tree = "&nbsp;&nbsp;├ ";
			}else{
				for($ii=2;$ii<=$value['depth'];$ii++){
					$tree .= "&nbsp;&nbsp;│";
				}
				$tree .= "&nbsp;&nbsp;├ ";
			}
			$cate[$key]['tree_catename'] = $tree.$value['catename'];
		}
	}
	$tpl->assign("cate",$cate);
}

function setting(){
	Core_Auth::checkauth("productcateedit");
	global $db,$tpl;
	$cat_id = 0;
	$sql	= "SELECT c.*,COUNT(s.cateid) AS has_children,COUNT(a.productid) AS content_count".
		  " FROM ".DB_PREFIX."productcate AS c".
		  " LEFT JOIN ".DB_PREFIX."productcate AS s ON c.cateid=s.parentid".
		  " LEFT JOIN ".DB_PREFIX."product AS a ON a.cateid=c.cateid".
		  " GROUP BY c.cateid ORDER BY parentid,orders ASC";
	$rows	= $db->getall($sql);
	$cate	= Core_Mod::orders_cate_array($cat_id,$rows);
	foreach($cate as $key => $value){
		if($value['depth']==0){
			$cate[$key]['tree_catename'] = $value['catename'];
		}else{
			$tree = "";
			if($value['depth']==1){
				$tree = "&nbsp;&nbsp;├ ";
			}else{
				for($ii=2;$ii<=$value['depth'];$ii++){
					$tree .= "&nbsp;&nbsp;│";
				}
				$tree .= "&nbsp;&nbsp;├ ";
			}
			$cate[$key]['tree_catename'] = $tree.$value['catename'];
		}
	}
	$tpl->assign("cate",$cate);
}

function add(){
	Core_Auth::checkauth("productcateadd");
	global $db,$tpl;
	$orders		= $db->fetch_newid("SELECT MAX(orders) FROM ".DB_PREFIX."productcate",1);
	$tpl->assign("orders",$orders);
	$tpl->assign("flag_checkbox",Core_Mod::checkbox("1","flag","审核"));
	$tpl->assign("elite_checkbox",Core_Mod::checkbox("","elite","推荐"));
	$tpl->assign("cate_select",Core_Mod::tree_select("productcate","","rootid"));
}

function edit(){
	Core_Auth::checkauth("productcateedit");
	global $db,$tpl;
    $id = Core_Fun::rec_post('id');
	if(!Core_Fun::isnumber($id)){
		Core_Fun::halt("ID丢失","",2);
	}
	$sql	= "SELECT * FROM ".DB_PREFIX."productcate WHERE cateid=$id";
	$cate	= $db->fetch_first($sql);
	if(!$cate){
		Core_Fun::halt("数据不存在","",2);
	}else{
		$tpl->assign("flag_checkbox",Core_Mod::checkbox($cate['flag'],"flag","审核"));
		$tpl->assign("elite_checkbox",Core_Mod::checkbox($cate['elite'],"elite","推荐"));
		$tpl->assign("id",$id);
		$tpl->assign("comeurl",$GLOBALS['comeurl']);
		$tpl->assign("cate_select",Core_Mod::tree_select("productcate",$cate['parentid'],"rootid"));
	    $tpl->assign("cate",$cate);
	}
}

function saveadd(){
	Core_Auth::checkauth("productcateadd");
	global $db;
	$rootid				= Core_Fun::detect_number(Core_Fun::rec_post('rootid',1));
	$catename			= Core_Fun::rec_post('catename',1);
	$metatitle			= Core_Fun::rec_post('metatitle',1);
	$metakeyword		= Core_Fun::rec_post('metakeyword',1);
	$metadescription	= Core_Fun::rec_post('metadescription',1);
	$pathname			= Core_Fun::rec_post('pathname',1);
	$intro				= Core_Fun::strip_post('intro',1);
	$orders				= Core_Fun::detect_number(Core_Fun::rec_post('orders',1));
	$flag				= Core_Fun::detect_number(Core_Fun::rec_post('flag',1));
	$elite				= Core_Fun::detect_number(Core_Fun::rec_post('elite',1));
	$cssname			= Core_Fun::rec_post('cssname',1);
	$img				= Core_Fun::rec_post('img',1);
	$target				= Core_Fun::detect_number(Core_Fun::rec_post('target',1),1);
	$linktype			= Core_Fun::detect_number(Core_Fun::rec_post('linktype',1),1);
	$linkurl			= Core_Fun::strip_post('linkurl',1);
	$founderr			= false;
	if(!Core_Fun::ischar($catename)){
	    $founderr	= true;
		$errmsg	   .="分类名称不能为空.<br />";
	}
	if($founderr == true){
	    Core_Fun::halt($errmsg,"",1);
	}
	if($rootid==0){
		$rootid	= 0;
		$depth	= 0;
	}else{
		$root_sql = "SELECT depth FROM ".DB_PREFIX."productcate WHERE cateid=$rootid";
		$root_rows = $db->fetch_first($root_sql);
		if($root_rows){
			$depth = (intval($root_rows['depth'])+1);
		}else{
			$depth = 0;
		}
	}
	if($depth>1){
		Core_Fun::halt("对不起，只支持2级分类！","",1);
	}
	$cateid	= $db->fetch_newid("SELECT MAX(cateid) FROM ".DB_PREFIX."productcate",1);
	$array	= array(
		'cateid'=>$cateid,
		'catename'=>$catename,
		'parentid'=>$rootid,
		'depth'=>$depth,
		'metatitle'=>$metatitle,
		'metakeyword'=>$metakeyword,
		'metadescription'=>$metadescription,
		'pathname'=>$pathname,
		'orders'=>$orders,
		'flag'=>$flag,
		'intro'=>$intro,
		'timeline'=>time(),
		'elite'=>$elite,
		'cssname'=>$cssname,
		'img'=>$img,
		'target'=>$target,
		'linktype'=>$linktype,
		'linkurl'=>$linkurl,
	);
	$result = $db->insert(DB_PREFIX."productcate",$array);
	if($result){
		Core_Command::runlog("","添加产品分类成功[$catename]",1);
		Core_Fun::halt("保存成功","ljcms_productcate.php",0);
	}else{
		Core_Fun::halt("保存失败","",1);
	}
}

function saveedit(){
	Core_Auth::checkauth("productcateedit");
	global $db;
	$id					= Core_Fun::rec_post('id',1);
	$rootid				= Core_Fun::detect_number(Core_Fun::rec_post('rootid',1));
	$catename			= Core_Fun::rec_post('catename',1);
	$metatitle			= Core_Fun::rec_post('metatitle',1);
	$metakeyword		= Core_Fun::rec_post('metakeyword',1);
	$metadescription	= Core_Fun::rec_post('metadescription',1);
	$pathname			= Core_Fun::rec_post('pathname',1);
	$intro				= Core_Fun::strip_post('intro',1);
	$orders				= Core_Fun::detect_number(Core_Fun::rec_post('orders',1));
	$flag				= Core_Fun::detect_number(Core_Fun::rec_post('flag',1));
	$elite				= Core_Fun::detect_number(Core_Fun::rec_post('elite',1));
	$cssname			= Core_Fun::rec_post('cssname',1);
	$img				= Core_Fun::rec_post('img',1);
	$target				= Core_Fun::detect_number(Core_Fun::rec_post('target',1),1);
	$linktype			= Core_Fun::detect_number(Core_Fun::rec_post('linktype',1),1);
	$linkurl			= Core_Fun::strip_post('linkurl',1);
	$founderr        = false;
	if(!Core_Fun::isnumber($id)){
	    $founderr   = true;
		$errmsg    .= "ID丢失.<br />";
	}
	if(!Core_Fun::ischar($catename)){
	    $founderr	= true;
		$errmsg	   .="分类名称不能为空.<br />";
	}
	if($founderr == true){
	    Core_Fun::halt($errmsg,"",1);
	}

	if($rootid==0){
		$rootid = 0;
		$depth  = 0;
	}else{
		if($id==$rootid){
			Core_Fun::halt("对不起，所属分类不能指定为自己！","",1);
		}
		$root_sql = "SELECT depth FROM ".DB_PREFIX."productcate WHERE cateid=$rootid";
		$root_rows = $db->fetch_first($root_sql);
		if($root_rows){
			$depth = (intval($root_rows['depth'])+1);
		}else{
			$depth = 0;
		}
	}
	if($depth>1){
		Core_Fun::halt("对不起，只支持2级分类！","",1);
	}

	$array = array(
		'catename'=>$catename,
		'parentid'=>$rootid,
		'depth'=>$depth,
		'metatitle'=>$metatitle,
		'metakeyword'=>$metakeyword,
		'metadescription'=>$metadescription,
		'pathname'=>$pathname,
		'orders'=>$orders,
		'flag'=>$flag,
		'intro'=>$intro,
		'elite'=>$elite,
		'cssname'=>$cssname,
		'img'=>$img,
		'target'=>$target,
		'linktype'=>$linktype,
		'linkurl'=>$linkurl,
	);

	$result = $db->update(DB_PREFIX."productcate",$array,"cateid=$id");
	if($result){

		Core_Mod::update_child_depth($id,$depth,"productcate");
		Core_Command::runlog("","编辑产品分类成功[$catename]",1);
		Core_Fun::halt("编辑成功","ljcms_productcate.php",0);

	}else{
		Core_Fun::halt("编辑失败","",1);	
	}
}

function del(){
	Core_Auth::checkauth("productcatedel");
	$id	= Core_Fun::rec_post('id');
	if(!Core_Fun::isnumber($id)){
		Core_Fun::halt("请选择要删除的数据","",1);
	}else{
		if(Core_Mod::exist_child($id,"productcate")){
			Core_Fun::halt("对不起，该分类下含有子类，不能删除！","",1);
		}else{
			$GLOBALS['db']->query("DELETE FROM ".DB_PREFIX."productcate WHERE cateid=$id");
			Core_Command::runlog("","删除产品分类分类成功[id=$id]",1);
			Core_Fun::halt("删除成功","ljcms_productcate.php",0);
		}
	}
}

function updateajax($_id,$_action){
	Core_Auth::checkauth("productcateedit");
    if(Core_Fun::isnumber($_id)){
		global $db;
		switch($_action){
			case 'flagopen';
			$db->query("UPDATE ".DB_PREFIX."productcate SET flag=1 WHERE cateid=$_id");
			break;
			case 'flagclose';
			$db->query("UPDATE ".DB_PREFIX."productcate SET flag=0 WHERE cateid=$_id");
			break;
			case 'eliteopen';
			$db->query("UPDATE ".DB_PREFIX."productcate SET elite=1 WHERE cateid=$_id");
			break;
			case 'eliteclose';
			$db->query("UPDATE ".DB_PREFIX."productcate SET elite=0 WHERE cateid=$_id");
			break;
			default;
			break;
		}
	}
}

function savesetting(){
	Core_Auth::checkauth("productcateedit");
	$arrid  = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
	if($arrid=="" || is_null($arrid)){
		Core_Fun::halt("请选择要更新的数据","",1);
	}
	global $db;
	for($ii=0;$ii<count($arrid);$ii++){
        $id					= (int)($arrid[$ii]);
		$catename			= Core_Fun::rec_post("catename_".$id,1);
		$metatitle			= Core_Fun::rec_post("metatitle_".$id,1);
		$metakeyword		= Core_Fun::rec_post("metakeyword_".$id,1);
		$metadescription	= Core_Fun::rec_post("metadescription_".$id,1);
		$orders				= Core_Fun::detect_number(Core_Fun::rec_post("orders_".$id,1));
		if(Core_Fun::isnumber($id)){
			$array = array(
				'catename'=>$catename,
				'metatitle'=>$metatitle,
				'metakeyword'=>$metakeyword,
				'metadescription'=>$metadescription,
				'orders'=>$orders,
			);
			$db->update(DB_PREFIX."productcate",$array,"cateid=$id");
		}
	}
	Core_Fun::halt("批量更新成功","ljcms_productcate.php?action=setting",0);
}

$tpl->assign("action",$action);
$tpl->display(ADMIN_TEMPLATE."productcate.tpl");
$tpl->assign("runtime",runtime());
$tpl->assign("copyright",$libadmin->copyright());
?>