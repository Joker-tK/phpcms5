<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
**/
require_once '../source/core/run.php';
require_once 'admin.inc.php';
$action		= Core_Fun::rec_post("action");
$page		= Core_Fun::detect_number(Core_Fun::rec_post("page"),1);
if($page<1){$page=1;}
$comeurl	= "page=$page";

if(Core_Fun::rec_post('act')=='update'){
    updateajax(Core_Fun::rec_post('id'),Core_Fun::rec_post('action'));
}
switch($action){
    case 'add';
	    add();
		break;
	case 'saveadd';
	    saveadd();
		break;
	case 'edit';
	    edit();
		break;
	case 'saveedit';
	    saveedit();
		break;
	case 'del';
	    del();
		break;
	case 'savepassword';
	    savepassword();
		break;
	default;
	    volist();
		break;
}

function volist(){
	Core_Auth::checkauth("uservolist");
	global $db,$tpl,$page;
    $pagesize	= 30;
	$searchsql	= " WHERE 1=1";
	$countsql	= "SELECT COUNT(a.userid) FROM ".DB_PREFIX."user AS a".$searchsql;
    $total		= $db->fetch_count($countsql);
    $pagecount	= ceil($total/$pagesize);
	$nextpage	= $page+1;
	$prepage	= $page-1;
	$start		= ($page-1)*$pagesize;
	$sql		= "SELECT a.*,g.grupname AS groupname".
	             " FROM ".DB_PREFIX."user AS a".
		         " LEFT JOIN ".DB_PREFIX."usergroup  AS g ON a.usergroupid=g.usergroupid".
		         $searchsql." ORDER BY a.userid ASC LIMIT $start, $pagesize";
	$user		= $db->getall($sql);
	$url		= $_SERVER['PHP_SELF'];
	$showpage	= Core_Page::adminpage($total,$pagesize,$page,$url,10);
	$tpl->assign("total",$total);
	$tpl->assign("pagecount",$pagecount);
	$tpl->assign("page",$page);
	$tpl->assign("showpage",$showpage);
	$tpl->assign("user",$user);
}

function add(){
	Core_Auth::checkauth("adminadd");
	global $tpl;
	$tpl->assign("flag_checkbox",Core_Mod::checkbox("1","flag","审核"));
	//$tpl->assign("super_checkbox",Core_Mod::checkbox("0","super","系统管理员"));

	$tpl->assign("groupid_userselect",Core_Mod::db_userselect("","usergroupid","usergroup"));
}

function edit(){
	Core_Auth::checkauth("adminedit");
	global $db,$tpl;
    $id = Core_Fun::rec_post('id');
	if(!Core_Fun::isnumber($id)){
		Core_Fun::halt("ID丢失","",2);
	}
	$sql   = "SELECT * FROM ".DB_PREFIX."user WHERE userid=$id";
	$user = $db->fetch_first($sql);
	if(!$user){
		Core_Fun::halt("数据不存在","",2);
	}else{
	$tpl->assign("flag_checkbox",Core_Mod::checkbox("1","flag","审核"));
	$tpl->assign("groupid_userselect",Core_Mod::db_userselect("","usergroupid","usergroup"));
	$tpl->assign("id",$id);
	$tpl->assign("comeurl",$GLOBALS['comeurl']);
	$tpl->assign("user",$user);
	}
}

function saveadd(){
	Core_Auth::checkauth("adminadd");
	global $db;
	$loginname	= Core_Fun::rec_post('adminname',1);
	$password	= Core_Fun::rec_post('password',1);
	$usergroupid= Core_Fun::detect_number(Core_Fun::rec_post('usergroupid',1));
	$relname	= Core_Fun::rec_post('relname',1);
	$flag		= Core_Fun::detect_number(Core_Fun::rec_post('flag',1));
	$sex		= Core_Fun::rec_post('Sex',1);
	$email		= Core_Fun::rec_post('email',1);
	$address	= Core_Fun::rec_post('address',1);
	$telno		= Core_Fun::rec_post('tel',1);
	$brothday	= Core_Fun::rec_post('brothday',1);
	$memo		= Core_Fun::strip_post('memo',1);

	$founderr	= false;
	if(!Core_Fun::ischar($loginname)){
	    $founderr	= true;
		$errmsg	   .="登录帐号不能为空.<br />";
	}else{
		if(!Core_Fun::check_userstr($loginname)){
			$founderr	= true;
			$errmsg	   .="帐号格式不正确，只能由中文，字母、数字和下横下组成.<br />";
		}
	}
	if(!Core_Fun::ischar($password)){
		$founderr	= true;
		$errmsg	   .= "登录密码不能为空.<br />";	
	}

	if(!Core_Fun::ischar($sex)){
		$founderr	= true;
		$errmsg	   .= "性别不能为空.<br />";	
	}

	if(!Core_Fun::ischar($email)){
		$founderr	= true;
		$errmsg	   .= "E-mail不能为空.<br />";	
	}
	if(!Core_Fun::ischar($telno)){
		$founderr	= true;
		$errmsg	   .= "联系电话不能为空.<br />";	
	}
	if($founderr == true){
	    Core_Fun::halt($errmsg,"",1);
	}

	if(!($db->checkdata("SELECT userid FROM ".DB_PREFIX."user WHERE lower(loginname)='".strtolower($loginname)."'"))){
		$userid	= $db->fetch_newid("SELECT MAX(userid) FROM ".DB_PREFIX."user",1);
		$password	= md5($password);
		$array	= array(
			'userid'=>$userid,
			'loginname'=>$loginname,
			'password'=>$password,
			'usergroupid'=>$usergroupid,
			'flag'=>$flag,
			'realname'=>$relname,
			'regdate'=>time(),
			'sex'=>$sex,
			'email'=>$email,
			'address'=>$address,
			'telno'=>$telno,
			'brothday'=>$brothday,
			'memo'=>$memo,
	    );
		$result = $db->insert(DB_PREFIX."user",$array);
		if($result){
			Core_Command::runlog("","添加会员员成功[$loginname]",1);
			Core_Fun::halt("保存成功","ljcms_ljuser.php",0);
		}else{
			Core_Fun::halt("保存失败","",1);
		}
	}else{
		Core_Fun::halt("该帐号已存在，请填写另外一个。","",1);
	}
}

function saveedit(){
	Core_Auth::checkauth("adminedit");
	global $db;
	$id			= Core_Fun::rec_post('id',1);
    $password	= Core_Fun::rec_post('password',1);
	$usergroupid= Core_Fun::detect_number(Core_Fun::rec_post('usergroupid',1));
	$relname	= Core_Fun::rec_post('relname',1);
	$flag		= Core_Fun::detect_number(Core_Fun::rec_post('flag',1));
	$sex		= Core_Fun::rec_post('Sex',1);
	$email		= Core_Fun::rec_post('email',1);
	$address	= Core_Fun::rec_post('address',1);
	$telno		= Core_Fun::rec_post('tel',1);
	$brothday	= Core_Fun::rec_post('brothday',1);
	$memo		= Core_Fun::strip_post('memo',1);
	$founderr	= false;
	if(!Core_Fun::isnumber($id)){
	    $founderr	= true;
		$errmsg	   .= "ID丢失.<br />";
	}
	if($founderr == true){
	    Core_Fun::halt($errmsg,"",1);
	}
	$array = array(
			'usergroupid'=>$usergroupid,
			'flag'=>$flag,
			'realname'=>$relname,
			'sex'=>$sex,
			'email'=>$email,
			'address'=>$address,
			'telno'=>$telno,
			'brothday'=>$brothday,
		    'memo'=>$memo,
	);
	if($password!=""){
		$array = $array + array('password'=>md5($password));
	}

	$result = $db->update(DB_PREFIX."user",$array,"userid=$id");
	if($result){
		Core_Command::runlog("","修改会员帐号成功[id=$id]");
		Core_Fun::halt("修改成功","ljcms_ljuser.php?".$GLOBALS['comeurl']."",0);
	}else{
		Core_Fun::halt("修改失败","",2);
	}
}

function del(){
	Core_Auth::checkauth("admindel");
	$arrid  = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
	if($arrid=="" || is_null($arrid)){
		Core_Fun::halt("请选择要删除的数据","",1);
	}
	for($ii=0;$ii<count($arrid);$ii++){
        $id = Core_Fun::replacebadchar(trim($arrid[$ii]));
		if(Core_Fun::isnumber($id)){
			$GLOBALS['db']->query("DELETE FROM ".DB_PREFIX."user WHERE userid=$id");
		}
	}
	Core_Command::runlog("","删除会员成功[id=$arrid]");
	Core_Fun::halt("删除成功","ljcms_ljuser.php",0);
}

function updateajax($_id,$_action){
	Core_Auth::checkauth("adminedit");
    if(Core_Fun::isnumber($_id)){
		global $db;
		switch($_action){
			case 'flagopen';
			$db->query("UPDATE ".DB_PREFIX."user SET flag=1 WHERE userid=$_id");
			break;
			case 'flagclose';
			$db->query("UPDATE ".DB_PREFIX."user SET flag=0 WHERE userid=$_id");
			break;
			default;
			break;
		}
	}
}

/*
function savepassword(){
	Core_Auth::checkauth("editpass");
	$oldpassword		= Core_Fun::rec_post('oldpassword',1);
	$newpassword		= Core_Fun::rec_post('newpassword',1);
	$confirmpassword	= Core_Fun::rec_post('confirmpassword',1);
	$founderr			= false;
	if(!Core_Fun::ischar($oldpassword)){
	    $founderr	= true;
		$errmsg	   .= "原密码不能为空.<br />";
	}
	if(!Core_Fun::ischar($newpassword)){
	    $founderr	= true;
		$errmsg    .= "新密码不能为空.<br />";
	}else{
		if(strlen($newpassword)<4 || strlen($newpassword)>16){
			$founderr	= true;
			$errmsg	   .= "密码长度不正确.<br />";
		}
	}
	if($confirmpassword!=$newpassword){
	    $founderr	= true;
		$errmsg    .= "确认密码不正确.<br />";
	}
	if($founderr == true){
	    Core_Fun::halt($errmsg,"",1);
	}
	if(md5($oldpassword)!=$GLOBALS['libadmin']->uc_password){
		Core_Fun::halt("原始密码不正确","",1);
	}
	$array = array(
	    'password'=>md5($newpassword),
	);
	$result = $GLOBALS['db']->update(DB_PREFIX."user",$array,"lower(adminname)='".strtolower($GLOBALS['libadmin']->uc_adminname."'"));
	if($result){
		Core_Fun::set_cookie(LJCMS_COOKIENAME."_ADMINPASSWORD",md5($newpassword),10);
		Core_Command::runlog("","修改登录密码成功");
		Core_Fun::halt("密码修改成功，请记住新密码。","ljcms_ljuser.php?action=changepassword",0);
	}else{
		Core_Fun::halt("密码修改失败.","",1);	
	}
}
*/
$tpl->assign("action",$action);
$tpl->display(ADMIN_TEMPLATE."ljuser.tpl");
$tpl->assign("runtime",runtime());
$tpl->assign("copyright",$libadmin->copyright());
?>