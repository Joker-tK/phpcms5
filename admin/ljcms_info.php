<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
**/
require_once '../source/core/run.php';
require_once 'admin.inc.php';
$action		= Core_Fun::rec_post("action");
$page		= Core_Fun::detect_number(Core_Fun::rec_post("page"),1);
$scateid    = Core_Fun::detect_number(Core_Fun::rec_post("scateid"));
$sname      = Core_Fun::rec_post("sname",1);
if($page<1){$page=1;}
$comeurl	= "page=$page&scateid=$scateid&sname=".urlencode($sname)."";

if(Core_Fun::rec_post('act')=='update'){
    updateajax(Core_Fun::rec_post('id'),Core_Fun::rec_post('action'));
}
switch($action){
    case 'add';
	    add();
		break;
	case 'saveadd';
	    saveadd();
		break;
	case 'edit';
	    edit();
		break;
	case 'saveedit';
	    saveedit();
		break;
	case 'setting';
	    setting();
		break;
	case 'savesetting';
	    savesetting();
		break;
	case 'del';
	    del();
		break;
	default;
	    volist();
		break;
}

function volist(){
	Core_Auth::checkauth("infovolist");
	global $db,$tpl,$page,$scateid,$sname;
    $pagesize	= 30;
	$searchsql	= " WHERE 1=1";
	if($scateid>0){
		$searchsql .= " AND i.cateid=$scateid";
	}
	if(Core_Fun::ischar($sname)){
		$searchsql .= " AND i.title LIKE '%".$sname."%'";
	}
	$countsql	= "SELECT COUNT(i.infoid) FROM ".DB_PREFIX."info AS i".$searchsql;
    $total		= $db->fetch_count($countsql);
    $pagecount	= ceil($total/$pagesize);
	$nextpage	= $page+1;
	$prepage	= $page-1;
	$start		= ($page-1)*$pagesize;
	$sql		= "SELECT i.*,c.catename".
		          " FROM ".DB_PREFIX."info AS i".
		          " LEFT JOIN ".DB_PREFIX."infocate AS c ON i.cateid=c.cateid".
		          $searchsql." ORDER BY i.infoid DESC LIMIT $start, $pagesize";
	$info		= $db->getall($sql);
	$url		= $_SERVER['PHP_SELF'];
	$urlitem	= "scateid=$scateid&sname=".urlencode($sname)."";
	$url	   .= "?".$urlitem;
	$showpage	= Core_Page::adminpage($total,$pagesize,$page,$url,10);
	$tpl->assign("total",$total);
	$tpl->assign("pagecount",$pagecount);
	$tpl->assign("page",$page);
	$tpl->assign("showpage",$showpage);
	$tpl->assign("info",$info);
	$tpl->assign("urlitem",$urlitem);
	$tpl->assign("cate_search",Core_Mod::db_select($scateid,"scateid","infocate"));
	$tpl->assign("sname",$sname);
}

function setting(){
	Core_Auth::checkauth("infovolist");
	global $db,$tpl,$page;
    $pagesize	= 30;
	$searchsql	= " WHERE 1=1";
	$countsql	= "SELECT COUNT(infoid) FROM ".DB_PREFIX."info".$searchsql;
    $total		= $db->fetch_count($countsql);
    $pagecount	= ceil($total/$pagesize);
	$nextpage	= $page+1;
	$prepage	= $page-1;
	$start		= ($page-1)*$pagesize;
	$sql		= "SELECT * FROM ".DB_PREFIX."info".
		          $searchsql." ORDER BY infoid DESC LIMIT $start, $pagesize";
	$info		= $db->getall($sql);
	$url		= $_SERVER['PHP_SELF'];
	$showpage	= Core_Page::adminpage($total,$pagesize,$page,$url,10);
	$tpl->assign("total",$total);
	$tpl->assign("pagecount",$pagecount);
	$tpl->assign("page",$page);
	$tpl->assign("showpage",$showpage);
	$tpl->assign("info",$info);
}

function add(){
	Core_Auth::checkauth("infoadd");
	global $tpl;
	$tpl->assign("flag_checkbox",Core_Mod::checkbox("1","flag","审核"));
	$tpl->assign("elite_checkbox",Core_Mod::checkbox("","elite","推荐"));
	$tpl->assign("cate_select",Core_Mod::db_select("","cateid","infocate"));
	$tpl->assign("ugroupid_select",Core_Mod::db_usergroulive("","groupid","usergroup"));





}

function edit(){
	Core_Auth::checkauth("infoedit");
	global $db,$tpl;
    $id = Core_Fun::rec_post('id');
	if(!Core_Fun::isnumber($id)){
		Core_Fun::halt("ID丢失","",2);
	}
	$sql	= "SELECT * FROM ".DB_PREFIX."info WHERE infoid=$id";
	$info	= $db->fetch_first($sql);
	if(!$info){
		Core_Fun::halt("数据不存在","",2);
	}else{
		$info['uploadpicname'] = Core_Mod::getpicname($info['uploadfiles']);
		$info['thumbpicname'] = Core_Mod::getpicname($info['thumbfiles']);
		$tpl->assign("flag_checkbox",Core_Mod::checkbox($info['flag'],"flag","审核"));
		$tpl->assign("elite_checkbox",Core_Mod::checkbox($info['elite'],"elite","推荐"));
		$tpl->assign("cate_select",Core_Mod::db_select($info['cateid'],"cateid","infocate"));
	    $tpl->assign("ugroupid_select",Core_Mod::db_usergroulive($info['ugroupid'],"groupid","usergroup"));
		$tpl->assign("id",$id);
		$tpl->assign("comeurl",$GLOBALS['comeurl']);
	    $tpl->assign("info",$info);
	}
}

function saveadd(){
	Core_Auth::checkauth("infoadd");
	global $db;
	$cateid			= Core_Fun::detect_number(Core_Fun::rec_post('cateid',1));
	$title			= Core_Fun::rec_post('title',1);
	$thumbfiles		= Core_Fun::strip_post('thumbfiles',1);
	$uploadfiles	= Core_Fun::strip_post('uploadfiles',1);
	$hits			= Core_Fun::detect_number(Core_Fun::rec_post('hits',1));
	$summary		= Core_Fun::strip_post('summary',1);
	$content		= Core_Fun::strip_post('content',1);
	$groupid		= Core_Fun::strip_post('groupid',1);
	$exclusive		= Core_Fun::strip_post('exclusive',1);
	$flag			= Core_Fun::detect_number(Core_Fun::rec_post('flag',1));
	$elite			= Core_Fun::detect_number(Core_Fun::rec_post('elite',1));
    $delimitname	= Core_Fun::rec_post('delimitname',1);
	$metatitle		= Core_Fun::rec_post('metatitle',1);
	$metakeyword	= Core_Fun::rec_post('metakeyword',1);
	$metadescription= Core_Fun::rec_post('metadescription',1);
	$tag			= Core_Fun::rec_post('tag',1);
	$founderr		= false;
	if($cateid<1){
	    $founderr	= true;
		$errmsg	   .="请选择分类.<br />";
	}
	if(!Core_Fun::ischar($title)){
	    $founderr	= true;
		$errmsg	   .="标题不能为空.<br />";
	}
	if(!Core_Fun::ischar($content)){
	    $founderr	= true;
		$errmsg	   .="内容不能为空.<br />";
	}
	if($founderr == true){
	    Core_Fun::halt($errmsg,"",1);
	}
	if(Core_Fun::ischar($thumbfiles)){
		if(!file_exists("../".$thumbfiles)){
			$thumbfiles = $uploadfiles;
		}
	}
	$infoid	= $db->fetch_newid("SELECT MAX(infoid) FROM ".DB_PREFIX."info",1);
	$array	= array(
		'infoid'=>$infoid,
		'cateid'=>$cateid,
		'title'=>$title,
		'thumbfiles'=>$thumbfiles,
		'uploadfiles'=>$uploadfiles,
		'summary'=>$summary,
		'content'=>$content,
		'timeline'=>time(),
		'elite'=>$elite,
		'ugroupid'=>$groupid,
		'exclusive'=>$exclusive,
		'flag'=>$flag,
		'metatitle'=>$metatitle,
		'metakeyword'=>$metakeyword,
		'metadescription'=>$metadescription,
		'delimitname'=>$delimitname,
		'tag'=>$tag,
		'hits'=>$hits,
	);
	$result = $db->insert(DB_PREFIX."info",$array);
	if($result){
		Core_Command::command_savetag("info",$tag);
		Core_Command::runlog("","发布信息成功[$title]",1);
		Core_Fun::halt("保存成功","ljcms_info.php",0);
	}else{
		Core_Fun::halt("保存失败","",1);
	}
}

function saveedit(){
	Core_Auth::checkauth("infoedit");
	global $db;
	$id				= Core_Fun::rec_post('id',1);
	$cateid			= Core_Fun::detect_number(Core_Fun::rec_post('cateid',1));
	$title			= Core_Fun::rec_post('title',1);
	$thumbfiles		= Core_Fun::strip_post('thumbfiles',1);
	$uploadfiles	= Core_Fun::strip_post('uploadfiles',1);
	$hits			= Core_Fun::detect_number(Core_Fun::rec_post('hits',1));
	$summary		= Core_Fun::strip_post('summary',1);
	$content		= Core_Fun::strip_post('content',1);
	$groupid		= Core_Fun::strip_post('groupid',1);
	$exclusive		= Core_Fun::strip_post('exclusive',1);
	$flag			= Core_Fun::detect_number(Core_Fun::rec_post('flag',1));
	$elite			= Core_Fun::detect_number(Core_Fun::rec_post('elite',1));
    $delimitname	= Core_Fun::rec_post('delimitname',1);
	$metatitle		= Core_Fun::rec_post('metatitle',1);
	$metakeyword	= Core_Fun::rec_post('metakeyword',1);
	$metadescription= Core_Fun::rec_post('metadescription',1);
	$tag			= Core_Fun::rec_post('tag',1);
	$founderr	= false;
	if(!Core_Fun::isnumber($id)){
	    $founderr	= true;
		$errmsg	   .= "ID丢失.<br />";
	}
	if($cateid<1){
	    $founderr	= true;
		$errmsg	   .="请选择分类.<br />";
	}
	if(!Core_Fun::ischar($title)){
	    $founderr	= true;
		$errmsg	   .="标题不能为空.<br />";
	}
	if(!Core_Fun::ischar($content)){
	    $founderr	= true;
		$errmsg	   .="内容不能为空.<br />";
	}
	if($founderr == true){
	    Core_Fun::halt($errmsg,"",1);
	}
	if(Core_Fun::ischar($thumbfiles)){
		if(!file_exists("../".$thumbfiles)){
			$thumbfiles = $uploadfiles;
		}
	}
	$array = array(
		'cateid'=>$cateid,
		'title'=>$title,
		'thumbfiles'=>$thumbfiles,
		'uploadfiles'=>$uploadfiles,
		'summary'=>$summary,
		'content'=>$content,
		'elite'=>$elite,
		'ugroupid'=>$groupid,
		'exclusive'=>$exclusive,
		'flag'=>$flag,
		'metatitle'=>$metatitle,
		'metakeyword'=>$metakeyword,
		'metadescription'=>$metadescription,
		'delimitname'=>$delimitname,
		'tag'=>$tag,
		'hits'=>$hits,
	);
	$result = $db->update(DB_PREFIX."info",$array,"infoid=$id");
	if($result){
		Core_Command::command_savetag("info",$tag);
		Core_Command::runlog("","编辑信息成功[id=$id]");
		Core_Fun::halt("编辑成功","ljcms_info.php?".$GLOBALS['comeurl']."",0);
	}else{
		Core_Fun::halt("编辑失败","",2);
	}
}

function del(){
	Core_Auth::checkauth("infodel");
	$arrid  = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
	if($arrid=="" || is_null($arrid)){
		Core_Fun::halt("请选择要删除的数据","",1);
	}
	global $db;
	for($ii=0;$ii<count($arrid);$ii++){
        $id = Core_Fun::replacebadchar(trim($arrid[$ii]));
		if(Core_Fun::isnumber($id)){
			$sql	= "SELECT thumbfiles,uploadfiles FROM ".DB_PREFIX."info WHERE infoid=$id";
			$rows	= $db->fetch_first($sql);
			if($rows){
				if(Core_Fun::ischar($rows['thumbfiles'])){
					Core_Fun::deletefile("../".$rows['thumbfiles']);
				}
				if(Core_Fun::ischar($rows['uploadfiles'])){
					Core_Fun::deletefile("../".$rows['uploadfiles']);
				}
			}
			$db->query("DELETE FROM ".DB_PREFIX."info WHERE infoid=$id");
		}
	}
	Core_Command::runlog("","删除成功案例成功[id=$arrid]");
	Core_Fun::halt("删除成功","ljcms_info.php",0);
}

function updateajax($_id,$_action){
	Core_Auth::checkauth("infoedit");
    if(Core_Fun::isnumber($_id)){
		global $db;
		switch($_action){
			case 'flagopen';
			$db->query("UPDATE ".DB_PREFIX."info SET flag=1 WHERE infoid=$_id");
			break;
			case 'flagclose';
			$db->query("UPDATE ".DB_PREFIX."info SET flag=0 WHERE infoid=$_id");
			break;
			case 'eliteopen';
			$db->query("UPDATE ".DB_PREFIX."info SET elite=1 WHERE infoid=$_id");
			break;
			case 'eliteclose';
			$db->query("UPDATE ".DB_PREFIX."info SET elite=0 WHERE infoid=$_id");
			break;
			default;
			break;
		}
	}
}

function savesetting(){
	Core_Auth::checkauth("infoedit");
	$arrid  = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
	if($arrid=="" || is_null($arrid)){
		Core_Fun::halt("请选择要更新的数据","",1);
	}
	global $db;
	for($ii=0;$ii<count($arrid);$ii++){
        $id					= (int)($arrid[$ii]);
		$title				= Core_Fun::rec_post("title_".$id,1);
		$metatitle			= Core_Fun::rec_post("metatitle_".$id,1);
		$metakeyword		= Core_Fun::rec_post("metakeyword_".$id,1);
		$metadescription	= Core_Fun::rec_post("metadescription_".$id,1);
		if(Core_Fun::isnumber($id)){
			$array = array(
				'title'=>$title,
				'metatitle'=>$metatitle,
				'metakeyword'=>$metakeyword,
				'metadescription'=>$metadescription,
			);
			$db->update(DB_PREFIX."info",$array,"infoid=$id");
		}
	}
	Core_Fun::halt("批量更新成功","ljcms_info.php?action=setting",0);
}

$tpl->assign("action",$action);
$tpl->display(ADMIN_TEMPLATE."info.tpl");
$tpl->assign("runtime",runtime());
$tpl->assign("copyright",$libadmin->copyright());
?>