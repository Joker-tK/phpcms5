<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
**/
require_once '../source/core/run.php';
require_once 'admin.inc.php';
$action			= isset($_REQUEST['action']) ? trim($_REQUEST['action']) : "";
$comeform		= isset($_REQUEST['comeform']) ? trim($_REQUEST['comeform']) : "";
$picfolder		= isset($_REQUEST['picfolder']) ? trim($_REQUEST['picfolder']) : "";
$inputid		= isset($_REQUEST['inputid']) ? trim($_REQUEST['inputid']) : "";
$channel		= isset($_REQUEST['channel']) ? trim($_REQUEST['channel']) : "";
$thumbinput	    = isset($_REQUEST['thumbinput']) ? trim($_REQUEST['thumbinput']) : "";
$thumbflag		= isset($_REQUEST['thumbflag']) ? intval($_REQUEST['thumbflag']) : 0;
$thumbwidth		= isset($_REQUEST['thumbwidth']) ? intval($_REQUEST['thumbwidth']) : 0;
$thumbheight	= isset($_REQUEST['thumbheight']) ? intval($_REQUEST['thumbheight']) : 0;
$thumbinput2	    = isset($_REQUEST['thumbinput2']) ? trim($_REQUEST['thumbinput2']) : "";
$thumbflag2		= isset($_REQUEST['thumbflag2']) ? intval($_REQUEST['thumbflag2']) : 0;
$thumbwidth2		= isset($_REQUEST['thumbwidth']) ? intval($_REQUEST['thumbwidth']) : 0;
$thumbheight2	= isset($_REQUEST['thumbheight']) ? intval($_REQUEST['thumbheight']) : 0;
$waterflag		= isset($_REQUEST['waterflag']) ? intval($_REQUEST['waterflag']) : 0;
$picname		= isset($_REQUEST['picname']) ? trim($_REQUEST['picname']) : "";
$picurl			= isset($_REQUEST['picurl']) ? trim($_REQUEST['picurl']) : "";
$comeurl		= "comeform=$comeform&picfolder=$picfolder&inputid=$inputid&thumbinput=$thumbinput".
                  "&channel=$channel&thumbflag=$thumbflag&thumbwidth=$thumbwidth&thumbheight=$thumbheight".
				  "&waterflag=$waterflag&picname=$picname&picurl=$picurl";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?=LJCMS_CHARSET?>" />
<script language="javascript">
var right_type=new Array(".jpg",".jpeg",".png",".gif")
function checkImgType(fileURL){
	var right_typeLen=right_type.length;
	var imgUrl=fileURL.toLowerCase();
	var postfixLen=imgUrl.length;
	var len4=imgUrl.substring(postfixLen-4,postfixLen);
	var len5=imgUrl.substring(postfixLen-5,postfixLen);
	for (i=0;i<right_typeLen;i++){
		if((len4==right_type[i])||(len5==right_type[i])){
			return true;
		}
	}
}
function Dom(ID){
	var Dom=document.getElementById(ID);
	return Dom;
}
function IsUpLoad(){
	if(Dom("sf_upfile").value==""){
		alert("请先用浏览按钮选择您要上传的图片，然后点浏览按钮傍边的上传按钮上传。");
		return false;
	}
	if(checkImgType(Dom("sf_upfile").value)){
	}else{
		alert("文件格式不正确,仅jpg和gif格式的图片! BMP文件请转换成Jpg格式后在上传! ")
		return false;
	}
	Dom("UpBox").style.display="none";
	Dom("LoadingBar").style.display="block";
}
function del(){
	parent.document.getElementById('<?=$inputid?>').value='';
	<?php
	if($thumbinput!=""){
	?>
	parent.document.getElementById('<?=$thumbinput?>').value='';
	<?php
    }
	?>
}
</script>
<style type="text/css">
body {font-size: 9pt;margin: 0px;padding: 0px;background-color:#fff;}
.bj {padding-top: 3px;padding-bottom: 3px;}
form {margin: 0px;padding: 0px;}
.border {border: 1px solid #2B2B2B;font-size: 9pt;width:250px;}
.button{background:none repeat scroll 0 0 #4e6a81;
border-color:#dddddd #000000 #000000 #dddddd;
border-style:solid;
border-width:2px;
color:#FFFFFF;cursor:pointer;letter-spacing:0.1em;overflow:visible;padding:3px 15px;width:auto;cursor:pointer;text-decoration:none;}
</style>
</head>
<body >
<?php
switch($action){
	case "show";
	show();
	break;
	case "del";
	del();
	break;
	default;
	upload();
	break;
}
function show(){
	echo("".$GLOBALS['picname']."上传成功，[<a href='../".$GLOBALS['picurl']."' title='点击预览上传的图片' target='_blank'>预览</a>]，[<a href='upload.php?action=&".$GLOBALS['comeurl']."' onclick='return(confirm(\"确定要重新上传图片吗？\"))'>重新上传</a>]，[<a href='upload.php?action=del&".$GLOBALS['comeurl']."' onclick='return del();'>删除</a>]");
}
function del(){
	Core_Fun::deletefile("../".$GLOBALS['picurl']);
	echo("<script language='javascript'>window.location.href='upload.php?action=&".$GLOBALS['comeurl']."';</script>");
}
?>
<?
function upload(){
?>
<div id="UpBox" style="display:block">
<form action="uploadaction.php" method="post" enctype="multipart/form-data" name="form1" id="form1" onSubmit="return IsUpLoad();">
<table border="0" cellspacing="0" cellpadding="0">
  <tr>
	<td><input name="sf_upfile" type="file"  class="border" id="sf_upfile"  style="" /></td>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<input type="submit" name="InputUpload" id="InputUpload" value="上 传" style="" />
    <input name="comeform"  type="hidden" value="<?=$GLOBALS['comeform']?>" />
    <input name="picfolder"  type="hidden" value="<?=$GLOBALS['picfolder']?>" />
    <input name="inputid"  type="hidden" value="<?=$GLOBALS['inputid']?>" />
	<input name="thumbinput"  type="hidden" value="<?=$GLOBALS['thumbinput']?>" />
	<input name="channel"  type="hidden" value="<?=$GLOBALS['channel']?>" />
    <input name="thumbflag"  type="hidden" value="<?=$GLOBALS['thumbflag']?>" />
    <input name="thumbwidth"  type="hidden" value="<?=$GLOBALS['thumbwidth']?>" />
    <input name="thumbheight"  type="hidden" value="<?=$GLOBALS['thumbheight']?>" />
	<input name="waterflag"  type="hidden" value="<?=$GLOBALS['waterflag']?>" />
	<input name="thumbinput2"  type="hidden" value="<?=$GLOBALS['thumbinput2']?>" />
	<input name="thumbflag2"  type="hidden" value="<?=$GLOBALS['thumbflag2']?>" />
    <input name="thumbwidth2"  type="hidden" value="<?=$GLOBALS['thumbwidth2']?>" />
    <input name="thumbheight2"  type="hidden" value="<?=$GLOBALS['thumbheight2']?>" />
    </td>
  </tr>
</table>
</form>
</div>
<div id="LoadingBar" style="display:none" class="bj"><img src="liangjingcms/images/uploading.gif" alt="文件上传中，请稍后..." width="220" height="19" border="0" /></div>
<?
}		
?>
</body>
</html>