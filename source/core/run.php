<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.09.15
**/
set_time_limit(0);
error_reporting(E_ALL & ~ E_NOTICE);
//error_reporting(E_ERROR);
if(PHP_VERSION<"5.3"){
    set_magic_quotes_runtime(0);
}
$chency_mic_time  = explode(' ', microtime());
$chency_starttime = $chency_mic_time[1] + $chency_mic_time[0];
define('SYS_DEBUG', FALSE);
define('PHP_KINGFISHER', TRUE);
define('CHENCY_ROOT', substr(dirname(__FILE__), 0, -11));
define('MAGIC_QUOTES_GPC', get_magic_quotes_gpc());
define('LJCMS_KEY','P1H2P3O4E5C6O7O8TM');
if(PHP_VERSION < '4.1.0') {
	$_GET     = &$HTTP_GET_VARS;
	$_POST    = &$HTTP_POST_VARS;
	$_COOKIE  = &$HTTP_COOKIE_VARS;
	$_SERVER  = &$HTTP_SERVER_VARS;
	$_ENV     = &$HTTP_ENV_VARS;
	$_FILES   = &$HTTP_POST_FILES;
}
//echo(CHENCY_ROOT.'./source/core/class.timer.php');
require_once CHENCY_ROOT.'./source/core/class.timer.php';
Core_Timer::start();
require_once CHENCY_ROOT.'./source/conf/db.inc.php';
require_once CHENCY_ROOT.'./source/conf/config.inc.php';
require_once CHENCY_ROOT.'./source/conf/version.inc.php';
require_once CHENCY_ROOT.'./source/conf/var.inc.php';
require_once CHENCY_ROOT.'./source/core/core.func.php';

if(!defined('LJCMS')) {
	Core_Fun::halt("对不起，您的网站还没安装，请先安装再使用。","install/index.php",4);
}

if (isset($_REQUEST['GLOBALS']) OR isset($_FILES['GLOBALS'])){ 
	exit('Request tainting attempted.');
}
/*foreach(array('_COOKIE', '_POST', '_GET') as $_request) {
	foreach($$_request as $_key => $_value) {
		$_key{0} != '_' && $$_key = stripslashes($_value);
	}
}*/
require_once CHENCY_ROOT.'./source/core/class.mysql.php';
$db = new chency_mysql;
$db->connect(DB_HOST, DB_USER, DB_PASS, DB_DATA, DB_CHARSET, DB_PCONNECT, true);

require_once CHENCY_ROOT.'./source/core/tpl.config.php';
require_once CHENCY_ROOT.'./source/core/tpl.php';
require_once CHENCY_ROOT.'./source/core/core_multi.php';
require_once CHENCY_ROOT.'./source/core/core.image.php';
require_once CHENCY_ROOT.'./source/core/core.command.php';
require_once CHENCY_ROOT.'./source/core/core.mod.php';
require_once CHENCY_ROOT.'./source/core/core.admin.php';
require_once CHENCY_ROOT.'./source/core/core.auth.php';

$libadmin = new lib_admin;
?>