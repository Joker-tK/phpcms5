<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.09.15
**/
if(!defined('PHP_KINGFISHER')) {
	exit('Access Denied');
}
class Core_Mod{

	protected static $config = array();
	protected static $obj    = NULL;
	protected static $var	 = NUll;

	public static function checkbox($inputvalue,$checkboxname,$boxtext){
		$temp = "<input type='checkbox' id='$checkboxname' name='$checkboxname' value='1'";
		if($inputvalue=="1"){
			$temp .=" checked";
		}
		$temp .= ">$boxtext";
		return $temp;
	}

	public static function var_select($inputvalue,$selectname,$varname="tagchannel"){
		self::$var = $GLOBALS['TagVars'][$varname];
		$temp   = "<select name='$selectname' id='$selectname'>";
		$temp  .= "<option value=''>==请选择==</option>";
		$array  = explode("|",self::$var);
		$loop	= "";
		for($i=0;$i<sizeof($array);$i++){
			$arrchar	= trim($array[$i]);
			$str		= explode("#",$arrchar);
			$loop = $loop ."<option value='".trim($str[0])."'";
			if(trim($inputvalue)==trim($str[0])){
				$loop = $loop ." selected";
			}
			$loop = $loop .">".$str[1]."</option>";
		}
		$temp .= $loop."</select>";
		return $temp;
	}

	/*  $Id     : db_checkbox 复选
		@params : $inputvalue 传入的值
		@params : $checkname  复选框名称
		@params : $dbtable    数据表名
		@params : $whereorders where和order by 排序
		@params : $trnum      每行显示个数
		@params : $width      HTML宽
		@params : $css        HTML CSS样式名
	*/
	public static function db_checkbox($inputvalue,$checkname,$dbtable,$whereorders="",$trnum=5,$width="98%",$css="hback"){
		self::$obj = $GLOBALS['db'];
		$temp = $loop = $fieldvalue = $fieldtext ="";
		$fieldvalue = "cateid";
		$fieldtext  = "catename";
		if(!Core_Fun::ischar($whereorders)){
			$whereorders = " WHERE flag=1 ORDER BY orders ASC";
		}
		$sql	= "SELECT ".$fieldvalue.",".$fieldtext." FROM ".DB_PREFIX.$dbtable."".$whereorders;
		$rows	= self::$obj->getall($sql);
		foreach($rows as $key=>$value){
			$loop = $loop ." <td class='".$css."' width='20%'>";
			$loop = $loop ." <input type='checkbox' name='".$checkname."[]' id='".$checkname."[]' value='".intval($value[$fieldvalue])."'";
			if(Core_Fun::ischar($inputvalue)){
				if(Core_Fun::foundinarr($inputvalue,trim($value[$fieldvalue]),",")){
					$loop .= " checked";
				}
			}
			$loop .= " /> ".trim($value[$fieldtext])."";
			$loop .= " </td>";
			if(($i+1)%($trnum)==0){
				$loop .= "</tr><tr>";
			}	
		}
		$temp  = "<table width=$width border='0' align='left' cellpadding='0' cellspacing='0'>";
		$temp .= "  <tr>";
		$temp .= $loop;
		$temp .= "  </tr>";
		$temp .= "</table>";
		return $temp;
	}
	/* $Id    db select 下拉选 
	 @params  $inputvalue   : 传入当前值
	 @params  $selectname   : SELECT 名称
	 @params  $dbtable      : 表名
	 @params  $text         : text提示
	 */
	public static function db_select($inputvalue,$selectname,$dbtable,$text="==请选择=="){
		self::$obj = $GLOBALS['db'];
		if($dbtable=="authgroup"){
			$fieldopvalue = "groupid";
			$fieldoptext  = "groupname";
			$where        = " WHERE flag=1 ORDER BY orders ASC";
		} elseif($dbtable=='skin'){
			$fieldopvalue = "skinid";
			$fieldoptext  = "skinname";
			$where        = " ORDER BY skinid ASC";
		} elseif($dbtable=='adszone'){
			$fieldopvalue = "zoneid";
			$fieldoptext  = "zonename";
			$where        = " WHERE flag=1 ORDER BY orders ASC";
		} else{
			$fieldopvalue = "cateid";
			$fieldoptext  = "catename";
			$where        = " WHERE flag=1 ORDER BY orders ASC";
		}
		$temp  = "<select name='$selectname' id='$selectname'>";
		$temp .= "<option value=''>$text</option>";
		$sel_sql	= "SELECT $fieldopvalue,$fieldoptext FROM ".DB_PREFIX.$dbtable.$where;
		$sel		= self::$obj->getall($sel_sql);
		$loop		= "";
		foreach($sel as $key=>$value){
			$loop .= "<option value=\"".$value[$fieldopvalue]."\"";
			if(trim($inputvalue)==trim($value[$fieldopvalue])){
				$loop .= " selected";
			}
			$loop .= ">".$value[$fieldoptext]."</option>";
		}
		$temp .= $loop."</select>";
		return $temp;
	}
/*
     function viewisshow 判断会员查看权限 
	 @params  $level   : 权限ID
	 @params  $gpurview   : 权限值
	 @params  $dbtable      : 表名
*/
public static function viewisshow($level,$gpurview){
      global $db,$gpurviewlv;
		$gpurviews=$_SESSION["gpurview"];
		if(trim($gpurviews)=="")
		{
		$_SESSION["gpurview"]=0;
		$gpurviews=0;
		}
		$sql  = "SELECT gpurview from ".DB_PREFIX."usergroup where level='$level' AND flag=1";
			$rows = $db->fetch_first($sql);
			if($rows){
		$gpurviewlv	= $rows['gpurview'];
			}
       $viewflg = 1;
         if(trim($gpurview)==">="){
               if($gpurviews>=$gpurviewlv){
                  $viewflg = 1;
                 }
                else
				{
				$viewflg=2;
				}
		}
				if(trim($gpurview)=="="){

				if($gpurviews==$gpurviewlv){
					$viewflg = 1;
					}
					else
					{
				   $viewflg=2;
					}
		}
				return $viewflg;
}

/*
     function db_userselect 显示会员级别 
	 @params  $inputvalue   : 传入当前值
	 @params  $selectname   : SELECT 名称
	 @params  $dbtable      : 表名
	 @params  $text         : text提示
*/
public static function db_userselect($inputvalue,$selectname,$dbtable,$text="==请选择==")
	{
	self::$obj = $GLOBALS['db'];

			$fieldopvalue = "usergroupid";
			$fieldoptext  = "grupname";
			$where        = " WHERE flag=1 ORDER BY $fieldopvalue ASC";

		$temp  = "<select name='$selectname' id='$selectname'>";
		$temp .= "<option value='0'>$text</option>";
		$sel_sql	= "SELECT $fieldopvalue,$fieldoptext FROM ".DB_PREFIX.$dbtable.$where;
		$sel		= self::$obj->getall($sel_sql);
		$loop		= "";
		foreach($sel as $key=>$value){
			$loop .= "<option value=\"".$value[$fieldopvalue]."\"";
			if(trim($inputvalue)==trim($value[$fieldopvalue])){
				$loop .= " selected";
			}
			$loop .= ">".$value[$fieldoptext]."</option>";
		}
		$temp .= $loop."</select>";
		return $temp;

}


/*
     function db_usergroulive 信息存入权限 
	 @params  $inputvalue   : 传入当前值
	 @params  $selectname   : SELECT 名称
	 @params  $dbtable      : 表名
*/

public static function db_usergroulive($inputvalue,$selectname,$dbtable)
	{
	self::$obj = $GLOBALS['db'];

			$fieldopvalue = "level";
			$fieldoptext  = "grupname";
			$where        = " WHERE flag=1 ORDER BY gpurview ASC";

		$temp  = "<select name='$selectname' id='$selectname'>";
		$sel_sql	= "SELECT $fieldopvalue,$fieldoptext FROM ".DB_PREFIX.$dbtable.$where;
		$sel		= self::$obj->getall($sel_sql);
		$loop		= "";
		foreach($sel as $key=>$value){
			$loop .= "<option value=\"".$value[$fieldopvalue]."\"";
			if(trim($inputvalue)==trim($value[$fieldopvalue])){
				$loop .= " selected";
			}
			$loop .= ">".$value[$fieldoptext]."</option>";
		}
		$temp .= $loop."</select>";
		return $temp;

}
	/* 获取图片名称名称 */
	public static function getpicname($picurl){
		if(Core_Fun::ischar($picurl)){
			$arr_t = explode("/",$picurl);
			return $arr_t[max(array_flip($arr_t))];
		}else{
			return "";
		}
	}
	/**
		@ 无限级分类_下拉选择框
		@ param string $table 表
		@ param string/int $inputvalue 当前值
		@ param string $selectname select名称
		@ params $text -- 文本提示
	*/
	public static function tree_select($table,$inputvalue,$selectname,$text="==选择=="){
		self::$obj = $GLOBALS['db'];
		$temp  = "<select name='$selectname' id='$selectname'>";
		$temp .= "<option value=''>$text</option>";
		$parent_sql  = "SELECT cateid,catename FROM ".DB_PREFIX.$table." WHERE flag=1 AND parentid=0 ORDER BY orders ASC";
		$parent_query = self::$obj->query($parent_sql);
		while($parent_rows = self::$obj->fetch_array($parent_query)){
			$temp .="<option value='".$parent_rows['cateid']."'";
			if(Core_Fun::isnumber($inputvalue)){
				if($inputvalue == $parent_rows['cateid']){
					$temp .=" selected";
				}
			}
			$temp .=">".$parent_rows['catename']."</option>";
			$temp .= self::tree_child_select($table,$parent_rows['cateid'],$inputvalue);
		}
		$temp .= "</select>";
		return $temp;
	}
	public static function tree_child_select($s_table,$s_rootid,$s_inputvalue,$s_temp=""){
		self::$obj = $GLOBALS['db'];
		$s_temp = $s_temp;
		if(Core_Fun::isnumber($s_rootid)){
			$s_child_sql = "SELECT cateid,catename,depth FROM ".DB_PREFIX.$s_table."".
			               " WHERE parentid=$s_rootid AND flag=1 ORDER BY orders ASC";
			$s_child_query = self::$obj->query($s_child_sql);
			while($s_child_rows = self::$obj->fetch_array($s_child_query)){
				$s_temp .="<option value='".$s_child_rows['cateid']."'";
				if(Core_Fun::isnumber($s_inputvalue)){
					if($s_inputvalue == $s_child_rows['cateid']){
						$s_temp .=" selected";
					}
				}
				$s_tree = "";
				if($s_child_rows['depth']==1){
					$s_tree = "&nbsp;&nbsp;├ ";
				}else{
					for($s_ii=2;$s_ii<=$s_child_rows['depth'];$s_ii++){
						$s_tree .= "&nbsp;&nbsp;│";
					}
					$s_tree .= "&nbsp;&nbsp;├ ";
				}
				$s_temp =$s_temp.">".$s_tree.$s_child_rows['catename']."</option>";
				$s_temp = self::tree_child_select($s_table,$s_child_rows['cateid'],$s_inputvalue,$s_temp);
			}
		}
		return $s_temp;
	}
	/**
		@ 过滤和排序所有分类，返回一个带有缩进级别的数组
		@ @param   int     $spec_cat_id     上级分类ID
		@ @param   array   $arr        含有所有分类的数组
		@ param   int     $depth      深度
		@ 返回值 array
	*/
	public static function orders_cate_array($spec_cat_id, $arr){
		static $cat_options = array();
		if (isset($cat_options[$spec_cat_id])){
			return $cat_options[$spec_cat_id];
		}
		if (!isset($cat_options[0])){
			$level = $last_cat_id = 0;
			$options = $cat_id_array = $level_array = array();
			while (!empty($arr)){
				foreach ($arr AS $key => $value){
					$cat_id = $value['cateid'];
					if ($level == 0 && $last_cat_id == 0){
						if ($value['parentid'] > 0){
							break;
						}
						$options[$cat_id]              = $value;
						$options[$cat_id]['depth']     = $level;
						$options[$cat_id]['cateid']    = $cat_id;
						$options[$cat_id]['catename']  = $value['catename'];
						unset($arr[$key]);
						if ($value['has_children'] == 0){
							continue;
						}
						$last_cat_id  = $cat_id;
						$cat_id_array = array($cat_id);
						$level_array[$last_cat_id] = ++$level;
						continue;
					}
					if ($value['parentid'] == $last_cat_id){
						$options[$cat_id]              = $value;
						$options[$cat_id]['depth']     = $level;
						$options[$cat_id]['cateid']    = $cat_id;
						$options[$cat_id]['catename']  = $value['catename'];
						unset($arr[$key]);
						if ($value['has_children'] > 0){
							if (end($cat_id_array) != $last_cat_id){
								$cat_id_array[] = $last_cat_id;
							}
							$last_cat_id    = $cat_id;
							$cat_id_array[] = $cat_id;
							$level_array[$last_cat_id] = ++$level;
						}
					}
					elseif ($value['parentid'] > $last_cat_id){
						break;
					}
				}
				$count = count($cat_id_array);
				if ($count > 1){
					$last_cat_id = array_pop($cat_id_array);
				}elseif ($count == 1){
					if ($last_cat_id != end($cat_id_array)){
						$last_cat_id = end($cat_id_array);
					}else{
						$level = 0;
						$last_cat_id = 0;
						$cat_id_array = array();
						continue;
					}
				}
				if ($last_cat_id && isset($level_array[$last_cat_id])){
					$level = $level_array[$last_cat_id];
				}else{
					$level = 0;
				}
			}
			$cat_options[0] = $options;

		}else{
			$options = $cat_options[0];
		}
		if (!$spec_cat_id){
			return $options;
		}else{
			if (empty($options[$spec_cat_id])){
				return array();
			}
			$spec_cat_id_level = $options[$spec_cat_id]['level'];
			foreach ($options AS $key => $value){
				if ($key != $spec_cat_id){
					unset($options[$key]);
				}else{
					break;
				}
			}
			$spec_cat_id_array = array();
			foreach ($options AS $key => $value){
				if (($spec_cat_id_level == $value['depth'] && $value['cateid'] != $spec_cat_id) ||
					($spec_cat_id_level > $value['depth'])){
					break;
				}else{
					$spec_cat_id_array[$key] = $value;
				}
			}
			$cat_options[$spec_cat_id] = $spec_cat_id_array;
			return $spec_cat_id_array;
		}
	}
	/**
		@ 无限级分类 sql查询语句 组合子类ID (文章分类/物品分类)
		@ param  varchar $s_table  表名
		@ param  varchar $s_as     别名 多表关联的表别名 select a.* from xxx as a left join ....;  
		@ param  int     $s_rootid  起始节点ID
		@ param  varchar $s_sqlstr SQL组合语句
		@ 返回值 sql string 格式如 or cateid=1 or cateid=2
	*/
	public static function build_childsql($s_table,$s_as="",$s_rootid,$s_sqlstr=""){
		self::$obj = $GLOBALS['db'];
		$s_sqlstr = $s_sqlstr;
		if(Core_Fun::isnumber($s_rootid)){
			$child_sql = "SELECT cateid FROM ".DB_PREFIX.$s_table." WHERE parentid=$s_rootid";
			$child_rows = self::$obj->getall($child_sql);
			foreach($child_rows as $s_key => $s_value){
				if($s_as!=""){
					$s_sqlstr = $s_sqlstr." OR ".$s_as.".cateid=".$s_value['cateid']."";
				}else{
					$s_sqlstr = $s_sqlstr." OR cateid=".$s_value['cateid']."";
				}
				$s_sqlstr = self::build_childsql($s_table,$s_as,$s_value['cateid'],$s_sqlstr);
			}
		}
		return $s_sqlstr;
	}
	/* count */
	public static function infoCount(){
		self::$obj = $GLOBALS['db'];
		return self::$obj->fetch_count("SELECT COUNT(infoid) FROM ".DB_PREFIX."info WHERE flag=1");
	}
	public static function articleCount(){
		self::$obj = $GLOBALS['db'];
		return self::$obj->fetch_count("SELECT COUNT(articleid) FROM ".DB_PREFIX."article WHERE flag=1");
	}
	public static function productCount(){
		self::$obj = $GLOBALS['db'];
		return self::$obj->fetch_count("SELECT COUNT(productid) FROM ".DB_PREFIX."product WHERE flag=1");
	}
	public static function solutionCount(){
		self::$obj = $GLOBALS['db'];
		return self::$obj->fetch_count("SELECT COUNT(solutionid) FROM ".DB_PREFIX."solution WHERE flag=1");
	}
	public static function caseCount(){
		self::$obj = $GLOBALS['db'];
		return self::$obj->fetch_count("SELECT COUNT(caseid) FROM ".DB_PREFIX."case WHERE flag=1");
	}
	public static function downloadCount(){
		self::$obj = $GLOBALS['db'];
		return self::$obj->fetch_count("SELECT COUNT(downid) FROM ".DB_PREFIX."download WHERE flag=1");
	}
	public static function userCount(){
		self::$obj = $GLOBALS['db'];
		return self::$obj->fetch_count("SELECT COUNT(userid) FROM ".DB_PREFIX."user WHERE flag=1");
	}

	public static function guestbookCount(){
		self::$obj = $GLOBALS['db'];
		return self::$obj->fetch_count("SELECT COUNT(bookid) FROM ".DB_PREFIX."guestbook WHERE flag=1");
	}

	public static function userguestbookcount($userid){
		self::$obj = $GLOBALS['db'];
		return self::$obj->fetch_count("SELECT COUNT(bookid) FROM ".DB_PREFIX."guestbook WHERE userid=$userid and flag=1");
	}

	public static function userAppjobcount($userid){
		self::$obj = $GLOBALS['db'];
		return self::$obj->fetch_count("SELECT COUNT(aid) FROM ".DB_PREFIX."applyjob WHERE userid=$userid and flag=1");
	}
	public static function proorderCount(){
		self::$obj = $GLOBALS['db'];
		return self::$obj->fetch_count("SELECT COUNT(id) FROM ".DB_PREFIX."order WHERE 1=1 and flag=1");
	}



	public static function userproorderCount($userid){
		self::$obj = $GLOBALS['db'];
		return self::$obj->fetch_count("SELECT COUNT(id) FROM ".DB_PREFIX."order WHERE 1=1 and userid=$userid and flag=1");
	}



	/*
	 @Id   更新所有子类的Depth(深度) 支持无限级
	 @params $rootid    -- 当前节点ID
	 @params $rootdepth -- 当前节点深度
	 @params $dbtable   -- 表名
	 @update 2011.08.26
	*/
	public static function update_child_depth($rootid,$rootdepth,$dbtable){
		self::$obj		= $GLOBALS['db'];
		$child_depth	= 0;
		$child_depth	= ($rootdepth+1);
		$child_sql		= "SELECT cateid FROM ".DB_PREFIX.$dbtable." WHERE parentid=$rootid";
		$child_rows		= self::$obj->getall($child_sql);
		foreach($child_rows as $key=>$value){
			self::$obj->update(DB_PREFIX.$dbtable,array('depth'=>$child_depth),"cateid=".$value['cateid']."");
			self::update_child_depth($value['cateid'],$child_depth,$dbtable);
		}
	}
	public static function exist_child($parentid,$dbtable){
		self::$obj		= $GLOBALS['db'];
		$child_counts	= self::$obj->fetch_count("SELECT COUNT(cateid) FROM ".DB_PREFIX.$dbtable." WHERE parentid=$parentid");
		if($child_counts>0){
			return true;
		}else{
			return false;
		}
	}
}
?>