<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.09.15
**/
if(!defined('PHP_KINGFISHER')) {
	exit('Access Denied');
}
class Core_Page{
	public static $config = array();
	/**
		分页函数，使用样式表显示 第一页 [1][2][3]...[4] 最后一页 后台中心使用
		@param $num     -- 总数
		@param $perpage -- 每页显示数量
		@param $curpage -- 当前页
		@param $mpurl   -- URL地址
		@param $maxpage -- 最大页 
	*/
	public static function adminpage($num,$perpage,$curr_page,$mpurl,$maxpage){   
		$multipage ='';
		$mpurl .= strpos($mpurl, '?') ? '&amp;' : '?';
		if($num>$perpage){   
			$page    = $maxpage;   
			$offset  = floor($page * 0.5);
			$pages   = ceil($num/$perpage);   
			$from    = $curr_page -$offset;   
			$to      = $curr_page + $page - $offset - 1;   
			if($page > $pages){
				$from = 1;   
				$to   = $pages;
			}else{
				if($from<1){
					$to   = $curr_page + 1 - $from;
					$from =1;
					if(($to - $from)<$page && ($to - $from) < $pages){
						$to = $page;
					}
				}elseif($to>$pages){   
					$from = $curr_page - $pages + $to;
					$to   = $pages;
					if(($to - $from) < $page && ($to - $from) < $pages) {
						$from = $pages - $page + 1;
					}
				}
			}
			$multipage .="<td align='center' class='page_redirect' style='cursor:pointer' onmouseover=\"this.className='on_page_redirect';\" onmouseout=\"this.className='page_redirect';\" onclick=\"window.location.href='".$mpurl."page=1';\" title='首页'><img src='liangjingcms/images/page_home.gif'></td>";
			for($i=$from;$i<=$to;$i++){   
				if($i!=$curr_page){
					$multipage.="<td align='center' class='page_number' style='cursor:pointer' onmouseover=\"this.className='on_page_number';\" onmouseout=\"this.className='page_number';\" onclick=\"window.location.href='".$mpurl."page=".$i."';\" title='第".$i."页'>".$i."</td>";
				}else{
					$multipage.="<td align='center' class='page_curpage' title='第".$i."页'>".$i."</td>";
				}
			}
			$multipage.=$pages > $page ? "<td align='center' class='page_redirect' style='cursor:pointer' onmouseover=\"this.className='on_page_redirect';\" onmouseout=\"this.className='page_redirect';\" onclick=\"window.location.href='".$mpurl."page=".$pages."';\" title='尾页'><img src='liangjingcms/images/page_end.gif'></td>":"<td align='center' class='page_redirect' style='cursor:pointer' onmouseover=\"this.className='on_page_redirect';\" onmouseout=\"this.className='page_redirect';\" onclick=\"window.location.href='".$mpurl."page=".$pages."';\" title='尾页'><img src='liangjingcms/images/page_end.gif'></td>";
			
			$multipage .="<td align='center'><input name='page' title='输入页码 按回车可跳转' type='text'  class='page_input' onkeypress=\"if(event.keyCode==13) window.location.href='".$mpurl."page='+value\" /></td>";
		}
		if(!$pages){
			$recordnav = "<td align='center' class='page_total' title='总记录/每页".$perpage."个'>&nbsp;".$num."&nbsp;</td>";
		}else{
			$recordnav = "<td align='center' class='page_total' title='总记录/每页".$perpage."个'>&nbsp;".$num."&nbsp;</td>";
			$recordnav.= "<td align='center' class='page_pages' title='当前页码/总页码'>&nbsp;".$curr_page."/".$pages."&nbsp;</td>";
		}
		$tabdiv.= "  <div style='float:center;'>";
		$tabdiv.= "    <table border='0' cellpadding='0' cellspacing='1'>";
		$tabdiv.= "      <tr>";
		$tabdiv = $tabdiv.$recordnav;
		$tabdiv = $tabdiv.$multipage;
		$tabdiv.= "      </tr>";
		$tabdiv.= "    </table>";
		$tabdiv.= "  </div>";

		return $tabdiv;
	}

	/**
		分页函数，前台通用分页 上一页 [1][2][3][4] 下一页
		@param $channel -- 频道 格式 product,info
		@param $cid     -- 分类ID
		@param $num     -- 总数
		@param $perpage -- 每页显示数量pagesize
		@param $curpage -- 当前页
		@param $mpurl   -- URL地址
		@param $maxpage -- 最大页 
		$showphpurl -- 是否强制显示PHP URL格式 1--Y,0--N


				product/page-1.html
				product/cat-1-1.html
				product-page-1.html
				product-cat-1-1.html

	*/
	public static function volistpage($channel,$cid,$num,$perpage,$curr_page,$mpurl,$maxpage,$showphpurl=0){
		self::$config = $GLOBALS['config'];
		$multipage ='';
		$mpurl .= strpos($mpurl, '?') ? '&amp;' : '?';
		$pagepath = PATH_URL.$channel;
		if($num>$perpage){   
			$page    = $maxpage;   
			$pages   = ceil($num/$perpage);
			$offset  = floor($page * 0.5);
			$from    = $curr_page -$offset;   
			$to      = $curr_page + $page - $offset - 1;   
			if($page > $pages){
				$from = 1;   
				$to   = $pages;
			}else{
				if($from<1){
					$to   = $curr_page + 1 - $from;
					$from =1;
					if(($to - $from)<$page && ($to - $from) < $pages){
						$to = $page;
					}
				}elseif($to>$pages){   
					$from = $curr_page - $pages + $to;
					$to   = $pages;
					if(($to - $from) < $page && ($to - $from) < $pages) {
						$from = $pages - $page + 1;
					}
				}
			}

			if($curr_page>1){
				if(intval($showphpurl)==1){
					$tippage = "<a href=\"".$mpurl."page=".($curr_page-1)."\">上一页</a>";
				}else{
					if(self::$config['htmltype']=='php'){
						$tippage = "<a href=\"".$mpurl."page=".($curr_page-1)."\">上一页</a>";
					}else{
						if(self::$config['routeurltype']==1){
							if(intval($cid)>0){
								$tippage = "<a href=\"".$pagepath."-cat-".$cid."-".($curr_page-1).".html\">上一页</a>";
							}else{
								$tippage = "<a href=\"".$pagepath."-page-".($curr_page-1).".html\">上一页</a>";
							}
						}else{
							if(intval($cid)>0){
								$tippage = "<a href=\"".$pagepath."/cat-".$cid."-".($curr_page-1).".html\">上一页</a>";
							}else{
								$tippage = "<a href=\"".$pagepath."/page-".($curr_page-1).".html\">上一页</a>";
							}

						}
					}
				}
			}

			for($i=$from;$i<=$to;$i++){   
				if($i!=$curr_page){
					if($showphpurl==1){
						$multipage .= "<a href=\"".$mpurl."page=".$i."\">".$i."</a>";
					}else{
						if(self::$config['htmltype']=='php'){
							$multipage .= "<a href=\"".$mpurl."page=".$i."\">".$i."</a>";
						}else{
							if(self::$config['routeurltype']==1){
								if(intval($cid>0)){
									$multipage .= "<a href=\"".$pagepath."-cat-".$cid."-".$i.".html\">".$i."</a>";
								}else{
									$multipage .= "<a href=\"".$pagepath."-page-".$i.".html\">".$i."</a>";
								}
							}else{
								if(intval($cid>0)){
									$multipage .= "<a href=\"".$pagepath."/cat-".$cid."-".$i.".html\">".$i."</a>";
								}else{
									$multipage .= "<a href=\"".$pagepath."/page-".$i.".html\">".$i."</a>";
								}
							}
						}
					}
				}else{
					if($showphpurl==1){
						$multipage .= "<a href=\"".$mpurl."page=".$i."\" class='currPage'>".$i."</a>";
					}else{
						if(self::$config['htmltype']=='php'){
							$multipage .= "<a href=\"".$mpurl."page=".$i."\" class='currPage'>".$i."</a>";
						}else{
							if(self::$config['routeurltype']==1){
								if(intval($cid)>0){
									$multipage .= "<a href=\"".$pagepath."-cat-".$cid."-".$i.".html\" class='currPage'>".$i."</a>";
								}else{
									$multipage .= "<a href=\"".$pagepath."-page-".$i.".html\" class='currPage'>".$i."</a>";
								}
							}else{
								if(intval($cid)>0){
									$multipage .= "<a href=\"".$pagepath."/cat-".$cid."-".$i.".html\" class='currPage'>".$i."</a>";
								}else{
									$multipage .= "<a href=\"".$pagepath."/page-".$i.".html\" class='currPage'>".$i."</a>";
								}
							}
						}
					}
				}
			}
			
			$tippage = $tippage.$multipage;

			if($pages>1 && $pages!=$curr_page){
				if($showphpurl==1){
					$tippage .= "<a href=\"".$mpurl."page=".($curr_page+1)."\">下一页</a>";
				}else{
					if(self::$config['htmltype']=='php'){
						$tippage .= "<a href=\"".$mpurl."page=".($curr_page+1)."\">下一页</a>";
					}else{
						if(self::$config['routeurltype']==1){
							if(intval($cid)>0){
								$tippage .= "<a href=\"".$pagepath."-cat-".$cid."-".($curr_page+1).".html\">下一页</a>";
							}else{
								$tippage .= "<a href=\"".$pagepath."-page-".($curr_page+1).".html\">下一页</a>";
							}
						}else{
							if(intval($cid)>0){
								$tippage .= "<a href=\"".$pagepath."/cat-".$cid."-".($curr_page+1).".html\">下一页</a>";
							}else{
								$tippage .= "<a href=\"".$pagepath."/page-".($curr_page+1).".html\">下一页</a>";
							}
						}
					}
				}
			}
			return $tippage;
			
		}else{
			return "";
		}
	}
}
?>