<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.09.15
**/
if(!defined('PHP_KINGFISHER')) {
	exit('Access Denied');
}
class Core_Timer{ 
    public static $_starttime;
    public static $_stoptime;
    public static $_spendtime;

    public static function getmicrotime()
	{
        list($usec,$sec)=explode(" ",microtime());
        return ((float)$usec + (float)$sec);
	}

	public static function start()
	{
		self::$_starttime = self::getmicrotime();
	}

    public static function display(){
		self::$_stoptime = self::getmicrotime();
		self::$_spendtime = self::$_stoptime-self::$_starttime;
        return round(self::$_spendtime,4);
	}
}
?>