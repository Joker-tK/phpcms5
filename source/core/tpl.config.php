<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.09.15
**/
if(!defined('PHP_KINGFISHER')) {
	exit('Access Denied');
}
if (!file_exists(CHENCY_ROOT . 'tpl/_caches')){
	@mkdir(CHENCY_ROOT . 'tpl/_caches', 0777);
	@chmod(CHENCY_ROOT . 'tpl/_caches', 0777);
}
if (!file_exists(CHENCY_ROOT . 'tpl/_compiled')){
	@mkdir(CHENCY_ROOT . 'tpl/_compiled', 0777);
	@chmod(CHENCY_ROOT . 'tpl/_compiled', 0777);
}
clearstatcache();
require_once CHENCY_ROOT.'./source/core/tpl.class.php';
$tpl = new Smarty;
$tpl->setTemplateDir(CHENCY_ROOT);
$tpl->setCacheDir(CHENCY_ROOT . 'tpl/_caches');
$tpl->setCompileDir(CHENCY_ROOT . 'tpl/_compiled');
$tpl->left_delimiter = "<!--{";
$tpl->right_delimiter = "}-->";
$tpl->setCaching(false); 
//$tpl->setCacheLifetime(120); 
$tpl->allow_php_tag = true;
$tpl->allow_php_templates = true;
$tpl->compile_check = true;
$tpl->force_compile = false;
$tpl->debugging = false;
?>