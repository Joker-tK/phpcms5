<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
**/
function smarty_modifier_replacebr($s_content){
	$s_content = str_replace("\n", "<br />", $s_content);
	return $s_content;
}
?>