<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
**/
///设置时区
date_default_timezone_set('Asia/Shanghai');
///系统安装目录
define('LJCMS_ROOT','/phpcms21/');
///系统Cookies名
define('LJCMS_COOKIENAME','OkSEsLiX');
///系统编码
define('LJCMS_CHARSET','utf-8');
///自定义链接路径
define('PATH_URL','/phpcms21/');
///前面默认模版目录
define('DEFAULT_TEMPLATE','tpl/liangjingcms/');
///后台默认模版目录
define('ADMIN_TEMPLATE','admin/liangjingcms/');
///安装标识
define('LJCMS',true);
?>