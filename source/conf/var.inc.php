<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.06.11
 * @author:    Kingfisher
**/

/* 后台管理员权限值 */
$AuthVars = array(
	'config'=>'系统配置',

	'adminvolist'=>'查看管理员',
	'adminadd'=>'添加管理员',
	'adminedit'=>'编辑管理员',
	'admindel'=>'删除管理员',
	'editpass'=>'修改登录密码',

	'groupvolist'=>'查看管理组',
	'groupadd'=>'添加管理组',
	'groupedit'=>'编辑管理组',
	'groupdel'=>'删除管理组',

	'skinvolist'=>'查看风格',
	'skinadd'=>'添加风格',
	'skinedit'=>'编辑风格',
	'skindel'=>'删除风格',

	'templatevolist'=>'查看模板文件',
	'templateedit'=>'编辑模板文件',
	'templatedel'=>'删除模板文件',

	'delimitvolsit'=>'查看自定义标签',
	'delimitadd'=>'添加自定义标签',
	'delimitedit'=>'编辑自定义标签',
	'delimitdel'=>'删除自定义标签',

	'adszonevolist'=>'查看广告标签',
	'adszoneadd'=>'添加广告标签',
	'adszoneedit'=>'编辑广告标签',
	'adszonedel'=>'删除广告标签',

	'adsvolist'=>'查看广告图片',
	'adsadd'=>'添加广告图片',
	'adsedit'=>'编辑广告图片',
	'adsdel'=>'删除广告图片',

	'jobcatevolsit'=>'查看招聘分类',
	'jobcateadd'=>'添加招聘分类',
	'jobcateedit'=>'编辑招聘分类',
	'jobcatedel'=>'删除招聘分类',

	'jobvolist'=>'查看招聘信息',
	'jobadd'=>'发布招聘信息',
	'jobedit'=>'编辑招聘信息',
	'jobdel'=>'删除招聘信息',

	'pagecatevolist'=>'查看单页分类',
	'pagecateadd'=>'添加单页分类',
	'pagecateedit'=>'编辑单页分类',
	'pagecatedel'=>'删除单页分类',

	'pagevolist'=>'查看自定义单页',
	'pageadd'=>'添加自定义单页',
	'pageedit'=>'编辑自定义单页',
	'pagedel'=>'删除自定义单页',

	'linkvolist'=>'查看友情链接',
	'linkadd'=>'添加友情链接',
	'linkedit'=>'编辑友情链接',
	'linkdel'=>'删除友情链接',

	'guestbookvolist'=>'查看留言',
	'guestbookedit'=>'回复留言',
	'guestbookdel'=>'删除留言',

	'tagsvolist'=>'查看TAGS',
	'tagsadd'=>'添加TAGS',
	'tagsedit'=>'编辑TAGS',
	'tagsdel'=>'删除TAGS',

	'infocatevolist'=>'查看新闻分类',
	'infocateadd'=>'添加新闻分类',
	'infocateedit'=>'编辑新闻分类',
	'infocatedel'=>'删除新闻分类',

	'infovolist'=>'查看新闻',
	'infoadd'=>'发布新闻',
	'infoedit'=>'编辑新闻',
	'infodel'=>'删除新闻',

	'productcatevolist'=>'查看产品分类',
	'productcateadd'=>'添加产品分类',
	'productcateedit'=>'编辑产品分类',
	'productcatedel'=>'删除产品分类',

	'productvolist'=>'查看产品',
	'productadd'=>'发布产品',
	'productedit'=>'编辑产品',
	'productdel'=>'删除产品',

	'casecatevolist'=>'查看成功案例分类',
	'casecateadd'=>'添加成功案例分类',
	'casecateedit'=>'编辑成功案例分类',
	'casecatedel'=>'删除成功案例分类',

	'casevolist'=>'查看成功案例',
	'caseadd'=>'发布成功案例',
	'caseedit'=>'编辑成功案例',
	'casedel'=>'删除成功案例',

	'solutioncatevolist'=>'查看解决方案分类',
	'solutioncateadd'=>'添加解决方案分类',
	'solutioncateedit'=>'编辑解决方案分类',
	'solutioncatedel'=>'删除解决方案分类',

	'solutionvolist'=>'查看解决方案',
	'solutionadd'=>'发布解决方案',
	'solutionedit'=>'编辑解决方案',
	'solutiondel'=>'删除解决方案',

	'downloadcatevolist'=>'查看下载分类',
	'downloadcateadd'=>'添加下载分类',
	'downloadcateedit'=>'编辑下载分类',
	'downloadcatedel'=>'删除下载分类',

	'downloadvolist'=>'查看下载',
	'downloadadd'=>'发布下载',
	'downloadedit'=>'编辑下载',
	'downloaddel'=>'删除下载',

	'databasevolist'=>'数据库管理',
	'databasebackup'=>'数据备份',
	'databaseimport'=>'数据恢复',
	'databasedel'=>'删除备份文件',

);

/* 后台TAGS频道设置 */
$TagVars = array(
	'tagchannel'=>"info#资讯内容频道|product#产品内容频道|article#文章内容频道|project#解决方案频道|case#成功案例频道|download#下载中心频道",
);
/* 前台菜单导航名称 */
$LANVAR = array(
    'index'=>'网站首页',
	'about'=>'关于我们',
	'contact'=>'联系我们',
	'sitemap'=>'网站地图',
	'link'=>'友情链接',
	'guestbook'=>'在线留言',
	'info'=>'新闻资讯',
	'case'=>'成功案例',
	'solution'=>'解决方案',
	'download'=>'下载中心',
	'product'=>'产品中心',
	'job'=>'人才招聘',
	'menbercenter'=>'会员中心',

);
?>