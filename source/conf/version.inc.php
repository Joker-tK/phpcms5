<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.06.11
 * @author:    Kingfisher
**/
define('LJCMS_VERSION','LJcms v1.2');
define('LJCMS_RELEASE',".R02012");
define('LJCMS_LICENCE','http://www.liangjing.org/');
define('LJCMS_URL','http://www.liangjing.org/');
?>