<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2011.09.07
**/
if(!defined('ALLOWGUEST')) { 
	exit('Access Denied');
}
require_once CHENCY_ROOT.'./source/module/mod.url.php';
require_once CHENCY_ROOT.'./source/module/mod.rep.php';
require_once CHENCY_ROOT.'./source/module/mod.page.php';
require_once CHENCY_ROOT.'./source/module/mod.ads.php';
require_once CHENCY_ROOT.'./source/module/mod.delimit.php';
require_once CHENCY_ROOT.'./source/module/mod.info.php';
require_once CHENCY_ROOT.'./source/module/mod.case.php';
require_once CHENCY_ROOT.'./source/module/mod.down.php';
require_once CHENCY_ROOT.'./source/module/mod.product.php';
require_once CHENCY_ROOT.'./source/module/mod.solution.php';
require_once CHENCY_ROOT.'./source/module/mod.job.php';
require_once CHENCY_ROOT.'./source/module/mod.part.php';

/* 初始化 */
Mod_Url::display_menurl();
Mod_Page::display();
Mod_Ads::display();
Mod_Delimit::display();




	/*
	  @$Id 一级分类
	  @params $where    -- 查询条件
	  @params $orderby  -- 排序
	  @params $limitnum -- 显示数量
	*/
function vo_category($where="",$orderby="",$limitnum=0){
		$obj = $GLOBALS['db'];
		$config = $GLOBALS['config'];
		$urlpath = PATH_URL;
		$where = Core_Fun::forbidchar($where);
		$orderby = Core_Fun::forbidchar($orderby);
		$query = "SELECT c.* FROM ".DB_PREFIX."category AS c".
			     " WHERE c.flag=1 and c.ismenu=1 ";
		if(Core_Fun::ischar($where)){
			$query .= " ".$where;
		}
		if(Core_Fun::ischar($orderby)){
			$query .= " ".$orderby;
		}else{
			$query .= " ORDER BY c.orders ASC";
		}
		if(intval($limitnum)>0){
			$query .= " LIMIT ".intval($limitnum)."";
		}
		$array = $obj->getall($query);
		$i = 1;
	




		foreach($array as $key=>$value){


$mystringlist='';

if($value['rootid']==0)
{

$mystringlist="<li class=\"m".$i."\"><a href=\"".$value['outurl']."\" href=\"javascript:;\">".$value['catname']."</a>";
$mystringlist=$mystringlist.vo_categorycs(" and rootid=".$value['catid']."",$i);
$mystringlist=$mystringlist."</li>"; 


}	
	
$array[$key]['urlstring']=$mystringlist;	
	
		
			
			
			
			
			$array[$key]['i'] = $i;
			$i = ($i+1);
		}
		return $array;
	}





	/*
	  @$Id 一级分类
	  @params $where    -- 查询条件
	  @params $orderby  -- 排序
	  @params $limitnum -- 显示数量
	*/
function vo_categoryscript($where="",$orderby="",$limitnum=0){
		$obj = $GLOBALS['db'];
		$config = $GLOBALS['config'];
		$urlpath = PATH_URL;
		$where = Core_Fun::forbidchar($where);
		$orderby = Core_Fun::forbidchar($orderby);
		$query = "SELECT c.* FROM ".DB_PREFIX."category AS c".
			     " WHERE c.flag=1 and c.ismenu=1 ";
		if(Core_Fun::ischar($where)){
			$query .= " ".$where;
		}
		if(Core_Fun::ischar($orderby)){
			$query .= " ".$orderby;
		}else{
			$query .= " ORDER BY c.orders ASC";
		}
		if(intval($limitnum)>0){
			$query .= " LIMIT ".intval($limitnum)."";
		}
		$array = $obj->getall($query);
		$i = 1;
	

        $scriptstringlist="<script>";


	    foreach($array as $key=>$value){


        if($value['rootid']==0)
         {

		if(vo_categorycs(" and rootid=".$value['catid']."",$i)!=""){
		$scriptstringlist=$scriptstringlist."$(\".header_menu_nav\").find(\".m".$i."\").hover(\n";
		$scriptstringlist=$scriptstringlist."function() {\n";
		$scriptstringlist=$scriptstringlist."$(\".header_menu_nav_miphone".$i."\").fadeIn(\"fast\");\n";
		$scriptstringlist=$scriptstringlist."},\n";
		$scriptstringlist=$scriptstringlist."function() {\n";
		$scriptstringlist=$scriptstringlist."$(\".header_menu_nav_miphone".$i."\").fadeOut();\n";
		$scriptstringlist=$scriptstringlist."}\n";
		$scriptstringlist=$scriptstringlist.");";
		}
		}
		  $i = ($i+1);
		}
		$scriptstringlist=$scriptstringlist."</script>\n";
		return $scriptstringlist;
	}










	/*
	  @$Id 一级分类
	  @params $where    -- 查询条件
	  @params $orderby  -- 排序
	  @params $limitnum -- 显示数量
	*/
function vo_categorymycss($where="",$orderby="",$limitnum=0){
		$obj = $GLOBALS['db'];
		$config = $GLOBALS['config'];
		$urlpath = PATH_URL;
		$where = Core_Fun::forbidchar($where);
		$orderby = Core_Fun::forbidchar($orderby);
		$query = "SELECT c.* FROM ".DB_PREFIX."category AS c".
			     " WHERE c.flag=1 and c.ismenu=1 ";
		if(Core_Fun::ischar($where)){
			$query .= " ".$where;
		}
		if(Core_Fun::ischar($orderby)){
			$query .= " ".$orderby;
		}else{
			$query .= " ORDER BY c.orders ASC";
		}
		if(intval($limitnum)>0){
			$query .= " LIMIT ".intval($limitnum)."";
		}
		$array = $obj->getall($query);
		$ii = 1;
	

$mystringlistee="<style type=\"text/css\">\n";



		foreach($array as $key=>$value){



if($value['rootid']==0)
{

$mystringlistee=$mystringlistee.".header_menu_nav_miphone".$ii." span{background-image:url(/tpl/Ljgoophone/images/webIndex_01.gif);background-repeat:no-repeat;}\n";
$mystringlistee=$mystringlistee.".header_menu_nav_miphone".$ii."{display:none;position:absolute;left:-15px;top:9px;width:130px;z-index:1003;}\n";
$mystringlistee=$mystringlistee.".header_menu_nav_miphone".$ii." p{border-width:0 1px 1px;border-style:solid;border-color:#e1e1e1;background:#fff url(/tpl/Ljgoophone/images/webIndex_05.gif) no-repeat left top;}\n";
$mystringlistee=$mystringlistee.".header_menu_nav_miphone".$ii." p a{display:block;line-height:16px;}\n";
$mystringlistee=$mystringlistee.".header_menu_nav li.m".$ii." .header_menu_nav_miphone".$ii." a{width:110px;margin:0 auto;height:50px;line-height:50px;text-indent:10px;border-bottom:1px solid #e6e4e3;color:#989898;font-family:\"Microsoft yahei\";font-size:14px;font-weight:bold;}\n";
$mystringlistee=$mystringlistee.".header_menu_nav li.m".$ii." .header_menu_nav_miphone".$ii." a:hover{width:110px;color:#f60;}\n";
$mystringlistee=$mystringlistee.".header_menu_nav_miphone".$ii." span{display:block;margin:8px 0 0 41px;background-position:-355px -67px;_background-position:-355px -64px;width:24px;height:14px;}\n";
$mystringlistee=$mystringlistee.".header_menu_nav li.m".$ii."{position:relative;z-index:1002;background-color:#fff;}\n";




}	
	


	
	
			
			
	
			$ii = ($ii+1);
		}

$mystringlistee=$mystringlistee."</style>\n";




		return $mystringlistee;
	}




	/*
	  @$Id 一级分类
	  @params $where    -- 查询条件
	  @params $orderby  -- 排序
	  @params $limitnum -- 显示数量
	*/
function vo_categorycs($wheres="",$hh){
		$objs = $GLOBALS['db'];
		$config = $GLOBALS['config'];
		$urlpath = PATH_URL;
		$wheres = Core_Fun::forbidchar($wheres);
	//	$orderbys = Core_Fun::forbidchar($orderby);
		$querys= "SELECT c.* FROM ".DB_PREFIX."category AS c".
			     " WHERE c.flag=1 and c.ismenu=1 ";
		if(Core_Fun::ischar($wheres)){
			$querys .= " ".$wheres;
		}
		
		$querys .= " ORDER BY c.orders ASC";
		
		$arrays = $objs->getall($querys);

		$mystringlists="";

		if($arrays!=null){

	    $mystringlists=$mystringlists."<div class=\"header_menu_nav_miphone".$hh."\">\n<span></span>\n<p>";
		
		foreach($arrays as $key=>$values){
		
	    $mystringlists=$mystringlists."<a href=\"".$values['outurl']."\">". $values['catname'] ."</a>\n";
		
        }	
		
        $mystringlists=$mystringlists."</p>\n</div>";	
		}
			
		return $mystringlists;
	}


/* 系统默认函数标签 */
$tpl->assign("volist_onlinechat",Mod_Part::volist_onlinechat());
$tpl->assign("volist_fontlink",Mod_Part::volist_fontlink());
$tpl->assign("volist_logolink",Mod_Part::volist_logolink());
$tpl->assign("volist_page",Mod_Page::volist());
$tpl->assign("volist_newinfo",Mod_Info::volist());
$tpl->assign("volist_infocategory",Mod_Info::category());
$tpl->assign("volist_newcase",Mod_Case::volist());
$tpl->assign("volist_casecategory",Mod_Case::category());
$tpl->assign("volist_categorys",vo_category());
$tpl->assign("volist_categorycss",vo_categorymycss());
$tpl->assign("volist_categoryscript",vo_categoryscript());
$tpl->assign("volist_newdownload",Mod_Down::volist());
$tpl->assign("volist_downloadcategory",Mod_Down::category());
$tpl->assign("volist_newjob",Mod_Job::volist());
$tpl->assign("volist_jobcategory",Mod_Job::category());
$tpl->assign("volist_newsolution",Mod_Solution::volist());
$tpl->assign("volist_solutioncategory",Mod_Solution::category());
$tpl->assign("volist_solutiontreecategory",Mod_Solution::treecategory());
$tpl->assign("volist_newproduct",Mod_Product::volist());
$tpl->assign("volist_productcategory",Mod_Product::category());
$tpl->assign("volist_producttreecategory",Mod_Product::treecategory());
?>