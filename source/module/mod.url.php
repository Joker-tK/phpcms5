<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.01.15
 * @Id         菜单栏目URL处理
**/
if(!defined('PHP_KINGFISHER')) {
	exit('Access Denied');
}
class Mod_Url{
	private static $tpl = NUll;
	private static $urlpath = NULL;
	private static $config = array();
	private static $var = array();

	public static function display_menurl(){
		self::$tpl = $GLOBALS['tpl'];
		self::$urlpath = PATH_URL;
		self::$config = $GLOBALS['config'];
		if(self::$config['htmltype']=='php'){

			self::$tpl->assign("url_index",self::$urlpath."index.php");
			self::$tpl->assign("url_about",self::$urlpath."page.php?mod=about");
			self::$tpl->assign("url_contact",self::$urlpath."page.php?mod=contact");
			self::$tpl->assign("url_sitemap",self::$urlpath."page.php?mod=sitemap");
			self::$tpl->assign("url_link",self::$urlpath."page.php?mod=link");
			self::$tpl->assign("url_guestbook",self::$urlpath."guestbook.php");
			self::$tpl->assign("url_applyjob",self::$urlpath."applyjob.php");
			self::$tpl->assign("url_productbuy",self::$urlpath."productbuy.php");
			self::$tpl->assign("url_info",self::$urlpath."info.php");
			self::$tpl->assign("url_case",self::$urlpath."case.php");
			self::$tpl->assign("url_solution",self::$urlpath."solution.php");
			self::$tpl->assign("url_download",self::$urlpath."download.php");
			self::$tpl->assign("url_product",self::$urlpath."product.php");
			self::$tpl->assign("url_job",self::$urlpath."job.php");
			self::$tpl->assign("url_member",self::$urlpath."member/index.php");


		}else{
			if(self::$config['routeurltype']==1){
                self::$tpl->assign("url_index",self::$urlpath."index.html");
				self::$tpl->assign("url_about",self::$urlpath."about.html");
				self::$tpl->assign("url_contact",self::$urlpath."contact.html");
				self::$tpl->assign("url_sitemap",self::$urlpath."sitemap.html");
				self::$tpl->assign("url_link",self::$urlpath."link.html");
				self::$tpl->assign("url_guestbook",self::$urlpath."guestbook.html");
				self::$tpl->assign("url_applyjob",self::$urlpath."applyjob.asp");
				self::$tpl->assign("url_productbuy",self::$urlpath."productbuy.php");
				self::$tpl->assign("url_info",self::$urlpath."info.html");
				self::$tpl->assign("url_case",self::$urlpath."case.html");
				self::$tpl->assign("url_solution",self::$urlpath."solution.html");
				self::$tpl->assign("url_download",self::$urlpath."download.html");
				self::$tpl->assign("url_product",self::$urlpath."product.html");
				self::$tpl->assign("url_job",self::$urlpath."job.html");
			    self::$tpl->assign("url_member",self::$urlpath."member/index.html");

			}else{
                self::$tpl->assign("url_index",self::$urlpath."index.html");
				self::$tpl->assign("url_about",self::$urlpath."about");
				self::$tpl->assign("url_contact",self::$urlpath."contact");
				self::$tpl->assign("url_sitemap",self::$urlpath."sitemap");
				self::$tpl->assign("url_link",self::$urlpath."link");
				self::$tpl->assign("url_guestbook",self::$urlpath."guestbook");
				self::$tpl->assign("url_applyjob",self::$urlpath."applyjob");
				self::$tpl->assign("url_productbuy",self::$urlpath."productbuy");
				self::$tpl->assign("url_info",self::$urlpath."info");
				self::$tpl->assign("url_case",self::$urlpath."case");
				self::$tpl->assign("url_solution",self::$urlpath."solution");
				self::$tpl->assign("url_download",self::$urlpath."download");
				self::$tpl->assign("url_product",self::$urlpath."product");
				self::$tpl->assign("url_job",self::$urlpath."job");
				self::$tpl->assign("url_member",self::$urlpath."member/index");

			}
		}
		self::langnav();
	}
	public static function langnav(){
		self::$tpl = $GLOBALS['tpl'];
		self::$urlpath = PATH_URL;
		self::$var = $GLOBALS['LANVAR'];
		foreach(self::$var as $key=>$value){
			self::$tpl->assign("lang_".$key."",$value);
		}
	}

    /* use 自定义标签和单页 系统标签处理 */
	public static function replace_menurl($string){
		if(Core_Fun::ischar($string)){
			self::$urlpath = PATH_URL;
			self::$config = $GLOBALS['config'];
			if(self::$config['htmltype']=='php'){

				$string = str_replace("{\$url_index}",self::$urlpath."index.php",$string);
				$string = str_replace("{\$url_about}",self::$urlpath."page.php?mod=about",$string);
				$string = str_replace("{\$url_contact}",self::$urlpath."page.php?mod=contact",$string);
				$string = str_replace("{\$url_sitemap}",self::$urlpath."page.php?mod=sitemap",$string);
				$string = str_replace("{\$url_link}",self::$urlpath."page.php?mod=link",$string);
				$string = str_replace("{\$url_guestbook}",self::$urlpath."guestbook.php",$string);
				$string = str_replace("{\$url_info}",self::$urlpath."info.php",$string);
				$string = str_replace("{\$url_case}",self::$urlpath."case.php",$string);
				$string = str_replace("{\$url_solution}",self::$urlpath."solution.php",$string);
				$string = str_replace("{\$url_download}",self::$urlpath."download.php",$string);
				$string = str_replace("{\$url_product}",self::$urlpath."product.php",$string);
				$string = str_replace("{\$url_job}",self::$urlpath."job.php",$string);
				$string = str_replace("{\$url_member}",self::$urlpath."member/index.php",$string);


			}else{
				if(self::$config['routeurltype']==1){
					$string = str_replace("{\$url_index}",self::$urlpath."index.html",$string);
					$string = str_replace("{\$url_about}",self::$urlpath."about.html",$string);
					$string = str_replace("{\$url_contact}",self::$urlpath."contact.html",$string);
					$string = str_replace("{\$url_sitemap}",self::$urlpath."sitemap.html",$string);
					$string = str_replace("{\$url_link}",self::$urlpath."link.html",$string);
					$string = str_replace("{\$url_guestbook}",self::$urlpath."guestbook.html",$string);
					$string = str_replace("{\$url_info}",self::$urlpath."info.html",$string);
					$string = str_replace("{\$url_case}",self::$urlpath."case.html",$string);
					$string = str_replace("{\$url_solution}",self::$urlpath."solution.html",$string);
					$string = str_replace("{\$url_download}",self::$urlpath."download.html",$string);
					$string = str_replace("{\$url_product}",self::$urlpath."product.html",$string);
					$string = str_replace("{\$url_job}",self::$urlpath."job.html",$string);
				    $string = str_replace("{\$url_member}",self::$urlpath."member/index.php",$string);



				}else{
					$string = str_replace("{\$url_index}",self::$urlpath."index.html",$string);
					$string = str_replace("{\$url_about}",self::$urlpath."about",$string);
					$string = str_replace("{\$url_contact}",self::$urlpath."contact",$string);
					$string = str_replace("{\$url_sitemap}",self::$urlpath."sitemap",$string);
					$string = str_replace("{\$url_link}",self::$urlpath."link",$string);
					$string = str_replace("{\$url_guestbook}",self::$urlpath."guestbook",$string);
					$string = str_replace("{\$url_info}",self::$urlpath."info",$string);
					$string = str_replace("{\$url_case}",self::$urlpath."case",$string);
					$string = str_replace("{\$url_solution}",self::$urlpath."solution",$string);
					$string = str_replace("{\$url_download}",self::$urlpath."download",$string);
					$string = str_replace("{\$url_product}",self::$urlpath."product",$string);
					$string = str_replace("{\$url_job}",self::$urlpath."job",$string);
		            $string = str_replace("{\$url_member}",self::$urlpath."member/index",$string);



				}
			}
			$string = self::replace_langnav($string);
		}
		return $string;
	}
	public static function replace_langnav($string){
		if(Core_Fun::ischar($string)){
			self::$var = $GLOBALS['LANVAR'];

			foreach(self::$var as $key=>$value){
				$string = str_replace("{\$lang_".$key."}",$value,$string);
			}
		}
		return $string;
	}

}
?>