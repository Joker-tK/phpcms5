<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2011.09.07
**/
if(!defined('PHP_KINGFISHER')) {
	exit('Access Denied');
}
class Mod_Rep{
	private static $obj = NULL;
	private static $tpl = NUll;
	private static $urlpath = NULL;
	private static $skinpath = NULL;
	private static $config = array();

	public static function configLabel($string){
		$new_string = NULL;
		if(Core_Fun::ischar($string)){
			self::$obj = $GLOBALS['db'];
			self::$urlpath = PATH_URL;
			self::$skinpath = PATH_URL.INDEX_TEMPLATE;
			self::$config = $GLOBALS['config'];
			$query	= "SELECT * FROM ".DB_PREFIX."config";
			$res	= self::$obj->query($query);
			$configNums = mysql_num_fields($res);
			for($i=0;$i<$configNums;$i++){
				$fieldname = mysql_field_name($res,$i);
				$string = str_replace("{\$config.".$fieldname."}",self::$config[$fieldname],$string);
			}
			$string = str_replace("{\$urlpath}",self::$urlpath,$string);
			$string = str_replace("{\$skinpath}",self::$skinpath,$string);
			$string = Mod_Url::replace_menurl($string);
		}
		return $string;
	}
}
?>