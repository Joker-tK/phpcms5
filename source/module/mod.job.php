<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.01.15
 * @Id         人才招聘
**/
if(!defined('PHP_KINGFISHER')) {
	exit('Access Denied');
}
class Mod_Job{
	private static $obj = NULL;
	private static $tpl = NUll;
	private static $urlpath = NULL;
	private static $config = array();

	/*
	  @$Id 列表
	  @params $where    -- 查询条件
	  @params $orderby  -- 排序
	  @params $limitnum -- 显示数量
	*/
	public static function volist($where="",$orderby="",$limitnum=0){
		self::$obj = $GLOBALS['db'];
		self::$tpl = $GLOBALS['tpl'];
		self::$urlpath = PATH_URL;
		self::$config = $GLOBALS['config'];
		$where = Core_Fun::forbidchar($where);
		$orderby = Core_Fun::forbidchar($orderby);
		$query  = "SELECT v.*,c.catename,c.img,c.cssname,c.target,c.linktype,c.linkurl".
			     " FROM ".DB_PREFIX."job AS v".
			     " LEFT JOIN ".DB_PREFIX."jobcate AS c ON v.cateid=c.cateid".
		         " WHERE v.flag=1";
		if(Core_Fun::ischar($where)){
			$query .= " ".$where;
		}
		if(Core_Fun::ischar($orderby)){
			$query .= " ".$orderby;
		}else{
			$query .= " ORDER BY v.jobid DESC";
		}
		if(intval($limitnum)>0){
			$query .= " LIMIT ".intval($limitnum)."";
		}else{
			$query .= " LIMIT ".intval(self::$config['jobnum'])."";
		}
		$array = self::$obj->getall($query);
		$i = 1;
		foreach($array as $key=>$value){
			if(self::$config['htmltype']=='php'){
				$array[$key]['url'] = self::$urlpath."job.php?mod=detail&id=".$value['jobid'];
				$array[$key]['caturl'] = self::$urlpath."job.php?mod=list&cid=".$value['cateid']."";
			}else{
				if(self::$config['routeurltype']==1){
					$array[$key]['url'] = self::$urlpath."job-".$value['jobid'].".html";
					$array[$key]['caturl'] = self::$urlpath."job-cat-".$value['cateid'].".html";
				}else{
					$array[$key]['url'] = self::$urlpath."job/".$value['jobid'].".html";
					$array[$key]['caturl'] = self::$urlpath."job/cat-".$value['cateid'].".html";
				}
			}
			if(intval($value['linktype'])==2){
				$array[$key]['caturl'] = $value['linkurl'];
			}
			if(intval($value['target'])==2){
				$array[$key]['target'] = "_blank";
			}else{
				$array[$key]['target'] = "_self";
			}
			if(strtolower(LJCMS_CHARSET)=="utf-8"){
				$array[$key]['sort_title'] = Core_Fun::cut_str($value['title'],self::$config['joblen']);
			}else{
				$array[$key]['sort_title'] = Core_Fun::cut_str($value['title'],self::$config['joblen'],0,"gbk");
			}
			$array[$key]['i'] = $i;
			$i = ($i+1);
		}
		return $array;
	}

	/*
	  @$Id 一级分类
	  @params $where    -- 查询条件
	  @params $orderby  -- 排序
	  @params $limitnum -- 显示数量
	*/
	public static function category($where="",$orderby="",$limitnum=0){
		self::$obj = $GLOBALS['db'];
		self::$config = $GLOBALS['config'];
		self::$urlpath = PATH_URL;
		$where = Core_Fun::forbidchar($where);
		$orderby = Core_Fun::forbidchar($orderby);
		$query = "SELECT c.* FROM ".DB_PREFIX."jobcate AS c".
			     " WHERE c.flag=1";
		if(Core_Fun::ischar($where)){
			$query .= " ".$where;
		}
		if(Core_Fun::ischar($orderby)){
			$query .= " ".$orderby;
		}else{
			$query .= " ORDER BY c.orders ASC";
		}
		if(intval($limitnum)>0){
			$query .= " LIMIT ".intval($limitnum)."";
		}
		$array = self::$obj->getall($query);
		$i = 1;
		foreach($array as $key=>$value){
			if(intval($value['linktype'])==2){
				$array[$key]['url'] = $value['linkurl'];
			}else{
				if(self::$config['htmltype']=='php'){
					$array[$key]['url'] = self::$urlpath."job.php?mod=list&cid=".$value['cateid'];
				}else{
					if(self::$config['routeurltype']==1){
						$array[$key]['url'] = self::$urlpath."job-cat-".$value['cateid'].".html";
					}else{
						$array[$key]['url'] = self::$urlpath."job/cat-".$value['cateid'].".html";
					}
				}
			}
			if(intval($value['target'])==2){
				$array[$key]['target'] = "_blank";
			}else{
				$array[$key]['target'] = "_self";
			}
			$array[$key]['i'] = $i;
			$i = ($i+1);
		}
		return $array;
	}

	/* 上一个 */
	public static function previousitem($id){
		self::$obj = $GLOBALS['db'];
		self::$config = $GLOBALS['config'];
		self::$urlpath = PATH_URL;
		$temp  = "";
		if(Core_Fun::isnumber($id)){
			$id = intval($id);
			$query = "SELECT jobid,title FROM ".DB_PREFIX."job WHERE jobid<$id".
					 " ORDER BY jobid DESC LIMIT 1";
			$rows  = self::$obj->fetch_first($query);
			if($rows){
				if(self::$config['htmltype']=='php'){
					$temp = "<a href=\"".self::$urlpath."job.php?mod=detail&id=".$rows['jobid']."\">".$rows['title']."</a>";
				}else{
					if(self::$config['routeurltype']==1){
						$temp = "<a href=\"".self::$urlpath."job-".$rows['jobid'].".html\">".$rows['title']."</a>";
					}else{
						$temp = "<a href=\"".self::$urlpath."job/".$rows['jobid'].".html\">".$rows['title']."</a>";
					}
				}
			}else{
				$temp = "没有了";
			}
		}
		return $temp;
	}

	/* 下一个 */
	function nextitem($id){
		self::$obj = $GLOBALS['db'];
		self::$config = $GLOBALS['config'];
		self::$urlpath = PATH_URL;
		$temp  = "";
		if(Core_Fun::isnumber($id)){
			$id = intval($id);
			$query = "SELECT jobid,title FROM ".DB_PREFIX."job WHERE jobid>$id".
					 " ORDER BY jobid ASC LIMIT 1";
			$rows  = self::$obj->fetch_first($query);
			if($rows){
				if(self::$config['htmltype']=='php'){
					$temp = "<a href=\"".self::$urlpath."job.php?mod=detail&id=".$rows['jobid']."\">".$rows['title']."</a>";
				}else{
					if(self::$config['routeurltype']==1){
						$temp = "<a href=\"".self::$urlpath."job-".$rows['jobid'].".html\">".$rows['title']."</a>";
					}else{
						$temp = "<a href=\"".self::$urlpath."job/".$rows['jobid'].".html\">".$rows['title']."</a>";
					}
				}
			}else{
				$temp = "没有了";
			}
		}
		return $temp;
	}

}
?>