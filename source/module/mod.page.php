<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Id         自定义单页
**/
if(!defined('PHP_KINGFISHER')) {
	exit('Access Denied');
}
class Mod_Page{
	private static $obj = NULL;
	private static $tpl = NUll;
	private static $urlpath	= NULL;
	private static $config = array();

	/* 显示分类标签 */
	public static function display(){
		self::$obj = $GLOBALS['db'];
		self::$tpl = $GLOBALS['tpl'];

		$query = "SELECT cateid FROM ".DB_PREFIX."pagecate WHERE flag=1 ORDER BY orders ASC";
		$array  = self::$obj->getall($query);
		foreach($array as $key=>$value){
			self::$tpl->assign("page_block".$value['cateid'],self::volistblock($value['cateid']));
		}
	}
	public static function volistblock($cid){
		self::$obj = $GLOBALS['db'];
		self::$config = $GLOBALS['config'];
		self::$urlpath = PATH_URL;
		$query =  "SELECT v.*,c.catename".
			     " FROM ".DB_PREFIX."page AS v".
			     " LEFT JOIN ".DB_PREFIX."pagecate AS c ON v.cateid=c.cateid".
			     " WHERE v.cateid=".intval($cid)." AND v.flag=1 ORDER BY v.orders ASC";
		$array  = self::$obj->getall($query);
		$i = 1;
		foreach($array as $key=>$value){
			if(intval($value['linktype'])==2){
				$array[$key]['url'] = $value['linkurl'];
			}else{
				if(self::$config['htmltype']=='php'){
					$array[$key]['url'] = self::$urlpath."page.php?mod=detail&id=".$value['pageid'];
				}else{
					if(self::$config['routeurltype']==1){
						$array[$key]['url'] = self::$urlpath."page-".$value['pageid'].".html";
					}else{
						$array[$key]['url'] = self::$urlpath."page/".$value['pageid'].".html";
					}
				}
			}
			$array[$key]['i'] = $i;
			$i = ($i+1);
		}
		return $array;
	}
    
	/*
	  @$Id 列表
	  @params $where    -- 查询条件
	  @params $orderby  -- 排序
	  @params $limitnum -- 显示数量
	*/
	public static function volist($where="",$orderby="",$limitnum=0){
		self::$obj = $GLOBALS['db'];
		self::$tpl = $GLOBALS['tpl'];
		self::$urlpath = PATH_URL;
		self::$config = $GLOBALS['config'];
		$where = Core_Fun::forbidchar($where);
		$orderby = Core_Fun::forbidchar($orderby);
		$query  = "SELECT v.*,c.catename,c.img,c.cssname".
			     " FROM ".DB_PREFIX."page AS v".
			     " LEFT JOIN ".DB_PREFIX."pagecate AS c ON v.cateid=c.cateid".
		         " WHERE v.flag=1";
		if(Core_Fun::ischar($where)){
			$query .= " ".$where;
		}
		if(Core_Fun::ischar($orderby)){
			$query .= " ".$orderby;
		}else{
			$query .= " ORDER BY v.orders ASC";
		}
		if(intval($limitnum)>0){
			$query .= " LIMIT ".intval($limitnum)."";
		}
		$array = self::$obj->getall($query);
		$i = 1;
		foreach($array as $key=>$value){
			if(intval($value['linktype'])==2){
				$array[$key]['url'] = $value['linkurl'];
			}else{
				if(self::$config['htmltype']=='php'){
					$array[$key]['url'] = self::$urlpath."page.php?mod=detail&id=".$value['pageid'];
				}else{
					if(self::$config['routeurltype']==1){
						$array[$key]['url'] = self::$urlpath."page-".$value['pageid'].".html";
					}else{
						$array[$key]['url'] = self::$urlpath."page/".$value['pageid'].".html";
					}
				}
			}
			$array[$key]['i'] = $i;
			$i = ($i+1);
		}
		return $array;
	}

	/*
	  @$Id 分类
	  @params $where    -- 查询条件
	  @params $orderby  -- 排序
	  @params $limitnum -- 显示数量
	*/
	public static function category($where="",$orderby="",$limitnum=0){
		self::$obj = $GLOBALS['db'];
		self::$config = $GLOBALS['config'];
		$where = Core_Fun::forbidchar($where);
		$orderby = Core_Fun::forbidchar($orderby);
		$query = "SELECT c.* FROM ".DB_PREFIX."pagecate AS c".
			     " WHERE c.flag=1";
		if(Core_Fun::ischar($where)){
			$query .= " ".$where;
		}
		if(Core_Fun::ischar($orderby)){
			$query .= " ".$orderby;
		}else{
			$query .= " ORDER BY c.orders ASC";
		}
		if(intval($limitnum)>0){
			$query .= " LIMIT ".intval($limitnum)."";
		}
		$array = self::$obj->getall($query);
		$i = 1;
		foreach($array as $key=>$value){
			$array[$key]['i'] = $i;
			$i = ($i+1);
		}
		return $array;
	}

}
?>