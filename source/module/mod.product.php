<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.01.15
 * @Id         产品展示
**/
if(!defined('PHP_KINGFISHER')) {
	exit('Access Denied');
}
class Mod_Product{
	private static $obj = NULL;
	private static $tpl = NUll;
	private static $urlpath = NULL;
	private static $config = array();

	/*
	  @$Id 根据sql条件查询列表
	  @params $where    -- 查询条件
	  @params $orderby  -- 排序
	  @params $limitnum -- 显示数量
	*/
	public static function volist($where="",$orderby="",$limitnum=0){
		self::$obj = $GLOBALS['db'];
		self::$tpl = $GLOBALS['tpl'];
		self::$urlpath = PATH_URL;
		self::$config = $GLOBALS['config'];
		$where = Core_Fun::forbidchar($where);
		$orderby = Core_Fun::forbidchar($orderby);
		$query  = "SELECT v.*,c.catename,c.img,c.cssname,c.target,c.linktype,c.linkurl".
			     " FROM ".DB_PREFIX."product AS v".
			     " LEFT JOIN ".DB_PREFIX."productcate AS c ON v.cateid=c.cateid".
		         " WHERE v.flag=1";
		if(Core_Fun::ischar($where)){
			$query .= " ".$where;
		}
		if(Core_Fun::ischar($orderby)){
			$query .= " ".$orderby;
		}else{
			$query .= " ORDER BY v.productid DESC";
		}
		if(intval($limitnum)>0){
			$query .= " LIMIT ".intval($limitnum)."";
		}else{
			$query .= " LIMIT ".intval(self::$config['productnum'])."";
		}
		$array = self::$obj->getall($query);
		$i = 1;
		foreach($array as $key=>$value){
			if(self::$config['htmltype']=='php'){
				$array[$key]['url'] = self::$urlpath."product.php?mod=detail&id=".$value['productid'];
				$array[$key]['caturl'] = self::$urlpath."product.php?mod=list&cid=".$value['cateid']."";
			}else{
				if(self::$config['routeurltype']==1){
					$array[$key]['url'] = self::$urlpath."product-".$value['productid'].".html";
					$array[$key]['caturl'] = self::$urlpath."product-cat-".$value['cateid'].".html";
				}else{
					$array[$key]['url'] = self::$urlpath."product/".$value['productid'].".html";
					$array[$key]['caturl'] = self::$urlpath."product/cat-".$value['cateid'].".html";
				}
			}
			if(intval($value['linktype'])==2){
				$array[$key]['caturl'] = $value['linkurl'];
			}
			if(intval($value['target'])==2){
				$array[$key]['target'] = "_blank";
			}else{
				$array[$key]['target'] = "_self";
			}
			if(Core_Fun::ischar($value['thumbfiles'])){
				$array[$key]['thumbfiles'] = self::$urlpath.$value['thumbfiles'];
			}
			if(Core_Fun::ischar($value['uploadfiles'])){
				$array[$key]['uploadfiles'] = self::$urlpath.$value['uploadfiles'];
			}
			if(strtolower(LJCMS_CHARSET)=="utf-8"){
				$array[$key]['sort_productname'] = Core_Fun::cut_str($value['productname'],self::$config['productlen']);
			}else{
				$array[$key]['sort_productname'] = Core_Fun::cut_str($value['productname'],self::$config['productlen'],0,"gbk");
			}
			$array[$key]['i'] = $i;
			$i = ($i+1);
		}
		return $array;
	}

	/*
	  @$Id 根据category id 查询列表 查询结果含子类
	  @params $cid      -- 分类ID
	  @params $orderby  -- 排序
	  @params $limitnum -- 显示数量
	*/
	public static function catvolist($cid=0,$orderby="",$limitnum=0){
		self::$obj = $GLOBALS['db'];
		self::$tpl = $GLOBALS['tpl'];
		self::$urlpath = PATH_URL;
		self::$config = $GLOBALS['config'];
		$orderby = Core_Fun::forbidchar($orderby);
		$query  = "SELECT v.*,c.catename,c.img,c.cssname,c.target,c.linktype,c.linkurl".
			     " FROM ".DB_PREFIX."product AS v".
			     " LEFT JOIN ".DB_PREFIX."productcate AS c ON v.cateid=c.cateid".
		         " WHERE v.flag=1";
		$cid = intval($cid);
		if(intval($cid)>0){
			$childs_sql = Core_Mod::build_childsql("productcate","v",$cid,"");
			if(Core_Fun::ischar($childs_sql)){
				$query .= " AND (v.cateid=$cid".$childs_sql.")";
			}else{
				$query .= " AND v.cateid=$cid";
			}
		}
		if(Core_Fun::ischar($orderby)){
			$query .= " ".$orderby;
		}else{
			$query .= " ORDER BY v.productid DESC";
		}
		if(intval($limitnum)>0){
			$query .= " LIMIT ".intval($limitnum)."";
		}else{
			$query .= " LIMIT ".intval(self::$config['productnum'])."";
		}
		$array = self::$obj->getall($query);
		$i = 1;
		foreach($array as $key=>$value){
			if(self::$config['htmltype']=='php'){
				$array[$key]['url'] = self::$urlpath."product.php?mod=detail&id=".$value['productid'];
				$array[$key]['caturl'] = self::$urlpath."product.php?mod=list&cid=".$value['cateid']."";
			}else{
				if(self::$config['routeurltype']==1){
					$array[$key]['url'] = self::$urlpath."product-".$value['productid'].".html";
					$array[$key]['caturl'] = self::$urlpath."product-cat-".$value['cateid'].".html";
				}else{
					$array[$key]['url'] = self::$urlpath."product/".$value['productid'].".html";
					$array[$key]['caturl'] = self::$urlpath."product/cat-".$value['cateid'].".html";
				}
			}
			if(intval($value['linktype'])==2){
				$array[$key]['caturl'] = $value['linkurl'];
			}
			if(intval($value['target'])==2){
				$array[$key]['target'] = "_blank";
			}else{
				$array[$key]['target'] = "_self";
			}
			if(Core_Fun::ischar($value['thumbfiles'])){
				$array[$key]['thumbfiles'] = self::$urlpath.$value['thumbfiles'];
			}
			if(Core_Fun::ischar($value['uploadfiles'])){
				$array[$key]['uploadfiles'] = self::$urlpath.$value['uploadfiles'];
			}
			if(strtolower(LJCMS_CHARSET)=="utf-8"){
				$array[$key]['sort_productname'] = Core_Fun::cut_str($value['productname'],self::$config['productlen']);
			}else{
				$array[$key]['sort_productname'] = Core_Fun::cut_str($value['productname'],self::$config['productlen'],0,"gbk");
			}
			$array[$key]['i'] = $i;
			$i = ($i+1);
		}
		return $array;
	}


	/*
	  @$Id 一级分类
	  @params $where    -- 查询条件
	  @params $orderby  -- 排序
	  @params $limitnum -- 显示数量
	*/
	public static function category($where="",$orderby="",$limitnum=0){
		self::$obj = $GLOBALS['db'];
		self::$config = $GLOBALS['config'];
		self::$urlpath = PATH_URL;
		$where = Core_Fun::forbidchar($where);
		$orderby = Core_Fun::forbidchar($orderby);
		$query = "SELECT c.* FROM ".DB_PREFIX."productcate AS c".
			     " WHERE c.flag=1 AND c.parentid=0";
		if(Core_Fun::ischar($where)){
			$query .= " ".$where;
		}
		if(Core_Fun::ischar($orderby)){
			$query .= " ".$orderby;
		}else{
			$query .= " ORDER BY c.orders ASC";
		}
		if(intval($limitnum)>0){
			$query .= " LIMIT ".intval($limitnum)."";
		}
		$array = self::$obj->getall($query);
		$i = 1;
		foreach($array as $key=>$value){
			if(intval($value['linktype'])==2){
				$array[$key]['url'] = $value['linkurl'];
			}else{
				if(self::$config['htmltype']=='php'){
					$array[$key]['url'] = self::$urlpath."product.php?mod=list&cid=".$value['cateid'];
				}else{
					if(self::$config['routeurltype']==1){
						$array[$key]['url'] = self::$urlpath."product-cat-".$value['cateid'].".html";
					}else{
						$array[$key]['url'] = self::$urlpath."product/cat-".$value['cateid'].".html";
					}
				}
		    }
			if(intval($value['target'])==2){
				$array[$key]['target'] = "_blank";
			}else{
				$array[$key]['target'] = "_self";
			}
			$array[$key]['i'] = $i;
			$i = ($i+1);
		}
		return $array;
	}

	/*
	  @$Id 一级与二级树形分类
	  @params $parentnum -- 一级分类个数
	  @params $childnum  -- 二级分类个数
	*/
	public static function treecategory($parentnum=0,$childnum=0){
		self::$obj = $GLOBALS['db'];
		self::$config = $GLOBALS['config'];
		self::$urlpath = PATH_URL;
		$array  = array();
		$parent_sql = "SELECT cateid,catename,img,cssname,target,linktype,linkurl FROM ".DB_PREFIX."productcate".
			         " WHERE parentid=0 AND depth=0 AND flag=1".
			         " ORDER BY orders ASC";
		if(intval($parentnum)>0){
			$parent_sql .= " LIMIT ".intval($parentnum)."";
		}
		$i = 1;
		$parent_array = self::$obj->getall($parent_sql);
		foreach($parent_array as $parent_key=>$parent_value){
			if(intval($parent_value['linktype'])==2){
				$parent_url = $parent_value['linkurl'];
			}else{
				if(self::$config['htmltype']=='php'){
					$parent_url = self::$urlpath."product.php?mod=list&cid=".$parent_value['cateid'];
				}else{
					if(self::$config['routeurltype']==1){
						$parent_url = self::$urlpath."product-cat-".$parent_value['cateid'].".html";
					}else{
						$parent_url = self::$urlpath."product/cat-".$parent_value['cateid'].".html";
					}
				}
			}
			if(intval($parent_value['target'])==2){
				$target = "_blank";
			}else{
				$target = "_self";
			}
			$array[] = array(
				'i'=>$i,
				'cateid'=>$parent_value['cateid'],
				'catename'=>$parent_value['catename'],
				'img'=>$parent_value['img'],
				'cssname'=>$parent_value['cssname'],
				'url'=>$parent_url,
				'target'=>$target,
				'childcategory'=>array()
			);
			$i = ($i+1);

			$child_sql = "SELECT cateid,catename,img,cssname,target,linktype,linkurl FROM ".DB_PREFIX."productcate".
				        " WHERE parentid=".intval($parent_value['cateid'])." AND flag=1".
				        " ORDER BY orders ASC";
			if(intval($childnum)>0){
				$child_sql .= " LIMIT ".intval($childnum)."";
			}
			$child_array = self::$obj->getall($child_sql);
			$ii = 1;
			foreach($child_array as $child_key=>$child_value){
				if(intval($child_value['linktype'])==2){
					$child_url = $child_value['linkurl'];
				}else{
					if(self::$config['htmltype']=='php'){
						$child_url = self::$urlpath."product.php?mod=list&cid=".$child_value['cateid'];
					}else{
						if(self::$config['routeurltype']==1){
							$child_url = self::$urlpath."product-cat-".$child_value['cateid'].".html";
						}else{
							$child_url = self::$urlpath."product/cat-".$child_value['cateid'].".html";
						}
					}
				}
				if(intval($child_value['target'])==2){
					$target = "_blank";
				}else{
					$target = "_self";
				}
				$array[count($array)-1]['childcategory'][] = array(
					'i'=>$ii,
					'cateid'=>$child_value['cateid'],
					'catename'=>$child_value['catename'],
					'img'=>$child_value['img'],
					'cssname'=>$child_value['cssname'],
					'url'=>$child_url,
					'target'=>$target,
				);
				$ii = ($ii+1);
			
			}
		}
		return $array;
	}

	/* 上一个 */
	public static function previousitem($id){
		self::$obj = $GLOBALS['db'];
		self::$config = $GLOBALS['config'];
		self::$urlpath = PATH_URL;
		$temp  = "";
		if(Core_Fun::isnumber($id)){
			$id = intval($id);
			$query = "SELECT productid,productname FROM ".DB_PREFIX."product WHERE productid<$id".
					 " ORDER BY productid DESC LIMIT 1";
			$rows  = self::$obj->fetch_first($query);
			if($rows){
				if(self::$config['htmltype']=='php'){
					$temp = "<a href=\"".self::$urlpath."product.php?mod=detail&id=".$rows['productid']."\">".$rows['productname']."</a>";
				}else{
					if(self::$config['routeurltype']==1){
						$temp = "<a href=\"".self::$urlpath."product-".$rows['productid'].".html\">".$rows['productname']."</a>";
					}else{
						$temp = "<a href=\"".self::$urlpath."product/".$rows['productid'].".html\">".$rows['productname']."</a>";
					}
				}
			}else{
				$temp = "没有了";
			}
		}
		return $temp;
	}

	/* 下一个 */
	function nextitem($id){
		self::$obj = $GLOBALS['db'];
		self::$config = $GLOBALS['config'];
		self::$urlpath = PATH_URL;
		$temp  = "";
		if(Core_Fun::isnumber($id)){
			$id = intval($id);
			$query = "SELECT productid,productname FROM ".DB_PREFIX."product WHERE productid>$id".
					 " ORDER BY productid ASC LIMIT 1";
			$rows  = self::$obj->fetch_first($query);
			if($rows){
				if(self::$config['htmltype']=='php'){
					$temp = "<a href=\"".self::$urlpath."product.php?mod=detail&id=".$rows['productid']."\">".$rows['productname']."</a>";
				}else{
					if(self::$config['routeurltype']==1){
						$temp = "<a href=\"".self::$urlpath."product-".$rows['productid'].".html\">".$rows['productname']."</a>";
					}else{
						$temp = "<a href=\"".self::$urlpath."product/".$rows['productid'].".html\">".$rows['productname']."</a>";
					}
				}
			}else{
				$temp = "没有了";
			}
		}
		return $temp;
	}

}
?>