<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.09.15
 * @Id         在线留言
**/

if(!defined('ALLOWGUEST')) {
	exit('Access Denied');
}
$uaction = Core_Fun::rec_post("uaction");

$page		= Core_Fun::detect_number(Core_Fun::rec_post("page"),1);
if($page<1){$page=1;}
$comeurl	= "page=$page";


switch($uaction){
    case 'jobave';
	    jobave();
		break;
	case 'editjob';
	    editjob();
		break;
	case 'deljob';
	    deljob();
		break;
	default;
        uvolist();
        break;
}


function jobave(){
    global $db;
	$id			= Core_Fun::rec_post('id',1);
	$jobid = Core_Fun::rec_post("jobid",1);
	$username      = Core_Fun::rec_post("username",1);
	$brothday      = Core_Fun::rec_post("brothday",1);
	$chinatext     = Core_Fun::rec_post("chinatext",1);
	$telno       = Core_Fun::rec_post("telno",1);
	$email        = Core_Fun::rec_post("email",1);
	$degree   = Core_Fun::rec_post("degree",1);
	$prosesion      = Core_Fun::rec_post("prosesion",1);
	$school         = Core_Fun::rec_post("school",1);
	$address       = Core_Fun::rec_post("address",1);
	$awards     = Core_Fun::rec_post("awards",1);
	$experience     = Core_Fun::rec_post("experience",1);
	$hobby     = Core_Fun::rec_post("hobby",1);

	$founderr	= false;

	if(!Core_Fun::isnumber($id)){
	    $founderr	= true;
		$errmsg	   .= "ID丢失.<br />";
	}
	if(!Core_Fun::ischar($username)){
		$founderr = true;
		$errmsg  .= "姓名不能为空<br />";
	}
	if(!Core_Fun::ischar($chinatext)){
		$founderr = true;
		$errmsg  .= "国籍不能为空<br />";
	}
	if(!Core_Fun::ischar($telno)){
		$founderr = true;
		$errmsg  .= "联系电话不能为空<br />";
	}
	if(!Core_Fun::ischar($email)){
		$founderr = true;
		$errmsg  .= "E-mail不能为空<br />";
	}
	

	if($founderr == true){
	    Core_Fun::halt($errmsg,"",1);
	}

	$array = array(
		'username'=>$username,
		'brothday'=>$brothday,
		'chinatext'=>$chinatext,
		'telno'=>$telno,
		'email'=>$email,
		'degree'=>$degree,
		'prosesion'=>$prosesion,
		'school'=>$school,
		'address'=>$address,
		'awards'=>$awards,
		'experience'=>$experience,
		'hobby'=>$hobby,
		'ip'=>Core_Fun::getip(),
	);

	$result = $db->update(DB_PREFIX."applyjob",$array,"aid=$id");

	if($result){
		Core_Command::runlog("","简历修改成功[id=$id]");
		Core_Fun::halt("简历修改成功","resume.php?".$GLOBALS['comeurl']."",0);
	}else{
		Core_Fun::halt("简历修改失败","",2);
	}

}






function deljob(){
	//Core_Auth::checkauth("admindel");
	$arrid  = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
	if($arrid=="" || is_null($arrid)){
		Core_Fun::halt("请选择要删除的数据","",1);
	}
	for($ii=0;$ii<count($arrid);$ii++){
        $id = Core_Fun::replacebadchar(trim($arrid[$ii]));
		if(Core_Fun::isnumber($id)){
			$GLOBALS['db']->query("DELETE FROM ".DB_PREFIX."applyjob WHERE aid=$id");
		}
	}
	Core_Command::runlog("","删除简历成功[id=$arrid]");
	Core_Fun::halt("删除成功","resume.php",0);
}






function editjob(){
	//Core_Auth::checkauth("adminedit");
	global $db,$tpl;
    $id = Core_Fun::rec_post('id');
	if(!Core_Fun::isnumber($id)){
		Core_Fun::halt("ID丢失","",2);
	}
	$sql		= "SELECT a.*,j.title AS jobtitle".
	             " FROM ".DB_PREFIX."applyjob AS a".
		         " LEFT JOIN ".DB_PREFIX."job AS j ON a.jobid=j.jobid where a.aid=$id ORDER BY a.aid ASC ";
	$jobs = $db->fetch_first($sql);
	if(!$jobs){
		Core_Fun::halt("数据不存在","",2);
	}else{

	$tpl->assign("id",$id);
	$tpl->assign("comeurl",$GLOBALS['comeurl']);
	$tpl->assign("jobs",$jobs);
	}
}






function uvolist(){
	//Core_Auth::checkauth("uservolist");
if (trim($_SESSION["USERNAME"])!="")
{
	global $db,$tpl,$page;
	$userid=trim($_SESSION["USERID"]);
    $pagesize	= 30;
	$searchsql	= " WHERE 1=1 and a.userid=$userid ";
	$countsql	= "SELECT COUNT(a.aid) FROM ".DB_PREFIX."applyjob AS a".$searchsql;
    $total		= $db->fetch_count($countsql);
    $pagecount	= ceil($total/$pagesize);
	$nextpage	= $page+1;
	$prepage	= $page-1;
	$start		= ($page-1)*$pagesize;
	$sql		= "SELECT a.*,j.title AS jobtitle".
	             " FROM ".DB_PREFIX."applyjob AS a".
		         " LEFT JOIN ".DB_PREFIX."job AS j ON a.jobid=j.jobid".
		         $searchsql."ORDER BY a.aid ASC LIMIT $start, $pagesize";
	$jobs		= $db->getall($sql);
	$url		= $_SERVER['PHP_SELF'];
	$showpage	= Core_Page::adminpage($total,$pagesize,$page,$url,10);
	$tpl->assign("total",$total);
	$tpl->assign("pagecount",$pagecount);
	$tpl->assign("page",$page);
	$tpl->assign("showpage",$showpage);
	$tpl->assign("jobs",$jobs);
	
	}
}





$tpl->assign("uaction",$uaction);
$tpl->assign("page_title","我的简历");
$tpl->assign("page_metadescription",$config['metadescription']);
$tpl->assign("page_metakeyword",$config['metakeyword']);






?>