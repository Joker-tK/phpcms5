<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.09.15
 * @Id         人才招聘内容
**/
if(!defined('ALLOWGUEST')) {
	exit('Access Denied');
}
$id = Core_Fun::detect_number(Core_Fun::rec_post("id"));
if($id<1){
	Core_Fun::halt("ID丢失或者已删除！","",1);
}
$sql	= "SELECT v.*,c.catename,c.img,c.cssname,c.target,c.linktype,c.linkurl FROM ".DB_PREFIX."job AS v".
         " LEFT JOIN ".DB_PREFIX."jobcate AS c ON v.cateid=c.cateid".
	     " WHERE v.flag=1 AND v.jobid='".intval($id)."'";
$job= $db->fetch_first($sql);
if(!$job){
	Core_Fun::halt("对不起，信息不存在或已删除！","",1);
}else{
	if(intval($job['target'])==2){
		$target = "_blank";
	}else{
		$target = "_self";
	}
	/* url和导航 */
	if($config['htmltype']=="php"){
		$job['url'] = PATH_URL."job.php?mod=detail&id=".$job['jobid'];
		if(intval($job['linktype'])==2){
			$job['caturl'] = $job['linkurl'];
		}else{
			$job['caturl'] = PATH_URL."job.php?mod=list&cid=".$job['cateid'];
		}
		$navigation  = "<a href=\"".PATH_URL."job.php\">".$LANVAR['job']."</a> >> ";
		if(intval($job['linktype'])==2){
			$navigation .= "<a href=\"".$job['linkurl']."\">".$job['catename']."</a> >> ";
		}else{
			$navigation .= "<a href=\"".PATH_URL."job.php?mod=list&cid=".$job['cateid']."\">".$job['catename']."</a> >> ";
		}
		$navigation .= "<a href=\"".PATH_URL."job.php?mod=detail&id=$id\">".$job['title']."</a>";
	}else{
		if($config['routeurltype']==1){
			$job['url'] = PATH_URL."job-".$job['jobid'].".html";
			if(intval($job['linktype'])==2){
				$job['caturl'] = $job['linkurl'];
			}else{
				$job['caturl'] = PATH_URL."job-cat-".$job['cateid'].".html";
			}

			$navigation  = "<a href=\"".PATH_URL."job.html\">".$LANVAR['job']."</a> >> ";
			if(intval($job['linktype'])==2){
				$navigation .= "<a href=\"".$job['linkurl']."\">".$job['catename']."</a> >> ";
			}else{
				$navigation .= "<a href=\"".PATH_URL."job-cat-".$job['cateid'].".html\">".$job['catename']."</a> >> ";
			}
			$navigation .= "<a href=\"".PATH_URL."job-".$id.".html\">".$job['title']."</a>";
		}else{
			$job['url'] = PATH_URL."job/".$job['jobid'].".html";
			if(intval($job['linktype'])==2){
				$job['caturl'] = $job['linkurl'];
			}else{
				$job['caturl'] = PATH_URL."job/cat-".$job['cateid'].".html";
			}
			$navigation  = "<a href=\"".PATH_URL."job\">".$LANVAR['job']."</a> >> ";
			if(intval($job['linktype'])==2){
				$navigation .= "<a href=\"".$job['linkurl']."\">".$job['catename']."</a> >> ";
			}else{
				$navigation .= "<a href=\"".PATH_URL."job/cat-".$job['cateid'].".html\">".$job['catename']."</a> >> ";
			}
			$navigation .= "<a href=\"".PATH_URL."job/".$id.".html\">".$job['title']."</a>";
		}
	}

	if(Core_Fun::ischar($job['metatitle'])){
		$page_title = $job['metatitle'];
	}else{
		$page_title = $job['title'];
	}
	$page_keyword = $job['metakeyword'];
	$page_description = $job['metadescription'];
	//$db->update(DB_PREFIX."job",array('hits'=>'[[hits+1]]',),"jobid=".$id."");
}
/* 同分类相关文章 */
$volist_relatedjob = Mod_Job::volist("AND v.cateid=".$job['cateid']." AND v.jobid<>$id","","");

$tpl->assign("id",$id);
$tpl->assign("job",$job);
$tpl->assign("navigation",$navigation);
$tpl->assign("previous_item",Mod_Job::previousitem($id));
$tpl->assign("next_item",Mod_Job::nextitem($id));
$tpl->assign("volist_relatedjob",$volist_relatedjob);
$tpl->assign("page_title",$page_title."-".$LANVAR['job']."-".$config['sitename']);
$tpl->assign("page_description",$page_description);
$tpl->assign("page_keyword",$page_keyword);
?>