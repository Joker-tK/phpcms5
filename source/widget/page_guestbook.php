<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.09.15
 * @Id         在线留言
**/

if(!defined('ALLOWGUEST')) {
	exit('Access Denied');
}
$action = Core_Fun::rec_post("action");
if($action=="saveadd"){

	$gender      = Core_Fun::rec_post("gender",1);
	$companyname = Core_Fun::rec_post("companyname",1);
	$address     = Core_Fun::rec_post("address",1);
	$trade       = Core_Fun::rec_post("trade",1);
	$jobs        = Core_Fun::rec_post("jobs",1);
	$telephone   = Core_Fun::rec_post("telephone",1);
	$mobile      = Core_Fun::rec_post("mobile",1);
	$fax         = Core_Fun::rec_post("fax",1);
	$email       = Core_Fun::rec_post("email",1);
	$content     = Core_Fun::rec_post("content",1);

    if (trim($_SESSION["USERNAME"])!="")
	{
	$userid=trim($_SESSION["USERID"]);
	$bookuser    = trim($_SESSION["USERNAME"]);
    }
	else 
		{
    $userid=0;
	$bookuser    = Core_Fun::rec_post("bookuser",1);
	}

	$founderr    = false;
	if(!Core_Fun::ischar($bookuser)){
		$founderr = true;
		$errmsg  .= "姓名不能为空<br />";
	}
	/*if(!Core_Fun::ischar($companyname)){
		$founderr = true;
		$errmsg  .= "公司名不能为空<br />";
	}
	if(!Core_Fun::ischar($address)){
		$founderr = true;
		$errmsg  .= "地址不能为空<br />";
	}
	if(!Core_Fun::ischar($trade)){
		$founderr = true;
		$errmsg  .= "行业不能为空<br />";
	}
	if(!Core_Fun::ischar($jobs)){
		$founderr = true;
		$errmsg  .= "职务不能为空<br />";
	}*/
	if(!Core_Fun::ischar($telephone)){
		$founderr = true;
		$errmsg  .= "电话不能为空<br />";
	}
	if(!Core_Fun::ischar($content)){
		$founderr = true;
		$errmsg  .= "留言内容不能为空<br />";
	}
	if($founderr==true){
		Core_Fun::halt($errmsg,"",1);
	}

	$bookid = $db->fetch_newid("SELECT MAX(bookid) FROM ".DB_PREFIX."guestbook",1);
	$array  = array(
		'bookid'=>$bookid,
		'title'=>$title,
		'bookuser'=>$bookuser,
		'gender'=>$gender,
		'jobs'=>$jobs,
		'telephone'=>$telephone,
		'fax'=>$fax,
		'mobile'=>$mobile,
		'email'=>$email,
		'qqmsn'=>$qqmsn,
		'companyname'=>$companyname,
		'address'=>$address,
		'trade'=>$trade,
		'homepage'=>$homepage,
		'content'=>$content,
		'booktimeline'=>time(),
		'ip'=>Core_Fun::getip(),
		'userid'=>$userid,
		'flag'=>1,
	);

	$db->insert(DB_PREFIX."guestbook",$array);
	if($urlsuffix=='php'){
		$url = PATH_URL."guestbook.php";
	}else{
		if($config['routeurltype']==1){
			$url = PATH_URL."guestbook.html";
		}else{
			$url = PATH_URL."guestbook";
		}
	}
	Core_Fun::halt("留言成功，我们将会尽快给您联系，感谢您的支持！","$url",0);

}else{
	$page_title = $LANVAR['guestbook'];


	$tpl->assign("username",$_SESSION["USERNAME"]);
	$tpl->assign("page_title",$page_title."-".$config['sitename']);
	$tpl->assign("page_metadescription",$config['metadescription']);
	$tpl->assign("page_metakeyword",$config['metakeyword']);
}
?>