<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.09.15
 * @Id         在线留言
**/

if(!defined('ALLOWGUEST')) {
	exit('Access Denied');
}
$action = Core_Fun::rec_post("uaction");
if($action=="usersave"){
	$id			= Core_Fun::rec_post('userid',1);
    $password	= Core_Fun::rec_post('password',1);
	$relname	= Core_Fun::rec_post('relname',1);
	$sex		= Core_Fun::rec_post('Sex',1);
	$email		= Core_Fun::rec_post('email',1);
	$address	= Core_Fun::rec_post('address',1);
	$telno		= Core_Fun::rec_post('tel',1);
	$brothday	= Core_Fun::rec_post('brothday',1);
	$memo		= Core_Fun::strip_post('memo',1);
	$founderr	= false;
	if(!Core_Fun::isnumber($id)){
	    $founderr	= true;
		$errmsg	   .= "ID丢失.<br />";
	}
	if($founderr == true){
	    Core_Fun::halt($errmsg,"",1);
	}
	$array = array(
			'realname'=>$relname,
			'sex'=>$sex,
			'email'=>$email,
			'address'=>$address,
			'telno'=>$telno,
			'brothday'=>$brothday,
		    'memo'=>$memo,
	);
	if($password!=""){
		$array = $array + array('password'=>md5($password));
	}
	$result = $db->update(DB_PREFIX."user",$array,"userid=$id");

	if($result){
		Core_Command::runlog("","修改资料成功[id=$id]");
		Core_Fun::halt("修改资料成功","index.php?".$GLOBALS['comeurl']."",0);
	}else{
		Core_Fun::halt("修改失败","",2);
	}

}else{


if (trim($_SESSION["USERNAME"])!="")
{
$userid=trim($_SESSION["USERID"]);
$username    = trim($_SESSION["USERNAME"]);
 useredit($userid);

}

	$tpl->assign("page_title","修改我的资料");
	$tpl->assign("page_metadescription",$config['metadescription']);
	$tpl->assign("page_metakeyword",$config['metakeyword']);
}


function useredit($ids){
	//Core_Auth::checkauth("adminedit");
	global $db,$tpl;
    $id =$ids;
	if(!Core_Fun::isnumber($id)){
		Core_Fun::halt("ID丢失","",2);
	}
	$sql   = "SELECT * FROM ".DB_PREFIX."user WHERE userid=$id";
	$user = $db->fetch_first($sql);
	if(!$user){
		Core_Fun::halt("数据不存在","",2);
	}else{

	$tpl->assign("id",$id);
	$tpl->assign("comeurl",$GLOBALS['comeurl']);
	$tpl->assign("user",$user);
	}
}



?>