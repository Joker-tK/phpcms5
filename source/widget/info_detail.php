<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.01.15
 * @Id         新闻资讯内容
**/
if(!defined('ALLOWGUEST')) {
	exit('Access Denied');
}
$id = Core_Fun::detect_number(Core_Fun::rec_post("id"));
if($id<1){
	Core_Fun::halt("ID丢失或者已删除！","",1);
}
$sql	= "SELECT v.*,c.catename,c.img,c.cssname,c.target,c.linktype,c.linkurl FROM ".DB_PREFIX."info AS v".
         " LEFT JOIN ".DB_PREFIX."infocate AS c ON v.cateid=c.cateid".
	     " WHERE v.flag=1 AND v.infoid='".intval($id)."'";
$info= $db->fetch_first($sql);
if(!$info){
	Core_Fun::halt("对不起，信息不存在或已删除！","",1);
}else{
	if(intval($info['target'])==2){
		$target = "_blank";
	}else{
		$target = "_self";
	}
	/* url和导航 */
	if($config['htmltype']=="php"){
		$info['url'] = PATH_URL."info.php?mod=detail&id=".$info['infoid'];
		if(intval($info['linktype'])==2){
			$info['caturl'] = $info['linkurl'];
		}else{
			$info['caturl'] = PATH_URL."info.php?mod=list&cid=".$info['cateid'];
		}
		$navigation  = "<a href=\"".PATH_URL."info.php\">".$LANVAR['info']."</a> >> ";
		if(intval($info['linktype'])==2){
			$navigation .= "<a href=\"".$info['linkurl']."\">".$info['catename']."</a> >> ";
		}else{
			$navigation .= "<a href=\"".PATH_URL."info.php?mod=list&cid=".$info['cateid']."\">".$info['catename']."</a> >> ";
		}
		$navigation .= "<a href=\"".PATH_URL."info.php?mod=detail&id=$id\">".$info['title']."</a>";
	}else{
		if($config['routeurltype']==1){
			$info['url'] = PATH_URL."info-".$info['infoid'].".html";
			if(intval($info['linktype'])==2){
				$info['caturl'] = $info['linkurl'];
			}else{
				$info['caturl'] = PATH_URL."info-cat-".$info['cateid'].".html";
			}

			$navigation  = "<a href=\"".PATH_URL."info.html\">".$LANVAR['info']."</a> >> ";
			if(intval($info['linktype'])==2){
				$navigation .= "<a href=\"".$info['linkurl']."\">".$info['catename']."</a> >> ";
			}else{
				$navigation .= "<a href=\"".PATH_URL."info-cat-".$info['cateid'].".html\">".$info['catename']."</a> >> ";
			}
			$navigation .= "<a href=\"".PATH_URL."info-".$id.".html\">".$info['title']."</a>";
		}else{
			$info['url'] = PATH_URL."info/".$info['infoid'].".html";
			if(intval($info['linktype'])==2){
				$info['caturl'] = $info['linkurl'];
			}else{
				$info['caturl'] = PATH_URL."info/cat-".$info['cateid'].".html";
			}
			$navigation  = "<a href=\"".PATH_URL."info\">".$LANVAR['info']."</a> >> ";
			if(intval($info['linktype'])==2){
				$navigation .= "<a href=\"".$info['linkurl']."\">".$info['catename']."</a> >> ";
			}else{
				$navigation .= "<a href=\"".PATH_URL."info/cat-".$info['cateid'].".html\">".$info['catename']."</a> >> ";
			}
			$navigation .= "<a href=\"".PATH_URL."info/".$id.".html\">".$info['title']."</a>";
		}
	}

 	if(!Core_Fun::ischar($info['thumbfiles'])){
		$into['thumbfiles'] = PATH_URL."static/images/nopic.jpg";
	}else{
		$into['thumbfiles'] = PATH_URL.$info['thumbfiles'];
	}
	if(Core_Fun::ischar($info['uploadfiles'])){
		$info['uploadfiles'] = PATH_URL.$info['uploadfiles'];
	}

	if(Core_Fun::ischar($info['metatitle'])){
		$page_title = $info['metatitle'];
	}else{
		$page_title = $info['title'];
	}
	$page_keyword = $info['metakeyword'];
	$page_description = $info['metadescription'];
	$info['content'] = Core_Command::command_replacetag(1,"info",$info['tag'],$info['content']);
	/*echo '<pre>';
	print_r($info['content']);*/
	//$db->update(DB_PREFIX."info",array('hits'=>'[[hits+1]]',),"infoid=".$id."");
}

/* 同分类相关文章 */
$volist_relatedinfo = Mod_Info::volist("AND v.cateid=".$info['cateid']." AND v.infoid<>$id","","");


$tpl->assign("id",$id);
$tpl->assign("info",$info);
header("Content-type: text/html; charset=utf-8");
echo '<pre>';
print_r($info);
$tpl->assign("navigation",$navigation);
$tpl->assign("previous_item",Mod_Info::previousitem($id));
$tpl->assign("next_item",Mod_Info::nextitem($id));
$tpl->assign("volist_relatedinfo",$volist_relatedinfo);
if((Core_Mod::viewisshow($info['ugroupid'],$info['exclusive']))==1){
$tpl->assign("viewfalgcontent","yes");
}else
{
$tpl->assign("viewfalgcontent","no");
}
$tpl->assign("page_title",$page_title."-".$LANVAR['info']."-".$config['sitename']);
$tpl->assign("page_description",$page_description);
$tpl->assign("page_keyword",$page_keyword);




//echo(viewisshow($info['ugroupid'],$info['exclusive'])."sdfdsf<br>");










?>