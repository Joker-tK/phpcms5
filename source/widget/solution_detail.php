<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.01.15
 * @Id         解决方案
**/
if(!defined('ALLOWGUEST')) {
	exit('Access Denied');
}
$id = Core_Fun::detect_number(Core_Fun::rec_post("id"));
if($id<1){
	Core_Fun::halt("ID丢失或者已删除！","",1);
}
$sql	= "SELECT v.*,c.catename,c.img,c.cssname,c.target,c.linktype,c.linkurl FROM ".DB_PREFIX."solution AS v".
         " LEFT JOIN ".DB_PREFIX."solutioncate AS c ON v.cateid=c.cateid".
	     " WHERE v.flag=1 AND v.solutionid='".intval($id)."'";
$solution= $db->fetch_first($sql);
if(!$solution){
	Core_Fun::halt("对不起，信息不存在或已删除！","",1);
}else{
    
    
	/* url和导航 */
	if($config['htmltype']=="php"){
		$solution['url'] = PATH_URL."solution.php?mod=detail&id=".$solution['solutionid'];
		if(intval($solution['linktype'])==2){
			$solution['caturl'] = $solution['linkurl'];
		}else{
			$solution['caturl'] = PATH_URL."solution.php?mod=list&cid=".$solution['cateid'];
		}
		$navigation  = "<a href=\"".PATH_URL."solution.php\">".$LANVAR['solution']."</a> >> ";
		if(intval($solution['linktype'])==2){
			$navigation .= "<a href=\"".$solution['linkurl']."\">".$solution['catename']."</a> >> ";
		}else{
			$navigation .= "<a href=\"".PATH_URL."solution.php?mod=list&cid=".$solution['cateid']."\">".$solution['catename']."</a> >> ";
		}
		$navigation .= "<a href=\"".PATH_URL."solution.php?mod=detail&id=$id\">".$solution['title']."</a>";
	}else{
		if($config['routeurltype']==1){
			$solution['url'] = PATH_URL."solution-".$solution['solutionid'].".html";
			if(intval($solution['linktype'])==2){
				$solution['caturl'] = $solution['linkurl'];
			}else{
				$solution['caturl'] = PATH_URL."solution-cat-".$solution['cateid'].".html";
			}

			$navigation  = "<a href=\"".PATH_URL."solution.html\">".$LANVAR['solution']."</a> >> ";
			if(intval($solution['linktype'])==2){
				$navigation .= "<a href=\"".$solution['linkurl']."\">".$solution['catename']."</a> >> ";
			}else{
				$navigation .= "<a href=\"".PATH_URL."solution-cat-".$solution['cateid'].".html\">".$solution['catename']."</a> >> ";
			}
			$navigation .= "<a href=\"".PATH_URL."solution-".$id.".html\">".$solution['title']."</a>";
		}else{
			$solution['url'] = PATH_URL."solution/".$solution['solutionid'].".html";
			if(intval($solution['linktype'])==2){
				$solution['caturl'] = $solution['linkurl'];
			}else{
				$solution['caturl'] = PATH_URL."solution/cat-".$solution['cateid'].".html";
			}
			$navigation  = "<a href=\"".PATH_URL."solution\">".$LANVAR['solution']."</a> >> ";
			if(intval($solution['linktype'])==2){
				$navigation .= "<a href=\"".$solution['linkurl']."\">".$solution['catename']."</a> >> ";
			}else{
				$navigation .= "<a href=\"".PATH_URL."solution/cat-".$solution['cateid'].".html\">".$solution['catename']."</a> >> ";
			}
			$navigation .= "<a href=\"".PATH_URL."solution/".$id.".html\">".$solution['title']."</a>";
		}
	}
 	if(!Core_Fun::ischar($solution['thumbfiles'])){
		$solution['thumbfiles'] = PATH_URL."static/images/nopic.jpg";
	}else{
		$solution['thumbfiles'] = PATH_URL.$solution['thumbfiles'];
	}
	if(Core_Fun::ischar($solution['uploadfiles'])){
		$solution['uploadfiles'] = PATH_URL.$solution['uploadfiles'];
	}

	if(Core_Fun::ischar($solution['metatitle'])){
		$page_title = $solution['metatitle'];
	}else{
		$page_title = $solution['title'];
	}
	$page_keyword = $solution['metakeyword'];
	$page_description = $solution['metadescription'];
	$solution['content'] = Core_Command::command_replacetag(1,"solution",$solution['tag'],$solution['content']);
	//$db->update(DB_PREFIX."solution",array('hits'=>'[[hits+1]]',),"solutionid=".$id."");
}

/* related info */
$volist_relatedsolution = Mod_Solution::volist("AND v.cateid=".$solution['cateid']." AND v.solutionid<>$id","","");

$tpl->assign("id",$id);
$tpl->assign("solution",$solution);
$tpl->assign("navigation",$navigation);
$tpl->assign("previous_item",Mod_Solution::previousitem($id));
$tpl->assign("next_item",Mod_Solution::nextitem($id));
$tpl->assign("volist_relatedsolution",$volist_relatedsolution);
if((Core_Mod::viewisshow($info['ugroupid'],$info['exclusive']))==1){
$tpl->assign("viewfalgcontent","yes");
}else
{
$tpl->assign("viewfalgcontent","no");
}
$tpl->assign("page_title",$page_title."-".$LANVAR['solution']."-".$config['sitename']);
$tpl->assign("page_description",$page_description);
$tpl->assign("page_keyword",$page_keyword);
?>