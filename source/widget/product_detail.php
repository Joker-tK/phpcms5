<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.01.15
 * @Id         产品内容
**/
if(!defined('ALLOWGUEST')) {
	exit('Access Denied');
}
$id = Core_Fun::detect_number(Core_Fun::rec_post("id"));
if($id<1){
	Core_Fun::halt("ID丢失或者已删除！","",1);
}
$sql	= "SELECT v.*,c.catename,c.img,c.cssname,c.target,c.linktype,c.linkurl FROM ".DB_PREFIX."product AS v".
         " LEFT JOIN ".DB_PREFIX."productcate AS c ON v.cateid=c.cateid".
	     " WHERE v.flag=1 AND v.productid='".intval($id)."'";
$product= $db->fetch_first($sql);
if(!$product){
	Core_Fun::halt("对不起，信息不存在或已删除！","",1);
}else{
    
	/* url和导航 */
	if($config['htmltype']=="php"){
		$product['url'] = PATH_URL."product.php?mod=detail&id=".$product['productid'];
		if(intval($product['linktype'])==2){
			$product['caturl'] = $product['linkurl'];
		}else{
			$product['caturl'] = PATH_URL."product.php?mod=list&cid=".$product['cateid'];
		}
		$navigation  = "<a href=\"".PATH_URL."product.php\">".$LANVAR['product']."</a> >> ";
		if(intval($product['linktype'])==2){
			$navigation .= "<a href=\"".$product['linkurl']."\">".$product['catename']."</a> >> ";
		}else{
			$navigation .= "<a href=\"".PATH_URL."product.php?mod=list&cid=".$product['cateid']."\">".$product['catename']."</a> >> ";
		}
		$navigation .= "<a href=\"".PATH_URL."product.php?mod=detail&id=$id\">".$product['productname']."</a>";
	}else{
		if($config['routeurltype']==1){
			$product['url'] = PATH_URL."product-".$product['productid'].".html";
			if(intval($product['linktype'])==2){
				$product['caturl'] = $product['linkurl'];
			}else{
				$product['caturl'] = PATH_URL."product-cat-".$product['cateid'].".html";
			}

			$navigation  = "<a href=\"".PATH_URL."product.html\">".$LANVAR['product']."</a> >> ";
			if(intval($product['linktype'])==2){
				$navigation .= "<a href=\"".$product['linkurl']."\">".$product['catename']."</a> >> ";
			}else{
				$navigation .= "<a href=\"".PATH_URL."product-cat-".$product['cateid'].".html\">".$product['catename']."</a> >> ";
			}
			$navigation .= "<a href=\"".PATH_URL."product-".$id.".html\">".$product['productname']."</a>";
		}else{
			$product['url'] = PATH_URL."product/".$product['productid'].".html";
			if(intval($product['linktype'])==2){
				$product['caturl'] = $product['linkurl'];
			}else{
				$product['caturl'] = PATH_URL."product/cat-".$product['cateid'].".html";
			}
			$navigation  = "<a href=\"".PATH_URL."product\">".$LANVAR['product']."</a> >> ";
			if(intval($product['linktype'])==2){
				$navigation .= "<a href=\"".$product['linkurl']."\">".$product['catename']."</a> >> ";
			}else{
				$navigation .= "<a href=\"".PATH_URL."product/cat-".$product['cateid'].".html\">".$product['catename']."</a> >> ";
			}
			$navigation .= "<a href=\"".PATH_URL."product/".$id.".html\">".$product['productname']."</a>";
		}
	}

 	if(!Core_Fun::ischar($product['thumbfiles'])){
		$product['thumbfiles'] = PATH_URL."static/images/nopic.jpg";
	}else{
		$product['thumbfiles'] = PATH_URL.$product['thumbfiles'];
	}
	if(Core_Fun::ischar($product['uploadfiles'])){
		$product['uploadfiles'] = PATH_URL.$product['uploadfiles'];
	}

	$product['price'] = Core_Fun::price_format($product['price']);
	if(Core_Fun::ischar($product['metatitle'])){
		$page_title = $product['metatitle'];
	}else{
		$page_title = $product['productname'];
	}
	$page_keyword = $product['metakeyword'];
	$page_description = $product['metadescription'];
	$product['content'] = Core_Command::command_replacetag(1,"product",$product['tag'],$product['content']);
	//$db->update(DB_PREFIX."product",array('hits'=>'[[hits+1]]',),"productid=".$id."");
}
$product['small1'] = str_replace('.jpg', 'thumba.jpg', $product['img1']);
$product['mid1'] = str_replace('.jpg', 'thumbb.jpg', $product['img1']);
/* 同分类下的相关产品 */
$volist_relatedproduct = Mod_Product::volist("AND v.cateid=".$product['cateid']." AND v.productid<>$id","","");

$tpl->assign("id",$id);
$tpl->assign("product",$product);
/*echo '<pre>';
print_r($product);*/
$tpl->assign("navigation",$navigation);
$tpl->assign("previous_item",Mod_Product::previousitem($id));
$tpl->assign("next_item",Mod_Product::nextitem($id));
$tpl->assign("volist_relatedproduct",$volist_relatedproduct);
if((Core_Mod::viewisshow($product['ugroupid'],$product['exclusive']))==1){
$tpl->assign("viewfalgcontent","yes");
}else
{
$tpl->assign("viewfalgcontent","no");
}
$tpl->assign("page_title",$page_title."-".$LANVAR['product']."-".$config['sitename']);
$tpl->assign("page_description",$page_description);
$tpl->assign("page_keyword",$page_keyword);
?>