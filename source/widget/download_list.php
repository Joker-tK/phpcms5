<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.09.15
 * @Id         新闻资讯列表
**/
if(!defined('ALLOWGUEST')) {
	exit('Access Denied');
}

/* params */
$cid		= Core_Fun::detect_number(Core_Fun::rec_post("cid"));
$page		= Core_Fun::detect_number(Core_Fun::rec_post("page"),1);
$pagesize	= $config['downpagesize'];
if($page<1){$page=1;}

/* volist */
$searchsql	= " WHERE v.flag=1";
if(intval($cid)>0){
	$searchsql .= " AND v.cateid='".intval($cid)."'";
}

if(Core_Fun::ischar($keyword)){
//	$searchsql .= " AND v.barcode LIKE '%".$keyword."%'";
	$searchsql .= " AND v.barcode = '".$keyword."'";
}

$countsql	= "SELECT COUNT(v.downid) FROM ".DB_PREFIX."download AS v".$searchsql;
$total		= $db->fetch_count($countsql);
$pagecount	= ceil($total/$pagesize);
$nextpage	= $page+1;
$prepage	= $page-1;
$start		= ($page-1)*$pagesize;
$sql		= "SELECT v.*,c.catename,c.img,c.cssname FROM ".DB_PREFIX."download AS v".
			 " LEFT JOIN ".DB_PREFIX."downloadcate AS c ON v.cateid=c.cateid".
	         $searchsql." ORDER BY v.downid DESC LIMIT $start, $pagesize";
$download	= $db->getall($sql);
$i			= 1;
foreach($download as $key=>$value){
	if($config['newslen']>0){
		if(strtolower(LJCMS_CHARSET)=="utf-8"){
			$download[$key]['sort_title'] = Core_Fun::cut_str($value['title'],$config['downlen']);
		}else{
			$download[$key]['sort_title'] = Core_Fun::cut_str($value['title'],$config['downlen'],0,"gbk");
		}
	}else{
		$download[$key]['sort_title'] = $value['title'];
	}
	$download[$key]['i'] = $i;
	$i = $i+1;

	if($config['htmltype']=="php"){
		$download[$key]['url'] = PATH_URL."download.php?mod=detail&id=".$value['downid'];
		$download[$key]['caturl'] = PATH_URL."download.php?mod=list&cid=".$value['cateid'];
	}else{
		if($config['routeurltype']==1){
			$download[$key]['url'] = PATH_URL."download-".$value['downid'].".html";
			$download[$key]['caturl'] = PATH_URL."download-cat-".$value['cateid'].".html";
		}else{
			$download[$key]['url'] = PATH_URL."download/".$value['downid'].".html";
			$download[$key]['caturl'] = PATH_URL."download/cat-".$value['cateid'].".html";
		}
	}
}

/* page */
$url = PATH_URL."download.php?mod=list";
if($cid>0){
	$url   .= "&cid=$cid";
}

if(Core_Fun::ischar($keyword)){
	$url .= "&keyword=".urlencode($keyword)."";
}

$channel	= "download";

if(Core_Fun::ischar($keyword)){
	$showpage	= Core_Page::volistpage($channel,$cid,$total,$pagesize,$page,$url,10);
}else{
	$showpage	= Core_Page::volistpage($channel,$cid,$total,$pagesize,$page,$url,10,1);
}



/* category */
$page_title       = $LANVAR['download'];
$page_keyword     = "";
$page_description = "";
if($config['htmltype']=="php"){
	$navigation = "<a href=\"".PATH_URL."download.php\">".$LANVAR['download']."</a>";
}else{
	if($config['routeurltype']==1){
		$navigation = "<a href=\"".PATH_URL."download.html\">".$LANVAR['download']."</a>";
	}else{
		$navigation = "<a href=\"".PATH_URL."download\">".$LANVAR['download']."</a>";
	}
	
}
$navcatname	= $LANVAR['download'];
$navurl     = NULL;
if($cid>0){
	$cate = $db->fetch_first("SELECT * FROM ".DB_PREFIX."downloadcate WHERE cateid='".intval($cid)."'");
	if($cate){
		if(Core_Fun::ischar($cate['metatitle'])){
			$page_title = $cate['metatitle'];
		}else{
			$page_title = $cate['catename'];
		}
		$page_keyword     = $cate['metakeyword'];
		$page_description = $cate['metadescription'];
		if($config['htmltype']=="php"){
			$navurl = "<a href=\"".PATH_URL."download.php?mod=list&cid=".$cate['cateid']."\">".$cate['catename']."</a>";
		}else{
			if($config['routeurltype']==1){
				$navurl = "<a href=\"".PATH_URL."download-cat-".$cate['cateid'].".html\">".$cate['catename']."</a>";
			}else{
				$navurl = "<a href=\"".PATH_URL."download/cat-".$cate['cateid'].".html\">".$cate['catename']."</a>";
			}
		}
		$navigation .= " >> ".$navurl;
		$navcatname = $cate['catename'];
	}
}
if($page>1){
	$page_title .= "-第".$page."页";
}

$tpl->assign("cid",$cid);
$tpl->assign("cate",$cate);
$tpl->assign("showpage",$showpage);
$tpl->assign("total",$total);
$tpl->assign("page",$page);
$tpl->assign("pagesize",$config['newspagesize']);
$tpl->assign("download",$download);
$tpl->assign("navurl",$navurl);
$tpl->assign("navigation",$navigation);
$tpl->assign("navcatname",$navcatname);
$tpl->assign("page_title",$page_title."-".$config['sitename']);
$tpl->assign("page_keyword",$page_keyword);
$tpl->assign("page_description",$page_description);
?>