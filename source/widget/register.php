<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.09.15
 * @Id         关于我们
**/
if(!defined('ALLOWGUEST')) {
	exit('Access Denied');
}

$sql	= "SELECT * FROM ".DB_PREFIX."re where showtime=1";
$result		= $db->getall($sql);


$page_title = $LANVAR['menbercenter'];
$tpl->assign("page_title",$page_title."-".$config['sitename']);
$tpl->assign("page_metadescription",$config['metadescription']);
$tpl->assign("page_metakeyword",$config['metakeyword']);
$tpl->assign("result",$result);
?>