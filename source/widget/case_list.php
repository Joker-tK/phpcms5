<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.01.15
 * @Id         成功案例列表
**/
if(!defined('ALLOWGUEST')) {
	exit('Access Denied');
}

/* params */
$cid		= Core_Fun::detect_number(Core_Fun::rec_post("cid"));
$page		= Core_Fun::detect_number(Core_Fun::rec_post("page"),1);
$pagesize	= $config['casepagesize'];
if($page<1){$page=1;}

/* volist */
$searchsql	= " WHERE v.flag=1";
if(intval($cid)>0){
	$searchsql .= " AND v.cateid='".intval($cid)."'";
}
$countsql	= "SELECT COUNT(v.caseid) FROM ".DB_PREFIX."case AS v".$searchsql;
$total		= $db->fetch_count($countsql);
$pagecount	= ceil($total/$pagesize);
$nextpage	= $page+1;
$prepage	= $page-1;
$start		= ($page-1)*$pagesize;
$sql		= "SELECT v.*,c.catename,c.img,c.cssname,c.target,c.linktype,c.linkurl FROM ".DB_PREFIX."case AS v".
			 " LEFT JOIN ".DB_PREFIX."casecate AS c ON v.cateid=c.cateid".
	         $searchsql." ORDER BY v.caseid DESC LIMIT $start, $pagesize";
$case	= $db->getall($sql);
$i		= 1;
foreach($case as $key=>$value){
	if($config['caselen']>0){
		if(strtolower(LJCMS_CHARSET)=="utf-8"){
			$case[$key]['sort_title'] = Core_Fun::cut_str($value['title'],$config['caselen']);
		}else{
			$case[$key]['sort_title'] = Core_Fun::cut_str($value['title'],$config['caselen'],0,"gbk");
		}
	}else{
		$case[$key]['sort_title'] = $value['title'];
	}
	$case[$key]['i'] = $i;
	$i = $i+1;

	if($config['htmltype']=="php"){
		$case[$key]['url'] = PATH_URL."case.php?mod=detail&id=".$value['caseid'];
		$case[$key]['caturl'] = PATH_URL."case.php?mod=list&cid=".$value['cateid'];
	}else{
		if($config['routeurltype']==1){
			$case[$key]['url'] = PATH_URL."case-".$value['caseid'].".html";
			$case[$key]['caturl'] = PATH_URL."case-cat-".$value['cateid'].".html";
		}else{
			$case[$key]['url'] = PATH_URL."case/".$value['caseid'].".html";
			$case[$key]['caturl'] = PATH_URL."case/cat-".$value['cateid'].".html";
		}
	}
	if(intval($value['linktype'])==2){
		$case[$key]['caturl'] = $value['linkurl'];
	}
	if(intval($value['target'])==2){
		$case[$key]['target'] = "_blank";
	}else{
		$case[$key]['target'] = "_self";
	}
	if(!Core_Fun::ischar($value['thumbfiles'])){
		$case[$key]['thumbfiles'] = PATH_URL."static/images/nopic.jpg";
	}else{
		$case[$key]['thumbfiles'] = PATH_URL.$value['thumbfiles'];
	}
	if(Core_Fun::ischar($value['uploadfiles'])){
		$case[$key]['uploadfiles'] = PATH_URL.$value['uploadfiles'];
	}
	//$case[$key]["filter_content"] = Core_Fun::filterhtml($value['content']);
}

/* page */
$url = PATH_URL."case.php?mod=list";
if($cid>0){
	$url   .= "&cid=$cid";
}
$channel	= "case";
$showpage	= Core_Page::volistpage($channel,$cid,$total,$pagesize,$page,$url,10);

/* category */
$page_title       = $LANVAR['case'];
$page_keyword     = "";
$page_description = "";
if($config['htmltype']=="php"){
	$navigation = "<a href=\"".PATH_URL."case.php\">".$LANVAR['case']."</a>";
}else{
	if($config['routeurltype']==1){
		$navigation = "<a href=\"".PATH_URL."case.html\">".$LANVAR['case']."</a>";
	}else{
		$navigation = "<a href=\"".PATH_URL."case\">".$LANVAR['case']."</a>";
	}
	
}
$navcatname	= $LANVAR['case'];
$navurl     = NULL;
if($cid>0){
	$cate = $db->fetch_first("SELECT * FROM ".DB_PREFIX."casecate WHERE cateid='".intval($cid)."'");
	if($cate){
		if(Core_Fun::ischar($cate['metatitle'])){
			$page_title = $cate['metatitle']."-".$page_title;
		}else{
			$page_title = $cate['catename']."-".$page_title;
		}
		$page_keyword     = $cate['metakeyword'];
		$page_description = $cate['metadescription'];
		if(intval($cate['target'])==2){
			$target = "_blank";
		}else{
			$target  = "_self";
		}
		if(intval($cate['linktype'])==2){
			$navurl = "<a href=\"".$cate['linkurl']."\" target='".$target."'>".$cate['catename']."</a>";
		}else{
			if($config['htmltype']=="php"){
				$navurl = "<a href=\"".PATH_URL."case.php?mod=list&cid=".$cate['cateid']."\" target='".$target."'>".$cate['catename']."</a>";
			}else{
				if($config['routeurltype']==1){
					$navurl = "<a href=\"".PATH_URL."case-cat-".$cate['cateid'].".html\"  target='".$target."'>".$cate['catename']."</a>";
				}else{
					$navurl = "<a href=\"".PATH_URL."case/cat-".$cate['cateid'].".html\"  target='".$target."'>".$cate['catename']."</a>";
				}
			}
		}
		$navigation .= " >> ".$navurl;
		$navcatname = $cate['catename'];
	}
}
if($page>1){
	$page_title .= "-第".$page."页";
}

$tpl->assign("cid",$cid);
$tpl->assign("cate",$cate);
$tpl->assign("showpage",$showpage);
$tpl->assign("total",$total);
$tpl->assign("page",$page);
$tpl->assign("pagecount",$pagecount);
$tpl->assign("pagesize",$config['casepagesize']);
$tpl->assign("case",$case);
$tpl->assign("navurl",$navurl);
$tpl->assign("navigation",$navigation);
$tpl->assign("navcatname",$navcatname);
$tpl->assign("page_title",$page_title."-".$config['sitename']);
$tpl->assign("page_keyword",$page_keyword);
$tpl->assign("page_description",$page_description);
?>