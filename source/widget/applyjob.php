<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.09.15
 * @Id         在线留言
**/

if(!defined('ALLOWGUEST')) {
	exit('Access Denied');
}
$action = Core_Fun::rec_post("action");


if($action=="saveadd"){
	$jobid = Core_Fun::rec_post("jobid",1);
	$brothday      = Core_Fun::rec_post("brothday",1);
	$chinatext     = Core_Fun::rec_post("chinatext",1);
	$telno       = Core_Fun::rec_post("telno",1);
	$email        = Core_Fun::rec_post("email",1);
	$degree   = Core_Fun::rec_post("degree",1);
	$prosesion      = Core_Fun::rec_post("prosesion",1);
	$school         = Core_Fun::rec_post("school",1);
	$address       = Core_Fun::rec_post("address",1);
	$awards     = Core_Fun::rec_post("awards",1);
	$experience     = Core_Fun::rec_post("experience",1);
	$hobby     = Core_Fun::rec_post("hobby",1);
	$flag      =1;
    if (trim($_SESSION["USERNAME"])!="")
	{
	$userid=trim($_SESSION["USERID"]);
	$username    = trim($_SESSION["USERNAME"]);
    }
	else 
	{
    $userid=0;
	$username    = Core_Fun::rec_post("username",1);
	}

	$founderr    = false;
	if(!Core_Fun::ischar($username)){
		$founderr = true;
		$errmsg  .= "姓名不能为空<br />";
	}
	if(!Core_Fun::ischar($chinatext)){
		$founderr = true;
		$errmsg  .= "国籍不能为空<br />";
	}
	if(!Core_Fun::ischar($telno)){
		$founderr = true;
		$errmsg  .= "联系电话不能为空<br />";
	}
	if(!Core_Fun::ischar($email)){
		$founderr = true;
		$errmsg  .= "E-mail不能为空<br />";
	}
	
	$aid = $db->fetch_newid("SELECT MAX(aid) FROM ".DB_PREFIX."applyjob",1);
	$array  = array(
		'aid'=>$aid,
		'username'=>$username,
		'jobid'=>$jobid,
		'userid'=>$userid,
		'brothday'=>$brothday,
		'chinatext'=>$chinatext,
		'telno'=>$telno,
		'email'=>$email,
		'degree'=>$degree,
		'prosesion'=>$prosesion,
		'school'=>$school,
		'address'=>$address,
		'awards'=>$awards,
		'experience'=>$experience,
		'hobby'=>$hobby,
		'flag'=>$flag,
		'ip'=>Core_Fun::getip(),
		'addtime'=>time(),
	);

	$db->insert(DB_PREFIX."applyjob",$array);
	if($urlsuffix=='php'){
		$url = PATH_URL."applyjob.php";
	}else{
		if($config['routeurltype']==1){
			$url = PATH_URL."applyjob.html";
		}else{
			$url = PATH_URL."applyjob";
		}
	}
	Core_Fun::halt("在线应聘申请成功，我们将会尽快给您联系，感谢您的支持！","$url",0);

}else{
	//$page_title = $LANVAR['guestbook'];


	$tpl->assign("username",$_SESSION["USERNAME"]);
	$tpl->assign("page_title","在线应聘");
	$tpl->assign("page_metadescription",$config['metadescription']);
	$tpl->assign("page_metakeyword",$config['metakeyword']);
}
?>