<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.01.15
 * @Id         下载内容
**/
if(!defined('ALLOWGUEST')) {
	exit('Access Denied');
}
$id = Core_Fun::detect_number(Core_Fun::rec_post("id"));
if($id<1){
	Core_Fun::halt("ID丢失或者已删除！","",1);
}
$sql	= "SELECT v.*,c.catename,c.img,c.cssname FROM ".DB_PREFIX."download AS v".
         " LEFT JOIN ".DB_PREFIX."downloadcate AS c ON v.cateid=c.cateid".
	     " WHERE v.flag=1 AND v.downid='".intval($id)."'";
$download= $db->fetch_first($sql);
if(!$download){
	Core_Fun::halt("对不起，信息不存在或已删除！","",1);
}else{
    
	/* url和导航 */
	if($config['htmltype']=="php"){
		$download['url'] = PATH_URL."download.php?mod=detail&id=".$download['downid'];
		$download['caturl'] = PATH_URL."download.php?mod=list&cid=".$download['cateid'];

		$navigation  = "<a href=\"".PATH_URL."download.php\">".$LANVAR['download']."</a> >> ";
		$navigation .= "<a href=\"".PATH_URL."download.php?mod=list&cid=".$download['cateid']."\">".$download['catename']."</a> >> ";
		$navigation .= "<a href=\"".PATH_URL."download.php?mod=detail&id=$id\">".$download['title']."</a>";

	}else{
		if($config['routeurltype']==1){
			$download['url'] = PATH_URL."download-".$download['downid'].".html";
			$download['caturl'] = PATH_URL."download-cat-".$download['cateid'].".html";

			$navigation  = "<a href=\"".PATH_URL."download.html\">".$LANVAR['download']."</a> >> ";
			$navigation .= "<a href=\"".PATH_URL."download-cat-".$download['cateid'].".html\">".$download['catename']."</a> >> ";
			$navigation .= "<a href=\"".PATH_URL."download-".$id.".html\">".$download['title']."</a>";
		}else{
			$download['url'] = PATH_URL."download/".$download['downid'].".html";
			$download['caturl'] = PATH_URL."download/cat-".$download['cateid'].".html";

			$navigation  = "<a href=\"".PATH_URL."download\">".$LANVAR['download']."</a> >> ";
			$navigation .= "<a href=\"".PATH_URL."download/cat-".$download['cateid'].".html\">".$download['catename']."</a> >> ";
			$navigation .= "<a href=\"".PATH_URL."download/".$id.".html\">".$download['title']."</a>";
		}
	}
	$download['downloadurl'] = PATH_URL."download.php?mod=down&id=".$download['downid']."";

	if(Core_Fun::ischar($download['metatitle'])){
		$page_title = $download['metatitle'];
	}else{
		$page_title = $download['title'];
	}
	$page_keyword = $download['metakeyword'];
	$page_description = $download['metadescription'];
	$db->update(DB_PREFIX."download",array('hits'=>'[[hits+1]]',),"downid=".$id."");
}

/* related info */
$volist_relateddownload = Mod_Down::volist("AND v.cateid=".$download['cateid']." AND v.downid<>$id","","");

$tpl->assign("id",$id);
$tpl->assign("download",$download);
$tpl->assign("navigation",$navigation);
$tpl->assign("previous_item",Mod_Down::previousitem($id));
$tpl->assign("next_item",Mod_Down::nextitem($id));
$tpl->assign("volist_relateddownload",$volist_relateddownload);
if((Core_Mod::viewisshow($info['ugroupid'],$info['exclusive']))==1){
$tpl->assign("viewfalgcontent","yes");
}else
{
$tpl->assign("viewfalgcontent","no");
}
$tpl->assign("page_title",$page_title."-".$LANVAR['download']."-".$config['sitename']);
$tpl->assign("page_description",$page_description);
$tpl->assign("page_keyword",$page_keyword);
?>