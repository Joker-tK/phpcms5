<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.01.15
 * @Id         成功案例_内容
**/
if(!defined('ALLOWGUEST')) {
	exit('Access Denied');
}
$id = Core_Fun::detect_number(Core_Fun::rec_post("id"));
if($id<1){
	Core_Fun::halt("ID丢失或者已删除！","",1);
}
$sql	= "SELECT v.*,c.catename,c.img,c.cssname,c.target,c.linktype,c.linkurl FROM ".DB_PREFIX."case AS v".
         " LEFT JOIN ".DB_PREFIX."casecate AS c ON v.cateid=c.cateid".
	     " WHERE v.flag=1 AND v.caseid='".intval($id)."'";
$case = $db->fetch_first($sql);
if(!$case){
	Core_Fun::halt("对不起，信息不存在或已删除！","",1);
}else{
	if(intval($case['target'])==2){
		$target = "_blank";
	}else{
		$target = "_self";
	}
	/* url和导航 */
	if($config['htmltype']=="php"){
		$case['url'] = PATH_URL."case.php?mod=detail&id=".$case['caseid'];
		if(intval($case['linktype'])==2){
			$case['caturl'] = $case['linkurl'];
		}else{
			$case['caturl'] = PATH_URL."case.php?mod=list&cid=".$case['cateid'];
		}
		$navigation  = "<a href=\"".PATH_URL."case.php\">".$LANVAR['case']."</a> >> ";
		if(intval($case['linktype'])==2){
			$navigation .= "<a href=\"".$case['linkurl']."\">".$case['catename']."</a> >> ";
		}else{
			$navigation .= "<a href=\"".PATH_URL."case.php?mod=list&cid=".$case['cateid']."\">".$case['catename']."</a> >> ";
		}
		$navigation .= "<a href=\"".PATH_URL."case.php?mod=detail&id=$id\">".$case['title']."</a>";


	}else{
		if($config['routeurltype']==1){
			$case['url'] = PATH_URL."case-".$case['caseid'].".html";
			if(intval($case['linktype'])==2){
				$case['caturl'] = $case['linkurl'];
			}else{
				$case['caturl'] = PATH_URL."case-cat-".$case['cateid'].".html";
			}

			$navigation  = "<a href=\"".PATH_URL."case.html\">".$LANVAR['case']."</a> >> ";
			if(intval($case['linktype'])==2){
				$navigation .= "<a href=\"".$case['linkurl']."\">".$case['catename']."</a> >> ";
			}else{
				$navigation .= "<a href=\"".PATH_URL."case-cat-".$case['cateid'].".html\">".$case['catename']."</a> >> ";
			}
			$navigation .= "<a href=\"".PATH_URL."case-".$id.".html\">".$case['title']."</a>";
		}else{
			$case['url'] = PATH_URL."case/".$case['caseid'].".html";
			if(intval($case['linktype'])==2){
				$case['caturl'] = $case['linkurl'];
			}else{
				$case['caturl'] = PATH_URL."case/cat-".$case['cateid'].".html";
			}
			$navigation  = "<a href=\"".PATH_URL."case\">".$LANVAR['case']."</a> >> ";
			if(intval($case['linktype'])==2){
				$navigation .= "<a href=\"".$case['linkurl']."\">".$case['catename']."</a> >> ";
			}else{
				$navigation .= "<a href=\"".PATH_URL."case/cat-".$case['cateid'].".html\">".$case['catename']."</a> >> ";
			}
			$navigation .= "<a href=\"".PATH_URL."case/".$id.".html\">".$case['title']."</a>";
		}
	}

 	if(!Core_Fun::ischar($case['thumbfiles'])){
		$case['thumbfiles'] = PATH_URL."static/images/nopic.jpg";
	}else{
		$case['thumbfiles'] = PATH_URL.$case['thumbfiles'];
	}
	if(Core_Fun::ischar($case['uploadfiles'])){
		$case['uploadfiles'] = PATH_URL.$case['uploadfiles'];
	}
	if(Core_Fun::ischar($case['metatitle'])){
		$page_title = $case['metatitle'];
	}else{
		$page_title = $case['title'];
	}
	$page_keyword = $case['metakeyword'];
	$page_description = $case['metadescription'];
	$case['content'] = Core_Command::command_replacetag(1,"case",$case['tag'],$case['content']);
	//$db->update(DB_PREFIX."case",array('hits'=>'[[hits+1]]',),"caseid=".$id."");
}

/* related info */
$volist_relatedcase = Mod_Case::volist("AND v.cateid=".$case['cateid']." AND v.caseid<>$id","","");

$tpl->assign("id",$id);
$tpl->assign("case",$case);
$tpl->assign("navigation",$navigation);
$tpl->assign("previous_item",Mod_Case::previousitem($id));
$tpl->assign("next_item",Mod_Case::nextitem($id));
$tpl->assign("volist_relatedcase",$volist_relatedcase);
if((Core_Mod::viewisshow($info['ugroupid'],$info['exclusive']))==1){
$tpl->assign("viewfalgcontent","yes");
}else
{
$tpl->assign("viewfalgcontent","no");
}
$tpl->assign("page_title",$page_title."-".$LANVAR['case']."-".$config['sitename']);
$tpl->assign("page_description",$page_description);
$tpl->assign("page_keyword",$page_keyword);
?>