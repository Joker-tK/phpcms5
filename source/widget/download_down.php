<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.01.15
 * @Id         下载跳转页
**/
if(!defined('ALLOWGUEST')) {
	exit('Access Denied');
}
$id = Core_Fun::detect_number(Core_Fun::rec_post("id"));
if($id<1){
	Core_Fun::halt("ID丢失或者已删除！","",1);
}
$sql	= "SELECT uploadfiles FROM ".DB_PREFIX."download WHERE downid='".intval($id)."'";
$download= $db->fetch_first($sql);
if(!$download){
	Core_Fun::halt("对不起，信息不存在或已删除！","",1);
}else{
    $db->update(DB_PREFIX."download",array('downs'=>'[[downs+1]]'),"downid=$id");
	if(Core_Fun::ischar($download['uploadfiles'])){
		$filename = basename($download['uploadfiles']);
		$file_extension = strtolower(substr(strrchr($filename,"."),1));
		if(in_array($file_extension,array('php','html','htm','asp','aspx','shtml','jsp'))){
			Core_Fun::halt("对不起，不允许下载 “.".$file_extension."”的文件！","",1);
		}
		echo("<script language='javascript'>top.window.location='".$download['uploadfiles']."';</script>");
	}
	die();
}
?>