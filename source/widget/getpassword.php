<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.09.15
 * @Id         在线留言
**/

if(!defined('ALLOWGUEST')) {
	exit('Access Denied');
}
$uaction = Core_Fun::rec_post("uaction");

switch($uaction){
    case 'selectuser';
	    selectuser();
		break;
	case 'seleemial';
	    seleemial();
		break;
	case 'updatepwd';
	    updatepwd();
		break;
	default;
        break;
}

function selectuser(){
	//Core_Auth::checkauth("adminedit");
	global $db,$tpl;
    $username = Core_Fun::rec_post('username');
	if(trim($username=="")){
		Core_Fun::halt("请输入会员名字","",2);
	}
	$sql		= "SELECT a.* FROM ".DB_PREFIX."user AS a where a.loginname='$username' ORDER BY a.userid ASC ";
	$user = $db->fetch_first($sql);
	if(!$user){
		Core_Fun::halt("用户不存在","",2);
	}else{
	$tpl->assign("id",$id);
	$tpl->assign("comeurl",$GLOBALS['comeurl']);
	$tpl->assign("user",$user);
	}
}

function seleemial(){
	//Core_Auth::checkauth("adminedit");
	global $db,$tpl;
    $loginname = Core_Fun::rec_post('loginname',1);
    $email = Core_Fun::rec_post('email',1);
	if(trim($loginname=="")){
		Core_Fun::halt("请输入会员名字","",2);
	}
	if(trim($email=="")){
		Core_Fun::halt("请输入email","",2);
	}
	$sql = "SELECT a.* FROM ".DB_PREFIX."user AS a where 1=1 and a.loginname='$loginname' and a.email='$email' ORDER BY a.userid ASC ";
	$users = $db->fetch_first($sql);
	if(!$users){
		Core_Fun::halt("用户不存在","",2);
	}else{
	$tpl->assign("id",$id);
	$tpl->assign("comeurl",$GLOBALS['comeurl']);
	$tpl->assign("users",$users);
	}
}


function updatepwd(){
	//Core_Auth::checkauth("adminedit");
	global $db;
	$userid		= Core_Fun::rec_post('userid',1);
	$loginname	= Core_Fun::rec_post('loginname',1);
    $password	= Core_Fun::rec_post('password',1);
    $password1	= Core_Fun::rec_post('password1',1);

	$founderr	= false;

	if(trim($password)==""){
	    $founderr	= true;
		$errmsg	   .= "密码不能为空<br />";
	}

	if(trim($password)!=trim($password1)){
	    $founderr	= true;
		$errmsg	   .= "与重复密码不符合<br />";
	}

	if(!Core_Fun::isnumber($userid)){
	    $founderr	= true;
		$errmsg	   .= "用户丢失.<br />";
	}

	if($founderr == true){
	    Core_Fun::halt($errmsg,"getpassword.php",1);
	}
	$array = array(
			'password'=>md5($password),
	);

	$result = $db->update(DB_PREFIX."user",$array,"userid=$userid");
	if($result){
		Core_Command::runlog("","修改会员帐号成功[id=$id]");
		Core_Fun::halt($loginname."密码修改成功你的","index.php",0);
	}else{
		Core_Fun::halt("修改失败","",2);
	}
}

$tpl->assign("uaction",$uaction);
$tpl->assign("page_title","我的留言");
$tpl->assign("page_metadescription",$config['metadescription']);
$tpl->assign("page_metakeyword",$config['metakeyword']);






?>