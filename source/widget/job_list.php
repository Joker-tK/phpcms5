<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.01.15
 * @Id         人才招聘列表
**/
if(!defined('ALLOWGUEST')) {
	exit('Access Denied');
}

/* params */
$cid		= Core_Fun::detect_number(Core_Fun::rec_post("cid"));
$page		= Core_Fun::detect_number(Core_Fun::rec_post("page"),1);
$pagesize	= $config['jobpagesize'];
if($page<1){$page=1;}

/* volist */
$searchsql	= " WHERE v.flag=1";
if(intval($cid)>0){
	$searchsql .= " AND v.cateid='".intval($cid)."'";
}
$countsql	= "SELECT COUNT(v.jobid) FROM ".DB_PREFIX."job AS v".$searchsql;
$total		= $db->fetch_count($countsql);
$pagecount	= ceil($total/$pagesize);
$nextpage	= $page+1;
$prepage	= $page-1;
$start		= ($page-1)*$pagesize;
$sql		= "SELECT v.*,c.catename,c.img,c.cssname,c.target,c.linktype,c.linkurl FROM ".DB_PREFIX."job AS v".
			 " LEFT JOIN ".DB_PREFIX."jobcate AS c ON v.cateid=c.cateid".
	         $searchsql." ORDER BY v.jobid DESC LIMIT $start, $pagesize";
$job		= $db->getall($sql);
$i			= 1;
foreach($job as $key=>$value){
	if($config['joblen']>0){
		if(strtolower(LJCMS_CHARSET)=="utf-8"){
			$job[$key]['sort_title'] = Core_Fun::cut_str($value['title'],$config['joblen']);
		}else{
			$job[$key]['sort_title'] = Core_Fun::cut_str($value['title'],$config['joblen'],0,"gbk");
		}
	}else{
		$job[$key]['sort_title'] = $value['title'];
	}
	$job[$key]['i'] = $i;
	$i = $i+1;

	if($config['htmltype']=="php"){
		$job[$key]['url'] = PATH_URL."job.php?mod=detail&id=".$value['jobid'];
		$job[$key]['caturl'] = PATH_URL."job.php?mod=list&cid=".$value['cateid'];
	}else{
		if($config['routeurltype']==1){
			$job[$key]['url'] = PATH_URL."job-".$value['jobid'].".html";
			$job[$key]['caturl'] = PATH_URL."job-cat-".$value['cateid'].".html";
		}else{
			$job[$key]['url'] = PATH_URL."job/".$value['jobid'].".html";
			$job[$key]['caturl'] = PATH_URL."job/cat-".$value['cateid'].".html";
		}
	}
	if(intval($value['linktype'])==2){
		$job[$key]['caturl'] = $value['linkurl'];
	}
	if(intval($value['target'])==2){
		$job[$key]['target'] = "_blank";
	}else{
		$job[$key]['target'] = "_self";
	}
}

/* page */
$url = PATH_URL."job.php?mod=list";
if($cid>0){
	$url   .= "&cid=$cid";
}
$channel	= "job";
$showpage	= Core_Page::volistpage($channel,$cid,$total,$pagesize,$page,$url,10);

/* category */
$page_title       = $LANVAR['job'];
$page_keyword     = "";
$page_description = "";
if($config['htmltype']=="php"){
	$navigation = "<a href=\"".PATH_URL."job.php\">".$LANVAR['job']."</a>";
}else{
	if($config['routeurltype']==1){
		$navigation = "<a href=\"".PATH_URL."job.html\">".$LANVAR['job']."</a>";
	}else{
		$navigation = "<a href=\"".PATH_URL."job\">".$LANVAR['job']."</a>";
	}
	
}
$navcatname	= $LANVAR['job'];
$navurl     = NULL;
if($cid>0){
	$cate = $db->fetch_first("SELECT * FROM ".DB_PREFIX."jobcate WHERE cateid='".intval($cid)."'");
	if($cate){
		if(Core_Fun::ischar($cate['metatitle'])){
			$page_title = $cate['metatitle'];
		}else{
			$page_title = $cate['catename'];
		}
		$page_keyword     = $cate['metakeyword'];
		$page_description = $cate['metadescription'];
		if(intval($cate['target'])==2){
			$target = "_blank";
		}else{
			$target  = "_self";
		}
		if(intval($cate['linktype'])==2){
			$navurl = "<a href=\"".$cate['linkurl']."\" target='".$target."'>".$cate['catename']."</a>";
		}else{
			if($config['htmltype']=="php"){
				$navurl = "<a href=\"".PATH_URL."job.php?mod=list&cid=".$cate['cateid']."\">".$cate['catename']."</a>";
			}else{
				if($config['routeurltype']==1){
					$navurl = "<a href=\"".PATH_URL."job-cat-".$cate['cateid'].".html\">".$cate['catename']."</a>";
				}else{
					$navurl = "<a href=\"".PATH_URL."job/cat-".$cate['cateid'].".html\">".$cate['catename']."</a>";
				}
			}
		}
		$navigation .= " >> ".$navurl;
		$navcatname = $cate['catename'];
	}
}
if($page>1){
	$page_title .= "-第".$page."页";
}

$tpl->assign("cid",$cid);
$tpl->assign("cate",$cate);
$tpl->assign("showpage",$showpage);
$tpl->assign("total",$total);
$tpl->assign("page",$page);
$tpl->assign("pagecount",$pagecount);
$tpl->assign("pagesize",$config['jobpagesize']);
$tpl->assign("job",$job);
$tpl->assign("navurl",$navurl);
$tpl->assign("navigation",$navigation);
$tpl->assign("navcatname",$navcatname);
$tpl->assign("page_title",$page_title."-".$config['sitename']);
$tpl->assign("page_keyword",$page_keyword);
$tpl->assign("page_description",$page_description);
?>