<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.01.15
 * @Id         解决方案列表
**/
if(!defined('ALLOWGUEST')) {
	exit('Access Denied');
}

/* params */
$cid		= Core_Fun::detect_number(Core_Fun::rec_post("cid"));
$page		= Core_Fun::detect_number(Core_Fun::rec_post("page"),1);
$pagesize	= $config['solutionpagesize'];
if($page<1){$page=1;}
/* volist */
$searchsql	= " WHERE v.flag=1";
if((int)$cid>0){
	$childs_sql = Core_Mod::build_childsql("solutioncate","v",intval($cid),"");
	if(Core_Fun::ischar($childs_sql)){
		$searchsql .= " AND (v.cateid='".intval($cid)."'".$childs_sql.")";
	}else{
		$searchsql .= " AND v.cateid='".intval($cid)."'";
	}
}
$countsql	= "SELECT COUNT(v.solutionid) FROM ".DB_PREFIX."solution AS v".$searchsql;
$total		= $db->fetch_count($countsql);
$pagecount	= ceil($total/$pagesize);
$nextpage	= $page+1;
$prepage	= $page-1;
$start		= ($page-1)*$pagesize;
$sql		= "SELECT v.*,c.catename,c.img,c.cssname,c.target,c.linktype,c.linkurl FROM ".DB_PREFIX."solution AS v".
			 " LEFT JOIN ".DB_PREFIX."solutioncate AS c ON v.cateid=c.cateid".
	         $searchsql." ORDER BY v.solutionid DESC LIMIT $start, $pagesize";
$solution	= $db->getall($sql);
$i			= 1;
foreach($solution as $key=>$value){
	if($config['solutionlen']>0){
		if(strtolower(LJCMS_CHARSET)=="utf-8"){
			$solution[$key]['sort_title'] = Core_Fun::cut_str($value['title'],$config['solutionlen']);
		}else{
			$solution[$key]['sort_title'] = Core_Fun::cut_str($value['title'],$config['solutionlen'],0,"gbk");
		}
	}else{
		$solution[$key]['sort_title'] = $value['title'];
	}
	$solution[$key]['i'] = $i;
	$i = $i+1;

	if($config['htmltype']=="php"){
		$solution[$key]['url'] = PATH_URL."solution.php?mod=detail&id=".$value['solutionid'];
		$solution[$key]['caturl'] = PATH_URL."solution.php?mod=list&cid=".$value['cateid'];
	}else{
		if($config['routeurltype']==1){
			$solution[$key]['url'] = PATH_URL."solution-".$value['solutionid'].".html";
			$solution[$key]['caturl'] = PATH_URL."solution-cat-".$value['cateid'].".html";
		}else{
			$solution[$key]['url'] = PATH_URL."solution/".$value['solutionid'].".html";
			$solution[$key]['caturl'] = PATH_URL."solution/cat-".$value['cateid'].".html";
		}
	}
	if(intval($solution['linktype'])==2){
		$solution[$key]['caturl'] = $value['linkurl'];
	}
	if(intval($value['target'])==2){
		$solution[$key]['target'] = "_blank";
	}else{
		$solution[$key]['target'] = "_self";
	}
	if(!Core_Fun::ischar($value['thumbfiles'])){
		$solution[$key]['thumbfiles'] = PATH_URL."static/images/nopic.jpg";
	}else{
		$solution[$key]['thumbfiles'] = PATH_URL.$value['thumbfiles'];
	}
	if(Core_Fun::ischar($value['uploadfiles'])){
		$solution[$key]['uploadfiles'] = PATH_URL.$value['uploadfiles'];
	}
}

/* page */
$url = PATH_URL."solution.php?mod=list";
if($cid>0){
	$url   .= "&cid=$cid";
}
$channel	= "solution";
$showpage	= Core_Page::volistpage($channel,$cid,$total,$pagesize,$page,$url,10);

/* category */
$page_title       = $LANVAR['solution'];
$page_keyword     = "";
$page_description = "";
if($config['htmltype']=="php"){
	$navigation = "<a href=\"".PATH_URL."solution.php\">".$LANVAR['solution']."</a>";
}else{
	if($config['routeurltype']==1){
		$navigation = "<a href=\"".PATH_URL."solution.html\">".$LANVAR['solution']."</a>";
	}else{
		$navigation = "<a href=\"".PATH_URL."solution\">".$LANVAR['solution']."</a>";
	}
	
}
$navcatname	= $LANVAR['solution'];
$navurl     = NULL;
if($cid>0){
	$cate = $db->fetch_first("SELECT * FROM ".DB_PREFIX."solutioncate WHERE cateid='".intval($cid)."'");
	if($cate){
		if(Core_Fun::ischar($cate['metatitle'])){
			$page_title = $cate['metatitle']."-".$page_title;
		}else{
			$page_title = $cate['catename']."-".$page_title;
		}
		$page_keyword     = $cate['metakeyword'];
		$page_description = $cate['metadescription'];

		if(intval($cate['target'])==2){
			$target = "_blank";
		}else{
			$target  = "_self";
		}
		if(intval($cate['linktype'])==2){
			$navurl = "<a href=\"".$cate['linkurl']."\" target='".$target."'>".$cate['catename']."</a>";
		}else{
			if($config['htmltype']=="php"){
				$navurl = "<a href=\"".PATH_URL."solution.php?mod=list&cid=".$cate['cateid']."\">".$cate['catename']."</a>";
			}else{
				if($config['routeurltype']==1){
					$navurl = "<a href=\"".PATH_URL."solution-cat-".$cate['cateid'].".html\">".$cate['catename']."</a>";
				}else{
					$navurl = "<a href=\"".PATH_URL."solution/cat-".$cate['cateid'].".html\">".$cate['catename']."</a>";
				}
			}
		}
		$navigation .= " >> ".$navurl;
		$navcatname = $cate['catename'];
	}
}
if($page>1){
	$page_title .= "-第".$page."页";
}

$tpl->assign("cid",$cid);
$tpl->assign("cate",$cate);
$tpl->assign("showpage",$showpage);
$tpl->assign("total",$total);
$tpl->assign("page",$page);
$tpl->assign("pagecount",$pagecount);
$tpl->assign("pagesize",$config['solutionpagesize']);
$tpl->assign("solution",$solution);
$tpl->assign("navurl",$navurl);
$tpl->assign("navigation",$navigation);
$tpl->assign("navcatname",$navcatname);
$tpl->assign("page_title",$page_title."-".$config['sitename']);
$tpl->assign("page_keyword",$page_keyword);
$tpl->assign("page_description",$page_description);
?>