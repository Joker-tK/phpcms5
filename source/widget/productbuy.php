<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.09.15
 * @Id         在线留言
**/

if(!defined('ALLOWGUEST')) {
	exit('Access Denied');
}
$action = Core_Fun::rec_post("action");


if($action=="saveadd"){


	$proid = Core_Fun::rec_post("proid",1);
	$ordername = Core_Fun::rec_post("ordername",1);
	$remark      = Core_Fun::rec_post("remark",1);
	$userid     = Core_Fun::rec_post("userid",1);
	$username       = Core_Fun::rec_post("username",1);
	$sex        = Core_Fun::rec_post("sex",1);
	$company   = Core_Fun::rec_post("company",1);
	$address       = Core_Fun::rec_post("address",1);
	$zipcode      = Core_Fun::rec_post("zipcode",1);
	$telephone         = Core_Fun::rec_post("telephone",1);
	$fax     = Core_Fun::rec_post("fax",1);
	$mobile     = Core_Fun::rec_post("mobile",1);
	$email     = Core_Fun::rec_post("email",1);
	$flag      =1;
    if (trim($_SESSION["USERNAME"])!="")
	{
	$userid=trim($_SESSION["USERID"]);
	$username    = trim($_SESSION["USERNAME"]);
    }
	else 
	{
    $userid=0;
	$username    = Core_Fun::rec_post("username",1);
	}
	$founderr    = false;
	if(!Core_Fun::ischar($username)){
		$founderr = true;
		$errmsg  .= "姓名不能为空<br />";
	}
	if(!Core_Fun::ischar($ordername)){
		$founderr = true;
		$errmsg  .= "订购产品不能为空<br />";
	}
	if(!Core_Fun::ischar($telephone)){
		$founderr = true;
		$errmsg  .= "联系电话不能为空<br />";
	}
	if(!Core_Fun::ischar($email)){
		$founderr = true;
		$errmsg  .= "E-mail不能为空<br />";
	}

	if(!Core_Fun::ischar($address)){
		$founderr = true;
		$errmsg  .= "地址不能为空<br />";
	}
	if(!Core_Fun::ischar($remark)){
		$founderr = true;
		$errmsg  .= "订购详细说明不能为空<br />";
	}

if(proid!="")
{
	$sql   = "SELECT * FROM ".DB_PREFIX."product WHERE productid=$proid";
	$pro = $db->fetch_first($sql);
	$ordername="订购产品如下：".$pro["productname"]."<br/>产品的编号为：".$pro["productnum"];
}
else 
{
   $ordername="订购产品出错";
}


	
	$id = $db->fetch_newid("SELECT MAX(id) FROM ".DB_PREFIX."order",1);
	$array  = array(
		'id'=>$id,
		'ordername'=>$ordername,
		'remark'=>$remark,
		'userid'=>$userid,
		'username'=>$username,
		'sex'=>$sex,
		'company'=>$company,
		'address'=>$address,
		'zipcode'=>$zipcode,
		'telephone'=>$telephone,
		'fax'=>$fax,
		'mobile'=>$mobile,
		'email'=>$email,
		'flag'=>$flag,
		'ip'=>Core_Fun::getip(),
		'addtime'=>time(),
	);

	$db->insert(DB_PREFIX."order",$array);
	if($urlsuffix=='php'){
		$url = PATH_URL."productbuy.php";
	}else{
		if($config['routeurltype']==1){
			$url = PATH_URL."productbuy.php";
		}else{
			$url = PATH_URL."productbuy";
		}
	}
	Core_Fun::halt("在线预定成功，我们将会尽快给您联系，感谢您的支持！","$url",0);

}else{
	//$page_title = $LANVAR['guestbook'];


	$tpl->assign("username",$_SESSION["USERNAME"]);
	$tpl->assign("page_title","在线预定");
	$tpl->assign("page_metadescription",$config['metadescription']);
	$tpl->assign("page_metakeyword",$config['metakeyword']);
}
?>