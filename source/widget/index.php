<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.01.15
**/
if(!defined('ALLOWGUEST')) {
	exit('Access Denied');
}
/* 推荐产品 */
$tpl->assign("volist_eliteproduct",Mod_Product::volist("AND v.elite=1","",$config['eliteproductnum']));

/* 标题 */
if(!Core_Fun::ischar($config['sitetitle'])){
	$page_title = $config['sitename'];
}else{
	$page_title = $config['sitetitle'];
}
$tpl->assign("page_title",$page_title);
$tpl->assign("page_description",$config['metadescription']);
$tpl->assign("page_keyword",$config['metakeyword']);
?>