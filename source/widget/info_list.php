<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.09.15
 * @Id         新闻资讯列表
**/
if(!defined('ALLOWGUEST')) {
	exit('Access Denied');
}

/* params */
$cid		= Core_Fun::detect_number(Core_Fun::rec_post("cid"));
$page		= Core_Fun::detect_number(Core_Fun::rec_post("page"),1);
$pagesize	= $config['newspagesize'];
if($page<1){$page=1;}

/* volist */
$searchsql	= " WHERE v.flag=1";
if(intval($cid)>0){
	$searchsql .= " AND v.cateid='".intval($cid)."'";
}
$countsql	= "SELECT COUNT(v.infoid) FROM ".DB_PREFIX."info AS v".$searchsql;
$total		= $db->fetch_count($countsql);
$pagecount	= ceil($total/$pagesize);
$nextpage	= $page+1;
$prepage	= $page-1;
$start		= ($page-1)*$pagesize;
$sql		= "SELECT v.*,c.catename,c.img,c.cssname,c.target,c.linktype,c.linkurl FROM ".DB_PREFIX."info AS v".
			 " LEFT JOIN ".DB_PREFIX."infocate AS c ON v.cateid=c.cateid".
	         $searchsql." ORDER BY v.infoid DESC LIMIT $start, $pagesize";
$info	= $db->getall($sql);
$i			= 1;
foreach($info as $key=>$value){
	if($config['newslen']>0){
		if(strtolower(LJCMS_CHARSET)=="utf-8"){
			$info[$key]['sort_title'] = Core_Fun::cut_str($value['title'],$config['newslen']);
		}else{
			$info[$key]['sort_title'] = Core_Fun::cut_str($value['title'],$config['newslen'],0,"gbk");
		}
	}else{
		$info[$key]['sort_title'] = $value['title'];
	}
	$info[$key]['i'] = $i;
	$i = $i+1;

	if($config['htmltype']=="php"){
		$info[$key]['url'] = PATH_URL."info.php?mod=detail&id=".$value['infoid'];
		$info[$key]['caturl'] = PATH_URL."info.php?mod=list&cid=".$value['cateid'];
	}else{
		if($config['routeurltype']==1){
			$info[$key]['url'] = PATH_URL."info-".$value['infoid'].".html";
			$info[$key]['caturl'] = PATH_URL."info-cat-".$value['cateid'].".html";
		}else{
			$info[$key]['url'] = PATH_URL."info/".$value['infoid'].".html";
			$info[$key]['caturl'] = PATH_URL."info/cat-".$value['cateid'].".html";
		}
	}
	if(intval($value['linktype'])==2){
		$info[$key]['caturl'] = $value['linkurl'];
	}
	if(intval($value['target'])==2){
		$info[$key]['target'] = "_blank";
	}else{
		$info[$key]['target'] = "_self";
	}
	if(!Core_Fun::ischar($value['thumbfiles'])){
		$info[$key]['thumbfiles'] = PATH_URL."static/images/nopic.jpg";
	}else{
		$info[$key]['thumbfiles'] = PATH_URL.$value['thumbfiles'];
	}
	if(Core_Fun::ischar($value['uploadfiles'])){
		$info[$key]['uploadfiles'] = PATH_URL.$value['uploadfiles'];
	}
}

/* page */
$url = PATH_URL."info.php?mod=list";
if($cid>0){
	$url   .= "&cid=$cid";
}
$channel	= "info";
$showpage	= Core_Page::volistpage($channel,$cid,$total,$pagesize,$page,$url,10);

/* category */
$page_title       = $LANVAR['info'];
$page_keyword     = "";
$page_description = "";
if($config['htmltype']=="php"){
	$navigation = "<a href=\"".PATH_URL."info.php\">".$LANVAR['info']."</a>";
}else{
	if($config['routeurltype']==1){
		$navigation = "<a href=\"".PATH_URL."info.html\">".$LANVAR['info']."</a>";
	}else{
		$navigation = "<a href=\"".PATH_URL."info\">".$LANVAR['info']."</a>";
	}
	
}
$navcatname	= $LANVAR['info'];
$navurl     = NULL;
if($cid>0){
	$cate = $db->fetch_first("SELECT * FROM ".DB_PREFIX."infocate WHERE cateid='".intval($cid)."'");
	if($cate){
		if(Core_Fun::ischar($cate['metatitle'])){
			$page_title = $cate['metatitle']."-".$page_title;
		}else{
			$page_title = $cate['catename']."-".$page_title;
		}
		$page_keyword     = $cate['metakeyword'];
		$page_description = $cate['metadescription'];
		if(intval($cate['target'])==2){
			$target = "_blank";
		}else{
			$target  = "_self";
		}
		if(intval($cate['linktype'])==2){
			$navurl = "<a href=\"".$cate['linkurl']."\" target='".$target."'>".$cate['catename']."</a>";
		}else{
			if($config['htmltype']=="php"){
				$navurl = "<a href=\"".PATH_URL."info.php?mod=list&cid=".$cate['cateid']."\">".$cate['catename']."</a>";
			}else{
				if($config['routeurltype']==1){
					$navurl = "<a href=\"".PATH_URL."info-cat-".$cate['cateid'].".html\">".$cate['catename']."</a>";
				}else{
					$navurl = "<a href=\"".PATH_URL."info/cat-".$cate['cateid'].".html\">".$cate['catename']."</a>";
				}
			}
		}
		$navigation .= " >> ".$navurl;
		$navcatname = $cate['catename'];
	}
}
if($page>1){
	$page_title .= "-第".$page."页";
}

$tpl->assign("cid",$cid);
$tpl->assign("cate",$cate);
$tpl->assign("showpage",$showpage);
$tpl->assign("total",$total);
$tpl->assign("page",$page);
$tpl->assign("pagecount",$pagecount);
$tpl->assign("pagesize",$config['newspagesize']);
$tpl->assign("info",$info);
$tpl->assign("navurl",$navurl);
$tpl->assign("navigation",$navigation);
$tpl->assign("navcatname",$navcatname);
$tpl->assign("page_title",$page_title."-".$config['sitename']);
$tpl->assign("page_keyword",$page_keyword);
$tpl->assign("page_description",$page_description);
?>