<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.09.15
 * Id          自定义HTML页面
**/
if(!defined('ALLOWGUEST')) {
	exit('Access Denied');
}
$id = Core_Fun::rec_post("id");
if(!Core_Fun::isnumber($id)){
	Core_Fun::halt("对不起，ID不存在或已删除！","",1);
}
$sql	= "SELECT * FROM ".DB_PREFIX."page WHERE flag=1 AND pageid='".intval($id)."'";
$page	= $db->fetch_first($sql);
if(!$page){
	Core_Fun::halt("对不起，页面不存在或已删除！","",1);
}else{
	$page_keyword		= $page['metakeyword'];
	$page_description	= $page['metadescription'];
	if(Core_Fun::ischar($page['metatitle'])){
		$page_title = $page['metatitle'];
	}else{
		$page_title = $page['title'];
	}
	if($config['htmltype']=="php"){
		$page['url'] = PATH_URL."page.php?mod=detail&id=".$page['pageid']."";
	}else{
		if($config['routeurltype']==1){
			$page['url'] = PATH_URL."page-".$page['pageid'].".html";
		}else{
			$page['url'] = PATH_URL."page/".$page['pageid'].".html";
		}
	}
	$page['content'] = Mod_Rep::configLabel($page['content']);

}
$tpl->assign("id",$id);
$tpl->assign("page",$page);
$tpl->assign("page_title",$page_title."-".$config['sitename']);
$tpl->assign("page_keyword",$page_keyword);
$tpl->assign("page_description",$page_description);
?>