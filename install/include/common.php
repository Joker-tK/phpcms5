<?php
/**
 * LiangJing TEAM
 * ============================================================================
 * * COPYRIGHT LiangJing 2012-2013.
 * http://www.liangjing.org;
 * ----------------------------------------------------------------------------
 * Author:LiangJing
 * Id: init.php 2012-10-19
 */

if(!defined('IN_LiangJing'))
{
	die('Hacking attempt');
}

/* 开启SESSION */
session_start();

/* 取得当前站点所在的根目录 */
define('ROOT_PATH', str_replace('install/include/common.php', '', str_replace('\\', '/', __FILE__)));
define('ROOT_URL', preg_replace('/install\//Ums', '', dirname('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']) . "/"));

include_once ('install.class.php');
include_once ('../install/languages/zh_cn.php');

/* 初始化 */
$install = new Install('utf8');
$file_config = ROOT_PATH . "source/conf/db.inc.php";
$file_configinc = ROOT_PATH . "source/conf/config.inc.php";
$file_lock = ROOT_PATH . "data/install.lock";
?>