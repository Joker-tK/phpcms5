/*
MySQL Backup
Source Server Version: 5.5.8
Source Database: ljcms
Date: 2012-12-16 21:04:33
*/


DROP TABLE IF EXISTS `ljcms_admin`;

CREATE TABLE IF NOT EXISTS `ljcms_admin` (
  `adminid` mediumint(8) unsigned NOT NULL default '0',
  `adminname` varchar(50) default NULL,
  `password` varchar(50) default NULL,
  `groupid` mediumint(8) unsigned default '0',
  `super` smallint(2) unsigned default '0',
  `timeline` int(10) unsigned default '0',
  `flag` smallint(2) unsigned default '0',
  `logintimeline` int(10) unsigned default '0',
  `logintimes` int(10) unsigned default '0',
  `loginip` varchar(50) default NULL,
  `memo` varchar(500) default NULL,
  PRIMARY KEY  (`adminid`),
  KEY `flag` (`flag`),
  KEY `groupid` (`groupid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `ljcms_adsfigure`;

CREATE TABLE IF NOT EXISTS `ljcms_adsfigure` (
  `adsid` mediumint(8) unsigned NOT NULL default '0',
  `adsname` varchar(255) default NULL,
  `zoneid` mediumint(8) unsigned default '0',
  `uploadfiles` varchar(255) default NULL,
  `url` varchar(255) default NULL,
  `width` smallint(2) unsigned default '0',
  `height` smallint(2) unsigned default '0',
  `orders` mediumint(8) unsigned default '0',
  `flag` smallint(2) unsigned default '0',
  `content` text,
  `timeline` int(10) unsigned default '0',
  PRIMARY KEY  (`adsid`),
  KEY `zoneid` (`zoneid`),
  KEY `flag` (`flag`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `ljcms_adszone`;

CREATE TABLE IF NOT EXISTS `ljcms_adszone` (
  `zoneid` mediumint(8) unsigned NOT NULL default '0',
  `zonename` varchar(255) default NULL,
  `zonelabel` varchar(255) default NULL,
  `skinid` mediumint(8) unsigned default '0',
  `orders` mediumint(8) unsigned default '0',
  `flag` smallint(2) unsigned default '0',
  `intro` varchar(500) default NULL,
  `width` smallint(2) unsigned default '0',
  `height` smallint(2) unsigned default '0',
  `slide` smallint(2) unsigned default '0',
  `zonetype` smallint(2) unsigned default '0',
  `timeline` int(10) unsigned default '0',
  PRIMARY KEY  (`zoneid`),
  KEY `skinid` (`skinid`),
  KEY `flag` (`flag`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ljcms_applyjob`;

CREATE TABLE IF NOT EXISTS `ljcms_applyjob` (
  `aid` int(11) NOT NULL auto_increment,
  `username` varchar(30) NOT NULL default '',
  `jobid` int(6) NOT NULL,
  `userid` int(11) NOT NULL default '0',
  `sex` int(2) NOT NULL,
  `brothday` varchar(10) default '',
  `chinatext` varchar(200) default '',
  `telno` varchar(100) default '',
  `email` varchar(200) default '',
  `degree` varchar(200) default '',
  `prosesion` varchar(200) default '',
  `school` varchar(200) default '',
  `address` varchar(200) default '',
  `awards` text,
  `experience` text,
  `hobby` text,
  `flag` smallint(2) NOT NULL default '0',
  `ip` varchar(50) NOT NULL,
  `addtime` varchar(20) NOT NULL,
  `RelyContent` text NOT NULL,
  PRIMARY KEY  (`aid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

DROP TABLE IF EXISTS `ljcms_article`;

CREATE TABLE IF NOT EXISTS `ljcms_article` (
  `articleid` mediumint(8) unsigned NOT NULL default '0',
  `cateid` mediumint(8) unsigned default '0',
  `title` varchar(255) default NULL,
  `thumbfiles` varchar(255) default NULL,
  `uploadfiles` varchar(255) default NULL,
  `summary` text,
  `content` text,
  `timeline` int(10) unsigned default '0',
  `author` varchar(50) default NULL,
  `cfrom` varchar(50) default NULL,
  `flag` smallint(2) unsigned default '0',
  `istop` smallint(2) unsigned default '0',
  `elite` smallint(2) unsigned default '0',
  `headline` smallint(2) unsigned default '0',
  `slide` smallint(2) unsigned default '0',
  `hits` int(10) unsigned default '0',
  `metatitle` varchar(255) default NULL,
  `metakeyword` varchar(255) default NULL,
  `metadescription` varchar(255) default NULL,
  `delimitname` varchar(255) default NULL,
  `deleted` smallint(2) unsigned default '0',
  `tag` varchar(255) default NULL,
  PRIMARY KEY  (`articleid`),
  KEY `cateid` (`cateid`),
  KEY `flag` (`flag`),
  KEY `deleted` (`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ljcms_articlecate`;

CREATE TABLE IF NOT EXISTS `ljcms_articlecate` (
  `cateid` mediumint(8) unsigned NOT NULL default '0',
  `catename` varchar(255) default NULL,
  `metatitle` varchar(255) default NULL,
  `metakeyword` varchar(255) default NULL,
  `metadescription` varchar(255) default NULL,
  `pathname` varchar(50) default NULL,
  `parentid` mediumint(8) unsigned default '0',
  `depth` mediumint(8) unsigned default '0',
  `orders` mediumint(8) unsigned default '0',
  `flag` smallint(2) unsigned default '0',
  `intro` varchar(500) default NULL,
  `timeline` int(10) unsigned default '0',
  `elite` smallint(2) unsigned default '0',
  `cssname` varchar(50) default NULL,
  `img` varchar(255) default NULL,
  `linktype` smallint(2) unsigned default '1',
  `linkurl` varchar(255) default NULL,
  `target` smallint(2) unsigned default '1',
  PRIMARY KEY  (`cateid`),
  KEY `parentid` (`parentid`),
  KEY `orders` (`orders`),
  KEY `flag` (`flag`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ljcms_authgroup`;

CREATE TABLE IF NOT EXISTS `ljcms_authgroup` (
  `groupid` mediumint(8) unsigned NOT NULL default '0',
  `groupname` varchar(50) default NULL,
  `auths` text,
  `flag` smallint(2) unsigned default '0',
  `timeline` int(10) unsigned default '0',
  `orders` smallint(2) unsigned default '0',
  `intro` varchar(500) default NULL,
  PRIMARY KEY  (`groupid`),
  KEY `flag` (`flag`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ljcms_case`;

CREATE TABLE IF NOT EXISTS `ljcms_case` (
  `caseid` mediumint(8) unsigned NOT NULL default '0',
  `title` varchar(255) default NULL,
  `metatitle` varchar(255) default NULL,
  `metakeyword` varchar(255) default NULL,
  `metadescription` varchar(255) default NULL,
  `delimitname` varchar(255) default NULL,
  `thumbfiles` varchar(255) default NULL,
  `uploadfiles` varchar(255) default NULL,
  `intro` text,
  `content` text,
  `cateid` mediumint(8) unsigned default '0',
  `hits` int(10) unsigned default '0',
  `elite` smallint(2) unsigned default '0',
  `isnew` smallint(2) unsigned default '0',
  `hots` smallint(2) unsigned default '0',
  `flag` smallint(2) unsigned default '0',
  `ugroupid` varchar(100) NOT NULL,
  `exclusive` varchar(20) NOT NULL,
  `timeline` int(10) unsigned default '0',
  `deleted` smallint(2) unsigned default '0',
  `tag` varchar(255) default NULL,
  PRIMARY KEY  (`caseid`),
  KEY `cateid` (`cateid`),
  KEY `flag` (`flag`),
  KEY `deleted` (`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ljcms_casecate`;

CREATE TABLE IF NOT EXISTS `ljcms_casecate` (
  `cateid` mediumint(8) unsigned NOT NULL default '0',
  `catename` varchar(255) default NULL,
  `metatitle` varchar(255) default NULL,
  `metakeyword` varchar(255) default NULL,
  `metadescription` varchar(255) default NULL,
  `pathname` varchar(50) default NULL,
  `parentid` mediumint(8) unsigned default '0',
  `depth` mediumint(8) unsigned default '0',
  `orders` smallint(2) unsigned default '0',
  `flag` smallint(2) unsigned default '0',
  `intro` varchar(500) default NULL,
  `timeline` int(10) unsigned default '0',
  `elite` smallint(2) unsigned default '0',
  `cssname` varchar(50) default NULL,
  `img` varchar(255) default NULL,
  `linktype` smallint(2) unsigned default '1',
  `linkurl` varchar(255) default NULL,
  `target` smallint(2) unsigned default '1',
  PRIMARY KEY  (`cateid`),
  KEY `parentid` (`parentid`),
  KEY `flag` (`flag`),
  KEY `orders` (`orders`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ljcms_config`;

CREATE TABLE IF NOT EXISTS `ljcms_config` (
  `sitename` varchar(255) default NULL,
  `sitetitle` varchar(255) default NULL,
  `siteurl` varchar(255) default NULL,
  `metadescription` varchar(500) default NULL,
  `metakeyword` varchar(500) default NULL,
  `indexvideo` text NOT NULL,
  `sitecopyright` text,
  `icpcode` varchar(255) default NULL,
  `tjcode` text,
  `about` text,
  `contact` text,
  `logoimg` varchar(255) default NULL,
  `logowidth` smallint(2) unsigned default '0',
  `logoheight` smallint(2) unsigned default '0',
  `bannerimg` varchar(255) default NULL,
  `bannerwidth` smallint(2) unsigned default '0',
  `bannerheight` smallint(2) unsigned default '0',
  `licestatus` smallint(2) unsigned default '0',
  `softkey` varchar(50) default NULL,
  `htmltype` varchar(50) default NULL,
  `routeurltype` smallint(2) unsigned default '0',
  `maxthumbwidth` smallint(2) unsigned default '0',
  `maxthumbheight` smallint(2) unsigned default '0',
  `thumbwidth` smallint(2) unsigned default '0',
  `thumbheight` smallint(2) unsigned default '0',
  `productthumbwidth` smallint(2) unsigned default '0',
  `productthumbheight` smallint(2) unsigned default '0',
  `casethumbwidth` smallint(2) unsigned default '0',
  `casethumbheight` smallint(2) unsigned default '0',
  `solutionthumbwidth` smallint(2) unsigned default '0',
  `solutionthumbheight` smallint(2) unsigned default '0',
  `watermarkflag` smallint(2) unsigned default '0',
  `watermarkfile` varchar(255) default NULL,
  `watermarkpos` smallint(2) unsigned default '0',
  `newspagesize` smallint(2) unsigned default '15',
  `newsnum` smallint(2) unsigned default '10',
  `newslen` smallint(2) unsigned default '10',
  `articlepagesize` smallint(2) unsigned default '15',
  `articlenum` smallint(2) unsigned default '10',
  `articlelen` smallint(2) unsigned default '10',
  `productpagesize` smallint(2) unsigned default '15',
  `productnum` smallint(2) unsigned default '10',
  `productlen` smallint(2) unsigned default '10',
  `casepagesize` smallint(2) unsigned default '15',
  `casenum` smallint(2) unsigned default '10',
  `caselen` smallint(2) unsigned default '10',
  `jobpagesize` smallint(2) unsigned default '15',
  `jobnum` smallint(2) unsigned default '10',
  `joblen` smallint(2) unsigned default '10',
  `downpagesize` smallint(2) unsigned default '15',
  `downnum` smallint(2) unsigned default '10',
  `downlen` smallint(2) unsigned default '10',
  `solutionpagesize` smallint(2) unsigned default '15',
  `solutionnum` smallint(2) unsigned default '10',
  `solutionlen` smallint(2) unsigned default '10',
  `eliteproductnum` smallint(2) unsigned default '15',
  `eliteproductlen` smallint(2) unsigned default '10',
  `qqstatus` smallint(2) unsigned default '0',
  `cachstatus` smallint(2) unsigned default '0',
  `cachtime` mediumint(8) unsigned default '0',
  `tagrange` smallint(2) unsigned default '0',
  `tagurlnum` smallint(2) unsigned default '0',
  `downsearchtop` text,
  `downsearchbottom` text,
  `nocontent` text,
  `adstatus` smallint(2) unsigned default '0',
  `adsyspic` varchar(255) default NULL,
  `adlink` varchar(200) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ljcms_delimitlabel`;

CREATE TABLE IF NOT EXISTS `ljcms_delimitlabel` (
  `labelid` mediumint(8) unsigned NOT NULL default '0',
  `skinid` mediumint(8) unsigned default '0',
  `labeltitle` varchar(255) default NULL,
  `labelname` varchar(255) default NULL,
  `labelcontent` text,
  `flag` smallint(2) unsigned default '0',
  `timeline` int(10) unsigned default '0',
  `intro` varchar(500) default NULL,
  PRIMARY KEY  (`labelid`),
  KEY `skinid` (`skinid`),
  KEY `flag` (`flag`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ljcms_download`;

CREATE TABLE IF NOT EXISTS `ljcms_download` (
  `downid` mediumint(8) unsigned NOT NULL default '0',
  `title` varchar(255) default NULL,
  `metatitle` varchar(255) default NULL,
  `metakeyword` varchar(255) default NULL,
  `metadescription` varchar(255) default NULL,
  `uploadfiles` varchar(255) default NULL,
  `filesize` varchar(50) default NULL,
  `intro` text,
  `content` text,
  `cateid` mediumint(8) unsigned default '0',
  `hits` int(10) unsigned default '0',
  `downs` int(10) unsigned default '0',
  `elite` smallint(2) unsigned default '0',
  `hots` smallint(2) unsigned default '0',
  `flag` smallint(2) unsigned default '0',
  `ugroupid` varchar(100) NOT NULL,
  `exclusive` varchar(20) NOT NULL,
  `dateline` int(10) unsigned default '0',
  `timeline` int(10) unsigned default '0',
  `deleted` smallint(2) unsigned default '0',
  `tag` varchar(255) default NULL,
  `barcode` text COMMENT '条形码',
  PRIMARY KEY  (`downid`),
  KEY `cateid` (`cateid`),
  KEY `flag` (`flag`),
  KEY `deleted` (`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ljcms_downloadcate`;

CREATE TABLE IF NOT EXISTS `ljcms_downloadcate` (
  `cateid` mediumint(8) unsigned NOT NULL default '0',
  `catename` varchar(255) default NULL,
  `metatitle` varchar(255) default NULL,
  `metakeyword` varchar(255) default NULL,
  `metadescription` varchar(255) default NULL,
  `pathname` varchar(50) default NULL,
  `parentid` mediumint(8) unsigned default '0',
  `depth` mediumint(8) unsigned default '0',
  `orders` mediumint(8) unsigned default '0',
  `flag` smallint(2) unsigned default '0',
  `intro` varchar(500) default NULL,
  `timeline` int(10) unsigned default '0',
  `elite` smallint(2) unsigned default '0',
  `cssname` varchar(50) default NULL,
  `img` varchar(255) default NULL,
  `linktype` smallint(2) unsigned default '1',
  `linkurl` varchar(255) default NULL,
  `target` smallint(2) unsigned default '1',
  PRIMARY KEY  (`cateid`),
  KEY `parentid` (`parentid`),
  KEY `orders` (`orders`),
  KEY `flag` (`flag`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ljcms_guestbook`;

CREATE TABLE IF NOT EXISTS `ljcms_guestbook` (
  `bookid` int(10) unsigned NOT NULL default '0',
  `title` varchar(255) default NULL,
  `bookuser` varchar(255) default NULL,
  `gender` smallint(2) unsigned default '0',
  `jobs` varchar(50) default NULL,
  `telephone` varchar(255) default NULL,
  `fax` varchar(255) default NULL,
  `mobile` varchar(255) default NULL,
  `email` varchar(255) default NULL,
  `qqmsn` varchar(255) default NULL,
  `companyname` varchar(255) default NULL,
  `address` varchar(255) default NULL,
  `trade` varchar(255) default NULL,
  `homepage` varchar(255) default NULL,
  `content` text,
  `booktimeline` int(10) unsigned default '0',
  `ip` varchar(50) default NULL,
  `userid` int(4) NOT NULL,
  `flag` smallint(2) unsigned default '0',
  `replyuser` varchar(50) default NULL,
  `replytimeline` int(10) unsigned default '0',
  `replycontent` text,
  PRIMARY KEY  (`bookid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ljcms_info`;

CREATE TABLE IF NOT EXISTS `ljcms_info` (
  `infoid` mediumint(8) unsigned NOT NULL default '0',
  `cateid` mediumint(8) unsigned default '0',
  `title` varchar(255) default NULL,
  `thumbfiles` varchar(255) default NULL,
  `uploadfiles` varchar(255) default NULL,
  `summary` text,
  `content` text,
  `timeline` int(10) unsigned default '0',
  `elite` smallint(2) unsigned default '0',
  `flag` smallint(2) unsigned default '0',
  `ugroupid` varchar(100) NOT NULL,
  `exclusive` varchar(20) NOT NULL,
  `hits` int(10) unsigned default '0',
  `orders` mediumint(8) unsigned default '0',
  `metatitle` varchar(255) default NULL,
  `metakeyword` varchar(255) default NULL,
  `metadescription` varchar(255) default NULL,
  `delimitname` varchar(255) default NULL,
  `tag` varchar(255) default NULL,
  PRIMARY KEY  (`infoid`),
  KEY `cateid` (`cateid`),
  KEY `flag` (`flag`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ljcms_infocate`;

CREATE TABLE IF NOT EXISTS `ljcms_infocate` (
  `cateid` mediumint(8) unsigned NOT NULL default '0',
  `catename` varchar(255) default NULL,
  `metatitle` varchar(255) default NULL,
  `metakeyword` varchar(255) default NULL,
  `metadescription` varchar(255) default NULL,
  `pathname` varchar(50) default NULL,
  `parentid` mediumint(8) unsigned default '0',
  `depth` mediumint(8) unsigned default '0',
  `orders` mediumint(8) unsigned default '0',
  `flag` smallint(2) unsigned default '0',
  `intro` varchar(500) default NULL,
  `timeline` int(10) unsigned default '0',
  `elite` smallint(2) unsigned default '0',
  `cssname` varchar(50) default NULL,
  `img` varchar(255) default NULL,
  `linktype` smallint(2) unsigned default '1',
  `linkurl` varchar(255) default NULL,
  `target` smallint(2) unsigned default '1',
  PRIMARY KEY  (`cateid`),
  KEY `parentid` (`parentid`),
  KEY `orders` (`orders`),
  KEY `flag` (`flag`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ljcms_job`;

CREATE TABLE IF NOT EXISTS `ljcms_job` (
  `jobid` mediumint(8) unsigned NOT NULL default '0',
  `cateid` mediumint(8) unsigned default '0',
  `title` varchar(255) default NULL,
  `workarea` varchar(50) default NULL,
  `number` smallint(2) unsigned default '0',
  `jobdescription` text,
  `jobrequest` text,
  `jobotherrequest` text,
  `jobcontact` text,
  `timeline` int(10) unsigned default '0',
  `flag` smallint(2) unsigned default '0',
  `hits` int(10) unsigned default '0',
  PRIMARY KEY  (`jobid`),
  KEY `cateid` (`cateid`),
  KEY `flag` (`flag`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ljcms_jobcate`;

CREATE TABLE IF NOT EXISTS `ljcms_jobcate` (
  `cateid` mediumint(8) unsigned NOT NULL default '0',
  `catename` varchar(255) default NULL,
  `metatitle` varchar(255) default NULL,
  `metakeyword` varchar(255) default NULL,
  `metadescription` varchar(255) default NULL,
  `pathname` varchar(50) default NULL,
  `parentid` mediumint(8) unsigned default '0',
  `depth` mediumint(8) unsigned default '0',
  `orders` smallint(2) unsigned default '0',
  `flag` smallint(2) unsigned default '0',
  `intro` varchar(500) default NULL,
  `timeline` int(10) unsigned default '0',
  `elite` smallint(2) unsigned default '0',
  `cssname` varchar(255) default NULL,
  `img` varchar(255) default NULL,
  `linktype` smallint(2) unsigned default '1',
  `linkurl` varchar(255) default NULL,
  `target` smallint(2) unsigned default '1',
  PRIMARY KEY  (`cateid`),
  KEY `parentid` (`parentid`),
  KEY `orders` (`orders`),
  KEY `flag` (`flag`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ljcms_link`;

CREATE TABLE IF NOT EXISTS `ljcms_link` (
  `linkid` mediumint(8) unsigned NOT NULL default '0',
  `linktitle` varchar(255) default NULL,
  `fontcolor` varchar(50) default NULL,
  `linkurl` varchar(255) default NULL,
  `linktype` smallint(2) unsigned default '0',
  `logoimg` varchar(255) default NULL,
  `timeline` int(10) unsigned default '0',
  `flag` smallint(2) unsigned default '0',
  `intro` varchar(255) default NULL,
  `orders` smallint(2) unsigned default '0',
  PRIMARY KEY  (`linkid`),
  KEY `flag` (`flag`),
  KEY `linktype` (`linktype`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ljcms_log`;

CREATE TABLE IF NOT EXISTS `ljcms_log` (
  `logid` int(10) unsigned NOT NULL default '0',
  `username` varchar(50) default NULL,
  `ip` varchar(50) default NULL,
  `content` varchar(255) default NULL,
  `logtype` smallint(2) unsigned default '0',
  `timeline` int(10) unsigned default '0',
  PRIMARY KEY  (`logid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ljcms_onlinechat`;

CREATE TABLE IF NOT EXISTS `ljcms_onlinechat` (
  `onid` mediumint(8) unsigned NOT NULL default '0',
  `ontype` smallint(2) unsigned default '0',
  `title` varchar(255) default NULL,
  `number` varchar(255) default NULL,
  `sitetitle` varchar(255) default NULL,
  `orders` mediumint(8) unsigned default '0',
  `flag` smallint(2) unsigned default '0',
  `timeline` int(10) unsigned default '0',
  PRIMARY KEY  (`onid`),
  KEY `flag` (`flag`),
  KEY `orders` (`orders`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ljcms_order`;

CREATE TABLE IF NOT EXISTS `ljcms_order` (
  `id` int(10) unsigned NOT NULL default '0',
  `ordername` varchar(255) default NULL,
  `remark` varchar(255) default NULL,
  `userid` int(6) unsigned default '0',
  `username` varchar(255) default NULL,
  `sex` varchar(50) default NULL,
  `company` varchar(50) default NULL,
  `address` varchar(255) default NULL,
  `zipcode` varchar(255) default NULL,
  `telephone` varchar(255) default NULL,
  `fax` varchar(255) default NULL,
  `mobile` varchar(255) default NULL,
  `email` varchar(255) default NULL,
  `addtime` int(11) NOT NULL default '0',
  `flag` smallint(2) unsigned NOT NULL,
  `ip` varchar(100) NOT NULL,
  `replyuser` varchar(500) NOT NULL,
  `replycontent` text,
  `replytime` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ljcms_page`;

CREATE TABLE IF NOT EXISTS `ljcms_page` (
  `pageid` mediumint(8) unsigned NOT NULL default '0',
  `cateid` mediumint(8) unsigned default '0',
  `linktype` smallint(2) unsigned default '1',
  `linkurl` varchar(500) default NULL,
  `target` smallint(2) unsigned default '1',
  `title` varchar(255) default NULL,
  `content` text,
  `flag` smallint(2) unsigned default '0',
  `navshow` smallint(2) unsigned default '0',
  `orders` mediumint(8) unsigned default '0',
  `timeline` int(10) unsigned default '0',
  `metatitle` varchar(255) default NULL,
  `metakeyword` varchar(255) default NULL,
  `metadescription` varchar(255) default NULL,
  `delimitname` varchar(255) default NULL,
  `tag` varchar(255) default NULL,
  PRIMARY KEY  (`pageid`),
  KEY `cateid` (`cateid`),
  KEY `flag` (`flag`),
  KEY `showstatus` (`navshow`),
  KEY `orders` (`orders`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ljcms_pagecate`;

CREATE TABLE IF NOT EXISTS `ljcms_pagecate` (
  `cateid` mediumint(8) unsigned NOT NULL default '0',
  `catename` varchar(255) default NULL,
  `metatitle` varchar(255) default NULL,
  `metakeyword` varchar(255) default NULL,
  `metadescription` varchar(255) default NULL,
  `pathname` varchar(50) default NULL,
  `parentid` mediumint(8) unsigned default '0',
  `depth` smallint(2) unsigned default '0',
  `orders` smallint(2) unsigned default '0',
  `flag` smallint(2) unsigned default '0',
  `intro` varchar(500) default NULL,
  `timeline` int(10) unsigned default '0',
  `elite` smallint(2) unsigned default '0',
  `cssname` varchar(50) default NULL,
  `img` varchar(255) default NULL,
  `linktype` smallint(2) unsigned default '1',
  `linkurl` varchar(255) default NULL,
  `target` smallint(2) unsigned default '1',
  PRIMARY KEY  (`cateid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ljcms_product`;

CREATE TABLE IF NOT EXISTS `ljcms_product` (
  `productid` mediumint(8) unsigned NOT NULL default '0',
  `cateid` mediumint(8) unsigned default '0',
  `productnum` varchar(50) default NULL,
  `productname` varchar(255) default NULL,
  `metatitle` varchar(255) default NULL,
  `metakeyword` varchar(255) default NULL,
  `metadescription` varchar(255) default NULL,
  `delimitname` varchar(255) default NULL,
  `thumbfiles` varchar(255) default NULL,
  `uploadfiles` varchar(255) default NULL,
  `img1` varchar(255) default NULL,
  `img2` varchar(255) default NULL,
  `img3` varchar(255) default NULL,
  `img4` varchar(255) default NULL,
  `img5` varchar(255) default NULL,
  `img6` varchar(255) default NULL,
  `img7` varchar(255) default NULL,
  `img8` varchar(255) default NULL,
  `intro` text,
  `content` text,
  `price` decimal(18,2) unsigned default '0.00',
  `nprice` decimal(18,2) unsigned NOT NULL default '0.00',
  `ugroupid` varchar(100) NOT NULL,
  `exclusive` varchar(20) NOT NULL,
  `hits` int(10) unsigned default '0',
  `elite` smallint(2) unsigned default '0',
  `isnew` smallint(2) unsigned default '0',
  `hots` smallint(2) unsigned default '0',
  `flag` smallint(2) unsigned default '0',
  `ismiao` smallint(2) unsigned default '0',
  `miaotime` varchar(100) default NULL,
  `timeline` int(10) unsigned default '0',
  `deleted` smallint(2) unsigned default '0',
  `tag` varchar(255) default NULL,
  PRIMARY KEY  (`productid`),
  KEY `cateid` (`cateid`),
  KEY `flag` (`flag`),
  KEY `deleted` (`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ljcms_productcate`;

CREATE TABLE IF NOT EXISTS `ljcms_productcate` (
  `cateid` mediumint(8) unsigned NOT NULL default '0',
  `catename` varchar(255) default NULL,
  `metatitle` varchar(255) default NULL,
  `metakeyword` varchar(255) default NULL,
  `metadescription` varchar(255) default NULL,
  `pathname` varchar(50) default NULL,
  `parentid` mediumint(8) unsigned default '0',
  `depth` mediumint(8) unsigned default '0',
  `orders` smallint(2) unsigned default '0',
  `flag` smallint(2) unsigned default '0',
  `elite` smallint(2) unsigned default '0',
  `intro` varchar(500) default NULL,
  `timeline` int(10) unsigned default '0',
  `cssname` varchar(50) default NULL,
  `img` varchar(255) default NULL,
  `linktype` smallint(2) unsigned default '1',
  `linkurl` varchar(255) default NULL,
  `target` smallint(2) unsigned default '1',
  PRIMARY KEY  (`cateid`),
  KEY `parentid` (`parentid`),
  KEY `orders` (`orders`),
  KEY `flag` (`flag`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ljcms_skin`;

CREATE TABLE IF NOT EXISTS `ljcms_skin` (
  `skinid` mediumint(8) unsigned NOT NULL default '0',
  `skinname` varchar(255) default NULL,
  `skindir` varchar(50) default NULL,
  `skinext` varchar(50) default NULL,
  `thumbfiles` varchar(255) default NULL,
  `orders` mediumint(8) unsigned default '0',
  `flag` smallint(2) unsigned default '0',
  `timeline` int(10) unsigned default '0',
  `remark` varchar(500) default NULL,
  PRIMARY KEY  (`skinid`),
  KEY `flag` (`flag`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ljcms_solution`;

CREATE TABLE IF NOT EXISTS `ljcms_solution` (
  `solutionid` mediumint(8) unsigned NOT NULL default '0',
  `cateid` mediumint(8) unsigned default '0',
  `title` varchar(255) default NULL,
  `thumbfiles` varchar(255) default NULL,
  `uploadfiles` varchar(255) default NULL,
  `summary` text,
  `content` text,
  `timeline` int(10) unsigned default '0',
  `author` varchar(50) default NULL,
  `cfrom` varchar(50) default NULL,
  `flag` smallint(2) unsigned default '0',
  `ugroupid` varchar(100) NOT NULL,
  `exclusive` varchar(20) NOT NULL,
  `istop` smallint(2) unsigned default '0',
  `elite` smallint(2) unsigned default '0',
  `headline` smallint(2) unsigned default '0',
  `slide` smallint(2) unsigned default '0',
  `hits` int(10) unsigned default '0',
  `metatitle` varchar(255) default NULL,
  `metakeyword` varchar(255) default NULL,
  `metadescription` varchar(255) default NULL,
  `delimitname` varchar(255) default NULL,
  `deleted` smallint(2) unsigned default '0',
  `tag` varchar(255) default NULL,
  PRIMARY KEY  (`solutionid`),
  KEY `cateid` (`cateid`),
  KEY `flag` (`flag`),
  KEY `deleted` (`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ljcms_solutioncate`;

CREATE TABLE IF NOT EXISTS `ljcms_solutioncate` (
  `cateid` mediumint(8) unsigned NOT NULL default '0',
  `catename` varchar(255) default NULL,
  `metatitle` varchar(255) default NULL,
  `metakeyword` varchar(255) default NULL,
  `metadescription` varchar(255) default NULL,
  `pathname` varchar(50) default NULL,
  `parentid` mediumint(8) unsigned default '0',
  `depth` mediumint(8) unsigned default '0',
  `orders` smallint(2) unsigned default '0',
  `flag` smallint(2) unsigned default '0',
  `intro` varchar(500) default NULL,
  `timeline` int(10) unsigned default '0',
  `elite` smallint(2) unsigned default '0',
  `cssname` varchar(50) default NULL,
  `img` varchar(255) default NULL,
  `linktype` smallint(2) unsigned default '1',
  `linkurl` varchar(255) default NULL,
  `target` smallint(2) unsigned default '1',
  PRIMARY KEY  (`cateid`),
  KEY `parentid` (`parentid`),
  KEY `flag` (`flag`),
  KEY `orders` (`orders`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ljcms_tag`;

CREATE TABLE IF NOT EXISTS `ljcms_tag` (
  `tagid` mediumint(8) unsigned NOT NULL default '0',
  `tag` varchar(255) default NULL,
  `channel` varchar(50) default NULL,
  `orders` smallint(2) unsigned default '0',
  `url` varchar(500) default NULL,
  `flag` smallint(2) unsigned default '0',
  `timeline` int(10) unsigned default '0',
  `target` smallint(2) unsigned default '0',
  `color` varchar(10) default NULL,
  `strong` smallint(2) unsigned default '0',
  PRIMARY KEY  (`tagid`),
  KEY `flag` (`flag`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ljcms_user`;

CREATE TABLE IF NOT EXISTS `ljcms_user` (
  `userid` int(11) NOT NULL auto_increment,
  `loginname` varchar(30) NOT NULL default '',
  `salt` varchar(32) NOT NULL default '',
  `password` varchar(40) NOT NULL default '',
  `postnums` int(11) NOT NULL default '0',
  `realname` varchar(30) default '',
  `explam` text,
  `url` varchar(200) default '',
  `email` varchar(200) default '',
  `regdate` int(11) NOT NULL default '0',
  `stopdate` int(11) NOT NULL default '0',
  `usergroupid` int(11) NOT NULL default '0',
  `pointnum` int(11) NOT NULL default '0',
  `flag` smallint(2) NOT NULL default '0',
  `nicename` varchar(30) default '',
  `address` varchar(200) default '',
  `headimg` varchar(200) default '',
  `brothday` varchar(10) default '',
  `sex` int(11) NOT NULL default '0',
  `im` varchar(200) default '',
  `telno` varchar(100) default '',
  `lastlogindate` int(11) NOT NULL default '0',
  `lastloginip` varchar(100) default '',
  `forgetpwd` varchar(200) default '',
  `memo` text,
  PRIMARY KEY  (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

DROP TABLE IF EXISTS `ljcms_usergroup`;

CREATE TABLE IF NOT EXISTS `ljcms_usergroup` (
  `usergroupid` mediumint(8) unsigned NOT NULL default '0',
  `grupname` varchar(255) default NULL,
  `level` varchar(50) NOT NULL default '0',
  `menu` text,
  `gpurview` int(4) NOT NULL default '0',
  `flag` smallint(2) NOT NULL,
  `addtime` varchar(100) default NULL,
  `intro` varchar(255) default NULL,
  PRIMARY KEY  (`usergroupid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `ljcms_category`;

CREATE TABLE IF NOT EXISTS `ljcms_category` (
  `catid` mediumint(8) unsigned NOT NULL default '0',
  `modalias` varchar(50) default NULL,
  `catname` varchar(100) default NULL,
  `asname` varchar(100) default NULL,
  `treeid` mediumint(8) unsigned default '0',
  `rootid` mediumint(8) unsigned default '0',
  `depth` mediumint(8) unsigned default '0',
  `childs` varchar(255) default NULL,
  `flag` tinyint(1) unsigned default '0',
  `orders` mediumint(8) unsigned default '0',
  `dirname` varchar(100) default NULL,
  `dirpath` varchar(200) default NULL,
  `catpic` varchar(255) default NULL,
  `intro` varchar(500) default NULL,
  `metatitle` varchar(255) default NULL,
  `metakeyword` varchar(255) default NULL,
  `metadescription` varchar(255) default NULL,
  `tplindex` varchar(255) default NULL,
  `tpllist` varchar(255) default NULL,
  `tpldetail` varchar(255) default NULL,
  `ismenu` tinyint(1) unsigned default '0',
  `isaccessory` tinyint(1) unsigned default '0',
  `showpart` tinyint(1) unsigned default '0',
  `orderby` tinyint(1) unsigned default '0',
  `pagemax` smallint(1) unsigned default '0',
  `linktype` tinyint(1) unsigned default '1',
  `outurl` varchar(255) default NULL,
  `purview` mediumint(8) unsigned default '0',
  `issystem` tinyint(1) unsigned default '0',
  `relid` int(10) unsigned default '0',
  PRIMARY KEY  (`catid`),
  KEY `rootid` (`rootid`),
  KEY `dirname` (`dirname`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ljcms_module`;

CREATE TABLE IF NOT EXISTS `ljcms_module` (
  `modid` smallint(2) unsigned NOT NULL,
  `modname` varchar(50) default NULL,
  `alias` varchar(50) NOT NULL default '',
  `color` varchar(20) default NULL,
  `tplindex` varchar(100) default NULL,
  `tpllist` varchar(100) default NULL,
  `tpldetail` varchar(100) default NULL,
  `posts` mediumint(8) unsigned default '0',
  `comments` mediumint(8) unsigned default '0',
  `pv` int(10) unsigned default '0',
  `sort` tinyint(2) unsigned default '0',
  `enabled` tinyint(1) unsigned default '1',
  `intro` varchar(255) default NULL,
  PRIMARY KEY  (`modid`),
  UNIQUE KEY `modelid` (`modid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;




INSERT INTO `ljcms_adsfigure` VALUES(4, 's', 3, 'data/attachment/201202/12/7b1081c81c7bb36ee68ef27d3d05fc1e.jpg', '', 0, 0, 4, 1, '', 1317031391);

INSERT INTO `ljcms_adsfigure` VALUES(3, 's', 2, 'data/attachment/201202/12/5251b2586a5df2d70669b8bf9eabd8a4.jpg', '', 0, 0, 3, 1, '', 1317031377);

INSERT INTO `ljcms_adsfigure` VALUES(2, '2', 1, 'data/attachment/201301/09/27fc6d9f69445848ee42f580eec4ba46.png', 'productbuy.php?proid=15', 0, 0, 1, 1, '', 1317031316);

INSERT INTO `ljcms_adsfigure` VALUES(1, '1', 1, 'data/attachment/201301/09/dfda0693e30b7d6ccef90c0b6f5439a1.png', 'productbuy.php?proid=15', 0, 0, 1, 1, '', 1317031301);

INSERT INTO `ljcms_adsfigure` VALUES(5, 'tttt', 1, 'data/attachment/201301/09/d08f1590405f30b7be7ddfd8f3e79037.png', 'productbuy.php?proid=3', 0, 0, 5, 1, '', 1337760294);

INSERT INTO `ljcms_adsfigure` VALUES(6, '22', 1, 'data/attachment/201301/09/26be765feba886097a13f4f14b8670c1.png', 'productbuy.php?proid=3', 0, 0, 6, 1, '', 1337760311);

INSERT INTO `ljcms_adsfigure` VALUES(7, 'q', 1, 'data/attachment/201301/09/b57a0f75ffdf919f080da50b19cd1cbc.png', 'productbuy.php?proid=3', 0, 0, 7, 1, 'q', 1337760330);

INSERT INTO `ljcms_adszone` VALUES(1, '首页_通栏广告', '', 0, 1, 1, '', 990, 200, 1, 0, 1314244583);

INSERT INTO `ljcms_adszone` VALUES(2, '首页_广告一', '', 0, 2, 1, '', 160, 120, 0, 0, 1314244778);

INSERT INTO `ljcms_adszone` VALUES(3, '频道_广告图一', '', 0, 3, 1, '', 990, 200, 0, 0, 1314682240);

INSERT INTO `ljcms_applyjob` VALUES(1, 'sdfdsf1111111', 1, 1, 0, '2008-10-11', '中国', '010-66884112', 'sdfsdfs@126.com', '大专', '计算机', '北京大学', '北京', 'sdfsfsdfs', 'fsdfsdfsd', 'sdfsdfsdfsd', 1, '127.0.0.1', '1341295834', '');

INSERT INTO `ljcms_applyjob` VALUES(4, 'itf4', 1, 6, 0, '1980', '北京', '81991660', 'asp3721@hotmail.com', '', '', '', '', '', '', '', 1, '192.168.1.104', '1343297294', '');

INSERT INTO `ljcms_applyjob` VALUES(3, 'adminsa', 1, 1, 0, '2008-10-11', '中国', '010-66884112', 'sdfsdfs@126.com', '大专', '计算机', '北京大学', '北京', '停停停停停停停停停停停停', '停停停停停停停停停停停停停', '钱钱钱钱钱钱钱钱钱钱钱钱钱钱钱钱钱钱', 1, '127.0.0.1', '1341364191', '');

INSERT INTO `ljcms_authgroup` VALUES(1, '信息录入', '', 1, 1313898288, 1, '主要给信息录入员录入数据');

INSERT INTO `ljcms_case` VALUES(7, '客户成功案例07', '', '', '', '', 'data/attachment/201202/12/ef811330877649b5906eaa310e808589.jpg.thumb.jpg', 'data/attachment/201202/12/ef811330877649b5906eaa310e808589.jpg', '', '<p>客户成功案例07</p>\r\n<p>客户成功案例07</p>\r\n<p>客户成功案例07</p>\r\n<p>客户成功案例07</p>\r\n<p>客户成功案例07</p>\r\n<p>客户成功案例07</p>\r\n<p>客户成功案例07</p>\r\n<p>客户成功案例07</p>', 1, 1, 0, 0, 0, 1, '6868668868', '>=', 1329042914, 0, '');

INSERT INTO `ljcms_case` VALUES(8, '客户成功案例08', '', '', '', '', 'data/attachment/201202/12/9cf6761f61b23181dea0ef91a9b6c361.jpg.thumb.jpg', 'data/attachment/201202/12/9cf6761f61b23181dea0ef91a9b6c361.jpg', '', '<p>客户成功案例08</p>\r\n<p>客户成功案例08</p>\r\n<p>客户成功案例08</p>\r\n<p>客户成功案例08</p>\r\n<p>客户成功案例08</p>\r\n<p>客户成功案例08</p>\r\n<p>客户成功案例08</p>\r\n<p>客户成功案例08</p>', 1, 9, 0, 0, 0, 1, '6868668868', '>=', 1329042955, 0, '');

INSERT INTO `ljcms_case` VALUES(9, '客户成功案例09', '', '', '', '', 'data/attachment/201202/12/a47e58f18b226191994cf816de4a7e57.jpg.thumb.jpg', 'data/attachment/201202/12/a47e58f18b226191994cf816de4a7e57.jpg', '', '<p>客户成功案例09</p>\r\n<p>客户成功案例09</p>\r\n<p>客户成功案例09</p>\r\n<p>客户成功案例09</p>\r\n<p>客户成功案例09</p>\r\n<p>客户成功案例09</p>\r\n<p>客户成功案例09</p>\r\n<p>客户成功案例09</p>', 1, 43, 0, 0, 0, 1, '6868668868', '>=', 1329042988, 0, '');

INSERT INTO `ljcms_case` VALUES(10, '客户成功案例10', '客户成功案例10', '客户成功案例10', '客户成功案例10', '', 'data/attachment/201202/12/0d9da185208b6dd83706599a61c34b62.jpg.thumb.jpg', 'data/attachment/201202/12/0d9da185208b6dd83706599a61c34b62.jpg', '客户成功案例10', '<p>客户成功案例10</p>\r\n<p>客户成功案例10</p>\r\n<p>客户成功案例10</p>\r\n<p>客户成功案例10</p>\r\n<p>客户成功案例10</p>\r\n<p>客户成功案例10</p>\r\n<p>客户成功案例10</p>\r\n<p>客户成功案例10</p>', 1, 30, 0, 0, 0, 1, '6868668868', '>=', 1329043019, 0, '');

INSERT INTO `ljcms_casecate` VALUES(2, '商城网店类型', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 0, 0, 2, 1, '', 1329038411, 0, '', '', 1, '', 1);

INSERT INTO `ljcms_casecate` VALUES(3, '政府医院类型', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 0, 0, 3, 1, '', 1329038448, 0, '', '', 1, '', 1);

INSERT INTO `ljcms_casecate` VALUES(1, '企业公司类型', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 0, 0, 1, 1, '', 1329038396, 0, '', '', 1, '', 1);

INSERT INTO `ljcms_casecate` VALUES(4, '团购建站类型', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 0, 0, 4, 1, '', 1329038462, 0, '', '', 1, '', 1);

INSERT INTO `ljcms_casecate` VALUES(5, '行业教育类型', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 0, 0, 5, 1, '', 1329038508, 0, '', '', 1, '', 1);

INSERT INTO `ljcms_category` VALUES(1, 'article', '资讯中心', '', 0, 0, 0, NULL, 1, 2, 'news', NULL, '', NULL, '', '', '', '', '0', '0', 1, 1, 0, 0, 0, 1, 'info.php', 0, 0, 0);

INSERT INTO `ljcms_category` VALUES(2, 'product', '产品中心', '', 0, 0, 0, NULL, 1, 3, 'product', NULL, '', NULL, '', '', '', '', '0', '0', 1, 1, 0, 0, 0, 1, 'product.php', 0, 0, 0);

INSERT INTO `ljcms_category` VALUES(3, 'photo', '成功案例', '', 0, 0, 0, NULL, 1, 4, 'photo', NULL, '', NULL, '', '', '', '', '0', '0', 1, 1, 0, 0, 0, 1, 'case.php', 0, 0, 0);

INSERT INTO `ljcms_category` VALUES(4, 'download', '下载中心', '', 0, 0, 0, NULL, 1, 5, 'download', NULL, '', NULL, '', '', '', '', '0', '0', 1, 0, 0, 0, 0, 1, 'download.php', 0, 0, 0);

INSERT INTO `ljcms_category` VALUES(5, 'hr', '招聘中心', '', 0, 0, 0, NULL, 1, 6, 'job', NULL, '', NULL, '', '', '', '', '0', '0', 0, 0, 0, 0, 0, 1, '', 0, 0, 0);

INSERT INTO `ljcms_category` VALUES(63, 'article', '公司简介', '', 62, 62, 1, NULL, 1, 1, 'Aboutus', NULL, '', NULL, '', '', '', '', '0', '0', 1, 0, 0, 0, 0, 1, 'page.php?mod=detail&id=1', 0, 0, 0);

INSERT INTO `ljcms_category` VALUES(7, 'outside', '在线留言', '', 0, 0, 0, NULL, 1, 7, NULL, NULL, '', NULL, '', '', '', '', '0', '0', 1, 0, 0, 0, 0, 0, 'guestbook.php', 0, 0, 0);

INSERT INTO `ljcms_category` VALUES(8, 'outside', '网站首页', '', 0, 0, 0, NULL, 1, 0, NULL, NULL, '', NULL, '', '', '', '', '0', '0', 1, 0, 0, 0, 0, 0, 'index.php', 0, 0, 0);

INSERT INTO `ljcms_category` VALUES(9, 'outside', '会员中心', NULL, 0, 0, 0, NULL, 1, 8, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 2, '{$urlpath}usercp.php', 0, 0, 0);

INSERT INTO `ljcms_category` VALUES(47, 'article', '公司新闻', '', 1, 1, 1, NULL, 1, 2, 'gonggao', NULL, '', NULL, '', '', '', '', '0', '0', 1, 0, 0, 0, 0, 1, 'info.php?mod=list&cid=1', 0, 0, 0);

INSERT INTO `ljcms_category` VALUES(48, 'article', '财经资讯', '', 1, 1, 1, NULL, 1, 2, 'hangye', NULL, '', NULL, '', '', '', '', '0', '0', 1, 0, 0, 0, 0, 1, 'info.php?mod=list&cid=5', 0, 0, 0);

INSERT INTO `ljcms_category` VALUES(49, 'article', '军事新闻', '', 1, 1, 1, NULL, 1, 2, 'guonei', NULL, '', NULL, '', '', '', '', '0', '0', 1, 0, 0, 0, 0, 1, 'info.php?mod=list&cid=3', 0, 0, 0);

INSERT INTO `ljcms_category` VALUES(55, 'product', '网站源码', '', 2, 2, 1, NULL, 1, 3, 'computer', NULL, '', NULL, '', '', '', '', '0', '0', 1, 0, 0, 0, 0, 1, 'product.php?mod=list&cid=4', 0, 0, 0);

INSERT INTO `ljcms_category` VALUES(56, 'product', '电脑配件', '', 2, 2, 1, NULL, 1, 3, 'clothing', NULL, '', NULL, '', '', '', '', '0', '0', 1, 0, 0, 0, 0, 1, 'product.php?mod=list&cid=3', 0, 0, 0);

INSERT INTO `ljcms_category` VALUES(62, 'article', '关于我们', '', 0, 0, 0, NULL, 1, 1, 'A', NULL, '', NULL, '', '', '', '', '0', '0', 1, 0, 0, 0, 0, 1, 'page.php?modabout', 0, 0, 0);

INSERT INTO `ljcms_category` VALUES(65, 'article', '科技新闻', '', 1, 1, 1, NULL, 1, 2, 'keji', NULL, '', NULL, '', '', '', '', '0', '0', 1, 0, 0, 0, 0, 1, 'info.php?mod=list&cid=6', 0, 0, 0);

INSERT INTO `ljcms_category` VALUES(39, 'photo', '企业公司类型', '', 3, 3, 1, NULL, 1, 4, 'hunsha', NULL, '', NULL, '', '', '', '', '0', '0', 1, 0, 0, 0, 0, 1, 'case.php?mod=list&cid=1', 0, 0, 0);

INSERT INTO `ljcms_category` VALUES(40, 'download', '网站源码', '', 4, 4, 1, NULL, 1, 5, 'ruanjian', NULL, '', NULL, '', '', '', '', '0', '0', 1, 0, 0, 0, 0, 1, 'download.php?mod=list&cid=1', 0, 0, 0);

INSERT INTO `ljcms_category` VALUES(46, 'photo', '商城网店类型', '', 3, 3, 1, NULL, 1, 4, 'weimei', NULL, '', NULL, '', '', '', '', '0', '0', 1, 0, 0, 0, 0, 1, 'case.php?mod=list&cid=2', 0, 0, 0);

INSERT INTO `ljcms_category` VALUES(64, 'article', '联系我们', '', 62, 62, 1, NULL, 1, 1, '1', NULL, '', NULL, '', '', '', '', '0', '0', 1, 0, 0, 0, 0, 1, 'page.php?mod=contact', 0, 0, 0);

INSERT INTO `ljcms_category` VALUES(44, 'hr', '家用电器', '', 5, 5, 1, NULL, 1, 3, 'scsw', NULL, '', NULL, '', '', '', '', '0', '0', 1, 0, 0, 0, 0, 1, 'product.php?mod=list&cid=2', 0, 0, 0);

INSERT INTO `ljcms_category` VALUES(54, 'product', '良精源码', '', 2, 2, 1, NULL, 1, 3, 'shuma', NULL, '', NULL, '', '', '', '', '0', '0', 1, 0, 0, 0, 0, 1, 'product.php?mod=list&cid=16', 0, 0, 0);

INSERT INTO `ljcms_category` VALUES(53, 'download', '常用软件', '', 4, 4, 1, NULL, 1, 5, 'gongju', NULL, '', NULL, '', '', '', '', '0', '0', 1, 0, 0, 0, 0, 1, 'download.php?mod=list&cid=2', 0, 0, 0);

INSERT INTO `ljcms_category` VALUES(61, 'article', '企业黄页', '', 0, 0, 0, NULL, 1, 9, '21321', NULL, '', NULL, '', '', '', '', '0', '0', 1, 0, 0, 0, 0, 2, 'http://yp.liangjing.org/', 0, 0, 0);

INSERT INTO `ljcms_category` VALUES(66, 'photo', '政府医院类型', '', 3, 3, 1, NULL, 1, 4, 'zhengfuyiyuan', NULL, '', NULL, '', '', '', '', '0', '0', 1, 0, 0, 0, 0, 1, 'case.php?mod=list&cid=3', 0, 0, 0);

INSERT INTO `ljcms_category` VALUES(67, 'photo', '团购建站类型', '', 3, 3, 1, NULL, 1, 4, 'tuangoujianzhan', NULL, '', NULL, '', '', '', '', '0', '0', 1, 0, 0, 0, 0, 1, 'case.php?mod=list&cid=4', 0, 0, 0);

INSERT INTO `ljcms_config` VALUES('良精企业网站系统', '站点SEO标题-良精PHP企业网站管理系统', 'http://www.liangjing.org/', 'meta描述,meta描述', 'meta关键meta关键', '<embed src="http://player.youku.com/player.php/sid/XNDU0NTE4Nzk2/v.swf" allowFullScreen="true" quality="high" width="330" height="240" align="middle" allowScriptAccess="always" type="application/x-shockwave-flash"></embed>', 'CopyRight 2006-2013&nbsp; 良精企业建站系统&nbsp;', '京ICP备08002262号', '<script src="http://tongji.liangjing.org/cf.asp?username=lj"></script>', '北京良精志诚科技有限责任公司 成立于1998年2月9日，专门从事于简单实用企业网站建设、企业应用软件开发、网页设计、网站托管(什么是网站托管见<a href="http://www.liangjing.org/zh/HTML/News_237.html),UI">http://www.liangjing.org/</a>设计等服务项目。北京良精志诚科技有限责任公司 拥有多名专业设计人员，均从事网页、软件、广告设计、平面设计工作多年，具有独到的设计意识和丰富的工作经验，能为您提供完善的服务、一流的设计和全面的解决方案。<br />\r\n&nbsp;<br />\r\n　　 根据国家工商局数据调查，2006年全国中小型企业已经超过2300多万家，目前以平均每年超过10万的数量继续诞生。良精系列建站程序，以特有的安全、稳定、高效、易用而快捷的优势，受到广大建站用户的青睐，已成为广大建站用户的最为欢迎的程序。<br />\r\n&nbsp;<br />\r\n　　 在我们的服务领域，我们的专业化程度、技术服务水平、工程质量以及先进的服务理念得到了客户的充分认可，这也使得我们能与客户保持长期友好的合作关系。同时，我们还与多家IT企业有着良好的伙伴关系，使我们在各方面得到更加完善的支持，从而更好地为客户提供服务。', '<p>业务邮箱:<a href="mailto:asp3721@hotmail.com">asp3721@hotmail.com</a></p>\r\n<p>业务 QQ：82993936</p>\r\n<p>Email:<a href="mailto:asp3721@hotmail.com">asp3721@hotmail.com</a></p>\r\n<p>服务热线: +86 010-81991660</p>', 'data/attachment/201301/09/3f6599fae5aff79d81c56f5b1abeb801.jpg', 290, 84, '', 0, 0, 0, '', 'php', 1, 580, 650, 145, 120, 145, 125, 145, 125, 145, 125, 1, '', 4, 15, 2, 16, 15, 10, 10, 15, 6, 12, 12, 10, 10, 12, 10, 10, 10, 10, 10, 10, 10, 10, 15, 10, 1, 0, 60, 0, 0, '通过输入主机编号获得更加精准的产品服务', '<p class="section_top_content_search_p">\r\n		<a id="auto-get-machineno" class="section_top_content_search_p_a autosn" href="javascript:void(0);">自动获取主机编号</a>\r\n		<i class="section_top_content_search_p_i">|</i>\r\n		<a class="section_top_content_search_p_a" href="/">如何查找主机编号？</a>\r\n		<i class="section_top_content_search_p_i">|</i>\r\n		<a class="section_top_content_search_p_a" href="/">使用工具自动安装驱动</a>\r\n		</p>', '暂无机器码信息', 1, 'data/attachment/201404/20/5547425dbf5e85f9b1783998616db833.jpg', 'http://www.baidu.com');

INSERT INTO `ljcms_delimitlabel` VALUES(3, 1, '底部链接导航', 'footerlink', '<a href="{$url_link}">{$lang_link}</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="{$url_guestbook}">{$lang_guestbook}</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="{$url_sitemap}">{$lang_sitemap}</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="{$url_job}">{$lang_job}</a>', 1, 1314604050, '');

INSERT INTO `ljcms_delimitlabel` VALUES(2, 1, '头部收藏', 'tiplink', '良精开源、免费企业网站系统，欢迎下载使用', 1, 1313935721, '');

INSERT INTO `ljcms_delimitlabel` VALUES(4, 1, '联系方式', 'tel', '<p style="padding-left:1em;color:#7a7a7a;"><strong>客服QQ</strong></p>\r\n<p style="padding-left:1em;">81993936</p>\r\n<p style="padding-left:1em;color:#7a7a7a;"><strong>手机</strong></p>\r\n<p style="padding-left:1em;">+86-010-81991660</p>', 1, 1314699408, '');

INSERT INTO `ljcms_delimitlabel` VALUES(5, 1, '联系我们标签', 'contact', '<p><strong>良精免费php企业网站系统</strong><br />\r\n<strong>地址</strong>：北京市海淀区中关村08号</p>\r\n<p>\r\n<strong>电话:&nbsp;&nbsp; </strong>010-81991660<br />\r\n<strong>Email</strong>：asp3721@hotmail.com<br />\r\n<strong>官方网站</strong>：<a href="http://www.liangjing.org" target="_blank">www.liangjing.org<br />\r\n</a></p>', 1, 1316770088, '');

INSERT INTO `ljcms_delimitlabel` VALUES(6, 4, '公告', 'notice', '良精通用网站管理系统V3.9 新增功能 兼容Firefox、Maxthon、TT等常用浏览器 公告可以切换成视频播放', 1, 1332914542, '网站公告');

INSERT INTO `ljcms_delimitlabel` VALUES(7, 4, '底部导航', 'footernvation', '<a href="{$url_link}">{$lang_link}</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="{$url_guestbook}">{$lang_guestbook}</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="{$url_member}">{$lang_menbercenter}</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="{$url_sitemap}">{$lang_sitemap}</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="{$url_job}">{$lang_job}</a>', 1, 1333957062, '良精v3.9底部导航');

INSERT INTO `ljcms_delimitlabel` VALUES(8, 5, '底部导航', 'footerlink', '<a href="{$url_link}">{$lang_link}</a>&nbsp;&nbsp;|&nbsp;&nbsp; <a href="{$url_guestbook}">{$lang_guestbook}</a>&nbsp;&nbsp;|&nbsp;&nbsp; <a href="{$url_sitemap}">{$lang_sitemap}</a>&nbsp;&nbsp;|&nbsp;&nbsp; <a href="{$url_member}">{$lang_menbercenter}</a>&nbsp;&nbsp;|&nbsp;&nbsp; <a href="{$url_job}">{$lang_job}</a>', 1, 1337911915, '底部导航');

INSERT INTO `ljcms_delimitlabel` VALUES(9, 6, '底部导航设置', 'footernavtion', '﻿ <div class="promise">\r\n<ul>\r\n<li class="pr1"><a href="/">7天退货保障</a></li>\r\n<li class="pr2"><a href="/">15天换货承诺</a></li>\r\n<li class="pr3"><a href="/">200元起在线支付包邮</a></li>\r\n<li class="pr4"><a href="/">365天品质保障服务</a></li>\r\n</ul>\r\n</div>\r\n<div id="footer">\r\n<div class="nav">\r\n<dl> <dt>购物指南</dt> <dd><a href="/">购物流程</a></dd> <dd><a href="/">订单状态</a></dd> <dd><a href="/">用户协议</a></dd> <dd><a href="/">购物常见问题</a></dd></dl> <dl> <dt>支付方式</dt> <dd><a href="/">货到付款</a></dd> <dd><a href="/">在线支付</a></dd> <dd><a href="/">正品验证</a></dd> <dd><a href="/">支付常见问题</a></dd></dl> <dl> <dt>配送方式</dt> <dd><a href="/">快递送货</a></dd> <dd><a href="/">售后常见问题</a></dd> <dd><a href="/">签收须知</a></dd> <dd><a href="/">退换货政策</a></dd></dl> <dl> <dt>售后服务</dt> <dd><a href="/">服务政策</a></dd> <dd><a href="/">申请返修</a></dd> <dd><a href="/">退换货流程</a></dd> <dd><a href="/">SN码查询</a></dd></dl> <dl> <dt>关于我们</dt> <dd><a href="/page.php?mod=about">公司简介</a></dd> <dd><a href="/job.php">公司招聘</a></dd> <dd><a href="/info.php?mod=list&amp;cid=1">公司新闻</a></dd> <dd><a href="/page.php?mod=contact">联系我们</a></dd></dl></div>\r\n<div class="service">\r\n<p>客服热线( 仅收市话费 )<br />\r\n周一至周日 9:00-21:00</p>\r\n<p><strong>13888888888</strong></p>\r\n</div>\r\n<a id="ol_service" href="/guestbook.php" target="_blank">在线留言</a> </div>', 1, 1357280632, '底部导航设置');

INSERT INTO `ljcms_download` VALUES(1, '小精灵Asp服务', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', 'http://www.liangjing.org/down/aspWebServer.rar', '', '', '<p><img border="0" src="http://liangjing.org/qiyejianzhan/uploadfile/20111222100228874.jpg" /></p>\r\n<p><br />\r\n1.将网站源码放置在www文件夹内</p>\r\n<p>2.是双击良精小精灵Asp服务器.exe)即可运行，绿色免安装</p>\r\n<p><br />\r\n测试和使用：</p>\r\n<p>www文件夹为网站主目录index.asp&nbsp; 为默认网站头文件 </p>\r\n<p>1.默认打开演示CMS系统<br />\r\n2.右击图标，则弹出系统菜单<br />\r\n3.选择系统菜单顶部的打开网站菜单进行测试</p>\r\n<p><br />\r\n官方网站 http://www.liangjing.org/<br />\r\n</p>', 2, 2, 0, 0, 0, 1, '', '', 1328976000, 1329042214, 0, '', '');

INSERT INTO `ljcms_download` VALUES(2, '良精微博 免费', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', 'http://www.liangjing.org/down/LJwb.rar', '', '', '<p><span style="font-family:Verdana;">良精微博网站管理系统</span></p>\r\n<p><span style="font-family:Verdana;">官方网址 <a href="http://www.liangjing.org/">http://www.liangjing.org/</a></span></p>\r\n<p><span style="font-family:Verdana;">演示地址 <a href="http://t.itf4.com/">http://t.itf4.com/</a></span></p>\r\n<p><span style="font-family:Verdana;">下载地址 <a href="http://www.liangjing.org/down/LJwb.rar">http://www.liangjing.org/down/LJwb.rar</a></span></p>\r\n<p><span style="font-family:Verdana;">分流下载 <br />\r\n&nbsp;站长站<a href="http://down.chinaz.com/soft/27336.htm">http://down.chinaz.com/soft/27336.htm</a></span><span style="font-family:Verdana;"><br />\r\n&nbsp;源码之家<a href="http://www.mycodes.net/18/1797.htm">http://www.mycodes.net/18/1797.htm</a><br />\r\n&nbsp;admin5&nbsp; <a href="http://down.admin5.com/code_asp/60728.html">http://down.admin5.com/code_asp/60728.html</a></span></p>\r\n<p><span style="font-family:Verdana;">良精微博asp源码描述<br />\r\n良精微博源码系统具有了微博系统中所具备的8%以上的功能适合于中型微博用户</span></p>\r\n<p><span style="font-family:Verdana;">特色功能<br />\r\n1.顶帖子；&nbsp; 2.发表评论；.微博公告<br />\r\n4.大图浏览&nbsp; 5.发布图片；.用户自定义标签；<br />\r\n7.站内留言&nbsp; 8.上传头像&nbsp; 9.发布链接 10.内容搜索</span></p>\r\n<span style="font-family:Verdana;"> <p><br />\r\n浏览微博方式多样化：1.最新顶贴2.最新发贴3.人气帖排行4.评论贴排行5.最新评论帖</p>\r\n<p>演示站点：http://t.itf4.com/<br />\r\n管理后台：admin/default.asp<br />\r\n账号：admin<br />\r\n密码：admin</p>\r\n<p>源码是良精科技开发成员开发而成 <br />\r\n使用本源码只需在贵站做好良精科技http://www.liangjing.org的链接即</p>\r\n<p>免费版用户请登陆<a href="http://server.liangjing.org/">http://server.liangjing.org/</a>&nbsp;以发帖的形式获得技术支持 </span></p>', 1, 0, 0, 0, 0, 1, '', '', 1328976000, 1329042241, 0, '', '');

INSERT INTO `ljcms_download` VALUES(3, '良精中英文博客网站管理系统免费', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', 'http://www.liangjing.org/down/LJblog.rar', '', '', '<p>演示地址 http://blog.liangjing.org/Ch/index.html</p>\r\n<p>下载地址 http://www.liangjing.org/down/LJblog.rar</p>\r\n<p>使用许可协议 http://blog.liangjing.org/Ch/New-51.html</p>\r\n<p>分流下载 http://down.admin5.com/code_asp/11726.html</p>\r\n<p>&nbsp;</p>\r\n<p>源码之家 http://www.mycodes.net/18/2307.htm<br />\r\n&nbsp;<br />\r\n一、在本地调试要注意几点：<br />\r\n1、程序必须在根目<br />\r\n2、必须开启父路径<br />\r\n3、硬盘为NTFS格式的时候，请设置硬盘属性&gt;安全属性标签，设置成evryone和user为完全控制<br />\r\n4，网站LOGO修改地址 Chimagelogo.jpg<br />\r\n二、后台管<br />\r\n管理演示登录/admin/admin_Login.asp<br />\r\n管理帐号：admin 密码：admin<br />\r\n&nbsp;<br />\r\n三、多级分类显示修<br />\r\nch tr en语言目录下有关文<br />\r\n*list.asp *代表不同文件<br />\r\n*view.asp *代表不同文件<br />\r\nSearch.asp<br />\r\n查找&nbsp;&nbsp; &lt;%=WebMenu(0,0,2)%&gt;<br />\r\n其中最后一个数字表示显示分类的级数,这里,表示显示二级分类，根据用户需要可以修改成1以上的数字，但为了网页美观，建议不要超过3<br />\r\n&nbsp;<br />\r\n使用此程序请保留低部信息,良精制作链接及版权信息谢谢!<br />\r\n（不保留低部信息,良精制作链接及版权信息按侵权处理）解释权良精保<br />\r\n&nbsp;<br />\r\n如果您在使用中遇到问题，请先查看压缩包中的程序说明和常见问题，可能问题可以马上解决。为了能给您提供更好的服务，请在提交问题时遵守下列规则<br />\r\n&nbsp;<br />\r\n准确：不要提“你的产品有问题”这样的问题，要尽量的表达准确，请写出发生错误的现象或者程序代码片段等与问题相关的信息<br />\r\n&nbsp;<br />\r\n简练：把您问题的特征描述尽量只要写成几个简单的句子，尽量限制附加很多的代码，如不要把一个完整程序每个页面的代码都附加进来<br />\r\n&nbsp;<br />\r\n耐心：因为我们要回答的问题可能较多，所以您的问题得到回复可能需要几分钟、几小时、甚至几天，请耐心等待<br />\r\n&nbsp;<br />\r\n技术支持：<br />\r\n您在使用我们的产品时(良精网站管理系统)，遇到问题寻求技术支持解决，或者您找到程序的bug要报告，发送到asp3721@hotmail.com<br />\r\n&nbsp;<br />\r\n帮助中心网址 http://help.liangjing.org/</p>', 1, 0, 0, 0, 0, 1, '', '', 1328976000, 1329042270, 0, '', '');

INSERT INTO `ljcms_download` VALUES(4, '良精地方分类信息网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', 'http://www.liangjing.org/down/itf4.rar', '', '', '<p>良精地方分类信息网站管理系统</p>\r\n<p>演示地址 http://www.itf4.cn/</p>\r\n<p>官方网址 http://www.liangjing.org/</p>\r\n<p>下载地址 http://www.liangjing.org/down/itf4.rar</p>\r\n<p>后台登陆地址 http://localhost/manage/login.asp</p>\r\n<p>开源源</p>\r\n<p>用户 admin 密码 admin</p>\r\n<p>采用asp+access开发 运行稳定，快速，安全性能优良,功能更枪法是一套通用的信息网站源码,分类网站源码,</p>\r\n<p>地区分类信息网源码分类信息网站,分类信息网站程序,北京分类信息网站.</p>\r\n<p>本分类信网程序基于ASP+Access技术开发的分类信息程序，是经过多年的经验积累，完善设计</p>\r\n<p>精心打造的适用于各种服务器环境的安全、稳定、快速、强大、高效、易用、优秀的网站建设解决方案</p>\r\n<p>采用人性化的Windows操作方式开发，运行速度快，服务器资源占用更少；</p>\r\n<p>无论在稳定性、负载能力、安全等方面都有可靠的保证并赢得了广大用户的良好称赞</p>', 1, 2, 0, 0, 0, 1, '', '', 1328976000, 1329042298, 0, '', '');

INSERT INTO `ljcms_download` VALUES(5, 'EditPlus V2.20 Build 303 简体中文版', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', 'http://www.myszw.com/EditPlus.rar', '', '', '一套状态相当强大，完全取代记事本的文词处理工具软件，它拥有完全无限制的Undo/Redo(还原)、英文拼写检查、自动换行、列数标记、查找取代、同时还可编辑多个文档、全屏浏览.等状态。而它还有一个很好用的状态，就是它有监视剪贴板的状态，能够同步于剪贴板而自动将文词贴进EditPlus的编辑窗口中，让你省去做贴上的步骤&nbsp;<br />\r\n<br />\r\n另外它也是一个好用的HTML网页编辑软件，除了可以颜色标记HTML&nbsp;Tag&nbsp;(同时支援&nbsp;C/C++、Perl、Java)&nbsp;外，还内置完整的HTML&nbsp;CSS1&nbsp;指令状态，支持&nbsp;HTML,&nbsp;CSS,&nbsp;PHP,&nbsp;ASP,&nbsp;Perl,&nbsp;C/C++,&nbsp;Java,Java<i>script</i>&nbsp;and&nbsp;VB<i>script</i>；对于习惯用记事本编辑网页的朋友，它可帮你节省一半以上的网页制作时间。倘若你有安装&nbsp;IE&nbsp;3.0以上版本，它还会结合&nbsp;IE&nbsp;浏览器于EditPlus的窗口中，让你可以直接预览编辑好的网页若没安装IE，也可指定浏览器路径)。是一个相当棒又多用途多状态的编辑软件<br />', 2, 3, 1, 0, 0, 1, '', '', 1328976000, 1329042322, 0, '', '');

INSERT INTO `ljcms_download` VALUES(6, 'iis 备份软件', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', 'http://www.liangjing.org/down/IISBackUp.rar', '', '', '本软件用于备份移植 IIS 的站点配置信息，可以导出或导入IIS 服务器的所有站点（Web MS Ftp）的配置信息<br />\r\n&nbsp; 当您需要重启IIS 服务器时，或者需要将一个IIS 服务器的站点配置移至另外一个IIS 服务器时，使用IIS 本身的“备份还原配置”功能是无法实现的，一般情况下只能通过手工的方法重新逐个地创建站点，而使用本软件则可以简单地自动完成任务<br />\r\n&nbsp; v0.2版本在程序上做了优化，比之前的v0.1版本更兼容于WINXP/WIN2003操作系统', 2, 0, 0, 0, 0, 1, '', '', 1328976000, 1329042349, 0, '', '');

INSERT INTO `ljcms_download` VALUES(7, 'WinRAR 简体美化版', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', 'http://www.asp99.cn/WRAR351.EXE', '', '', '<span style="font-family:Verdana;">WinRAR 是强大的压缩文件管理器。它提供RAR ZIP 文件的完整支持，能解 ARJ、CAB、LZH、ACE、TAR、GZ、UUE、BZ2、JAR、ISO 格式文件。WinRAR 的功能包括强力压缩、分卷、加密、自解压模块、备份简易</span>', 2, 0, 0, 0, 0, 1, '', '', 1328976000, 1329042379, 0, '', '');

INSERT INTO `ljcms_download` VALUES(8, '建站留言本程', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', 'http://down.asp99.cn/book.rar', '', '', '<p><span style="font-family:Verdana;">安装方法<br />\r\n只需将此目录下的全部文件上传到网站的任一目录即可<br />\r\n例如上传到您的网站的book目录，则以http://域名/book/index.asp 的方式打开留言本</span></p>\r\n<p><span style="font-family:Verdana;"><br />\r\n管理方法<br />\r\n点击留言本右下角的小书本图标即可进入管理页<br />\r\n默认的管理员名称和密码均为：admin（小写，前后无空格）</span></p>', 1, 4, 0, 0, 0, 1, '', '', 1328976000, 1329042407, 0, '', '');

INSERT INTO `ljcms_download` VALUES(9, '解析Flash动画.swf文件.exe文件）的工具', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', 'http://www.asp99.cn/swf.rar', '', '', '硕思闪客精灵是一款用于浏览和解析Flash动画.swf文件.exe文件）的工具。最新版本有着强大的功能，可以将swf文件导出成FLA文件。它还能够将flash动画中的图片、矢量图、声音、视频（*.flv）、文字、按钮、影片片段、帧等基本元素完全分解，最重要的是可以对动作的脚本(Action&nbsp;<i>script</i>)进行解析，清楚的显示其动作的代码，让对Flash动画的构造一目了然。最新版本能更好的支持Flash&nbsp;MX和动作脚本2.0.&nbsp;她不仅能够从IE浏览器中或临时文件缓存中直接采集flash动画，还能够通过分析和反编译将flash动画中的声音，图像，动画短片等元素提取出来，甚至能分析出该动画中包含的动作，并转化为清晰可读的代码&nbsp;<br />', 2, 5, 0, 0, 0, 1, '', '', 1328976000, 1329042430, 0, '', '');

INSERT INTO `ljcms_download` VALUES(10, '在线客服 插件 下载', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', 'http://down.asp99.cn/left-kf.rar', '', '', '<p><img border="0" src="http://liangjing.org/qiyejianzhan/uploadfile/20100826105522154.jpg" /></p>\r\n<p><img border="0" src="http://liangjing.org/qiyejianzhan/uploadfile/20100826105548816.jpg" /></p>\r\n<p>使用此客服功能，把下面一段代码加入到默认首页文件(例如index.asp)最底部保存即可<br />\r\n修改客服号码等内容，打开kefu.htm文件修改<br />\r\n------------------------------------------------------------<br />\r\n欢迎经常登录我们的网站，获取最新最全的程序代码<br />\r\n演示地址&nbsp;<a href="http://admin.asp99.cn/web26/Index.Asp" target="_blank">http://admin.asp99.cn/web26/Index.Asp</a><br />\r\n</p>', 1, 4, 0, 0, 0, 1, '', '', 1328976000, 1329042453, 0, '', '');

INSERT INTO `ljcms_download` VALUES(11, 'FTP软件', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', 'http://www.myszw.com/ftp.rar', '', '', '<span style="font-family:Verdana;">使用容易且很受欢迎的FTP软件，下载文件支持续传、可下载或上传整个目录、具有不会因闲置过久而被站台踢出站台。可以上载下载队列，上载断点续传，整个目录覆盖和删除等</span>', 2, 5, 0, 0, 0, 1, '6868668868', '', 1328976000, 1329042483, 0, '', 'PN11231313131');

INSERT INTO `ljcms_download` VALUES(12, '良精net企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', 'http://www.liangjing.org/down/LJnetCMS.rar', '', '1111', '<p>分流下载</p>\r\n<p>源码之家&nbsp;&nbsp; <a href="http://www.mycodes.net/101/4637.htm">http://www.mycodes.net/101/4637.htm</a></p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;&nbsp; <a href="http://down.chinaz.com/soft/30358.htm">http://down.chinaz.com/soft/30358.htm</a></p>\r\n<p>&nbsp;</p>\r\n<p>admin5&nbsp;&nbsp;&nbsp;&nbsp; <a href="http://down.admin5.com/net/4009.html">http://down.admin5.com/net/4009.html</a></p>\r\n<p>&nbsp;</p>\r\n<p>2011年7月1日更新如下...</p>\r\n<p>新增 后台上传视频功能</p>\r\n<p>更新 兼火狐浏览器 用户注册时间</p>\r\n<p>更新 后台上传图片管理</p>\r\n<p>新增 产品排序</p>\r\n<p>(新增了skin22风格)共两个风格可用</p>\r\n<p>新增功能 静态生生 风格切换 模版管理</p>\r\n<p>中英文加繁体三语版本</p>\r\n<p>常见错误请见 http://help.liangjing.org/HTML/200812111337159375.html</p>\r\n<p>使用说明书请见帮助中&nbsp; http://help.liangjing.org/index.html<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br />\r\n前台访问地址：http://网址/Default.aspx</p>\r\n<p>打开首页后会看到下面有后台访问地址</p>\r\n<p>编码UTF-8 用户名admin密码admin</p>\r\n<p>后台主要功能如下</p>\r\n<p>1、系统管理：管理员管理，网站配置，上传文件管理,QQ-MSN 在线客服设置，文件浏览，模版的编辑，样式表的编辑</p>\r\n<p>2、企业信息：后台自由添加修改企业的各类信息及介绍</p>\r\n<p>3、产品管理：产品类别新增修改管理，产品添加修改以及产品的审核</p>\r\n<p>4、订单管理：查看订单的详细信息及订单处理</p>\r\n<p>5、会员管理：查看修改删除会员资料，及锁定解锁功能。可在线给会员发信！</p>\r\n<p>6、新闻管理：能分大类和小类新闻，不再受新闻栏目的限制<br />\r\n</p>', 1, 58, 6, 0, 0, 1, '6868668868', '>=', 1328976000, 1329042510, 0, '', 'PN11231313131');

INSERT INTO `ljcms_downloadcate` VALUES(2, '常用软件', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 0, 0, 2, 1, '', 1329042177, 0, '', '', 1, '', 1);

INSERT INTO `ljcms_downloadcate` VALUES(1, '网站源码', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 0, 0, 1, 1, '', 1329042168, 0, '', '', 1, '', 1);

INSERT INTO `ljcms_guestbook` VALUES(1, '', '张三', 1, '普通职', '020-12345678', '020-87654321', '13812345678', 'admin@qq.com', '123456', '北京良精科技有限公司', '北京市海淀区中关村08', 'IT互联', '', '苏丹解放了接待来访\r\n第三方加了斯蒂芬\r\n是否是否', 1314271072, '127.0.0.1', 0, 1, '管理', 1314271315, '谢谢您的支持');

INSERT INTO `ljcms_guestbook` VALUES(2, '', '张山', 1, '普通职', '020-12345678', '020-87654321', '13812345678', 'asp3721@hotmail.com', '', '北京市某某科技公司', '北京市海淀区中关村08', 'IT互联', '', '这是测试留言！！', 1314615718, '127.0.0.1', 0, 1, 'admin', 1341307118, 'testestset');

INSERT INTO `ljcms_guestbook` VALUES(3, '', '张三', 1, '普通职', '020-12345678', '020-87654321', '13812345678', 'asp3721@hotmail.com', '', '广州市某某科技公司', '北京市海淀区中关村08', 'IT互联', '', '斯蒂芬斯蒂芬地方师傅', 1314615990, '127.0.0.1', 0, 1, '', 0, '');

INSERT INTO `ljcms_guestbook` VALUES(4, '', 'adminsa', 1, '软件工程师', '010-35143213', '010-6868222', '13223423423', 'sdfsdfs@126.com', '', '良精科技', '北京', 'IT', '', '良精科技企业网站正式发布了', 1341280983, '127.0.0.1', 1, 1, NULL, 0, NULL);

INSERT INTO `ljcms_info` VALUES(1, 1, '发布公司新闻-标题-01', '', '', '该新闻资讯的简单文字介', '详细的文字内', 1329038798, 1, 1, '6868668868', '>=', 115, 0, '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', '');

INSERT INTO `ljcms_info` VALUES(7, 1, '中国中东部将出现大风降温天气', '', '', '核心提示：据中央气象台预报，12 5日，中国黄淮以南大部地区将出现一次大范围雨雪天气过程。昨日，全国大部地区气温有所回升，但从今天开始，中东部降水将逐渐增多，并自北向南出现大风降温天气', '<div style="text-align:center;"><img alt="中国中东部将出现大风降温天气 苏皖鲁豫有雨雪" src="http://img1.cache.netease.com/catchpic/5/53/537DE2D6394042B081B8E90E362E8166.jpg" /></div>\r\n<div style="text-align:left;text-indent:2em;margin:5px 0px;"></div>\r\n<p>中新&nbsp;&nbsp;2日电 据中央气象台预报&nbsp;2&nbsp;5日，中国黄淮以南大部地区将出现一次大范围雨雪天气过程，长江以南地区以降雨为主，西北地区东南部、黄淮、江淮北部等地的部分地区有小到中雪或雨夹雪</p>\r\n<p>昨日，中国雨雪天气较弱，南方大部地区降雨量不&nbsp;毫米，全国大部地区气温有所回升。但从今天开始，中国中东部的降水将逐渐增多，预计未&nbsp;4小时，雨雪主要出现在黄淮、江淮等地，明日，雨雪范围将继续扩大；与此同时，中东部地区将自北向南先后出现大风降温天气，周末好不容易爬升了几度的气温又将继续被打压</p>\r\n<p>今天白天到夜间，黄淮大部、陕西东南部以及新疆北部、西藏西部和东部、青海南部、川西高原西部等地有小到中雪或雨夹雪；江淮、江汉、江南大部、华南西部以及西南地区东部等地有小到中雨或阵&nbsp;附图)。预计周一(13&nbsp;，雨雪范围会进一步扩大，江淮大部、江南北部将有中雨</p>\r\n<p>除雨雪天气的影响外，13&nbsp;6日，受冷空气影响，中东部大部地区将自北向南出&nbsp;&nbsp;℃降温，局&nbsp;0℃；内蒙古中东部、华北大部、东北地区、黄淮有4&nbsp;级偏北风，东部海域先后有6&nbsp;级大风</p>\r\n<p>气象专家提醒，目前正值春运后期，大范围雨雪天气将对交通出行产生一定影响，请上述地区的居民注意天气变化，合理安排行程。同时，近期气温多波动，起伏较大，大家要注意做好防寒保暖工作，以免生病感冒</p>', 1329039653, 1, 1, '6868668868', '>=', 21, 0, '企业网站开发，OA办公系统、政府网站、旅游网站', '企业网站开发，OA办公系统、政府网站、旅游网站', '企业网站开发，OA办公系统、政府网站、旅游网站', '', '');

INSERT INTO `ljcms_info` VALUES(17, 1, '关于良精php开源企业网站管理系统', '', '', '精php开源企业网站管理系统也称良精企业网站程序，是良精公司开发中国首套免费提供企业网站模板的营销型企业网站管理系统，系统前台生成html、完全符合SEO、同时有在线客服、潜在客户跟踪、便捷企业网站模板制作、搜索引擎推广等功能的企业网站系统', '<div>\r\n<p style="text-indent:2em;">良精php开源企业网站管理系统也称良精企业网站程序，是良精公司开发中国首套免费提供企业网站模板的营销型企业网站管理系统，系统前台生成html、完全符合SEO、同时有在线客服、潜在客户跟踪、便捷企业网站模板制作、搜索引擎推广等功能的企业网站系统。良精企业网站系统也称良精企业网站程序，是良精公司开发中国首套免费提供企业网站模板的营销型企业网站管理系统，系统前台生成html、完全符合<a title="SEO技术、方法和技巧,基于SEO的网站策划等内容，SEO课程先进完整实用，快速打造SEO高手！" href="http://www.liangjing.org/" target="_blank">seo</a>、同时有在线客服、潜在客户跟踪、便捷企业网站模板制作、搜索引擎推广等功能的企业网站系统。 </p>\r\n<p style="text-indent:2em;">说明：以下情况不属于服务范围</p>\r\n<div class="blank30"></div>\r\n<ul class="description">\r\n<li>自行修改或使用非原始<a class="seolabel" title="企业网站管理系统" href="http://www.liangjing.org/" target="_blank">良精企业网站管理系统</a>程序代码及模板风格产生的问题； <li>自行对数据库进行直接操作导致数据库出错或者崩溃； <li>非官方的插件安装以及由于安装插件造成的故障； <li>服务器、虚拟主机原因造成的系统故障； <li>非官方二次开发或定制，官方二次开发或定制后不提供升级服务； </li>\r\n</ul>\r\n<p style="text-indent:2em;"><strong>注：</strong> </p>\r\n<ul class="zhuyi">\r\n<li>任何网站可永久免费，且不限制商业用途，但必须保留网站底部及相应的官方版权信息和链接； <li>商业授权以顶级域名为授权基础，一个普通商业授权只能用于一个网站；高级版一组域名必须为同一个单位或个人注册，且关键词必须一致或相近，如 liangjing.org，liangjing.net； <li>由于商品的特殊性，商业授权码生成后不提供更改域名服务； <li>首次购买服务版商业授权赠送1年的技术服务，技术论坛可以提供永久的技术支持，第二年起如需QQ/MSN等技术服务需要续费服务费用； <div class="blank30">\r\n<p><strong>本系统更新说明：</strong></p>\r\n<p>2012年更新如下：</p>\r\n<p>2012-02-01 更新网站后台弹框补丁。<br />\r\n2012-02-01 更新前台模版 使其实现多模版程序。</p>\r\n<p>2012-05-01 更新</p>\r\n<p>1、更新联系我们 <br />\r\n2、修改友情连接问题<br />\r\n3、留言不能提交问题</p>\r\n<p>2012-06-01 更新<br />\r\n1、模版在次增加 良精程序V3.5版本<br />\r\n2、修改后台bug问题。</p>\r\n<p>2012-06-13 更新<br />\r\n1、新增加良精V3.9模版</p>\r\n<p>2、最新增加仿小米官方站模版<br />\r\n</p>\r\n</div>\r\n</li>\r\n</ul>\r\n</div>', 1357268142, 1, 1, '6868668868', '>=', 15, 0, '关于良精php开源企业网站管理系统', '关于良精php开源企业网站管理系统', '关于良精php开源企业网站管理系统', '', '');

INSERT INTO `ljcms_info` VALUES(15, 1, '通用企网站导行栏目添加', '', '', '一.登陆网站后台 点击 企业信息 新增企业信息 添加如:解决方案\r\n\r\n1 管理企业信息 查看一下添加的解决方案是否添加成功 点解决方案的查看 IE栏里地址是 Aboutus.asp?Title=解决方案\r\n\r\n\r\n二 添加导行栏目 \r\n\r\n1进入后台管理 点击菜单管理 中文菜单管理 添加菜单栏目 \r\n\r\n菜单名称：解决方案\r\n菜单说明：解决方案\r\n菜单链接地址：Aboutus.asp?Title=解决方案\r\n\r\n添加成功返回前台查看是否添加成功', '<table class="ke-zeroborder" border="0" cellspacing="0" cellpadding="0" width="95%" align="center">\r\n<tbody>\r\n<tr>\r\n<td style="line-height:25px;padding-left:8px;padding-right:8px;font-size:12px;" class="SiteLink">\r\n<p>一.登陆网站后台 点击 企业信息 新增企业信息 添加如:解决方案</p>\r\n<p>1 管理企业信息 查看一下添加的解决方案是否添加成功 点解决方案的查看 IE栏里地址是 Aboutus.asp?Title=解决方案</p>\r\n<p><br />\r\n二 添加导行栏目 </p>\r\n<p>1进入后台管理 点击菜单管理 中文菜单管理 添加菜单栏目 </p>\r\n<p>菜单名称：解决方案<br />\r\n菜单说明：解决方案<br />\r\n菜单链接地址：Aboutus.asp?Title=解决方案</p>\r\n<p>添加成功返回前台查看是否添加成功</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td style="padding-right:18px;" height="20" align="right"></td>\r\n</tr>\r\n</tbody>\r\n</table>', 1357267822, 1, 1, '6868668868', '>=', 3, 0, '通用企网站导行栏目添加', '通用企网站导行栏目添加', '通用企网站导行栏目添加', '', '');

INSERT INTO `ljcms_info` VALUES(16, 1, '企业建站系统V9.0', '', '', '1. 企业信息：发布介绍企业的各类信息，如企业简介、组织机构、营销网络、企业荣誉、联系方式，并可随意增加新的栏目等。 \r\n2. 新闻动态：发布企业新闻和业内资讯，从后台到前台真正实现无限级分类显示，并随意控制显示级数，大大增加信息发布的灵活性。 \r\n3. 产品展示：发布企业产品，按产品类别显示及搜索产品，并可多选产品直接下订单询盘，无限级分类，大大增加信息发布的灵活性。 \r\n4. 下载资源：发布供网站浏览者和客户下载的资料等，如使用手册、销售合同、软件等，无限级分类。', '<div style="line-height:24px;display:block;" id="con1">\r\n<p>良精中文企业网站管理系统V9.0</p>\r\n<p><br />\r\n前台演示 <a href="http://admin.itf4.com/LJv90/Index.html">http://admin.itf4.com/LJv90/Index.html</a><br />\r\n&nbsp;<br />\r\n后台演示 <a href="http://admin.itf4.com/ljv90/admin/Admin_Login.asp">http://admin.itf4.com/ljv90/admin/Admin_Login.asp</a></p>\r\n<p><br />\r\n下载地址 <a href="http://www.liangjing.org/down/LJwebFree.rar">http://www.liangjing.org/down/LJwebFree.rar</a></p>\r\n<p><br />\r\n&nbsp;<br />\r\n&nbsp;<br />\r\n用户名 admin 密码 admin 管理号 admin<br />\r\n&nbsp;<br />\r\n良精通用网站管理系统V9.0兼容Firefox、Maxthon、TT等常用浏览器！<br />\r\n&nbsp;<br />\r\n后台可先切换 动态和静态 asp+html <br />\r\n主要功能模块介绍 <br />\r\n1. 企业信息：发布介绍企业的各类信息，如企业简介、组织机构、营销网络、企业荣誉、联系方式，并可随意增加新的栏目等。 <br />\r\n2. 新闻动态：发布企业新闻和业内资讯，从后台到前台真正实现无限级分类显示，并随意控制显示级数，大大增加信息发布的灵活性。 <br />\r\n3. 产品展示：发布企业产品，按产品类别显示及搜索产品，并可多选产品直接下订单询盘，无限级分类，大大增加信息发布的灵活性。 <br />\r\n4. 下载资源：发布供网站浏览者和客户下载的资料等，如使用手册、销售合同、软件等，无限级分类。 <br />\r\n5. 人力资源：发布招聘信息，人才策略，浏览者可在线递交简历。 <br />\r\n6. 其他信息：相当于无限扩展栏，并可进行无限分类，可以用于发布网站主栏目未归类的信息，如解决方案、成功案例、购买流程等。 <br />\r\n7. 会员中心：会员可任意设置级别，并可根据级别限制浏览相关内容，会员机制与订购、应聘、留言三大模块有机结合的，我们充分考虑到了网站访问者的惰性，所以会员机制与三大模块又可完全脱离，即未注册也同样能留言、下订单、递交简历。 <br />\r\n8. 留言反馈：以留</p>\r\n<p>&nbsp;</p>\r\n</div>', 1357267933, 1, 1, '6868668868', '>=', 5, 0, '企业建站系统V9.0', '企业建站系统V9.0', '企业建站系统V9.0', '', '企业建站系统V9.0');

INSERT INTO `ljcms_info` VALUES(10, 7, '罗姆尼赢得缅因州共和党总统初', '', '', '核心提示：美国缅因州共和党党部负责人称，罗姆尼赢得了该州11日举行的总统初选，得票率为39%。此前，罗姆尼连续在佛罗里达与内华达州获胜，而桑托勒姆则取得了密苏里、明尼苏达与科罗拉多等三州的胜利', '<p>中新&nbsp;&nbsp;2日电 据外电报道，美国缅因州共和党党部负责人称，共和党总统候选人罗姆尼赢得了该州11日举行的总统初选，其得票率&nbsp;9%</p>\r\n<p>得克萨斯州众议员保罗得票&nbsp;6%，位居第二</p>\r\n<p>前参议员桑托勒姆获得18%选票的支持，前众议院议长金里&nbsp;%</p>\r\n<p>当地时间7日，密苏里、明尼苏达与科罗拉多等三州同时举行总统初选，桑托勒姆夺得这三州的胜利</p>\r\n<p>此前，罗姆尼连续在佛罗里达与内华达州获胜</p>\r\n<p></p>', 1329039751, 1, 1, '2147483647', '>=', 22, 0, '企业网站开发，OA办公系统、政府网站、旅游网站', '企业网站开发，OA办公系统、政府网站、旅游网站', '企业网站开发，OA办公系统、政府网站、旅游网站', '', '');

INSERT INTO `ljcms_info` VALUES(11, 7, '欧洲严寒致&nbsp50人死 数十万人交通受', '', '', '核心提示：据“中央社  2日报道，欧洲超强寒流已夺走 50条生命，部分地区积雪深及民宅屋顶，数十万偏远地区的民众受困屋内。多瑙河已因结冰而关闭数百公里的航运。意大利多地航班受到影响，而塞尔维亚电力吃紧，政府已采取限电措施。气象预报预测寒流将持续 月中旬', '<p>中新&nbsp;&nbsp;2日电 据“中央社&nbsp;2日报道，欧洲部分地区积雪已深及民宅屋顶，导致数十万偏远地区民众受困屋内，欧洲超强寒流已夺走&nbsp;50条生命</p>\r\n<p>欧洲巴尔干半岛地区和意大利迎接更大降雪。多瑙河已因结了厚厚的冰而关闭数百公里的航运，其保加利亚河段也结冻，&nbsp;7年首次</p>\r\n<p>意大利各地持续降下大雪，首都罗马成白茫茫的大地，山区村落对外交通断绝，各地公路、铁路大乱，航班也受到影响</p>\r\n<p>这是罗马&nbsp;0世纪80年代以来最大的一场雪，迫使罗马竞技场冰封关闭，但也让观光客和居民有机会一睹圣伯多禄广&nbsp;Saint Peter''s Square)和特莱维喷泉(Trevi fountain)白雪皑皑美景</p>\r\n<p>意大利民航局声明，罗马费乌米奇诺机场(Fiumicino airport)格林尼治时间11日下&nbsp;点起取消一半航班。境内其它机场也将关闭或减少航班</p>\r\n<p>在黑山，首都波德戈里察积雪创&nbsp;0年新高的50厘米，迫机场关闭，并因雪崩意外暂停通往塞尔维亚的铁路运输，城市几乎陷入瘫痪</p>\r\n<p>罗马尼亚接获的通报丧生人数目前&nbsp;5人，塞尔维亚3人，捷克和奥地利各有1人罹难</p>\r\n<p>罗马尼亚一&nbsp;8岁老妇告诉摄影师：“我这辈子没看过那么多雪。</p>\r\n<p>各国有关单位表示，估计罗马尼亚目前仍&nbsp;万人受困，巴尔干地区各国有&nbsp;1万人受困，其中黑山就&nbsp;万人动弹不得，占该国人口的近10%</p>\r\n<p>在塞尔维亚，当地电力吃紧，政府因此采取限电措施，并呼吁企业将用电量降至最低</p>\r\n<p>15日&nbsp;6日为塞尔维亚国庆，政府已宣布17日连续放假，将假期延伸至下一周</p>\r\n<p><br />\r\n在科索沃，当局表示南部瑞斯特里（Restelica)小村庄发生雪崩，造成至少7人丧生，另外3人失踪</p>\r\n<p>警方表示，雪崩吞噬至&nbsp;5间民房，但当时仅有两间民宅内有人</p>\r\n<p>居民和紧急救难人员协助清除民宅时，发&nbsp;名年纪约&nbsp;&nbsp;岁的女孩生还，她目前已被送往医院</p>\r\n<p>在波兰，消防大队发言人佛拉卡（Pawel Fratcak)表示，暖气设备故障引发多处民宅和公寓爆发致命大火，过去两夜酿11死</p>\r\n<p>气象预报预测这波两周前袭欧的寒流将持续至2月中旬。比利时气象学家表示，比利时遭遇70年来历时最长的寒流，布鲁塞尔市郊气温连&nbsp;3天下将到摄氏0度以下</p>\r\n<p>&nbsp;</p>', 1329041347, 1, 1, '2147483647', '>=', 43, 0, '企业网站开发，OA办公系统、政府网站、旅游网站', '企业网站开发，OA办公系统、政府网站、旅游网站', '企业网站开发，OA办公系统、政府网站、旅游网站', '', '');

INSERT INTO `ljcms_info` VALUES(12, 7, '卡扎菲第三子称利比亚即将爆发起义', '', '', '核心提示：卡扎菲第三子萨阿迪日前称，利比亚即将爆发针对执政当局的“起义”，起义将席卷全国。另外，他称希望“随时”回国，见证这场起义', '<p>首先，这将不会是一场仅限于部分地区的起义。起义将席卷全国，我将跟随并见证这场确实存在的起义，它将逐日壮大</p>\r\n<p>——卡扎菲三子萨阿</p>\r\n<p>据新华社&nbsp;利比亚过渡政&nbsp;1日敦促邻国尼日尔引渡利前领导人穆阿迈尔·卡扎菲的第三子萨阿迪·卡扎菲，指认萨阿迪部分言论“威胁两国关系”</p>\r\n<p>萨阿迪前一天经阿拉伯卫星电视台发声，希望“随时”回国，称利比亚即将爆发针对执政当局的“起义”</p>\r\n<p><br />\r\n利比亚通讯&nbsp;1日援引过渡政府外交部长阿舒尔·本·哈伊勒和尼日尔外长巴祖姆·穆罕默德当天通话内容报道，利方对萨阿迪“攻击性言论”表达“强烈不满”。哈伊勒向尼方重申，萨阿迪言论威胁两国双边关系，尼政府应当对他采取严厉措施，包括引渡回利比亚接受调查</p>\r\n<p>萨阿迪和部分家人去年8月在反卡扎菲武装控制首都的黎波里后逃往尼首都尼亚美，受到居住监视。尼方先前表态，萨阿迪将留在这一西非国家，直至联合国取消他的旅行禁令</p>\r\n<p>按照利比亚通讯社的说法，尼外长巴祖姆当天向利政府和民众致歉，承诺将与总统穆罕默杜·优素福会商。“他（巴祖姆）希望向利方确认，（引渡）要求将依照法律和惯例得到回应。</p>\r\n<p>&nbsp;</p>', 1329041379, 1, 1, '2147483647', '>=', 116, 0, '企业网站开发，OA办公系统、政府网站、旅游网站', '企业网站开发，OA办公系统、政府网站、旅游网站', '企业网站开发，OA办公系统、政府网站、旅游网站', '', '');

INSERT INTO `ljcms_info` VALUES(13, 1, '忘记网站后台密码怎么办', '', '', '(数据库文件在Database文件夹下，原样Database\\#Database.mdb) \r\n\r\n2.用Microsoft Office Access 打开数据库\r\n3.替换admin表里的MD5(16)字符串', '<p>1.用FTP下载网站的数据库.<br />\r\n&nbsp;<br />\r\n(数据库文件在Database文件夹下，原样Database\\#Database.mdb) </p>\r\n<p>2.用Microsoft Office Access 打开数据库<br />\r\n3.替换admin表里的MD5(16)字符串<br />\r\n4.关闭数据库 用FTP上传替换数据库 就OK了</p>\r\n<p>&nbsp;</p>\r\n<p>说明:<br />\r\n1.数据库存放的位置 Database域Databases这个文件夹里<br />\r\n2.MD5加密后的字符串7a57a5a743894a0e 等于 admin</p>', 1357267563, 1, 1, '6868668868', '>=', 2, 0, '忘记网站后台密码怎么办', '忘记网站后台密码怎么办', '忘记网站后台密码怎么办', '', '忘记网站 后台密码');

INSERT INTO `ljcms_info` VALUES(14, 1, '解决某些网站验证码不能显示的问题', '', '', 'Q：错误提示： 未启用父路径 \r\n症状举例： \r\nServer.MapPath() 错误 ''ASP 0175 : 80004005'' \r\n不允许的 Path 字符 \r\n/liangjing/cms/news/OpenDatabase.asp ，行 4 \r\n在 MapPath 的 Path 参数中不允许字符 ''..'' 。', '<p>Q：错误提示： 未启用父路径 <br />\r\n症状举例： <br />\r\nServer.MapPath() 错误 ''ASP 0175 : 80004005'' <br />\r\n不允许的 Path 字符 <br />\r\n/liangjing/cms/news/OpenDatabase.asp ，行 4 <br />\r\n在 MapPath 的 Path 参数中不允许字符 ''..'' 。 <br />\r\nA：许多 Web 页面里要用到诸如 ../ 格式的语句（即回到上一层的页面，也就是父路径），而 IIS6.0 出于安全考虑，这一选项默认是关闭的。 <br />\r\n解决方法： <br />\r\n在 IIS 中属性 -&gt; 主目录 -&gt; 配置 -&gt; 选项中。把 ” 启用父路径 “ 前面打上勾。确认刷新。</p>', 1357267692, 1, 1, '6868668868', '>=', 4, 0, '解决某些网站验证码不能显示的问题', '解决某些网站验证码不能显示的问题', '解决某些网站验证码不能显示的问题', '', '解决某些网站,验证码');

INSERT INTO `ljcms_infocate` VALUES(1, '公司新闻', '公司新闻', '公司新闻', '公司新闻', '', 0, 0, 1, 1, '公司新闻', 1314323798, 0, '', '', 1, '', 1);

INSERT INTO `ljcms_infocate` VALUES(7, '国外新闻', '企业网站开发，OA办公系统、政府网站、旅游网站', '企业网站开发，OA办公系统、政府网站、旅游网站', '企业网站开发，OA办公系统、政府网站、旅游网站', '', 0, 0, 7, 1, '良精新闻发布系统', 1333956364, 0, '', '', 1, '', 1);

INSERT INTO `ljcms_infocate` VALUES(3, '军事新闻', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 0, 0, 3, 1, '', 1332724078, 0, '', '', 1, '', 1);

INSERT INTO `ljcms_infocate` VALUES(4, '体育新闻', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 0, 0, 4, 1, '', 1332724102, 0, '', '', 1, '', 1);

INSERT INTO `ljcms_infocate` VALUES(5, '财经新闻', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 0, 0, 5, 1, '', 1332724123, 0, '', '', 1, '', 1);

INSERT INTO `ljcms_infocate` VALUES(6, '科技新闻', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 0, 0, 6, 1, '', 1332724132, 0, '', '', 1, '', 1);

INSERT INTO `ljcms_job` VALUES(1, 2, '技术支持', '北京', 0, '<p>1、编写开发计划</p>\r\n<p>负责公司旗下网站功能改进计划和网络安全计划的编写。 </p>\r\n<p>2、网站功能修改和升级</p>\r\n<p>按照计划的时间和质量要求，对网站前后台功能进行修改和升级；负责网站代码的优化和维护，保证网站的运行效率。</p>\r\n<p>3、日常业务开发</p>\r\n<p>每天程序员根据公司网站业务需要开发，制作和程序修改要求，必须按时按质按量地完成日常公司网站业务的编程开发技术工作。 </p>\r\n<p>4、网站测试</p>\r\n<p>网站开发前期必需先测试，测试成功后方可上传。如因违规操作造成的公司损失由个人全部负责。 </p>\r\n<p>5、软硬件维护</p>\r\n<p>负责每半个月必须对公司旗下网站软硬设施进行安全和稳定性巡检；并负责统计和监视系统日志。同时，也要做好内部局域网和网站机房的系统和网络故障的检修排除工作。</p>\r\n<p>6、防毒防黑</p>\r\n<p>负责即时监控互联网上发现的最新病毒和黑客程序及查杀方法，并及时为每台工作机和服务器查堵系统安全漏洞。每半月定期杀毒和升级防黑策略，排除因此出现的网络故障。 </p>\r\n<p>7、数据管理</p>\r\n<p>每三天必须对网站的重要数据（包括网站程序、网站数据库和网站运行日志等）做增量备份，并半个月对程序和数据库做完全备份。日常负责管理网站的备份数据，一旦出现问题，及时安全恢复数据。 </p>\r\n<p>8、技术支持</p>\r\n<p>每日为客户提出的、客服人员无法解答的专业技术问题提供支持和回馈，保证客户的满意度。 </p>\r\n<p>9、软硬件采购</p>\r\n<p>负责公司旗下网站发展所需要的软硬件的采购和选型；同时对外包编程工作的质量和进度加以监督和管理 </p>\r\n<p><br />\r\n10、外包 (网站合作)</p>\r\n<p>项目技术控制对于外包的软件项目的技术方面进行设计、实施跟踪和交付成果的控制和验证。保证外包项目能完全按照我方技术要求和规划完成。 <br />\r\n</p>', '<p>1、 具备丰富Web开发经验，具备网站设计经验。熟悉PHP、ASP等网站程序开发语言，熟悉SqlServer、MySql等数据库的管理及开发。了解VSS或CVS等源代码管理工具的使用。 </p>\r\n<p>2、 具有良好的沟通能力，理解力强，有团队合作意识，具备敬业负责的精神。 </p>\r\n<p>3、 具备良好的程序设计功底，要求不低于一年行业工作经验. </p>\r\n<p>4、出易于维有良好的编码习惯，能够编写护的代码。有良好的程序设计功底。 </p>\r\n<p>5、 精通 JAVA, C++, FLASH,DIV</p>\r\n<p>6、 能吃苦耐劳，有工作热情，能够按时完成公司交给的设计及编程任务。<br />\r\n</p>', '', '82993936@qq.com', 1329270968, 1, 0);

INSERT INTO `ljcms_jobcate` VALUES(1, '产品交互', '', '', '', '', 0, 0, 1, 1, '', 1314257257, 0, '', '', 1, '', 1);

INSERT INTO `ljcms_jobcate` VALUES(2, '技术研发类', '', '', '', '', 0, 0, 2, 1, '', 1314257314, 0, '', '', 1, '', 1);

INSERT INTO `ljcms_jobcate` VALUES(3, '数据平台', '', '', '', '', 0, 0, 3, 1, '', 1314257321, 0, '', '', 1, '', 1);

INSERT INTO `ljcms_jobcate` VALUES(4, '测试及支持类', '', '', '', '', 0, 0, 4, 1, '', 1314257326, 0, '', '', 1, '', 1);

INSERT INTO `ljcms_jobcate` VALUES(5, '产品运营', '', '', '', '', 0, 0, 5, 1, '', 1314257332, 0, '', '', 1, '', 1);

INSERT INTO `ljcms_jobcate` VALUES(6, '市场商务', '', '', '', '', 0, 0, 6, 1, '', 1314257336, 0, '', '', 1, '', 1);

INSERT INTO `ljcms_link` VALUES(6, '婚纱摄影工作室', '', 'http://www.itf4.net', 1, '', 1315218371, 1, '', 5);

INSERT INTO `ljcms_link` VALUES(3, '官方网站', '', 'http://www.liangjing.org/', 1, '', 1314254196, 1, '', 1);

INSERT INTO `ljcms_link` VALUES(4, '企业黄页', '', 'http://yp.liangjing.org/', 1, '', 1314254219, 1, '企业黄页', 4);

INSERT INTO `ljcms_link` VALUES(5, '文章阅卖网', '', 'http://www.itf4.com', 1, '', 1315218352, 1, '', 3);

INSERT INTO `ljcms_link` VALUES(7, '网址导航', '', 'http://url.itf4.com/', 1, '', 1332724366, 1, '网址导航', 6);

INSERT INTO `ljcms_link` VALUES(8, '北京酒水批发', '', 'http://www.itf4.net/', 1, '', 1339409968, 1, '北京酒水批发', 7);

INSERT INTO `ljcms_log` VALUES(355, 'admin', '127.0.0.1', '编辑导航成功[id=63]', 1, 1402127253);

INSERT INTO `ljcms_log` VALUES(356, 'admin', '127.0.0.1', '编辑导航成功[id=63]', 1, 1402127445);

INSERT INTO `ljcms_log` VALUES(357, 'admin', '127.0.0.1', '编辑导航成功[id=63]', 1, 1402127451);

INSERT INTO `ljcms_log` VALUES(358, 'admin', '127.0.0.1', '编辑导航成功[id=64]', 1, 1402127466);

INSERT INTO `ljcms_log` VALUES(359, 'admin', '127.0.0.1', '编辑导航成功[id=47]', 1, 1402127485);

INSERT INTO `ljcms_log` VALUES(360, 'admin', '127.0.0.1', '编辑导航成功[id=48]', 1, 1402127496);

INSERT INTO `ljcms_log` VALUES(361, 'admin', '127.0.0.1', '编辑导航成功[id=49]', 1, 1402127509);

INSERT INTO `ljcms_log` VALUES(362, 'admin', '127.0.0.1', '编辑导航成功[id=65]', 1, 1402127528);

INSERT INTO `ljcms_log` VALUES(363, 'admin', '127.0.0.1', '编辑导航成功[id=55]', 1, 1402127561);

INSERT INTO `ljcms_log` VALUES(364, 'admin', '127.0.0.1', '编辑导航成功[id=56]', 1, 1402127586);

INSERT INTO `ljcms_log` VALUES(365, 'admin', '127.0.0.1', '编辑导航成功[id=44]', 1, 1402127616);

INSERT INTO `ljcms_log` VALUES(366, 'admin', '127.0.0.1', '编辑导航成功[id=54]', 1, 1402127653);

INSERT INTO `ljcms_log` VALUES(367, 'admin', '127.0.0.1', '编辑导航成功[id=39]', 1, 1402127679);

INSERT INTO `ljcms_log` VALUES(368, 'admin', '127.0.0.1', '编辑导航成功[id=46]', 1, 1402127709);

INSERT INTO `ljcms_log` VALUES(369, 'admin', '127.0.0.1', '添加案例分类成功[]', 1, 1402127769);

INSERT INTO `ljcms_log` VALUES(370, 'admin', '127.0.0.1', '添加案例分类成功[]', 1, 1402127823);

INSERT INTO `ljcms_log` VALUES(371, 'admin', '127.0.0.1', '编辑导航成功[id=40]', 1, 1402127907);

INSERT INTO `ljcms_log` VALUES(372, 'admin', '127.0.0.1', '编辑导航成功[id=53]', 1, 1402127928);

INSERT INTO `ljcms_module` VALUES(1, '文章模型', 'article', '#000000', 'article_index', 'article_list', 'article_detail', 0, 0, 0, 0, 1, NULL);

INSERT INTO `ljcms_module` VALUES(2, '产品模型', 'product', '#008000', 'product_index', 'product_list', 'product_detail', 0, 0, 0, 0, 1, NULL);

INSERT INTO `ljcms_module` VALUES(3, '案例模型', 'photo', '#ff8000', 'photo_index', 'photo_list', 'photo_detail', 0, 0, 0, 0, 1, NULL);

INSERT INTO `ljcms_module` VALUES(4, '下载模型', 'download', '#0000ff', 'download_index', 'download_list', 'download_detail', 0, 0, 0, 0, 1, NULL);

INSERT INTO `ljcms_module` VALUES(5, '招聘模型', 'hr', '#a52a2a', 'hr_index', 'hr_list', 'hr_detail', 0, 0, 0, 0, 1, NULL);

INSERT INTO `ljcms_module` VALUES(6, '单页模型', 'about', '#008080', '', '', 'about_detail', 0, 0, 0, 0, 1, NULL);

INSERT INTO `ljcms_module` VALUES(7, '外部模型', 'outside', '#ff0000', '', '', '', 0, 0, 0, 0, 1, NULL);

INSERT INTO `ljcms_onlinechat` VALUES(1, 1, '业务咨询', '82993936', 'LJcms', 1, 1, 1314844796);

INSERT INTO `ljcms_onlinechat` VALUES(2, 1, '技术支', '65961930', 'LJcms', 2, 1, 1315218622);

INSERT INTO `ljcms_onlinechat` VALUES(4, 3, 'MSN咨询', 'asp3721@hotmail.com', '', 4, 1, 1317050957);

INSERT INTO `ljcms_order` VALUES(6, '订购产品如下：良精商城网店系统<br/>产品的编号为：1329042063', 'fsdffsdfsd', 0, 'sdsdf', '1', 'test', 'testest', '100031', '1321241341', '010-2313131', '1312415241', 'sdfsdfs@dsfsd.com', 1342591877, 1, '127.0.0.1', 'admin', '我们已经发货了', 1343187162);

INSERT INTO `ljcms_order` VALUES(4, '订购产品如下：良精商城网店系统产品的编号为：1329042063', 'sdfdsfsfsfsdfsdfsdf', 1, 'adminsa', '1', 'tsetse', 'testest', '100031', '1321241341', '010-2313131', '1312415241', 'sdfsdfs@dsfsd.com', 1342581749, 1, '127.0.0.1', 'admin', 'sdfsdsdfsdfsdfsfsdf', 1343187222);

INSERT INTO `ljcms_order` VALUES(5, '订购产品如下：企业建站系统V8.6<br/>产品的编号为：1329041563', 'dsfsdsdsfsdfsdf', 1, 'adminsa', '1', 'testsete', 'testestest', '100031', '1321241341', '010-2313131', '13718215123', 'sdfsdfs@dsfsd.com', 1342582002, 1, '127.0.0.1', '', NULL, 0);

INSERT INTO `ljcms_order` VALUES(7, '订购产品如下：良精商城网店系统<br/>产品的编号为：1329042063', 'test', 6, 'itf4', '1', '', 'test', '', '', '', '13621388118', 'asp3721@hotmail.com', 1343297387, 1, '192.168.1.104', '', NULL, 0);

INSERT INTO `ljcms_page` VALUES(1, 1, 1, '', 1, 'LJcms介绍', '<p>良精中英文企业网站管理系统V8.6</p>\r\n<p>前台演示 <a href="http://admin.itf4.com/LJv86/Index.html">http://admin.itf4.com/LJv86/Index.html</a></p>\r\n<p>后台演示 <a href="http://admin.itf4.com/LJv86/admin/Admin_Login.asp">http://admin.itf4.com/LJv86/admin/Admin_Login.asp</a></p>\r\n<p>下载地址 <a href="http://www.liangjing.org/down/LJwebFree.rar">http://www.liangjing.org/down/LJwebFree.rar</a></p>\r\n<p>用户名 admin 密码 admin</p>\r\n<p>良精通用网站管理系统V8.6兼容Firefox、Maxthon、TT等常用浏览器！</p>\r\n<p>后台可先切换 动态和静态 asp+html <br />\r\n主要功能模块介绍 <br />\r\n1. 企业信息：发布介绍企业的各类信息，如企业简介、组织机构、营销网络、企业荣誉、联系方式，并可随意增加新的栏目等。 <br />\r\n2. 新闻动态：发布企业新闻和业内资讯，从后台到前台真正实现无限级分类显示，并随意控制显示级数，大大增加信息发布的灵活性。 <br />\r\n3. 产品展示：发布企业产品，按产品类别显示及搜索产品，并可多选产品直接下订单询盘，无限级分类，大大增加信息发布的灵活性。 <br />\r\n4. 下载资源：发布供网站浏览者和客户下载的资料等，如使用手册、销售合同、软件等，无限级分类。 <br />\r\n5. 人力资源：发布招聘信息，人才策略，浏览者可在线递交简历。 <br />\r\n6. 其他信息：相当于无限扩展栏，并可进行无限分类，可以用于发布网站主栏目未归类的信息，如解决方案、成功案例、购买流程等。 <br />\r\n7. 会员中心：会员可任意设置级别，并可根据级别限制浏览相关内容，会员机制与订购、应聘、留言三大模块有机结合的，我们充分考虑到了网站访问者的惰性，所以会员机制与三大模块又可完全脱离，即未注册也同样能留言、下订单、递交简历。 <br />\r\n8. 留言反馈：以留<br />\r\n</p>', 1, 0, 1, 1314267244, 'LJcms介绍', 'LJcms介绍', 'LJcms介绍', '', '');

INSERT INTO `ljcms_pagecate` VALUES(1, '单页分类一', '', '', '', '', 0, 0, 1, 1, '', 1314262501, 0, '', '', 1, '', 1);

INSERT INTO `ljcms_product` VALUES(1, 5, '1329041563', '企业建站系统V8.6', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 'data/attachment/201202/12/bd77ef11371f56626094951b088ac5ef.jpg.thumb.jpg', 'data/attachment/201202/12/bd77ef11371f56626094951b088ac5ef.jpg', '', '', '', '', '', '', '', '', '演示地址：http://admin.itf4.com/LJv86/Index.html\r\n后台测试：http://admin.itf4.com/LJv86/admin/Admin_Login.asp\r\n下载地址：http://www.liangjing.org/down/LJwebFree.rar', '&nbsp;<p>良精中文企业网站管理系统V8.6</p>\r\n<p>前台演示 <a href="http://admin.itf4.com/LJv86/Index.html">http://admin.itf4.com/LJv86/Index.html</a></p>\r\n<p>后台演示 <a href="http://admin.itf4.com/LJv86/admin/Admin_Login.asp">http://admin.itf4.com/LJv86/admin/Admin_Login.asp</a></p>\r\n<p>下载地址 <a href="http://www.liangjing.org/down/LJwebFree.rar">http://www.liangjing.org/down/LJwebFree.rar</a></p>\r\n<p>用户 admin 密码 admin</p>\r\n<p>良精通用网站管理系统V8.6兼容Firefox、Maxthon、TT等常用浏览器</p>\r\n<p>后台可先切换 动态和静 asp+html <br />\r\n主要功能模块介绍 <br />\r\n1. 企业信息：发布介绍企业的各类信息，如企业简介、组织机构、营销网络、企业荣誉、联系方式，并可随意增加新的栏目等 <br />\r\n2. 新闻动态：发布企业新闻和业内资讯，从后台到前台真正实现无限级分类显示，并随意控制显示级数，大大增加信息发布的灵活性 <br />\r\n3. 产品展示：发布企业产品，按产品类别显示及搜索产品，并可多选产品直接下订单询盘，无限级分类，大大增加信息发布的灵活性 <br />\r\n4. 下载资源：发布供网站浏览者和客户下载的资料等，如使用手册、销售合同、软件等，无限级分类 <br />\r\n5. 人力资源：发布招聘信息，人才策略，浏览者可在线递交简历 <br />\r\n6. 其他信息：相当于无限扩展栏，并可进行无限分类，可以用于发布网站主栏目未归类的信息，如解决方案、成功案例、购买流程等 <br />\r\n7. 会员中心：会员可任意设置级别，并可根据级别限制浏览相关内容，会员机制与订购、应聘、留言三大模块有机结合的，我们充分考虑到了网站访问者的惰性，所以会员机制与三大模块又可完全脱离，即未注册也同样能留言、下订单、递交简历 <br />\r\n</p>', '0.00', '0.00', '', '', 2, 0, 0, 0, 1, 0, '', 1329041563, 0, '');

INSERT INTO `ljcms_product` VALUES(2, 5, '1329041609', '企业建站系统V7.4', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 'data/attachment/201202/12/b692aaf0a247536bf55054aacf34b213.jpg.thumb.jpg', 'data/attachment/201202/12/b692aaf0a247536bf55054aacf34b213.jpg', '', '', '', '', '', '', '', '', '', '&nbsp;<p>良精中文企业网站管理系统V7.4</p>\r\n<p>前台演示 <a href="http://admin.itf4.com/LJV74/index.html">http://admin.itf4.com/LJV74/index.html</a></p>\r\n<p>后台演示 <a href="http://admin.itf4.com/LJV74/admin/admin_login.asp">http://admin.itf4.com/LJV74/admin/admin_login.asp</a></p>\r\n<p>下载地址 <a href="http://www.liangjing.org/down/LJwebFree.rar">http://www.liangjing.org/down/LJwebFree.rar</a></p>\r\n<p>用户 admin 密码 admin</p>\r\n<p>良精通用网站管理系统V7.4 兼容Firefox、Maxthon、TT等常用浏览器</p>\r\n<p>后台可先切换 动态和静 asp+html <br />\r\n主要功能模块介绍 <br />\r\n1. 企业信息：发布介绍企业的各类信息，如企业简介、组织机构、营销网络、企业荣誉、联系方式，并可随意增加新的栏目等 <br />\r\n2. 新闻动态：发布企业新闻和业内资讯，从后台到前台真正实现无限级分类显示，并随意控制显示级数，大大增加信息发布的灵活性 <br />\r\n3. 产品展示：发布企业产品，按产品类别显示及搜索产品，并可多选产品直接下订单询盘，无限级分类，大大增加信息发布的灵活性 <br />\r\n4. 下载资源：发布供网站浏览者和客户下载的资料等，如使用手册、销售合同、软件等，无限级分类 <br />\r\n5. 人力资源：发布招聘信息，人才策略，浏览者可在线递交简历 <br />\r\n6. 其他信息：相当于无限扩展栏，并可进行无限分类，可以用于发布网站主栏目未归类的信息，如解决方案、成功案例、购买流程等 <br />\r\n7. 会员中心：会员可任意设置级别，并可根据级别限制浏览相关内容，会员机制与订购、应聘、留言三大模块有机结合的，我们充分考虑到了网站访问者的惰性，所以会员机制与三大模块又可完全脱离，即未注册也同样能留言、下订单、递交简历 <br />\r\n</p>', '0.00', '0.00', '', '', 8, 0, 0, 0, 1, 0, '', 1329041609, 0, '');

INSERT INTO `ljcms_product` VALUES(3, 5, '1329041637', '山寨i5 入门版V1', '山寨i5 入门版V1', '山寨i5 入门版V1', '山寨i5 入门版V1', '', 'data/attachment/201301/09/cf7899fcf17a626a05dc55e15421db8f.jpg.thumb.jpg', 'data/attachment/201301/09/cf7899fcf17a626a05dc55e15421db8f.jpg', '', '', '', '', '', '', '', '', '山寨i5 入门版V1', '&nbsp;<p>良精中文企业网站管理系统V8.5</p>\r\n<p>前台演示 <a href="http://admin.itf4.com/LJCMSV85/index.html">http://admin.itf4.com/LJCMSV85/index.html</a></p>\r\n<p>后台演示 <a href="http://admin.itf4.com/LJCMSV85/admin/Admin_Login.asp">http://admin.itf4.com/LJCMSV85/admin/Admin_Login.asp</a></p>\r\n<p>下载地址 <a href="http://www.liangjing.org/down/LJwebFree.rar">http://www.liangjing.org/down/LJwebFree.rar</a></p>\r\n<p>用户 admin 密码 admin</p>\r\n<p>良精通用网站管理系统V8.5 兼容Firefox、Maxthon、TT等常用浏览器</p>\r\n<p>后台可先切换 动态和静 asp+html <br />\r\n主要功能模块介绍 <br />\r\n1. 企业信息：发布介绍企业的各类信息，如企业简介、组织机构、营销网络、企业荣誉、联系方式，并可随意增加新的栏目等 <br />\r\n2. 新闻动态：发布企业新闻和业内资讯，从后台到前台真正实现无限级分类显示，并随意控制显示级数，大大增加信息发布的灵活性 <br />\r\n3. 产品展示：发布企业产品，按产品类别显示及搜索产品，并可多选产品直接下订单询盘，无限级分类，大大增加信息发布的灵活性 <br />\r\n4. 下载资源：发布供网站浏览者和客户下载的资料等，如使用手册、销售合同、软件等，无限级分类 <br />\r\n5. 人力资源：发布招聘信息，人才策略，浏览者可在线递交简历 <br />\r\n6. 其他信息：相当于无限扩展栏，并可进行无限分类，可以用于发布网站主栏目未归类的信息，如解决方案、成功案例、购买流程等 <br />\r\n7. 会员中心：会员可任意设置级别，并可根据级别限制浏览相关内容，会员机制与订购、应聘、留言三大模块有机结合的，我们充分考虑到了网站访问者的惰性，所以会员机制与三大模块又可完全脱离，即未注册也同样能留言、下订单、递交简历 <br />\r\n</p>', '999.00', '1200.00', '6868668868', '', 8, 0, 0, 1, 1, 0, '', 1329041637, 0, '');

INSERT INTO `ljcms_product` VALUES(4, 5, '1329041665', '企业建站系统V7.3', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 'data/attachment/201202/12/d99a003b376fb3133da1d004d823a2de.jpg.thumb.jpg', 'data/attachment/201202/12/d99a003b376fb3133da1d004d823a2de.jpg', '', '', '', '', '', '', '', '', '', '&nbsp;<p>良精中文企业网站管理系统V7.3</p>\r\n<p>前台演示 <a href="http://admin.itf4.com/LJV73/index.html">http://admin.itf4.com/LJV73/index.html</a></p>\r\n<p>后台演示 <a href="http://admin.itf4.com/LJV73/admin/admin_login.asp">http://admin.itf4.com/LJV73/admin/admin_login.asp</a></p>\r\n<p>下载地址 <a href="http://www.liangjing.org/down/LJwebFree.rar">http://www.liangjing.org/down/LJwebFree.rar</a></p>\r\n<p>用户 admin 密码 admin</p>\r\n<p>良精通用网站管理系统V7.3 兼容Firefox、Maxthon、TT等常用浏览器</p>\r\n<p>后台可先切换 动态和静 asp+html <br />\r\n主要功能模块介绍 <br />\r\n1. 企业信息：发布介绍企业的各类信息，如企业简介、组织机构、营销网络、企业荣誉、联系方式，并可随意增加新的栏目等 <br />\r\n2. 新闻动态：发布企业新闻和业内资讯，从后台到前台真正实现无限级分类显示，并随意控制显示级数，大大增加信息发布的灵活性 <br />\r\n3. 产品展示：发布企业产品，按产品类别显示及搜索产品，并可多选产品直接下订单询盘，无限级分类，大大增加信息发布的灵活性 <br />\r\n4. 下载资源：发布供网站浏览者和客户下载的资料等，如使用手册、销售合同、软件等，无限级分类 <br />\r\n5. 人力资源：发布招聘信息，人才策略，浏览者可在线递交简历 <br />\r\n6. 其他信息：相当于无限扩展栏，并可进行无限分类，可以用于发布网站主栏目未归类的信息，如解决方案、成功案例、购买流程等 <br />\r\n7. 会员中心：会员可任意设置级别，并可根据级别限制浏览相关内容，会员机制与订购、应聘、留言三大模块有机结合的，我们充分考虑到了网站访问者的惰性，所以会员机制与三大模块又可完全脱离，即未注册也同样能留言、下订单、递交简历 <br />\r\n</p>', '0.00', '0.00', '', '', 6, 0, 0, 0, 1, 0, '', 1329041665, 0, '');

INSERT INTO `ljcms_product` VALUES(5, 6, '1329041706', '旅游建站系统V1.3', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 'data/attachment/201202/12/e987b421545a57f63766d1c850866fe5.jpg.thumb.jpg', 'data/attachment/201202/12/e987b421545a57f63766d1c850866fe5.jpg', '', '', '', '', '', '', '', '', '', '<div style="line-height:24px;display:block;" id="con1">\r\n<p><span style="font-family:Verdana;">良精旅游建站系统1.3</span></p>\r\n<p>演示地址　<span style="font-family:Verdana;"><a href="http://admin.itf4.com/lvyou3/Index.html">http://admin.itf4.com/lvyou3/Index.html</a></span></p>\r\n<p>后台测试 <span style="font-family:Verdana;"><a href="http://admin.itf4.com/lvyou3/admin/Admin_Login.asp">http://admin.itf4.com/lvyou3/admin/Admin_Login.asp</a></span></p>\r\n<p>用户 admin 密码 admin</p>\r\n</div>', '0.00', '0.00', '', '', 2, 0, 0, 0, 1, 0, '', 1329041706, 0, '');

INSERT INTO `ljcms_product` VALUES(6, 6, '1329041762', '剧团建站系统V1.0', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 'data/attachment/201202/12/2d3d8a34c32f40eeab8ab8f2e8460c7f.jpg.thumb.jpg', 'data/attachment/201202/12/2d3d8a34c32f40eeab8ab8f2e8460c7f.jpg', '', '', '', '', '', '', '', '', '', '<div style="line-height:24px;display:block;" id="con1">\r\n<div style="line-height:24px;display:block;" id="con1"><span style="font-family:Verdana;"> <div style="line-height:24px;display:block;" id="con1">\r\n<p><span style="font-family:Verdana;">良精剧团建站系统V1.0</span></p>\r\n<p>演示地址　<a href="http://admin.itf4.com/jutuan/index.html">http://admin.itf4.com/jutuan/index.html</a></p>\r\n<p>后台测试 <a href="http://admin.itf4.com/jutuan/admin/admin_login.asp">http://admin.itf4.com/jutuan/admin/admin_login.asp</a></p>\r\n<p><a href="http://www.liangjing.org/down/LJwebFree.rar">下载地址 http://www.liangjing.org/down/LJwebFree.rar</a></p>\r\n<p>用户 admin 密码 admin</p>\r\n</div>\r\n</span></div>\r\n</div>', '0.00', '0.00', '', '', 1, 0, 0, 0, 1, 0, '', 1329041762, 0, '');

INSERT INTO `ljcms_product` VALUES(7, 6, '1329041784', '政府建站系统 V2.1', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 'data/attachment/201301/09/b3e125df204f5fb33145bc2ebac719bb.jpg.thumb.jpg', 'data/attachment/201301/09/b3e125df204f5fb33145bc2ebac719bb.jpg', '', '', '', '', '', '', '', '', '', '<p>良精政府网站管理系统 V2.1</p>\r\n<p>演示地址：http://admin.itf4.com/zf21</p>\r\n<p><br />\r\n后台测试：http://admin.itf4.com/zf21/admin</p>\r\n<p><br />\r\n下载地址：http://www.liangjing.org/down/LJwebFree.rar</p>\r\n<p><br />\r\n后台测试：用户名 admin 密码 admin</p>', '999.00', '1600.00', '6868668868', '', 4, 0, 0, 0, 1, 0, '', 1329041784, 0, '');

INSERT INTO `ljcms_product` VALUES(8, 6, '1329041803', '政府建站系统 V2.0', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 'data/attachment/201202/12/2a0b2bea856bb073c670b7b1877fb4fa.jpg.thumb.jpg', 'data/attachment/201202/12/2a0b2bea856bb073c670b7b1877fb4fa.jpg', '', '', '', '', '', '', '', '', '', '<p>良精政府网站管理系统 V2.0</p>\r\n<p>演示地址<a href="http://admin.itf4.com/zf02">http://admin.itf4.com/zf02</a></p>\r\n<p><br />\r\n后台测试<a href="http://admin.itf4.com/zf02/admin">http://admin.itf4.com/zf02/admin</a><br />\r\n</p>\r\n<p>下载地址<a href="http://www.liangjing.org/down/LJwebFree.rar">http://www.liangjing.org/down/LJwebFree.rar</a></p>\r\n<p><br />\r\n后台测试：用户名 admin 密码 admin</p>', '0.00', '0.00', '6868668868', '>=', 2, 0, 0, 0, 1, 0, '', 1329041803, 0, '');

INSERT INTO `ljcms_product` VALUES(9, 7, '1329041925', '剪卡器', '剪卡器', '剪卡器', '剪卡器', '', 'data/attachment/201301/09/b90518f0dae02a8920507f873794e6a3.jpg.thumb.jpg', 'data/attachment/201301/09/b90518f0dae02a8920507f873794e6a3.jpg', '', '', '', '', '', '', '', '', '', '<p><span style="font-family:Verdana;">良精<span style="font-family:Verdana;">中英文网店系统V2.2</span></span></p>\r\n<p><span style="font-family:Verdana;">前台演示 <a href="http://admin.itf4.com/LJshopV22/Ch/index.html">http://admin.itf4.com/LJshopV22/Ch/index.html</a></span></p>\r\n<p><span style="font-family:Verdana;">后台演示 <a href="http://admin.itf4.com/LJshopV22/admin/Admin_Login.asp">http://admin.itf4.com/LJshopV22/admin/Admin_Login.asp</a></span></p>\r\n<p><span style="font-family:Verdana;">下载地址 <a href="http://www.liangjing.org/down/LJshop20091216.rar">http://www.liangjing.org/down/LJshop20091216.rar</a></span></p>\r\n<p><span style="font-family:Verdana;">用户 admin 密码 admin</span></p>\r\n<p><span style="font-family:Verdana;">后台可先切换 动态和静 asp+html+ACCESS </span></p>\r\n<p><span style="font-family:Verdana;">在线实时支付 兼容Firefox、Maxthon、TT等常用浏览器</span></p>\r\n<p><span style="font-family:Verdana;">不含SQL数据 如需SQL数据库加 000<br />\r\n主要功能模块介绍 <br />\r\n1. 企业信息：发布介绍企业的各类信息，如企业简介、组织机构、营销网络、企业荣誉、联系方式，并可随意增加新的栏目等 <br />\r\n2. 新闻动态：发布企业新闻和业内资讯，从后台到前台真正实现无限级分类显示，并随意控制显示级数，大大增加信息发布的灵活性 <br />\r\n3. 商品展示：发布企业商品，按商品类别显示及搜索商品，并可多选商品直接下订单询盘，无限级分类，大大增加信息发布的灵活性 <br />\r\n4. 下载资源：发布供网站浏览者和客户下载的资料等，如使用手册、销售合同、软件等，无限级分类 <br />\r\n5. 人力资源：发布招聘信息，人才策略，浏览者可在线递交简历 <br />\r\n6. 其他信息：相当于无限扩展栏，并可进行无限分类，可以用于发布网站主栏目未归类的信息，如解决方案、成功案例、购买流程等 <br />\r\n7. 会员中心：会员可任意设置级别，并可根据级别限制浏览相关内容，会员机制与订购、应聘、留言三大模块有机结合的，我们充分考虑到了网站访问者的惰性，所以会员机制与三大模块又可完全脱离，即未注册也同样能留言、下订单、递交简历 <br />\r\n8. 留言反馈：以留言板的模式让有意见和建议的浏览者反馈回来，可设悄悄话留言方式，可设默认是否通过审核后显示留言<br />\r\n</span></p>', '20.00', '0.00', '6868668868', '', 6, 1, 0, 0, 1, 0, '', 1329041925, 0, '');

INSERT INTO `ljcms_product` VALUES(10, 7, '1329041956', '充电器', '充电器', '充电器', '充电器', '', 'data/attachment/201301/09/096eec98efda7c91f37ae9973d092718.jpg.thumb.jpg', 'data/attachment/201301/09/096eec98efda7c91f37ae9973d092718.jpg', '', '', '', '', '', '', '', '', '充电器', '<div style="line-height:24px;display:block;" id="con1">\r\n<p><span style="font-family:Verdana;">良精<span style="font-family:Verdana;">中英文网店系统V2.0</span></span></p>\r\n<p><span style="font-family:Verdana;">前台演示 http://admin.itf4.com/LJshopV20/Ch/index.html</span></p>\r\n<p><span style="font-family:Verdana;">后台演示 http://admin.itf4.com/LJshopV20/admin/Admin_Login.asp</span></p>\r\n<p><span style="font-family:Verdana;">下载地址 http://www.liangjing.org/down/LJshop20091216.rar</span></p>\r\n<p><span style="font-family:Verdana;">用户 admin 密码 admin</span></p>\r\n<p><span style="font-family:Verdana;">后台可先切换 动态和静 asp+html+ACCESS </span></p>\r\n<p><span style="font-family:Verdana;">在线实时支付 兼容Firefox、Maxthon、TT等常用浏览器</span></p>\r\n<p><span style="font-family:Verdana;">不含SQL数据 如需SQL数据库加 000<br />\r\n主要功能模块介绍 <br />\r\n1. 企业信息：发布介绍企业的各类信息，如企业简介、组织机构、营销网络、企业荣誉、联系方式，并可随意增加新的栏目等 <br />\r\n2. 新闻动态：发布企业新闻和业内资讯，从后台到前台真正实现无限级分类显示，并随意控制显示级数，大大增加信息发布的灵活性 <br />\r\n3. 商品展示：发布企业商品，按商品类别显示及搜索商品，并可多选商品直接下订单询盘，无限级分类，大大增加信息发布的灵活性 <br />\r\n4. 下载资源：发布供网站浏览者和客户下载的资料等，如使用手册、销售合同、软件等，无限级分类 <br />\r\n5. 人力资源：发布招聘信息，人才策略，浏览者可在线递交简历 <br />\r\n6. 其他信息：相当于无限扩展栏，并可进行无限分类，可以用于发布网站主栏目未归类的信息，如解决方案、成功案例、购买流程等 <br />\r\n7. 会员中心：会员可任意设置级别，并可根据级别限制浏览相关内容，会员机制与订购、应聘、留言三大模块有机结合的，我们充分考虑到了网站访问者的惰性，所以会员机制与三大模块又可完全脱离，即未注册也同样能留言、下订单、递交简历 <br />\r\n8. 留言反馈：以留言板的模式让有意见和建议的浏览者反馈回来，可设悄悄话留言方式，可设默认是否通过审核后显示留言<br />\r\n</span></p>\r\n</div>', '20.00', '0.00', '6868668868', '', 11, 1, 0, 0, 1, 0, '', 1329041956, 0, '');

INSERT INTO `ljcms_product` VALUES(11, 7, '1329041986', '保护膜', '保护膜', '保护膜', '保护膜', '', 'data/attachment/201301/09/90f38ee81d16b5380ba777a989d463df.jpg.thumb.jpg', 'data/attachment/201301/09/90f38ee81d16b5380ba777a989d463df.jpg', '', '', '', '', '', '', '', '', '', '<div style="line-height:24px;display:block;" id="con1">建一个网站，就像修改QQ个资料一样方便 <p>演示地址 <span style="font-family:Verdana;"><a href="http://admin.asp99.cn/LJhtmlShopV3/index.html">http://admin.asp99.cn/LJhtmlShopV3/index.html</a></span></p>\r\n<p>后台演示 <span style="font-family:Verdana;"><a href="http://admin.asp99.cn/shop2009/admin/index.asp">http://admin.asp99.cn/shop2009/admin/index.asp</a></span></p>\r\n<p>下载地址 <span style="font-family:Verdana;"><a href="http://www.asp99.cn/ljnetshop.rar"><span style="font-family:Verdana;">http://www.liangjing.org/down/LJshop20091216.rar</span></a></span></p>\r\n<p>分流下载 <span style="font-family:Verdana;"><a href="http://down.admin5.com/code_asp/17681.html">http://down.admin5.com/code_asp/17681.html</a></span></p>\r\n<p>后台用户名密码admin<br />\r\n<span style="font-family:Verdana;">良精网店购物系统是一套能够适合不同类型商品、超强灵活的多功能在线商店系统，为您提供了一个完整的在线开店解决方案。良精网店购物系统除了拥有一般网上商店系统所具有的所有功能，还拥有着其它网店系统没有的许多超强功能。多种独创的技术使得系统能满足各行业广大用户的各种各样的需求，是一个经过完善设计并适用于各种服务器环境的高效、全新、快速和优秀的网上购物软件解决方案</span></p>\r\n<p><span style="font-family:Verdana;">良精网店购物系统无论在稳定性、代码优化、运行效率、负载能力、安全等级、功能可操控性和权限严密性等方面都居国内外同类产品领先地位。经过长期创新性开发，成为目前国内最具性价比、最受欢迎的网上购物软件之一。如果您寻求一款能按您的想法随意发挥的网上购物软件，那么良精网店购物系统将是您最佳的选择</span></p>\r\n<p><span style="font-family:Verdana;">良精网店商城系统是向中小企业及个人快速构建个性化网上商店,网店系统，是目前国内最受欢迎的网店系统之一。其安全实用的商品管理、订单管理、销售管理、客户和代理商管理，批发管理、支付管理、模板管理等众多强大功能，让您可以快速低成本地构建个性化网上商店</span></p>\r\n</div>', '10.00', '0.00', '6868668868', '', 11, 1, 0, 0, 1, 0, '', 1329041986, 0, '');

INSERT INTO `ljcms_product` VALUES(12, 7, '1329042008', '耳机', '耳机', '耳机', '耳机', '', 'data/attachment/201301/09/160f2f21b0041be9852ae17adff014c5.jpg.thumb.jpg', 'data/attachment/201301/09/160f2f21b0041be9852ae17adff014c5.jpg', '', '', '', '', '', '', '', '', '', '<div style="line-height:24px;display:block;" id="con1">\r\n<p><span style="font-family:Verdana;">建一个网站，就像修改QQ个资料一样方便 <br />\r\n演示地址 <a href="http://admin.asp99.cn/LJhtmlShopV2/index.html">http://admin.asp99.cn/LJhtmlShopV2/index.html</a></span></p>\r\n<p><span style="font-family:Verdana;">后台演示 http://admin.asp99.cn/shop2009/admin/index.asp</span></p>\r\n<p><span style="font-family:Verdana;">下载地址 <span style="font-family:Verdana;">http://www.liangjing.org/down/LJshop20091216.rar</span></span></p>\r\n<p><span style="font-family:Verdana;">后台用户名密码admin<br />\r\n良精网店购物系统是一套能够适合不同类型商品、超强灵活的多功能在线商店系统，为您提供了一个完整的在线开店解决方案。良精网店购物系统除了拥有一般网上商店系统所具有的所有功能，还拥有着其它网店系统没有的许多超强功能。多种独创的技术使得系统能满足各行业广大用户的各种各样的需求，是一个经过完善设计并适用于各种服务器环境的高效、全新、快速和优秀的网上购物软件解决方案</span></p>\r\n<p><span style="font-family:Verdana;">良精网店购物系统无论在稳定性、代码优化、运行效率、负载能力、安全等级、功能可操控性和权限严密性等方面都居国内外同类产品领先地位。经过长期创新性开发，成为目前国内最具性价比、最受欢迎的网上购物软件之一。如果您寻求一款能按您的想法随意发挥的网上购物软件，那么良精网店购物系统将是您最佳的选择</span></p>\r\n<p><span style="font-family:Verdana;">良精网店商城系统是向中小企业及个人快速构建个性化网上商店,网店系统，是目前国内最受欢迎的网店系统之一。其安全实用的商品管理、订单管理、销售管理、客户和代理商管理，批发管理、支付管理、模板管理等众多强大功能，让您可以快速低成本地构建个性化网上商店<br />\r\n</span></p>\r\n</div>', '30.00', '0.00', '6868668868', '', 13, 1, 0, 0, 1, 0, '', 1329042008, 0, '');

INSERT INTO `ljcms_product` VALUES(16, 3, '1357709552', '数据线', '数据线', '数据线', '数据线', '', 'data/attachment/201301/09/7650a56906400f372acc78e46a9af617.jpg.thumb.jpg', 'data/attachment/201301/09/7650a56906400f372acc78e46a9af617.jpg', '', '', '', '', '', '', '', '', '数据线', '数据线', '10.00', '20.00', '', '>=', 202, 0, 0, 0, 1, 0, '', 1357709552, 0, '数据线');

INSERT INTO `ljcms_product` VALUES(13, 7, '1329042037', '良精商城网店系统V1.1', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 'data/attachment/201202/12/48960354a46969d246a1cd9af990e03a.jpg.thumb.jpg', 'data/attachment/201202/12/48960354a46969d246a1cd9af990e03a.jpg', '', '', '', '', '', '', '', '', '', '<table class="tf ke-zeroborder" border="0" width="98%">\r\n<tbody>\r\n<tr>\r\n<td class="bw"><span class="htd">演示地址&nbsp;<img border="0" align="absMiddle" src="http://www.asp99.net/skin/skin_3/small/url.gif" /><a href="http://admin.asp99.cn/newshop/" target="_blank">http://admin.asp99.cn/newshop/</a><br />\r\n<br />\r\n后台测试&nbsp;<img border="0" align="absMiddle" src="http://www.asp99.net/skin/skin_3/small/url.gif" /><a href="http://admin.asp99.cn/newshop/admin.asp" target="_blank">http://admin.asp99.cn/newshop/admin.asp</a><br />\r\n<br />\r\n下载地址&nbsp;<img border="0" align="absMiddle" src="http://www.asp99.net/skin/skin_3/small/url.gif" /><a href="http://down.asp99.cn/shop20061220.rar" target="_blank"><span style="font-family:Verdana;">http://www.liangjing.org/down/LJshop20091216.rar</span></a>&nbsp;<br />\r\n后台测试&nbsp;帐号&nbsp;密码admin</span></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p><span style="font-family:Verdana;">良精网店购物系统是一套能够适合不同类型商品、超强灵活的多功能在线商店系统，为您提供了一个完整的在线开店解决方案。良精网店购物系统除了拥有一般网上商店系统所具有的所有功能，还拥有着其它网店系统没有的许多超强功能。多种独创的技术使得系统能满足各行业广大用户的各种各样的需求，是一个经过完善设计并适用于各种服务器环境的高效、全新、快速和优秀的网上购物软件解决方案</span></p>\r\n<p><span style="font-family:Verdana;">良精网店购物系统无论在稳定性、代码优化、运行效率、负载能力、安全等级、功能可操控性和权限严密性等方面都居国内外同类产品领先地位。经过长期创新性开发，成为目前国内最具性价比、最受欢迎的网上购物软件之一。如果您寻求一款能按您的想法随意发挥的网上购物软件，那么良精网店购物系统将是您最佳的选择</span></p>\r\n<p><span style="font-family:Verdana;">良精网店商城系统是向中小企业及个人快速构建个性化网上商店,网店系统，是目前国内最受欢迎的网店系统之一。其安全实用的商品管理、订单管理、销售管理、客户和代理商管理，批发管理、支付管理、模板管理等众多强大功能，让您可以快速低成本地构建个性化网上商店<br />\r\n</span></p>', '0.00', '0.00', '', '', 28, 0, 0, 0, 1, 0, '', 1329042037, 0, '');

INSERT INTO `ljcms_product` VALUES(14, 7, '1329042063', '良精商城网店系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 'data/attachment/201202/12/673643d43790a1d95e34a2abf55beb63.jpg.thumb.jpg', 'data/attachment/201202/12/673643d43790a1d95e34a2abf55beb63.jpg', '', '', '', '', '', '', '', '', '', '<div style="line-height:24px;display:block;" id="con1">\r\n<p>&nbsp;商城网店系统正式<br />\r\n<br />\r\n建一个网站，就像修改QQ个资料一样方便</p>\r\n<p><span style="font-size:18px;"><span style="font-family:宋体;font-size:18px;">后台用户名密码admin</span><br />\r\n</span>多风格商城功能太强大了</p>\r\n<p><span style="font-family:Verdana;">良精网店购物系统是一套能够适合不同类型商品、超强灵活的多功能在线商店系统，为您提供了一个完整的在线开店解决方案。良精网店购物系统除了拥有一般网上商店系统所具有的所有功能，还拥有着其它网店系统没有的许多超强功能。多种独创的技术使得系统能满足各行业广大用户的各种各样的需求，是一个经过完善设计并适用于各种服务器环境的高效、全新、快速和优秀的网上购物软件解决方案</span></p>\r\n<p><span style="font-family:Verdana;">良精网店购物系统无论在稳定性、代码优化、运行效率、负载能力、安全等级、功能可操控性和权限严密性等方面都居国内外同类产品领先地位。经过长期创新性开发，成为目前国内最具性价比、最受欢迎的网上购物软件之一。如果您寻求一款能按您的想法随意发挥的网上购物软件，那么良精网店购物系统将是您最佳的选择</span></p>\r\n<p><span style="font-family:Verdana;">良精网店商城系统是向中小企业及个人快速构建个性化网上商店,网店系统，是目前国内最受欢迎的网店系统之一。其安全实用的商品管理、订单管理、销售管理、客户和代理商管理，批发管理、支付管理、模板管理等众多强大功能，让您可以快速低成本地构建个性化网上商店</span></p>\r\n</div>', '0.00', '0.00', '2147483647', '>=', 133, 0, 0, 0, 1, 0, '', 1329042063, 0, '');

INSERT INTO `ljcms_product` VALUES(15, 5, '1357539505', '山寨i4S', '山寨i4S', '山寨i4S', '山寨i4S', '', 'data/attachment/201301/09/d9d297d8ad80b432f877e54b62aaabb1.jpg.thumb.jpg', 'data/attachment/201301/09/d9d297d8ad80b432f877e54b62aaabb1.jpg', '', '', '', '', '', '', '', '', '良精企业网站管理系统V9.0正式发布 全站采用div+css完全符合搜索引擎的标准,更让搜索引擎更喜欢的模式。', '<p>8.5程序基础上增加到三个模板</p>\r\n<p>1、更新幻灯不能随便控制大小&nbsp;&nbsp; 2011-11-15<br />\r\n2、新增加导航点击改变图标提示&nbsp;&nbsp; 2011-11-15<br />\r\n3、修改后台不能直接上传图片死机状态 2011-11-15</p>\r\n<p>8.5程序基础上增加到三个模板 2011-12-12</p>\r\n<p>1、更新前台首页信息全部删除出现混乱。2011-12-11<br />\r\n2、更新前台产品图片大小不一补丁。2011-12-11<br />\r\n3、更新友情连接后台选择图片连接不显示问题。2011-12-12<br />\r\n4、更新第三个模板 2011-12-12</p>\r\n<p>8.6程序更新 2011-12-16</p>\r\n<p>1、更新了后台验证码不能隐藏Bug<br />\r\n2、模板3不能显示QQ客服和留言公告首页弹出等问题.</p>\r\n<p>8.6程序更新 2011-12-19</p>\r\n<p>1、前台删除完新闻混乱问题。<br />\r\n2、模板3首页产品列表名字过长问题。<br />\r\n3、修复后台招聘和下载静态名字不能自动获得bug.</p>\r\n<p>修改<br />\r\n(1)有中英文版本<br />\r\n(2)banner 下来一行小图片 要滚动幻灯显示最新产品的图片共5张<br />\r\n(3)企业咨询可以显示多行，用户在后台设定显示行数<br />\r\n(4)新产品展示可以显示多行，用户在后台设定显示行数<br />\r\n(5)服务电话可以显示多行多个<br />\r\n(6)邮箱可以显示多行多个<br />\r\n(7)在线QQ可以显示多个<br />\r\n(8)留言薄改名字为服务咨询，且在后台审核通过后才可以显示给游客或者用户看<br />\r\n(9)在线购物功能完全屏蔽<br />\r\n(10)产品详细介绍页面除了产品概述和功能特点，另外加一个相关资料下载，并在后台可以添加上传相关文件如 rar Jpg PDF 等合法安全文件<br />\r\n(11)鼠标经过产品图片可以显示高清大图<br />\r\n(12)关于我们，里面搞一个企业画册，类似QQ空间相册<br />\r\n(13)产品图片大小采用4:3，产品缩略图能自动生成，不需要上传产品小图<br />\r\n(14)迁移旧网站数据<br />\r\n(15)后台可以自己加入在线客服代码</p>\r\n<p>良精CMS网站管理系统前台模版分离 打破以前非分离模版 <br />\r\n例子：<br />\r\n所有模版文件都在template文件夹下<br />\r\n如头部文件：<br />\r\n如： <br />\r\n风格1 那么为 style1 文件夹<br />\r\n风格2 那么为 style2 文件夹</p>\r\n<p>依次可以放n个模板<br />\r\n然后在后台选择</p>\r\n<p>网站标题&nbsp;&nbsp; {Sys:SiteTitle}<br />\r\n网站关键字 {Sys:Keywords}<br />\r\n网站描述&nbsp; {Sys:Descriptions}</p>\r\n<p><br />\r\n头部标签<br />\r\n{Sys:Header}</p>\r\n<p>底部标签 <br />\r\n{Sys:Footer}</p>\r\n<p><br />\r\n页面幻灯 {main:LiangjingCMSSlide}<br />\r\n首页新闻 ： {main:LiangjingCMSIndexNews}<br />\r\n底部导航： {main:LiangjingCMSFootNavigation}<br />\r\n企业信息导航: {main:LiangjingCMSAboutFolder}<br />\r\nQQ客服: {main:ShowQQkefuMessage}</p>\r\n<p>&nbsp;</p>\r\n<p>公司名称 ： {Sys:ComName}<br />\r\n地址 ： {Sys:Address}<br />\r\n公司网址 ： {Sys:SiteUrl}<br />\r\nicp备案 : {Sys:IcpNumber}<br />\r\n网站统计: {Sys:LiangjingCMS_Stat}</p>\r\n<p>&nbsp;</p>\r\n<p>最新程序请访问官方 <a href="http://www.liangjing.org">http://www.liangjing.org</a></p>\r\n<p>在线浏览：<a href="http://server.liangjing.org/">http://server.liangjing.org/</a> 在线帮助或者 访问官方客服QQ 谢谢！！<br />\r\n</p>', '999.00', '1680.00', '6868668868', '>=', 626, 0, 0, 1, 1, 1, '2013年8月13日', 1357539505, 0, '良精企业网站管理系统V9.0,正式发布');

INSERT INTO `ljcms_productcate` VALUES(10, '显卡', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 3, 1, 9, 1, 0, '', 1329037773, '', '', 1, '', 1);

INSERT INTO `ljcms_productcate` VALUES(6, '政府网站源码', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 4, 1, 5, 1, 0, '', 1329037658, '', '', 1, '', 1);

INSERT INTO `ljcms_productcate` VALUES(15, '热水', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 2, 1, 14, 1, 0, '', 1329037824, '', '', 1, '', 1);

INSERT INTO `ljcms_productcate` VALUES(14, '洗衣', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 2, 1, 13, 1, 0, '', 1329037817, '', '', 1, '', 1);

INSERT INTO `ljcms_productcate` VALUES(13, '冰箱', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 2, 1, 12, 1, 0, '', 1329037810, '', '', 1, '', 1);

INSERT INTO `ljcms_productcate` VALUES(12, '电视', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 2, 1, 11, 1, 0, '', 1329037801, '', '', 1, '', 1);

INSERT INTO `ljcms_productcate` VALUES(11, '硬盘', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 3, 1, 10, 1, 0, '', 1329037783, '', '', 1, '', 1);

INSERT INTO `ljcms_productcate` VALUES(7, '商城网店源码', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 4, 1, 6, 1, 0, '', 1329037696, '', '', 1, '', 1);

INSERT INTO `ljcms_productcate` VALUES(8, 'CPU', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 3, 1, 7, 1, 0, '', 1329037747, '', '', 1, '', 1);

INSERT INTO `ljcms_productcate` VALUES(9, '主板', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 3, 1, 8, 1, 0, '', 1329037764, '', '', 1, '', 1);

INSERT INTO `ljcms_productcate` VALUES(5, '企业网站源码', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 4, 1, 4, 1, 0, '', 1329037632, '', '', 1, '', 1);

INSERT INTO `ljcms_productcate` VALUES(4, '网站源码', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 0, 0, 1, 1, 0, '', 1329037579, '', '', 1, '', 1);

INSERT INTO `ljcms_productcate` VALUES(3, '电脑配件', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 0, 0, 2, 1, 0, '', 1329037501, '', '', 1, '', 1);

INSERT INTO `ljcms_productcate` VALUES(2, '家用电器', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 0, 0, 3, 1, 0, '', 1329037465, '', '', 1, '', 1);

INSERT INTO `ljcms_productcate` VALUES(16, '良精源码', '良精源码', '良精源码', '良精源码', '', 0, 0, 15, 1, 0, '良精源码', 1332578443, '', '', 1, '', 1);

INSERT INTO `ljcms_productcate` VALUES(17, '其他源码', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 0, 0, 16, 1, 0, '', 1332578839, '', '', 1, '', 1);

INSERT INTO `ljcms_skin` VALUES(1, '良精蓝色通用', 'blue', 'tpl', 'data/attachment/201206/11/d602cb754e3a8995f85f1bc915118252.jpg', 0, 0, 1313905723, '良精蓝色通用');

INSERT INTO `ljcms_skin` VALUES(6, '谷歌和乐风手机站', 'Ljgoophone', 'html', 'data/attachment/201212/31/cac93a11ef216c1daea12666cc3bc528.jpg', 0, 1, 1356918654, '谷歌和乐风手机站');

INSERT INTO `ljcms_skin` VALUES(3, '良精企业建站V3.5', 'Ljcms1', 'tpl', 'data/attachment/201206/11/fa086afb9f03e15918d577fe1712a5bb.jpg', 0, 0, 1332464415, '良精企业建站V3.5');

INSERT INTO `ljcms_skin` VALUES(4, '良精企业网站3.9', 'Ljcms39', 'tpl', 'data/attachment/201206/11/21222e07c25e43f51d301c79ceaa9027.jpg', 0, 0, 1332901239, '良精企业网站3.9');

INSERT INTO `ljcms_skin` VALUES(5, '良精仿小米官方站', 'LjimitateIm', 'html', 'data/attachment/201206/11/75302241c42a0fabb1df252cc6cbadaa.gif', 0, 0, 1337752082, '良精仿小米官方企业站');

INSERT INTO `ljcms_solution` VALUES(1, 2, '良精科技最新推出php企业网站管理系统', 'data/attachment/201202/12/351b27ec8e7a700b47165be887625cae.jpg.thumb.jpg', 'data/attachment/201202/12/351b27ec8e7a700b47165be887625cae.jpg', '良精科技最新推出php企业网站管理系统', '<p>解决方案1011</p>\r\n<p>解决方案1011</p>\r\n<p>解决方案1011</p>\r\n<p>解决方案1011</p>\r\n<p>解决方案1011</p>\r\n<p>解决方案1011</p>\r\n<p>解决方案1011</p>\r\n<p>解决方案1011</p>', 1329044073, 'admin', 'admin', 1, '6868668868', '', 0, 1, 0, 0, 5, '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 0, 'php企业网站管理系统');

INSERT INTO `ljcms_solution` VALUES(2, 2, '公司企业相关问题的解决方案编号102', '', '', '', '<p>公司企业相关问题的解决方案编号102</p>\r\n<p>公司企业相关问题的解决方案编号102</p>\r\n<p>公司企业相关问题的解决方案编号102</p>', 1329044109, '', '', 1, '', '', 0, 1, 0, 0, 4, '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 0, '');

INSERT INTO `ljcms_solution` VALUES(3, 2, '公司企业相关问题的解决方案编号103', '', '', '', '<p>公司企业相关问题的解决方案编号103</p>\r\n<p>公司企业相关问题的解决方案编号103</p>\r\n<p>公司企业相关问题的解决方案编号103</p>\r\n<p>&nbsp;</p>\r\n<p>公司企业相关问题的解决方案编号103</p>\r\n<p>公司企业相关问题的解决方案编号103</p>', 1329044121, '', '', 1, '', '', 0, 1, 0, 0, 8, '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 0, '');

INSERT INTO `ljcms_solution` VALUES(4, 1, '公司企业相关问题的解决方案编号1031', '', '', '', '<p>公司企业相关问题的解决方案编号1031</p>\r\n<p>公司企业相关问题的解决方案编号1031</p>\r\n<p>公司企业相关问题的解决方案编号1031</p>\r\n<p>公司企业相关问题的解决方案编号1031</p>\r\n<p>公司企业相关问题的解决方案编号1031</p>\r\n<p>公司企业相关问题的解决方案编号1031</p>\r\n<p>公司企业相关问题的解决方案编号1031</p>', 1329044142, 'admin', 'admin', 1, '6868668868', '>=', 0, 1, 0, 0, 12, '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 0, '');

INSERT INTO `ljcms_solution` VALUES(5, 1, '公司企业相关问题的解决方案编号1032', '', '', '', '<p>公司企业相关问题的解决方案编号1032</p>\r\n<p>公司企业相关问题的解决方案编号1032</p>\r\n<p>公司企业相关问题的解决方案编号1032</p>\r\n<p>公司企业相关问题的解决方案编号1032</p>\r\n<p>公司企业相关问题的解决方案编号1032</p>\r\n<p>公司企业相关问题的解决方案编号1032</p>\r\n<p>公司企业相关问题的解决方案编号1032</p>\r\n<p>公司企业相关问题的解决方案编号1032</p>', 1329044156, '', '', 1, '', '', 0, 1, 0, 0, 19, '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 0, '');

INSERT INTO `ljcms_solution` VALUES(6, 1, '收费版与免费版的区别', '', '', '收费版与免费版的区别', '<p><span style="color:#f70909;">收费版与免费版的区别</span></p>\r\n<p><span style="color:#f70909;">收费版自动去除良精程序版权信息及广告</span></p>\r\n<p><span style="color:#f70909;">免费版不可以去除良精版本信息及广告</span></p>\r\n<p>收费版提供一年的售后服务,程序本身有任何问题由我们负责.</p>\r\n<p>(出售的程序不提供修改如需修改订制其他功能是要另收费.收费的标准以功能的难度和要求来参考洽谈)</p>\r\n<p><br />\r\n<span style="color:#f70909;">购买商业版可以使有两个风格,默认01风格 第二个为您自选风格! </span><a href="http://www.liangjing.org/zh/HTML/List_net_15_1.html" target="_blank">选择风格</a></p>\r\n<p>&nbsp;</p>\r\n<p>使用说明:</p>\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 前台访问地址：http://网址/Default.aspx</p>\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 打开首页后会看到下面有后台访问地址</p>\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 编码UTF-8 用户名admin密码admin</p>\r\n<p>主要功能模块介绍 <br />\r\n1. 企业信息：发布介绍企业的各类信息，如企业简介、组织机构、营销网络、企业荣誉、联系方式，并可随意增加新的栏目等。 <br />\r\n2. 新闻动态：发布企业新闻和业内资讯，从后台到前台真正实现无限级分类显示，并随意控制显示级数，大大增加信息发布的灵活性。 <br />\r\n3. 产品展示：发布企业产品，增加了产品权限,并可多选产品直接下订单询盘，无限级分类，大大增加信息发布的灵活性。 <br />\r\n4. 下载资源：发布供网站浏览者和客户下载的资料等，如使用手册、销售合同、软件等，无限级分类。 <br />\r\n5. 其他信息：相当于无限扩展栏，并可进行无限分类，可以用于发布网站主栏目未归类的信息，如解决方案、成功案例、购买流程等。 <br />\r\n6. 会员中心：会员可任意设置级别<br />\r\n7. 留言反馈：以留言板的模式让有意见和建议的浏览者反馈回来。 <br />\r\n8. 站内检索：可检索站内全部栏目内容。 <br />\r\n9. 友情链接：可设文字和图片链接方式。 <br />\r\n10. 网站导航：可随意开通、屏蔽网站模块，并可设置站外链接，让网站可大可小。 <br />\r\n11. 系统管理：管理密码修改、网站信息设置、<br />\r\n12.管理权限：可根据需要增设多个管理员帐户，并任意分配不同管理权限。</p>', 1357440267, 'admin', 'admin', 1, '', '>=', 0, 0, 0, 0, 20, '收费版与免费版的区别', '1. 企业信息：发布介绍企业的各类信息，如企业简介、组织机构、营销网络、企业荣誉、联系方式，并可随意增加新的栏目等。', '1. 企业信息：发布介绍企业的各类信息，如企业简介、组织机构、营销网络、企业荣誉、联系方式，并可随意增加新的栏目等。', '', 0, '收费版与免费版的区别');

INSERT INTO `ljcms_solution` VALUES(7, 2, 'ASP.NET服务器控件的优化选择', '', '', 'ASP.NET服务器控件的优化选择', '<p><strong>减少不必要的服务器控件</strong></p>\r\n<p>服务器控件带来的方便和功能是html控件所不能比拟的。但是每一个服务器控件都需要在服务器端创建相应的对象，是以牺牲服务器端的资源为代价的，过多的使用服务器控件会极大的影响程序性能。</p>\r\n<p>很多情况下，简单地使用html标记或数据绑定即能够实现所需功能。比如＜asp：Label＞控件，若使用它来显示静态信息，则完全可用简单的标记来实现。如果html控件达不到所要实现的功能，而且在脚本语言如javascript、vbscript也不能实现的情况下，才考虑选择服务器控件。</p>\r\n<p><strong>禁用不必要的状态视图</strong></p>\r\n<p>服务器控件的状态视图属性能够自动的在页面往返过程中维护服务器控件的状态，减少开发者的工作量，但是需要占用大量的服务器内存资源。因此，在不需要服务器控件状态视图的情况下，应将其EnableViewState属性设置为false，如常用的＜asp：Lable＞和＜asp：Button＞控件。</p>\r\n<p><strong>Page.IsPostBack的运用</strong></p>\r\n<p>Page.IsPostBack用于记录页面是否从客户端返回，若为false表示初次运行，否则表示从客户端再次返回该页面。Page.IsPostBack的合理应用可以避免页面在往返过程中的一些不必要的操作。在Page_Load函数及一些只需要初始化一次的事件函数中均可以使用该属性来提高应用程序性能。</p>', 1357440427, 'admin', 'admin', 1, '', '>=', 0, 0, 0, 0, 22, 'ASP.NET服务器控件的优化选择', 'ASP.NET服务器控件的优化选择', 'ASP.NET服务器控件的优化选择', '', 0, 'ASP.NET服务器控件的优化选择');

INSERT INTO `ljcms_solution` VALUES(8, 2, '应用的缓存兼容性设计', '', '', '应用的缓存兼容性设计', '<span class="STYLE2">经过代理以后，由于在客户端和服务之间增加了中间层，因此服务器无法直接拿到客户端的IP，服务器端应用也无法直接通过转发请求的地址返回给客户端。但是在转发请求的HTTD头信息中，增加了HTTP_X_FORWARDED_????信息。用以跟踪原有的客户端IP地址和原来客户端请求的服务器地址：<br />\r\n下面是2个例子，用于说明缓存兼容性应用的设计原则：<br />\r\n<pre>    ''对于一个需要服务器名的地址的ASP应用：不要直接引用HTTP_HOST/SERVER_NAME，</pre><pre>判断一下是否有HTTP_X_FORWARDED_SERVER\r\n    function getHostName ()\r\n        dim hostName as String = ""\r\n        hostName = Request.ServerVariables("HTTP_HOST")\r\n        if not isDBNull(Request.ServerVariables("HTTP_X_FORWARDED_HOST")) then\r\n            if len(trim(Request.ServerVariables("HTTP_X_FORWARDED_HOST"))) &gt; 0 then\r\n                hostName = Request.ServerVariables("HTTP_X_FORWARDED_HOST")\r\n            end if\r\n        end if\r\n        return hostNmae\r\n    end function\r\n\r\n    //对于一个需要记录客户端IP的PHP应用：不要直接引用REMOTE_ADDR，而是要使用HTTP_X_FORWARDED_FOR，\r\n    function getUserIP (){\r\n        $user_ip = $_SERVER["REMOTE_ADDR"];\r\n        if ($_SERVER["HTTP_X_FORWARDED_FOR"]) {\r\n            $user_ip = $_SERVER["HTTP_X_FORWARDED_FOR"];\r\n        }\r\n    } </pre><br />\r\n注意：HTTP_X_FORWARDED_FOR如果经过了多个中间代理服务器，有何能是逗号分割的多个地址，<br />\r\n比如：200.28.7.155,200.10.225.77 unknown,219.101.137.3<br />\r\n因此在很多旧的数据库设计中（比如BBS）往往用来记录客户端地址的字段被设置成20个字节就显得过小了。<br />\r\n经常见到类似以下的错误信息：<br />\r\n<p>Microsoft JET Database Engine 错误 ''80040e57'' </p>\r\n<p>字段太小而不能接受所要添加的数据的数量。试着插入或粘贴较少的数据。 </p>\r\n<p>/inc/char.asp，行236 </p>\r\n原因就是在设计客户端访问地址时，相关用户IP字段大小最好要设计到50个字节以上，当然经过3层以上代理的几率也非常小。<br />\r\n如何检查目前站点页面的可缓存性（Cacheablility）呢？可以参考以下2个站点上的工具：<br />\r\n</span>', 1357440499, 'admin', 'admin', 1, '', '>=', 0, 0, 0, 0, 20, '应用的缓存兼容性设计', '应用的缓存兼容性设计', '应用的缓存兼容性设计', '', 0, '应用的缓存兼容性设计');

INSERT INTO `ljcms_solution` VALUES(9, 2, 'CMS三层体系结构', '', '', 'CMS三层体系结构', '<p>CMS三层体系结构与标准三层客户端/服务器体系结构是基本对应的。CMS三层体系结构没有什么难以理解的内容，都是一些常识性的东西。它的每一层对应着一个体系结构中必须的元素：交互（interaction）、操作（manipulation）以及存储。这三层是：</p>\r\n<p>表示层——处理与用户的交互、交流。 <br />\r\n事务逻辑（business logic）层——处理用户所需要的信息。 <br />\r\n数据库层——存储系统所处理的所有数据。 <br />\r\n图A中直观的表示了CMS三层体系结构。图A所示的层分别位于不同的机器上，实际上，多个层可以存在于同一台计算机中，但是将它们分布在多台计算机中可以更好的分配CMS系统的负荷。</p>\r\n<p>图A</p>\r\n<p><br />\r\n此主题相关图片如下：</p>\r\n<p><br />\r\nCMS三层体系结构</p>\r\n<p>CMS n层体系结构<br />\r\n简单的说，CMS n层体系结构就是把CMS三层体系结构的各个层分解为多个层，如图B所示。把层进行分解的好处是使得各个层更好的协调工作从而提高了系统性能；同时这也使得系统分布在更多的计算机上，这样可以减少系统由于指定计算机耗时过多而造成的瓶颈，从而提高了系统负荷。</p>\r\n<p>图B</p>\r\n<p><br />\r\n此主题相关图片如下:<br />\r\nCMS n层体系结构</p>\r\n<p>表示层是什么？<br />\r\n尽管表示层并不见得比其它层更重要，但是它几乎得到了全部的荣耀——因为它是唯一的CMS用户可以看到的层。这个层负责CMS与用户的交互工作。</p>\r\n<p>表示层实际上由两部分组成：即Web客户端和Web服务器。Web客户端驻留在用户计算机中，通常用来接受Web浏览器的表格（form）。Web服务器位于Web主机地址上，用来生成动态Web页面和组成CMS系统的表格。</p>\r\n<p>Web客户端与Web服务器端通过“请求——回应”的方式来相互通信。Web客户端向Web服务器发出请求，Web服务器根据请求作出回应。</p>\r\n<p>Web客户端使用的是HTTP的请求方式。如：</p>\r\n<p>GET /index.html HTTP/1.0User-Agent: Mozilla/4.0 (compatible; MSIE 5.0; Windows NT)Host: www.contentmgr.comWeb servers respond using the HTTP response. For example:HTTP/1.1 200 OKServer: Microsoft-IIS/5.0Date: Thu, 12 Jul 2002 19:19:52 GMTConnection: Keep-AliveContent-Length: 1270Content-Type: text/htmlSet-Cookie: ASPSESSIONIDQQQGQGDC=MOFPDBPCPNIBACIBDCIOFCCL; path=/Cache-control:&nbsp; private&nbsp; ...&lt; /BODY&gt;&nbsp; HTML嵌入了Web服务器发出的回应，该回应用来指示浏览器显示什么内容；JavaScript用来实现客户端的基本功能。最近以来，其它技术，如Java applet和ActiveX组件开始流行了，不过Web服务器在最初发出的回应中，绝大多数还是使用HTML，这包括了服务器发出的用于通知客户端使用何种HTML以外的技术来接管后面的执行过程的回应。</p>\r\n<p><br />\r\n事务逻辑层是什么？<br />\r\n重申一次，事务逻辑层的功能可以放到单个的服务器上（三层体系结构），也可以分布到多个服务器上（n层体系结构）。事务逻辑层的功能包括以下三个部分：</p>\r\n<p>访问（获取和保存）数据库层的数据。 <br />\r\n从表示层获取数据。 <br />\r\n执行必要的运算并且/或者处理数据。 <br />\r\n事务逻辑层从数据库层获取数据，并根据表示层的需要来对数据进行处理。事务逻辑层也可以获得表示层提供的数据，并根据数据库层的需要对其进行处理。</p>\r\n<p>CMS事务逻辑层的许多逻辑与其它两个层的交接（interfacing）有关。在Microsoft.NET下，由于ADO.NET、.NET remoting和Web服务器的帮助，这种逻辑的复杂性大多都被降低了。有了.NET之后，该层的最复杂的逻辑就是用于处理事务逻辑而进行计算和处理数据任务了（用C＃或者Managed C++）。</p>\r\n<p>什么是数据库层<br />\r\n数据库层的名字告诉了我们它的任务是什么了；它用来处理CMS数据。一个不太引人注意的地方就是，它的数据存储和检索功能并不限制于数据库。它可以是单个或者一系列平面文件（flat file），可能是XML格式。不过，数据通常还是存在数据库中。数据库</p>', 1357440588, 'admin', 'admin', 1, '', '>=', 0, 0, 0, 0, 4, 'CMS三层体系结构', 'CMS三层体系结构', 'CMS三层体系结构', '', 0, 'CMS三层体系结构');

INSERT INTO `ljcms_solution` VALUES(10, 2, '企业内容管理', '', '', '企业内容管理', '<span class="STYLE2"><span style="color:#333333;font-size:x-small;font-family:''宋体, Arial, Helvetica'';"><span>1．企业内容管理的定义 </span> <p></p>\r\n<p>企业内容管理正是随着数据管理的发展而为客户提供的一种应用软件，它管理、集成和访问从音频、视频到扫描图像的各种格式的商业信息。内容管理处理的对象范围比传统关系数据库管理系统(RDBMS)处理的结构化数据更广，除了一般文字、文档、多媒体、流媒体外，还包括Web网页、广告、程序（如JavaScript）、软件等一切数字资产(Digital Asset)，即所有结构化的数据和非结构化的文档。内容管理解决方案重点解决各种非结构化或半结构化的数字资源的采集、管理、利用、传递和增值，并集成到结构化数据的信息系统中，如ERP、CRM等，从而为这些应用系统提供更加广泛的数据来源。 </p>\r\n<p>2．“内容”和“内容管理” </p>\r\n<p>“内容”一词，源于出版媒体业，书报杂志、唱片影带里的创作，叫做内容，所以早期的内容管理CM(Content Management)，偏向出版产品的管理，以储存、流程、元数据(Metadata)为要件来制作系统。储存多以关连式数据库的方式，也有的以一般数据文件方式储存，或者是根据需要两种并存。内容从制作编辑到成品储存，都需经过或多或少的加工过程，有的是单线的简单流程，有的是分叉多线的并行操作，各种流程不同、相差甚远。至于元数据，是对内容的描述，如作者、日期、关键词、媒体种类、版权等，都以XML标记的方式记录，来达到跨媒体出版与个人化出版的目标。 </p>\r\n<p>内容管理在几年前，还是一个通称，因为还没有任何单一的系统，能适用于所有出版媒体内容的管理。各种内容虽然在数字化之后都是电子数据，应可用数字管理方式来处理，但在应用的层面上，却复杂得多。内容从制作、分送、到储存，生命周期的每一阶段，在处理上都有不同的需求。所以，这时的内容管理系统，是以个别出版媒体为主的。 </p>\r\n<p>3．与内容管理相关的概念 </p>\r\n<p>这段时间，有几个与出版内容关系密切的管理概念常被讨论，有数字资产管理DAM(Digital Asset Management)、网站内容管理WCM(Web Content Management)和数字版权管理DRM(Digital Rights Management)。 </p>\r\n<p>数字资产管理（DAM），原是为了影像的储存与管理设计的，以后领域渐广，触及到别的管理系统，所以界线也渐模糊。目前的概念，有企业智能资产管理，象企业的商标、自制的影像与字体；查询版权的所有权，以便于再次使用；商品目录价格的管理，便于销售人员的实时查询。 </p>\r\n<p>网站内容管理（WCM），是出版内容另一项管理，源于网上活动，以互联网为媒介，制作、传送电子内容，终端让使用人也可以与之互动，形成网站内容管理的独特性质。最基本的管理功能，包括内容的制作、更新、编辑、上线，让非技术人员便利地管理网站。 </p>\r\n<p>至于数字版权管理（DRM），则强调的是内容的使用权限，而非内容制作，其核心技术是加密，在流程上是最后传送阶段，原为保护版权、防止非法的内容传递，后来才发展出新的内容格式和更安全的传递技术，把内容与传递绑在一起。 </p>\r\n<p>4．“内容”的范围不断扩大 </p>\r\n<p>随着网络的普遍使用，企业内部信息的流通加速、文案与资料数量增加，象电子邮件、从Lotus Notes来的资料、网上讨论、实时简讯、Office文案，甚至是印刷资料的内容，即使不是出版媒体机构，也有加强管理这些资料的必要性。于是，“内容”就由原来的出版媒体内容，扩大成企业内部全部资料信息的内容了，同时也产生了“企业内容管理”ECM的新名词。 </p>\r\n<p>ECM常被视为知识管理KM(Knowledge Management)的基础建设，知识管理是企业由下而上、累积智能的系统方法，已有16年的历史，虽然在技术与方法上不断演进，但在实际应用中因投资回报不易评估，更需企业文化的配合，让大家养成知识与人共享的意愿，所以发展并不迅速。 </p>\r\n<p>除了前面提到与内容相关的各种管理，这一两年，另外出现了M字头的管理概念与系统，象客户关系管理CRM、供应链管理SCM、人力资源管理HRM，以及虽没有M但归在一类的企业资源计划ERP和企业网络入口EIP。这么多的管理，所产生的信息与数据，自然更要纳入企业“内容”的体系了。所以，企业的“内容”范围越来越大。 </p>\r\n<p>但这么多名词、概念与系统，不但企业用户弄不清楚哪一个做什么事、与别的系统有什么关连，就连制作推销系统的业者，也未必区分得清楚，而且系统越发展越有重迭走势。况且每类系统涵盖的都不小，都打算以高投入构建一个适用全企业的大系统。但经验告诉我们，这样的系统太昂贵、发展太耗时，尤其是构建集中式大型数据库，增加了系统的复杂程度，且风险也在提高。 </p>\r\n<p>5．把“内容”连起来管理 </p>\r\n<p>就在这时候，出现了另类的思考。如果制作一个新的系统，把上述提到的那些M放进去，则多半是由上而下的结构，也就是把下层原来运行的系统、原来分散在各处各种格式的数据，移植到上层，建立一个庞大系统，形成一个庞大的集中式数据储存库，这样，工程浩大是在意料之中的。但是，一个创新的构思正好相反，是从下而上，下层的原有运行的系统与数据，不论分散在什幺地方、也不论什么格式，都可以维持不动、继续运行。然后用转换的方式，把数据转换成中性如XML的格式，作为与别的系统衔接沟通的共同语言，再由一个信道把这些系统串连起来。 </p>\r\n<p>概括来说内容管理，是把现有各个底层的数据，建立一个共同的目录控管机制，各个系统处理数据的软件，都依此目录配送，使数据流动横跨各系统，既不必制造一个集中的庞大数据库，也不必更改现有系统的运行。这种模式的建立，自然较传统的作法省时省钱，风险也大为降低。Agari(<a href="http://www.agari.com/" target="_blank">www.agari.com</a>)的Mediaware系统即是一例。 </p>\r\n<p>内容管理的顺畅，有赖于内容的结构化，因为只有结构化，才能对内容分类、索引、排序、搜寻。利用XML相关工具，来制作结构化的内容，正是内容管理的基础建设。这项基础建设，无疑是内容管理的重要程序，也是一项十分艰苦的工作，更是实时企业所需要付出的代价。 </p>\r\n<p>企业内容管理的重要意义 </p>\r\n<p>今年春季引爆的SARS疫情扩散危机，突显了国内企业知识管理应用的重要性。许多企业为了维护正常运营，纷纷采取了员工分批上班或分地上班，彼此间依靠计算机、网络和公司资源间的互联以达到正常办公的目的。但除了计算机、电话网络建构等硬件设施外，维持公司运营的核心资源实际上存在于公司长期累积的无形知识资产之中。因此，在日常工作中，系统地完整地积累和撷取信息的便利性，则成为危机下维系公司命脉、决胜千里的关键。 </p>\r\n<p>根据IDC研究报告指出，知识管理是未来企业提高工作效率和增加竞争力的关键。作为其不可或缺的核心基础——企业内容管理方案，便成为业界炙手可热的新议题。在知识管理领域中，所有的知识都必须以各种形式进行储存管理，这使内容管理成为企业的一项重要问题。</p>\r\n</span></span>', 1357440663, 'admin', 'admin', 1, '', '>=', 0, 0, 0, 0, 23, '企业内容管理', '企业内容管理', '企业内容管理', '', 0, '企业内容管理');

INSERT INTO `ljcms_solutioncate` VALUES(1, '网站报错', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 0, 0, 1, 1, '', 1332723699, 0, '', '', 1, '', 1);

INSERT INTO `ljcms_solutioncate` VALUES(2, 'seo优化', '良精php企业网站管理系统', '良精php企业网站管理系统', '良精php企业网站管理系统', '', 0, 0, 2, 1, '', 1332723725, 0, '', '', 1, '', 1);

INSERT INTO `ljcms_solutioncate` VALUES(3, '购物指南', '购物指南', '购物指南', '购物指南', '', 0, 0, 3, 1, '购物指南', 1357524395, 0, '', '', 1, '', 1);

INSERT INTO `ljcms_solutioncate` VALUES(4, '支付方式', '支付方式', '支付方式', '支付方式', '', 0, 0, 4, 1, '支付方式', 1357524525, 0, '', '', 1, '', 1);

INSERT INTO `ljcms_solutioncate` VALUES(5, '配送方式', '配送方式', '配送方式', '配送方式', '', 0, 0, 5, 1, '配送方式', 1357536769, 0, '', '', 1, '', 1);

INSERT INTO `ljcms_tag` VALUES(1, '忘记网站 后台密码', 'info', 0, NULL, 1, 1357267563, 0, NULL, 0);

INSERT INTO `ljcms_tag` VALUES(2, '解决某些网站', 'info', 0, NULL, 1, 1357267692, 0, NULL, 0);

INSERT INTO `ljcms_tag` VALUES(3, '验证码', 'info', 0, NULL, 1, 1357267692, 0, NULL, 0);

INSERT INTO `ljcms_tag` VALUES(4, '企业建站系统V9.0', 'info', 0, NULL, 1, 1357267933, 0, NULL, 0);

INSERT INTO `ljcms_tag` VALUES(5, '收费版与免费版的区别', 'solution', 0, NULL, 1, 1357440267, 0, NULL, 0);

INSERT INTO `ljcms_tag` VALUES(6, 'ASP.NET服务器控件的优化选择', 'solution', 0, NULL, 1, 1357440427, 0, NULL, 0);

INSERT INTO `ljcms_tag` VALUES(7, '应用的缓存兼容性设计', 'solution', 0, NULL, 1, 1357440499, 0, NULL, 0);

INSERT INTO `ljcms_tag` VALUES(8, 'CMS三层体系结构', 'solution', 0, NULL, 1, 1357440588, 0, NULL, 0);

INSERT INTO `ljcms_tag` VALUES(9, '企业内容管理', 'solution', 0, NULL, 1, 1357440663, 0, NULL, 0);

INSERT INTO `ljcms_tag` VALUES(10, 'php企业网站管理系统', 'solution', 0, NULL, 1, 1357440756, 0, NULL, 0);

INSERT INTO `ljcms_tag` VALUES(11, '良精企业网站管理系统V9.0', 'product', 0, NULL, 1, 1357539505, 0, NULL, 0);

INSERT INTO `ljcms_tag` VALUES(12, '正式发布', 'product', 0, NULL, 1, 1357539505, 0, NULL, 0);

INSERT INTO `ljcms_tag` VALUES(13, 'tttt', 'product', 0, NULL, 1, 1357610821, 0, NULL, 0);

INSERT INTO `ljcms_tag` VALUES(14, '数据线', 'product', 0, NULL, 1, 1357709552, 0, NULL, 0);

INSERT INTO `ljcms_user` VALUES(1, 'adminsa', '1', 'e10adc3949ba59abbe56e057f20f883e', 0, '', '1111', '2222', '2222', 1341203150, 0, 1, 59, 1, '1111', '1111', '1111', '1111', 0, '111', '', 1357544544, '127.0.0.1', '11111', '大家好');

INSERT INTO `ljcms_user` VALUES(12, '11111', '', '96e79218965eb72c92a549dd5a330112', 0, '', NULL, '', '', 1343194395, 0, 1, 0, 1, '', '', '', '', 0, '', '', 0, '', '', NULL);

INSERT INTO `ljcms_user` VALUES(11, 'test23', '', 'e10adc3949ba59abbe56e057f20f883e', 0, '', NULL, '', '', 1343194252, 0, 1, 0, 1, '', '', '', '', 0, '', '', 0, '', '', NULL);

INSERT INTO `ljcms_user` VALUES(10, 'test220', '', 'e10adc3949ba59abbe56e057f20f883e', 0, '', NULL, '', '', 1343194153, 0, 1, 0, 1, '', '', '', '', 0, '', '', 0, '', '', NULL);

INSERT INTO `ljcms_user` VALUES(3, 'test11', '', 'e10adc3949ba59abbe56e057f20f883e', 0, '', NULL, '', 'sdfsdfs@126.com', 1341276533, 0, 1, 3, 1, '', 'testestes', '', '2009-10-11', 0, '', '010-6684214', 1341468162, '127.0.0.1', '', 'sdfsfsfdfsf');

INSERT INTO `ljcms_user` VALUES(4, 'test22', '', 'e10adc3949ba59abbe56e057f20f883e', 0, '', NULL, '', 'sdfsdfs@126.com', 1341308536, 0, 1, 0, 1, '', '', '', '2008-10-11', 1, '', '010-66884112', 0, '', '', '');

INSERT INTO `ljcms_user` VALUES(13, '1122', '', '202cb962ac59075b964b07152d234b70', 0, '', NULL, '', '', 1343194685, 0, 1, 0, 1, '', '', '', '', 0, '', '', 0, '', '', NULL);

INSERT INTO `ljcms_user` VALUES(6, 'itf4', '', 'a10e774504c45fb60a5643105ba6093c', 0, '', NULL, '', 'asp3721@163.com', 1341388598, 0, 1, 2, 1, '', '', '', '', 1, '', '81991660', 1343297221, '192.168.1.104', '', '');

INSERT INTO `ljcms_user` VALUES(9, 'test123', '', 'e10adc3949ba59abbe56e057f20f883e', 0, '', NULL, '', '', 1343192969, 0, 1, 0, 1, '', '', '', '', 0, '', '', 0, '', '', NULL);

INSERT INTO `ljcms_user` VALUES(7, 'test110', '', 'e10adc3949ba59abbe56e057f20f883e', 0, '', NULL, '', '', 1343182525, 0, 1, 3, 1, '', '', '', '', 0, '', '', 1343187287, '127.0.0.1', '', NULL);

INSERT INTO `ljcms_user` VALUES(8, 'test120', '', 'e10adc3949ba59abbe56e057f20f883e', 0, '', NULL, '', '', 1343183961, 0, 1, 0, 1, '', '', '', '', 0, '', '', 0, '', '', NULL);

INSERT INTO `ljcms_user` VALUES(14, '1q1', '', '202cb962ac59075b964b07152d234b70', 0, '', NULL, '', '', 1343195043, 0, 1, 0, 1, '', '', '', '', 0, '', '', 0, '', '', NULL);

INSERT INTO `ljcms_user` VALUES(15, '123sa', '', 'e10adc3949ba59abbe56e057f20f883e', 0, '', NULL, '', '', 1343195138, 0, 1, 0, 1, '', '', '', '', 0, '', '', 0, '', '', NULL);

INSERT INTO `ljcms_user` VALUES(16, '1qw', '', 'e10adc3949ba59abbe56e057f20f883e', 0, '', NULL, '', '', 1343195256, 0, 1, 0, 1, '', '', '', '', 0, '', '', 0, '', '', NULL);

INSERT INTO `ljcms_user` VALUES(17, '1231', '', 'e10adc3949ba59abbe56e057f20f883e', 0, '', NULL, '', '', 1343195458, 0, 1, 0, 1, '', '', '', '', 0, '', '', 0, '', '', NULL);

INSERT INTO `ljcms_user` VALUES(18, 'qwewq', '', 'e10adc3949ba59abbe56e057f20f883e', 0, '', NULL, '', '', 1343195850, 0, 1, 0, 1, '', '', '', '', 0, '', '', 0, '', '', NULL);

INSERT INTO `ljcms_user` VALUES(19, 'test1123', '', 'e10adc3949ba59abbe56e057f20f883e', 0, '', NULL, '', '', 1343196219, 0, 1, 1, 1, '', '', '', '', 0, '', '', 1343196219, '127.0.0.1', '', NULL);

INSERT DELAYED IGNORE INTO `ljcms_usergroup` VALUES(1, '普通会员', '2147483647', '', 1, 1, 'new.php', '普通会员普通会员');

INSERT DELAYED IGNORE INTO `ljcms_usergroup` VALUES(2, '高级会员', '666666666', '', 6, 1, '1340955505', '高级会员');

INSERT DELAYED IGNORE INTO `ljcms_usergroup` VALUES(3, '合作伙伴', '99999999', '', 9, 1, '1340955539', '合作伙伴');

INSERT DELAYED IGNORE INTO `ljcms_usergroup` VALUES(4, '临时游客', '6868668868', '', 0, 1, '1341387474', '临时游客');
