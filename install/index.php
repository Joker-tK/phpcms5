<?php
/**
 * LiangJing TEAM
 * ============================================================================
 * * COPYRIGHT LiangJing 2012-2013.
 * http://www.liangjing.org;
 * ----------------------------------------------------------------------------
 * Author:LiangJing
 * Id: backup.php 2012-10-23
 */
 
define('IN_LiangJing', true);

include_once ('./include/common.php');
		
//开启SESSION
session_start();

/* step操作项的初始化 */
if (empty ($_REQUEST['step']))
{
	$step = 'welcome';
}
else
{
	$step = trim($_REQUEST['step']);
}

if (file_exists($file_lock))
{
	$title = $_LANG['LJPHPCMS'] . " &rsaquo; " . $_LANG['lock'];
	
	include $install->tpl('install.lock');
	exit();
}

if ($step == 'welcome')
{
	$title = $_LANG['welcome'];
	include $install->tpl('welcome');
}
elseif ($step == 'check')
{
	$title = $_LANG['LJPHPCMS'] . " &rsaquo; " . $_LANG['check'];
	
	/* 系统信息 */
	$sys_info['os']            = PHP_OS;
	$sys_info['web_server']    = $_SERVER['SERVER_SOFTWARE'];
	$sys_info['php_ver']       = PHP_VERSION;
	$sys_info['mysql_ver']     = extension_loaded('mysql') ? $_LANG['yes'] : $_LANG['no'];
	$sys_info['zlib']          = function_exists('gzclose') ? $_LANG['yes']:$_LANG['no'];
	$sys_info['timezone']      = function_exists("date_default_timezone_get") ? date_default_timezone_get() : $_LANG['no_timezone'];
	$sys_info['socket']        = function_exists('fsockopen') ? $_LANG['yes'] : $_LANG['no'];
	$sys_info['gd']        = extension_loaded("gd") ? $_LANG['yes'] : $_LANG['no'];

	/* 检查目录 */
  $check_dirs = array (
		'admin',
		'install',
		'css',
		'data',
		'rewrite',
		'member',
		'images',
		'source'
	);
	
	foreach ($check_dirs AS $dir)
	{
		$full_dir = ROOT_PATH . $dir;
		$writeable = $install->check_writeable($full_dir);
		if ($writeable == '1')
		{
			$if_write = "<b class='write'>" . $_LANG['write'] . "</b>";
		}
		elseif ($writeable == '0')
		{
			$if_write = "<b class='noWrite'>" . $_LANG['no_write'] . "</b>";
			$no_write = true;
		}
		elseif ($writeable == '2')
		{
			$if_write = "<b class='noWrite'>" . $_LANG['not_exist'] . "</b>";
			$no_write = true;
		}
		
		$writeable_dir[$dir] = $if_write;
	}
	
	//根据 Web 服务器 信息配置伪静态文件
	if (stristr($sys_info['web_server'], "Apache"))
	{
		$rewrite_file = ".htaccess.txt";
	}
	elseif (stristr($sys_info['web_server'], "nginx"))
	{
		$rewrite_file = ".nginx.txt";
	}
	elseif (stristr($sys_info['web_server'], "IIS"))
	{
		$iis_exp = explode("/",$sys_info['web_server']);
		$iis_ver = $iis_exp['1'];
		
		if ($iis_ver >= 7.0)
		{
			$rewrite_file = "web.config.txt";
		}
		else
		{
			$rewrite_file = "httpd.ini.txt";
		}
	}
		
	//复制rewrite文件到站点根目录
	if ($rewrite_file)
	{
		$source = ROOT_PATH . "install/rewrite/" . $rewrite_file;
		$destination = ROOT_PATH . $rewrite_file;
		@copy($source, $destination);
	}
	
	include $install->tpl('check');
}
elseif ($step == 'setting')
{
	$title = $_LANG['LJPHPCMS'] . " &rsaquo; " . $_LANG['setting'];
	
	//如果提交表单执行以下
	if ($_POST['install']) 
	{
		//生成config文件内容
		$config_str = "<?php\n";
		$config_str .= "/**\n";
		$config_str .= " * LiangJing TEAM 豆壳\n";
		$config_str .= " * ============================================================================\n";
		$config_str .= " * * COPYRIGHT LiangJing 2012-2013.\n";
		$config_str .= " * http://www.liangjing.org;\n";
		$config_str .= " * ----------------------------------------------------------------------------\n";
		$config_str .= " * Author:LiangJing\n";
		$config_str .= " * Id: config.php 2012-10-19\n";
		$config_str .= "*/\n\n";
		$config_str .= "///数据库类型\n";
		$config_str .= "define('DB_TYPE','mysql');\n";
		$config_str .= "///数据库编码\n";
		$config_str .= "define('DB_CHARSET','utf8');\n";
		$config_str .= "///数据库服务器\n";
		$config_str .= "define('DB_HOST','" . $_POST[dbhost] . "');\n";
		$config_str .= "///数据库名\n";
		$config_str .= "define('DB_DATA','". $_POST[dbname] ."');\n";
		$config_str .= "///数据库登录帐号\n";
		$config_str .= "define('DB_USER','". $_POST[dbuser] ."');\n";
		$config_str .= "///数据库登录密码\n";
		$config_str .= "define('DB_PASS','". $_POST[dbpass] ."');\n";
		$config_str .= "///数据表扩展\n";
		$config_str .= "define('DB_PREFIX','". $_POST[prefix] ."');\n";
		$config_str .= "///数据库持久连接 0=关闭, 1=打开\n";
		$config_str .= "define('DB_PCONNECT',0);\n";
		$config_str .= "?>";
		
		$fopen_config = fopen($file_config, "w+");
		fwrite($fopen_config, $config_str);

		//嵌入config配置文件
		include_once ($file_config); 
		
		//检查表单
		if (!@$link = mysql_connect(DB_HOST, DB_USER, DB_PASS))
		{
			$cue = $_LANG['cue_connect'];
		}
		elseif (!$_POST['username'])
		{
			$cue = $_LANG['cue_username_empty'];
		}
		elseif (!$install->is_username($_POST['username']))
		{
			$cue = $_LANG['cue_username_wrong'];
		}
		elseif (!$_POST['password'])
		{
			$cue = $_LANG['cue_password_empty'];
		}
		elseif (!$install->is_password($_POST['password']))
		{
			$cue = $_LANG['cue_password_wrong'];
		}
		elseif (!$_POST['password_confirm'])
		{
			$cue = $_LANG['cue_password_confirm_empty'];
		}
		elseif ($_POST['password'] != $_POST['password_confirm'])
		{
			$cue = $_LANG['cue_password_confirm_wrong'];
		}
		
		//如果存在错误信息则不执行数据库操作
		if (!$cue)
		{
			mysql_query("CREATE DATABASE IF NOT EXISTS `".DB_DATA."` default charset utf8 COLLATE utf8_general_ci");
			mysql_select_db(DB_DATA);
		
			//读取SQL文件到一个字符串中
			$sql = file_get_contents('./data/ljcms_v1.sql');

		//	echo($sql);
		//	exit();
			
			//进行安装的常规替换
			$sql = str_replace('ljcms_', $_POST[prefix] , $sql);
			
			//进行安装的常规替换
			$sql_head = "SET SQL_MODE='NO_AUTO_VALUE_ON_ZERO';\n";
			$sql_head .= "SET time_zone = '+00:00';\n\n\n";

			$sql_head .= "/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;\n";
			$sql_head .= "/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;\n";
			$sql_head .= "/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;\n";
			$sql_head .= "/*!40101 SET NAMES utf8 */;\n\n";
			
			$sql = $sql_head . $sql;
			
			//生成管理员
			$username = $_POST['username'];
			$password = md5($_POST['password']);
			$add_time = time();
			
			//导入数据
			$install->sql_execute($sql);
	
			/* 写入 hash_code，做为网站唯一性密钥 */
		//	$hash_code = md5(md5(time()) . md5(md5(ROOT_URL . $dbhost . $dbname . $dbuser . $dbpass)));
			
			//初始化数据
			$table_admin = $prefix . "admin";
			$table_config = $prefix . "config";
			$build_date = time();
			
			//初始化管理员账号
			$install->sql_execute("INSERT INTO ljcms_admin VALUES (1, '$username', '".$password."', 0, 1, ".time().", 1, 0, 0, '', '') ");

			//初始化系统信息
			//$install->sql_execute("UPDATE $table_config SET value = '0' WHERE name = 'rewrite'");
			//$install->sql_execute("UPDATE $table_config SET value = '$build_date' WHERE name = 'build_date'");
			//$install->sql_execute("UPDATE $table_config SET value = '$hash_code' WHERE name = 'hash_code'");
	
			$_SESSION['cue'] = '';
			$_SESSION['username'] = $_POST['username'];
		
			header("Location: index.php?step=finish");
		}
		else
		{
			$_SESSION['cue'] = $cue;
		}
	}
	
	include $install->tpl('setting');
}
elseif ($step == 'finish')
{
	//生成install.lock文件
	$fopen_lock = fopen($file_lock, "w+");
	fwrite($fopen_lock, "LJPHPCMS INSTALLED");
	
	$title = $_LANG['LJPHPCMS'] . " &rsaquo; " . $_LANG['finish'];
	
	include $install->tpl('finish');
}

?>