<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2012.06.05
 * @Id         在线应聘
**/
header("Content-type: text/html; charset=utf-8");
define('ALLOWGUEST',true);
session_start();
require_once './source/core/run.php';

require_once 'data/include/Functions.php';
require_once 'data/include/Cart.class.php';


$tplfile = INDEX_TEMPLATE."productbuy.".$tplext;
$widgetfile = "./source/widget/productbuy.php";
if(!Core_Fun::fileexists($tplfile)){
	Core_Fun::halt("对不起，模板文件“".$tplfile."”不存在，请检查！","",1);
}
if(!Core_Fun::fileexists($widgetfile)){
	Core_Fun::halt("对不起，部件文件“".$widgetfile."”不存在，请检查！","",1);
}

/* 缓存,模板处理 */
if($config['cachstatus']==1){
	$cache_seconds = $config['cachtime']*60;
	$tpl->setCaching(true); 
	$tpl->setCacheLifetime($cache_seconds);	
}
$cacheid = md5($_SERVER["REQUEST_URI"]);
if(Core_Fun::rec_post('action')=='saveadd'){
	require_once './source/module/app.php';
	require_once './source/widget/productbuy.php';
}else{
	if(!$tpl->isCached($tplfile,$cacheid)){
		require_once './source/module/app.php';
		require_once './source/widget/productbuy.php';
	}
}


$proid = Core_Fun::request("proid");

productlist();
$id = $_GET['proid'];
echo '<pre>';
print_r($id);
function productlist(){
global $db,$tpl;
$sql   = "SELECT * FROM ".DB_PREFIX."product WHERE flag=1 ORDER BY productid";
$products = $db->getall($sql);

if(!$products){
}
else
{
$tpl->assign("products",$products);
/*	echo '<pre>';
print_r($products);*/
}
}

//取得具体某商品信息
if(!empty($_GET['proid'])){
	global $db;
	$sql   = "SELECT * FROM ".DB_PREFIX."product WHERE productid={$_GET['proid']}";
	$detail  = $db->getall($sql);
	$tpl->assign("detail",$detail);
}

$tpl->assign("proid",trim($proid));
$tpl->assign("username",$_SESSION["USERNAME"]);
$tpl->assign("runtime",Core_Fun::runtime());
$tpl->display($tplfile,$cacheid);
echo $tplfile;








?>