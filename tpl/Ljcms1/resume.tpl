<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title><!--{$page_title}-->-<!--{$copyright_header}--></title>
<meta name="description" content="<!--{$page_description}-->" />
<meta name="keywords" content="<!--{$page_keyword}-->" />
<meta name="author" content="<!--{$copyright_author}-->" />
<script src="<!--{$skinpath}-->js/AC_RunActiveContent.js" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/Html.js" type="text/javascript"></script>
<link href="<!--{$skinpath}-->style/css.css" id="Skin" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<!--{$skinpath}-->style/cssset.js"></script>
<link href="<!--{$skinpath}-->style/css.css" id="Skin" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='<!--{$skinpath}-->js/command.js'></script>
<script src="<!--{$skinpath}-->js/jquery-1.4.4.min.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/downnav.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/jquery.cross-slide.js" language="javascript" type="text/javascript"></script>
<link rel="alternate" type="application/rss+xml" title="订阅该空间的 良 精 文章" href="../rss.xml">
<script type="text/javascript">
function mysubumit()
 {
 if (formcheck())
 {

	  document.editform.action="userinfo.php";
      document.editform.method="post";
      document.editform.submit();
   }  
}

  function formcheck() 
    { 
       if(document.getElementById("loginname").value== "") 
         { 
		 
		   alert("请输入用户名");
		   document.editform.loginname.focus();
		   document.editform.loginname.select(); 
           return false;
         }

	   if(document.editform.loginname.value.length<3) 
         { 
		   alert("用户名不能小于3位");
		   document.editform.loginname.focus();
		   document.editform.loginname.select(); 
           return false;
         }   

    return true;      
    }


function checkform(){

if (document.myform.username.value.length == 0) 
{
alert('姓名 不能为空');
document.myform.username.focus();
return false;
}

if (document.myform.email.value.length == 0)
{
alert('E–mail 不能为空');
document.myform.email.focus();
return false;
}
if (document.myform.telno.value.length == 0)
{
alert('联系电话不能为空');
document.myform.telno.focus();
return false;
}
}


</script>
</head>
<body>
    
<div class="divbody" id="wrap">
<div class="headler" id="headler">

                <!-- star pub header# //-->
  <!--{include file="<!--{$tplpath}-->block_head.tpl"}-->
                <!-- end header# //-->

   </div>
</div>

        <div class="center">
            <div class="center_left">

<!-- star pub login#新闻分类-->
<div class="module" id="login">
    <div class="moduleheadler">
        <div class="moduleheadle_left">
        </div>
        <div class="moduleheadle_center">
            <span>会员中心</span>
        </div>
        <div class="moduleheadle_right">
        </div>
    </div>
    <div class="modulebody">
        <div class="modulebody_left">
        </div>
        <div class="modulebody_center">

	  <div class="webnav"> 
        <div id="web-sidebar">
<!--{if $username eq ""}-->
<dl>
<dt class="part2" id="part1-id6"><a href="register.php">注册会员</a></dt>
</dl>	
<!--{else}-->
<dl>
<dt class="part2" id="part1-id6"><a href='index.php'  title='会员中心首页'>会员中心首页</a></dt>
</dl>	
<dl>
<dt class="part2" id="part1-id6"><a  href='userinfo.php' title='修改基本信息'>修改基本信息</a></dt>
</dl>	
<dl>
<dt class="part2" id="part1-id6"><a  href='mymessage.php' title='我的留言'>我的留言</a></dt>
</dl>	
<dl>
<dt class="part2" id="part1-id6"><a  href='myorder.php' title='我的订单'>我的订单</a></dt>
</dl>	
<dl>
<dt class="part2" id="part1-id6"><a  href='resume.php' title='我的简历'>我的简历</a></dt>
</dl>	
<dl>
<dt class="part2" id="part1-id6"><a href='loginout.php' title='安全退出'>安全退出</a></dt>
</dl>	
<!--{/if}-->
        </div>
      </div>

        </div>
        <div class="modulebody_right">
        </div>
    </div>
    <div class="modulebuttom">
    </div>
</div>
  <!-- end login#新闻分类-->

      <!--{include file="<!--{$tplpath}-->block_contact.tpl"}--> 
</div>
            <div class="main_right">
                <!--企业简介内容 start-->
                <div class="module" id="enterpriseInformationDetail">
    <div class="moduleheadler">
        <div class="moduleheadle_left">
        </div>
        <div class="moduleheadle_center">
            <span>&nbsp;当前位置：<a href="<!--{$url_index}-->">首 页</a>&nbsp;&nbsp;>>&nbsp;&nbsp;<!--{$navigation}--></span>
        </div>
        <div class="moduleheadle_right">
        </div>
    </div>
    <div class="modulebody">
        <div class="modulebody_left">
        </div>
        <div class="modulebody_center">
<!--{if $uaction eq ""}-->
<div class="main-wrap">
  <div class="main-cont">
	<form action="resume.php" method="post" name="myform" id="myform" style="margin:0">
	<input type="hidden" name="uaction" id="uaction" value="deljob" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <thead class="tb-tit-bg">
	  <tr>
	    <th width="10%"><div class="th-gap-u">简历编号</div></th>
		<th width="10%"><div class="th-gap-u">我的名字</div></th>
		<th width="16%"><div class="th-gap-u">应聘职位</div></th>
		<th width="30%"><div class="th-gap-u">联系电话</div></th>
		<th width="18%"><div class="th-gap-u">注册时间</div></th>
		<th><div class="th-gap">操作</div></th>
	  </tr>
	  </thead>
	  <tfoot class="tb-foot-bg"></tfoot>
	  <!--{foreach $jobs as $uvolist}-->
	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">
	    <td align="center"><input name="id[]" type="checkbox" value="<!--{$uvolist.aid}-->" onClick="checkItem(this, 'chkAll')"></td>
		<td><!--{$uvolist.username}--></td>
		<td align="center">
		<!--{$uvolist.jobtitle}-->
		</td>
		<td align="center"><!--{$uvolist.telno}--></td>
		<td><!--{$uvolist.addtime|date_format:"%Y/%m/%d %H:%M:%S"}--></td>
		<td align="center"><a href="resume.php?uaction=editjob&id=<!--{$uvolist.aid}-->&page=<!--{$page}-->" class="icon-edit">编辑</a></td>
	  </tr>
	  <!--{foreachelse}-->
      <tr>
	    <td colspan="6" align="center">暂无信息</td>
	  </tr>
	  <!--{/foreach}-->
	  <!--{if $total>0}-->
	  <tr>
		<td align="center"><input name="chkAll" type="checkbox" id="chkAll" onClick="checkAll(this, 'id[]')" value="checkbox"></td>
		<td class="hback" colspan="5"><input class="button" name="btn_del" type="button" value="删 除" onClick="{if(confirm('确定删除选定信息吗!?')){$('#uaction').val('deljob');$('#myform').submit();return true;}return false;}" class="button">&nbsp;&nbsp;共[ <b><!--{$total}--></b> ]条记录</td>
	  </tr>
	  <!--{/if}-->
	</table>
	</form>
	<!--{if $pagecount>1}-->
	<table width='95%' border='0' cellspacing='0' cellpadding='0' align='center' style="margin-top:10px;">
	  <tr>
		<td align='center'><!--{$showpage}--></td>
	  </tr>
	</table>
	<!--{/if}-->
  <div style="clear:both;height:10px;width:100%"></div>
  </div>
</div>
<!--{/if}-->
<!--{if $uaction eq "editjob"}-->
<div class="main-wrap">
<div class="main-cont">
<h3 class="title"> <a href="resume.php?<!--{$comeurl}-->" class="btn-general"><span>返回列表</span></a> 编辑简历</h3>
<form name="myform" id="myform" method="post" action="resume.php" onsubmit="return checkform();" />
<form method='POST' onSubmit='return CheckJob();' name='myform' action='<!--{$url_applyjob}-->' >
<input type="hidden" name="uaction" value="jobave" />
<input type="hidden" name="id" value="<!--{$id}-->" />
<table class='job_table' width="95%" border="0" align="center" cellpadding="5" cellspacing="3" style="margin-top:20px">
<tr>
<td class='text'>应聘职位</td>
<td class='input'><!--{$jobs.jobtitle}--></td>
</tr>
<tr><td class='text'>姓名</td>
<td class='input'><input name='username' type='text' value="<!--{$jobs.username}-->" class='input-text' size='40'><span class='info'>*</span></td></tr>
<tr><td class='text'>性别</td>
<td class='input'>
<!--{if $jobs.sex==1}-->
<input name='sex' type='radio' id='para12_1' value='1' checked='checked' /><label for='para12_1'>男士</label> 
<input name='sex' type='radio' id='para12_2' value='0'  /><label for='para12_2'>女士</label> 
<!--{else}-->
<input name='sex' type='radio' id='para12_1' value='1'/><label for='para12_1'>男士</label> 
<input name='sex' type='radio' id='para12_2' value='0' checked='checked'   /><label for='para12_2'>女士</label> 
<!--{/if}-->
</td></tr>
<tr><td class='text'>出生年月</td>
<td class='input'><input name='brothday' type='text' value="<!--{$jobs.brothday}-->" class='input-text' size='40'><span class='info'></span></td></tr>
<tr><td class='text'>籍贯</td>
<td class='input'><input name='chinatext' type='text' value="<!--{$jobs.chinatext}-->" class='input-text' size='40'><span class='info'></span></td></tr>
<tr><td class='text'>联系电话</td>
<td class='input'><input name='telno' type='text'  value="<!--{$jobs.telno}-->" class='input-text' size='40'><span class='info'></span></td></tr>
<tr><td class='text'>E–mail</td>
<td class='input'><input name='email' type='text'  value="<!--{$jobs.email}-->" class='input-text' size='40'><span class='info'>*</span></td></tr>
<tr><td class='text'>学历</td>
<td class='input'><input name='degree' type='text'  value="<!--{$jobs.degree}-->" class='input-text' size='40'><span class='info'></span></td></tr>
<tr><td class='text'>专业</td>
<td class='input'><input name='prosesion' type='text'  value="<!--{$jobs.prosesion}-->" class='input-text' size='40'><span class='info'></span></td></tr>
<tr><td class='text'>学校</td>
<td class='input'><input name='school' type='text'  value="<!--{$jobs.school}-->" class='input-text' size='40'><span class='info'></span></td></tr>
<tr><td class='text'>通讯地址</td>
<td class='input'><input name='address' type='text'  value="<!--{$jobs.address}-->" class='input-text' size='40'><span class='info'></span></td></tr>
<tr><td class='text'>所获奖项</td>
<td class='input'><textarea name='awards' class='textarea-text' cols='60' rows='5'><!--{$jobs.awards}--></textarea><span class='info'></span></td></tr>
<tr><td class='text'>工作经历</td>
<td class='input'><textarea name='experience' class='textarea-text' cols='60' rows='5'><!--{$jobs.experience}--></textarea><span class='info'></span></td></tr>
<tr><td class='text'>业余爱好</td>
<td class='input'><textarea name='hobby' class='textarea-text' cols='60' rows='5'><!--{$jobs.hobby}--></textarea><span class='info'></span></td></tr>
<tr><td class='text'></td>
<td class='submint'>
<input type='submit' name='Submit' value='修改简历' class='submit' />&nbsp;&nbsp;
<input type='reset' name='Submit' value='重新填写' class='reset' /></td>
</tr></table>
	</form>
  <div style="clear:both;height:10px;width:100%"></div>
  </div>
  <div style="clear:both;height:10px;"></div>
</div>
<!--{/if}-->
        </div>
        <div class="modulebody_right">
        </div>
    </div>
    <div class="modulebuttom">
    </div>
</div>

                <!--企业简介 end-->
            </div>
        </div>


            <div class="footer">
                <!-- star pub buttom#-->
          

  <!--{include file="<!--{$tplpath}-->block_footer.tpl"}-->


                <!-- end pub buttom#-->
            </div>
    
	<script type="text/javascript" src="<!--{$skinpath}-->js/screen.js"></script>
</body>
</html>






