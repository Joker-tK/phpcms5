function SetHome(obj,vrl){
        try{
            obj.style.behavior='url(#default#homepage)';obj.setHomePage(vrl);
            }
        catch(e){
                if(window.netscape) {
                        try { 
						netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");  
                        }  
                        catch (e){ 
                                alert("Your Browser does not support ");
                        }
                        var prefs = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefBranch);
                        prefs.setCharPref('browser.startup.homepage',vrl);
                 }
        }
}

function addFavorite(){
    var vDomainName=window.location.href;
    var description=document.title;
    try{//IE
        window.external.AddFavorite(vDomainName,description);
    }catch(e){//FF
        window.sidebar.addPanel(description,vDomainName,"");
    }
}

//图片缩略图效果
function DrawImage(ImgD,FitWidth,FitHeight){
    var image=new Image();
	image.src=ImgD.src;
	if(image.width>0 && image.height>0){
		if(image.width/image.height>= FitWidth/FitHeight){
			if(image.width>FitWidth){
				ImgD.width=FitWidth;
				ImgD.height=(image.height*FitWidth)/image.width;
			}else{
				ImgD.width=image.width; 
				ImgD.height=image.height;
			}
		}else{
			if(image.height>FitHeight){
				ImgD.height=FitHeight;
				ImgD.width=(image.width*FitHeight)/image.height;
			}else{
				ImgD.width=image.width; 
				ImgD.height=image.height;
			} 
		}
	}
	//居中
	if(ImgD.height < FitHeight ){
		var paddH = parseInt((FitHeight - ImgD.height)/2);
		ImgD.style.paddingTop    = paddH+"px";
		ImgD.style.paddingBottom = paddH+"px";
	}
	if(ImgD.width < FitWidth ){
		var paddW = parseInt((FitWidth - ImgD.width)/2);
		ImgD.style.paddingRight = paddW+"px";
		ImgD.style.paddingLeft  = paddW+"px";
	}
	//var paddH = parseInt((FitHeight - ImgD.height)/2);
	//var paddW = parseInt((FitWidth - ImgD.width)/2);
	//var paddH = ((FitHeight - ImgD.height)/2);
	//var paddW = ((FitWidth - ImgD.width)/2);
	//ImgD.style.padding = paddH+"px "+paddW+"px "+paddH+"px "+paddW+"px";
 }