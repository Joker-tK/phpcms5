<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title><!--{$page_title}-->-<!--{$copyright_header}--></title>
<meta name="description" content="<!--{$page_description}-->" />
<meta name="keywords" content="<!--{$page_keyword}-->" />
<meta name="author" content="<!--{$copyright_author}-->" />
<script src="<!--{$skinpath}-->js/AC_RunActiveContent.js" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/Html.js" type="text/javascript"></script>
<link href="<!--{$skinpath}-->style/css.css" id="Skin" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<!--{$skinpath}-->style/cssset.js"></script>
<script src="<!--{$skinpath}-->js/jquery-1.4.4.min.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/downnav.js" language="javascript" type="text/javascript"></script>
<script type="text/javascript" src="<!--{$skinpath}-->js/ui.tabs.pack.js"></script>
<link rel="alternate" type="application/rss+xml" title="订阅该空间的 良 精 文章" href="../rss.xml">
</head>
<body>
    
<div class="divbody" id="wrap">
<div class="headler" id="headler">

                <!-- star pub header# //-->
  <!--{include file="<!--{$tplpath}-->block_head.tpl"}-->
                <!-- end header# //-->

   </div>
</div>

 <div class="center" id="main">


  <div class="center_left" id="center_left">




                    <!-- star pub productList#产品列表-->
                    <div class="module" id="proclassmod" style="text-align: left">
                        <div class="moduleheadler">
                            <div class="moduleheadle_left">
                            </div>
                            <div class="moduleheadle_center">
                                <span>产品列表</span>
                            </div>
                            <div class="moduleheadle_right">
                            </div>
                        </div>
                        <div class="modulebody">
                            <div class="modulebody_left">
                            </div>
                            <div class="modulebody_center">

          
<div class="list1">
	    <div id="web-sidebar">
		  <!--{section name=p loop=$volist_producttreecategory}-->
		  <dl>
		    <dt class="part2" id="part1-id<!--{$volist_producttreecategory[p].cateid}-->"><b><a href="<!--{$volist_producttreecategory[p].url}-->"><!--{$volist_producttreecategory[p].catename}--></a></b></dt>
			<dd class="part3dom">
			  <!--{section name=c loop=$volist_producttreecategory[p].childcategory}-->
			  <h4 class="part3" id="part2-id<!--{$volist_producttreecategory[p].childcategory[c].cateid}-->"><a href="<!--{$volist_producttreecategory[p].childcategory[c].url}-->"><!--{$volist_producttreecategory[p].childcategory[c].catename}--></a></h4>
			  <!--{/section}-->
			</dd>
		  </dl>	
		  <!--{/section}-->
		</div><!-- #web-sidebar //-->
		<script type="text/javascript">
			$(document).ready(function(){
					var Plug1 = 0; 
					var Plug2 = 1; 
				var i = 0;
				var SideBar = $("#web-sidebar");
				var SideBar_dt = SideBar.find("dt.part2");
				var SideBar_dd = SideBar.find("dd.part3dom");
				var part1_dom = $("#part1-id0"); 
				var part2_dom = $("#part2-id0"); 
				var part_dom_dd = part1_dom.next("dd.part3dom");
					SideBar_dd.css("display","none");
					part1_dom.addClass("ondown");
					part2_dom.addClass("ondown");
				if(Plug1 == 1){ SideBar_dd.css("display","block"); SideBar_dt.addClass("part_on"); i = 1;} 
				
				if(Plug2 == 1 && part1_dom.length!=0){
						part_dom_dd.css("display","block");
						i = 1; 
				}
				
				SideBar_dt.click(function(){
					i++;if(i>1)i=0;
					i==1?SideBar_on($(this)):SideBar_out($(this));	
				});
				
		//****************


			});
			
			function SideBar_on(dom){
				var dd = dom.next("dd.part3dom")
					dom.addClass("part_on");
					dom.removeClass("part_out");
					dd.css("display","block");
			}
			
			function SideBar_out(dom){
				var dd = dom.next("dd.part3dom")
					dom.addClass("part_out");
					dom.removeClass("part_on");
					dd.css("display","none");
			}

			
		//************

		</script>


      </div><!-- $list1 //--->




                            </div>
                            <div class="modulebody_right">
                            </div>
                        </div>
                        <div class="modulebuttom">
                        </div>
                    </div>
                    <!-- end productList#产品列表-->




              <!-- star pub newsAdvice#成功案例-->
                    <div class="module" id="enterpriseInformation">
                        <div class="moduleheadler">
                            <div class="moduleheadle_left">
                            </div>
                            <div class="moduleheadle_center">
                                <span>成功案例</span>
                            </div>
                            <div class="moduleheadle_right">
                            </div>
                        </div>
                        <div class="modulebody">
                            <div class="modulebody_left">
                            </div>
                            <div class="modulebody_center">





  <ul class="AllUl">	 
		   <!--{foreach $volist_casecategory as $volist}-->
                      <li>
                                        <img src="<!--{$skinpath}-->/image/Tx_Ar3.gif" style="margin-left: 5px;" />
                                        <a href='<!--{$volist.url}-->' target="_blank" style="margin-left: 5px;"><!--{$volist.catename}--></a>
                      </li>
		  <!--{/foreach}-->
		  </ul>



     

                            </div>
                            <div class="modulebody_right">
                            </div>
                        </div>
                        <div class="modulebuttom">
                        </div>
                    </div>


     <!-- end newsAdvice#成功案例-->





                    <!-- star pub FriendlyLinks#招聘分类-->
                    <div class="module" id="enterpriseInformation">
                        <div class="moduleheadler">
                            <div class="moduleheadle_left">
                            </div>
                            <div class="moduleheadle_center">
                                <span>招聘分类</span>
                            </div>
                            <div class="moduleheadle_right">
                            </div>
                        </div>
                        <div class="modulebody">
                            <div class="modulebody_left">
                            </div>
                            <div class="modulebody_center">

 <ul class="AllUl">	 
		   <!--{foreach $volist_jobcategory as $volist}-->
                      <li>
                                        <img src="<!--{$skinpath}-->/image/Tx_Ar3.gif" style="margin-left: 5px;" />
                                        <a href='<!--{$volist.url}-->' target="_blank" style="margin-left: 5px;"><!--{$volist.catename}--></a>
                      </li>
		  <!--{/foreach}-->

		  </ul>
     

                            </div>
                            <div class="modulebody_right">
                            </div>
                        </div>
                        <div class="modulebuttom">
                        </div>
                    </div>
                    <!-- end FriendlyLinks#招聘分类-->
                    <!-- star pub qqShow#在线客服-->
                    <div class="module" id="qqShow">
                        <div class="moduleheadler">
                            <div class="moduleheadle_left">
                            </div>
                            <div class="moduleheadle_center">
                                <span>联系我们</span>
                            </div>
                            <div class="moduleheadle_right">
                            </div>
                        </div>
                        <div class="modulebody">
                            <div class="modulebody_left">
                            </div>
                            <div class="modulebody_center">
                                <ul style="line-height:17px">
                               <li class="indexcontact" id="indexcontact">
	      



   <!--{$config.contact}-->


	          
                           </li>
                                </ul>
                            </div>
                            <div class="modulebody_right">
                            </div>
                        </div>
                        <div class="modulebuttom">
                        </div>
                    </div>
                    <!-- end qqShow#在线客服-->
                </div>


                <div class="center_center" id="center_center">
                    <!-- star pub photoSlide#幻灯片-->
                    <div class="module" id="photoSlide">
                        <div class="moduleheadler">
                            <div class="moduleheadle_left">
                            </div>
                            <div class="moduleheadle_center">
                                <span></span>
                            </div>
                            <div class="moduleheadle_right">
                            </div>
                        </div>
                        <div class="modulebody">
                            <div class="modulebody_left">
                            </div>
                            <div class="modulebody_center">
                                <table width="483" align="center" style="margin-left: -3px; margin-top: -3px">
                                    <tr>
                                        <td>

               
  <!--{if $ads_zone1}-->
  <div id="flash"> 
    <div class="flash">



	  <!--{if $zone_silde1=="1"}-->
		<div id="slide_wp" style="overflow:hidden;width:478px;height:auto;position:relative;clear:both;boder:0px;background-color:#ffffff;margin:auto;">
		  <div id="MSClassBox">
			<ul id="ContentID" style="margin:0px;padding:0px;">
			  <!--{foreach $ads_zone1 as $volist}-->
			  <li><a href="<!--{$volist.url}-->"><img border="0" src="<!--{$volist.uploadfiles}-->" width="478px" height="172px" title="<!--{$volist.adsname}-->" alt="<!--{$volist.adsname}-->" /></a></li>
			  <!--{/foreach}-->
			</ul>
		  </div>
		  <ul id="TabID" style="display:none;">
			<!--{foreach $ads_zone1 as $volist}-->
			<li class=""><!--{$volist.i}--></li>
			<!--{/foreach}-->
		  </ul>
		</div>
		<script type="text/javascript">
		new Marquee(
		{
			MSClassID : "MSClassBox",
			ContentID : "ContentID",
			TabID	  : "TabID",
			Direction : 2,
			Step	  : 0.3,
			Width	  : 478,
			Height	  : 172,
			Timer	  : 20,
			DelayTime : 2000,
			WaitTime  : 0,
			ScrollStep: 610,
			SwitchType: 1,
			AutoStart : 1
		})
		</script>
	  <!--{else}-->


	  <div id="banner">
      <!--{foreach $ads_zone1 as $volist}-->
      <a href="<!--{$volist.url}-->"><img src="<!--{$volist.uploadfiles}-->" width="<!--{$volist.width}-->" height="<!--{$volist.height}-->" title="<!--{$volist.adsname}-->" alt="<!--{$volist.adsname}-->" /></a>
	  <!--{/foreach}-->
	  </div>

	  <!--{/if}-->



	</div>
  </div><!-- #flash //-->
  <!--{/if}-->

                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="modulebody_right">
                            </div>
                        </div>
                        <div class="modulebuttom">
                        </div>
                    </div>
                    <!-- end photoSlide#幻灯片-->
                    <!-- star pub newDisplay#新品展示-->
                    <div class="module" id="newDisplay">
                        <div class="moduleheadler">
                            <div class="moduleheadle_left">
                            </div>
                            <div class="moduleheadle_center">
                                <span>新品展示</span>
                            </div>
                            <div class="moduleheadle_right">
                            </div>
                        </div>
                        <div class="modulebody">
                            <div class="modulebody_left">
                            </div>
                            <div class="modulebody_center">
                                <div id="newproductlist">
                              <ul>
	           <!--{foreach $volist_newproduct as $volist}-->




                                        <li>
                                            <div>
                                                <div class="productlist_img">
                                                    <div class="border_img">
                                                        <a href="<!--{$volist.url}-->" target="_blank">
                                                            <img src="<!--{$volist.thumbfiles}-->" onload="javascript:DrawImage(this,140,100);" width="140px"
                                                                heigth="100px" /></a></div>
                                                </div>
                                                <div style="clear: both">
                                                </div>
                                                <div class="productlist_title1">
                                                    <div class="productlist_title">
                                                        <a href="<!--{$volist.url}-->" target="_blank"><!--{$volist.productname}--></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                         <!--{/foreach}-->
                             </ul>
                         </div>
                     </div>

                   <div class="modulebody_right">
                            </div>
                        </div>
                        <div class="modulebuttom">
                        </div>
                    </div>
                    <!-- end newDisplay#新品展示-->
                    <!-- star pub news#新闻-->
                    <div class="module" id="news">
                        <div class="moduleheadler">
                            <div class="moduleheadle_left">
                            </div>
                            <div class="moduleheadle_center">
                                <span>新闻</span>
                            </div>
                            <div class="moduleheadle_right">
                            </div>
                        </div>
                        <div class="modulebody">
                            <div class="modulebody_left">
                            </div>
                            <div class="modulebody_center">





                <div>
 
   <script type="text/javascript">
            $(function() {
              
                $('#container-11 ul').tabs({ event: 'mouseover' }).find('a').click(function() {
                    return false;
                });
               $("#container-11 ul li:first-child").find('span').css("min-width","103px");
	});
	
    </script>
 

                                    <div id="container-11">
                                        <ul style="padding-left: 0px;">
                                            <li><a href="#fragment-0"><span>企业简介</span></a></li>
                                                                                                                                    <li><a href="#fragment-99"><span>最新新闻</span></a></li>
                                                                                        <li><a href="#fragment-100"><span>最新招聘 </span></a></li>
                                                                                        <li><a href="#fragment-101"><span>最新方案</span></a></li>
                                                                                    </ul>
                                    </div>
                                    <div id="fragment-0">
                                        <div class="mb" id="todaytabscon" style="height: 160px">
                                           
	    <div class="editor" style="text-align:left;">
		  <p>
		  <!--{if $ads_zone2}-->
		  <!--{foreach $ads_zone2 as $volist}-->
		  <img width="<!--{$volist.width}-->" height="<!--{$volist.height}-->" hspace="2" align="left" vspace="2" src="<!--{$volist.uploadfiles}-->" />
		  <!--{/foreach}-->
		  <!--{/if}-->
		 <!--{$config.about|filterhtml:210}-->...</p>
		</div>

 </div>
               </div>
                                                                        <div id="fragment-99">
                                        <ul>
					<li>
                                            <table width="98%" border="0" cellspacing="0" cellpadding="0">



              


	    <!--{foreach $volist_newinfo as $volist}-->





                                          <tr height="28">
                                                    <td style="background: url(<!--{$skinpath}-->Images/bg2.gif) repeat-x left bottom;">
                                                        &nbsp;<img src="<!--{$skinpath}-->images/arr.gif" width="11" height="14" align="absmiddle" />&nbsp;&nbsp;<a
                                                            href="<!--{$volist.url}-->"><!--{$volist.sort_title}--></a></td>
                                                    <td align="center" style="background: url(<!--{$skinpath}-->Images/bg2.gif) repeat-x left bottom;
                                                        color: #999999">
                                                       <!--{$volist.delimitname}-->
                                                    </td>
                                                </tr>



		<!--{/foreach}-->





                                               
                                                                                            </table>



</li>








                                        </ul>
                                    </div>
                                                                        <div id="fragment-100">
                                        <ul>
					<li>
                <table width="98%" border="0" cellspacing="0" cellpadding="0">
	    <!--{foreach $volist_newjob as $volist}-->
                                          <tr height="28">
                                                    <td style="background: url(<!--{$skinpath}-->Images/bg2.gif) repeat-x left bottom;">
                                                        &nbsp;<img src="<!--{$skinpath}-->images/arr.gif" width="11" height="14" align="absmiddle" />&nbsp;&nbsp;<a
                                                            href="<!--{$volist.url}-->"><!--{$volist.title}--></a></td>
                                                    <td align="center" style="background: url(<!--{$skinpath}-->Images/bg2.gif) repeat-x left bottom;
                                                        color: #999999">
                                                       <!--{$volist.delimitname}-->
                                                    </td>
                                                </tr>
		<!--{/foreach}-->

</table>
</li>
                                        </ul>
                                    </div>
                                                                        <div id="fragment-101">
                                        <ul>
					<li>
                                            <table width="98%" border="0" cellspacing="0" cellpadding="0">
	    <!--{foreach $volist_newsolution as $volist}-->
                                          <tr height="28">
                                                    <td style="background: url(<!--{$skinpath}-->Images/bg2.gif) repeat-x left bottom;">
                                                        &nbsp;<img src="<!--{$skinpath}-->images/arr.gif" width="11" height="14" align="absmiddle" />&nbsp;&nbsp;<a
                                                            href="<!--{$volist.url}-->"><!--{$volist.title}--></a></td>
                                                    <td align="center" style="background: url(<!--{$skinpath}-->Images/bg2.gif) repeat-x left bottom;
                                                        color: #999999">
                                                       <!--{$volist.delimitname}-->
                                                    </td>
                                                </tr>
		<!--{/foreach}-->
					    </table>
					    </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="modulebody_right">
                            </div>
                        </div>
                        <div class="modulebuttom">
                        </div>
                    </div>
                    <!-- end news#新闻-->
                </div>

                <div class="center_right" id="center_right">
                    <!-- star pub siteNotice#新闻列表-->
                    <div class="module" id="company">
                        <div class="moduleheadler">
                            <div class="moduleheadle_left">
                            </div>
                            <div class="moduleheadle_center">
                                <span>新闻列表</span>
                            </div>
                            <div class="moduleheadle_right">
                            </div>
                        </div>
                        <div class="modulebody">
                            <div class="modulebody_left">
                            </div>
                            <div class="modulebody_center">
                                <ul class="AllUl">
                              
                  <!--{foreach $volist_infocategory as $volist}-->
		  <li style="line-height:16px;">
                      <img src="<!--{$skinpath}-->/image/Tx_Ar3.gif" style="margin-left: 5px;" />
                                        <a href="<!--{$volist.url}-->" target="_blank" style="margin-left: 5px;"><!--{$volist.catename}--></a>
					</li>
                                   <!--{/foreach}-->


                                </ul>
     
                            </div>
                            <div class="modulebody_right">
                            </div>
                        </div>
                        <div class="modulebuttom">
                        </div>
                    </div>
                    <!-- end newlist#新闻列表-->
                  
                    <!-- star pub searchModule#搜索-->

<script type="text/javascript">
function searchres()
{
var vartype=1;
if(document.getElementById("rdseachtype1").checked)
{
vartype=1;
}
else
{
  if(document.getElementById("rdseachtype2").checked)
  {
  vartype=2;
  }
  else
   if(document.getElemtentById("rdseachtype3").checked)
   {
    vartype=3;
   }
}
 var varkey=document.getElementById("keyword").value;
 window.location.href="SeachResultHtml.aspx?seachtype="+vartype+"&key="+varkey;
 }
</script>
                    <div class="module" id="searchModule">
                        <div class="moduleheadler">
                            <div class="moduleheadle_left">
                            </div>
                            <div class="moduleheadle_center">
                                <span>搜索</span>
                            </div>
                            <div class="moduleheadle_right">
                            </div>
                        </div>
                        <div class="modulebody">
                            <div class="modulebody_left">
                            </div>
                            <div class="modulebody_center">
                              
 <ul id="search">
	    <form method='post' name='search_form' action='<!--{$url_product}-->' style="margin:0px;">
	    <li><span class='parasearch_title'>内容</span><span class='parasearch_input'><input type='text' name='keyword' id="keyword"  /></span></li>
		<li><span class='parasearch_search'><input class='searchimage' type='image' src='<!--{$skinpath}-->images/navserach.gif' /></span></li>
		</form>
	  </ul>


                            </div>
                            <div class="modulebody_right">
                            </div>
                        </div>
                        <div class="modulebuttom">
                        </div>
                    </div>
                    <!-- end searchModule#搜索-->
                    <!-- star pub onlineSurvey#解决方案分类-->

  
                    <div class="module" id="enterpriseInformation">
                        <div class="moduleheadler">
                            <div class="moduleheadle_left">
                            </div>
                            <div class="moduleheadle_center">
                                <span>解决方案分类</span>
                            </div>
                            <div class="moduleheadle_right">
                            </div>
                        </div>
                        <div class="modulebody">
                            <div class="modulebody_left">
                            </div>
                            <div class="modulebody_center" >


                                   <ul class="AllUl">	 
		   <!--{foreach $volist_solutioncategory as $volist}-->
                      <li>
                                        <img src="<!--{$skinpath}-->/image/Tx_Ar3.gif" style="margin-left: 5px;" />
                                        <a href='<!--{$volist.linkurl}-->' target="_blank" style="margin-left: 5px;"><!--{$volist.catename}--></a>
                      </li>
		  <!--{/foreach}-->
		  </ul>



                            </div>
                            <div class="modulebody_right">
                            </div>
                        </div>
                        <div class="modulebuttom">
                        </div>
                    </div>
					
                    <!-- end onlineSurvey#解决方案分类-->






                    <!-- star pub onlineSurvey#友情连接-->

  
                    <div class="module" id="enterpriseInformation">
                        <div class="moduleheadler">
                            <div class="moduleheadle_left">
                            </div>
                            <div class="moduleheadle_center">
                                <span>友情连接</span>
                            </div>
                            <div class="moduleheadle_right">
                            </div>
                        </div>
                        <div class="modulebody">
                            <div class="modulebody_left">
                            </div>
                            <div class="modulebody_center" >
                                   <ul class="AllUl">	 
<!--{foreach $volist_fontlink as $volist}-->


 <li>
                                        <img src="<!--{$skinpath}-->/image/Tx_Ar3.gif" style="margin-left: 5px;" />
                                        <a href='<!--{$volist.linkurl}-->' target="_blank" style="margin-left: 5px;"><!--{$volist.linktitle}--> </a>
                                    </li>

<!--{/foreach}-->


                                </ul>





                            </div>
                            <div class="modulebody_right">
                            </div>
                        </div>
                        <div class="modulebuttom">
                        </div>
                    </div>
					
                    <!-- end onlineSurvey#友情连接-->













                </div>
            </div>



            <div class="footer">
                <!-- star pub buttom#-->
          

  <!--{include file="<!--{$tplpath}-->block_footer.tpl"}-->


                <!-- end pub buttom#-->
            </div>
    
<script type="text/javascript" src="<!--{$skinpath}-->js/screen.js"></script>

<!--{include file="<!--{$tplpath}-->block_onlinechat.tpl"}-->
</body>
</html>