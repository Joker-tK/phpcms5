<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title><!--{$page_title}-->-<!--{$copyright_header}--></title>
<meta name="description" content="<!--{$page_description}-->" />
<meta name="keywords" content="<!--{$page_keyword}-->" />
<meta name="author" content="<!--{$copyright_author}-->" />
<script src="<!--{$skinpath}-->js/AC_RunActiveContent.js" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/Html.js" type="text/javascript"></script>
<link href="<!--{$skinpath}-->style/css.css" id="Skin" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<!--{$skinpath}-->style/cssset.js"></script>

<script type='text/javascript' src='<!--{$skinpath}-->js/command.js'></script>
<script src="<!--{$skinpath}-->js/jquery-1.4.4.min.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/downnav.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/jquery.cross-slide.js" language="javascript" type="text/javascript"></script>


<link rel="alternate" type="application/rss+xml" title="订阅该空间的 良 精 文章" href="../rss.xml">
</head>
<body>
    
<div class="divbody" id="wrap">
<div class="headler" id="headler">

                <!-- star pub header# //-->
  <!--{include file="<!--{$tplpath}-->block_head.tpl"}-->
                <!-- end header# //-->

   </div>
</div>

        <div class="center">
            <div class="center_left">

<!-- star pub 案例分类-->
<div class="module" id="login">
    <div class="moduleheadler">
        <div class="moduleheadle_left">
        </div>
        <div class="moduleheadle_center">
            <span>案例分类</span>
        </div>
        <div class="moduleheadle_right">
        </div>
    </div>
    <div class="modulebody">
        <div class="modulebody_left">
        </div>
        <div class="modulebody_center">

	  <div class="webnav"> 
        <div id="web-sidebar">
	  <!--{foreach $volist_casecategory as $volist}-->
          <dl>
		    <dt class="part2" id="part1-id<!--{$volist.cateid}-->"><a href="<!--{$volist.url}-->"><!--{$volist.catename}--></a></dt>
		  </dl>	
		  <!--{/foreach}-->
        </div>
      </div>
        </div>
        <div class="modulebody_right">
        </div>
    </div>
    <div class="modulebuttom">
    </div>
</div>
  <!-- end 案例分类-->

      <!--{include file="<!--{$tplpath}-->block_contact.tpl"}--> 
</div>
            <div class="main_right">
                <!--企业简介内容 start-->
                <div class="module" id="enterpriseInformationDetail">
    <div class="moduleheadler">
        <div class="moduleheadle_left">
        </div>
        <div class="moduleheadle_center">
            <span>&nbsp;当前位置：<a href="<!--{$url_index}-->">首 页</a>&nbsp;&nbsp;>>&nbsp;&nbsp;<!--{$navigation}--></span>
        </div>
        <div class="moduleheadle_right">
        </div>
    </div>
    <div class="modulebody">
        <div class="modulebody_left">
        </div>
        <div class="modulebody_center">
 <div class="webcontent">
	    <div id="img_list">
		  <style type="text/css">#img_list #plug1 li{ width:158px; height:183px;}</style>
		  <div id="plug1">
		    <ul>
			  <!--{foreach $case as $volist}-->
			  <li>
			    <span class='info_img' ><a href='<!--{$volist.url}-->' target='_blank'  ><img src="<!--{$volist.thumbfiles}-->" alt="<!--{$volist.title}-->" onload="javascript:DrawImage(this,'150','150');" /></a></span><span class='info_title' ><a title='<!--{$volist.title}-->' href="<!--{$volist.url}-->" target='_blank' ><!--{$volist.title}--></a></span>
			  </li>
			  <!--{/foreach}-->
			</ul>
			<div style=" clear:both;"></div>
		  </div>
		  <!--{if $showpage!=""}-->
		  <div class="clear"></div>
		  <div class="pageController"><!--{$showpage}--></div> 
		  <!--{/if}-->
		</div>
	  </div>
        </div>
        <div class="modulebody_right">
        </div>
    </div>
    <div class="modulebuttom">
    </div>
</div>

                <!--企业简介 end-->
            </div>
        </div>


            <div class="footer">
                <!-- star pub buttom#-->
          

  <!--{include file="<!--{$tplpath}-->block_footer.tpl"}-->


                <!-- end pub buttom#-->
            </div>
    
	<script type="text/javascript" src="<!--{$skinpath}-->js/screen.js"></script>
</body>
</html>