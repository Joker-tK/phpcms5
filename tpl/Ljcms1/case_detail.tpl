<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title><!--{$page_title}-->-<!--{$copyright_header}--></title>
<meta name="description" content="<!--{$page_description}-->" />
<meta name="keywords" content="<!--{$page_keyword}-->" />
<meta name="author" content="<!--{$copyright_author}-->" />
<script src="<!--{$skinpath}-->js/AC_RunActiveContent.js" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/Html.js" type="text/javascript"></script>
<link href="<!--{$skinpath}-->style/css.css" id="Skin" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<!--{$skinpath}-->style/cssset.js"></script>

<script type='text/javascript' src='<!--{$skinpath}-->js/command.js'></script>
<script src="<!--{$skinpath}-->js/jquery-1.4.4.min.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/downnav.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/jquery.cross-slide.js" language="javascript" type="text/javascript"></script>


<link rel="alternate" type="application/rss+xml" title="订阅该空间的 良 精 文章" href="../rss.xml">
</head>
<body>
    
<div class="divbody" id="wrap">
<div class="headler" id="headler">

                <!-- star pub header# //-->
  <!--{include file="<!--{$tplpath}-->block_head.tpl"}-->
                <!-- end header# //-->

   </div>
</div>

        <div class="center">
            <div class="center_left">

<!-- star pub 案例分类-->
<div class="module" id="login">
    <div class="moduleheadler">
        <div class="moduleheadle_left">
        </div>
        <div class="moduleheadle_center">
            <span>案例分类</span>
        </div>
        <div class="moduleheadle_right">
        </div>
    </div>
    <div class="modulebody">
        <div class="modulebody_left">
        </div>
        <div class="modulebody_center">

	  <div class="webnav"> 
        <div id="web-sidebar">
	  <!--{foreach $volist_casecategory as $volist}-->
          <dl>
		    <dt class="part2" id="part1-id<!--{$volist.cateid}-->"><a href="<!--{$volist.url}-->"><!--{$volist.catename}--></a></dt>
		  </dl>	
		  <!--{/foreach}-->
        </div>
      </div>
        </div>
        <div class="modulebody_right">
        </div>
    </div>
    <div class="modulebuttom">
    </div>
</div>
  <!-- end 案例分类-->

      <!--{include file="<!--{$tplpath}-->block_contact.tpl"}--> 
</div>
            <div class="main_right">
                <!--案例内容 start-->
                <div class="module" id="enterpriseInformationDetail">
    <div class="moduleheadler">
        <div class="moduleheadle_left">
        </div>
        <div class="moduleheadle_center">
            <span>&nbsp;当前位置：<a href="<!--{$url_index}-->">首 页</a>&nbsp;&nbsp;>>&nbsp;&nbsp;<!--{$navigation}--></span>
        </div>
        <div class="moduleheadle_right">
        </div>
    </div>
    <div class="modulebody">
        <div class="modulebody_left">
        </div>
        <div class="modulebody_center">
 <div class="webcontent">
	    <div id="showproduct">
		  <dl>
		    <dt style="width:364px;">
			  <!--{if $case.uploadfiles!=''}-->
			  <span class='info_img' id='imgqwe'><a id='view_bigimg' href='<!--{$case.uploadfiles}-->' title="查看大图" target='_blank'><img id='view_img' border='0' onload="javascript:DrawImage(this,'200','200');" src='<!--{$case.thumbfiles}-->'></a></span>
			  <script type='text/javascript'>var zoomImagesURI   = '<!--{$skinpath}-->images/zoom/';</script>
			  <script src='<!--{$skinpath}-->js/zoom.js' language='javascript' type='text/javascript'></script>
			  <script src='<!--{$skinpath}-->js/zoomhtml.js' language='javascript' type='text/javascript'></script>
			  <script type='text/javascript'>	window.onload==setupZoom();	</script>
			  <!--{else}-->
			  <span class='info_img'><img id='view_img' border='0' onload="javascript:DrawImage(this,'200','200');" src='<!--{$case.thumbfiles}-->'></span>
			  <!--{/if}-->
			</dt>
			
			<dd style="width:326px;">
			  <ul class="list">
			    <li class="title"><h1>名称 ：<span><!--{$case.title}--></span></h1></li>
				<li><b>分类 : </b><span><a href="<!--{$case.caturl}-->"><!--{$case.catename}--></a></span></li>
				<li><b>日期 : </b><span><!--{$case.timeline|date_format:'%Y-%m-%d'}--></span></li>
			  </ul>
			</dd>
		  </dl>
		  
		  <div style="clear:both;"></div>
		  <h3 class="hr"><span>案例介绍</span></h3>
		  <div class="text editor">
		    <!--{$case.content|htmlencode}-->
		    <script type="text/javascript"><!--
google_ad_client = "pub-3406709034811813";
/* 468x60, 创建于 09-5-11 */
google_ad_slot = "6276836029";
google_ad_width = 468;
google_ad_height = 60;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js"> 
</script>
		  </div>
		  <div class="hits">点击次数：<span><script src="<!--{$urlpath}-->data/include/casehits.php?id=<!--{$case.caseid}-->"></script></span>&nbsp;&nbsp;发布日期：<!--{$case.timeline|date_format:"%Y/%m/%d"}-->&nbsp;&nbsp;【<a href='javascript:window.print()'>打印此页</a>】&nbsp;&nbsp;【<a href='javascript:self.close()'>关闭</a>】</div>
		  
		  <div class="page">上一篇：<!--{$previous_item}--><br>下一条：<!--{$next_item}--></div>
		</div>
      </div>
        </div>
        <div class="modulebody_right">
        </div>
    </div>
    <div class="modulebuttom">
    </div>
</div>

                <!--案例 end-->
            </div>
        </div>


            <div class="footer">
                <!-- star pub buttom#-->
          

  <!--{include file="<!--{$tplpath}-->block_footer.tpl"}-->


                <!-- end pub buttom#-->
            </div>
    
	<script type="text/javascript" src="<!--{$skinpath}-->js/screen.js"></script>
</body>
</html>