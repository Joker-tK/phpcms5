<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title><!--{$page_title}-->-<!--{$copyright_header}--></title>
<meta name="description" content="<!--{$page_description}-->" />
<meta name="keywords" content="<!--{$page_keyword}-->" />
<meta name="author" content="<!--{$copyright_author}-->" />
<script src="<!--{$skinpath}-->js/AC_RunActiveContent.js" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/Html.js" type="text/javascript"></script>
<link href="<!--{$skinpath}-->style/css.css" id="Skin" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<!--{$skinpath}-->style/cssset.js"></script>
<script type='text/javascript' src='<!--{$skinpath}-->js/command.js'></script>
<script src="<!--{$skinpath}-->js/jquery-1.4.4.min.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/downnav.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/jquery.cross-slide.js" language="javascript" type="text/javascript"></script>
<link rel="alternate" type="application/rss+xml" title="订阅该空间的 良 精 文章" href="../rss.xml">
<script language="javascript">
function checkform(){
	if($("#bookuser").val()==""){
		alert("姓名不能为空.");
		return false;
	}
	if($("#companyname").val()==""){
		alert("公司不能为空.");
		return false;
	}
	if($("#address").val()==""){
		alert("公司地址不能为空.");
		return false;
	}
	if($("#trade").val()==""){
		alert("所属行业不能为空.");
		return false;
	}
	if($("#jobs").val()==""){
		alert("职务不能为空.");
		return false;
	}
	if($("#telephone").val()==""){
		alert("联系电话不能为空.");
		return false;
	}
	if($("#content").val()==""){
		alert("留言内容不能为空.");
		return false;
	}
}
</script>
</head>
<body>
    
<div class="divbody" id="wrap">
<div class="headler" id="headler">

                <!-- star pub header# //-->
  <!--{include file="<!--{$tplpath}-->block_head.tpl"}-->
                <!-- end header# //-->

   </div>
</div>

        <div class="center">
            <div class="center_left">

<!-- star pub login#关于我们-->
<div class="module" id="login">
    <div class="moduleheadler">
        <div class="moduleheadle_left">
        </div>
        <div class="moduleheadle_center">
            <span>关于我们</span>
        </div>
        <div class="moduleheadle_right">
        </div>
    </div>
    <div class="modulebody">
        <div class="modulebody_left">
        </div>
        <div class="modulebody_center">
            <ul class="AllUl">
                        <li>
                            <img src="<!--{$skinpath}-->image/Tx_Ar3.gif" style="margin-left: 5px;" />
                            <a href="<!--{$url_about}-->" target="_blank" style="margin-left: 5px;">
                            <!--{$lang_about}-->
                            </a></li>
	 <!--{foreach $volist_page as $volist}-->
                        <li>
                            <img src="<!--{$skinpath}-->image/Tx_Ar3.gif" style="margin-left: 5px;" />
                            <a href="<!--{$volist.url}-->" target="_blank" style="margin-left: 5px;">
                              <!--{$volist.title}-->
                            </a></li>
		  <!--{/foreach}-->
            </ul>
        </div>
        <div class="modulebody_right">
        </div>
    </div>
    <div class="modulebuttom">
    </div>
</div>
  <!-- end login#关于我们-->
<!--{include file="<!--{$tplpath}-->block_info.tpl"}--> 
</div>
            <div class="main_right">
                <!--企业简介内容 start-->
                <div class="module" id="enterpriseInformationDetail">
    <div class="moduleheadler">
        <div class="moduleheadle_left">
        </div>
        <div class="moduleheadle_center">
            <span>&nbsp;当前位置：<!--{$lang_about}-->
                
            </span>
        </div>
        <div class="moduleheadle_right">
        </div>
    </div>
    <div class="modulebody">
        <div class="modulebody_left">
        </div>
        <div class="modulebody_center">
         	  <div class="webcontent">
	    <div class="showtext editor">
		  <p>

			<form method="post" action="<!--{$url_guestbook}-->" name="myform" id="myform" onsubmit="return checkform();" />
			<input type="hidden" name="action" value="saveadd" />
			<table cellpadding='8' cellspacing='8' border="0" style="line-height:30px;" width="80%" align="center">
			  <tr>
				<td colspan='2' class='topTd'></td>
			  </tr>
			  <tr>
				<td class='hback_1' width='15%'><b>姓 名：</b></td>
				<td class='hback' width='85%'><input name="bookuser" id="bookuser" type="text" size="15" />  <select name="gender" id="gender"><option value="1">先生</option><option value="2">女士</option></select></td>
			  </tr>
			  <tr>
				<td><b>公 司：</b></td>
				<td><input name="companyname" id="companyname" type="text" size="30" /> <font color='red'>*</font></td>
			  </tr>
			  <tr>
				<td><b>地 址：</b></td>
				<td><input name="address" id="address" type="text" size="30" /> <font color='red'>*</font></td>
			  </tr>
			  <tr>
				<td><b>行 业：</b></td>
				<td><input name="trade" id="trade" type="text" size="20" /> <font color='red'>*</font></td>
			  </tr>
			  <tr>
				<td><b>职 务：</b></td>
				<td><input name="jobs" id="jobs" type="text" size="20" /> <font color='red'>*</font></td>
			  </tr>
			  <tr>
				<td><b>电 话：</b></td>
				<td><input name="telephone" id="telephone" type="text" size="25" /> <font color='red'>*</font></td>
			  </tr>
			  <tr>
				<td><b>手 机：</b></td>
				<td><input name="mobile" id="mobile" type="text" size="25" /> <font color='red'></font></td>
			  </tr>
			  <tr>
				<td><b>传 真：</b></td>
				<td><input name="fax" id="fax" type="text" size="25" /> <font color='red'></font></td>
			  </tr>
			  <tr>
				<td><b>E-mail：</b></td>
				<td><input name="email" id="email" type="text" size="35" /> <font color='red'></font></td>
			  </tr>
			  <tr>
				<td><b>留言内容：</b></td>
				<td><textarea name="content" id="content" style="width:75%;height:100px;overflow:auto;"></textarea> <font color="red">*</font></td>
			  </tr>
			  <tr>
				<td colspan='2' align="center" height="30px;"><input type="submit" name="btn_save" value="提交保存" />&nbsp;&nbsp;<input type="reset" name="btn_reset" value="重 置" /></td>
			  </tr>
			</table>

			</form>


          </p>
	  <script type="text/javascript"><!--
google_ad_client = "pub-3406709034811813";
/* 468x60, 创建于 09-5-11 */
google_ad_slot = "6276836029";
google_ad_width = 468;
google_ad_height = 60;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js"> 
</script>
		</div>
      </div>
        </div>
        <div class="modulebody_right">
        </div>
    </div>
    <div class="modulebuttom">
    </div>
</div>

                <!--企业简介 end-->
            </div>
        </div>


            <div class="footer">
                <!-- star pub buttom#-->
          

  <!--{include file="<!--{$tplpath}-->block_footer.tpl"}-->


                <!-- end pub buttom#-->
            </div>
    
	<script type="text/javascript" src="<!--{$skinpath}-->js/screen.js"></script>
</body>
</html>