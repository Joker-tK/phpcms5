<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title><!--{$page_title}-->-<!--{$copyright_header}--></title>
<meta name="description" content="<!--{$page_description}-->" />
<meta name="keywords" content="<!--{$page_keyword}-->" />
<meta name="author" content="<!--{$copyright_author}-->" />
<link rel="stylesheet" type="text/css" href="<!--{$skinpath}-->style/main.css" />
<link rel="stylesheet" type="text/css" href="<!--{$skinpath}-->style/css.css" />
<script type='text/javascript' src='<!--{$skinpath}-->js/command.js'></script>
<script src="<!--{$skinpath}-->js/jquery-1.4.4.min.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/downnav.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/jquery.cross-slide.js" language="javascript" type="text/javascript"></script>
<!--[if IE 6]>
<script src="<!--{$skinpath}-->js/deletepng.js" language="javascript" type="text/javascript"></script>
<script type="text/javascript">
DD_belatedPNG.fix('.bg,img'); 
</script>
<![endif]-->
</head>
<body>
<div id="wrap">
  <!--{include file="<!--{$tplpath}-->block_head.tpl"}-->
  <!--{include file="<!--{$tplpath}-->block_banner.tpl"}-->

  <div id="web">
  
    <div id="left">
	  <h3 class="title"><span><a href="<!--{$url_index}-->">首 页</a>&nbsp;&nbsp;>>&nbsp;&nbsp;<!--{$navigation}--></span></h3>
	  <div class="webcontent">

        <div id="job_list">
		  <dl id="plug1">
		    <dt>
			  <span>浏览次数</span>
			  <span>查看详细</span>
			  <span>发布日期</span>
			  <span>工作地点</span>
			  <span>招聘人数</span>
			  招聘职位
			</dt>
			<!--{foreach $job as $volist}-->
			<dd>
			  <span><!--{$volist.hits}--></span>
			  <span><a href="<!--{$volist.url}-->">查看详细</a></span>
			  <span><!--{$volist.timeline|date_format:'%Y/%m/%d'}--></span>
			  <span><!--{$volist.workarea}--></span>
			  <span><!--{$volist.number}-->人</span>
			  <a href="<!--{$volist.url}-->"><!--{$volist.title}--></a>
			</dd>
			<!--{/foreach}-->
		  </dl>
        </div>
		  <!--{if $showpage!=""}-->
		  <div class="clear"></div>
		  <div class="pageController"><!--{$showpage}--></div> 
		  <!--{/if}-->
	  </div>

	</div><!-- $left //-->

    <div id="right">
      <h3 class="title"><span>招聘分类</span></h3>
	  <div class="webnav"> 
        <div id="web-sidebar">
		  <!--{foreach $volist_jobcategory as $volist}-->
          <dl>
		    <dt class="part2" id="part1-id6"><a href="<!--{$volist.url}-->"><!--{$volist.catename}--></a></dt>
		  </dl>	
		  <!--{/foreach}-->
        </div>
      </div>

      <!--{include file="<!--{$tplpath}-->block_info.tpl"}--> 
      <!--{include file="<!--{$tplpath}-->block_contact.tpl"}-->  

    </div><!--#right //-->
    <div style="clear:both;"></div>
  </div>

  <!--{include file="<!--{$tplpath}-->block_footer.tpl"}-->
</div>
<script type="text/javascript" src="<!--{$skinpath}-->js/screen.js"></script>
</body>
</html>