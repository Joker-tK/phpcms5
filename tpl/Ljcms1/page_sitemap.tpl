<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title><!--{$page_title}-->-<!--{$copyright_header}--></title>
<meta name="description" content="<!--{$page_description}-->" />
<meta name="keywords" content="<!--{$page_keyword}-->" />
<meta name="author" content="<!--{$copyright_author}-->" />
<link rel="stylesheet" type="text/css" href="<!--{$skinpath}-->style/main.css" />
<link rel="stylesheet" type="text/css" href="<!--{$skinpath}-->style/css.css" />
<script type='text/javascript' src='<!--{$skinpath}-->js/command.js'></script>
<script src="<!--{$skinpath}-->js/jquery-1.4.4.min.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/downnav.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/jquery.cross-slide.js" language="javascript" type="text/javascript"></script>
<!--[if IE 6]>
<script src="<!--{$skinpath}-->js/deletepng.js" language="javascript" type="text/javascript"></script>
<script type="text/javascript">
DD_belatedPNG.fix('.bg,img'); 
</script>
<![endif]-->
</head>
<body>
<div id="wrap">
  <!--{include file="<!--{$tplpath}-->block_head.tpl"}-->
  <!--{include file="<!--{$tplpath}-->block_banner.tpl"}-->

  <div id="web">
    
	<div id="left">
	  
	  <h3 class="title"><span>网站地图</span></h3>
	  <div class="webcontent">
	    
		<div id="sitemap">
		  <dl class='sitemapclass'>
		    <dd class='sitemapclass1'><h2 style='font-size:13px;'><a href='<!--{$url_about}-->'>关于我们</a></h2></dd>
			<!--{foreach $volist_page as $value}-->
		    <dd class='sitemapclass2'><h3 style='font-weight:normal; font-size:12px;'><a href='<!--{$volist.url}-->'><!--{$volist.title}--></a></h3><div></div></dd>
			<!--{/foreach}-->
		    <dd class='sitemapclass2'><h3 style='font-weight:normal; font-size:12px;'><a href='<!--{$url_contact}-->'>联系我们</a></h3><div></div></dd>
		  </dl>
          
		  <dl class='sitemapclass'>
		    <dd class='sitemapclass1'><h2 style='font-size:13px;'><a href='<!--{$url_info}-->'><!--{$lang_info}--></a></h2></dd>
			<!--{foreach $volist_infocategory as $volist}-->
			<dd class='sitemapclass2'><h3 style='font-weight:normal; font-size:12px;'><a href='<!--{$volist.url}-->'><!--{$volist.catename}--></a></h3><div></div></dd>
			<!--{/foreach}-->
		  </dl>
		 
		  <dl class='sitemapclass'>
		    <dd class='sitemapclass1'><h2 style='font-size:13px;'><a href='<!--{$url_product}-->'><!--{$lang_product}--></a></h2></dd>
		   <!--{section name=p loop=$volist_producttreecategory}-->
		    <dd class='sitemapclass2'>
		      <h3 style='font-weight:normal; font-size:12px;'><a href="<!--{$volist_producttreecategory[p].url}-->"><!--{$volist_producttreecategory[p].catename}--></a></h3>
		      <div>
			  <!--{section name=c loop=$volist_producttreecategory[p].childcategory}-->
			  <h4 class='sitemapclass3' style='font-weight:normal; font-size:12px;'><a href="<!--{$volist_producttreecategory[p].childcategory[c].url}-->"><!--{$volist_producttreecategory[p].childcategory[c].catename}--></a></h4>
			  <!--{/section}-->
			  </div>
		    </dd>
		   <!--{/section}-->
	      </dl>

          <dl class='sitemapclass'>
            <dd class='sitemapclass1' ><h2 style='font-size:13px;'><a href='<!--{$url_download}-->'><!--{$lang_download}--></a></h2></dd>
		    <!--{foreach $volist_downloadcategory as $volist}-->
		    <dd class='sitemapclass2' ><h3 style='font-weight:normal; font-size:12px;'><a href='<!--{$volist.url}-->'><!--{$volist.catename}--></a></h3><div></div></dd>
		    <!--{/foreach}-->
		  </dl>
		 
		  <dl class='sitemapclass'>
		    <dd class='sitemapclass1' ><h2 style='font-size:13px;'><a href='<!--{$url_case}-->'><!--{$lang_case}--></a></h2></dd>
		    <!--{foreach $volist_casecategory as $volist}-->
		    <dd class='sitemapclass2' ><h3 style='font-weight:normal; font-size:12px;'><a href='<!--{$volist.url}-->'><!--{$volist.catename}--></a></h3><div></div></dd>
		    <!--{/foreach}-->
		  </dl>

		  <dl class='sitemapclass'>
		    <dd class='sitemapclass1'><h2 style='font-size:13px;'><a href='<!--{$url_solution}-->'><!--{$lang_solution}--></a></h2></dd>
		   <!--{section name=p loop=$volist_solutiontreecategory}-->
		    <dd class='sitemapclass2'>
		      <h3 style='font-weight:normal; font-size:12px;'><a href="<!--{$volist_solutiontreecategory[p].url}-->"><!--{$volist_solutiontreecategory[p].catename}--></a></h3>
		      <div>
			  <!--{section name=c loop=$volist_solutiontreecategory[p].childcategory}-->
			  <h4 class='sitemapclass3' style='font-weight:normal; font-size:12px;'><a href="<!--{$volist_solutiontreecategory[p].childcategory[c].url}-->"><!--{$volist_solutiontreecategory[p].childcategory[c].catename}--></a></h4>
			  <!--{/section}-->
			  </div>
		    </dd>
		   <!--{/section}-->
	      </dl>

          <dl class='sitemapclass'>
		    <dd class='sitemapclass1' ><h2 style='font-size:13px;'><a href='<!--{$url_job}-->'><!--{$lang_job}--></a></h2></dd>
		    <!--{foreach $volist_jobcategory as $volist}-->
		    <dd class='sitemapclass2' ><h3 style='font-weight:normal; font-size:12px;'><a href='<!--{$volist.url}-->'><!--{$volist.catename}--></a></h3><div></div></dd>
		    <!--{/foreach}-->
		  </dl>
		
		  <dl class='sitemapclass'>
		    <dd class='sitemapclass1' ><h2 style='font-size:13px;'><a href='<!--{$url_guestbook}-->'><!--{$lang_guestbook}--></a></h2></dd>
		  </dl>
		
		  <dl class='sitemapclass'>
		    <dd class='sitemapclass1' ><h2 style='font-size:13px;'><a href='<!--{$url_link}-->'><!--{$lang_link}--></a></h2></dd>
		  </dl>
		
		  <dl class='sitemapclass'>
		    <dd class='sitemapclass1' ><h2 style='font-size:13px;'><a href='<!--{$url_map}-->'><!--{$lang_map}--></a></h2></dd>
		  </dl>

	    </div>
      </div>

    </div><!--#left //-->
    
    <div id="right">
      <h3 class="title"><span>产品分类</span></h3>
	  <div class="webnav"> 
        <!--{include file="<!--{$tplpath}-->block_productcat.tpl"}-->
      </div>

      <!--{include file="<!--{$tplpath}-->block_info.tpl"}--> 
      <!--{include file="<!--{$tplpath}-->block_contact.tpl"}-->  
    </div><!--#right //-->

    <div style="clear:both;"></div>
  </div>

  <!--{include file="<!--{$tplpath}-->block_footer.tpl"}-->
</div>
<script type="text/javascript" src="<!--{$skinpath}-->js/screen.js"></script>
</body>
</html>