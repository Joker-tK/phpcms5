<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<meta http-equiv="Content-Language" content="zh-CN" />
<title><!--{$page_title}-->-<!--{$copyright_header}--></title>
<meta name="description" content="<!--{$page_description}-->" />
<meta name="keywords" content="<!--{$page_keyword}-->" />
<meta name="author" content="<!--{$copyright_author}-->" />
<LINK rel="stylesheet" type="text/css" href="<!--{$skinpath}-->css/style.css">
<LINK rel="stylesheet" type="text/css" href="<!--{$skinpath}-->css/index.css">
<script type='text/javascript' src='<!--{$skinpath}-->js/command.js'></script>
<script src="<!--{$skinpath}-->js/jquery-1.4.4.min.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/downnav.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/jquery.cross-slide.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/Kingfisher.js" language="javascript" type="text/javascript"></script>
<link rel="alternate" type="application/rss+xml" title="订阅该空间的汽车网站管理系统 V1.0 静态HTML中英文版文章"  href="rss.php">

<script type="text/javascript">
function formsubmit()
 {
 if (formcheck())
 {

	  document.regform.action="saveuser.php?action=add";
      document.regform.method="post";
      document.regform.submit();
   }  
}

  function formcheck() 
    { 
       if(document.getElementById("loginname").value== "") 
         { alert("请输入用户名");
		   document.regform.loginname.focus();
		   document.regform.loginname.select(); 
           return false;
         }
	   if(document.regform.loginname.value.length<3) 
         { alert("用户名不能小于3位");
		   document.regform.loginname.focus();
		   document.regform.loginname.select(); 
           return false;
         }   


if ((document.regform.password.value).length<6)
{
alert("密码应大于等于6位！");
document.regform.password.focus();
document.regform.password.select();
return false;
}

if(document.regform.password1.value!=document.regform.password.value)
{
alert("两次密码输入不一致");
document.regform.password1.focus();
document.regform.password1.select();
return false;
}



    return true;      
    }





</script>

</head>
<BODY><!--顶部-->
<DIV class=content>

<!-- star pub header# //-->
<!--{include file="<!--{$tplpath}-->block_head.tpl"}-->
<!-- end header# //-->
<!--幻灯片-->
<DIV class=sub_flash>
  <!--{if $ads_zone1}-->
  <div id="flash"> 
    <div class="flash">
	  <!--{if $zone_silde1=="1"}-->
		<div id="slide_wp" style="overflow:hidden;width:980px;height:auto;position:relative;clear:both;boder:0px;background-color:#ffffff;margin:auto;">
		  <div id="MSClassBox">
			<ul id="ContentID" style="margin:0px;padding:0px;">
			  <!--{foreach $ads_zone1 as $volist}-->
			  <li><a href="<!--{$volist.url}-->"><img border="0" src="<!--{$volist.uploadfiles}-->" width="980px" height="163px" title="<!--{$volist.adsname}-->" alt="<!--{$volist.adsname}-->" /></a></li>
			  <!--{/foreach}-->
			</ul>
		  </div>
		  <ul id="TabID" style="display:none;">
			<!--{foreach $ads_zone1 as $volist}-->
			<li class=""><!--{$volist.i}--></li>
			<!--{/foreach}-->
		  </ul>
		</div>
		<script type="text/javascript">
		new Marquee(
		{
			MSClassID : "MSClassBox",
			ContentID : "ContentID",
			TabID	  : "TabID",
			Direction : 2,
			Step	  : 0.3,
			Width	  : 980,
			Height	  : 163,
			Timer	  : 20,
			DelayTime : 2000,
			WaitTime  : 0,
			ScrollStep: 610,
			SwitchType: 1,
			AutoStart : 1
		})
		</script>
	  <!--{else}-->


	  <div id="banner">
      <!--{foreach $ads_zone1 as $volist}-->
      <a href="<!--{$volist.url}-->"><img src="<!--{$volist.uploadfiles}-->" width="<!--{$volist.width}-->" height="<!--{$volist.height}-->" title="<!--{$volist.adsname}-->" alt="<!--{$volist.adsname}-->" /></a>
	  <!--{/foreach}-->
	  </div>

	  <!--{/if}-->



	</div>
  </div><!-- #flash //-->
  <!--{/if}-->
</DIV>
<div class="clear"></div>

<div id="contant">
  <div class="cleft">
    
	<div class="news">

      <div class="title">
	  &nbsp;<b>会员中心</b>
	  </div>


	  <div class="webnav"> 
        <div id="web-sidebar">
<!--{if $username eq ""}-->
          <dl>
		    <dt class="part2" id="part1-id6"><a href="register.php" target="_blank">注册会员</a></dt>
		  </dl>	
<!--{else}-->

          <dl>
		    <dt class="part2" id="part1-id6"><a href='index.php'  title='会员中心'>会员中心</a></dt>
		  </dl>	
		   
		   <dl>
		    <dt class="part2" id="part1-id6"><a href='userinfo.php' target="_blank" title='修改基本信息'>修改基本信息</a></dt>
		  </dl>	
		            <dl>
		    <dt class="part2" id="part1-id6"><a href='mymessage.php' title='我的留言'>我的留言</a></dt>
		  </dl>	
		            <dl>
		    <dt class="part2" id="part1-id6"><a  href='myorder.php' title='我的订单'>我的订单</a></dt>
		  </dl>	
		            <dl>
		    <dt class="part2" id="part1-id6"><a href='resume.php' title='我的简历'>我的简历</a></dt>
		  </dl>	
          <dl>
		    <dt class="part2" id="part1-id6"><a href='loginout.php' title='安全退出'>安全退出</a></dt>
		  </dl>	
<!--{/if}-->
        </div>
      </div>
    </div>

  </div>
  <div class="cright">
	<div class="crightb">
	  <div class="crightbtitle">

	    <div class="crightbtitlel">
		当前位置：
		</div>
		<div class="crightbtitler">
		&nbsp;<a href="<!--{$url_index}-->">首 页</a>&nbsp;&nbsp;>>&nbsp;&nbsp;注册会员

		</div>
	  </div>

	  <div class="crightbcontent">

    <div id="news_list">

  <form method="POST"  name="regform" >
    <table cellpadding="1" border="0" cellspacing="1">
	<tr>
	<td class="zhuce_text"><span>用户名:</span></td>
	<td class="zhuce_input"><input id="loginname" name="loginname" type="text" class="input_text" /></td>
	<td class="zhuce_sm">*</td>
	</tr>
	<tr>
	<td class="zhuce_text"><span>登录密码:</span></td>
	<td class="zhuce_input"><input name="password" id="password" type="password" class="input_text" /></td>
	<td class="zhuce_sm">*</td>
	</tr>
	<tr>
	<td class="zhuce_text"><span>密码确认:</span></td>
	<td class="zhuce_input"><input  name="password1" id="password1" type="password" class="input_text" /></td>
	<td class="zhuce_sm">*</td>
	</tr>
	<tr>
	<td class="zhuce_text"><span>性别：</span></td>
	<td class="zhuce_input"><input name="Sex" type="radio" value="1" checked="checked" class="inputnoborder" />先生 
     <input type="radio" name="Sex" value="0" class="inputnoborder" />女士</td>
	<td class="zhuce_sm">*</td>
	</tr>
    <tr>
	<td class="zhuce_text"><span>真实姓名:</span></td>
    <td class="zhuce_input"><input name="realname" id="realname" type="text" class="input_text" /></td>
	<td class="zhuce_sm">*</td>
	</tr>
	<tr>
	<td class="zhuce_text"><span>联系电话:</span></td>
    <td class="zhuce_input"><input name="telno" id="telno" type="text" class="input_text" /></td>
	<td class="zhuce_sm"></td>
	</tr>
	<tr>
	<td class="zhuce_text"><span>生日:</span></td>
    <td class="zhuce_input"><input id="brothday" name="brothday" type="text" class="input_text" /></td>
	<td class="zhuce_sm"></td>
	</tr>
	<tr>
	<td class="zhuce_text"><span>E-mail:</span></td>
    <td class="zhuce_input"><input id="email" name="email" type="text" class="input_text" /></td>
	<td class="zhuce_sm">*</td>
	</tr>
	<tr>
	<td class="zhuce_text"><span>联系地址:</span></td>
    <td class="zhuce_input"><input name="address" id="address" type="text" class="input_text" /></td>
	<td class="zhuce_sm"></td>
	</tr>
	<tr>
	<td class="zhuce_text"><span>个人说明:</span></td>
    <td class="zhuce_input" colspan="2"><textarea name='memo' id='memo' style='width:260px;height:65px;overflow:auto;color:#444444;'></textarea></td>
	</tr>
	<tr>
	<td class="zhuce_text"></td>
	<td class="zhuce_subimt" colspan="2">
	<input type="button" id="b_save" name="b_save" onClick="formsubmit()" value="立即注册" />
	<input type="reset" value="重填" id="b_reset" name="b_reset" /></td>
    </tr>
	</table>
</form>


		</div>

	  </div>
	</div>
  </div>

</div>
<!--{include file="<!--{$tplpath}-->block_footer.tpl"}-->
</DIV>
<script type="text/javascript" src="<!--{$skinpath}-->js/screen.js"></script>
</BODY>
</HTML>































