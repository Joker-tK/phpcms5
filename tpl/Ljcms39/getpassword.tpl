<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-CN" lang="zh-CN">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<meta http-equiv="Content-Language" content="zh-CN" />
<title><!--{$page_title}-->-<!--{$copyright_header}--></title>
<meta name="description" content="<!--{$page_metadescription}-->" />
<meta name="keywords" content="<!--{$page_metakeyword}-->" />
<meta name="author" content="<!--{$copyright_author}-->" />
<link rel="stylesheet" type="text/css" href="<!--{$skinpath}-->css/style.css?20120519201" /> 
<link rel="stylesheet" type="text/css" href="<!--{$skinpath}-->css/member.css" /> 
<script type='text/javascript' src='<!--{$skinpath}-->js/jquery-1.4.4.min.js'></script>
<script src="<!--{$skinpath}-->js/command.js" language="javascript" type="text/javascript"></script>
<script type="text/javascript">
function checkform(){

if (document.newmyform.password.value.length == 0) 
{
alert('新密码不能为空？');
document.newmyform.password.focus();
return false;
}
if (document.newmyform.password.value.length<6)
{
alert('最新密码不能小于六位？');
document.newmyform.password.focus();
return false;
}

if (document.newmyform.password.value!=document.newmyform.password1.value)
{
alert('你输入的重复密码不一样？');
document.newmyform.password.focus();
return false;
}
}
</script>
</head>
<body>
<div class="reg" id="regArea">
                <ul class="cfl">
                    <li class="regArea_l">
<a href="<!--{$url_contact}-->" style="padding-top:1px;"><!--{$lang_contact}--></a>
 <a href='#' onclick='SetHome(this,window.location);' style='cursor:pointer;padding-top:1px;border-right:0px;' title='设为首页'>设为首页</a>
                    </li>						
                    <li class="regArea_r" style="position:relative;">
<a class="tf" id="based" style="padding-top:1px;">繁体中文</a> <script language=Javascript src="<!--{$skinpath}-->js/j2f.js"></script>
		<a href='#' onclick='addFavorite();' style='cursor:pointer;padding-top:1px;border-right:0px;' title='收藏本站'>收藏本站</a>
                    </li>
                </ul>
            </div>
<div id="page" class="container">
<!-- star pub header# //-->
<!--{include file="<!--{$tplpath}-->block_head.html"}-->
<!-- end header# //-->

<div id="banner">
<!--{foreach key=key item=item from=$ads_zone3 name=adsname}-->
<img width="980" height="200" src="<!--{$item.uploadfiles}-->" alt="<!--{$item.adsname}-->" /></a></li>
<!--{/foreach}-->
</div>

  <div class="accessories">
	<div class="aside">
		<div class="nav">
			<h3>会员中心</h3>
			<ul>
<li><a href="register.php" target="_blank" style="margin-left: 5px;">注册会员</a></li>
<li><a href='index.php'  title='会员中心首页'>会员中心首页</a></li>
<li><a  href='userinfo.php' target="_blank" title='修改基本信息'>修改基本信息</a></li>
<li><a  href='mymessage.php' title='我的留言'>我的留言</a></li>
<li><a  href='resume.php' title='我的简历'>我的简历</a></li>
<li><a href='loginout.php' title='安全退出'>安全退出</a></li>



			</ul>
		</div>
	</div>
	<!-- aside -->
 
<div class="main" style="float:right;width:775px">
<div class="promo">
<div class="mainnatming"><H5>找回密码</H5></div>
<div class="contentmain">
<div>
<div class="main_zhuce">


<!--{if $uaction eq ""}-->
<div class="main-wrap">
  <div class="main-cont">
	<form action="getpassword.php" method="post" name="myform" id="myform" style="margin:0">
	<input type="hidden" name="uaction" id="uaction" value="selectuser" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">
	    <td align="center">登录名</td>
		<td><input type="text" name="username" id="username" class="input-s" value="" /></td>
	  </tr>
    <tr>
	<td class='submint' colspan="2">
	<input type='submit' name='Submit' value='提交信息' class='submit' />&nbsp;&nbsp;
	<input type='reset' name='Submit' value='重新填写' class='reset' /></td></td>
	  </tr>
	</table>
	</form>
  <div style="clear:both;height:10px;width:100%"></div>
  </div>
</div>
<!--{/if}-->





<!--{if $uaction eq "selectuser"}-->
<div class="main-wrap">
<div class="main-cont">
<h3 class="title" style="text-align:center">查询Email是否正确</h3>
<!--{if $user.loginname eq ""}-->
<!--{else}-->
<form action="getpassword.php" method="post" name="myform" id="myform" style="margin:0">
	<input type="hidden" name="uaction" id="uaction" value="seleemial" />
	<input type="hidden" name="loginname" id="loginname" value="<!--{$user.loginname}-->" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">
	    <td align="center">请输入E-mail:</td>
		<td><input type="text" name="email" id="emal" class="input-s" value="" /></td>
	  </tr>
    <tr>
	<td class='submint' colspan="2">
	<input type='submit' name='Submit' value='提交信息' class='submit' />&nbsp;&nbsp;
	<input type='reset' name='Submit' value='重新填写' class='reset' /></td></td>
	  </tr>
	</table>
	</form>
<!--{/if}-->
  <div style="clear:both;height:10px;width:100%"></div>
  </div>
  <div style="clear:both;height:10px;"></div>
</div>
<!--{/if}-->





<!--{if $uaction eq "seleemial"}-->
<div class="main-wrap">
<div class="main-cont">
<h3 class="title" style="text-align:center">请输入新密码:</h3>
<!--{if $users.loginname eq ""}-->
<!--{else}-->
<form action="getpassword.php" method="post" name="newmyform" id="newmyform" style="margin:0" onsubmit="return checkform()">
	<input type="hidden" name="uaction" id="uaction" value="updatepwd" />
	<input type="hidden" name="loginname" id="loginname" value="<!--{$users.loginname}-->" />
	<input type="hidden" name="userid" id="userid" value="<!--{$users.userid}-->" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <tr>
	    <td align="center">新密码:</td>
		<td><input type="password" name="password" id="password" class="input-s" value="" /></td>
	  </tr>
	  <tr>
	    <td align="center">重复密码:</td>
		<td><input type="password" name="password1" id="password1" class="input-s" value="" /></td>
	  </tr>
    <tr>
	<td class='submint' colspan="2">
	<input type='submit' name='Submit' value='提交信息' class='submit' />&nbsp;&nbsp;
	<input type='reset' name='Submit' value='重新填写' class='reset' /></td></td>
	  </tr>
	</table>
	</form>
<!--{/if}-->
  <div style="clear:both;height:10px;width:100%"></div>
  </div>
  <div style="clear:both;height:10px;"></div>
</div>
<!--{/if}-->

</div>
 </div>
</div>
</div>
</div>
</div>

<div style="width:100%;height:10px;clear:both;"></div>
<!--{include file="<!--{$tplpath}-->block_footer.html"}-->
</div>	
</div>
</body>
</html>
























<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title><!--{$page_title}-->-<!--{$copyright_header}--></title>
<meta name="description" content="<!--{$page_description}-->" />
<meta name="keywords" content="<!--{$page_keyword}-->" />
<meta name="author" content="<!--{$copyright_author}-->" />
<LINK rel="stylesheet" type="text/css" href="<!--{$skinpath}-->css/style.css">
<LINK rel="stylesheet" type="text/css" href="<!--{$skinpath}-->css/index.css">
<script type='text/javascript' src='<!--{$skinpath}-->js/command.js'></script>
<script src="<!--{$skinpath}-->js/jquery-1.4.4.min.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/downnav.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/jquery.cross-slide.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/Kingfisher.js" language="javascript" type="text/javascript"></script>
<link rel="alternate" type="application/rss+xml" title="订阅该空间的汽车网站管理系统 V1.0 静态HTML中英文版文章"  href="rss.php">
</head>
<BODY><!--顶部-->
<DIV class=content>

<!-- star pub header# //-->
<!--{include file="<!--{$tplpath}-->block_head.tpl"}-->
<!-- end header# //-->
<!--幻灯片-->
<DIV class=sub_flash>
  <!--{if $ads_zone1}-->
  <div id="flash"> 
    <div class="flash">
	  <!--{if $zone_silde1=="1"}-->
		<div id="slide_wp" style="overflow:hidden;width:980px;height:auto;position:relative;clear:both;boder:0px;background-color:#ffffff;margin:auto;">
		  <div id="MSClassBox">
			<ul id="ContentID" style="margin:0px;padding:0px;">
			  <!--{foreach $ads_zone1 as $volist}-->
			  <li><a href="<!--{$volist.url}-->"><img border="0" src="<!--{$volist.uploadfiles}-->" width="980px" height="163px" title="<!--{$volist.adsname}-->" alt="<!--{$volist.adsname}-->" /></a></li>
			  <!--{/foreach}-->
			</ul>
		  </div>
		  <ul id="TabID" style="display:none;">
			<!--{foreach $ads_zone1 as $volist}-->
			<li class=""><!--{$volist.i}--></li>
			<!--{/foreach}-->
		  </ul>
		</div>
		<script type="text/javascript">
		new Marquee(
		{
			MSClassID : "MSClassBox",
			ContentID : "ContentID",
			TabID	  : "TabID",
			Direction : 2,
			Step	  : 0.3,
			Width	  : 980,
			Height	  : 163,
			Timer	  : 20,
			DelayTime : 2000,
			WaitTime  : 0,
			ScrollStep: 610,
			SwitchType: 1,
			AutoStart : 1
		})
		</script>
	  <!--{else}-->


	  <div id="banner">
      <!--{foreach $ads_zone1 as $volist}-->
      <a href="<!--{$volist.url}-->"><img src="<!--{$volist.uploadfiles}-->" width="<!--{$volist.width}-->" height="<!--{$volist.height}-->" title="<!--{$volist.adsname}-->" alt="<!--{$volist.adsname}-->" /></a>
	  <!--{/foreach}-->
	  </div>

	  <!--{/if}-->



	</div>
  </div><!-- #flash //-->
  <!--{/if}-->
</DIV>
<div class="clear"></div>

<div id="contant">
  <div class="cleft">
    
	<div class="news">

      <div class="title">
	  &nbsp;<b>会员中心</b>
	  </div>


	  <div class="webnav"> 
        <div id="web-sidebar">
<!--{if $username eq ""}-->
          <dl>
		    <dt class="part2" id="part1-id6"><a href="register.php" target="_blank">注册会员</a></dt>
		  </dl>	
<!--{else}-->

          <dl>
		    <dt class="part2" id="part1-id6"><a href='index.php'  title='会员中心'>会员中心</a></dt>
		  </dl>	
		   
		   <dl>
		    <dt class="part2" id="part1-id6"><a href='userinfo.php' target="_blank" title='修改基本信息'>修改基本信息</a></dt>
		  </dl>	
		            <dl>
		    <dt class="part2" id="part1-id6"><a href='mymessage.php' title='我的留言'>我的留言</a></dt>
		  </dl>	
		            <dl>
		    <dt class="part2" id="part1-id6"><a  href='myorder.php' title='我的订单'>我的订单</a></dt>
		  </dl>	
		            <dl>
		    <dt class="part2" id="part1-id6"><a href='resume.php' title='我的简历'>我的简历</a></dt>
		  </dl>	
          <dl>
		    <dt class="part2" id="part1-id6"><a href='loginout.php' title='安全退出'>安全退出</a></dt>
		  </dl>	
<!--{/if}-->
        </div>
      </div>
    </div>

  </div>
  <div class="cright">
	<div class="crightb">
	  <div class="crightbtitle">

	    <div class="crightbtitlel">
		当前位置：
		</div>
		<div class="crightbtitler">
		&nbsp;<a href="<!--{$url_index}-->">首 页</a>&nbsp;&nbsp;>>&nbsp;&nbsp;会员中心

		</div>
	  </div>

	  <div class="crightbcontent">


<div id="news_list">


<!--{if $uaction eq ""}-->
<div class="main-wrap">
  <div class="main-cont">
	<form action="getpassword.php" method="post" name="myform" id="myform" style="margin:0">
	<input type="hidden" name="uaction" id="uaction" value="selectuser" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">
	    <td align="center">登录名</td>
		<td><input type="text" name="username" id="username" class="input-s" value="" /></td>
	  </tr>
    <tr>
	<td class='submint' colspan="2">
	<input type='submit' name='Submit' value='提交信息' class='submit' />&nbsp;&nbsp;
	<input type='reset' name='Submit' value='重新填写' class='reset' /></td></td>
	  </tr>
	</table>
	</form>
  <div style="clear:both;height:10px;width:100%"></div>
  </div>
</div>
<!--{/if}-->





<!--{if $uaction eq "selectuser"}-->
<div class="main-wrap">
<div class="main-cont">
<h3 class="title" style="text-align:center">查询Email是否正确</h3>
<!--{if $user.loginname eq ""}-->
<!--{else}-->
<form action="getpassword.php" method="post" name="myform" id="myform" style="margin:0">
	<input type="hidden" name="uaction" id="uaction" value="seleemial" />
	<input type="hidden" name="loginname" id="loginname" value="<!--{$user.loginname}-->" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">
	    <td align="center">请输入E-mail:</td>
		<td><input type="text" name="email" id="emal" class="input-s" value="" /></td>
	  </tr>
    <tr>
	<td class='submint' colspan="2">
	<input type='submit' name='Submit' value='提交信息' class='submit' />&nbsp;&nbsp;
	<input type='reset' name='Submit' value='重新填写' class='reset' /></td></td>
	  </tr>
	</table>
	</form>
<!--{/if}-->
  <div style="clear:both;height:10px;width:100%"></div>
  </div>
  <div style="clear:both;height:10px;"></div>
</div>
<!--{/if}-->





<!--{if $uaction eq "seleemial"}-->
<div class="main-wrap">
<div class="main-cont">
<h3 class="title" style="text-align:center">请输入新密码:</h3>
<!--{if $users.loginname eq ""}-->
<!--{else}-->
<form action="getpassword.php" method="post" name="newmyform" id="newmyform" style="margin:0" onsubmit="return checkform()">
	<input type="hidden" name="uaction" id="uaction" value="updatepwd" />
	<input type="hidden" name="loginname" id="loginname" value="<!--{$users.loginname}-->" />
	<input type="hidden" name="userid" id="userid" value="<!--{$users.userid}-->" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <tr>
	    <td align="center">新密码:</td>
		<td><input type="password" name="password" id="password" class="input-s" value="" /></td>
	  </tr>
	  <tr>
	    <td align="center">重复密码:</td>
		<td><input type="password" name="password1" id="password1" class="input-s" value="" /></td>
	  </tr>
    <tr>
	<td class='submint' colspan="2">
	<input type='submit' name='Submit' value='提交信息' class='submit' />&nbsp;&nbsp;
	<input type='reset' name='Submit' value='重新填写' class='reset' /></td></td>
	  </tr>
	</table>
	</form>
<!--{/if}-->
  <div style="clear:both;height:10px;width:100%"></div>
  </div>
  <div style="clear:both;height:10px;"></div>
</div>
<!--{/if}-->


		</div>

	  </div>
	</div>
  </div>

</div>
<!--{include file="<!--{$tplpath}-->block_footer.tpl"}-->
</DIV>
<script type="text/javascript" src="<!--{$skinpath}-->js/screen.js"></script>
</BODY>
</HTML>































