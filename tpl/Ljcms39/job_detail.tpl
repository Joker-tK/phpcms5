<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title><!--{$page_title}-->-<!--{$copyright_header}--></title>
<meta name="description" content="<!--{$page_description}-->" />
<meta name="keywords" content="<!--{$page_keyword}-->" />
<meta name="author" content="<!--{$copyright_author}-->" />
<LINK rel="stylesheet" type="text/css" href="<!--{$skinpath}-->css/index.css">
<LINK rel="stylesheet" type="text/css" href="<!--{$skinpath}-->css/style.css">
<script type='text/javascript' src='<!--{$skinpath}-->js/command.js'></script>
<script src="<!--{$skinpath}-->js/jquery-1.4.4.min.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/downnav.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/jquery.cross-slide.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/Kingfisher.js" language="javascript" type="text/javascript"></script>
<link rel="alternate" type="application/rss+xml" title="订阅该空间的汽车网站管理系统 V1.0 静态HTML中英文版文章"  href="rss.php">
</head>
<BODY><!--顶部-->
<DIV class=content>

<!-- star pub header# //-->
<!--{include file="<!--{$tplpath}-->block_head.tpl"}-->
<!-- end header# //-->
<!--幻灯片-->
<DIV class=sub_flash>
  <!--{if $ads_zone1}-->
  <div id="flash"> 
    <div class="flash">
	  <!--{if $zone_silde1=="1"}-->
		<div id="slide_wp" style="overflow:hidden;width:980px;height:auto;position:relative;clear:both;boder:0px;background-color:#ffffff;margin:auto;">
		  <div id="MSClassBox">
			<ul id="ContentID" style="margin:0px;padding:0px;">
			  <!--{foreach $ads_zone1 as $volist}-->
			  <li><a href="<!--{$volist.url}-->"><img border="0" src="<!--{$volist.uploadfiles}-->" width="980px" height="163px" title="<!--{$volist.adsname}-->" alt="<!--{$volist.adsname}-->" /></a></li>
			  <!--{/foreach}-->
			</ul>
		  </div>
		  <ul id="TabID" style="display:none;">
			<!--{foreach $ads_zone1 as $volist}-->
			<li class=""><!--{$volist.i}--></li>
			<!--{/foreach}-->
		  </ul>
		</div>
		<script type="text/javascript">
		new Marquee(
		{
			MSClassID : "MSClassBox",
			ContentID : "ContentID",
			TabID	  : "TabID",
			Direction : 2,
			Step	  : 0.3,
			Width	  : 980,
			Height	  : 163,
			Timer	  : 20,
			DelayTime : 2000,
			WaitTime  : 0,
			ScrollStep: 610,
			SwitchType: 1,
			AutoStart : 1
		})
		</script>
	  <!--{else}-->


	  <div id="banner">
      <!--{foreach $ads_zone1 as $volist}-->
      <a href="<!--{$volist.url}-->"><img src="<!--{$volist.uploadfiles}-->" width="<!--{$volist.width}-->" height="<!--{$volist.height}-->" title="<!--{$volist.adsname}-->" alt="<!--{$volist.adsname}-->" /></a>
	  <!--{/foreach}-->
	  </div>

	  <!--{/if}-->



	</div>
  </div><!-- #flash //-->
  <!--{/if}-->
</DIV>
<div class="clear"></div>

<div id="contant">
  <div class="cleft">
    
	<div class="news">

      <div class="title">
	  &nbsp;<b>招聘分类</b>
	  </div>
	  <div class="webnav"> 
	  <div class="list1">

	    <div id="web-sidebar">

		  <!--{foreach $volist_jobcategory as $volist}-->
          <dl>
		    <dt class="part2" id="part1-id6"><a href="<!--{$volist.url}-->"><!--{$volist.catename}--></a></dt>
		  </dl>	
		  <!--{/foreach}-->

		</div>

     </div>
      </div>
    </div>
<!--{include file="<!--{$tplpath}-->block_info.tpl"}--> 
<!--{include file="<!--{$tplpath}-->block_contact.tpl"}--> 
  </div>
  <div class="cright">
	<div class="crightb">
	  <div class="crightbtitle">

	    <div class="crightbtitlel">
		当前位置：
		</div>
		<div class="crightbtitler">
		&nbsp;<a href="<!--{$url_index}-->">首 页</a>&nbsp;&nbsp;>>&nbsp;&nbsp;<!--{$navigation}-->

		</div>
	  </div>

	  <div class="crightbcontent">

	  <div class="webcontent">


	    <div id="showjob">
		  <h1 class="title">招聘职位：<!--{$job.title}--></h1>
		  <div class="para">
		  		    <ul style="width:300px;float:left;">
			  <li class='info_deal'><b>职位编号 ：</b> #<!--{$job.jobid}--></li>
			  <li class='info_person'><b>招聘人数 ： </b> <!--{$job.number}--> 人</li>
			  <li class='info_address'><b>工作地点 ：</b> <!--{$job.workarea}--></li>
			  <li class='info_updatetime'><b>发布日期 ：</b> <!--{$job.timeline|date_format:'%Y-%m-%d'}--></li>
			  <li class='info_validity'><b>浏览次数 ：</b> <!--{$job.hits}--></li>
			</ul>
			<ul style="float:left;width:300px">
            <li style="height:30px;"></li>
            <li style="height:30px;"></li>
			<li>
			<div class="info_job">
			<a href="applyjob.php?jobid=<!--{$job.jobid}-->" target="_blank" title="在线应聘" class='hover-none'><span>在线应聘</span></a>
			</div>
			</li>
			</ul>
			<div style="clear:both;"></div>
		  </div>
		  <h3 class="hr"><span>岗位职责</span></h3>
		  <div class="text editor"> <!--{$job.jobdescription}--></div>

		  <h3 class="hr"><span>职位要求</span></h3>
		  <div class="text editor"><!--{$job.jobrequest}--></div>
                  <!--{if $job.jobotherrequest!=""}-->
		  <h3 class="hr"><span>其他要求 </span></h3>
		  <div class="text editor"> <!--{$job.jobotherrequest}--></div>
		  <!--{/if}-->

		  <h3 class="hr"><span>招聘负责人联系方式 </span></h3>
		  <div class="text editor"><!--{$job.jobcontact}--></div>

		  <div class="hits"></div>
		  <div class="page">上一条：<!--{$previous_item}--><br>下一条：<!--{$next_item}--></div>
		</div>

	  </div><!-- $webcontent //-->

	  </div>
	</div>
  </div>

</div>
<!--{include file="<!--{$tplpath}-->block_footer.tpl"}-->
</DIV>
<script type="text/javascript" src="<!--{$skinpath}-->js/screen.js"></script>
</BODY>
</HTML>



























