<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title><!--{$page_title}-->-<!--{$copyright_header}--></title>
<meta name="description" content="<!--{$page_description}-->" />
<meta name="keywords" content="<!--{$page_keyword}-->" />
<meta name="author" content="<!--{$copyright_author}-->" />
<LINK rel="stylesheet" type="text/css" href="<!--{$skinpath}-->css/style.css">
<LINK rel="stylesheet" type="text/css" href="<!--{$skinpath}-->css/index.css">
<script type='text/javascript' src='<!--{$skinpath}-->js/command.js'></script>
<script src="<!--{$skinpath}-->js/jquery-1.4.4.min.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/downnav.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/jquery.cross-slide.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/Kingfisher.js" language="javascript" type="text/javascript"></script>
<link rel="alternate" type="application/rss+xml" title="订阅该空间的汽车网站管理系统 V1.0 静态HTML中英文版文章"  href="rss.php">
<script language="javascript">
function CheckJob(){
if (document.myform.username.value.length == 0) 
{
alert('姓名 不能为空');
document.myform.username.focus();
return false;
}
if (document.myform.email.value.length == 0)
{
alert('E–mail 不能为空');
document.myform.email.focus();
return false;
}
if (document.myform.telno.value.length == 0)
{
alert('联系电话不能为空');
document.myform.telno.focus();
return false;
}
}
</script>
</head>
<BODY><!--顶部-->
<DIV class=content>

<!-- star pub header# //-->
<!--{include file="<!--{$tplpath}-->block_head.tpl"}-->
<!-- end header# //-->
<!--幻灯片-->
<DIV class=sub_flash>
  <!--{if $ads_zone1}-->
  <div id="flash"> 
    <div class="flash">
	  <!--{if $zone_silde1=="1"}-->
		<div id="slide_wp" style="overflow:hidden;width:980px;height:auto;position:relative;clear:both;boder:0px;background-color:#ffffff;margin:auto;">
		  <div id="MSClassBox">
			<ul id="ContentID" style="margin:0px;padding:0px;">
			  <!--{foreach $ads_zone1 as $volist}-->
			  <li><a href="<!--{$volist.url}-->"><img border="0" src="<!--{$volist.uploadfiles}-->" width="980px" height="163px" title="<!--{$volist.adsname}-->" alt="<!--{$volist.adsname}-->" /></a></li>
			  <!--{/foreach}-->
			</ul>
		  </div>
		  <ul id="TabID" style="display:none;">
			<!--{foreach $ads_zone1 as $volist}-->
			<li class=""><!--{$volist.i}--></li>
			<!--{/foreach}-->
		  </ul>
		</div>
		<script type="text/javascript">
		new Marquee(
		{
			MSClassID : "MSClassBox",
			ContentID : "ContentID",
			TabID	  : "TabID",
			Direction : 2,
			Step	  : 0.3,
			Width	  : 980,
			Height	  : 163,
			Timer	  : 20,
			DelayTime : 2000,
			WaitTime  : 0,
			ScrollStep: 610,
			SwitchType: 1,
			AutoStart : 1
		})
		</script>
	  <!--{else}-->


	  <div id="banner">
      <!--{foreach $ads_zone1 as $volist}-->
      <a href="<!--{$volist.url}-->"><img src="<!--{$volist.uploadfiles}-->" width="<!--{$volist.width}-->" height="<!--{$volist.height}-->" title="<!--{$volist.adsname}-->" alt="<!--{$volist.adsname}-->" /></a>
	  <!--{/foreach}-->
	  </div>

	  <!--{/if}-->



	</div>
  </div><!-- #flash //-->
  <!--{/if}-->
</DIV>
<div class="clear"></div>

<div id="contant">
  <div class="cleft">
    
	<div class="news">

      <div class="title">
	  &nbsp;<b>产品分类</b>
	  </div>
	  <div class="webnav"> 
	  <div class="list1">
  <!--{include file="<!--{$tplpath}-->block_productcat.tpl"}-->
     </div>
      </div>
    </div>
<!--{include file="<!--{$tplpath}-->block_info.tpl"}--> 
<!--{include file="<!--{$tplpath}-->block_contact.tpl"}--> 
  </div>
  <div class="cright">
	<div class="crightb">
	  <div class="crightbtitle">

	    <div class="crightbtitlel">
		当前位置：
		</div>
		<div class="crightbtitler">
		&nbsp;<a href="<!--{$url_index}-->">首 页</a>&nbsp;&nbsp;>>&nbsp;&nbsp;在线订购

		</div>
	  </div>

	  <div class="crightbcontent">

	   <div class="webcontent">
	    <div class="showtext editor">
<form method='POST' onSubmit='return CheckJob();' name='myform' action='<!--{$url_productbuy}-->' >
<input type="hidden" name="action" value="saveadd" />
<table class='job_table' width="95%" border="0" align="center" cellpadding="5" cellspacing="3" style="margin-top:20px">
<!--{if $proid eq ""}-->
<tr><td class='text'>选择要订购的产品</td>
<td class='input'>
<select name='proid' id='proid'>
<!--{foreach $products as $productlist}-->	
<option value='<!--{$productlist.productid}-->' ><!--{$productlist.productname}--></option>
<!--{/foreach}-->
</select>
<span class='info'>*</span></td></tr>
<!--{else}-->
<input type="hidden" name="proid" value="<!--{$proid}-->" />
<!--{/if}-->
<tr><td class='text'>姓名</td>
<td class='input'><input name='username' type='text' class='input-text' value="<!--{$username}-->" size='40'><span class='info'>*</span></td></tr>
<tr><td class='text'>性别</td>
<td class='input'>
<input name='sex' type='radio' id='para12_1' value='1' checked='checked' />
<label for='para12_1'>男士</label> 
<input name='sex' type='radio' id='para12_2' value='0'  />
<label for='para12_2'>女士</label>  <span class='info'></span>
</td></tr>
<tr><td class='text'>公司名字</td>
<td class='input'><input name='company' type='text' class='input-text' size='40'><span class='info'></span></td></tr>
<tr><td class='text'>公司地址</td>
<td class='input'><input name='address' type='text' class='input-text' size='40'><span class='info'>*</span></td></tr>
<tr><td class='text'>邮政编码</td>
<td class='input'><input name='zipcode' type='text' class='input-text' size='40'><span class='info'></span></td></tr>
<tr><td class='text'>联系电话</td>
<td class='input'><input name='telephone' type='text' class='input-text' size='40'><span class='info'></span></td></tr>
<tr><td class='text'>传真：</td>
<td class='input'><input name='fax' type='text' class='input-text' size='40'><span class='info'></span></td></tr>
<tr><td class='text'>手机号</td>
<td class='input'><input name='mobile' type='text' class='input-text' size='40'><span class='info'>*</span></td></tr>
<tr><td class='text'>E–mail</td>
<td class='input'><input name='email' type='text' class='input-text' size='40'><span class='info'>*</span></td></tr>
<tr><td class='text'>订购详细说明</td>
<td class='input'><textarea name='remark' class='textarea-text' cols='60' rows='5'></textarea><span class='info'>*</span></td></tr>
<tr><td class='text'></td>
<td class='submint'><input type='submit' name='Submit' value='立即订购' class='submit' />&nbsp;&nbsp;<input type='reset' name='Submit' value='重新填写' class='reset' /></td>
</tr></table></form>

		</div>
      </div>

	  </div>
	</div>
  </div>

</div>
<!--{include file="<!--{$tplpath}-->block_footer.tpl"}-->
</DIV>
<script type="text/javascript" src="<!--{$skinpath}-->js/screen.js"></script>
</BODY>
</HTML>































































