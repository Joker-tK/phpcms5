<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title><!--{$page_title}-->-<!--{$copyright_header}--></title>
<meta name="description" content="<!--{$page_description}-->" />
<meta name="keywords" content="<!--{$page_keyword}-->" />
<meta name="author" content="<!--{$copyright_author}-->" />
<LINK rel="stylesheet" type="text/css" href="<!--{$skinpath}-->css/style.css">
<LINK rel="stylesheet" type="text/css" href="<!--{$skinpath}-->css/index.css">
<script type='text/javascript' src='<!--{$skinpath}-->js/command.js'></script>
<script src="<!--{$skinpath}-->js/jquery-1.4.4.min.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/downnav.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/jquery.cross-slide.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/Kingfisher.js" language="javascript" type="text/javascript"></script>
<link rel="alternate" type="application/rss+xml" title="订阅该空间的汽车网站管理系统 V1.0 静态HTML中英文版文章"  href="rss.php">
<script language="javascript">
function checkform(){
	if($("#bookuser").val()==""){
		alert("姓名不能为空.");
		return false;
	}
	if($("#companyname").val()==""){
		alert("公司不能为空.");
		return false;
	}
	if($("#address").val()==""){
		alert("公司地址不能为空.");
		return false;
	}
	if($("#trade").val()==""){
		alert("所属行业不能为空.");
		return false;
	}
	if($("#jobs").val()==""){
		alert("职务不能为空.");
		return false;
	}
	if($("#telephone").val()==""){
		alert("联系电话不能为空.");
		return false;
	}
	if($("#content").val()==""){
		alert("留言内容不能为空.");
		return false;
	}
}
</script>
</head>
<BODY><!--顶部-->
<DIV class=content>
<!-- star pub header# //-->
<!--{include file="<!--{$tplpath}-->block_head.tpl"}-->
<!-- end header# //-->
<!--幻灯片-->
<DIV class=sub_flash>
  <!--{if $ads_zone1}-->
  <div id="flash"> 
    <div class="flash">
	  <!--{if $zone_silde1=="1"}-->
		<div id="slide_wp" style="overflow:hidden;width:980px;height:auto;position:relative;clear:both;boder:0px;background-color:#ffffff;margin:auto;">
		  <div id="MSClassBox">
			<ul id="ContentID" style="margin:0px;padding:0px;">
			  <!--{foreach $ads_zone1 as $volist}-->
			  <li><a href="<!--{$volist.url}-->"><img border="0" src="<!--{$volist.uploadfiles}-->" width="980px" height="163px" title="<!--{$volist.adsname}-->" alt="<!--{$volist.adsname}-->" /></a></li>
			  <!--{/foreach}-->
			</ul>
		  </div>
		  <ul id="TabID" style="display:none;">
			<!--{foreach $ads_zone1 as $volist}-->
			<li class=""><!--{$volist.i}--></li>
			<!--{/foreach}-->
		  </ul>
		</div>
		<script type="text/javascript">
		new Marquee(
		{
			MSClassID : "MSClassBox",
			ContentID : "ContentID",
			TabID	  : "TabID",
			Direction : 2,
			Step	  : 0.3,
			Width	  : 980,
			Height	  : 163,
			Timer	  : 20,
			DelayTime : 2000,
			WaitTime  : 0,
			ScrollStep: 610,
			SwitchType: 1,
			AutoStart : 1
		})
		</script>
	  <!--{else}-->
	  <div id="banner">
      <!--{foreach $ads_zone1 as $volist}-->
      <a href="<!--{$volist.url}-->"><img src="<!--{$volist.uploadfiles}-->" width="<!--{$volist.width}-->" height="<!--{$volist.height}-->" title="<!--{$volist.adsname}-->" alt="<!--{$volist.adsname}-->" /></a>
	  <!--{/foreach}-->
	  </div>
	  <!--{/if}-->
	</div>
  </div><!-- #flash //-->
  <!--{/if}-->
</DIV>
<div class="clear"></div>

<div id="contant">
  <div class="cleft">
    
	<div class="news">

      <div class="title">
	  &nbsp;<b>关于我们</b>
	  </div>
<div class="webnav"> 
	  <div class="list1">
<!--{include file="<!--{$tplpath}-->block_solutioncat.tpl"}-->
     </div>
      </div>
    </div>
<!--{include file="<!--{$tplpath}-->block_info.tpl"}--> 
<!--{include file="<!--{$tplpath}-->block_contact.tpl"}--> 
  </div>
  <div class="cright">
	<div class="crightb">
	  <div class="crightbtitle">

	    <div class="crightbtitlel">
		当前位置：
		</div>
		<div class="crightbtitler">
		&nbsp;在线留言
		</div>
	  </div>
	  <div class="crightbcontent">

	  <div class="webcontent">
	    <div class="showtext editor">
		  <p>

			<form method="post" action="<!--{$url_guestbook}-->" name="myform" id="myform" onsubmit="return checkform();" />
			<input type="hidden" name="action" value="saveadd" />
			<table cellpadding='1' cellspacing='0' border="0" style="line-height:30px;" width="80%" align="center">
			  <tr>
				<td colspan='2' class='topTd'></td>
			  </tr>
			  <tr>
				<td class='hback_1' width='15%'><b>姓 名：</b></td>
				<td class='hback' width='85%'><input name="bookuser" id="bookuser" type="text" size="15" />  <select name="gender" id="gender"><option value="1">先生</option><option value="2">女士</option></select></td>
			  </tr>
			  <tr>
				<td><b>公 司：</b></td>
				<td><input name="companyname" id="companyname" type="text" size="30" /> <font color='red'>*</font></td>
			  </tr>
			  <tr>
				<td><b>地 址：</b></td>
				<td><input name="address" id="address" type="text" size="30" /> <font color='red'>*</font></td>
			  </tr>
			  <tr>
				<td><b>行 业：</b></td>
				<td><input name="trade" id="trade" type="text" size="20" /> <font color='red'>*</font></td>
			  </tr>
			  <tr>
				<td><b>职 务：</b></td>
				<td><input name="jobs" id="jobs" type="text" size="20" /> <font color='red'>*</font></td>
			  </tr>
			  <tr>
				<td><b>电 话：</b></td>
				<td><input name="telephone" id="telephone" type="text" size="25" /> <font color='red'>*</font></td>
			  </tr>
			  <tr>
				<td><b>手 机：</b></td>
				<td><input name="mobile" id="mobile" type="text" size="25" /> <font color='red'></font></td>
			  </tr>
			  <tr>
				<td><b>传 真：</b></td>
				<td><input name="fax" id="fax" type="text" size="25" /> <font color='red'></font></td>
			  </tr>
			  <tr>
				<td><b>E-mail：</b></td>
				<td><input name="email" id="email" type="text" size="35" /> <font color='red'></font></td>
			  </tr>
			  <tr>
				<td><b>留言内容：</b></td>
				<td><textarea name="content" id="content" style="width:75%;height:100px;overflow:auto;"></textarea> <font color="red">*</font></td>
			  </tr>
			  <tr>
				<td colspan='2' align="center" height="30px;"><input type="submit" name="btn_save" value="提交保存" />&nbsp;&nbsp;<input type="reset" name="btn_reset" value="重 置" /></td>
			  </tr>
			</table>

			</form>


          </p>
	  			<div style="height:90px;width:728px;clear:both;">
<script type="text/javascript"><!--
google_ad_client = "pub-3406709034811813";
/* 728x90, 创建于 08-8-28 */
google_ad_slot = "7097595396";
google_ad_width = 728;
google_ad_height = 90;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js"> 
</script>
</div>
		</div>
      </div>
	  </div>
	</div>
  </div>
</div>
<!--{include file="<!--{$tplpath}-->block_footer.tpl"}-->
</DIV>
<script type="text/javascript" src="<!--{$skinpath}-->js/screen.js"></script>
</BODY>
</HTML>



























