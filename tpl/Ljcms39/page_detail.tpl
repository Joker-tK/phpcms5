<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title><!--{$page_title}-->-<!--{$copyright_header}--></title>
<meta name="description" content="<!--{$page_description}-->" />
<meta name="keywords" content="<!--{$page_keyword}-->" />
<meta name="author" content="<!--{$copyright_author}-->" />
<LINK rel="stylesheet" type="text/css" href="<!--{$skinpath}-->css/style.css">
<script type='text/javascript' src='<!--{$skinpath}-->js/command.js'></script>
<script src="<!--{$skinpath}-->js/jquery-1.4.4.min.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/downnav.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/jquery.cross-slide.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/Kingfisher.js" language="javascript" type="text/javascript"></script>
<link rel="alternate" type="application/rss+xml" title="订阅该空间的汽车网站管理系统 V1.0 静态HTML中英文版文章"  href="rss.php">
</head>
<BODY>
<!--顶部-->
<DIV class=content>
<!-- star pub header# //-->
<!--{include file="<!--{$tplpath}-->block_head.tpl"}-->
<!-- end header# //-->
<!--幻灯片-->
<DIV class=sub_flash>
  <!--{if $ads_zone1}-->
  <div id="flash"> 
    <div class="flash">
	  <!--{if $zone_silde1=="1"}-->
		<div id="slide_wp" style="overflow:hidden;width:980px;height:auto;position:relative;clear:both;boder:0px;background-color:#ffffff;margin:auto;">
		  <div id="MSClassBox">
			<ul id="ContentID" style="margin:0px;padding:0px;">
			  <!--{foreach $ads_zone1 as $volist}-->
			  <li><a href="<!--{$volist.url}-->"><img border="0" src="<!--{$volist.uploadfiles}-->" width="980px" height="163px" title="<!--{$volist.adsname}-->" alt="<!--{$volist.adsname}-->" /></a></li>
			  <!--{/foreach}-->
			</ul>
		  </div>
		  <ul id="TabID" style="display:none;">
			<!--{foreach $ads_zone1 as $volist}-->
			<li class=""><!--{$volist.i}--></li>
			<!--{/foreach}-->
		  </ul>
		</div>
		<script type="text/javascript">
		new Marquee(
		{
			MSClassID : "MSClassBox",
			ContentID : "ContentID",
			TabID	  : "TabID",
			Direction : 2,
			Step	  : 0.3,
			Width	  : 980,
			Height	  : 163,
			Timer	  : 20,
			DelayTime : 2000,
			WaitTime  : 0,
			ScrollStep: 610,
			SwitchType: 1,
			AutoStart : 1
		})
		</script>
	  <!--{else}-->


	  <div id="banner">
      <!--{foreach $ads_zone1 as $volist}-->
      <a href="<!--{$volist.url}-->"><img src="<!--{$volist.uploadfiles}-->" width="<!--{$volist.width}-->" height="<!--{$volist.height}-->" title="<!--{$volist.adsname}-->" alt="<!--{$volist.adsname}-->" /></a>
	  <!--{/foreach}-->
	  </div>

	  <!--{/if}-->



	</div>
  </div><!-- #flash //-->
  <!--{/if}-->
 
</DIV>

<DIV class=mian>
<DIV class=rightbar>
<H5><!--{$page.title}--></H5>
<DIV class=lefterbar>
<DIV id=d_con1>
 <p><!--{$page.content|htmlencode}--></p>
 <div style="height:90px;width:728px;clear:both;">
<script type="text/javascript"><!--
google_ad_client = "pub-3406709034811813";
/* 728x90, 创建于 08-8-28 */
google_ad_slot = "7097595396";
google_ad_width = 728;
google_ad_height = 90;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js"> 
</script>
</div>
</DIV>
</DIV>
<DIV class=clearbox></DIV>
</DIV>
<DIV class=leftbar>
<DL>
  <DT>关于我们 </DT>
  <DD class=list_1>
<UL style="line-height:22px !important;padding-top:10px;padding-bottom:10px">
<li><a href="<!--{$url_about}-->" target="_blank" style="margin-left: 5px;"><!--{$lang_about}--></a></li>
<!--{foreach $volist_page as $volist}-->
<li><a href="<!--{$volist.url}-->" target="_blank" style="margin-left: 5px;"><!--{$volist.title}--></a></li>
<!--{/foreach}-->    
      </UL>
    </DD>
  <DT style="margin-top:10px;">新闻动态</DT>
  <DD>
  <UL>
   <!--{foreach $volist_newinfo as $volist}-->
 <li style="background:none;padding-left:0px !important;"><img src="<!--{$skinpath}-->images/Tx_Ar3.gif" style="margin-left: 0px;" /><a href="<!--{$volist.url}-->"  title="<!--{$volist.title}-->" target="_blank" style="margin-left: 5px;">
 <!--{substr($volist.sort_title,0,42)}--></a></li>
 <!--{/foreach}-->
   </UL>
   </DD>
</DL>
<DIV class=clearbox></DIV></DIV></DIV>
<!--页尾-->
<!--{include file="<!--{$tplpath}-->block_footer.tpl"}-->
</DIV>
<script type="text/javascript" src="<!--{$skinpath}-->js/screen.js"></script>
</BODY>
</HTML>
</BODY>
</HTML>

