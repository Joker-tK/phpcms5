<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title><!--{$page_title}-->-<!--{$copyright_header}--></title>
<meta name="description" content="<!--{$page_description}-->" />
<meta name="keywords" content="<!--{$page_keyword}-->" />
<meta name="author" content="<!--{$copyright_author}-->" />
<LINK rel="stylesheet" type="text/css" href="<!--{$skinpath}-->css/style.css">
<LINK rel="stylesheet" type="text/css" href="<!--{$skinpath}-->css/index.css">
<script type='text/javascript' src='<!--{$skinpath}-->js/command.js'></script>
<script src="<!--{$skinpath}-->js/jquery-1.4.4.min.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/downnav.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/jquery.cross-slide.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/Kingfisher.js" language="javascript" type="text/javascript"></script>
<link rel="alternate" type="application/rss+xml" title="订阅该空间的汽车网站管理系统 V1.0 静态HTML中英文版文章"  href="rss.php">
</head>
<BODY><!--顶部-->
<DIV class=content>

<!-- star pub header# //-->
<!--{include file="<!--{$tplpath}-->block_head.tpl"}-->
<!-- end header# //-->
<!--幻灯片-->
<DIV class=sub_flash>
  <!--{if $ads_zone1}-->
  <div id="flash"> 
    <div class="flash">
	  <!--{if $zone_silde1=="1"}-->
		<div id="slide_wp" style="overflow:hidden;width:980px;height:auto;position:relative;clear:both;boder:0px;background-color:#ffffff;margin:auto;">
		  <div id="MSClassBox">
			<ul id="ContentID" style="margin:0px;padding:0px;">
			  <!--{foreach $ads_zone1 as $volist}-->
			  <li><a href="<!--{$volist.url}-->"><img border="0" src="<!--{$volist.uploadfiles}-->" width="980px" height="163px" title="<!--{$volist.adsname}-->" alt="<!--{$volist.adsname}-->" /></a></li>
			  <!--{/foreach}-->
			</ul>
		  </div>
		  <ul id="TabID" style="display:none;">
			<!--{foreach $ads_zone1 as $volist}-->
			<li class=""><!--{$volist.i}--></li>
			<!--{/foreach}-->
		  </ul>
		</div>
		<script type="text/javascript">
		new Marquee(
		{
			MSClassID : "MSClassBox",
			ContentID : "ContentID",
			TabID	  : "TabID",
			Direction : 2,
			Step	  : 0.3,
			Width	  : 980,
			Height	  : 163,
			Timer	  : 20,
			DelayTime : 2000,
			WaitTime  : 0,
			ScrollStep: 610,
			SwitchType: 1,
			AutoStart : 1
		})
		</script>
	  <!--{else}-->


	  <div id="banner">
      <!--{foreach $ads_zone1 as $volist}-->
      <a href="<!--{$volist.url}-->"><img src="<!--{$volist.uploadfiles}-->" width="<!--{$volist.width}-->" height="<!--{$volist.height}-->" title="<!--{$volist.adsname}-->" alt="<!--{$volist.adsname}-->" /></a>
	  <!--{/foreach}-->
	  </div>

	  <!--{/if}-->



	</div>
  </div><!-- #flash //-->
  <!--{/if}-->
</DIV>
<div class="clear"></div>

<div id="contant">
  <div class="cleft">
    
	<div class="news">

      <div class="title">
	  &nbsp;<b>产品分类</b>
	  </div>
	  <div class="webnav"> 
	  <div class="list1">
  <!--{include file="<!--{$tplpath}-->block_productcat.tpl"}-->
     </div>
      </div>
    </div>
<!--{include file="<!--{$tplpath}-->block_info.tpl"}--> 
<!--{include file="<!--{$tplpath}-->block_contact.tpl"}--> 
  </div>
  <div class="cright">
	<div class="crightb">
	  <div class="crightbtitle">

	    <div class="crightbtitlel">
		当前位置：
		</div>
		<div class="crightbtitler">
		&nbsp;<a href="<!--{$url_index}-->">首 页</a>&nbsp;&nbsp;>>&nbsp;&nbsp;<!--{$navigation}-->

		</div>
	  </div>

	  <div class="crightbcontent">

	  <div class="webcontent">
	    <div id="product-list">
		  <style type="text/css">#product-list #plug2 dd li{ padding:2px 0px;}</style>
		  <div id="plug2">
		    <!--{foreach $product as $volist}-->
		    <dl style="height:165px;">
			  <dt style="width:158px;">
			    <a href="<!--{$volist.url}-->" title="<!--{$volist.productname}-->" target="_blank"><img src="<!--{$volist.thumbfiles}-->" alt="<!--{$volist.productname}-->" title="<!--{$volist.productname}-->" onload="javascript:DrawImage(this,'150','150');" /></a>
			  </dt>
			  <dd style="width:167px;">
			    <ul>
				  <li><b>名称 : </b><span class="title"><a href="<!--{$volist.url}-->" title="<!--{$volist.productname}-->"><!--{$volist.productname|truncate:8:0:"utf-8"}--></a></span></li>
				  <li><b>编号 : </b><span><!--{$volist.productnum}--></span></li>
				  <li><b>分类 : </b><span><!--{$volist.catename}--></span></li>
				  <li><b>日期 : </b><span><!--{$volist.timeline|date_format:'%Y-%m-%d'}--></span></li>
				</ul>
				<div class="detail"><a href="<!--{$volist.url}-->" title="详细介绍" target="_blank"><img src="<!--{$skinpath}-->images/picd.gif" title="详细介绍" alt="详细介绍" /></a></div>
			  </dd>
			</dl>
			<!--{/foreach}-->
		  
		  </div><!-- #plug2 //-->
		  <div style="clear:both;"></div>
		</div>
		<!--{if $showpage!=""}-->
		<div class="clear"></div>
		<div class="pageController"><!--{$showpage}--></div> 
		<!--{/if}-->	  
	  </div><!-- $webcontent //-->

	  </div>
	</div>
  </div>

</div>
<!--{include file="<!--{$tplpath}-->block_footer.tpl"}-->
</DIV>
<script type="text/javascript" src="<!--{$skinpath}-->js/screen.js"></script>
</BODY>
</HTML>



























