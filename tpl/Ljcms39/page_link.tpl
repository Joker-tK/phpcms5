<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title><!--{$page_title}-->-<!--{$copyright_header}--></title>
<meta name="description" content="<!--{$page_description}-->" />
<meta name="keywords" content="<!--{$page_keyword}-->" />
<meta name="author" content="<!--{$copyright_author}-->" />
<link rel="stylesheet" type="text/css" href="<!--{$skinpath}-->style/main.css" />
<link rel="stylesheet" type="text/css" href="<!--{$skinpath}-->style/css.css" />
<script type='text/javascript' src='<!--{$skinpath}-->js/command.js'></script>
<script src="<!--{$skinpath}-->js/jquery-1.4.4.min.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/downnav.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/jquery.cross-slide.js" language="javascript" type="text/javascript"></script>
<!--[if IE 6]>
<script src="<!--{$skinpath}-->js/deletepng.js" language="javascript" type="text/javascript"></script>
<script type="text/javascript">
DD_belatedPNG.fix('.bg,img'); 
</script>
<![endif]-->
</head>
<body>
<div id="wrap">
  <!--{include file="<!--{$tplpath}-->block_head.tpl"}-->
  <!--{include file="<!--{$tplpath}-->block_banner.tpl"}-->

  <div id="web">
    
	<div id="left">
	  
	  <h3 class="title"><span>友情链接</span></h3>
	  <div class="webcontent">
	    <div id="link_index">
		  <!--{if $volist_logolink}-->
		  <dl>
		    <dt>图片链接:</dt>
			<dd>
			  <ul>
			    <!--{foreach $volist_logolink as $volist}-->
			    <li><a href='<!--{$volist.linkurl}-->' target='_blank'><img src='{$volist.logoimg}' alt='{$volist.linktitle}' /></a></li>
				<!--{/foreach}-->
			  </ul>
			</dd>
		  </dl>
		  <!--{/if}-->
		  <!--{if $volist_fontlink}-->
		  <dl>
			<dt>文字链接:</dt>
			<dd>
			  <ul>
			    <!--{foreach $volist_fontlink as $volist}-->
			    <li><a href='<!--{$volist.linkurl}-->' target='_blank'><!--{$volist.linktitle}--></a></li>
				<!--{/foreach}-->
			  </ul>
			</dd>
		  </dl>
		  <!--{/if}-->
		</div>
      </div>

    </div><!--#left //-->

    <div id="right">
      <h3 class="title"><span>产品分类</span></h3>
	  <div class="webnav"> 
        <!--{include file="<!--{$tplpath}-->block_productcat.tpl"}-->
      </div>

      <!--{include file="<!--{$tplpath}-->block_info.tpl"}--> 
      <!--{include file="<!--{$tplpath}-->block_contact.tpl"}-->  
    </div><!--#right //-->

    <div style="clear:both;"></div>
  </div>

  <!--{include file="<!--{$tplpath}-->block_footer.tpl"}-->
</div>
<script type="text/javascript" src="<!--{$skinpath}-->js/screen.js"></script>
</body>
</html>