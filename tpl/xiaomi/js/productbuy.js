$(function(){
	$('.delete').click(function(){
		var sid = $(this).parents('.goodThis').find('.brandBuy').attr('sid');
		var goodThis = $(this).parents('.goodThis');
		$.ajax({
			type:"post",
			url:"product-buy-cart-ajax.php",
			data:{"sid":sid},
			dataType:'json',
			success:function(phpDath){
				
				$('#J_cartTotalNum').html(phpDath.total_rows);
				$('#J_cartTotalPrice').html(phpDath.total);
				goodThis.remove();
				
			}
		});
		
	});
})
