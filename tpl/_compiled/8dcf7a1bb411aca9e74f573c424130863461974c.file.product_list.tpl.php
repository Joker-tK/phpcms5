<?php /* Smarty version Smarty-3.0.5, created on 2015-11-21 15:28:13
         compiled from "D:\WWW\phpcms5\tpl/blue/product_list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1843856501d0d2cb486-44132331%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8dcf7a1bb411aca9e74f573c424130863461974c' => 
    array (
      0 => 'D:\\WWW\\phpcms5\\tpl/blue/product_list.tpl',
      1 => 1448064296,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1843856501d0d2cb486-44132331',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_truncate')) include 'D:\WWW\phpcms5\source\core\plugins\modifier.truncate.php';
if (!is_callable('smarty_modifier_date_format')) include 'D:\WWW\phpcms5\source\core\plugins\modifier.date_format.php';
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $_smarty_tpl->getVariable('page_charset')->value;?>
" />
<title><?php echo $_smarty_tpl->getVariable('page_title')->value;?>
-<?php echo $_smarty_tpl->getVariable('copyright_header')->value;?>
</title>
<meta name="description" content="<?php echo $_smarty_tpl->getVariable('page_description')->value;?>
" />
<meta name="keywords" content="<?php echo $_smarty_tpl->getVariable('page_keyword')->value;?>
" />
<meta name="author" content="<?php echo $_smarty_tpl->getVariable('copyright_author')->value;?>
" />
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
style/main.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
style/css.css" />
<script type='text/javascript' src='<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
js/command.js'></script>
<script src="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
js/jquery-1.4.4.min.js" language="javascript" type="text/javascript"></script>
<script src="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
js/downnav.js" language="javascript" type="text/javascript"></script>
<script src="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
js/jquery.cross-slide.js" language="javascript" type="text/javascript"></script>
<!--[if IE 6]>
<script src="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
js/deletepng.js" language="javascript" type="text/javascript"></script>
<script type="text/javascript">
DD_belatedPNG.fix('.bg,img'); 
</script>
<![endif]-->
</head>
<body>
<div id="wrap">
  <?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('tplpath')->value)."block_head.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
  <?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('tplpath')->value)."block_banner.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
  <div id="web">
    <div id="left">

      <h3 class="title"><span><a href="<?php echo $_smarty_tpl->getVariable('url_index')->value;?>
">首 页</a>&nbsp;&nbsp;>>&nbsp;&nbsp;<?php echo $_smarty_tpl->getVariable('navigation')->value;?>
</span></h3>
	  <div class="webcontent">
	    <div id="product-list">
		  <style type="text/css">#product-list #plug2 dd li{ padding:2px 0px;}</style>
		  <div id="plug2">
		    <?php  $_smarty_tpl->tpl_vars['volist'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('product')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['volist']->key => $_smarty_tpl->tpl_vars['volist']->value){
?>
		    <dl style="height:165px;">
			  <dt style="width:158px;">
			    <a href="<?php echo $_smarty_tpl->tpl_vars['volist']->value['url'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['volist']->value['productname'];?>
" target="_blank"><img src="<?php echo $_smarty_tpl->tpl_vars['volist']->value['thumbfiles'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['volist']->value['productname'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['volist']->value['productname'];?>
" onload="javascript:DrawImage(this,'150','150');" /></a>
			  </dt>
			  <dd style="width:167px;">
			    <ul>
				  <li><b>名称 : </b><span class="title"><a href="<?php echo $_smarty_tpl->tpl_vars['volist']->value['url'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['volist']->value['productname'];?>
"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['volist']->value['productname'],8,0,"utf-8");?>
</a></span></li>
				  <li><b>编号 : </b><span><?php echo $_smarty_tpl->tpl_vars['volist']->value['productnum'];?>
</span></li>
				  <li><b>分类 : </b><span><?php echo $_smarty_tpl->tpl_vars['volist']->value['catename'];?>
</span></li>
				  <li><b>日期 : </b><span><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['volist']->value['timeline'],'%Y-%m-%d');?>
</span></li>
				</ul>
				<div class="detail"><a href="<?php echo $_smarty_tpl->tpl_vars['volist']->value['url'];?>
" title="详细介绍" target="_blank"><img src="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
images/picd.gif" title="详细介绍" alt="详细介绍" /></a></div>
			  </dd>
			</dl>
			<?php }} ?>
		  
		  </div><!-- #plug2 //-->
		  <div style="clear:both;"></div>
		</div>
		<?php if ($_smarty_tpl->getVariable('showpage')->value!=''){?>
		<div class="clear"></div>
		<div class="pageController"><?php echo $_smarty_tpl->getVariable('showpage')->value;?>
</div> 
		<?php }?>	  
	  </div><!-- $webcontent //-->
	
	</div><!-- #left //-->

    <div id="right">
	  <h3 class="title"><span>产品分类</span></h3>
	  <div class="webnav"> 
	  <?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('tplpath')->value)."block_productcat.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
	  </div><!-- $webnav -->
      <?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('tplpath')->value)."block_info.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?> 
      <?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('tplpath')->value)."block_contact.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>  
	</div><!-- $right //-->

    <div style="clear:both;"></div>
  </div>

  <?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('tplpath')->value)."block_footer.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
</div>
<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
js/screen.js"></script>
</body>
</html>