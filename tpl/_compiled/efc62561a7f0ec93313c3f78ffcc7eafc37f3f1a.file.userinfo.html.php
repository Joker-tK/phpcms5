<?php /* Smarty version Smarty-3.0.5, created on 2015-11-22 22:37:46
         compiled from "D:\WWW\phpcms8\tpl/xiaomi/userinfo.html" */ ?>
<?php /*%%SmartyHeaderCode:38835651d33ac4be60-53462175%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'efc62561a7f0ec93313c3f78ffcc7eafc37f3f1a' => 
    array (
      0 => 'D:\\WWW\\phpcms8\\tpl/xiaomi/userinfo.html',
      1 => 1357541916,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '38835651d33ac4be60-53462175',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_date_format')) include 'D:\WWW\phpcms8\source\core\plugins\modifier.date_format.php';
?><!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $_smarty_tpl->getVariable('page_charset')->value;?>
" />
<title><?php echo $_smarty_tpl->getVariable('page_title')->value;?>
-<?php echo $_smarty_tpl->getVariable('copyright_header')->value;?>
</title>
<meta name="description" content="<?php echo $_smarty_tpl->getVariable('page_metadescription')->value;?>
" />
<meta name="keywords" content="<?php echo $_smarty_tpl->getVariable('page_metakeyword')->value;?>
" />
<meta name="author" content="<?php echo $_smarty_tpl->getVariable('copyright_author')->value;?>
" />
<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
js/language.js"></script>
<link href="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
css/mergesite.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
css/global.css" rel="stylesheet" type="text/css"  />
<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
js/mergesite.js"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
js/command.js"></script>
<script type="text/javascript">
function mysubumit()
 {
 if (formcheck())
 {

	  document.editform.action="userinfo.php";
      document.editform.method="post";
      document.editform.submit();
   }  
}

  function formcheck() 
    { 
       if(document.getElementById("loginname").value== "") 
         { 
		 
		   alert("请输入用户名");
		   document.editform.loginname.focus();
		   document.editform.loginname.select(); 
           return false;
         }

	   if(document.editform.loginname.value.length<3) 
         { 
		   alert("用户名不能小于3位");
		   document.editform.loginname.focus();
		   document.editform.loginname.select(); 
           return false;
         }   

    return true;      
    }
</script>
<link rel="shortcut icon" href="/favicon.ico" />
</head>
<body>
<div id="header" class="cfl">
<div class="header_wrap">
<h1 class="header_logo">
<a href="<?php echo $_smarty_tpl->getVariable('urlpath')->value;?>
"><img src="<?php echo $_smarty_tpl->getVariable('urlpath')->value;?>
<?php echo $_smarty_tpl->getVariable('config')->value['logoimg'];?>
" width="170px" height="70px" ></a>
</h1>
<div class="header_menu">
<!-- star pub header# //-->
<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('tplpath')->value)."block_head.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
<!-- end header# //-->
</div>
</div>
</div>

<div class="main_content"> 
<div class="accessories">
	<div class="aside">
	<div class="nav">
			<h3>会员中心</h3>
			<ul>
<?php if ($_smarty_tpl->getVariable('username')->value==''){?>
<li><a href="register.php" target="_blank" style="margin-left: 5px;">注册会员</a></li>
<?php }else{ ?>
<li><a href='index.php'  title='会员中心首页'>会员中心首页</a></li>
<li><a  href='userinfo.php' target="_blank" title='修改基本信息'>修改基本信息</a></li>
<li><a  href='mymessage.php' title='我的留言'>我的留言</a></li>
<li><a  href='myorder.php' title='我的订单'>我的订单</a></li>
<li><a  href='resume.php' title='我的简历'>我的简历</a></li>
<li><a href='loginout.php' title='安全退出'>安全退出</a></li>
<?php }?>
			</ul>
		</div>
	</div>
	<!-- aside -->
 
<div class="main" style="float:right;width:758px">
<div class="promo">
<div class="mainnatming"><H5>修改资料</H5></div>
<div class="contentmain">
 <div>

  <form method="POST"  name="editform" >
  	<input type="hidden" name="uaction" id="uaction" value="usersave" />
  	<input type="hidden" name="userid" id="userid" value="<?php echo $_smarty_tpl->getVariable('id')->value;?>
" />
    <table cellpadding="1" border="0" cellspacing="1">
	<tr>
	<td class="zhuce_text"><span>用户名:</span></td>
	<td class="zhuce_input"><input id="loginname" name="loginname" value="<?php echo $_smarty_tpl->getVariable('user')->value['loginname'];?>
" type="text" class="input-text" /></td>
	<td class="zhuce_sm">*</td>
	</tr>
	<tr>
	<td class="zhuce_text"><span>登录密码:</span></td>
	<td class="zhuce_input"><input name="password" id="password" type="password" class="input-text" /></td>
	<td class="zhuce_sm">*</td>
	</tr>
	<tr>
	<td class="zhuce_text"><span>密码确认:</span></td>
	<td class="zhuce_input"><input  name="password1" id="password" type="password" class="input-text" /></td>
	<td class="zhuce_sm">*</td>
	</tr>
	<tr>
	<td class="zhuce_text"><span>性别：</span></td>
	<td class="zhuce_input">
     <?php if ($_smarty_tpl->getVariable('user')->value['sex']==1){?>
	<input name="Sex" type="radio" value="先生" checked="checked" class="inputnoborder" />先生 
     <input type="radio" name="Sex" value="女士" class="inputnoborder" />女士
	 <?php }else{ ?>
	<input name="Sex" type="radio" value="先生" class="inputnoborder" />先生 
     <input type="radio" name="Sex" value="女士" class="inputnoborder" checked="checked" />女士
	 <?php }?>
	 </td>
	<td class="zhuce_sm">*</td>
	</tr>
    <tr>
	<td class="zhuce_text"><span>真实姓名:</span></td>
    <td class="zhuce_input"><input name="realname" id="realname" value="<?php echo $_smarty_tpl->getVariable('user')->value['loginname'];?>
" type="text" class="input-text" /></td>
	<td class="zhuce_sm">*</td>
	</tr>
	<tr>
	<td class="zhuce_text"><span>联系电话:</span></td>
    <td class="zhuce_input"><input name="telno" id="telno" value="<?php echo $_smarty_tpl->getVariable('user')->value['telno'];?>
" type="text" class="input-text" /></td>
	<td class="zhuce_sm"></td>
	</tr>
	<tr>
	<td class="zhuce_text"><span>生日:</span></td>
    <td class="zhuce_input"><input id="brothday" value="<?php echo $_smarty_tpl->getVariable('user')->value['brothday'];?>
" name="brothday" type="text" class="input-text" /></td>
	<td class="zhuce_sm"></td>
	</tr>
	<tr>
	<td class="zhuce_text"><span>E-mail:</span></td>
    <td class="zhuce_input"><input id="email" name="email" value="<?php echo $_smarty_tpl->getVariable('user')->value['email'];?>
" type="text" class="input-text" /></td>
	<td class="zhuce_sm">*</td>
	</tr>
	<tr>
	<td class="zhuce_text"><span>联系地址:</span></td>
    <td class="zhuce_input"><input name="address" id="address" value="<?php echo $_smarty_tpl->getVariable('user')->value['address'];?>
" type="text" class="input-text" /></td>
	<td class="zhuce_sm"></td>
	</tr>
	<tr>
	<td class="zhuce_text"><span>注册时间:</span></td>
    <td class="zhuce_input"><?php echo smarty_modifier_date_format($_smarty_tpl->getVariable('user')->value['regdate'],"%Y-%m-%d %H:%M:%S");?>
</td>
	<td class="zhuce_sm"></td>
	</tr>
	<tr>
	<td class="zhuce_text"><span>登陆次数:</span></td>
    <td class="zhuce_input"><?php echo $_smarty_tpl->getVariable('user')->value['pointnum'];?>
</td>
	<td class="zhuce_sm"></td>
	</tr>
	<tr>
	<td class="zhuce_text"><span>最后登陆IP:</span></td>
    <td class="zhuce_input"><?php echo $_smarty_tpl->getVariable('user')->value['lastloginip'];?>
</td>
	<td class="zhuce_sm"></td>
	</tr>
	<tr>
	<td class="zhuce_text"><span>个人说明:</span></td>
    <td class="zhuce_input" colspan="2"><textarea name='memo' id='memo' style='width:260px;height:65px;overflow:auto;color:#444444;'><?php echo $_smarty_tpl->getVariable('user')->value['memo'];?>
</textarea></td>
	</tr>
	<tr>
	<td class="zhuce_text"></td>
	<td class="zhuce_subimt" colspan="2">
	<input type="button" id="b_save" name="b_save" onClick="mysubumit()" value="立即修改" />
	<input type="reset" value="重填" id="b_reset" name="b_reset" /></td>
    </tr>
	</table>
</form>



 </div>
</div>
</div>
</div>
</div>

</div>
<div style="width:100%;height:10px;clear:both;"></div>


<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('tplpath')->value)."block_footer.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>

<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('tplpath')->value)."block_onlinechat.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
</body>
</html>



