<?php /* Smarty version Smarty-3.0.5, created on 2015-11-21 14:47:14
         compiled from "D:\WWW\phpcms5\admin/liangjingcms/authgroup.tpl" */ ?>
<?php /*%%SmartyHeaderCode:27172565013727c2447-11383899%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0771612b74289d95ffdfe4fcb5d8fd9bb79c2c0b' => 
    array (
      0 => 'D:\\WWW\\phpcms5\\admin/liangjingcms/authgroup.tpl',
      1 => 1448064337,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '27172565013727c2447-11383899',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_date_format')) include 'D:\WWW\phpcms5\source\core\plugins\modifier.date_format.php';
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $_smarty_tpl->getVariable('page_charset')->value;?>
" />
<title>权限组</title>
<meta name="author" content="<?php echo $_smarty_tpl->getVariable('copyright_author')->value;?>
" />
<link type="text/css" rel="stylesheet" href="liangjingcms/css/admin_style.css" />
<script type='text/javascript' src='js/jquery-1.4.4.min.js'></script>
<script type='text/javascript' src='js/command.js'></script>
<script type="text/javascript" src='../data/editor/kindeditor.js'></script>
</head>
<body>
<?php if ($_smarty_tpl->getVariable('action')->value==''){?>
<div class="main-wrap">
  <div class="path"><p>当前位置：系统设置<span>&gt;&gt;</span>管理组设置</p></div>
  <div class="main-cont">
    <h3 class="title"><a href="ljcms_authgroup.php?action=add" class="btn-general"><span>添加管理组</span></a>管理组</h3>
	<form action="ljcms_authgroup.php" method="post" name="myform" id="myform" style="margin:0">
	<input type="hidden" name="action" id="action" value="del" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <thead class="tb-tit-bg">
	  <tr>
	    <th width="10%"><div class="th-gap">编号</div></th>
		<th width="15%"><div class="th-gap">组名</div></th>
		<th width="8%"><div class="th-gap">状态</div></th>
		<th width="8%"><div class="th-gap">排序</div></th>
		<th width="18%"><div class="th-gap">录入时间</div></th>
		<th><div class="th-gap">备注说明</div></th>
		<th width="15%"><div class="th-gap">操作</div></th>
	  </tr>
	  </thead>
	  <tfoot class="tb-foot-bg"></tfoot>
	  <?php  $_smarty_tpl->tpl_vars['volist'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('authgroup')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['volist']->key => $_smarty_tpl->tpl_vars['volist']->value){
?>
	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">
	    <td align="center"><input name="id[]" type="checkbox" value="<?php echo $_smarty_tpl->tpl_vars['volist']->value['groupid'];?>
" onClick="checkItem(this, 'chkAll')"></td>
		<td><?php echo $_smarty_tpl->tpl_vars['volist']->value['groupname'];?>
</td>
		<td align="center">
		<?php if ($_smarty_tpl->tpl_vars['volist']->value['flag']==0){?>
			<input type="hidden" id="attr_flag<?php echo $_smarty_tpl->tpl_vars['volist']->value['groupid'];?>
" value="flagopen" />
			<img id="flag<?php echo $_smarty_tpl->tpl_vars['volist']->value['groupid'];?>
" src="liangjingcms/images/no.gif" onClick="javascript:fetch_ajax('flag','<?php echo $_smarty_tpl->tpl_vars['volist']->value['groupid'];?>
');" style="cursor:pointer;">
		<?php }else{ ?>
			<input type="hidden" id="attr_flag<?php echo $_smarty_tpl->tpl_vars['volist']->value['groupid'];?>
" value="flagclose" />
			<img id="flag<?php echo $_smarty_tpl->tpl_vars['volist']->value['groupid'];?>
" src="liangjingcms/images/yes.gif" onClick="javascript:fetch_ajax('flag','<?php echo $_smarty_tpl->tpl_vars['volist']->value['groupid'];?>
');" style="cursor:pointer;">	
		<?php }?>
		</td>
		<td align="center"><?php echo $_smarty_tpl->tpl_vars['volist']->value['orders'];?>
</td>
		<td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['volist']->value['timeline'],"%Y/%m/%d %H:%M:%S");?>
</td>
		<td align="left"><?php echo $_smarty_tpl->tpl_vars['volist']->value['intro'];?>
</td>
		<td align="center"><a href="ljcms_authgroup.php?action=edit&id=<?php echo $_smarty_tpl->tpl_vars['volist']->value['groupid'];?>
&page=<?php echo $_smarty_tpl->getVariable('page')->value;?>
" class="icon-set">设置</a>&nbsp;&nbsp;<a href="ljcms_authgroup.php?action=del&id[]=<?php echo $_smarty_tpl->tpl_vars['volist']->value['groupid'];?>
" onClick="{if(confirm('确定要删除该信息?')){return true;} return false;}" class="icon-del">删除</a></td>
	  </tr>
	  <?php }} else { ?>
      <tr>
	    <td colspan="7" align="center">暂无信息</td>
	  </tr>
	  <?php } ?>
	  <?php if ($_smarty_tpl->getVariable('total')->value>0){?>
	  <tr>
		<td align="center"><input name="chkAll" type="checkbox" id="chkAll" onClick="checkAll(this, 'id[]')" value="checkbox"></td>
		<td class="hback" colspan="6"><input class="button" name="btn_del" type="button" value="删 除" onClick="{if(confirm('确定删除选定信息吗!?')){$('#action').val('del');$('#myform').submit();return true;}return false;}" class="button">&nbsp;&nbsp;共[ <b><?php echo $_smarty_tpl->getVariable('total')->value;?>
</b> ]条记录</td>
	  </tr>
	  <?php }?>
	</table>
	</form>
	<?php if ($_smarty_tpl->getVariable('pagecount')->value>1){?>
	<table width='95%' border='0' cellspacing='0' cellpadding='0' align='center' style="margin-top:10px;">
	  <tr>
		<td align='center'><?php echo $_smarty_tpl->getVariable('showpage')->value;?>
</td>
	  </tr>
	</table>
	<?php }?>
  </div>
</div>
<?php }?>

<?php if ($_smarty_tpl->getVariable('action')->value=="add"){?>
<div class="main-wrap">
  <div class="path"><p>当前位置：系统设置<span>&gt;&gt;</span>添加管理组</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_authgroup.php" class="btn-general"><span>返回列表</span></a>添加管理组</h3>
    <form name="myform" id="myform" method="post" action="ljcms_authgroup.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveadd" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">组名：<span class='f_red'>*</span></td>
		<td class='hback' width="85%"><input type="text" name="groupname" id="groupname" class="input-txt" /> <span class='f_red' id="dgroupname"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>排序：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="orders" id="orders" class="input-txt" value="<?php echo $_smarty_tpl->getVariable('orders')->value;?>
" /> <span class='f_red' id="dorders"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>设置：<span class='f_red'></span></td>
		<td class='hback'><?php echo $_smarty_tpl->getVariable('flag_checkbox')->value;?>
</td>
	  </tr>
	  <tr>
		<td class='hback_1'>备注说明： </td>
		<td class='hback'><textarea name="intro" id='intro' style='width:60%;height:65px;overflow:auto;color:#444444;'></textarea></td>
	  </tr>
	  <tr>
		<td class='hback_1'>操作权限：<span class='f_red'></span></td>
		<td class='hback'><?php echo $_smarty_tpl->getVariable('auth_checkbox')->value;?>
</td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="添加保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<?php }?>

<?php if ($_smarty_tpl->getVariable('action')->value=="edit"){?>
<div class="main-wrap">
  <div class="path"><p>当前位置：系统设置<span>&gt;&gt;</span>编辑管理组</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_authgroup.php?<?php echo $_smarty_tpl->getVariable('comeurl')->value;?>
" class="btn-general"><span>返回列表</span></a>编辑管理组</h3>
    <form name="myform" id="myform" method="post" action="ljcms_authgroup.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveedit" />
	<input type="hidden" name="id" value="<?php echo $_smarty_tpl->getVariable('id')->value;?>
" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">组名：<span class='f_red'>*</span></td>
		<td class='hback' width="85%"><input type="text" name="groupname" id="groupname" class="input-txt" value="<?php echo $_smarty_tpl->getVariable('authgroup')->value['groupname'];?>
" /> <span class='f_red' id="dgroupname"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>排序：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="orders" id="orders" class="input-txt" value="<?php echo $_smarty_tpl->getVariable('authgroup')->value['orders'];?>
" /> <span class='f_red' id="dorders"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>设置：<span class='f_red'></span></td>
		<td class='hback'><?php echo $_smarty_tpl->getVariable('flag_checkbox')->value;?>
</td>
	  </tr>
	  <tr>
		<td class='hback_1'>备注说明： </td>
		<td class='hback'><textarea name="intro" id='intro' style='width:60%;height:65px;overflow:auto;color:#444444;'><?php echo $_smarty_tpl->getVariable('authgroup')->value['intro'];?>
</textarea></td>
	  </tr>
	  <tr>
		<td class='hback_1'>操作权限：<span class='f_red'></span></td>
		<td class='hback'><?php echo $_smarty_tpl->getVariable('auth_checkbox')->value;?>
</td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="更新保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<?php }?>

</body>
</html>
<script type="text/javascript">
function checkform() {
	var t = "";
	var v = "";

	t = "groupname";
	v = $("#"+t).val();
	if(v=="") {
		dmsg("管理组名不能为空！", t);
		return false;
	}
	return true;
}
</script>
