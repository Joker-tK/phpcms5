<?php /* Smarty version Smarty-3.0.5, created on 2015-11-30 10:34:28
         compiled from "D:\WWW\phpcms21\tpl/xiaomi/productbuy.html" */ ?>
<?php /*%%SmartyHeaderCode:31810565bb5b4d23035-56706507%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fa627a8124d72fccd6b280c2539dfb9455d73d8e' => 
    array (
      0 => 'D:\\WWW\\phpcms21\\tpl/xiaomi/productbuy.html',
      1 => 1448850858,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '31810565bb5b4d23035-56706507',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html>
<html xml:lang="zh-CN" lang="zh-CN">

	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<script src=""></script>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta charset="UTF-8">
		<title>购物车</title>
		<meta name="viewport" content="width=1226">
		<link rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
css/base.css">
		<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
css/cart.css">
		<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
js/jquery-1.8.2.min.js"></script>
		<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
js/productbuy.js"></script>
		<script type="text/javascript">
			$(function(){
				//点击减少
				$('.decrease').click(function(){
					var inputNum = $(this).parents('.goodThis').find('.inputNum').val();
					inputNum = parseInt(inputNum);
					inputNum = inputNum-1;
					$(this).parents('.goodThis').find('.inputNum').val(inputNum);
					var sid = $(this).parents('.goodThis').find('.brandBuy').attr('sid');
					var total = $(this).parents('.goodThis').find('.col-total');
					$.ajax({
						type:"post",
						url:'cartajax.php',
						data:{"inputNum":inputNum,'sid':sid},
						dataType:'json',
						success:function(phpDath){
							total.html(phpDath.total);
							$('.totelnum').html(phpDath.goodsNum+'件商品');
							$('#J_cartTotalPrice').html(phpDath.TotalPrice);
							$('.yourPay').children('i').html(phpDath.TotalPrice);
							$('#J_cartTotalNum').html(phpDath.goodsNum);
							
						}
					});
				});
				//点击增加
				$('.increase').click(function(){
					
					var inputNum = $(this).parents('.goodThis').find('.inputNum').val();
					inputNum = parseInt(inputNum);
					inputNum = inputNum+1;
					var total = $(this).parents('.goodThis').find('.col-total');
					$(this).parents('.goodThis').find('.inputNum').val(inputNum);
					var sid = $(this).parents('.goodThis').find('.brandBuy').attr('sid');
					$.ajax({
						type:"post",
						url:"cartajax.php",
						data:{"inputNum":inputNum,'sid':sid},
						dataType:'json',
						success:function(phpDath){
							total.html(phpDath.total);
							$('.totelnum').html(phpDath.goodsNum+'件商品');
							$('#J_cartTotalPrice').html(phpDath.TotalPrice);
							$('.yourPay').children('i').html(phpDath.TotalPrice);
							$('#J_cartTotalNum').html(phpDath.goodsNum);
						}
					});
				});
				//input框输入离开焦点事件
				$('.inputNum').blur(function(){
					var inputNum =$(this).val();
					
					if(inputNum<=0){
						return false;
					}
					if(isNaN(inputNum)){
						alert('请输入正确的购买数字');
						return false;
					}
						var sid = $(this).parents('.goodThis').find('.brandBuy').attr('sid');
						var total = $(this).parents('.goodThis').find('.col-total');
					$.ajax({
						type:"post",
						url:"cartajax.php",
						data:{"inputNum":inputNum,'sid':sid},
						dataType:'json',
						success:function(phpDath){
							total.html(phpDath.total);
							$('.totelnum').html(phpDath.goodsNum+'件商品');
							$('#J_cartTotalPrice').html(phpDath.TotalPrice);
							$('.yourPay').children('i').html(phpDath.TotalPrice);
							$('#J_cartTotalNum').html(phpDath.goodsNum);
						}
					});
					
				});
				})
					
				
			
		</script>
	</head>

	<body>
		<div class="site-header site-mini-header">
			<div class="container">
				<div class="header-logo">
					<a class="logo ir" href="<?php echo $_smarty_tpl->getVariable('urlpath')->value;?>
" title="小米官网">小米官网</a>
				</div>
				<div class="header-title has-more" id="J_miniHeaderTitle">
					<h2>我的购物车</h2>
					<p><a href="http://static.mi.com/feedback/" target="_blank">问题反馈 &gt;</a></p>
				</div>
				<div class="topbar-info" id="J_userInfo"><span class="user"><a onclick="_msq.push(['trackEvent', '08fae3d5cb3abaaf-89cc569c9671d567', 'http://my.mi.com/portal', 'pcpid']);" data-stat-id="89cc569c9671d567" class="user-name" href="http://my.mi.com/portal" target="_blank"><span class="name">23033422</span>
					<i
					class="iconfont"></i>
						</a>
						<ul class="user-menu">
							<li><a >个人中心</a></li>
							<li><a>评价晒单</a></li>
							<li><a>我的喜欢</a></li>
							<li><a>小米账户</a></li>
							<li><a onclick="_msq.push(['trackEvent', '08fae3d5cb3abaaf-7014141d5b446729', 'http://order.mi.com/site/logout', 'pcpid']);" data-stat-id="7014141d5b446729" href="http://order.mi.com/site/logout">退出登录</a></li>
						</ul>
						</span><span class="sep">|</span><a onclick="_msq.push(['trackEvent', '08fae3d5cb3abaaf-1e35b5f1a579a3ca', 'http://static.mi.com/order/', 'pcpid']);" data-stat-id="1e35b5f1a579a3ca" class="link link-order" href="http://static.mi.com/order/" target="_blank">我的订单</a></div>
			</div>
		</div>

		<div class="page-main">

			<div class="container">
				<div class="cart-loading loading hide" id="J_cartLoading">
					<div class="loader"></div>
				</div>
				<div class="cart-empty hide" id="J_cartEmpty">
					<h2>您的购物车还是空的！</h2>
					<p class="login-desc">登录后将显示您之前加入的商品</p>
					<a onclick="_msq.push(['trackEvent', '08fae3d5cb3abaaf-7874490dbcbc1e60', '#', 'pcpid']);" data-stat-id="7874490dbcbc1e60" href="#" class="btn btn-primary btn-login" id="J_loginBtn">立即登录</a>
					<a onclick="_msq.push(['trackEvent', '08fae3d5cb3abaaf-9e3768d6a6cc9414', 'http://list.mi.com/0', 'pcpid']);" data-stat-id="9e3768d6a6cc9414" href="http://list.mi.com/0" class="btn btn-primary btn-shoping J_goShoping">马上去购物</a>
				</div>
				<div id="J_cartBox" class="">
					<div class="cart-goods-list">
						<div class="list-head clearfix">
							<div class="col col-check"><i class="iconfont icon-checkbox icon-checkbox-selected" id="J_selectAll">√</i>全选</div>
							<div class="col col-img">&nbsp;</div>
							<div class="col col-name">商品名称</div>
							<div class="col col-price">单价</div>
							<div class="col col-num">数量</div>
							<div class="col col-total">小计</div>
							<div class="col col-action">操作</div>
						</div>
						 
						<div class="list-body" id="J_cartListBody">
							<div class="item-box">
								<div class="item-table J_cartGoods">
									<?php  $_smarty_tpl->tpl_vars['volist'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('cart')->value['goods']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['volist']->key => $_smarty_tpl->tpl_vars['volist']->value){
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['volist']->key;
?>
									<div class="item-row clearfix goodThis">
										<div class="col col-check brandBuy" sid='<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
'>
											<i class="iconfont icon-checkbox icon-checkbox-selected J_itemCheckbox" data-itemid="2152400007_0_buy" data-status="1">√</i>
										</div>
										<div class="col col-img">
											<a href="" target="_blank">
												<img alt="" src="<?php echo $_smarty_tpl->tpl_vars['volist']->value['img'];?>
" height="80" width="80">
											</a>
										</div>
										<div class="col col-name">
											<div class="tags">
											</div>
											<h3 class="name"> 
					                            <a href="" target="_blank"><?php echo $_smarty_tpl->tpl_vars['volist']->value['name'];?>
</a>  
					                        </h3>
											
										</div>
										<div class="col col-price"> <?php echo $_smarty_tpl->tpl_vars['volist']->value['price'];?>
 </div>
										<div class="col col-num">
											<div class="change-goods-num clearfix J_changeGoodsNum">
												<a href="javascript:void(0)" class="">
													<i class="iconfont decrease">-</i>
												</a>
												<input tyep="text" name="2152400007_0_buy" value="<?php echo $_smarty_tpl->tpl_vars['volist']->value['num'];?>
" class="goods-num J_goodsNum inputNum">
												<a href="javascript:void(0)" class="">
													<i class="iconfont increase">+</i>
												</a>
												
											</div>
										</div>
										
										<div class="col col-total"> <?php echo $_smarty_tpl->tpl_vars['volist']->value['total'];?>
元
											<p class="pre-info"> </p>
										</div>
										<div class="col col-action">
											<a class="del J_delGoods">
												<i class="iconfont delete"></i>
											</a>
										</div>
									</div>
									<?php }} ?>
								</div>
							</div>

						</div>
						
						<div class="cart-bar clearfix" id="J_cartBar">
							<div class="section-left">
								<a href="<?php echo $_smarty_tpl->getVariable('urlpath')->value;?>
" class="back-shopping J_goShoping">继续购物</a>
								<span class="cart-total">共 <i id="J_cartTotalNum"><?php echo $_smarty_tpl->getVariable('cart')->value['total_rows'];?>
</i> 件商品，已选择 <i id="J_selTotalNum">4</i> 件</span>
								<span class="cart-coudan hide" id="J_coudanTip">
                        ，还需 <i id="J_postFreeBalance"></i> 元即可免邮费  <a  href="javascript:void(0);" id="J_showCoudan">立即凑单</a>
                    </span>
							</div>
							<span class="activity-money hide">
                    活动优惠：减 <i id="J_cartActivityMoney">0</i> 元
                </span>
							<span class="total-price">
                    合计（不含运费）：<em id="J_cartTotalPrice" class="totalPrice"><?php echo $_smarty_tpl->getVariable('cart')->value['total'];?>
</em>元
                </span>
							<a  class="btn btn-a btn btn-primary" id="J_goCheckout">去结算</a>

							<div class="no-select-tip hide" id="J_noSelectTip">
								请勾选需要结算的商品
								<i class="arrow arrow-a"></i>
								<i class="arrow arrow-b"></i>
							</div>
						</div>
					</div>

				</div>
			</div>
			<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('tplpath')->value)."site_footer.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>