<?php /* Smarty version Smarty-3.0.5, created on 2015-11-26 14:33:38
         compiled from "D:\WWW\phpcms21\admin/liangjingcms/productcate.tpl" */ ?>
<?php /*%%SmartyHeaderCode:249935656a7c243c921-35776258%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4a94d80f7905b40ed78690618c2177409e0a0f46' => 
    array (
      0 => 'D:\\WWW\\phpcms21\\admin/liangjingcms/productcate.tpl',
      1 => 1448412860,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '249935656a7c243c921-35776258',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_date_format')) include 'D:\WWW\phpcms21\source\core\plugins\modifier.date_format.php';
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $_smarty_tpl->getVariable('page_charset')->value;?>
" />
<title>产品分类</title>
<meta name="author" content="<?php echo $_smarty_tpl->getVariable('copyright_author')->value;?>
" />
<link type="text/css" rel="stylesheet" href="liangjingcms/css/admin_style.css" media="screen" />
<script type='text/javascript' src='js/jquery-1.4.4.min.js'></script>
<script type='text/javascript' src='js/command.js'></script>
<script type="text/javascript" src='../data/editor/kindeditor.js'></script>
</head>
<body>
<table class="tableBorder" width="95%" border="0" align="center" cellpadding="5" cellspacing="1">
<tr>
<td width="100%" class="leftrow">
<?php if ($_smarty_tpl->getVariable('action')->value==''){?>
<div class="main-wrap">
  <div class="path"><p>当前位置：产品管理<span>&gt;&gt;</span>产品分类</p></div>
  <div class="main-cont">
    <h3 class="title"><a href="ljcms_productcate.php?action=add" class="btn-general"><span>添加分类</span></a>产品分类</h3>

    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <thead class="tb-tit-bg">
	  <tr>
	    <th width="8%"><div class="th-gap">ID</div></th>
		<th width="18%"><div class="th-gap">分类名称</div></th>
		<th width="7%"><div class="th-gap">排序</div></th>
		<th width="10%"><div class="th-gap">图标</div></th>
		<th width="8%"><div class="th-gap">CSS</div></th>
		<th width="8%"><div class="th-gap">链接</div></th>
		<th width="10%"><div class="th-gap">产品数</div></th>
		<th width="7%"><div class="th-gap">状态</div></th>
		<th width="10%"><div class="th-gap">录入时间</div></th>
		<th><div class="th-gap">操作</div></th>
	  </tr>
	  </thead>
	  <tfoot class="tb-foot-bg"></tfoot>
	  <?php  $_smarty_tpl->tpl_vars['volist'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('cate')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['volist']->key => $_smarty_tpl->tpl_vars['volist']->value){
?>
	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">
	    <td align="center"><?php echo $_smarty_tpl->tpl_vars['volist']->value['cateid'];?>
</td>
	    <td align="left"><?php if ($_smarty_tpl->tpl_vars['volist']->value['depth']==0){?><b><?php echo $_smarty_tpl->tpl_vars['volist']->value['tree_catename'];?>
</b><?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['volist']->value['tree_catename'];?>
<?php }?></td>
		<td align="center"><?php echo $_smarty_tpl->tpl_vars['volist']->value['orders'];?>
</td>
		<td align="center"><?php if ($_smarty_tpl->tpl_vars['volist']->value['img']!=''){?><img src="../<?php echo $_smarty_tpl->tpl_vars['volist']->value['img'];?>
" border="0" /><?php }else{ ?><font color="#999999">无图标</font><?php }?></td>
		<td align="center"><?php echo $_smarty_tpl->tpl_vars['volist']->value['cssname'];?>
</td>
		<td align="center"><?php if ($_smarty_tpl->tpl_vars['volist']->value['linktype']==1){?><font color="green">内部</font><?php }else{ ?><font color="blue">外部</font><?php }?></td>
		<td align="center"><?php echo $_smarty_tpl->tpl_vars['volist']->value['content_count'];?>
</td>
		<td align="center">
		<?php if ($_smarty_tpl->tpl_vars['volist']->value['flag']==0){?>
			<input type="hidden" id="attr_flag<?php echo $_smarty_tpl->tpl_vars['volist']->value['cateid'];?>
" value="flagopen" />
			<img id="flag<?php echo $_smarty_tpl->tpl_vars['volist']->value['cateid'];?>
" src="liangjingcms/images/no.gif" onClick="javascript:fetch_ajax('flag','<?php echo $_smarty_tpl->tpl_vars['volist']->value['cateid'];?>
');" style="cursor:pointer;">
		<?php }else{ ?>
			<input type="hidden" id="attr_flag<?php echo $_smarty_tpl->tpl_vars['volist']->value['cateid'];?>
" value="flagclose" />
			<img id="flag<?php echo $_smarty_tpl->tpl_vars['volist']->value['cateid'];?>
" src="liangjingcms/images/yes.gif" onClick="javascript:fetch_ajax('flag','<?php echo $_smarty_tpl->tpl_vars['volist']->value['cateid'];?>
');" style="cursor:pointer;">	
		<?php }?>
		</td>
		<td align="center"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['volist']->value['timeline'],"%Y/%m/%d");?>
</td>
		<td align="center"><a href="ljcms_productcate.php?action=edit&id=<?php echo $_smarty_tpl->tpl_vars['volist']->value['cateid'];?>
&page=<?php echo $_smarty_tpl->getVariable('page')->value;?>
" class="icon-set">设置</a>&nbsp;&nbsp;<a href="ljcms_productcate.php?action=del&id=<?php echo $_smarty_tpl->tpl_vars['volist']->value['cateid'];?>
" onClick="{if(confirm('确定要删除该信息?')){return true;} return false;}" class="icon-del">删除</a></td>
	  </tr>
	  <?php }} else { ?>
      <tr>
	    <td colspan="10" align="center">暂无信息</td>
	  </tr>
	  <?php } ?>
	</table>


  </div>
</div>
<?php }?>

<?php if ($_smarty_tpl->getVariable('action')->value=="setting"){?>
<div class="main-wrap">
  <div class="path"><p>当前位置：产品管理<span>&gt;&gt;</span>设置产品分类</p></div>
  <div class="main-cont">
    <h3 class="title"><a href="ljcms_productcate.php?action=add" class="btn-general"><span>添加分类</span></a>设置产品分类</h3>
	<form action="ljcms_productcate.php" method="post" name="myform" id="myform" style="margin:0">
	<input type="hidden" name="action" id="action" value="savesetting" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <thead class="tb-tit-bg">
	  <tr>
	    <th width="8%"><div class="th-gap">选择</div></th>
		<th width="20%"><div class="th-gap">分类名称</div></th>
		<th width="20%"><div class="th-gap">Meta标题</div></th>
		<th width="20%"><div class="th-gap">Meta关键字</div></th>
		<th width="20%"><div class="th-gap">Meta描述</div></th>
		<th><div class="th-gap">排序</div></th>
	  </tr>
	  </thead>
	  <tfoot class="tb-foot-bg"></tfoot>
	  <?php  $_smarty_tpl->tpl_vars['volist'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('cate')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['volist']->key => $_smarty_tpl->tpl_vars['volist']->value){
?>
	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">
	    <td align="center"><input name="id[]" type="checkbox" value="<?php echo $_smarty_tpl->tpl_vars['volist']->value['cateid'];?>
" onClick="checkItem(this, 'chkAll')"></td>
		<td align="center"><input type="text" name="catename_<?php echo $_smarty_tpl->tpl_vars['volist']->value['cateid'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['volist']->value['catename'];?>
" size="18" /></td>
		<td align="center"><input type="text" name="metatitle_<?php echo $_smarty_tpl->tpl_vars['volist']->value['cateid'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['volist']->value['metatitle'];?>
" size="18" /></td>
		<td align="center"><input type="text" name="metakeyword_<?php echo $_smarty_tpl->tpl_vars['volist']->value['cateid'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['volist']->value['metakeyword'];?>
" size="18" /></td>
		<td align="center"><input type="text" name="metadescription_<?php echo $_smarty_tpl->tpl_vars['volist']->value['cateid'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['volist']->value['metadescription'];?>
" size="18" /></td>
		<td align="center"><input type="text" name="orders_<?php echo $_smarty_tpl->tpl_vars['volist']->value['cateid'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['volist']->value['orders'];?>
" size="6" /></td>
	  </tr>
	  <?php }} else { ?>
      <tr>
	    <td colspan="6" align="center">暂无信息</td>
	  </tr>
	  <?php } ?>
	  <?php if ($_smarty_tpl->getVariable('cate')->value){?>
	  <tr>
		<td align="center"><input name="chkAll" type="checkbox" id="chkAll" onClick="checkAll(this, 'id[]')" value="checkbox"></td>
		<td class="hback" colspan="5"><input class="button" name="btn_del" type="button" value="批量更新" onClick="{if(confirm('确定更新选定信息吗!?')){$('#action').val('savesetting');$('#myform').submit();return true;}return false;}" class="button"></td>
	  </tr>
	  <?php }?>
	</table>
	</form>

  </div>
</div>
<?php }?>


<?php if ($_smarty_tpl->getVariable('action')->value=="add"){?>
<div class="main-wrap">
  <div class="path"><p>当前位置：产品管理<span>&gt;&gt;</span>添加产品分类</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_productcate.php" class="btn-general"><span>返回列表</span></a>添加产品分类</h3>
    <form name="myform" id="myform" method="post" action="ljcms_productcate.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveadd" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">分类名称：<span class='f_red'>*</span></td>
		<td class='hback' width="85%"><input type="text" name="catename" id="catename" class="input-txt" /> <span class='f_red' id="dcatename"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta标题 </td>
		<td class='hback'><input type="text" name="metatitle" id="metatitle" class="input-txt" />  <span id='dmetatitle' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta关键字 <span class="f_red"></span></td>
		<td class='hback'><textarea name="metakeyword" id="metakeyword" style="width:60%;height:45px;display:;overflow:auto;"></textarea> <span id='dmetakeyword' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta描述 <span class="f_red"></span></td>
		<td class='hback'><textarea name="metadescription" id="metadescription" style="width:60%;height:45px;display:;overflow:auto;"></textarea> <span id='dmetadescription' class='f_red'></span></td>
	  </tr>

	  <tr>
		<td class='hback_1'>所属分类：<span class='f_red'></span></td>
		<td class='hback'><?php echo $_smarty_tpl->getVariable('cate_select')->value;?>
 （不选择表示作为一级分类，最多支持二级分类） <span class='f_red' id="drootid"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>分类排序：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="orders" id="orders" class="input-s" value="<?php echo $_smarty_tpl->getVariable('orders')->value;?>
" /> <span class='f_red' id="dorders"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>参数设置：<span class='f_red'></span></td>
		<td class='hback'><?php echo $_smarty_tpl->getVariable('flag_checkbox')->value;?>
</td>
	  </tr>
	  <tr>
		<td class='hback_1'>CSS样式：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="cssname" id="cssname" class="input-s" /> <span class='f_red' id="dcssname"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>样式图标 <span class="f_red"></span> </td>
		<td class='hback'>
		  <input type="hidden" name="img" id="img" /><span id='dimg' class='f_red'></span>
		  <iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?comeform=myform&inputid=img&thumbflag=0'></iframe>
		</td>
	  </tr>
	  <tr>
		<td class='hback_1'>打开方式：<span class='f_red'></span></td>
		<td class='hback'><select name="target" id="target"><option value="1">本页面</option><option value="2">新页面</option></select></td>
	  </tr>
	  <tr>
		<td class='hback_1'>链接类型：<span class='f_red'></span></td>
		<td class='hback'><input type="radio" name="linktype" value="1" checked />内部链接，<input type="radio" name="linktype" value="2" />外部链接</td>
	  </tr>
	  <tr>
		<td class='hback_1'>外部URL：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="linkurl" id="linkurl" class="input-txt" /> <span class='f_red' id="dlinkurl"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>分类简介： </td>
		<td class='hback'><textarea name="intro" id="intro" style='width:60%;height:65px;overflow:auto;color:#444444;'></textarea><br />（500字符以内）</td>
	  </tr>

	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="添加保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<?php }?>

<?php if ($_smarty_tpl->getVariable('action')->value=="edit"){?>
<div class="main-wrap">
  <div class="path"><p>当前位置：产品管理<span>&gt;&gt;</span>编辑产品分类</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_productcate.php?<?php echo $_smarty_tpl->getVariable('comeurl')->value;?>
" class="btn-general"><span>返回列表</span></a>编辑产品分类</h3>
    <form name="myform" id="myform" method="post" action="ljcms_productcate.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveedit" />
	<input type="hidden" name="id" value="<?php echo $_smarty_tpl->getVariable('id')->value;?>
" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">分类名称：<span class='f_red'>*</span></td>
		<td class='hback' width="85%"><input type="text" name="catename" id="catename" class="input-txt" value="<?php echo $_smarty_tpl->getVariable('cate')->value['catename'];?>
" /> <span class='f_red' id="dcatename"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta标题 </td>
		<td class='hback'><input type="text" name="metatitle" id="metatitle" class="input-txt" value="<?php echo $_smarty_tpl->getVariable('cate')->value['metatitle'];?>
" />  <span id='dmetatitle' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta关键字 <span class="f_red"></span></td>
		<td class='hback'>
		  <textarea name="metakeyword" id="metakeyword" style="width:60%;height:45px;display:;overflow:auto;"><?php echo $_smarty_tpl->getVariable('cate')->value['metakeyword'];?>
</textarea> <span id='dmetakeyword' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta描述 <span class="f_red"></span></td>
		<td class='hback'>
		  <textarea name="metadescription" id="metadescription" style="width:60%;height:45px;display:;overflow:auto;"><?php echo $_smarty_tpl->getVariable('cate')->value['metadescription'];?>
</textarea> <span id='dmetadescription' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>所属分类：<span class='f_red'></span></td>
		<td class='hback'><?php echo $_smarty_tpl->getVariable('cate_select')->value;?>
 （不选择表示作为一级分类，最多支持二级分类） <span class='f_red' id="drootid"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>分类排序：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="orders" id="orders" class="input-s" value="<?php echo $_smarty_tpl->getVariable('cate')->value['orders'];?>
" /> <span class='f_red' id="dorders"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>参数设置：<span class='f_red'></span></td>
		<td class='hback'><?php echo $_smarty_tpl->getVariable('flag_checkbox')->value;?>
</td>
	  </tr>
	  <tr>
		<td class='hback_1'>CSS样式：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="cssname" id="cssname" class="input-s" value="<?php echo $_smarty_tpl->getVariable('cate')->value['cssname'];?>
" /> <span class='f_red' id="dcssname"></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>样式图标 <span class="f_red"></span> </td>
		<td class='hback'>
		  <input type="hidden" name="img" id="img" value="<?php echo $_smarty_tpl->getVariable('cate')->value['img'];?>
" /><span id='dimg' class='f_red'></span>
		    <?php if ($_smarty_tpl->getVariable('cate')->value['img']!=''){?>
			<iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?action=show&comeform=myform&inputid=img&thumbflag=0&picname=<?php echo $_smarty_tpl->getVariable('cate')->value['imgname'];?>
&picurl=<?php echo $_smarty_tpl->getVariable('cate')->value['img'];?>
'></iframe>
			<?php }else{ ?>
			<iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?comeform=myform&inputid=img&thumbflag=0'></iframe>
			<?php }?>
		</td>
	  </tr>
	  <tr>
		<td class='hback_1'>打开方式：<span class='f_red'></span></td>
		<td class='hback'><select name="target" id="target"><option value="1"<?php if ($_smarty_tpl->getVariable('cate')->value['target']==1){?> selected<?php }?>>本页面</option><option value="2"<?php if ($_smarty_tpl->getVariable('cate')->value['target']==2){?> selected<?php }?>>新页面</option></select></td>
	  </tr>
	  <tr>
		<td class='hback_1'>链接类型：<span class='f_red'></span></td>
		<td class='hback'><input type="radio" name="linktype" value="1"<?php if ($_smarty_tpl->getVariable('cate')->value['linktype']==1){?> checked<?php }?> />内部链接，<input type="radio" name="linktype" value="2"<?php if ($_smarty_tpl->getVariable('cate')->value['linktype']==2){?> checked<?php }?> />外部链接</td>
	  </tr>
	  <tr>
		<td class='hback_1'>外部URL：<span class='f_red'></span></td>
		<td class='hback'><input type="text" name="linkurl" id="linkurl" class="input-txt" value="<?php echo $_smarty_tpl->getVariable('cate')->value['linkurl'];?>
" /> <span class='f_red' id="dlinkurl"></span></td>
	  </tr>

	  <tr>
		<td class='hback_1'>分类简介： </td>
		<td class='hback'><textarea name="intro" id="intro" style='width:60%;height:65px;overflow:auto;color:#444444;'><?php echo $_smarty_tpl->getVariable('cate')->value['intro'];?>
</textarea><br />（500字符以内）</td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="更新保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<?php }?>
</td></tr></table>
</body>
</html>
<script type="text/javascript">
function checkform() {
	var t = "";
	var v = "";

	t = "catename";
	v = $("#"+t).val();
	if(v=="") {
		dmsg("分类名称不能为空", t);
		return false;
	}

	return true;
}
</script>
