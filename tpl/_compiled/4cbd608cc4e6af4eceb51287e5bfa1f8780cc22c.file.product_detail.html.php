<?php /* Smarty version Smarty-3.0.5, created on 2015-11-27 11:07:51
         compiled from "D:\WWW\phpcms21\tpl/xiaomi/product_detail.html" */ ?>
<?php /*%%SmartyHeaderCode:278625657c90753ffb3-72485870%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4cbd608cc4e6af4eceb51287e5bfa1f8780cc22c' => 
    array (
      0 => 'D:\\WWW\\phpcms21\\tpl/xiaomi/product_detail.html',
      1 => 1448593667,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '278625657c90753ffb3-72485870',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_date_format')) include 'D:\WWW\phpcms21\source\core\plugins\modifier.date_format.php';
if (!is_callable('smarty_modifier_htmlencode')) include 'D:\WWW\phpcms21\source\core\plugins\modifier.htmlencode.php';
?><?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('tplpath')->value)."site_header.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>

<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
css/goods-detail.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
jqZoom/css/jqzoom.css" />
<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
jqZoom/js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
jqZoom/js/jqzoom.pack.1.0.1.js"></script>
<script type="text/javascript">
    		
    		$(function() {
			var options =
				{
					zoomWidth: 480, //放大镜的宽度
					zoomHeight: 480,//放大镜的高度
					zoomType:'standard'
				};
				$(".jqzoom").jqzoom(options);
			});
			
			</script>
		
<!-- S 面包屑 -->

<div class="breadcrumbs">
    <div class="breadcrumbs">
        <div class="container breadcrumbs">
            <a href='../../<?php echo $_smarty_tpl->getVariable('url_index')->value;?>
'>首页</a><span class="sep">/</span><?php echo $_smarty_tpl->getVariable('navigation')->value;?>

        </div>
     </div>
</div>
<!-- E 面包屑 -->
<div class="goods-detail">
    <div class="goods-detail-info  clearfix J_goodsDetail">
        <div class="container">
            <div class="row">
                <div class="span13  J_mi_goodsPic_block goods-detail-left-info">
                    <div class="goods-pic-box  " id="J_mi_goodsPicBox">
                        <div class="goods-big-pic J_bigPicBlock" >
                        	<a href="<?php echo $_smarty_tpl->getVariable('product')->value['img1'];?>
" class="jqzoom" style="" title="图片标题"> <img src="<?php echo $_smarty_tpl->getVariable('product')->value['mid1'];?>
" class="J_goodsBigPic" id="J_BigPic"  title="图片标题" style="border: 1px solid #666;"> </a> 
                            
                        </div>
                         <div class="goods-pic-loading">
                           <div class="loader loader-gray"></div>
                       </div>
                        <div class="goods-small-pic clearfix">
                           <ul id="goodsPicList" >
                               
                               <li class="">
                                   <img src="<?php echo $_smarty_tpl->getVariable('product')->value['small1'];?>
" />
                               </li>
                           </ul>
                       </div> 
                    </div>
                    <div class="span11 goods-batch-img-list-block J_goodsBatchImg"></div>
                </div>
                <div class="span7 goods-info-rightbox">
                    <div class="goods-info-leftborder"></div>
                    	<dl class="goods-info-box ">
                    		<dt class="goods-info-head">
                        <dl id="J_goodsInfoBlock">
                    	<dt id="goodsName" class="goods-name"><?php echo $_smarty_tpl->getVariable('product')->value['productname'];?>
</dt>
                            <?php echo $_smarty_tpl->getVariable('product')->value['intro'];?>

                        	<dd class="goods-info-head-price clearfix"> 
                        		<b class="J_mi_goodsPrice"><?php echo $_smarty_tpl->getVariable('product')->value['price'];?>
</b> 
                        		<i>元</i>
                                <del> <span class="J_mi_marketPrice"><?php echo $_smarty_tpl->getVariable('product')->value['nprice'];?>
</span> </del>
                        	</dd>
                        	<dd class="goods-info-head-price clearfix" style="color: gray;"> 
									<span style="display:inline-block;margin-right: 10px; line-height: 30px;font-size: 25px;">数量</span>
									<i class="decrease" style="font-size: 30px;">-</i>
									<input type="" name="" id="" class="inputNum" value="1" style="width: 20px;height: 20px;font-size: 10px;text-align: center;"/>
									<i class="increase" style="font-size: 30px;">+</i>
                                <span class="stock" style="font-size: 19px;margin-left: 15px;color: gray;" stock="<?php echo $_smarty_tpl->getVariable('product')->value['productnum'];?>
">库存:<?php echo $_smarty_tpl->getVariable('product')->value['productnum'];?>
件</span>
                        	</dd>
                        	<dd class="goods-info-head-cart" id="goodsDetailBtnBox">
                            	<a href="javascript:;" gid="<?php echo $_smarty_tpl->getVariable('product')->value['productid'];?>
" id="goodsDetailAddCartBtn" class="btn  btn-primary goods-add-cart-btn" > 
                            		<i class="iconfont"></i>立刻购买
                            	</a> 
                            	<script type="text/javascript">
                            		$(function(){
                            			$('#goodsDetailAddCartBtn').click(function(){
                            				var gid = $(this).attr('gid');
                            				var gnum = $('.inputNum').attr('value');
                            				$.ajax({
                            					type:"post",
                            					url:"productajax.php",
                            					data:{"gid":gid,"gnum":gnum},
												dataType:'json',
												success:function(phpDath){
													location.href= 'productbuy.php';
												}
                            				});
                            			});
                            			//点击减少
										$('.decrease').click(function(){
											var inputNum = parseInt($('.inputNum').val());
											if(inputNum <= 0){
												alert('请正确填写商品数');
												return fasle;
											}
											inputNum = inputNum-1;
											$('.inputNum').val(inputNum);
										});
										//点击增加
										$('.increase').click(function(){
											var inputNum = parseInt($('.inputNum').val());
											var sum = parseInt($('.stock').attr('stock'));
											if(inputNum>=sum){
												alert('库存仅'+sum);
												return fasle;
											}
											inputNum = inputNum+1;
											$('.inputNum').val(inputNum);
										});
                            		});
                            	</script>
                            	<a id="goodsDetailCollectBtn" data-isfavorite="false" class=" btn btn-gray  goods-collect-btn " >
                            		<i class="iconfont default"></i>
                            		<i class="iconfont red"></i>
                            		<i class="iconfont red J_redCopy"></i> 
                            	</a>
                            </dd>
                            <dd class="goods-info-head-userfaq">
                            	<ul>
                            		<li class="J_scrollHref" data-href="#goodsComment" data-index="2">
                            			类型：
                            			<b><a href="<?php echo $_smarty_tpl->getVariable('product')->value['caturl'];?>
"><?php echo $_smarty_tpl->getVariable('product')->value['catename'];?>
</a></b>
                            		</li>
                            		<li class="J_scrollHref mid" data-href="#goodsFaq" data-index="3">
                            			浏览次数：
                            			<b><?php echo $_smarty_tpl->getVariable('product')->value['hits'];?>
</b>
                            		</li>
                            		<li> 上线时间：
                            			<b><?php echo smarty_modifier_date_format($_smarty_tpl->getVariable('product')->value['timeline'],'%Y-%m-%d');?>
</b>
                            		</li>
                            	</ul>
                            </dd>
                        </dl>
                    </dt>
                    <dd class="goods-info-foot">
                        <a href="<?php echo $_smarty_tpl->getVariable('url_product')->value;?>
" >
                        <span class="iconfont"></span>
                        查看更多</a>
                    </dd>
                </dl>
                </div>
            </div>

        </div>
    </div>
  
    <div class="goods-detail-desc J_itemBox" id="goodsDesc">
        <div class="container">
            <div class="shape-container" >
                <p class='detailBlock'>
                    
                </p>
            </div>
        </div>
    </div>
    <div class="goods-detail-nav-name-block J_itemBox" id="goodsParam">
        <div class="container main-block">
            <div class="border-line"></div>
            <h2 class="nav-name">详细介绍</h2>
        </div>
    </div>  
    <!--详细介绍-->
    <div class="goods-detail-param  J_itemBox">
        <div class="container">
            <ul class='param-list'>
                <li class='goods-img'>
                    <img src="<?php echo $_smarty_tpl->getVariable('product')->value['thumbfiles'];?>
" alt="">
                </li>
                <li class="goods-tech-spec">
                 <?php echo smarty_modifier_htmlencode($_smarty_tpl->getVariable('product')->value['content']);?>

                </li>
            </ul>
        </div>
    </div>

  
    <!--评价-->

    <div class="goods-detail-nav-name-block J_itemBox" id="goodsFaq">
        <div class="container main-block">
            <div class="border-line"></div>
            <h2 class="nav-name">商品提问</h2>
        </div>
    </div>  

    <!--商品提问-->
    <div class="goods-detail-question-block J_itemBox" id="goodsFaqContent">
        <div class="container">
            <div class="question-input">
                <input type="text" placeholder="输入你的提问" class='input-block J_inputQuestion' data-can-search='true' data-pageSize ='6'/>
                <div class="btn btn-primary question-btn J_btnQuestion" >提问</div>
            </div>   
        </div>
    </div>
    <!--商品提问-->


   
</div>
<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('tplpath')->value)."site_footer.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>







  
 
</body>
</html>
