<?php /* Smarty version Smarty-3.0.5, created on 2015-12-01 17:08:05
         compiled from "D:\WWW\phpcms21\tpl/xiaomi/order-list.html" */ ?>
<?php /*%%SmartyHeaderCode:4035565d637582da01-10771887%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3c6c980545cd18a85a0e8165652a574cc1967377' => 
    array (
      0 => 'D:\\WWW\\phpcms21\\tpl/xiaomi/order-list.html',
      1 => 1448960883,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4035565d637582da01-10771887',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html>
<html xml:lang="zh-CN" lang="zh-CN">

	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta charset="UTF-8">
		<title>填写订单信息</title>
		<meta name="viewport" content="width=1226">
		
		
		<link href="http://libs.baidu.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet">
		<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
		<script src="http://libs.baidu.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
js/order-list.js"></script>

		<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
css/checkout.css">
		<script type="text/javascript">
			
		</script>
	</head>

	<body>
		<div class="site-header site-mini-header">
			<div class="container">
				<div class="header-logo">
					<a class="logo " href="" title="小米官网"></a>
				</div>
				<div class="header-title" id="J_miniHeaderTitle">
					<h2>确认订单</h2></div>
				<div class="topbar-info" id="J_userInfo"><span class="user"><a  class="link link-order"  target="_blank">我的订单</a></div>
    </div>
</div>
<!-- .site-mini-header END -->
<div class="page-main">
    <div class="container">
        <div class="checkout-box">
            <div class="section section-address">
                <div class="section-header clearfix">
                    <h3 class="title">收货地址</h3>
                <div class="more">
            </div>
        </div>
                <div class="section-body clearfix" id="J_addressList">
                    <!-- addresslist begin -->
                    <!-- addresslist end -->
                    <div class="address-item address-item-new" id="J_newAddress">
                        <i class="iconfont" id="add-address"></i>
                        添加新地址
                    </div>
                    
                    <!--添加收获地址-->
                    <!-- Button trigger modal -->
					<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
					  Launch demo modal
					</button>
					
					<!-- Modal -->
					<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
					      </div>
					      <div class="modal-body">
					      	<form action="">
					       		<input type="text" name="" id="" value="" />
					       		<input type="submit" name="" id="" value="提交" />
					       	</form>
					      </div>
					      <div class="modal-footer">
					        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					        <button type="button" class="btn btn-primary">Save changes</button>
					      </div>
					    </div>
					  </div>
					</div>
                    <!--添加收货地址结束-->
                    
                </div>
            </div>

            <div class="section section-options section-payment clearfix">
                <div class="section-header">
                    <h3 class="title">支付方式</h3>
                </div>
                <div class="section-body clearfix">
                    <ul class="J_optionList options ">
                        <li data-type="pay" class="J_option selected" data-value="1">
	                                                在线支付   
	                        <span>（支持支付宝、银联、财付通、小米钱包等）</span>
						</li>
					</ul>
				</div>
			</div>

			<div class="section section-options section-shipment clearfix">
				<div class="section-header">
					<h3 class="title">配送方式</h3>
				</div>
				<div class="section-body clearfix">
					<ul class="clearfix J_optionList options ">
						<li data-type="shipment" class="J_option selected">
							快递配送（免运费） </li>
					</ul>

					<div style="display: none;" class="service-self-tip" id="J_serviceSelfTip"></div>
				</div>
			</div>

			<div class="section section-options section-time clearfix">
				<div class="section-header">
					<h3 class="title">配送时间</h3>
				</div>
				<div class="section-body clearfix">
					<ul class="J_optionList options options-list clearfix">
						<!-- besttime start -->
						<li  class="J_option selected" >
							不限送货时间：<span>周一至周日</span>
						</li>
						<li  class="J_option ">
							工作日送货：<span>周一至周五</span>
						</li>
						<li  class="J_option ">
							双休日、假日送货：<span>周六至周日</span>
						</li>
						<!-- besttime end -->
					</ul>
				</div>
			</div>

			<div class="section section-options section-invoice clearfix">
				<div class="section-header">
					<h3 class="title">发票</h3>
				</div>
				<div class="section-body clearfix">
					<ul class="J_optionList options options-list  J_tabSwitch clearfix">
						<li class="hide J_option">
							不开发票
						</li>
						<li data-invoice-type="electron" class="J_option tab-active selected">
							电子发票（非纸质）
						</li>
						<li class="J_option" id="J_paperInvoice">
							普通发票（纸质）
						</li>
					</ul>

					<div class="tab-container">
						<div style="display: none;" class="tab-content hide"></div>
						<div class="tab-content e-invoice-detail">
							电子发票目前仅对个人用户开具，不可用于单位报销 ，不随商品寄送。<a  id="J_showEinvoiceDetail">什么是电子发票 <i class="icon-qa">?</i></a>
							<div class="e-invoice-qa hide" id="J_einvoiceDetail">
								<ul>
									<li>感谢您选择电子发票，小米开具的电子发票均为真实有效的合法发票，与传统纸质发票具有同等法律效力，可作为用户维权、保修的有效凭据。</li>
									<li>电子开票不可用于单位报销，开具后不可更换纸质发票。</li>
									<li>您在订单详情的"发票信息"栏可查看、下载您的电子发票。</li>
								</ul>
								<a>了解详情&gt;</a>
							</div>
						</div>

						<div style="display: none;" class="tab-content hide paper-invoice-detail">
							<ul class="J_optionList options options-list J_tabSwitch clearfix">
								<li data-value="1" data-type="invoice" data-invoice-type="personal" class="J_option">
									个人
								</li>
								<li data-value="2" data-type="invoice" data-invoice-type="company" class="J_option">
									单位
								</li>
							</ul>
							<div class="tab-container">
								<div class="tab-content paper-invoice-person">
									发票抬头：个人
									<br> 发票内容：购买商品明细
								</div>
								<div class="tab-content hide paper-invoice-company">
									<div class="form-section">
										<label class="input-label" for="invoice_title">请输入发票抬头</label>
										<input class="input-text" id="invoice_title" name="invoice_title" type="text">
									</div>
									<p>发票内容：购买商品明细</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="section section-goods">
				<div class="section-header clearfix">
					<h3 class="title">商品及优惠券</h3>
					<div class="more">
						<a>返回购物车<i class="iconfont"></i></a>
					</div>
				</div>
				<div class="section-body">
					<ul class="goods-list" id="J_goodsList">
						<li class="clearfix">
							<div class="col col-img">
								<img height="30" width="30">
							</div>
							<div class="col col-name">
								<a>小米蓝牙耳机 白色</a>
							</div>
							<div class="col col-price">79元 x 2 </div>
							<div class="col col-status">有货</div>
							<div class="col col-total">158元</div>
						</li>
						<li class="clearfix">
							<div class="col col-img">
								<img src="" height="30" width="30">
							</div>
							<div class="col col-name">
								<a href="http://item.mi.com/1141300052.html" target="_blank">简约收纳袋大号 深灰<em class="label">加价购</em></a>
							</div>
							<div class="col col-price">4.9元 x 1</div>
							<div class="col col-status">有货</div>
							<div class="col col-total">4.9元</div>
						</li>
						<li class="clearfix">
							<div class="col col-img">
								<img src="" height="30" width="30">
							</div>
							<div class="col col-name">
								<a target="_blank">ZMI随身路由器（全网通版） 白色</a>
							</div>
							<div class="col col-price">349元 x 1 </div>
							<div class="col col-status">有货</div>
							<div class="col col-total">349元</div>
						</li>
						<li class="clearfix">
							<div class="col col-img">
								<img src="" height="30" width="30">
							</div>
							<div class="col col-name">
								<a onclick="" target="_blank">SanDisk 8GB高速存储卡（TF Class10） 黑色  </a>
							</div>
							<div class="col col-price">26.9元 x 1 </div>
							<div class="col col-status">有货</div>
							<div class="col col-total">26.9元</div>
						</li>
					</ul>
				</div>
			</div>

			<div class="section section-count clearfix">
				<div class="count-actions">
					<div class="coupon-trigger" id="J_useCoupon"><i class="iconfont icon-plus"></i>使用优惠券</div>
					<div class="coupon-result hide" id="J_couponResult">
						<i class="iconfont icon-selected"></i> 正在使用：
						<span class="coupon-title" id="J_couponTitle"></span>
						<a href="javascript:void(0)" id="J_changeConpon">［修改］</a>
					</div>
					<div class="coupon-box hide" id="J_couponBox">
						<ul class="clearfix tab-switch J_tabSwitch">
							<li>选择优惠券</li>
							<li>输入优惠券码</li>
						</ul>
						<div class="tab-container">
							<div class="tab-content list-content">
								<p class="coupon-empty">您暂时没有可用的优惠券</p>
							</div>
							<div class="tab-content code-content hide">
								<div class="form-section">
									<label class="input-label" for="coupon_code">请输入优惠券码</label>
									<input class="input-text" id="coupon_code" name="coupon_code" type="text">
								</div>
								<div class="coupon-confirm">
									<a onclick="" class="btn btn-gray J_couponCancel">不使用优惠券</a>
									<a onclick="" class="btn btn-primary" id="J_useCouponCode">立即使用</a>
								</div>
							</div>
						</div>
					</div>

					<div class="raise-buy-box">
						<ul id="J_raiseBuyList">
							<li class="item  J_raiseBuyItem">
								<div class="inner">
									<i class="iconfont icon-plus"></i>+1元得小米礼品袋 </div>

								<div class="add-cart-result">
									<a onclick="" class="J_del del"><i class="iconfont">×</i></a>
									<i class="iconfont icon-selected"></i>已选购：<span class="J_goodsName">+1元得小米礼品袋</span>
								</div>
							</li>

							<!--S 保障计划 产品选择弹框 -->
							<!--E 保障计划 产品选择弹框 -->
						</ul>
					</div>
				</div>

				<div class="money-box" id="J_moneyBox">
					<ul>
						<li class="clearfix">
							<label>商品件数：</label>
							<span class="val">5件</span>
						</li>
						<li class="clearfix">
							<label>金额合计：</label>
							<span class="val">538.8元</span>
						</li>
						<li class="clearfix">
							<label>活动优惠：</label>
							<span class="val">-0元</span>
						</li>
						<li class="clearfix">
							<label>优惠券抵扣：</label>
							<span class="val"><i id="J_couponVal">-0</i>元</span>
						</li>
						<li class="clearfix">
							<label>运费：</label>
							<span class="val"><i id="J_postageVal">0</i>元</span>
						</li>
						<li class="clearfix total-price">
							<label>应付总额：</label>
							<span class="val"><em id="J_totalPrice">538.80</em>元</span>
						</li>
					</ul>
				</div>
			</div>

			<div class="section-bar clearfix">
				<div class="fl">
					<div class="seleced-address hide" id="J_confirmAddress">
					</div>
					<div class="big-pro-tip hide J_confirmBigProTip"></div>
				</div>
				<div class="fr">
					<a onclick="" class="btn btn-primary" id="J_checkoutToPay">去结算</a>
				</div>
			</div>
		</div>
		</div>
		</div>
		<!-- 禮品卡提示 S-->
		<div class="modal fade modal-hide modal-lipin" id="J_lipinTip">
			<div class="modal-header">
				<h3 class="title">温馨提示</h3>
			</div>
			<div class="modal-body">
				<p>
					为保障您的利益与安全，下单后礼品卡将会被使用，
					<br> 且订单信息将不可修改。请确认收货信息：
				</p>
				<ul>
					<li class="clearfix">
						<strong>收&nbsp;&nbsp;货&nbsp;&nbsp;人：</strong>
						<span id="J_lipinUserName"></span>
					</li>
					<li class="clearfix">
						<strong>联系电话：</strong>
						<span id="J_lipinUserPhone"></span>
					</li>
					<li class="clearfix">
						<strong>收货地址：</strong>
						<span id="J_lipinUserAddress"></span>
					</li>
				</ul>
			</div>
			<div class="modal-footer">
				<a onclick="" class="btn btn-primary" id="J_lipinSubmit">确认下单</a>
				<a onclick="" class="btn btn-gray" data-dismiss="modal">返回修改</a>
			</div>
		</div>
		<!--  禮品卡提示 E-->

		<!-- 预售提示 S-->
		<div class="modal fade modal-hide modal-yushou" id="J_yushouTip">
			<div class="modal-header">
				<h3 class="title">请确认收货地址及发货时间</h3>
			</div>
			<div class="modal-body">
				<ul class="content">
					<li>
						<h3>请确认配送地址，提交后不可变更：</h3>
						<p id="J_yushouAddress"> </p>
						<span class="icon icon-1"></span>
					</li>
					<li>
						<h3>支付后发货</h3>
						<p>如您随预售商品一起购买的商品，将与预售商品一起发货</p>
						<span class="icon icon-2"></span>
					</li>
					<li>
						<h3>以支付价格为准</h3>
						<p>如预售产品发生调价，已支付订单价格将不受影响。</p>
						<span class="icon icon-3"></span>
					</li>
				</ul>
			</div>
			<div class="modal-footer">
				<a  class="btn btn-gray" data-dismiss="modal">返回修改地址</a>
				<a  class="btn btn-primary" id="J_yushouSubmit">确认并继续下单</a>
			</div>
		</div>
		<h2>创建模态框（Modal）</h2>
<button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
开始演示模态框
</button>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
×
</button>
<h4 class="modal-title" id="myModalLabel">
模态框（Modal）标题
</h4>
</div>
<div class="modal-body">
在这里添加一些文本
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">关闭
</button>
<button type="button" class="btn btn-primary">
提交更改
</button>
</div>
</div>
</div>
</div>
	</body>

</html>