<?php /* Smarty version Smarty-3.0.5, created on 2015-11-22 20:10:31
         compiled from "D:\WWW\phpcms8\tpl/xiaomi/product_detail.html" */ ?>
<?php /*%%SmartyHeaderCode:38775651b0b7dd90e7-86469874%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2edf806dbb6f59732920abba6b4435699b951f2c' => 
    array (
      0 => 'D:\\WWW\\phpcms8\\tpl/xiaomi/product_detail.html',
      1 => 1446100224,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '38775651b0b7dd90e7-86469874',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_date_format')) include 'D:\WWW\phpcms8\source\core\plugins\modifier.date_format.php';
if (!is_callable('smarty_modifier_htmlencode')) include 'D:\WWW\phpcms8\source\core\plugins\modifier.htmlencode.php';
?><?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('tplpath')->value)."site_header.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
css/goods-detail.min.css" />

<!-- S 面包屑 -->

<div class="breadcrumbs">
    <div class="breadcrumbs">
        <div class="container breadcrumbs">
            <a href='../../<?php echo $_smarty_tpl->getVariable('url_index')->value;?>
'>首页</a><span class="sep">/</span><?php echo $_smarty_tpl->getVariable('navigation')->value;?>

        </div>
     </div>
</div>
<!-- E 面包屑 -->
<div class="goods-detail">
    <div class="goods-detail-info  clearfix J_goodsDetail">
        <div class="container">
            <div class="row">
                <div class="span13  J_mi_goodsPic_block goods-detail-left-info">
                    <div class="goods-pic-box  " id="J_mi_goodsPicBox">
                        <div class="goods-big-pic J_bigPicBlock" >
                            <img src="<?php echo $_smarty_tpl->getVariable('product')->value['thumbfiles'];?>
"  class="J_goodsBigPic" id="J_BigPic" >
                        </div>
                       <!--  <div class="goods-pic-loading">
                           <div class="loader loader-gray"></div>
                       </div>
                        <div class="goods-small-pic clearfix">
                           <ul id="goodsPicList" >
                               <li class="current">
                                   <img src="{$case.thumbfiles}" data-src="{$case.thumbfiles}" 
                               data-src-b="http://i1.mifile.cn/a1/T1n2DvBKhT1RXrhCrK!800x800.jpg" >
                               </li>
                               <li class="">
                                   <img src="http://i1.mifile.cn/a1/T1mYY_B4JT1RXrhCrK!60x60.jpg" data-src="http://i1.mifile.cn/a1/T1mYY_B4JT1RXrhCrK!482x482.jpg" 
                               data-src-b="http://i1.mifile.cn/a1/T1mYY_B4JT1RXrhCrK!800x800.jpg" >
                               </li>
                               <li class="">
                                   <img src="http://i1.mifile.cn/a1/T1l5VvB5KT1RXrhCrK!60x60.jpg" data-src="http://i1.mifile.cn/a1/T1l5VvB5KT1RXrhCrK!482x482.jpg" 
                               data-src-b="http://i1.mifile.cn/a1/T1l5VvB5KT1RXrhCrK!800x800.jpg" >
                               </li>
                           </ul>
                       </div> -->
                    </div>
                    <div class="span11 goods-batch-img-list-block J_goodsBatchImg"></div>
                </div>
                <div class="span7 goods-info-rightbox">
                    <div class="goods-info-leftborder"></div>
                    	<dl class="goods-info-box ">
                    		<dt class="goods-info-head">
                        <dl id="J_goodsInfoBlock">
                    	<dt id="goodsName" class="goods-name"><?php echo $_smarty_tpl->getVariable('product')->value['productname'];?>
</dt>
                            <?php echo $_smarty_tpl->getVariable('product')->value['intro'];?>

                        	<dd class="goods-info-head-price clearfix"> 
                        		<b class="J_mi_goodsPrice"><?php echo $_smarty_tpl->getVariable('product')->value['price'];?>
</b> 
                        		<i>元</i>
                                <del> <span class="J_mi_marketPrice"><?php echo $_smarty_tpl->getVariable('product')->value['nprice'];?>
</span> </del>
                        	</dd>
                        	<dd class="goods-info-head-cart" id="goodsDetailBtnBox">
                            	<a href="productbuy.php?proid=<?php echo $_smarty_tpl->getVariable('product')->value['productid'];?>
" id="goodsDetailAddCartBtn" class="btn  btn-primary goods-add-cart-btn" data-disabled="false" data-gid="2144900034" data-package="0" data-stat-id="d5a9c5d9b356567b" onclick="_msq.push(['trackEvent', '206b2b22bf3a8f88-d5a9c5d9b356567b', 'http://cart.mi.com/cart/add/2144900034', 'pcpid']);"> 
                            		<i class="iconfont"></i>立刻预定 
                            	</a> 
                            	<a id="goodsDetailCollectBtn" data-isfavorite="false" class=" btn btn-gray  goods-collect-btn " data-stat-id="9d1c11913f946c7f" onclick="_msq.push(['trackEvent', '206b2b22bf3a8f88-9d1c11913f946c7f', '', 'pcpid']);">
                            		<i class="iconfont default"></i>
                            		<i class="iconfont red"></i>
                            		<i class="iconfont red J_redCopy"></i> 
                            	</a>
                            </dd>
                            <dd class="goods-info-head-userfaq">
                            	<ul>
                            		<li class="J_scrollHref" data-href="#goodsComment" data-index="2">
                            			类型：
                            			<b><a href="<?php echo $_smarty_tpl->getVariable('product')->value['caturl'];?>
"><?php echo $_smarty_tpl->getVariable('product')->value['catename'];?>
</a></b>
                            		</li>
                            		<li class="J_scrollHref mid" data-href="#goodsFaq" data-index="3">
                            			浏览次数：
                            			<b><?php echo $_smarty_tpl->getVariable('product')->value['hits'];?>
</b>
                            		</li>
                            		<li> 上线时间：
                            			<b><?php echo smarty_modifier_date_format($_smarty_tpl->getVariable('product')->value['timeline'],'%Y-%m-%d');?>
</b>
                            		</li>
                            	</ul>
                            </dd>
                        </dl>
                    </dt>
                    <dd class="goods-info-foot">
                        <a href="<?php echo $_smarty_tpl->getVariable('url_product')->value;?>
" data-stat-id="aa40f772b35f7c57" onclick="_msq.push(['trackEvent', '206b2b22bf3a8f88-aa40f772b35f7c57', '<?php echo $_smarty_tpl->getVariable('url_product')->value;?>
', 'pcpid']);">
                        <span class="iconfont"></span>
                        查看更多</a>
                    </dd>
                </dl>
                </div>
            </div>

        </div>
    </div>
  
    <div class="goods-detail-desc J_itemBox" id="goodsDesc">
        <div class="container">
            <div class="shape-container" >
                <p class='detailBlock'>
                    
                </p>
            </div>
        </div>
    </div>
    <div class="goods-detail-nav-name-block J_itemBox" id="goodsParam">
        <div class="container main-block">
            <div class="border-line"></div>
            <h2 class="nav-name">详细介绍</h2>
        </div>
    </div>  
    <!--详细介绍-->
    <div class="goods-detail-param  J_itemBox">
        <div class="container">
            <ul class='param-list'>
                <li class='goods-img'>
                    <img src="<?php echo $_smarty_tpl->getVariable('product')->value['thumbfiles'];?>
" alt="">
                </li>
                <li class="goods-tech-spec">
                 <?php echo smarty_modifier_htmlencode($_smarty_tpl->getVariable('product')->value['content']);?>

                </li>
            </ul>
        </div>
    </div>

  
    <!--评价-->

    <div class="goods-detail-nav-name-block J_itemBox" id="goodsFaq">
        <div class="container main-block">
            <div class="border-line"></div>
            <h2 class="nav-name">商品提问</h2>
        </div>
    </div>  

    <!--商品提问-->
    <div class="goods-detail-question-block J_itemBox" id="goodsFaqContent">
        <div class="container">
            <div class="question-input">
                <input type="text" placeholder="输入你的提问" class='input-block J_inputQuestion' data-can-search='true' data-pageSize ='6'/>
                <div class="btn btn-primary question-btn J_btnQuestion" >提问</div>
            </div>

            <ul class="question-content" id="J_goodsQuestionBlock">
            </ul>
            <div class="question-null-content J_nullInfo">抱歉，没有找到答案，您可以点击“提问”提交此条提问给已经购买者、企行官方客服和产品经理，我们会及时回复。</div>
            <div class="loader-block">
                <div class='loader'></div>
            </div>
            <div class="goods-detail-null-content" id='J_questionTipInfo'>
                <div class="container">
                    <h3>暂时还没有提问</h3>
                    <p>对商品有不太了解，问问看吧</p>
                </div>
            </div> 
        </div>
    </div>
    <!--商品提问-->


    <div id="J_recentGoods"   style='margin-top:15px;padding-bottom:30px;'></div>
</div>
<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('tplpath')->value)."site_footer.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>

<!--单品图片，放大的浮层-->
<script id='carousel-tmpl' type='text/template'>
    <div class="mi-carousel ">
        <ul class="item-list J_itemCarousel">
            {{~it: val: index}}
            <li>
                <img data-src="{{=val}}" alt="" {{?index!==0}}data-ignoreHei='true'{{?}} >
            </li>
            {{~}}
        </ul>
    </div>
</script>
<script id='itemBigImg-tmpl' type='text/template'>
<div class='goods-pic-zoom-block zoomImgInit-a zoomImgInit-b ' id='goodsPicZoomBlock'>
    <div class="zoom-big-block" id = 'J_zoomBigBlock'>
        <div class='loader loader-gray'></div>
        <img src="{{=it.imgArr[it.curIndex].big}}" alt="">
        <div class="img" style='background-image:url({{=it.imgArr[it.curIndex].big}});'></div>
    </div>
    <div class="zoom-big-nav">
        <div class="nav-block left-nav J_itemZoomLeft {{?it.curIndex==0}}dective{{?}}">
            <i class='iconfont'>&#xe627;</i>
        </div>
        <div class="nav-block right-nav J_itemZoomRight {{?it.curIndex==(it.imgArr.length-1)}}dective{{?}}">
            <i class='iconfont'>&#xe622;</i>
        </div>
    </div>
    <ul class='zoom-sml-list J_itemZoomSmlList'>
        {{~it.imgArr: img: index}}
        <li {{?index ==it.curIndex}}class='current'{{?}}>
            <img src="{{=img.sml}}" alt="">
        </li>
        {{~}}
    </ul>
    <div class="exit-btn J_exitItemBig" href="javascript:void(0);">
        <i class='iconfont'>&#xe602;</i>
    </div>
</div>
</script>
<script id='bigImg-tmpl' type='text/template'>
    <img src="{{=it.img}}"  class="J_goodsBigPic" id="J_BigPic" >
</script>
<script id='batch-tmpl' type='text/template'>
<h3 class="batch-title">{{=it.short_name}} <i>{{=it.price_min}}元</i></h3>    
<ul class="batch-goods-list goods-shape-{{=it.batch_info.length}}">
    {{~it.batch_info: batch:index}} 
    <li>
        <div class="batch-goods-img">
            <img src="{{=batch.image}}" alt="">
        </div>
        <div class="batch-goods-name">{{=batch.name}}</div>
    </li>
    {{~}}
</ul>
</script>
<script type='text/template' id='service-tmpl'>
    <h3 class="title">{{=it.cat_name}}</h3>
    {{~it.items: item: index}}
    <h4 class="sub-title">
        {{?item.son_cat}}
        <div class="tit">A</div>{{=item.son_cat}}: 
        {{?}}
    </h4>
    <p class='content'>
        {{=item.item_html}}
    </p>
    {{~}}
</script>
<script id="goodsInfo-tmpl" type="text/template">
    <dt id="goodsName" class="goods-name">
        32GB高速存储卡（TF Class10）    </dt>
     
    {{?it.adaptDesc}}
    <dd class="goods-phone-type">
       <p> {{=it.adaptDesc}}</p>
    </dd>
    {{?}}
    {{?it.canJoinActs.length!= 0}}
    <dd class="goods-info-head-tip">
        <ul>
            {{~it.canJoinActs:act:index}}
            <li class='{{=act.type}}'>
                <i>{{=act.label}}</i>{{=act.title}} 
            </li>
            {{~}}
        </ul>
    </dd>
    {{?}}
    <dd class="goods-info-head-price clearfix">
                <b  class="J_mi_goodsPrice">
            79        </b>
        <i>&nbsp;元</i>
        <del>
            <span class="J_mi_marketPrice">
            99元
            </span>
        </del>
         
    </dd>
    
    {{? it.goodsColor.curName}}
    <dd class="goods-info-head-colors clearfix">
        <span class='style-name'>{{=it.goodsColor.colorName}}：{{=it.goodsColor.curName}}</span>
        <div class="clearfix">
            {{~it.goodsColor.colorArr:color:index}}
            {{? color.is_sale}}
            <div class="colorli">
                <a href="{{=color.goodsUrl}}" class="{{=color.selectedClass}}" title="{{=color.imgTitle}}">
                    <img class="square" src="{{=color.goodsBack}}"/>
                </a>
            </div>
            {{?}}
            {{~}}
        </div>
    </dd>
    {{?}}
    {{? it.goodsSize.curSize}}
    <dd class="goods-info-head-size clearfix">
        <span class="label">{{=it.goodsSize.sizeName}}：</span>
        <ul class="clearfix" id='J_goodsSize'>
            {{~it.goodsSize.sizeArr:sizeObj:index}}
            {{? sizeObj.is_sale}}
            <li class=''>
                {{? !sizeObj.is_cos}}
                <a href="javascript:void(0);" class="item goodsStyle " 
    data-id="{{=sizeObj.goods_id}}" data-price="{{=sizeObj.price}}"  data-is-cos="{{=sizeObj.is_cos}}" >
                    {{=sizeObj.size}}
                </a>
                {{??}}
                <button class="btn item goodsStyle" disabled>{{=sizeObj.size}}</button>
                {{?}}
            </li>
            {{?}}
            {{~}}
        </ul>
    </dd>
    {{?}}
    {{
        var rIndex = 0;
        for( var j in it.recommend_package) {
            rIndex++;
        }
    }} 
    {{?rIndex > 0}}
    <dd class="goods-info-head-batch">
        <div class="batch-name">套餐: <i></i> </div>
        <ul class="batch-list J_batchBtnList">
            {{ for(var i in it.recommend_package){var rp = it.recommend_package[i]; }} 
            {{?!rp.is_cos}}
            <li class='J_batchBtn  ' data-id='{{=i}}'>{{=rp.short_name}} <i>{{=rp.price_min}}</i> 元</li>
            {{??}} 
            <li class='J_batchBtn disabled' data-id='{{=i}}'>
                <button class="btn " disabled>{{=rp.short_name}} <i>{{=rp.price_min}}</i> 元</button>
            </li>
            {{?}}
            {{ } }}
            <li class="J_batchBtn default current" data-id='none'>无</li>
        </ul>
        <div class="batch-info">下一步，可以挑选套装中商品的颜色</div>
    </dd>
    {{?}}
    {{? !it.is_cos}}
    <dd class="goods-info-head-cart" id="goodsDetailBtnBox">
        <a href="{{=it.nextStep}}" id="goodsDetailAddCartBtn" class="btn  btn-primary goods-add-cart-btn"
    data-disabled="false" data-gid="{{=it.addCartGid}}" data-package="{{=it.needChioce}}">
            <i class="iconfont">&#xe60a;</i>加入购物车
        </a>
        <a id="goodsDetailCollectBtn"  data-isfavorite="false" {{?it.closeFavoriteAddBtn}}style='display:none;'{{?}} class=" btn btn-gray  goods-collect-btn ">
            <i class="iconfont default">&#xe62d;</i> <i class="iconfont red">&#xe62e;</i><i class="iconfont red J_redCopy">&#xe62e;</i>喜欢
        </a>
    </dd>
    {{?? !it.isPackage && it.defaultGoodsId}}
    <dd class="goods-info-head-cart" id="goodsDetailBtnBox">
        {{?!it.closeSubscribe}}
        <a href="{{=it.subUrl}}"  class="btn  btn-gray goods-over-btn">
            <i class="iconfont "></i>到货通知
        </a>
        {{??}}
        <a href="javascript:void(0);"  class="btn  btn-disabled goods-over-btn">
            <i class="iconfont "></i>暂时缺货
        </a>
        {{?}}
        <a id="goodsDetailCollectBtn"  data-isfavorite="false" {{?it.closeFavoriteAddBtn}}style='display:none;'{{?}} class=" btn btn-gray  goods-collect-btn ">
            <i class="iconfont default">&#xe62d;</i> <i class="iconfont red">&#xe62e;</i><i class="iconfont red J_redCopy">&#xe62e;</i>喜欢
        </a>
    </dd>    
    {{??}}
    <dd class="goods-info-head-cart" id="goodsDetailBtnBox">
        <a href="javascript:void(0);"  class="btn  btn-disabled goods-over-btn">
            <i class="iconfont "></i>暂时缺货
        </a>
        <a id="goodsDetailCollectBtn"  data-isfavorite="false"  {{?it.closeFavoriteAddBtn}}style='display:none;'{{?}} class=" btn btn-gray  goods-collect-btn ">
            <i class="iconfont default">&#xe62d;</i> <i class="iconfont red">&#xe62e;</i><i class="iconfont red J_redCopy">&#xe62e;</i>喜欢
        </a>
    </dd>    
    {{?}}
    <dd class="goods-info-head-userfaq">
        <ul>
            <li  class='J_scrollHref' data-href='#goodsComment' data-index='2'>
                <i class="iconfont">&#xe610;</i>
                评价<b>7428</b>
            </li>
            <li class='J_scrollHref mid' data-href='#goodsFaq'  data-index='3'>
                <i class="iconfont">&#xe617;</i>
                提问<b>97</b>
            </li>
            <li>
                <i class="iconfont">&#xe60f;</i>
                满意度<b>99%</b>
            </li>
        </ul>
    </dd>   
</script>
<script id='goodsSubBar-tmpl' type='text/template'>
    <div class="container">
        <div class="row">
            <div class="span4">
                <dl class="goods-sub-bar-info clearfix">
                    <dt>
                        <img src="http://i1.mifile.cn/a1/T1n2DvBKhT1RXrhCrK!68x68.jpg" >
                    </dt>
                    <dd>
                        <strong>32GB高速存储卡（TF Class10）</strong>
                        <p>
                                                    <em ><span class="J_mi_goodsPrice">79</span>元</em>
                            <del><span class="J_mi_marketPrice">99元</span></del>
                                                 </p>
                    </dd>
                </dl>
            </div>
            <div class="span12">
                <div class="goods-desc-menu" id="goodsSubMenu">
                    <ul class="detail-list J_pro_related_btns  J_detailNav">
                        <li class='current'>
                            <a data-href="#goodsDesc" data-index='0'  class='J_scrollHref'>详情描述</a>
                        </li>

                        <li>
                            <a data-href="#goodsParam" data-index='1' class='J_scrollHref'>规格参数</a>
                        </li>

                        <li>
                            <a data-href="#goodsComment" data-index='2' class='J_scrollHref'>评价晒单 <i>(7428)</i></a>
                        </li>

                        <li>
                            <a data-href="#goodsFaq" data-index='3' class='J_scrollHref'>商品提问 <i>(97)</i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="span4">
                <div class="fr" id="goodsSubBarBtnBox">
                    {{?!it.is_cos}}
                    <a href="{{=it.nextStep}}" class="btn btn-primary goods-add-cart-btn"   
                     id="goodsSubBarAddCartBtn" data-disabled="false" data-gid="{{=it.addCartGid}}" 
                    data-package="{{=it.needChioce}}" >
                        <i class="iconfont">&#xe60a;</i>加入购物车</a>
                    {{?? !it.isPackage && it.defaultGoodsId}}
                   
                        {{?!it.closeSubscribe}}
                    <a href="{{=it.subUrl}}"  class="btn  btn-gray goods-over-btn">
                        <i class="iconfont "></i>到货通知
                    </a>
                        {{??}}
                    <a href="javascript:void(0);"  class="btn  btn-disabled goods-over-btn">
                        <i class="iconfont "></i>暂时缺货
                    </a>
                        {{?}}
                    {{??}} 
                    <a href="javascript:void(0);"  class="btn  btn-disabled goods-over-btn">
                        <i class="iconfont "></i>暂时缺货
                    </a>
                    {{?}}
                </div>
            </div>
        </div>
    </div>
</script>
    <script src="http://s01.mifile.cn/js/base.min.js?v2015a"></script>
    <script>
    (function() {
        MI.namespace('GLOBAL_CONFIG');
        MI.GLOBAL_CONFIG = {
            orderSite: 'http://order.mi.com',
            wwwSite: 'http://www.mi.com',
            cartSite: 'http://cart.mi.com',
            itemSite: 'http://item.mi.com',
            assetsSite: 'http://s01.mifile.cn',
            listSite: 'http://list.mi.com',
            searchSite: 'http://search.mi.com',
            mySite: 'http://my.mi.com',
            damiaoSite: 'http://tp.hd.mi.com/',
            damiaoGoodsId:null,
            logoutUrl: 'http://order.mi.com/site/logout',
            staticSite: 'http://static.mi.com',
            quickLoginUrl: 'https://account.xiaomi.com/pass/static/login.html'
        };
        MI.setLoginInfo.orderUrl = MI.GLOBAL_CONFIG.orderSite + '/user/order';
        MI.setLoginInfo.logoutUrl = MI.GLOBAL_CONFIG.logoutUrl;
        MI.setLoginInfo.init(MI.GLOBAL_CONFIG);
        MI.miniCart.init();
        MI.updateMiniCart();      
    })();
    </script>
    <script src="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
js/xmsg_ti.js"></script>
        <script type="text/javascript" src="http://s01.mifile.cn/js/item/goodsDetail.min.js?2015083102"></script>

<script>
 MI.goodsDetailConfig = {};
 MI.goodsDetailConfig.commentWwwUrl = "http://comment.www.mi.com/";
 MI.goodsDetailConfig.commentOrderUrl = "http://comment.order.mi.com";
 MI.goodsDetailConfig.commentHuodongUrl = "http://comment.huodong.mi.com";
 MI.goodsDetailConfig.stockUrl = "http://cn.orderapi.mi.com";
 </script>
        <script>
    var _msq = _msq || [];
    _msq.push(['setDomainId', 100]);
    _msq.push(['trackPageView']);
    (function() {
        var ms = document.createElement('script');
        ms.type = 'text/javascript';
        ms.async = true;
        ms.src = '<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
js/xmst.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ms, s);
    })();
    </script>
</body>
</html>
