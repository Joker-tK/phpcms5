<?php /* Smarty version Smarty-3.0.5, created on 2015-11-21 14:47:24
         compiled from "D:\WWW\phpcms5\admin/liangjingcms/admin.tpl" */ ?>
<?php /*%%SmartyHeaderCode:229255650137c21b324-93046580%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bfab7509004fe77e331b8d98ca05956232bf20ba' => 
    array (
      0 => 'D:\\WWW\\phpcms5\\admin/liangjingcms/admin.tpl',
      1 => 1448064337,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '229255650137c21b324-93046580',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_date_format')) include 'D:\WWW\phpcms5\source\core\plugins\modifier.date_format.php';
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $_smarty_tpl->getVariable('page_charset')->value;?>
" />
<title>管理员</title>
<meta name="author" content="<?php echo $_smarty_tpl->getVariable('copyright_author')->value;?>
" />
<link type="text/css" rel="stylesheet" href="liangjingcms/css/admin_style.css" />
<script type='text/javascript' src='js/jquery-1.4.4.min.js'></script>
<script type='text/javascript' src='js/command.js'></script>
<script type="text/javascript" src='../data/editor/kindeditor.js'></script>
</head>
<body>
<?php if ($_smarty_tpl->getVariable('action')->value==''){?>
<div class="main-wrap">
  <div class="path"><p>当前位置：系统设置<span>&gt;&gt;</span>管理员设置</p></div>
  <div class="main-cont">
    <h3 class="title"><a href="ljcms_admin.php?action=add" class="btn-general"><span>添加帐号</span></a>管理员</h3>
	<form action="ljcms_admin.php" method="post" name="myform" id="myform" style="margin:0">
	<input type="hidden" name="action" id="action" value="del" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <thead class="tb-tit-bg">
	  <tr>
	    <th width="8%"><div class="th-gap">选择</div></th>
		<th width="10%"><div class="th-gap">帐号</div></th>
		<th width="15%"><div class="th-gap">权限组</div></th>
		<th width="10%"><div class="th-gap">状态</div></th>
		<th width="18%"><div class="th-gap">登录时间</div></th>
		<th width="15%"><div class="th-gap">登录IP</div></th>
		<th width="10%"><div class="th-gap">登录次数</div></th>
		<th><div class="th-gap">操作</div></th>
	  </tr>
	  </thead>
	  <tfoot class="tb-foot-bg"></tfoot>
	  <?php  $_smarty_tpl->tpl_vars['volist'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('admin')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['volist']->key => $_smarty_tpl->tpl_vars['volist']->value){
?>
	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">
	    <td align="center"><input name="id[]" type="checkbox" value="<?php echo $_smarty_tpl->tpl_vars['volist']->value['adminid'];?>
" onClick="checkItem(this, 'chkAll')"></td>
		<td><?php echo $_smarty_tpl->tpl_vars['volist']->value['adminname'];?>
</td>
		<td align="center"><?php if ($_smarty_tpl->tpl_vars['volist']->value['super']==1){?><font color="green">系统管理员</font><?php }else{ ?><?php if ($_smarty_tpl->tpl_vars['volist']->value['groupid']==0){?><font color="#999999">~~</font><?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['volist']->value['groupname'];?>
<?php }?><?php }?></td>
		<td align="center">
		<?php if ($_smarty_tpl->tpl_vars['volist']->value['flag']==0){?>
			<input type="hidden" id="attr_flag<?php echo $_smarty_tpl->tpl_vars['volist']->value['adminid'];?>
" value="flagopen" />
			<img id="flag<?php echo $_smarty_tpl->tpl_vars['volist']->value['adminid'];?>
" src="liangjingcms/images/no.gif" onClick="javascript:fetch_ajax('flag','<?php echo $_smarty_tpl->tpl_vars['volist']->value['adminid'];?>
');" style="cursor:pointer;">
		<?php }else{ ?>
			<input type="hidden" id="attr_flag<?php echo $_smarty_tpl->tpl_vars['volist']->value['adminid'];?>
" value="flagclose" />
			<img id="flag<?php echo $_smarty_tpl->tpl_vars['volist']->value['adminid'];?>
" src="liangjingcms/images/yes.gif" onClick="javascript:fetch_ajax('flag','<?php echo $_smarty_tpl->tpl_vars['volist']->value['adminid'];?>
');" style="cursor:pointer;">	
		<?php }?>
		</td>
		<td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['volist']->value['logintimeline'],"%Y/%m/%d %H:%M:%S");?>
</td>
		<td><?php echo $_smarty_tpl->tpl_vars['volist']->value['loginip'];?>
</td>
		<td align="center"><?php echo $_smarty_tpl->tpl_vars['volist']->value['logintimes'];?>
</td>
		<td align="center"><a href="ljcms_admin.php?action=edit&id=<?php echo $_smarty_tpl->tpl_vars['volist']->value['adminid'];?>
&page=<?php echo $_smarty_tpl->getVariable('page')->value;?>
" class="icon-edit">编辑</a>&nbsp;&nbsp;<a href="ljcms_admin.php?action=del&id[]=<?php echo $_smarty_tpl->tpl_vars['volist']->value['adminid'];?>
" onClick="{if(confirm('确定要删除该信息?')){return true;} return false;}" class="icon-del">删除</a></td>
	  </tr>
	  <?php }} else { ?>
      <tr>
	    <td colspan="8" align="center">暂无信息</td>
	  </tr>
	  <?php } ?>
	  <?php if ($_smarty_tpl->getVariable('total')->value>0){?>
	  <tr>
		<td align="center"><input name="chkAll" type="checkbox" id="chkAll" onClick="checkAll(this, 'id[]')" value="checkbox"></td>
		<td class="hback" colspan="7"><input class="button" name="btn_del" type="button" value="删 除" onClick="{if(confirm('确定删除选定信息吗!?')){$('#action').val('del');$('#myform').submit();return true;}return false;}" class="button">&nbsp;&nbsp;共[ <b><?php echo $_smarty_tpl->getVariable('total')->value;?>
</b> ]条记录</td>
	  </tr>
	  <?php }?>
	</table>
	</form>
	<?php if ($_smarty_tpl->getVariable('pagecount')->value>1){?>
	<table width='95%' border='0' cellspacing='0' cellpadding='0' align='center' style="margin-top:10px;">
	  <tr>
		<td align='center'><?php echo $_smarty_tpl->getVariable('showpage')->value;?>
</td>
	  </tr>
	</table>
	<?php }?>
  </div>
</div>
<?php }?>

<?php if ($_smarty_tpl->getVariable('action')->value=="add"){?>
<div class="main-wrap">
  <div class="path"><p>当前位置：系统设置<span>&gt;&gt;</span>添加管理员</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_admin.php" class="btn-general"><span>返回列表</span></a>添加管理员</h3>
    <form name="myform" id="myform" method="post" action="ljcms_admin.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveadd" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">登录帐号：<span class='f_red'>*</span></td>
		<td class='hback' width="85%"><input type="text" name="adminname" id="adminname" class="input-txt" /> <span class='f_red' id="dadminname"></span> 4-16个字符，只能由中文、字母、数字和下横线组成</td>
	  </tr>
	  <tr>
		<td class='hback_1'>登录密码：<span class='f_red'>*</span></td>
		<td class='hback'><input type="password" name="password" id="password" class="input-txt" /> <span class='f_red' id="dpassword"></span> 4-16个字符</td>
	  </tr>
	  <tr>
		<td class='hback_1'>帐号设置：<span class='f_red'></span></td>
		<td class='hback'><?php echo $_smarty_tpl->getVariable('flag_checkbox')->value;?>
，<?php echo $_smarty_tpl->getVariable('super_checkbox')->value;?>
</td>
	  </tr>
	  <tr>
		<td class='hback_1'>帐号权限：<span class='f_red'></span></td>
		<td class='hback'><?php echo $_smarty_tpl->getVariable('groupid_select')->value;?>
 <span class='f_red' id="dgroupid"></span><br /><span class="f_gray">非系统管理员的权限组，系统管理员拥有所有操作权限。</span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>备注说明： </td>
		<td class='hback'><textarea name='memo' id='memo' style='width:60%;height:65px;overflow:auto;color:#444444;'></textarea></td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="添加保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<?php }?>

<?php if ($_smarty_tpl->getVariable('action')->value=="edit"){?>
<div class="main-wrap">
  <div class="path"><p>当前位置：系统设置<span>&gt;&gt;</span>编辑帐号</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_admin.php?<?php echo $_smarty_tpl->getVariable('comeurl')->value;?>
" class="btn-general"><span>返回列表</span></a>编辑帐号</h3>
    <form name="myform" id="myform" method="post" action="ljcms_admin.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveedit" />
	<input type="hidden" name="id" value="<?php echo $_smarty_tpl->getVariable('id')->value;?>
" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width='15%'>登录帐号 </td>
		<td class='hback' width='85%'><?php echo $_smarty_tpl->getVariable('admin')->value['adminname'];?>
</td>
	  </tr>
	  <tr>
		<td class='hback_1'>登录密码 </td>
		<td class='hback'><input type="password" name="password" id="password" class="input-txt" /> <span id="dpassword" class="f_red"></span> (不修改请留空)</td>
	  </tr>
	  <tr>
		<td class='hback_1'>帐号设置 <span class='f_red'></span></td>
		<td class='hback'><?php echo $_smarty_tpl->getVariable('flag_checkbox')->value;?>
，<?php echo $_smarty_tpl->getVariable('super_checkbox')->value;?>
</td>
	  </tr>
	  <tr>
		<td class='hback_1'>帐号权限 <span class='f_red'></span></td>
		<td class='hback'><?php echo $_smarty_tpl->getVariable('groupid_select')->value;?>
 <span class='f_red' id="dgroupid"></span><br /><span class="f_gray">非系统管理员的权限组，系统管理员拥有所有操作权限。</span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>备注说明 </td>
		<td class='hback'><textarea name="memo" id="memo" style="width:60%;height:65px;overflow:auto;color:#444444;"><?php echo $_smarty_tpl->getVariable('admin')->value['memo'];?>
</textarea></td>
	  </tr>
	  <tr>
		<td class='hback_1'>登录次数 </td>
		<td class='hback'><?php echo $_smarty_tpl->getVariable('admin')->value['logintimes'];?>
</td>
	  </tr>
	  <tr>
		<td class='hback_1'>登录时间 </td>
		<td class='hback'><?php echo smarty_modifier_date_format($_smarty_tpl->getVariable('admin')->value['logintimeline'],"%Y-%m-%d %H:%M:%S");?>
</td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="更新保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<?php }?>

<?php if ($_smarty_tpl->getVariable('action')->value=="changepassword"){?>
<div class="main-wrap">
  <div class="path"><p>当前位置：系统设置<span>&gt;&gt;</span>修改密码</p></div>
  <div class="main-cont">
	<h3 class="title">修改密码</h3>
    <form name="myform" id="myform" method="post" action="ljcms_admin.php" onsubmit='return checkpassword();' />
    <input type="hidden" name="action" value="savepassword" />
	<table cellpadding='5' cellspacing='5' class='tab'>
	  <tr>
		<td class='hback_none' width='15%'>登录帐号： </td>
		<td class='hback_none' width='85%'><?php echo $_smarty_tpl->getVariable('uc_adminname')->value;?>
</td>
	  </tr>
	  <tr>
		<td class='hback_none'>旧 密 码： </td>
		<td class='hback_none'><input type='password' name='oldpassword' id='oldpassword' maxlength="16" class="input-txt" /> <span id='doldpassword' class='f_red'></span> </td>
	  </tr>
	  <tr>
		<td class='hback_none'>新 密 码： </td>
		<td class='hback_none'><input type='password' name='newpassword' id='newpassword' maxlength="16" class="input-txt" /> <span id='dnewpassword' class='f_red'></span> 4-16个字符</td>
	  </tr>
	  <tr>
		<td class='hback_none'>确认密码： </td>
		<td class='hback_none'><input type='password' name='confirmpassword' id='confirmpassword' maxlength="16" class="input-txt" /> <span id='dconfirmpassword' class='f_red'></span> </td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="更新保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<?php }?>
</td></tr></table>
</body>
</html>
<script type="text/javascript">
function checkform() {
	var t = "";
	var v = "";

	t = "adminname";
	v = $("#"+t).val().length;
	if(v<4 || v>16) {
		dmsg("登录帐号必须为4-16个字符！", t);
		return false;
	}

	t = "password";
	v = $("#"+t).val().length;
	if(v<4 || v>16) {
		dmsg("登录密码必须为4-16个字符", t);
		return false;
	}
	return true;
}

function checkpassword() {
	var t = "";
	var v = "";

	t = "oldpassword";
	v = $("#"+t).val();
	if(v=="") {
		dmsg("原密码不能为空", t);
		return false;
	}

	t = "newpassword";
	v = $("#"+t).val().length;
	if(v<4 || v>16) {
		dmsg("新密码必须为4-16个字符！", t);
		return false;
	}
    
	t = "confirmpassword";
	if($("#confirmpassword").val()!=$("#newpassword").val()) {
		dmsg("确认密码不正确！", t);
		return false;
	}
	return true;
}
</script>
