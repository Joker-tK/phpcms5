<?php /* Smarty version Smarty-3.0.5, created on 2015-11-26 16:53:08
         compiled from "D:\WWW\phpcms21\tpl/xiaomi/download_list.html" */ ?>
<?php /*%%SmartyHeaderCode:239115656c874ef5cb8-94253457%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9519ad0a3aed156bb11a15a7524212f5139a1c65' => 
    array (
      0 => 'D:\\WWW\\phpcms21\\tpl/xiaomi/download_list.html',
      1 => 1448412730,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '239115656c874ef5cb8-94253457',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('tplpath')->value)."site_header.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>

<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
css/index2.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
css/download.min.css" />

<div class="exchange xm-service-box">

    <div class="breadcrumbs">
        <div class="container breadcrumbs">
            <a href='../../<?php echo $_smarty_tpl->getVariable('url_index')->value;?>
'>首页</a><span class="sep">/</span><?php echo $_smarty_tpl->getVariable('navigation')->value;?>

        </div>
    </div>

    <div class="container">
        <div class="exchange">
            <div class="row">


                <div class="span4">
                    <div class="xm-service-sidebar">
                        <div class="content">
                            <div class="xm-sidebar-content">
                                <div class="nav-list">
                                    <h3><?php echo $_smarty_tpl->getVariable('lang_download')->value;?>
</h3>
                                    <ul class="uc-nav-list feature">
                                    <?php  $_smarty_tpl->tpl_vars['volist'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('volist_downloadcategory')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['volist']->key => $_smarty_tpl->tpl_vars['volist']->value){
?>
                                        <li data-class="fast"><a href="<?php echo $_smarty_tpl->tpl_vars['volist']->value['url'];?>
" data-stat-id="c2fc487af4829e16" onclick="_msq.push(['trackEvent', 'c325bcac4b1c78ed-c2fc487af4829e16', '<?php echo $_smarty_tpl->tpl_vars['volist']->value['url'];?>
', 'pcpid']);"><?php echo $_smarty_tpl->tpl_vars['volist']->value['catename'];?>
</a></li>
                                    <?php }} ?>     
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="span16">
                    <div class="content">
                        <div class="xm-exchange-content phone" data-class="phone" style="display: block;">
                            <div class="service-download-index">
                                <div class="service-download-title">
                                    <h2>下载中心</h2>
                                    <p>在这里您可获取企行网中所有相关内容下载（有付费内容请注意）</p>
                                </div>
                                <div class="service">
                                    <ul>
                                    <?php  $_smarty_tpl->tpl_vars['volist'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('download')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['volist']->key => $_smarty_tpl->tpl_vars['volist']->value){
?>
                                        <li>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['volist']->value['url'];?>
" target="_blank" data-stat-id="f7d3531d36de2c1e" onclick="_msq.push(['trackEvent', '3b7fd396c60a921e-f7d3531d36de2c1e', '<?php echo $_smarty_tpl->tpl_vars['volist']->value['url'];?>
', 'pcpid']);">
                                                <i class="icon-welcome-old icon-download01"></i>
                                                <span><?php echo $_smarty_tpl->tpl_vars['volist']->value['title'];?>
</span>
                                            </a>
                                        </li>
                                    <?php }} ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>


   

    <?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('tplpath')->value)."site_footer.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
<script src="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
js/base.min.js"></script>
<script>
(function() {
    MI.namespace('GLOBAL_CONFIG');
    MI.GLOBAL_CONFIG = {
        orderSite: 'http://order.mi.com',
        wwwSite: 'http://www.mi.com',
        cartSite: 'http://cart.mi.com',
        itemSite: 'http://item.mi.com',
        assetsSite: 'http://s01.mifile.cn',
        listSite: 'http://list.mi.com',
        searchSite: 'http://search.mi.com',
        mySite: 'http://my.mi.com',
        damiaoSite: 'http://tp.hd.mi.com/',
        damiaoGoodsId:null,
        logoutUrl: 'http://order.mi.com/site/logout',
        staticSite: 'http://static.mi.com',
        quickLoginUrl: 'https://account.xiaomi.com/pass/static/login.html'
    };
    MI.setLoginInfo.orderUrl = MI.GLOBAL_CONFIG.orderSite + '/user/order';
    MI.setLoginInfo.logoutUrl = MI.GLOBAL_CONFIG.logoutUrl;
    MI.setLoginInfo.init(MI.GLOBAL_CONFIG);
    MI.miniCart.init();
    MI.updateMiniCart();
})();
</script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
js/air.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
css/product-air.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
css/product-air-features.min.css">
<script src="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
js/xmsg_ti.js"></script>
<script>
var _msq = _msq || [];
_msq.push(['setDomainId', 100]);
_msq.push(['trackPageView']);
(function() {
    var ms = document.createElement('script');
    ms.type = 'text/javascript';
    ms.async = true;
    ms.src = '<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
js/xmst.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(ms, s);
})();
</script>

</body>
</html>