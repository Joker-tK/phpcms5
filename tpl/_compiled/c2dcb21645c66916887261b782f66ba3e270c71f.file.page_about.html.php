<?php /* Smarty version Smarty-3.0.5, created on 2015-11-26 17:54:41
         compiled from "D:\WWW\phpcms21\tpl/xiaomi/page_about.html" */ ?>
<?php /*%%SmartyHeaderCode:88455656d6e138ca36-72711177%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c2dcb21645c66916887261b782f66ba3e270c71f' => 
    array (
      0 => 'D:\\WWW\\phpcms21\\tpl/xiaomi/page_about.html',
      1 => 1448531678,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '88455656d6e138ca36-72711177',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_htmlencode')) include 'D:\WWW\phpcms21\source\core\plugins\modifier.htmlencode.php';
?><?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('tplpath')->value)."site_header.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
css/index2.min.css" />

<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
css/lubo.min.css" />
<script type="text/javascript">var _head_over_time = (new Date()).getTime();</script>
<div class="exchange xm-service-box">

     <div class="breadcrumbs">
        <div class="container breadcrumbs">
            <a href='../../<?php echo $_smarty_tpl->getVariable('url_index')->value;?>
'>首页</a><span class="sep">/</span>关于我们<?php echo $_smarty_tpl->getVariable('navigation')->value;?>

        </div>
    </div>
   
    <div class="container">
        <div class="exchange">
            <div class="home-hero">
                <div class="home-hero-slider">
                    <div id="J_homeSlider" class="xm-slider" data-stat-title="焦点图轮播">
                    <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('ads_zone1')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['adsname']['iteration']=0;
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['item']->key;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['adsname']['iteration']++;
?>
                        <div class="slide loaded">
                            <a href="<?php echo $_smarty_tpl->tpl_vars['item']->value['url'];?>
" target="_blank"  onClick="_gaq.push(['_trackEvent', '<?php echo $_smarty_tpl->tpl_vars['item']->value['adsname'];?>
', 'A<?php echo $_smarty_tpl->getVariable('smarty')->value['foreach']['adsname']['iteration'];?>
', '<?php echo $_smarty_tpl->tpl_vars['item']->value['url'];?>
']);">
                                <img width="1226" height="440" src="<?php echo $_smarty_tpl->tpl_vars['item']->value['uploadfiles'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['item']->value['adsname'];?>
" />
                            </a>
                        </div>
                    <?php }} ?>  
                   </div>
                </div>
            </div>
    
            <div class="row">
                <div class="span4">
                    <div class="xm-service-sidebar">
                        <div class="content">
                            <div class="xm-sidebar-content">
                                <div class="nav-list">
                                    <h3><?php echo $_smarty_tpl->getVariable('lang_about')->value;?>
</h3>
                                    <ul class="uc-nav-list feature">
                                    <?php  $_smarty_tpl->tpl_vars['volist'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('volist_page')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['volist']->key => $_smarty_tpl->tpl_vars['volist']->value){
?>
                                        <li data-class="fast"><a href="<?php echo $_smarty_tpl->tpl_vars['volist']->value['url'];?>
" data-stat-id="c2fc487af4829e16" onclick="_msq.push(['trackEvent', 'c325bcac4b1c78ed-c2fc487af4829e16', '<?php echo $_smarty_tpl->tpl_vars['volist']->value['url'];?>
', 'pcpid']);"><?php echo $_smarty_tpl->tpl_vars['volist']->value['title'];?>
</a></li>
                                    <?php }} ?>     
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="span16">
                    <div class="content">
                        <div class="xm-exchange-content phone" data-class="phone" style="display: block;">
                            <div class="service-hd">
                                <h2><?php echo $_smarty_tpl->getVariable('lang_about')->value;?>
</h2>
                            </div>
                            <div class="text phone">
                                <div class="exchange_bd">
                                    <?php echo smarty_modifier_htmlencode($_smarty_tpl->getVariable('config')->value['about']);?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            

            </div>
        </div>
    </div>

</div>



   

    <?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('tplpath')->value)."site_footer1.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
