<?php /* Smarty version Smarty-3.0.5, created on 2015-11-27 17:13:14
         compiled from "D:\WWW\phpcms21\tpl/xiaomi/site_header.html" */ ?>
<?php /*%%SmartyHeaderCode:3083656581eaaef04c2-32555288%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2e1569b4bf32f8d2f2da6a6eb9d09c4a9345db41' => 
    array (
      0 => 'D:\\WWW\\phpcms21\\tpl/xiaomi/site_header.html',
      1 => 1448615583,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3083656581eaaef04c2-32555288',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta charset="UTF-8">
        <title><?php echo $_smarty_tpl->getVariable('page_title')->value;?>
-<?php echo $_smarty_tpl->getVariable('copyright_header')->value;?>
</title>
        <meta name="description" content="<?php echo $_smarty_tpl->getVariable('page_description')->value;?>
" />
        <meta name="keywords" content="<?php echo $_smarty_tpl->getVariable('page_keyword')->value;?>
" />
        <meta name="author" content="<?php echo $_smarty_tpl->getVariable('copyright_author')->value;?>
" />
        
        <link rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
css/base.min.css">
        <script type="text/javascript">var _head_over_time = (new Date()).getTime();</script>  
             
        <div class="site-topbar">
            <div class="container">
                <div class="topbar-nav">
                    <a href="http://www.mi.com/index.html" >企行网</a><span class="sep">|</span>
                    <a href="http://www.miui.com/" target="_blank" >MIUI</a><span class="sep">|</span>
                    <a href="http://www.miliao.com/" target="_blank" >行聊</a><span class="sep">|</span>
                    <a href="http://game.xiaomi.com/" target="_blank" >游戏</a><span class="sep">|</span>
                    <a href="http://www.duokan.com/" target="_blank" >多看阅读</a><span class="sep">|</span>
                    <a href="https://i.mi.com/" target="_blank" ">云服务</a><span class="sep">|</span>
                    <a href="http://www.mi.com/c/appdownload/" target="_blank" ">企行网移动版</a><span class="sep">|</span>
                    <a href="http://static.mi.com/feedback/" target="_blank" >问题反馈</a><span class="sep">|</span>
                    <a href="http://www.mi.com/air/#J_modal-globalSites"  >Select Region</a>
                </div>
                <div class="topbar-cart" id="J_miniCartTrigger">
                    <a class="cart-mini" id="J_miniCartBtn" href="productbuy.php"><i class="iconfont"></i>购物车<span class="cart-mini-num J_cartNum">（0）</span></a>
                    <div class="cart-menu" id="J_miniCartMenu">
                        <div class="loading"><div class="loader"></div></div>
                    </div>
                </div>
                <div class="topbar-info" id="J_userInfo">
                    <a class="link" href="indexm.php">登录</a><span class="sep">|</span><a class="link" href="register.php" >注册</a>        </div>
            </div>
        </div>
    </head>
<body>

<div class="site-header">
     <div class="container">
        <div class="header-logo">
           <a href="<?php echo $_smarty_tpl->getVariable('urlpath')->value;?>
"><img src="<?php echo $_smarty_tpl->getVariable('urlpath')->value;?>
<?php echo $_smarty_tpl->getVariable('config')->value['logoimg'];?>
" width="170px" height="70px" ></a>
        </div>
    	                 <!--顶部导航  -->
        <div class="header-nav">
            <ul class="nav-list J_navMainList clearfix">
                <li class="nav-item" >
                    <a class="link" href="<?php echo $_smarty_tpl->getVariable('url_index')->value;?>
"  ><span class="text"></span><span class="arrow"></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
            		
                </li>
                <li class="nav-item">
                    <a class="link" href="<?php echo $_smarty_tpl->getVariable('url_index')->value;?>
" "><span class="text"></span><span class="arrow"></span><?php echo $_smarty_tpl->getVariable('lang_index')->value;?>
</a>
                </li>
                <li class="nav-item">
                    <a class="link" href="<?php echo $_smarty_tpl->getVariable('url_about')->value;?>
" ><span class="text"></span><span class="arrow"></span><?php echo $_smarty_tpl->getVariable('lang_about')->value;?>
</a>
                </li>
                <li class="nav-item">
                    <a class="link" href="<?php echo $_smarty_tpl->getVariable('url_product')->value;?>
"><span class="text"></span><span class="arrow"></span><?php echo $_smarty_tpl->getVariable('lang_product')->value;?>
</a>
                </li>
                <li class="nav-item">
                    <a class="link" href="<?php echo $_smarty_tpl->getVariable('url_info')->value;?>
"><span class="text"></span><span class="arrow"></span><?php echo $_smarty_tpl->getVariable('lang_info')->value;?>
</a>
                </li>
                <li class="nav-item">
                    <a class="link" href="<?php echo $_smarty_tpl->getVariable('url_case')->value;?>
" ><span class="text"></span><span class="arrow"></span><?php echo $_smarty_tpl->getVariable('lang_case')->value;?>
</a>
                </li>
                <li class="nav-item">
                    <a class="link" href="<?php echo $_smarty_tpl->getVariable('url_download')->value;?>
" ><span class="text"></span><span class="arrow"></span><?php echo $_smarty_tpl->getVariable('lang_download')->value;?>
</a>
                </li>
                <li class="nav-item">
                    <a class="link" href="<?php echo $_smarty_tpl->getVariable('url_guestbook')->value;?>
"><span class="text"></span><span class="arrow"></span><?php echo $_smarty_tpl->getVariable('lang_guestbook')->value;?>
</a>
                </li>
               
            </ul>
        </div>
        <div class="header-search">
            <form id="J_searchForm" class="search-form clearfix" action="javascript:void(0)" method="get">
                <label for="search" class="hide">站内搜索</label>
                <input class="search-text" type="search" id="search" name="keyword">
                <input type="submit" class="search-btn iconfont" value="">
                <div class="search-hot-words"></div>
    	        <div id="J_keywordList" class="keyword-list hide">
    	        	<ul class="result-list"></ul>
    	        </div>
    	    </form>
        </div>
    </div>
</div>
