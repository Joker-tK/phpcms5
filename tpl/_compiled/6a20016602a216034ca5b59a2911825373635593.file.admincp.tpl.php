<?php /* Smarty version Smarty-3.0.5, created on 2015-11-21 23:46:11
         compiled from "D:\WWW\phpcms8\admin/liangjingcms/admincp.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4045565091c359aad7-43145078%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6a20016602a216034ca5b59a2911825373635593' => 
    array (
      0 => 'D:\\WWW\\phpcms8\\admin/liangjingcms/admincp.tpl',
      1 => 1448084839,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4045565091c359aad7-43145078',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $_smarty_tpl->getVariable('page_charset')->value;?>
" />
<title>管理中心-[<?php echo $_smarty_tpl->getVariable('config')->value['sitename'];?>
]</title>
<LINK href="liangjingcms/css/style.css" type="text/css" rel="stylesheet">
<SCRIPT language=javaScript src="liangjingcms/js/admin.js" type="text/javascript"></SCRIPT>
<SCRIPT>
var status = 1;
var Menus = new DvMenuCls;
document.onclick=Menus.Clear;
function switchSysBar(){
     if (1 == window.status){
		  window.status = 0;
          switchPoint.innerHTML = '<img src="liangjingcms/images/left.gif">';
          document.all("frmTitle").style.display="none"
     }
     else{
		  window.status = 1;
          switchPoint.innerHTML = '<img src="liangjingcms/images/right.gif">';
          document.all("frmTitle").style.display=""
     }
}
</SCRIPT>
<META content="MSHTML 6.00.6000.16640" name=GENERATOR></HEAD>
<BODY style="MARGIN: 0px"><!--NvHeader-->
<DIV class=top_table>
<DIV class=top_table_leftbg>
<DIV class=system_logo><IMG src="liangjingcms/images/logo_up.gif"></DIV>
<DIV class=menu>
<UL>
  <LI id=menu_0 onmouseover=Menus.Show(this,0) onClick="getleftbar(this);"><a href="#">单页管理</a>
  <DIV class=menu_childs onmouseout=Menus.Hide(0);>
  <UL>

  <li><a href="ljcms_setting.php?action=about" target='frmright'>关于我们</a></LI>
  <li><a href="ljcms_setting.php?action=contact" target='frmright'>联系我们</a></LI> 
  <li><a href="ljcms_pagecate.php" target='frmright'>单页分类</a></li>
  <li><a href="ljcms_page.php" target='frmright'>自定义页面</a></li>
   </UL></DIV>
  <DIV class=menu_div><IMG style="VERTICAL-ALIGN: bottom" 
  src="liangjingcms/images/menu01_right.gif"></DIV></LI>
  <LI id=menu_1 onmouseover=Menus.Show(this,0) onclick=getleftbar(this);><a href="#">新闻资讯</a>
  <DIV class=menu_childs onmouseout=Menus.Hide(0);>
  <UL>
  <li><a href="ljcms_infocate.php" target='frmright'>新闻分类</a></LI>
  <li><a href="ljcms_infocate.php?action=add" target='frmright'>添加新闻分类</a></LI>
  <li><a href="ljcms_infocate.php?action=setting" target='frmright'>新闻分类设置</a></LI>
  <li><a href="ljcms_info.php" target='frmright'>新闻信息列表</a></LI>
  <li><a href="ljcms_info.php?action=add" target='frmright'>发布新闻信息</a></LI>
  <li><a href="ljcms_info.php?action=setting" target='frmright'>批量设置新闻信息</a></LI>
  </UL></DIV>
  <DIV class=menu_div><IMG style="VERTICAL-ALIGN: bottom" 
  src="liangjingcms/images/menu01_right.gif"></DIV></LI>
  <LI id=menu_2 onmouseover=Menus.Show(this,0) onclick=getleftbar(this);><a href="#">产品中心</A> 
  <DIV class=menu_childs onmouseout=Menus.Hide(0);>
  <UL>
    <li><a href="ljcms_productcate.php" target='frmright'>产品分类列表</a></LI>
    <li><a href="ljcms_productcate.php?action=add" target='frmright'>添加产品分类</a></LI>
    <li><a href="ljcms_productcate.php?action=setting" target='frmright'>分类批量设置</a></LI>
    <li><a href="ljcms_product.php" target='frmright'>产品信息列表</a></LI>
    <li><a href="ljcms_product.php?action=add" target='frmright'>添加产品信息</a></LI>
    <li><a href="ljcms_product.php?action=setting" target='frmright'>产品批量设置</a></LI>
	</UL>
	</DIV>
  <DIV class=menu_div><IMG style="VERTICAL-ALIGN: bottom" 
  src="liangjingcms/images/menu01_right.gif"></DIV></LI>

  <LI id=menu_3 onmouseover=Menus.Show(this,0) onclick=getleftbar(this);><a href="#">成功案例</a>
  <DIV class=menu_childs onmouseout=Menus.Hide(0);>
  <UL>
    <li><a href="ljcms_casecate.php" target='frmright'>成功案例分类</a></LI> 
    <li><a href="ljcms_casecate.php?action=add" target='frmright'>添加成功案例</a></LI> 
    <li><a href="ljcms_casecate.php?action=setting" target='frmright'>案例分类批量设置</a></LI>
    <li><a href="ljcms_case.php" target='frmright'>成功案例内容</a> </LI>
    <li><a href="ljcms_case.php?action=add" target='frmright'>发布成功案例</a></LI> 
    <li><a href="ljcms_case.php?action=setting" target='frmright'>成功案例批量设置</a></LI>
		  </UL></DIV>
  <DIV class=menu_div><IMG style="VERTICAL-ALIGN: bottom" 
  src="liangjingcms/images/menu01_right.gif"></DIV></LI>
  <LI id=menu_4 onmouseover=Menus.Show(this,0) onclick=getleftbar(this);><a href="#">下载中心</a> 
  <DIV class=menu_childs onmouseout=Menus.Hide(0);>
  <UL>
        <li><a href="ljcms_downloadcate.php" target='frmright'>下载信息分类</a></LI> 
        <li><a href="ljcms_downloadcate.php?action=add" target='frmright'>添加下载分类</a></LI>
        <li><a href="ljcms_downloadcate.php?action=setting" target='frmright'>下载分类批量设置</a></LI>
        <li><a href="ljcms_download.php" target='frmright'>下载信息列表</a></LI>
        <li><a href="ljcms_download.php?action=add" target='frmright'>添加下载信息</a> </LI>
        <li><a href="ljcms_download.php?action=setting" target='frmright'>下载信息批量设置</a></LI>
		</UL></DIV>
  <DIV class=menu_div><IMG style="VERTICAL-ALIGN: bottom" 
  src="liangjingcms/images/menu01_right.gif"></DIV></LI>
  <LI id=menu_5 onmouseover=Menus.Show(this,0) onclick=getleftbar(this);><a href="#">解决方案</a> 
  <DIV class=menu_childs onmouseout=Menus.Hide(0);>
  <UL>
      <li><a href="ljcms_solutioncate.php" target='frmright'>解决方案分类</a></LI>
      <li><a href="ljcms_solutioncate.php?action=add" target='frmright'>添加方案分类</a></LI> 
      <li><a href="ljcms_solutioncate.php?action=setting" target='frmright'>方案分类批量设置</a></LI>
      <li><a href="ljcms_solution.php" target='frmright'>解决方案列表</a></LI> 
      <li><a href="ljcms_solution.php?action=add" target='frmright'>添加解决方案列表</a></LI> 
      <li><a href="ljcms_solution.php?action=setting" target='frmright'>解决方案批量设置</a></LI>
          </UL></DIV>
  <DIV class=menu_div><IMG style="VERTICAL-ALIGN: bottom" 
  src="liangjingcms/images/menu01_right.gif"></DIV></LI>
<LI id=menu_8 onmouseover=Menus.Show(this,0) onclick=getleftbar(this);><a href="#">网站优化</a>
  <DIV class=menu_childs onmouseout=Menus.Hide(0);>
  <UL>
     <li><a href="ljcms_setting.php?action=seo" target='frmright'>站点SEO设置</a></li>
     <li><a href="ljcms_setting.php?action=cache" target='frmright'>站点缓存优化</a></li>
     <li><a href="ljcms_setting.php?action=rewrite" target='frmright'>站点伪静态设置</a></li>
     <li><a href="ljcms_infocate.php?action=setting" target='frmright'>新闻分类</a></li>
     <li><a href="ljcms_info.php?action=setting" target='frmright'>新闻内容</a></li>
     <li><a href="ljcms_productcate.php?action=setting" target='frmright'>产品分类</a></li>
     <li><a href="ljcms_product.php?action=setting" target='frmright'>产品内容</a></li>
     <li><a href="ljcms_casecate.php?action=setting" target='frmright'>案例分类</a></li>
     <li><a href="ljcms_case.php?action=setting" target='frmright'>案例内容</a></li>
     <li><a href="ljcms_solutioncate.php?action=setting" target='frmright'>方案分类</a></li>
     <li><a href="ljcms_solution.php?action=setting" target='frmright'>方案内容</a></li>
     <li><a href="ljcms_downloadcate.php?action=setting" target='frmright'>下载分类</a></li>
     <li><a href="ljcms_download.php?action=setting" target='frmright'>下载内容</a></li>
</UL></DIV>
 <DIV class=menu_div><IMG style="VERTICAL-ALIGN: bottom" 
  src="liangjingcms/images/menu01_right.gif"></DIV></LI>



  <LI id=menu_9 onmouseover=Menus.Show(this,0) onclick=getleftbar(this);><a href="#">系统设置</a>
  <DIV class=menu_childs onmouseout=Menus.Hide(0);>
  <UL>
  <li><a href="ljcms_setting.php" target='frmright'>站点设置</a></LI>
  <li><a href="ljcms_setting.php?action=config" target='frmright'>参数设置</a></LI>
  <li><a href="ljcms_setting.php?action=upload" target='frmright'>图片设置</a></LI>
  <li><a href="ljcms_onlinechat.php" target='frmright'>客服设置</a></LI>

  <li><a href="ljcms_setting.php?action=clearcache" target='frmright'>清除缓存</a></LI>
  <li><a href="ljcms_setting.php?action=clearcompiled" target='frmright'>清除编译文件</a></LI>
  <li><a href="ljcms_admin.php" target='frmright'>管理员设置</a></LI>
  <li><a href="ljcms_admin.php?action=add" target='frmright'>添加管理员</a></LI>
  <li><a href="ljcms_admin.php?action=changepassword" target='frmright'>修改密码</a></LI>
  <li><a href="ljcms_authgroup.php" target='frmright'>管理组设置</a></LI>
  <li><a href="ljcms_authgroup.php?action=add" target='frmright'>添加管理组</a></LI>
  </UL></DIV>
  <DIV class=menu_div><IMG style="VERTICAL-ALIGN: bottom" 
  src="liangjingcms/images/menu01_right.gif"></DIV></LI>

  <LI id=menu_10 onmouseover=Menus.Show(this,0) onclick=getleftbar(this);><a href="#">功能扩展</a>
  <DIV class=menu_childs onmouseout=Menus.Hide(0);>
  <UL>
<li><a href="ljcms_delimitlabel.php" target='frmright'>HTML标签</a></li>
<li><a href="ljcms_delimitlabel.php?action=add" target='frmright'>添加标签</a></li>
<li><a href="ljcms_jobcate.php" target='frmright'>招聘分类</a></li>
<li><a href="ljcms_job.php" target='frmright'>招聘内容</a></li>
<li><a href="ljcms_adszone.php" target='frmright'>广告标签</a></li>
<li><a href="ljcms_adszone.php?action=add" target='frmright'>添加标签</a></li>
<li><a href="ljcms_ads.php" target='frmright'>广告图片</a></li> 
<li><a href="ljcms_ads.php?action=add" target='frmright'>添加图片</a></li>
<li><a href="ljcms_link.php" target='frmright'>链接列表</a></li> 
<li><a href="ljcms_link.php?action=add" target='frmright'>添加链接</a></li>
<li><a href="ljcms_guestbook.php" target='frmright'>留言管理</a></li> 
<li><a href="ljcms_applyjob.php" target='frmright'>简历管理</a></li> 
<li><a href="ljcms_log.php" target='frmright'>系统日志</a></li>
<li><a href="ljcms_tag.php" target='frmright'>Tags列表</a></li>
<li><a href="ljcms_tag.php?action=add" target='frmright'>添加Tags</a></li>
<li><a href="ljcms_database.php" target='frmright'>数据备份</a></li> 
<li><a href="ljcms_database.php?action=restore" target='frmright'>数据恢复</a></li>
<li><a href="register_setting.php?action=setting" target='frmright'>注册信息</a></li>
</UL>
</DIV>
    <DIV class=menu_div><IMG style="VERTICAL-ALIGN: bottom" 
  src="liangjingcms/images/menu01_right.gif"></DIV></LI>
  
 <LI id=menu_6 onmouseover=Menus.Show(this,0) onclick=getleftbar(this);><a href="#">模版管理</a> 
  <DIV class=menu_childs onmouseout=Menus.Hide(0);>
  <UL>					
<li><a href="ljcms_skin.php" target='frmright'>风格列表</a></li>
<li><a href="ljcms_skin.php?action=add" target='frmright'>添加风格</a></li>
<li><a href="ljcms_template.php" target='frmright'>模板文件</a></li>
</UL></DIV>
   <DIV class=menu_div><IMG style="VERTICAL-ALIGN: bottom" 
  src="liangjingcms/images/menu01_right.gif"></DIV></LI>  
  
  
   <LI id=menu_10 onmouseover=Menus.Show(this,0) onclick=getleftbar(this);><a href="#">功能扩展</a>
  <DIV class=menu_childs onmouseout=Menus.Hide(0);>
  <UL>
<li><a href="ljcms_delimitlabel.php" target='frmright'>HTML标签</a></li>
<li><a href="ljcms_delimitlabel.php?action=add" target='frmright'>添加标签</a></li>
<li><a href="ljcms_jobcate.php" target='frmright'>招聘分类</a></li>
<li><a href="ljcms_job.php" target='frmright'>招聘内容</a></li>
<li><a href="ljcms_adszone.php" target='frmright'>广告标签</a></li>
<li><a href="ljcms_adszone.php?action=add" target='frmright'>添加标签</a></li>
<li><a href="ljcms_ads.php" target='frmright'>广告图片</a></li> 
<li><a href="ljcms_ads.php?action=add" target='frmright'>添加图片</a></li>
<li><a href="ljcms_link.php" target='frmright'>链接列表</a></li> 
<li><a href="ljcms_link.php?action=add" target='frmright'>添加链接</a></li>
<li><a href="ljcms_guestbook.php" target='frmright'>留言管理</a></li> 
<li><a href="ljcms_applyjob.php" target='frmright'>简历管理</a></li> 
<li><a href="ljcms_log.php" target='frmright'>系统日志</a></li>
<li><a href="ljcms_tag.php" target='frmright'>Tags列表</a></li>
<li><a href="ljcms_tag.php?action=add" target='frmright'>添加Tags</a></li>
<li><a href="ljcms_database.php" target='frmright'>数据备份</a></li> 
<li><a href="ljcms_database.php?action=restore" target='frmright'>数据恢复</a></li>
</UL>
</DIV>
    <DIV class=menu_div><IMG style="VERTICAL-ALIGN: bottom" 
  src="liangjingcms/images/menu01_right.gif"></DIV></LI>
  
  
  
  
    <LI id=menu_7 onmouseover=Menus.Show(this,0) onclick=getleftbar(this);><a href="#">手机版系统设置</a>
  <DIV class=menu_childs onmouseout=Menus.Hide(0);>
  <UL>
     <!--<li><a href="ljcms_applyjobaaa.php" target='frmright'>手机版系统设置</a></li>-->
	 <li><a href="ljcms_mobile_navigation.php" target='frmright'>手机版导航</a></li>
     <li><a href="ljcms_mobile_huandengpian.php" target='frmright'>手机版幻灯</a></li>
     <!--<li><a href="ljcms_applyjobddd.php" target='frmright'>手机版模板</a></LI>-->
</UL>
</DIV>
 <DIV class=menu_div><IMG style="VERTICAL-ALIGN: bottom" 
  src="liangjingcms/images/menu01_right.gif">
</DIV>
</LI>
  
  
  
  
  </UL></DIV></DIV></DIV>
<DIV style="BACKGROUND: #337abb; HEIGHT: 24px"></DIV>
<!--Nvation End-->
<TABLE style="BACKGROUND: #337abb" height="92%" cellSpacing=0 cellPadding=0 
width="100%" border=0>
  <TBODY>
  <TR>
    <TD class=main_left id=frmTitle vAlign=top align=middle name="fmTitle">
      <TABLE class=main_left_top cellSpacing=0 cellPadding=0 width="100%" 
      border=0>
        <TBODY>
        <TR height=32>
          <TD vAlign=top></TD>
          <TD class=main_left_title id=leftmenu_title>常用快捷功能</TD>
          <TD vAlign=top align=right></TD></TR></TBODY></TABLE><IFRAME 
      class=left_iframe id=frmleft name=frmleft src="admincp.php?mod=left" 
      frameBorder=0 allowTransparency scrolling="no"></IFRAME>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR height=32>
          <TD vAlign=top></TD>
          <TD vAlign=bottom align=middle></TD>
          <TD vAlign=top align=right></TD></TR></TBODY></TABLE></TD>
    <TD style="WIDTH: 10px" bgColor=>
      <TABLE height="100%" cellSpacing=0 cellPadding=0 border=0>
        <TBODY>
        <TR>
          <TD style="HEIGHT: 100%" onclick=switchSysBar()><SPAN class=navPoint 
            id=switchPoint title="test"><IMG 
            src="liangjingcms/images/right.gif"></SPAN> </TD></TR></TBODY></TABLE></TD>
    <TD vAlign=top width="100%" bgColor=>
      <TABLE cellSpacing="0" cellPadding="0" width="100%" bgColor="#337abb" border="0"><TBODY>
        <TR height=32>
          <TD vAlign=top width=10 background=liangjingcms/images/bg2.gif><IMG 
            alt="" src="liangjingcms/images/teble_top_left.gif"></TD>
          <TD width=28 background="liangjingcms/images/bg2.gif"></TD>
          <TD background="liangjingcms/images/bg2.gif"><SPAN 
            style="FLOAT: left">良精企业网站管理系统</SPAN><SPAN id=dvbbsannounce 
            style="FONT-WEIGHT: bold; FLOAT: left; WIDTH: 300px; COLOR: #c00"></SPAN></TD>
          <TD style="COLOR: #135294; TEXT-ALIGN: right" background="liangjingcms/images/bg2.gif">    
	 <p>欢迎回来：<?php echo $_smarty_tpl->getVariable('uc_adminname')->value;?>
 | <A href="admincp.php" target=_top>后台首页</A> | <a href="../index.php" target="_blank">网站首页</a> 
	 | <a href="logout.php" target="_top">退出登录</a> 
	    </TD>
          <TD vAlign=top align=right width=28  background="liangjingcms/images/bg2.gif"><IMG alt="" src="liangjingcms/images/teble_top_right.gif"></TD>
          <TD align=right width=16 bgColor=></TD></TR></TBODY></TABLE>
	<IFRAME class=main_iframe id=frmright name=frmright src="admincp.php?mod=main" frameBorder=0 scrolling=yes></IFRAME>
      <TABLE style="BACKGROUND:#cfe4e9" cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR>
          <TD><IMG height=6 alt="" src="liangjingcms/images/teble_bottom_left.gif"  width=5></TD>
          <TD align=right><IMG height=6 alt=""  src="liangjingcms/images/teble_bottom_right.gif" width=5></TD>
          <TD align=right width=16  bgColor="#337abb"></TD>
		  </TR>
	 </TBODY>
	</TABLE>
</TD></TR></TBODY></TABLE>
<DIV id=dvbbsannounce_true style="DISPLAY: none">
</DIV>
<SCRIPT language=JavaScript>
<!--
document.getElementById("dvbbsannounce").innerHTML = document.getElementById("dvbbsannounce_true").innerHTML;
//-->
</SCRIPT>
</BODY></HTML>