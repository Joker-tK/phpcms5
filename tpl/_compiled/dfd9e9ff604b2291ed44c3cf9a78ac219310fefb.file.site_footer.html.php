<?php /* Smarty version Smarty-3.0.5, created on 2015-11-21 23:48:21
         compiled from "D:\WWW\phpcms8\tpl/xiaomi/site_footer.html" */ ?>
<?php /*%%SmartyHeaderCode:1787256509245268056-65172626%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'dfd9e9ff604b2291ed44c3cf9a78ac219310fefb' => 
    array (
      0 => 'D:\\WWW\\phpcms8\\tpl/xiaomi/site_footer.html',
      1 => 1446087773,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1787256509245268056-65172626',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="site-footer">
    <div class="container">
        <div class="footer-service">
                <ul class="list-service clearfix">
                                <li><a rel="nofollow" href="#/service/exchange#repaire" target="_blank" data-stat-id="6b165acae0a512cf" onclick="_msq.push(['trackEvent', '3b7fd396c60a921e-6b165acae0a512cf', '#/service/exchange#repaire', 'pcpid']);"><i class="iconfont"></i>1小时快修服务</a></li>
                                <li><a rel="nofollow" href="#/service/exchange#back" target="_blank" data-stat-id="4bcdbd7b49855b21" onclick="_msq.push(['trackEvent', '3b7fd396c60a921e-4bcdbd7b49855b21', '#/service/exchange#back', 'pcpid']);"><i class="iconfont"></i>7天无理由退货</a></li>
                                <li><a rel="nofollow" href="#/service/exchange#free" target="_blank" data-stat-id="e801808d1b971db5" onclick="_msq.push(['trackEvent', '3b7fd396c60a921e-e801808d1b971db5', '#/service/exchange#free', 'pcpid']);"><i class="iconfont"></i>15天免费换货</a></li>
                                <li><a rel="nofollow" href="#/service/exchange#mail" target="_blank" data-stat-id="55d6bb7bcb37832c" onclick="_msq.push(['trackEvent', '3b7fd396c60a921e-55d6bb7bcb37832c', '#/service/exchange#mail', 'pcpid']);"><i class="iconfont"></i>满150元包邮</a></li>
                                <li><a rel="nofollow" href="#/c/service/poststation/" target="_blank" data-stat-id="eb10a3b1b392bacf" onclick="_msq.push(['trackEvent', '3b7fd396c60a921e-eb10a3b1b392bacf', '#/c/service/poststation/', 'pcpid']);"><i class="iconfont"></i>520余家售后网点</a></li>
                            </ul>
            </div>
        <div class="footer-links clearfix">
            <dl class="col-links col-links-first">
                <dt>帮助中心</dt>
                <dd>
                    <a rel="nofollow" href="#/service/help_center/guide/" target="_blank" data-stat-id="e20a1843bdcc6d50" onclick="_msq.push([&#39;trackEvent&#39;, &#39;03f01b3cc5c53194-e20a1843bdcc6d50&#39;, &#39;#/service/help_center/guide/&#39;, &#39;pcpid&#39;]);">购物指南</a>
                </dd>
                <dd>
                    <a rel="nofollow" href="#/service/help_center/pay/" target="_blank" data-stat-id="cbc83e341928cad2" onclick="_msq.push([&#39;trackEvent&#39;, &#39;03f01b3cc5c53194-cbc83e341928cad2&#39;, &#39;#/service/help_center/pay/&#39;, &#39;pcpid&#39;]);">支付方式</a>
                </dd>
                <dd><a rel="nofollow" href="#/service/help_center/delivery/" target="_blank" data-stat-id="961c92c727284046" onclick="_msq.push([&#39;trackEvent&#39;, &#39;03f01b3cc5c53194-961c92c727284046&#39;, &#39;#/service/help_center/delivery/&#39;, &#39;pcpid&#39;]);">配送方式</a>
                </dd>
            </dl>
                    
            <dl class="col-links ">
            <dt>服务支持</dt>
                <dd>
                    <a rel="nofollow" href="#/service/exchange" target="_blank" data-stat-id="08819f19ea21f3e4" onclick="_msq.push([&#39;trackEvent&#39;, &#39;03f01b3cc5c53194-08819f19ea21f3e4&#39;, &#39;#/service/exchange&#39;, &#39;pcpid&#39;]);">售后政策</a>
                </dd>
                
                <dd>
                    <a rel="nofollow" href="http://fuwu.mi.com/" target="_blank" data-stat-id="5f2408ab3c808aa2" onclick="_msq.push([&#39;trackEvent&#39;, &#39;03f01b3cc5c53194-5f2408ab3c808aa2&#39;, &#39;http://fuwu.mi.com/&#39;, &#39;pcpid&#39;]);">自助服务</a>
                </dd>
                
                <dd>
                    <a rel="nofollow" href="http://xiazai.mi.com/" target="_blank" data-stat-id="18c340c920a278a1" onclick="_msq.push([&#39;trackEvent&#39;, &#39;03f01b3cc5c53194-18c340c920a278a1&#39;, &#39;http://xiazai.mi.com/&#39;, &#39;pcpid&#39;]);">相关下载</a>
                </dd>
            </dl>
                
            <dl class="col-links ">
                <dt>企行之家</dt>
                
                <dd><a rel="nofollow" href="#/c/xiaomizhijia/" target="_blank" data-stat-id="078fe5e483ca402c" onclick="_msq.push([&#39;trackEvent&#39;, &#39;03f01b3cc5c53194-078fe5e483ca402c&#39;, &#39;#/c/xiaomizhijia/&#39;, &#39;pcpid&#39;]);">企行之家</a></dd>
                
                <dd><a rel="nofollow" href="#/c/service/poststation/" target="_blank" data-stat-id="6b28dd256c6ca580" onclick="_msq.push([&#39;trackEvent&#39;, &#39;03f01b3cc5c53194-6b28dd256c6ca580&#39;, &#39;#/c/service/poststation/&#39;, &#39;pcpid&#39;]);">服务网点</a></dd>
                
                <dd><a rel="nofollow" href="http://service.order.mi.com/mihome/index" target="_blank" data-stat-id="ab58b266b5b52950" onclick="_msq.push([&#39;trackEvent&#39;, &#39;03f01b3cc5c53194-ab58b266b5b52950&#39;, &#39;http://service.order.mi.com/mihome/index&#39;, &#39;pcpid&#39;]);">预约亲临到店服务</a></dd>
                
            </dl>
                
            <dl class="col-links ">
                <dt>关于企行</dt>
                
                <dd><a rel="nofollow" href="#/about" target="_blank" data-stat-id="e7dd29b3633d3b9d" onclick="_msq.push([&#39;trackEvent&#39;, &#39;03f01b3cc5c53194-e7dd29b3633d3b9d&#39;, &#39;#/about&#39;, &#39;pcpid&#39;]);">了解企行</a></dd>
                
                <dd><a rel="nofollow" href="http://hr.xiaomi.com/" target="_blank" data-stat-id="4307a445f5592adb" onclick="_msq.push([&#39;trackEvent&#39;, &#39;03f01b3cc5c53194-4307a445f5592adb&#39;, &#39;http://hr.xiaomi.com/&#39;, &#39;pcpid&#39;]);">加入企行</a></dd>
                
                <dd><a rel="nofollow" href="#/about/contact" target="_blank" data-stat-id="bd03a61dc2a9251e" onclick="_msq.push([&#39;trackEvent&#39;, &#39;03f01b3cc5c53194-bd03a61dc2a9251e&#39;, &#39;#/about/contact&#39;, &#39;pcpid&#39;]);">联系我们</a></dd>
                
            </dl>
                
            <dl class="col-links ">
                <dt>关注我们</dt>
                
                <dd><a rel="nofollow" href="http://e.weibo.com/xiaomishouji" target="_blank" data-stat-id="464883aa6e799290" onclick="_msq.push([&#39;trackEvent&#39;, &#39;03f01b3cc5c53194-464883aa6e799290&#39;, &#39;http://e.weibo.com/xiaomishouji&#39;, &#39;pcpid&#39;]);">新浪微博</a></dd>
                
                <dd><a rel="nofollow" href="http://xiaoqu.qq.com/mobile/share/index.html?url=http%3A%2F%2Fxiaoqu.qq.com%2Fmobile%2Fbarindex.html%3Fwebview%3D1%26type%3D%26source%3Dindex%26_lv%3D25741%26sid%3D%26_wv%3D5123%26_bid%3D128%26%23bid%3D10525%26from%3Dwechat" target="_blank" data-stat-id="78fdefa9dee561b5" onclick="_msq.push([&#39;trackEvent&#39;, &#39;03f01b3cc5c53194-78fdefa9dee561b5&#39;, &#39;http://xiaoqu.qq.com/mobile/share/index.html?url=http%3A%2F%2Fxiaoqu.qq.com%2Fmobile%2Fbarindex.html%3Fwebview%3D1%26type%3D%26source%3Dindex%26_lv%3D25741%26sid%3D%26_wv%3D5123%26_bid%3D128%26%23bid%3D10525%26from%3Dwechat&#39;, &#39;pcpid&#39;]);">企行部落</a></dd>
                
                <dd><a rel="nofollow" href="#/air/#J_modalWeixin" data-toggle="modal" data-stat-id="47539f6570f0da90" onclick="_msq.push([&#39;trackEvent&#39;, &#39;03f01b3cc5c53194-47539f6570f0da90&#39;, &#39;#J_modalWeixin&#39;, &#39;pcpid&#39;]);">官方微信</a></dd>
                
            </dl>
                
            <div class="col-contact">
                <p class="phone">400-100-5678</p>
<p><span class="J_serviceTime-normal" style="
">周一至周日 8:00-18:00</span>
<span class="J_serviceTime-holiday" style="display:none;">10月1日至7日服务时间 9:00-18:00</span><br>（仅收市话费）</p>
<a rel="nofollow" class="btn btn-line-primary btn-small" href="#/service/contact" target="_blank" data-stat-id="e71bbbe7c3afda0f" onclick="_msq.push([&#39;trackEvent&#39;, &#39;03f01b3cc5c53194-e71bbbe7c3afda0f&#39;, &#39;#/service/contact&#39;, &#39;pcpid&#39;]);"><i class="iconfont"></i> 24小时在线客服</a>            </div>
        </div>
    </div>
</div>
<div class="site-info">
    <div class="container">
        <div class="logo ir">企行官网</div>
        <div class="info-text">
            <p>©<a href="http://www.qihang.com/" target="_blank" title="mi.com" data-stat-id="d7577c2c6d9aff60" onclick="_msq.push([&#39;trackEvent&#39;, &#39;03f01b3cc5c53194-d7577c2c6d9aff60&#39;, &#39;#/&#39;, &#39;pcpid&#39;]);">qihang.com</a> 京ICP证110507号 京ICP备10046444号 京公网安备1101080212535号 <a href="http://c1.mifile.cn/f/i/2013/cn/jingwangwen.jpg" target="_blank" data-stat-id="fc34c079ca0f8423" onclick="_msq.push([&#39;trackEvent&#39;, &#39;03f01b3cc5c53194-fc34c079ca0f8423&#39;, &#39;http://c1.mifile.cn/f/i/2013/cn/jingwangwen.jpg&#39;, &#39;pcpid&#39;]);">京网文[2014]0059-0009号</a></p>
        </div>
        <div class="info-links">
                    <a href="https://search.szfw.org/cert/l/CX20120926001783002010" target="_blank" data-stat-id="9e1604cd6612205c" onclick="_msq.push([&#39;trackEvent&#39;, &#39;03f01b3cc5c53194-9e1604cd6612205c&#39;, &#39;<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
images/cx.png&#39;, &#39;pcpid&#39;]);"><img src="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
images/v-logo-2.png" alt="诚信网站"></a>

                    <a href="https://ss.knet.cn/verifyseal.dll?sn=e12033011010015771301369&ct=df&pa=461082" target="_blank" data-stat-id="3e1533699f264eac" onclick="_msq.push([&#39;trackEvent&#39;, &#39;03f01b3cc5c53194-3e1533699f264eac&#39;, &#39;https://ss.knet.cn/verifyseal.dll?sn=e12033011010015771301369&amp;ct=df&amp;pa=461082&#39;, &#39;pcpid&#39;]);"><img src="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
images/v-logo-1.png" alt="可信网站"></a>

                    <a href="http://www.315online.com.cn/member/315140007.html" target="_blank" data-stat-id="b085e50c7ec83104" onclick="_msq.push([&#39;trackEvent&#39;, &#39;03f01b3cc5c53194-b085e50c7ec83104&#39;, &#39;http://www.315online.com.cn/member/315140007.html&#39;, &#39;pcpid&#39;]);"><img src="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
images/v-logo-3.png" alt="网上交易保障中心"></a>
                </div>
    </div>
</div>
<div id="J_modalWeixin" class="modal fade modal-hide modal-weixin" data-width="480" data-height="520">
        <div class="modal-hd">
            <a class="close" data-dismiss="modal" data-stat-id="cfd3189b8a874ba4" onclick="_msq.push([&#39;trackEvent&#39;, &#39;03f01b3cc5c53194-cfd3189b8a874ba4&#39;, &#39;&#39;, &#39;pcpid&#39;]);"><i class="iconfont"></i></a>
            <h3>企行官方微信二维码</h3>
        </div>
        <div class="modal-bd">
            <p style="margin: 0 0 10px;">打开微信，点击右上角的“+”，选择“扫一扫”功能，<br>对准下方二维码即可。</p>
            <img alt="" src="./企行空气净化器 － 企行手机官网_files/qr.png" width="375" height="375">
        </div>
    </div>
<!-- .modal-weixin END -->
<div class="modal modal-hide modal-bigtap-queue" id="J_bigtapQueue">
    <div class="modal-body">
        <span class="close" data-dismiss="modal" aria-hidden="true"><i class="iconfont"></i></span>
        <h3>正在排队，请稍候喔！</h3>
        <p class="queue-tip">抱歉，目前排队人数较多，导致服务器压力山大，请您耐心排队等待，<br>我们将在稍后告诉您排队结果。</p>
        <div class="queue-animate">
            <div id="J_mituWalking" class="mitu-walk"> </div>
            <div class="animate-mask animate-mask-left"></div>
            <div class="animate-mask animate-mask-right"></div>
        </div>
    </div>
</div>
<!-- .xm-dm-queue END -->
<div id="J_bigtapError" class="modal modal-hide modal-bigtap-error">
    <span class="close" data-dismiss="modal" aria-hidden="true"><i class="iconfont"></i></span>
    <div class="modal-body">
        <h3>抱歉，网络拥堵无法连接服务器</h3>
        <p class="error-tip">由于访问人数太多导致服务器压力山大，请您稍后再重试。</p>
        <p>
            <a class="btn btn-primary" id="J_bigtapRetry" data-stat-id="c148a4197491d5bd" onclick="_msq.push([&#39;trackEvent&#39;, &#39;03f01b3cc5c53194-c148a4197491d5bd&#39;, &#39;&#39;, &#39;pcpid&#39;]);">重试</a>
        </p>
    </div>
</div>
<!-- .xm-dm-error END -->
<div id="J_modal-globalSites" class="modal fade modal-hide modal-globalSites" data-width="640">
       <div class="modal-hd">
            <a class="close" data-dismiss="modal" data-stat-id="d63900908fde14b1" onclick="_msq.push([&#39;trackEvent&#39;, &#39;03f01b3cc5c53194-d63900908fde14b1&#39;, &#39;&#39;, &#39;pcpid&#39;]);"><i class="iconfont"></i></a>
            <h3>Select Region</h3>
        </div>
        <div class="modal-bd">
            <h3>Welcome to Mi.com</h3>
            <p class="modal-globalSites-tips">Please select your country or region</p>
            <p class="modal-globalSites-links clearfix">
                <a href="#/index.html" data-stat-id="5983038ffc84af95" onclick="_msq.push([&#39;trackEvent&#39;, &#39;03f01b3cc5c53194-5983038ffc84af95&#39;, &#39;#/index.html&#39;, &#39;pcpid&#39;]);">Mainland China</a>
                <a href="#/hk/" data-stat-id="d8e4264197de1747" onclick="_msq.push([&#39;trackEvent&#39;, &#39;03f01b3cc5c53194-d8e4264197de1747&#39;, &#39;#/hk/&#39;, &#39;pcpid&#39;]);">Hong Kong</a>
                <a href="#/tw/" data-stat-id="e7bd0f4e372c0b29" onclick="_msq.push([&#39;trackEvent&#39;, &#39;03f01b3cc5c53194-e7bd0f4e372c0b29&#39;, &#39;#/tw/&#39;, &#39;pcpid&#39;]);">TaiWan</a>
                <a href="#/sg/" data-stat-id="e9c0506f7e4e7161" onclick="_msq.push([&#39;trackEvent&#39;, &#39;03f01b3cc5c53194-e9c0506f7e4e7161&#39;, &#39;#/sg/&#39;, &#39;pcpid&#39;]);">Singapore</a>
                <a href="#/my/" data-stat-id="d6299ad30ec761a8" onclick="_msq.push([&#39;trackEvent&#39;, &#39;03f01b3cc5c53194-d6299ad30ec761a8&#39;, &#39;#/my/&#39;, &#39;pcpid&#39;]);">Malaysia</a>
                <a href="#/ph/" data-stat-id="22b601cf7b3ada84" onclick="_msq.push([&#39;trackEvent&#39;, &#39;03f01b3cc5c53194-22b601cf7b3ada84&#39;, &#39;#/ph/&#39;, &#39;pcpid&#39;]);">Philippines</a>
                <a href="#/in/" data-stat-id="441d26d4571e10dc" onclick="_msq.push([&#39;trackEvent&#39;, &#39;03f01b3cc5c53194-441d26d4571e10dc&#39;, &#39;#/in/&#39;, &#39;pcpid&#39;]);">India</a>
                <a href="#/id/" data-stat-id="88ccf9755c488ec5" onclick="_msq.push([&#39;trackEvent&#39;, &#39;03f01b3cc5c53194-88ccf9755c488ec5&#39;, &#39;#/id/&#39;, &#39;pcpid&#39;]);">Indonesia</a>
                <a href="http://br.mi.com/" data-stat-id="c41d871bf5ddcd95" onclick="_msq.push([&#39;trackEvent&#39;, &#39;03f01b3cc5c53194-c41d871bf5ddcd95&#39;, &#39;http://br.mi.com/&#39;, &#39;pcpid&#39;]);">Brasil</a>
                <a href="#/en/" data-stat-id="4426c5dac474df5f" onclick="_msq.push([&#39;trackEvent&#39;, &#39;03f01b3cc5c53194-4426c5dac474df5f&#39;, &#39;#/en/&#39;, &#39;pcpid&#39;]);">Global Home</a>
            </p>
        </div>
    </div>
</div>



<!-- 底部js特效 -->
    <script src="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
js/base.min.js"></script>
    <script>
    (function() {
        MI.namespace('GLOBAL_CONFIG');
        MI.GLOBAL_CONFIG = {
            orderSite: 'http://order.mi.com',
            wwwSite: 'http://www.mi.com',
            cartSite: 'http://cart.mi.com',
            itemSite: 'http://item.mi.com',
            assetsSite: 'http://s01.mifile.cn',
            listSite: 'http://list.mi.com',
            searchSite: 'http://search.mi.com',
            mySite: 'http://my.mi.com',
            damiaoSite: 'http://tp.hd.mi.com/',
            damiaoGoodsId:["2141700014","2151300011","1151600007","2152400009"],
            logoutUrl: 'http://order.mi.com/site/logout',
            staticSite: 'http://static.mi.com',
            quickLoginUrl: 'https://account.xiaomi.com/pass/static/login.html'
        };
        MI.setLoginInfo.orderUrl = MI.GLOBAL_CONFIG.orderSite + '/user/order';
        MI.setLoginInfo.logoutUrl = MI.GLOBAL_CONFIG.logoutUrl;
        MI.setLoginInfo.init(MI.GLOBAL_CONFIG);
        MI.miniCart.init();
        MI.updateMiniCart();
    })();
    </script>
    <script src="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
js/home.min.js"></script>
    <script src="<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
js/xmsg_ti.js"></script>
    <script>
    var _msq = _msq || [];
    _msq.push(['setDomainId', 100]);
    _msq.push(['trackPageView']);
    (function() {
        var ms = document.createElement('script');
        ms.type = 'text/javascript';
        ms.async = true;
        ms.src = '<?php echo $_smarty_tpl->getVariable('skinpath')->value;?>
js/xmst.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ms, s);
    })();
    </script>

</body>
</html>