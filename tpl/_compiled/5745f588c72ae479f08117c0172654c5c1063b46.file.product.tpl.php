<?php /* Smarty version Smarty-3.0.5, created on 2015-11-26 10:48:52
         compiled from "D:\WWW\phpcms21\admin/liangjingcms/product.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2184356567314ec2724-45142422%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5745f588c72ae479f08117c0172654c5c1063b46' => 
    array (
      0 => 'D:\\WWW\\phpcms21\\admin/liangjingcms/product.tpl',
      1 => 1448505937,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2184356567314ec2724-45142422',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_date_format')) include 'D:\WWW\phpcms21\source\core\plugins\modifier.date_format.php';
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $_smarty_tpl->getVariable('page_charset')->value;?>
" />
<title>产品内容</title>
<meta name="author" content="<?php echo $_smarty_tpl->getVariable('copyright_author')->value;?>
" />
<link type="text/css" rel="stylesheet" href="liangjingcms/css/admin_style.css" media="screen" />
<script type='text/javascript' src='js/jquery-1.4.4.min.js'></script>
<script type='text/javascript' src='js/command.js'></script>
<script type="text/javascript" src='../data/editor/kindeditor.js'></script>
<script type="text/javascript" src="js/jquery.date_input.js"></script>
<link rel="stylesheet" href="liangjingcms/css/date_input.css" type="text/css">
<script type="text/javascript">$($.date_input.initialize);</script>
</head>
<body>
<table class="tableBorder" width="95%" border="0" align="center" cellpadding="5" cellspacing="1">
<tr>
<td width="100%" class="leftrow">
<?php if ($_smarty_tpl->getVariable('action')->value==''){?>
<div class="main-wrap">
  <div class="path"><p>当前位置：产品管理<span>&gt;&gt;</span>产品展示</p></div>
  <div class="main-cont">
    <h3 class="title"><a href="ljcms_product.php?action=add" class="btn-general"><span>发布产品</span></a>产品展示</h3>
	<div class="search-area ">
	  <form method="post" id="search_form" action="ljcms_product.php" />
	  <div class="item">
	    <label>产品分类：</label><?php echo $_smarty_tpl->getVariable('cate_search')->value;?>
&nbsp;&nbsp;
		<label>标题：</label><input type="text" id="sname" name="sname" size="20" class="input" value="<?php echo $_smarty_tpl->getVariable('sname')->value;?>
" />&nbsp;&nbsp;&nbsp;
		<input type="submit" class="button_s" value="搜 索" />
	  </div>
	  </form>
	</div>
	<form action="ljcms_product.php" method="post" name="myform" id="myform" style="margin:0">
	<input type="hidden" name="action" id="action" value="del" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <thead class="tb-tit-bg">
	  <tr>
	    <th width="8%"><div class="th-gap">选择</div></th>
		<th width="12%"><div class="th-gap">所在分类</div></th>
		<th width="12%"><div class="th-gap">预览图</div></th>
		<th width="20%"><div class="th-gap">产品名称</div></th>
		<th width="8%"><div class="th-gap">浏览</div></th>
		<th width="7%"><div class="th-gap">状态</div></th>
		<th width="7%"><div class="th-gap">推荐</div></th>
		<th width="10%"><div class="th-gap">录入时间</div></th>
		<th><div class="th-gap">操作</div></th>
	  </tr>
	  </thead>
	  <tfoot class="tb-foot-bg"></tfoot>
	  <?php  $_smarty_tpl->tpl_vars['volist'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('product')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['volist']->key => $_smarty_tpl->tpl_vars['volist']->value){
?>
	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">
	    <td align="center"><input name="id[]" type="checkbox" value="<?php echo $_smarty_tpl->tpl_vars['volist']->value['productid'];?>
" onClick="checkItem(this, 'chkAll')"><?php echo $_smarty_tpl->tpl_vars['volist']->value['productid'];?>
</td>
		<td align="center"><?php echo $_smarty_tpl->tpl_vars['volist']->value['catename'];?>
</td>
		<td align="center">
		<?php if ($_smarty_tpl->tpl_vars['volist']->value['thumbfiles']!=''){?>
		<a href="../<?php echo $_smarty_tpl->tpl_vars['volist']->value['uploadfiles'];?>
" target="_blank"><img src="../<?php echo $_smarty_tpl->tpl_vars['volist']->value['thumbfiles'];?>
" width="100" height="100" border="0" /></a>
		<?php }else{ ?>
		<img src="../tpl/static/images/s_nopic.jpg" width="100" height="100" border="0" />
		<?php }?>
        </td>
		<td align="left"><a href="../product.php?mod=detail&id=<?php echo $_smarty_tpl->tpl_vars['volist']->value['productid'];?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['volist']->value['productname'];?>
</a></td>
		<td align="center"><?php echo $_smarty_tpl->tpl_vars['volist']->value['hits'];?>
</td>
		<td align="center">
		<?php if ($_smarty_tpl->tpl_vars['volist']->value['flag']==0){?>
			<input type="hidden" id="attr_flag<?php echo $_smarty_tpl->tpl_vars['volist']->value['productid'];?>
" value="flagopen" />
			<img id="flag<?php echo $_smarty_tpl->tpl_vars['volist']->value['productid'];?>
" src="liangjingcms/images/no.gif" onClick="javascript:fetch_ajax('flag','<?php echo $_smarty_tpl->tpl_vars['volist']->value['productid'];?>
');" style="cursor:pointer;">
		<?php }else{ ?>
			<input type="hidden" id="attr_flag<?php echo $_smarty_tpl->tpl_vars['volist']->value['productid'];?>
" value="flagclose" />
			<img id="flag<?php echo $_smarty_tpl->tpl_vars['volist']->value['productid'];?>
" src="liangjingcms/images/yes.gif" onClick="javascript:fetch_ajax('flag','<?php echo $_smarty_tpl->tpl_vars['volist']->value['productid'];?>
');" style="cursor:pointer;">	
		<?php }?>
        </td>
		<td align="center">
		<?php if ($_smarty_tpl->tpl_vars['volist']->value['elite']==0){?>
			<input type="hidden" id="attr_elite<?php echo $_smarty_tpl->tpl_vars['volist']->value['productid'];?>
" value="eliteopen" />
			<img id="elite<?php echo $_smarty_tpl->tpl_vars['volist']->value['productid'];?>
" src="liangjingcms/images/no.gif" onClick="javascript:fetch_ajax('elite','<?php echo $_smarty_tpl->tpl_vars['volist']->value['productid'];?>
');" style="cursor:pointer;">
		<?php }else{ ?>
			<input type="hidden" id="attr_elite<?php echo $_smarty_tpl->tpl_vars['volist']->value['productid'];?>
" value="eliteclose" />
			<img id="elite<?php echo $_smarty_tpl->tpl_vars['volist']->value['productid'];?>
" src="liangjingcms/images/yes.gif" onClick="javascript:fetch_ajax('elite','<?php echo $_smarty_tpl->tpl_vars['volist']->value['productid'];?>
');" style="cursor:pointer;">	
		<?php }?>
        </td>
		<td align="center"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['volist']->value['timeline'],"%Y/%m/%d");?>
</td>
		<td align="center"><a href="ljcms_product.php?action=edit&id=<?php echo $_smarty_tpl->tpl_vars['volist']->value['productid'];?>
&page=<?php echo $_smarty_tpl->getVariable('page')->value;?>
&<?php echo $_smarty_tpl->getVariable('urlitem')->value;?>
" class="icon-edit">编辑</a>&nbsp;&nbsp;<a href="ljcms_product.php?action=del&id[]=<?php echo $_smarty_tpl->tpl_vars['volist']->value['productid'];?>
" onClick="{if(confirm('确定要删除该信息?')){return true;} return false;}" class="icon-del">删除</a></td>
	  </tr>
	  <?php }} else { ?>
      <tr>
	    <td colspan="9" align="center">暂无信息</td>
	  </tr>
	  <?php } ?>
	  <?php if ($_smarty_tpl->getVariable('total')->value>0){?>
	  <tr>
		<td align="center"><input name="chkAll" type="checkbox" id="chkAll" onClick="checkAll(this, 'id[]')" value="checkbox"></td>
		<td class="hback" colspan="8"><input class="button" name="btn_del" type="button" value="删 除" onClick="{if(confirm('确定删除选定信息吗!?')){$('#action').val('del');$('#myform').submit();return true;}return false;}" class="button">&nbsp;&nbsp;共[ <b><?php echo $_smarty_tpl->getVariable('total')->value;?>
</b> ]条记录</td>
	  </tr>
	  <?php }?>
	</table>
	</form>
	<?php if ($_smarty_tpl->getVariable('pagecount')->value>1){?>
	<table width='95%' border='0' cellspacing='0' cellpadding='0' align='center' style="margin-top:10px;">
	  <tr>
		<td align='center'><?php echo $_smarty_tpl->getVariable('showpage')->value;?>
</td>
	  </tr>
	</table>
	<?php }?>
  </div>
</div>
<?php }?>

<?php if ($_smarty_tpl->getVariable('action')->value=="setting"){?>
<div class="main-wrap">
  <div class="path"><p>当前位置：内容管理<span>&gt;&gt;</span>设置产品内容</p></div>
  <div class="main-cont">
    <h3 class="title"><a href="ljcms_product.php?action=add" class="btn-general"><span>发布产品</span></a>设置产品内容</h3>
	<form action="ljcms_product.php" method="post" name="myform" id="myform" style="margin:0">
	<input type="hidden" name="action" id="action" value="savesetting" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <thead class="tb-tit-bg">
	  <tr>
	    <th width="8%"><div class="th-gap">选择</div></th>
		<th width="20%"><div class="th-gap">标题</div></th>
		<th width="20%"><div class="th-gap">Meta标题</div></th>
		<th width="20%"><div class="th-gap">Meta关键字</div></th>
		<th width="20%"><div class="th-gap">Meta描述</div></th>
	  </tr>
	  </thead>
	  <tfoot class="tb-foot-bg"></tfoot>
	  <?php  $_smarty_tpl->tpl_vars['volist'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('product')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['volist']->key => $_smarty_tpl->tpl_vars['volist']->value){
?>
	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">
	    <td align="center"><input name="id[]" type="checkbox" value="<?php echo $_smarty_tpl->tpl_vars['volist']->value['productid'];?>
" onClick="checkItem(this, 'chkAll')"><?php echo $_smarty_tpl->tpl_vars['volist']->value['productid'];?>
</td>
		<td align="center"><input type="text" name="productname_<?php echo $_smarty_tpl->tpl_vars['volist']->value['productid'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['volist']->value['productname'];?>
" size="22" /></td>
		<td align="center"><input type="text" name="metatitle_<?php echo $_smarty_tpl->tpl_vars['volist']->value['productid'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['volist']->value['metatitle'];?>
" size="20" /></td>
		<td align="center"><input type="text" name="metakeyword_<?php echo $_smarty_tpl->tpl_vars['volist']->value['productid'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['volist']->value['metakeyword'];?>
" size="20" /></td>
		<td align="center"><input type="text" name="metadescription_<?php echo $_smarty_tpl->tpl_vars['volist']->value['productid'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['volist']->value['metadescription'];?>
" size="20" /></td>
	  </tr>
	  <?php }} else { ?>
      <tr>
	    <td colspan="5" align="center">暂无信息</td>
	  </tr>
	  <?php } ?>
	  <?php if ($_smarty_tpl->getVariable('total')->value>0){?>
	  <tr>
		<td align="center"><input name="chkAll" type="checkbox" id="chkAll" onClick="checkAll(this, 'id[]')" value="checkbox"></td>
		<td class="hback" colspan="4"><input class="button" name="btn_del" type="button" value="批量更新" onClick="{if(confirm('确定更新选定信息吗!?')){$('#action').val('savesetting');$('#myform').submit();return true;}return false;}" class="button"></td>
	  </tr>
	  <?php }?>
	</table>
	</form>
	<?php if ($_smarty_tpl->getVariable('pagecount')->value>1){?>
	<table width='95%' border='0' cellspacing='0' cellpadding='0' align='center' style="margin-top:10px;">
	  <tr>
		<td align='center'><?php echo $_smarty_tpl->getVariable('showpage')->value;?>
</td>
	  </tr>
	</table>
	<?php }?>
  </div>
</div>
<?php }?>


<?php if ($_smarty_tpl->getVariable('action')->value=="add"){?>
<div class="main-wrap">
  <div class="path"><p>当前位置：内容管理<span>&gt;&gt;</span>发布产品</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_product.php" class="btn-general"><span>返回列表</span></a>发布产品</h3>
    <form name="myform" id="myform" method="post" action="ljcms_product.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveadd" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">产品分类 <span class='f_red'></span></td>
		<td class='hback' width="85%"><?php echo $_smarty_tpl->getVariable('cate_select')->value;?>
 <span id="dcateid" class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>产品编号 <span class="f_red"></span> </td>
		<td class='hback'><input type="text" name="productnum" id="productnum" class="input-txt" /> <span id='dproductnum' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>产品名称 <span class="f_red">*</span> </td>
		<td class='hback'><input type="text" name="productname" id="productname" class="input-txt" /> <span id='dproductname' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta标题 </td>
		<td class='hback'><input type="text" name="metatitle" id="metatitle" class="input-txt" />  <span id='dmetatitle' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta关键字 <span class="f_red"></span></td>
		<td class='hback'><textarea name="metakeyword" id="metakeyword" style="width:60%;height:45px;display:;overflow:auto;"></textarea> <span id='dmetakeyword' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta描述 <span class="f_red"></span></td>
		<td class='hback'><textarea name="metadescription" id="metadescription" style="width:60%;height:45px;display:;overflow:auto;"></textarea> <span id='dmetadescription' class='f_red'></span></td>
	  </tr>


	  <tr>
		<td class='hback_1'>产品图片 <span class="f_red"></span> </td>
		<td class='hback'>
		  <input type="hidden" name="uploadfiles" id="uploadfiles" /><input type="hidden" name="thumbfiles" id="thumbfiles" /><span id='duploadfiles' class='f_red'></span>
		  <iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?comeform=myform&inputid=uploadfiles&thumbflag=1&thumbinput=thumbfiles&channel=product&waterflag=<?php echo $_smarty_tpl->getVariable('config')->value['watermarkflag'];?>
'></iframe>
		</td>
	  </tr>
	  <tr>
		<td class='hback_1'>列表页图片 <span class="f_red"></span> </td>
		<td class='hback'>
		  <input type="hidden" name="listpicurl" id="listpicurl" /><input type="hidden" name="lsitthumburl" id="lsitthumburl" /><span id='duploadfiles' class='f_red'></span>
		  <iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?comeform=myform&inputid=listpicurl&thumbflag=1&thumbinput=lsitthumburl&channel=product&waterflag=<?php echo $_smarty_tpl->getVariable('config')->value['watermarkflag'];?>
'></iframe>
		</td>
	  </tr>
	  <tr>
		<td class='hback_1'>内容页图片<span class="f_red"></span> </td>
		<td class='hback'>
		  <input type="hidden" name="img1" id="img1" /><input type="hidden" name="img1thumb" id="img1thumb" /><input type="hidden" name="img1thumb2" id="img1thumb2" /><span id='duploadfiles' class='f_red'></span>
		  <iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?comeform=myform&inputid=img1&thumbflag=1&thumbinput=img1thumb&thumbflag2=2&channel=product&waterflag=<?php echo $_smarty_tpl->getVariable('config')->value['watermarkflag'];?>
'></iframe>
		</td>
	  </tr>
	  <tr>
		<td class='hback_1'>产品原价 <span class="f_red"></span> </td>
		<td class='hback'><input type="text" name="nprice" id="nprice" class="input-s" /> （填写数字，单位为人民币￥） <span id='dnprice' class='f_red'></span></td>
	  </tr>


	  <tr>
		<td class='hback_1'>产品价格 <span class="f_red"></span> </td>
		<td class='hback'><input type="text" name="price" id="price" class="input-s" /> （填写数字，单位为人民币￥） <span id='dprice' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>浏览次数 <span class="f_red"></span> </td>
		<td class='hback'><input type="text" name="hits" id="hits" class="input-s" /> <span id='dhits' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>TAGS </td>
		<td class='hback'><input type="text" name="tag" id="tag" class="input-txt" />  <span id='dtag' class='f_red'></span> (多个请用英文,号隔开) <a href="ljcms_tag.php">管理Tags链接</a></td>
	  </tr>
	  <tr>
		<td class='hback_1'>状态设置 </td>
		<td class='hback'><?php echo $_smarty_tpl->getVariable('flag_checkbox')->value;?>
，<?php echo $_smarty_tpl->getVariable('elite_checkbox')->value;?>
，<?php echo $_smarty_tpl->getVariable('isnew_checkbox')->value;?>
，<?php echo $_smarty_tpl->getVariable('hots_checkbox')->value;?>
，<?php echo $_smarty_tpl->getVariable('ismiao_checkbox')->value;?>

		
		</td>
	  </tr>
	  <tr>
		<td class='hback_1'>秒杀截至日期 </td>
		<td class='hback'><input type="text" name="miaotime" id="miaotime" class="date_input" readonly/>  <span id='dmiaotime' class='f_red'></span> </td>
	  </tr>
          <tr>
	<td class='hback_1'>浏览权限</td>
	<td class='hback'>
       <?php echo $_smarty_tpl->getVariable('ugroupid_select')->value;?>


        <input name="exclusive" type="radio" value="&gt;=" checked>
        隶属
        <input name="exclusive"  type="radio"  value="=">
        专属（隶属：权限值≥可查看，专属：权限值＝可查看）	
		</td>
	  </tr>
	  <tr>
		<td class='hback_1'>产品简介  </td>
		<td class='hback'><textarea name="intro" id="intro" style='width:60%;height:65px;overflow:auto;color:#444444;'></textarea></td>
	  </tr>
	  <tr>
		<td class='hback_1'>详细介绍 <span class="f_red">*</span></td>
		<td class='hback'>
		  <textarea name="content" id="content" style="width:99%;height:300px;display:none;"></textarea>
		  <script>KE.show({id : 'content' });</script>  <span id='dcontent' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="添加保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<?php }?>

<?php if ($_smarty_tpl->getVariable('action')->value=="edit"){?>
<div class="main-wrap">
  <div class="path"><p>当前位置：内容管理<span>&gt;&gt;</span>编辑产品</p></div>
  <div class="main-cont">
	<h3 class="title"><a href="ljcms_product.php?<?php echo $_smarty_tpl->getVariable('comeurl')->value;?>
" class="btn-general"><span>返回列表</span></a>编辑产品</h3>
    <form name="myform" id="myform" method="post" action="ljcms_product.php" onsubmit='return checkform();' />
    <input type="hidden" name="action" value="saveedit" />
	<input type="hidden" name="id" value="<?php echo $_smarty_tpl->getVariable('id')->value;?>
" />
	<table cellpadding='3' cellspacing='3' class='tab'>
	  <tr>
		<td class='hback_1' width="15%">产品分类 <span class='f_red'></span></td>
		<td class='hback' width="85%"><?php echo $_smarty_tpl->getVariable('cate_select')->value;?>
 <span id="dcateid" class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>产品编号 <span class="f_red"></span> </td>
		<td class='hback'><input type="text" name="productnum" id="productnum" class="input-txt" value="<?php echo $_smarty_tpl->getVariable('product')->value['productnum'];?>
" /> <span id='dproductnum' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>产品名称 <span class="f_red">*</span> </td>
		<td class='hback'><input type="text" name="productname" id="productname" class="input-txt" value="<?php echo $_smarty_tpl->getVariable('product')->value['productname'];?>
" /> <span id='dproductname' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta标题 </td>
		<td class='hback'><input type="text" name="metatitle" id="metatitle" class="input-txt" value="<?php echo $_smarty_tpl->getVariable('product')->value['metatitle'];?>
" />  <span id='dmetatitle' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta关键字 <span class="f_red"></span></td>
		<td class='hback'><textarea name="metakeyword" id="metakeyword" style="width:60%;height:45px;display:;overflow:auto;"><?php echo $_smarty_tpl->getVariable('product')->value['metakeyword'];?>
</textarea> <span id='dmetakeyword' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Meta描述 <span class="f_red"></span></td>
		<td class='hback'><textarea name="metadescription" id="metadescription" style="width:60%;height:45px;display:;overflow:auto;"><?php echo $_smarty_tpl->getVariable('product')->value['metadescription'];?>
</textarea> <span id='dmetadescription' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>产品图片 <span class="f_red"></span> </td>
		<td class='hback'>
		  <input type="hidden" name="uploadfiles" id="uploadfiles" value="<?php echo $_smarty_tpl->getVariable('product')->value['uploadfiles'];?>
" /><input type="hidden" name="thumbfiles" id="thumbfiles" value="<?php echo $_smarty_tpl->getVariable('product')->value['thumbfiles'];?>
" /><span id='duploadfiles' class='f_red'></span>
		    <?php if ($_smarty_tpl->getVariable('product')->value['thumbfiles']!=''){?>
			<iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?action=show&comeform=myform&inputid=uploadfiles&thumbflag=1&thumbinput=thumbfiles&channel=product&waterflag=<?php echo $_smarty_tpl->getVariable('config')->value['watermarkflag'];?>
&picname=<?php echo $_smarty_tpl->getVariable('product')->value['uploadpicname'];?>
&picurl=<?php echo $_smarty_tpl->getVariable('product')->value['uploadfiles'];?>
'></iframe>
			<?php }else{ ?>
			<iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?comeform=myform&inputid=uploadfiles&thumbflag=1&thumbinput=thumbfiles&channel=product&waterflag=<?php echo $_smarty_tpl->getVariable('config')->value['watermarkflag'];?>
'></iframe>
			<?php }?>
		</td>
	  </tr>
	  <tr>
	  	<tr>
		<td class='hback_1'>列表页图片 <span class="f_red"></span> </td>
		<td class='hback'>
		  <input type="hidden" name="listpicurl" id="listpicurl" value="<?php echo $_smarty_tpl->getVariable('product')->value['listpicurl'];?>
" /><input type="hidden" name="lsitthumburl" id="lsitthumburl" value="<?php echo $_smarty_tpl->getVariable('product')->value['lsitthumburl'];?>
" /><span id='duploadfiles' class='f_red'></span>
		    <?php if ($_smarty_tpl->getVariable('product')->value['thumbfiles']!=''){?>
			<iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?action=show&comeform=myform&inputid=listpicurl&thumbflag=1&thumbinput=lsitthumburl&channel=product&waterflag=<?php echo $_smarty_tpl->getVariable('config')->value['watermarkflag'];?>
&picname=<?php echo $_smarty_tpl->getVariable('product')->value['uploadpicname'];?>
&picurl=<?php echo $_smarty_tpl->getVariable('product')->value['listpicurl'];?>
'></iframe>
			<?php }else{ ?>
			<iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?comeform=myform&inputid=listpicurl&thumbflag=1&thumbinput=lsitthumburl&channel=product&waterflag=<?php echo $_smarty_tpl->getVariable('config')->value['watermarkflag'];?>
'></iframe>
			<?php }?>
		</td>
	  </tr>
	  <tr>
		<td class='hback_1'>列表页图片 <span class="f_red"></span> </td>
		<td class='hback'>
		  <input type="hidden" name="img1" id="img1" value="<?php echo $_smarty_tpl->getVariable('product')->value['img1'];?>
" /><span id='duploadfiles' class='f_red'></span>
		    <?php if ($_smarty_tpl->getVariable('product')->value['thumbfiles']!=''){?>
			<iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?action=show&comeform=myform&inputid=img1&channel=product&waterflag=<?php echo $_smarty_tpl->getVariable('config')->value['watermarkflag'];?>
&picname=<?php echo $_smarty_tpl->getVariable('product')->value['uploadpicname'];?>
&picurl=<?php echo $_smarty_tpl->getVariable('product')->value['img1'];?>
'></iframe>
			<?php }else{ ?>
			<iframe id='iframe_t' border='0' frameborder='0' scrolling='no' width='450' height='25' src='upload.php?comeform=myform&inputid=img1&channel=product&waterflag=<?php echo $_smarty_tpl->getVariable('config')->value['watermarkflag'];?>
'></iframe>
			<?php }?>
		</td>
	  </tr>
	  <tr>
		<td class='hback_1'>产品原格 <span class="f_red"></span> </td>
		<td class='hback'><input type="text" name="nprice" id="nprice" class="input-s" value="<?php echo $_smarty_tpl->getVariable('product')->value['nprice'];?>
" /> （填写数字，单位为人民币￥） <span id='dnprice' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>产品价格 <span class="f_red"></span> </td>
		<td class='hback'><input type="text" name="price" id="price" class="input-s" value="<?php echo $_smarty_tpl->getVariable('product')->value['price'];?>
" /> （填写数字，单位为人民币￥） <span id='dprice' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>浏览次数 <span class="f_red"></span> </td>
		<td class='hback'><input type="text" name="hits" id="hits" class="input-s" value="<?php echo $_smarty_tpl->getVariable('product')->value['hits'];?>
" /> <span id='dhits' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_1'>TAGS </td>
		<td class='hback'><input type="text" name="tag" id="tag" class="input-txt" value="<?php echo $_smarty_tpl->getVariable('product')->value['tag'];?>
" />  <span id='dtag' class='f_red'></span> (多个请用英文,号隔开) <a href="ljcms_tag.php">管理Tags链接</a></td>
	  </tr>
	  <tr>
		<td class='hback_1'>状态设置 </td>
		<td class='hback'><?php echo $_smarty_tpl->getVariable('flag_checkbox')->value;?>
，<?php echo $_smarty_tpl->getVariable('elite_checkbox')->value;?>
，<?php echo $_smarty_tpl->getVariable('isnew_checkbox')->value;?>
，<?php echo $_smarty_tpl->getVariable('hots_checkbox')->value;?>
，<?php echo $_smarty_tpl->getVariable('ismiao_checkbox')->value;?>
</td>
	  </tr>

	  <tr>
		<td class='hback_1'>秒杀截至日期 </td>
		<td class='hback'><input type="text" name="miaotime" id="miaotime" class="date_input" value="<?php echo $_smarty_tpl->getVariable('product')->value['miaotime'];?>
" readonly/>  <span id='dmiaotime' class='f_red'></span> </td>
	  </tr>

	  <tr>
		<td class='hback_1'>浏览权限</td>
	<td class='hback'>
       <?php echo $_smarty_tpl->getVariable('ugroupid_select')->value;?>


        <input name="exclusive" type="radio" value="&gt;=" <?php if ($_smarty_tpl->getVariable('product')->value['exclusive']==">="){?> checked<?php }?>>
        隶属
        <input name="exclusive"  type="radio"  value="=" <?php if ($_smarty_tpl->getVariable('product')->value['exclusive']=="="){?> checked<?php }?>>
        专属（隶属：权限值≥可查看，专属：权限值＝可查看）	
		</td>
	  </tr>
	  <tr>
		<td class='hback_1'>产品简介 </td>
		<td class='hback'><textarea name="intro" id="intro" style='width:60%;height:65px;overflow:auto;color:#444444;'><?php echo $_smarty_tpl->getVariable('product')->value['intro'];?>
</textarea></td>
	  </tr>
	  <tr>
		<td class='hback_1'>详细内容 <span class="f_red">*</span></td>
		<td class='hback'>
		  <textarea name="content" id="content" style="width:99%;height:300px;display:none;"><?php echo $_smarty_tpl->getVariable('product')->value['content'];?>
</textarea>
		  <script>KE.show({id : 'content' });</script>  <span id='dcontent' class='f_red'></span></td>
	  </tr>
	  <tr>
		<td class='hback_none'></td>
		<td class='hback_none'><input type="submit" name="btn_save" class="button" value="更新保存" /></td>
	  </tr>
	</table>
	</form>
  </div>
  <div style="clear:both;"></div>
</div>
<?php }?>
</td></tr></table>
</body>
</html>
<script type="text/javascript">
function checkform() {
	var t = "";
	var v = "";

	t = "productname";
	v = $("#"+t).val();
	if(v=="") {
		dmsg("产品名称不能为空", t);
		return false;
	}




	t = 'content';
	v = KE.html(t).length;
	if(v=="") {
		dmsg("详细内容不能为空", t);
		return false;
	}

	return true;
}
</script>