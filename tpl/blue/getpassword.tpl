<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title><!--{$page_title}-->-<!--{$copyright_header}--></title>
<meta name="description" content="<!--{$page_description}-->" />
<meta name="keywords" content="<!--{$page_keyword}-->" />
<meta name="author" content="<!--{$copyright_author}-->" />
<link rel="stylesheet" type="text/css" href="<!--{$skinpath}-->style/main.css" />
<link rel="stylesheet" type="text/css" href="<!--{$skinpath}-->style/css.css" />
<script type='text/javascript' src='<!--{$skinpath}-->js/command.js'></script>
<script src="<!--{$skinpath}-->js/jquery-1.4.4.min.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/downnav.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/jquery.cross-slide.js" language="javascript" type="text/javascript"></script>
<!--[if IE 6]>
<script src="<!--{$skinpath}-->js/deletepng.js" language="javascript" type="text/javascript"></script>
<script type="text/javascript">
DD_belatedPNG.fix('.bg,img'); 
</script>
<![endif]-->
<script type="text/javascript">
function checkform(){

if (document.newmyform.password.value.length == 0) 
{
alert('新密码不能为空？');
document.newmyform.password.focus();
return false;
}
if (document.newmyform.password.value.length<6)
{
alert('最新密码不能小于六位？');
document.newmyform.password.focus();
return false;
}

if (document.newmyform.password.value!=document.newmyform.password1.value)
{
alert('你输入的重复密码不一样？');
document.newmyform.password.focus();
return false;
}
}
</script>
<link rel="stylesheet" type="text/css" href="<!--{$skinpath}-->css/member.css" /> 
</head>
<body>
<div id="wrap">
  <!--{include file="<!--{$tplpath}-->block_head.tpl"}-->
  <!--{include file="<!--{$tplpath}-->block_banner.tpl"}-->
  <div id="web">
    <div id="left">
	  <h3 class="title"><span>修改资料</span></h3>
	  <div class="webcontent">
	    <div class="showtext editor">
 <!--{if $uaction eq ""}-->
<div class="main-wrap">
  <div class="main-cont">
	<form action="getpassword.php" method="post" name="myform" id="myform" style="margin:0">
	<input type="hidden" name="uaction" id="uaction" value="selectuser" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">
	    <td align="center">登录名</td>
		<td><input type="text" name="username" id="username" class="input-s" value="" /></td>
	  </tr>
    <tr>
	<td class='submint' colspan="2">
	<input type='submit' name='Submit' value='提交信息' class='submit' />&nbsp;&nbsp;
	<input type='reset' name='Submit' value='重新填写' class='reset' /></td></td>
	  </tr>
	</table>
	</form>
  <div style="clear:both;height:10px;width:100%"></div>
  </div>
</div>
<!--{/if}-->





<!--{if $uaction eq "selectuser"}-->
<div class="main-wrap">
<div class="main-cont">
<h3 class="title" style="text-align:center">查询Email是否正确</h3>
<!--{if $user.loginname eq ""}-->
<!--{else}-->
<form action="getpassword.php" method="post" name="myform" id="myform" style="margin:0">
	<input type="hidden" name="uaction" id="uaction" value="seleemial" />
	<input type="hidden" name="loginname" id="loginname" value="<!--{$user.loginname}-->" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">
	    <td align="center">请输入E-mail:</td>
		<td><input type="text" name="email" id="emal" class="input-s" value="" /></td>
	  </tr>
    <tr>
	<td class='submint' colspan="2">
	<input type='submit' name='Submit' value='提交信息' class='submit' />&nbsp;&nbsp;
	<input type='reset' name='Submit' value='重新填写' class='reset' /></td></td>
	  </tr>
	</table>
	</form>
<!--{/if}-->
  <div style="clear:both;height:10px;width:100%"></div>
  </div>
  <div style="clear:both;height:10px;"></div>
</div>
<!--{/if}-->





<!--{if $uaction eq "seleemial"}-->
<div class="main-wrap">
<div class="main-cont">
<h3 class="title" style="text-align:center">请输入新密码:</h3>
<!--{if $users.loginname eq ""}-->
<!--{else}-->
<form action="getpassword.php" method="post" name="newmyform" id="newmyform" style="margin:0" onsubmit="return checkform()">
	<input type="hidden" name="uaction" id="uaction" value="updatepwd" />
	<input type="hidden" name="loginname" id="loginname" value="<!--{$users.loginname}-->" />
	<input type="hidden" name="userid" id="userid" value="<!--{$users.userid}-->" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <tr>
	    <td align="center">新密码:</td>
		<td><input type="password" name="password" id="password" class="input-s" value="" /></td>
	  </tr>
	  <tr>
	    <td align="center">重复密码:</td>
		<td><input type="password" name="password1" id="password1" class="input-s" value="" /></td>
	  </tr>
    <tr>
	<td class='submint' colspan="2">
	<input type='submit' name='Submit' value='提交信息' class='submit' />&nbsp;&nbsp;
	<input type='reset' name='Submit' value='重新填写' class='reset' /></td></td>
	  </tr>
	</table>
	</form>
<!--{/if}-->
  <div style="clear:both;height:10px;width:100%"></div>
  </div>
  <div style="clear:both;height:10px;"></div>
</div>
<!--{/if}-->

		 
		</div>
      </div>
	</div><!---left //-->

    <div id="right">
	  <h3 class="title"><span>找回密码</span></h3>
	  <div class="webnav"> 
	    <div id="web-sidebar">



<!--{if $username eq ""}-->
		  <dl>
		    <dt class="part2"><a href="register.php" target="_blank" title='注册会员' style="margin-left: 5px;" >注册会员</a></dt>
		  </dl>	


<!--{else}-->
		  <dl>
		    <dt class="part2"><a href='index.php' target="_blank" title='会员中心首页' style="margin-left: 5px;">会员中心首页</a></dt>
		  </dl>	
		  <dl>
		    <dt class="part2"><a href='userinfo.php' target="_blank" title='修改基本信息' style="margin-left: 5px;">修改基本信息</a></dt>
		  </dl>	
		  		  <dl>
		    <dt class="part2"><a  href='mymessage.php' title='我的留言' style="margin-left: 5px;">我的留言</a></dt>
		  </dl>	
		  		  <dl>
		    <dt class="part2"><a href='myorder.php' title='我的订单' target="_blank" style="margin-left: 5px;">我的订单</a></dt>
		  </dl>	
		  		  <dl>
		    <dt class="part2"><a href='resume.php' title='我的简历' target="_blank" style="margin-left: 5px;">我的简历</a></dt>
		  </dl>	
		  		  <dl>
		    <dt class="part2"><a href='loginout.php' title='安全退出' style="margin-left: 5px;">安全退出</a></dt>
		  </dl>	
<!--{/if}-->




		</div>
	  
	  </div><!--$webnav //-->

      <!--{include file="<!--{$tplpath}-->block_info.tpl"}--> 
      <!--{include file="<!--{$tplpath}-->block_contact.tpl"}-->  
		
    </div><!-- $right //--->

    <div style="clear:both;"></div>
  </div><!--#web //-->
  <!--{include file="<!--{$tplpath}-->block_footer.tpl"}-->
</div>
<script type="text/javascript" src="<!--{$skinpath}-->js/screen.js"></script>
</body>
</html>






