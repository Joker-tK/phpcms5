<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title><!--{$page_title}-->-<!--{$copyright_header}--></title>
<meta name="description" content="<!--{$page_description}-->" />
<meta name="keywords" content="<!--{$page_keyword}-->" />
<meta name="author" content="<!--{$copyright_author}-->" />
<link rel="stylesheet" type="text/css" href="<!--{$skinpath}-->style/main.css" />
<link rel="stylesheet" type="text/css" href="<!--{$skinpath}-->style/css.css" />
<script type='text/javascript' src='<!--{$skinpath}-->js/command.js'></script>
<script src="<!--{$skinpath}-->js/jquery-1.4.4.min.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/downnav.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/jquery.cross-slide.js" language="javascript" type="text/javascript"></script>
<!--[if IE 6]>
<script src="<!--{$skinpath}-->js/deletepng.js" language="javascript" type="text/javascript"></script>
<script type="text/javascript">
DD_belatedPNG.fix('.bg,img'); 
</script>
<![endif]-->
<script type="text/javascript">
function formsubmit()
 {
 if (formcheck())
 {

	  document.regform.action="saveuser.php?action=add";
      document.regform.method="post";
      document.regform.submit();
   }  
}

  function formcheck() 
    { 
       if(document.getElementById("loginname").value== "") 
         { alert("请输入用户名");
		   document.regform.loginname.focus();
		   document.regform.loginname.select(); 
           return false;
         }
	   if(document.regform.loginname.value.length<3) 
         { alert("用户名不能小于3位");
		   document.regform.loginname.focus();
		   document.regform.loginname.select(); 
           return false;
         }   


if ((document.regform.password.value).length<6)
{
alert("密码应大于等于6位！");
document.regform.password.focus();
document.regform.password.select();
return false;
}

if(document.regform.password1.value!=document.regform.password.value)
{
alert("两次密码输入不一致");
document.regform.password1.focus();
document.regform.password1.select();
return false;
}



    return true;      
    }

</script>
<link rel="stylesheet" type="text/css" href="<!--{$skinpath}-->css/member.css" /> 
</head>
<body>
<div id="wrap">
  <!--{include file="<!--{$tplpath}-->block_head.tpl"}-->
  <!--{include file="<!--{$tplpath}-->block_banner.tpl"}-->
  <div id="web">
    <div id="left">
	  <h3 class="title"><span>注册会员</span></h3>
	  <div class="webcontent">
	    <div class="showtext editor">
 
   <form method="POST"  name="regform" >
    <table cellpadding="1" border="0" cellspacing="1">
	<tr>
	<td class="zhuce_text"><span>用户名:</span></td>
	<td class="zhuce_input"><input id="loginname" name="loginname" type="text" class="input_text" /></td>
	<td class="zhuce_sm">*</td>
	</tr>
	<tr>
	<td class="zhuce_text"><span>登录密码:</span></td>
	<td class="zhuce_input"><input name="password" id="password" type="password" class="input_text" /></td>
	<td class="zhuce_sm">*</td>
	</tr>
	<tr>
	<td class="zhuce_text"><span>密码确认:</span></td>
	<td class="zhuce_input"><input  name="password1" id="password1" type="password" class="input_text" /></td>
	<td class="zhuce_sm">*</td>
	</tr>
	<tr>
	<td class="zhuce_text"><span>性别：</span></td>
	<td class="zhuce_input"><input name="Sex" type="radio" value="1" checked="checked" class="inputnoborder" />先生 
     <input type="radio" name="Sex" value="0" class="inputnoborder" />女士</td>
	<td class="zhuce_sm">*</td>
	</tr>
    <tr>
	<td class="zhuce_text"><span>真实姓名:</span></td>
    <td class="zhuce_input"><input name="realname" id="realname" type="text" class="input_text" /></td>
	<td class="zhuce_sm">*</td>
	</tr>
	<tr>
	<td class="zhuce_text"><span>联系电话:</span></td>
    <td class="zhuce_input"><input name="telno" id="telno" type="text" class="input_text" /></td>
	<td class="zhuce_sm"></td>
	</tr>
	<tr>
	<td class="zhuce_text"><span>生日:</span></td>
    <td class="zhuce_input"><input id="brothday" name="brothday" type="text" class="input_text" /></td>
	<td class="zhuce_sm"></td>
	</tr>
	<tr>
	<td class="zhuce_text"><span>E-mail:</span></td>
    <td class="zhuce_input"><input id="email" name="email" type="text" class="input_text" /></td>
	<td class="zhuce_sm">*</td>
	</tr>
	<tr>
	<td class="zhuce_text"><span>联系地址:</span></td>
    <td class="zhuce_input"><input name="address" id="address" type="text" class="input_text" /></td>
	<td class="zhuce_sm"></td>
	</tr>
	<tr>
	<td class="zhuce_text"><span>个人说明:</span></td>
    <td class="zhuce_input" colspan="2"><textarea name='memo' id='memo' style='width:260px;height:65px;overflow:auto;color:#444444;'></textarea></td>
	</tr>
	<tr>
	<td class="zhuce_text"></td>
	<td class="zhuce_subimt" colspan="2">
	<input type="button" id="b_save" name="b_save" onClick="formsubmit()" value="立即注册" />
	<input type="reset" value="重填" id="b_reset" name="b_reset" /></td>
    </tr>
	</table>
</form>
		</div>
      </div>
	</div><!---left //-->

    <div id="right">
	  <h3 class="title"><span>会员中心</span></h3>
	  <div class="webnav"> 
	    <div id="web-sidebar">



<!--{if $username eq ""}-->
		  <dl>
		    <dt class="part2"><a href="register.php" target="_blank" title='注册会员' style="margin-left: 5px;" >注册会员</a></dt>
		  </dl>	


<!--{else}-->
		  <dl>
		    <dt class="part2"><a href='index.php' target="_blank" title='会员中心首页' style="margin-left: 5px;">会员中心首页</a></dt>
		  </dl>	
		  <dl>
		    <dt class="part2"><a href='userinfo.php' target="_blank" title='修改基本信息' style="margin-left: 5px;">修改基本信息</a></dt>
		  </dl>	
		  		  <dl>
		    <dt class="part2"><a  href='mymessage.php' title='我的留言' style="margin-left: 5px;">我的留言</a></dt>
		  </dl>	
		  		  <dl>
		    <dt class="part2"><a href='myorder.php' title='我的订单' target="_blank" style="margin-left: 5px;">我的订单</a></dt>
		  </dl>	
		  		  <dl>
		    <dt class="part2"><a href='resume.php' title='我的简历' target="_blank" style="margin-left: 5px;">我的简历</a></dt>
		  </dl>	
		  		  <dl>
		    <dt class="part2"><a href='loginout.php' title='安全退出' style="margin-left: 5px;">安全退出</a></dt>
		  </dl>	
<!--{/if}-->




		</div>
	  
	  </div><!--$webnav //-->

      <!--{include file="<!--{$tplpath}-->block_info.tpl"}--> 
      <!--{include file="<!--{$tplpath}-->block_contact.tpl"}-->  
		
    </div><!-- $right //--->

    <div style="clear:both;"></div>
  </div><!--#web //-->
  <!--{include file="<!--{$tplpath}-->block_footer.tpl"}-->
</div>
<script type="text/javascript" src="<!--{$skinpath}-->js/screen.js"></script>
</body>
</html>








