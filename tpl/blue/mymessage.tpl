<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title><!--{$page_title}-->-<!--{$copyright_header}--></title>
<meta name="description" content="<!--{$page_description}-->" />
<meta name="keywords" content="<!--{$page_keyword}-->" />
<meta name="author" content="<!--{$copyright_author}-->" />
<link rel="stylesheet" type="text/css" href="<!--{$skinpath}-->style/main.css" />
<link rel="stylesheet" type="text/css" href="<!--{$skinpath}-->style/css.css" />
<script type='text/javascript' src='<!--{$skinpath}-->js/command.js'></script>
<script src="<!--{$skinpath}-->js/jquery-1.4.4.min.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/downnav.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/jquery.cross-slide.js" language="javascript" type="text/javascript"></script>
<!--[if IE 6]>
<script src="<!--{$skinpath}-->js/deletepng.js" language="javascript" type="text/javascript"></script>
<script type="text/javascript">
DD_belatedPNG.fix('.bg,img'); 
</script>
<![endif]-->
<script type="text/javascript">
function mysubumit()
 {
 if (formcheck())
 {

	  document.editform.action="userinfo.php";
      document.editform.method="post";
      document.editform.submit();
   }  
}

  function formcheck() 
    { 
       if(document.getElementById("loginname").value== "") 
         { 
		 
		   alert("请输入用户名");
		   document.editform.loginname.focus();
		   document.editform.loginname.select(); 
           return false;
         }

	   if(document.editform.loginname.value.length<3) 
         { 
		   alert("用户名不能小于3位");
		   document.editform.loginname.focus();
		   document.editform.loginname.select(); 
           return false;
         }   

    return true;      
    }


function checkform(){

if (document.myform.username.value.length == 0) 
{
alert('姓名 不能为空');
document.myform.username.focus();
return false;
}

if (document.myform.email.value.length == 0)
{
alert('E–mail 不能为空');
document.myform.email.focus();
return false;
}
if (document.myform.telno.value.length == 0)
{
alert('联系电话不能为空');
document.myform.telno.focus();
return false;
}
}


</script>
</head>
<body>
<div id="wrap">
  <!--{include file="<!--{$tplpath}-->block_head.tpl"}-->
  <!--{include file="<!--{$tplpath}-->block_banner.tpl"}-->
  <div id="web">
    <div id="left">
	  <h3 class="title"><span>修改资料</span></h3>
	  <div class="webcontent">
	    <div class="showtext editor">

<!--{if $uaction eq ""}-->
<div class="main-wrap">
  <div class="main-cont">
	<form action="resume.php" method="post" name="myform" id="myform" style="margin:0">
	<input type="hidden" name="uaction" id="uaction" value="deljob" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <thead class="tb-tit-bg">
	  <tr>
	    <th width="10%"><div class="th-gap-u">留言编号</div></th>
		<th width="10%"><div class="th-gap-u">留言人名</div></th>
		<th width="16%"><div class="th-gap-u">工作</div></th>
		<th width="30%"><div class="th-gap-u">联系电话</div></th>
		<th width="18%"><div class="th-gap-u">留言时间</div></th>
		<th><div class="th-gap">操作</div></th>
	  </tr>
	  </thead>
	  <tfoot class="tb-foot-bg"></tfoot>
	  <!--{foreach $mymsg as $uvolist}-->
	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">
	    <td align="center"><!--{$uvolist.bookid}--></td>
		<td><!--{$uvolist.bookuser}--></td>
		<td align="center">
		<!--{$uvolist.jobs}-->
		</td>
		<td align="center"><!--{$uvolist.telephone}--></td>
		<td><!--{$uvolist.booktimeline|date_format:"%Y/%m/%d %H:%M:%S"}--></td>
		<td align="center"><a href="mymessage.php?uaction=showmsg&id=<!--{$uvolist.bookid}-->&page=<!--{$page}-->" class="icon-edit">查看留言</a></td>
	  </tr>
	  <!--{foreachelse}-->
      <tr>
	    <td colspan="6" align="center">暂无留言信息</td>
	  </tr>
	  <!--{/foreach}-->
	  <!--{if $total>0}-->
	  <tr>
		<td class="hback" colspan="6" align="right">&nbsp;&nbsp;共[ <b><!--{$total}--></b> ]条记录</td>
	  </tr>
	  <!--{/if}-->
	</table>
	</form>
	<!--{if $pagecount>1}-->
	<table width='95%' border='0' cellspacing='0' cellpadding='0' align='center' style="margin-top:10px;">
	  <tr>
		<td align='center'><!--{$showpage}--></td>
	  </tr>
	</table>
	<!--{/if}-->
  <div style="clear:both;height:10px;width:100%"></div>
  </div>
</div>
<!--{/if}-->

<!--{if $uaction eq "showmsg"}-->
<div class="main-wrap">
<div class="main-cont">
<h3 class="title" style="text-align:center"> <a href="mymessage.php?<!--{$comeurl}-->" class="btn-general"><span>返回列表</span></a></h3>
<table cellpadding='3' cellspacing='3' class='tab' ALIGN="center">
	  <tr>
		<td class='hback_1' width="15%">留言人：<span class='f_red'>*</span></td>
		<td class='hback' width="85%"><!--{$mymsg.bookuser}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>性别：<span class='f_red'></span></td>
		<td class='hback'><input type="radio" name="gender" id="gender" value="1"<!--{if $mymsg.gender==1}--> checked<!--{/if}--> />男，<input type="radio" name="gender" id="gender" value="2"<!--{if $mymsg.gender==0}--> checked<!--{/if}--> />女</td>
	  </tr>

	  <tr>
		<td class='hback_1'>职位：<span class='f_red'></span></td>
		<td class='hback'><!--{$mymsg.jobs}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>电话：<span class='f_red'></span></td>
		<td class='hback'><!--{$mymsg.telephone}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>传真：<span class='f_red'></span></td>
		<td class='hback'><!--{$mymsg.fax}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>手机：<span class='f_red'></span></td>
		<td class='hback'><!--{$mymsg.mobile}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>Email：<span class='f_red'></span></td>
		<td class='hback'><!--{$mymsg.email}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>QQ/MSN：<span class='f_red'></span></td>
		<td class='hback'><!--{$mymsg.qqmsn}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>公司名称：<span class='f_red'></span></td>
		<td class='hback'><!--{$mymsg.companyname}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>公司地址：<span class='f_red'></span></td>
		<td class='hback'><!--{$mymsg.address}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>公司行业：<span class='f_red'></span></td>
		<td class='hback'><!--{$mymsg.trade}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>公司主页：<span class='f_red'></span></td>
		<td class='hback'><!--{$mymsg.homepage}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>留言内容： <span class='f_red'>*</span></td>
		<td class='hback'><textarea name="content" id="content" style='width:60%;height:100px;overflow:auto;color:#444444;'><!--{$mymsg.content}--></textarea></td>
	  </tr>
	  <tr>
		<td class='hback_1'>留言时间：<span class='f_red'></span></td>
		<td class='hback'><!--{$mymsg.booktimeline|date_format:"%Y/%m/%d %H:%M:%S"}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>留言IP：<span class='f_red'></span></td>
		<td class='hback'><!--{$mymsg.ip}--></td>
	  </tr>
<!--{if $mymsg.replycontent eq ""}-->
	  <tr>
		<td class='hback' colspan="2"><font STYLE="COLOR:RED;">管理员暂时未回复</font></td>
	  </tr>
 <!--{else}-->
	  <tr>
		<td class='hback_1'>回复人：<span class='f_red'></span></td>
		<td class='hback'><!--{$mymsg.replyuser}--></td>
	  </tr>
	  <tr>
		<td class='hback_1'>回复内容： <span class='f_red'></span></td>
		<td class='hback'><textarea name="replycontent" id="replycontent" style='width:80%px;height:100px;overflow:auto;color:#444444;'><!--{$mymsg.replycontent}--></textarea></td>
	  </tr>	
 <!--{/if}-->
	</table>
  <div style="clear:both;height:10px;width:100%"></div>
  </div>
  <div style="clear:both;height:10px;"></div>
</div>
<!--{/if}-->
		 
		</div>
      </div>
	</div><!---left //-->

    <div id="right">
	  <h3 class="title"><span>会员中心</span></h3>
	  <div class="webnav"> 
	    <div id="web-sidebar">



<!--{if $username eq ""}-->
		  <dl>
		    <dt class="part2"><a href="register.php" target="_blank" title='注册会员' style="margin-left: 5px;" >注册会员</a></dt>
		  </dl>	


<!--{else}-->
		  <dl>
		    <dt class="part2"><a href='index.php' target="_blank" title='会员中心首页' style="margin-left: 5px;">会员中心首页</a></dt>
		  </dl>	
		  <dl>
		    <dt class="part2"><a href='userinfo.php' target="_blank" title='修改基本信息' style="margin-left: 5px;">修改基本信息</a></dt>
		  </dl>	
		  		  <dl>
		    <dt class="part2"><a  href='mymessage.php' title='我的留言' style="margin-left: 5px;">我的留言</a></dt>
		  </dl>	
		  		  <dl>
		    <dt class="part2"><a href='myorder.php' title='我的订单' target="_blank" style="margin-left: 5px;">我的订单</a></dt>
		  </dl>	
		  		  <dl>
		    <dt class="part2"><a href='resume.php' title='我的简历' target="_blank" style="margin-left: 5px;">我的简历</a></dt>
		  </dl>	
		  		  <dl>
		    <dt class="part2"><a href='loginout.php' title='安全退出' style="margin-left: 5px;">安全退出</a></dt>
		  </dl>	
<!--{/if}-->




		</div>
	  
	  </div><!--$webnav //-->

      <!--{include file="<!--{$tplpath}-->block_info.tpl"}--> 
      <!--{include file="<!--{$tplpath}-->block_contact.tpl"}-->  
		
    </div><!-- $right //--->

    <div style="clear:both;"></div>
  </div><!--#web //-->
  <!--{include file="<!--{$tplpath}-->block_footer.tpl"}-->
</div>
<script type="text/javascript" src="<!--{$skinpath}-->js/screen.js"></script>
</body>
</html>





























