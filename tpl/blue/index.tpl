﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title><!--{$page_title}-->-<!--{$copyright_header}--></title>
<meta name="description" content="<!--{$page_description}-->" />
<meta name="keywords" content="<!--{$page_keyword}-->" />
<meta name="author" content="<!--{$copyright_author}-->" />
<link rel="stylesheet" type="text/css" href="<!--{$skinpath}-->style/main.css" />
<link rel="stylesheet" type="text/css" href="<!--{$skinpath}-->style/css.css" />
<script type='text/javascript' src='<!--{$skinpath}-->js/command.js'></script>
<script src="<!--{$skinpath}-->js/jquery-1.4.4.min.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/downnav.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/jquery.cross-slide.js" language="javascript" type="text/javascript"></script>
<!--[if IE 6]>
<script src="<!--{$skinpath}-->js/deletepng.js" language="javascript" type="text/javascript"></script>
<script type="text/javascript">
DD_belatedPNG.fix('.bg,img'); 
</script>
<![endif]-->
</head>
<body>
<div id="wrap">
  <!--{include file="<!--{$tplpath}-->block_head.tpl"}-->
  <!--{if $ads_zone1}-->
  <div id="flash"> 
    <div class="flash">
	  <!--{if $zone_silde1=="1"}-->
		<div id="slide_wp" style="overflow:hidden;width:<!--{$zone_width1}-->px;height:auto;position:relative;clear:both;boder:0px;background-color:#ffffff;margin:auto;">
		  <div id="MSClassBox">
			<ul id="ContentID">
			  <!--{foreach $ads_zone1 as $volist}-->
			  <li><a href="<!--{$volist.url}-->"><img border="0" src="<!--{$volist.uploadfiles}-->" width="<!--{$volist.width}-->" height="<!--{$volist.height}-->" title="<!--{$volist.adsname}-->" alt="<!--{$volist.adsname}-->" /></a></li>
			  <!--{/foreach}-->
			</ul>
		  </div>
		  <ul id="TabID" style="display:none;">
			<!--{foreach $ads_zone1 as $volist}-->
			<li class=""><!--{$volist.i}--></li>
			<!--{/foreach}-->
		  </ul>
		</div>
		<script type="text/javascript">
		new Marquee(
		{
			MSClassID : "MSClassBox",
			ContentID : "ContentID",
			TabID	  : "TabID",
			Direction : 2,
			Step	  : 0.3,
			Width	  : <!--{$zone_width1}-->,
			Height	  : <!--{$zone_height1}-->,
			Timer	  : 20,
			DelayTime : 2000,
			WaitTime  : 0,
			ScrollStep: 610,
			SwitchType: 1,
			AutoStart : 1
		})
		</script>
	  <!--{else}-->
	  <div id="banner">
      <!--{foreach $ads_zone1 as $volist}-->
      <a href="<!--{$volist.url}-->"><img src="<!--{$volist.uploadfiles}-->" width="<!--{$volist.width}-->" height="<!--{$volist.height}-->" title="<!--{$volist.adsname}-->" alt="<!--{$volist.adsname}-->" /></a>
	  <!--{/foreach}-->
	  </div>
	  <!--{/if}-->
	</div>
  </div><!-- #flash //-->
  <!--{/if}-->

  <div id="main">

    <div id="left">

	  <h3 class="title"><a href="<!--{$url_product}-->" title="More" class="more">More</a><span>产品类别</span></h3>
	  <div class="list1">
	    <div id="web-sidebar">
		  <!--{section name=p loop=$volist_producttreecategory}-->
		  <dl>
		    <dt class="part2" id="part1-id<!--{$volist_producttreecategory[p].cateid}-->"><b><a href="<!--{$volist_producttreecategory[p].url}-->"><!--{$volist_producttreecategory[p].catename}--></a></b></dt>
			<dd class="part3dom">
			  <!--{section name=c loop=$volist_producttreecategory[p].childcategory}-->
			  <h4 class="part3" id="part2-id<!--{$volist_producttreecategory[p].childcategory[c].cateid}-->"><a href="<!--{$volist_producttreecategory[p].childcategory[c].url}-->"><!--{$volist_producttreecategory[p].childcategory[c].catename}--></a></h4>
			  <!--{/section}-->
			</dd>
		  </dl>	
		  <!--{/section}-->
		</div><!-- #web-sidebar //-->
		<script type="text/javascript">
			$(document).ready(function(){
					var Plug1 = 0; 
					var Plug2 = 1; 
				var i = 0;
				var SideBar = $("#web-sidebar");
				var SideBar_dt = SideBar.find("dt.part2");
				var SideBar_dd = SideBar.find("dd.part3dom");
				var part1_dom = $("#part1-id0"); 
				var part2_dom = $("#part2-id0"); 
				var part_dom_dd = part1_dom.next("dd.part3dom");
					SideBar_dd.css("display","none");
					part1_dom.addClass("ondown");
					part2_dom.addClass("ondown");
				if(Plug1 == 1){ SideBar_dd.css("display","block"); SideBar_dt.addClass("part_on"); i = 1;} 
				
				if(Plug2 == 1 && part1_dom.length!=0){
						part_dom_dd.css("display","block");
						i = 1; 
				}
				
				SideBar_dt.click(function(){
					i++;if(i>1)i=0;
					i==1?SideBar_on($(this)):SideBar_out($(this));	
				});
				
		//****************


			});
			
			function SideBar_on(dom){
				var dd = dom.next("dd.part3dom")
					dom.addClass("part_on");
					dom.removeClass("part_out");
					dd.css("display","block");
			}
			
			function SideBar_out(dom){
				var dd = dom.next("dd.part3dom")
					dom.addClass("part_out");
					dom.removeClass("part_on");
					dd.css("display","none");
			}

			
		//************

		</script>


      </div><!-- $list1 //--->
	  
	  <h3 class="title"><a href="<!--{$url_info}-->" title="More" class="more">More</a><span>最新动态</span></h3>
	  <ul class="list2">
	    <!--{foreach $volist_newinfo as $volist}-->
	    <li><a href="<!--{$volist.url}-->" title="<!--{$volist.title}-->"><!--{$volist.sort_title}--></a></li>
		<!--{/foreach}-->
	  </ul>
	  <h3 class="title"><a href="<!--{$url_contact}-->" title="More" class="more">More</a><span>联系方式</span></h3>
	  <div class="text editor">
	    <p><!--{$delimit_contact}--></p>
	  </div>
	</div><!-- #left //-->

	<div id="right">
	  <h3 class="title"><a href="<!--{$url_about}-->" title="More" class="more">More</a><span>公司简介</span></h3>
	  <div class="text">
	    <div class="editor">
		  <p>
		  <!--{if $ads_zone2}-->
		  <!--{foreach $ads_zone2 as $volist}-->
		  <img width="<!--{$volist.width}-->" height="<!--{$volist.height}-->" hspace="2" align="left" vspace="2" src="<!--{$volist.uploadfiles}-->" />
		  <!--{/foreach}-->
		  <!--{/if}-->
		  </p><p><!--{$config.about|filterhtml:435}-->...</p>
		</div>
	  </div>
	  
	  <h3 class="title line"><a href="<!--{$url_product}-->" title="More" class="more">More</a><span>最新产品</span></h3>
	  <div class="list">
	    <ul id="drawimg">
		  <!--{foreach $volist_newproduct as $volist}-->
		  <li style="width:158px; height:183px;">
		    <a href="<!--{$volist.url}-->" class="imgbox"><img src="<!--{$volist.thumbfiles}-->" onload="javascript:DrawImage(this,'150','150');" alt="<!--{$volist.produtname}-->" /></a>
			<h4><a href="<!--{$volist.url}-->" title="<!--{$volist.productname}-->"><!--{$volist.sort_productname}--></a></h4>
		  </li>
		  <!--{/foreach}-->
		</ul>
		<div style="clear:both;"></div>
	  </div><!-- #list //-->
	
	</div><!-- #right //-->
	<div style="clear:both;"></div>
  </div><!-- #main //-->
  <!--{include file="<!--{$tplpath}-->block_footer.tpl"}-->
</div>
<script type="text/javascript" src="<!--{$skinpath}-->js/screen.js"></script>
</body>
</html>