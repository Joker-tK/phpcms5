<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title><!--{$page_title}-->-<!--{$copyright_header}--></title>
<meta name="description" content="<!--{$page_description}-->" />
<meta name="keywords" content="<!--{$page_keyword}-->" />
<meta name="author" content="<!--{$copyright_author}-->" />
<link rel="stylesheet" type="text/css" href="<!--{$skinpath}-->style/main.css" />
<link rel="stylesheet" type="text/css" href="<!--{$skinpath}-->style/css.css" />
<script type='text/javascript' src='<!--{$skinpath}-->js/command.js'></script>
<script src="<!--{$skinpath}-->js/jquery-1.4.4.min.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/downnav.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/jquery.cross-slide.js" language="javascript" type="text/javascript"></script>
<!--[if IE 6]>
<script src="<!--{$skinpath}-->js/deletepng.js" language="javascript" type="text/javascript"></script>
<script type="text/javascript">
DD_belatedPNG.fix('.bg,img'); 
</script>
<![endif]-->
<script type="text/javascript">
function mysubumit()
 {
 if (formcheck())
 {

	  document.editform.action="userinfo.php";
      document.editform.method="post";
      document.editform.submit();
   }  
}

  function formcheck() 
    { 
       if(document.getElementById("loginname").value== "") 
         { 
		 
		   alert("请输入用户名");
		   document.editform.loginname.focus();
		   document.editform.loginname.select(); 
           return false;
         }

	   if(document.editform.loginname.value.length<3) 
         { 
		   alert("用户名不能小于3位");
		   document.editform.loginname.focus();
		   document.editform.loginname.select(); 
           return false;
         }   

    return true;      
    }


function checkform(){

if (document.myform.username.value.length == 0) 
{
alert('姓名 不能为空');
document.myform.username.focus();
return false;
}

if (document.myform.email.value.length == 0)
{
alert('E–mail 不能为空');
document.myform.email.focus();
return false;
}
if (document.myform.telno.value.length == 0)
{
alert('联系电话不能为空');
document.myform.telno.focus();
return false;
}
}
</script>
<link rel="stylesheet" type="text/css" href="<!--{$skinpath}-->css/member.css" /> 
</head>
<body>
<div id="wrap">
  <!--{include file="<!--{$tplpath}-->block_head.tpl"}-->
  <!--{include file="<!--{$tplpath}-->block_banner.tpl"}-->
  <div id="web">
    <div id="left">
	  <h3 class="title"><span>管理简历</span></h3>
	  <div class="webcontent">
	    <div class="showtext editor">
 
  
<!--{if $uaction eq ""}-->
<div class="main-wrap">
  <div class="main-cont">
	<form action="resume.php" method="post" name="myform" id="myform" style="margin:0">
	<input type="hidden" name="uaction" id="uaction" value="deljob" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" align="center">
	  <thead class="tb-tit-bg">
	  <tr>
	    <th width="10%"><div class="th-gap-u">简历编号</div></th>
		<th width="10%"><div class="th-gap-u">我的名字</div></th>
		<th width="16%"><div class="th-gap-u">应聘职位</div></th>
		<th width="30%"><div class="th-gap-u">联系电话</div></th>
		<th width="18%"><div class="th-gap-u">注册时间</div></th>
		<th><div class="th-gap">操作</div></th>
	  </tr>
	  </thead>
	  <tfoot class="tb-foot-bg"></tfoot>
	  <!--{foreach $jobs as $uvolist}-->
	  <tr onMouseOver="overColor(this)" onMouseOut="outColor(this)">
	    <td align="center"><input name="id[]" type="checkbox" value="<!--{$uvolist.aid}-->" onClick="checkItem(this, 'chkAll')"></td>
		<td><!--{$uvolist.username}--></td>
		<td align="center">
		<!--{$uvolist.jobtitle}-->
		</td>
		<td align="center"><!--{$uvolist.telno}--></td>
		<td><!--{$uvolist.addtime|date_format:"%Y/%m/%d %H:%M:%S"}--></td>
		<td align="center"><a href="resume.php?uaction=editjob&id=<!--{$uvolist.aid}-->&page=<!--{$page}-->" class="icon-edit">编辑</a></td>
	  </tr>
	  <!--{foreachelse}-->
      <tr>
	    <td colspan="6" align="center">暂无信息</td>
	  </tr>
	  <!--{/foreach}-->
	  <!--{if $total>0}-->
	  <tr>
		<td align="center"><input name="chkAll" type="checkbox" id="chkAll" onClick="checkAll(this, 'id[]')" value="checkbox"></td>
		<td class="hback" colspan="5"><input class="button" name="btn_del" type="button" value="删 除" onClick="{if(confirm('确定删除选定信息吗!?')){$('#uaction').val('deljob');$('#myform').submit();return true;}return false;}" class="button">&nbsp;&nbsp;共[ <b><!--{$total}--></b> ]条记录</td>
	  </tr>
	  <!--{/if}-->
	</table>
	</form>
	<!--{if $pagecount>1}-->
	<table width='95%' border='0' cellspacing='0' cellpadding='0' align='center' style="margin-top:10px;">
	  <tr>
		<td align='center'><!--{$showpage}--></td>
	  </tr>
	</table>
	<!--{/if}-->
  <div style="clear:both;height:10px;width:100%"></div>
  </div>
</div>
<!--{/if}-->





<!--{if $uaction eq "editjob"}-->
<div class="main-wrap">
<div class="main-cont">
<h3 class="title"> <a href="resume.php?<!--{$comeurl}-->" class="btn-general"><span>返回列表</span></a> 编辑简历</h3>
<form name="myform" id="myform" method="post" action="resume.php" onsubmit="return checkform();" />
<form method='POST' onSubmit='return CheckJob();' name='myform' action='<!--{$url_applyjob}-->' >
<input type="hidden" name="uaction" value="jobave" />
<input type="hidden" name="id" value="<!--{$id}-->" />
<table class='job_table' width="95%" border="0" align="center" cellpadding="5" cellspacing="3" style="margin-top:20px">
<tr>
<td class='text'>应聘职位</td>
<td class='input'><!--{$jobs.jobtitle}--></td>
</tr>
<tr><td class='text'>姓名</td>
<td class='input'><input name='username' type='text' value="<!--{$jobs.username}-->" class='input-text' size='40'><span class='info'>*</span></td></tr>
<tr><td class='text'>性别</td>
<td class='input'>
<!--{if $jobs.sex==1}-->
<input name='sex' type='radio' id='para12_1' value='1' checked='checked' /><label for='para12_1'>男士</label> 
<input name='sex' type='radio' id='para12_2' value='0'  /><label for='para12_2'>女士</label> 
<!--{else}-->
<input name='sex' type='radio' id='para12_1' value='1'/><label for='para12_1'>男士</label> 
<input name='sex' type='radio' id='para12_2' value='0' checked='checked'   /><label for='para12_2'>女士</label> 
<!--{/if}-->
</td></tr>
<tr><td class='text'>出生年月</td>
<td class='input'><input name='brothday' type='text' value="<!--{$jobs.brothday}-->" class='input-text' size='40'><span class='info'></span></td></tr>
<tr><td class='text'>籍贯</td>
<td class='input'><input name='chinatext' type='text' value="<!--{$jobs.chinatext}-->" class='input-text' size='40'><span class='info'></span></td></tr>
<tr><td class='text'>联系电话</td>
<td class='input'><input name='telno' type='text'  value="<!--{$jobs.telno}-->" class='input-text' size='40'><span class='info'></span></td></tr>
<tr><td class='text'>E–mail</td>
<td class='input'><input name='email' type='text'  value="<!--{$jobs.email}-->" class='input-text' size='40'><span class='info'>*</span></td></tr>
<tr><td class='text'>学历</td>
<td class='input'><input name='degree' type='text'  value="<!--{$jobs.degree}-->" class='input-text' size='40'><span class='info'></span></td></tr>
<tr><td class='text'>专业</td>
<td class='input'><input name='prosesion' type='text'  value="<!--{$jobs.prosesion}-->" class='input-text' size='40'><span class='info'></span></td></tr>
<tr><td class='text'>学校</td>
<td class='input'><input name='school' type='text'  value="<!--{$jobs.school}-->" class='input-text' size='40'><span class='info'></span></td></tr>
<tr><td class='text'>通讯地址</td>
<td class='input'><input name='address' type='text'  value="<!--{$jobs.address}-->" class='input-text' size='40'><span class='info'></span></td></tr>
<tr><td class='text'>所获奖项</td>
<td class='input'><textarea name='awards' class='textarea-text' cols='60' rows='5'><!--{$jobs.awards}--></textarea><span class='info'></span></td></tr>
<tr><td class='text'>工作经历</td>
<td class='input'><textarea name='experience' class='textarea-text' cols='60' rows='5'><!--{$jobs.experience}--></textarea><span class='info'></span></td></tr>
<tr><td class='text'>业余爱好</td>
<td class='input'><textarea name='hobby' class='textarea-text' cols='60' rows='5'><!--{$jobs.hobby}--></textarea><span class='info'></span></td></tr>
<tr><td class='text'></td>
<td class='submint'>
<input type='submit' name='Submit' value='修改简历' class='submit' />&nbsp;&nbsp;
<input type='reset' name='Submit' value='重新填写' class='reset' /></td>
</tr></table>
	</form>
  <div style="clear:both;height:10px;width:100%"></div>
  </div>
  <div style="clear:both;height:10px;"></div>
</div>
<!--{/if}-->



		</div>
      </div>
	</div><!---left //-->

    <div id="right">
	  <h3 class="title"><span>会员中心</span></h3>
	  <div class="webnav"> 
	    <div id="web-sidebar">



<!--{if $username eq ""}-->
		  <dl>
		    <dt class="part2"><a href="register.php" target="_blank" title='注册会员' style="margin-left: 5px;" >注册会员</a></dt>
		  </dl>	


<!--{else}-->
		  <dl>
		    <dt class="part2"><a href='index.php' target="_blank" title='会员中心首页' style="margin-left: 5px;">会员中心首页</a></dt>
		  </dl>	
		  <dl>
		    <dt class="part2"><a href='userinfo.php' target="_blank" title='修改基本信息' style="margin-left: 5px;">修改基本信息</a></dt>
		  </dl>	
		  		  <dl>
		    <dt class="part2"><a  href='mymessage.php' title='我的留言' style="margin-left: 5px;">我的留言</a></dt>
		  </dl>	
		  		  <dl>
		    <dt class="part2"><a href='myorder.php' title='我的订单' target="_blank" style="margin-left: 5px;">我的订单</a></dt>
		  </dl>	
		  		  <dl>
		    <dt class="part2"><a href='resume.php' title='我的简历' target="_blank" style="margin-left: 5px;">我的简历</a></dt>
		  </dl>	
		  		  <dl>
		    <dt class="part2"><a href='loginout.php' title='安全退出' style="margin-left: 5px;">安全退出</a></dt>
		  </dl>	
<!--{/if}-->




		</div>
	  
	  </div><!--$webnav //-->

      <!--{include file="<!--{$tplpath}-->block_info.tpl"}--> 
      <!--{include file="<!--{$tplpath}-->block_contact.tpl"}-->  
		
    </div><!-- $right //--->

    <div style="clear:both;"></div>
  </div><!--#web //-->
  <!--{include file="<!--{$tplpath}-->block_footer.tpl"}-->
</div>
<script type="text/javascript" src="<!--{$skinpath}-->js/screen.js"></script>
</body>
</html>






