<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title><!--{$page_title}-->-<!--{$copyright_header}--></title>
<meta name="description" content="<!--{$page_description}-->" />
<meta name="keywords" content="<!--{$page_keyword}-->" />
<meta name="author" content="<!--{$copyright_author}-->" />
<link rel="stylesheet" type="text/css" href="<!--{$skinpath}-->style/main.css" />
<link rel="stylesheet" type="text/css" href="<!--{$skinpath}-->style/css.css" />
<script type='text/javascript' src='<!--{$skinpath}-->js/command.js'></script>
<script src="<!--{$skinpath}-->js/jquery-1.4.4.min.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/downnav.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/jquery.cross-slide.js" language="javascript" type="text/javascript"></script>
<!--[if IE 6]>
<script src="<!--{$skinpath}-->js/deletepng.js" language="javascript" type="text/javascript"></script>
<script type="text/javascript">
DD_belatedPNG.fix('.bg,img'); 
</script>
<![endif]-->
</head>
<body>
<div id="wrap">
  <!--{include file="<!--{$tplpath}-->block_head.tpl"}-->
  <!--{include file="<!--{$tplpath}-->block_banner.tpl"}-->
  <div id="web">
    <div id="left">

      <h3 class="title"><span><a href="<!--{$url_index}-->">首 页</a>&nbsp;&nbsp;>>&nbsp;&nbsp;<!--{$navigation}--></span></h3>
	  <div class="webcontent">
	    <div id="product-list">
		  <style type="text/css">#product-list #plug2 dd li{ padding:2px 0px;}</style>
		  <div id="plug2">
		    <!--{foreach $product as $volist}-->
		    <dl style="height:165px;">
			  <dt style="width:158px;">
			    <a href="<!--{$volist.url}-->" title="<!--{$volist.productname}-->" target="_blank"><img src="<!--{$volist.thumbfiles}-->" alt="<!--{$volist.productname}-->" title="<!--{$volist.productname}-->" onload="javascript:DrawImage(this,'150','150');" /></a>
			  </dt>
			  <dd style="width:167px;">
			    <ul>
				  <li><b>名称 : </b><span class="title"><a href="<!--{$volist.url}-->" title="<!--{$volist.productname}-->"><!--{$volist.productname|truncate:8:0:"utf-8"}--></a></span></li>
				  <li><b>编号 : </b><span><!--{$volist.productnum}--></span></li>
				  <li><b>分类 : </b><span><!--{$volist.catename}--></span></li>
				  <li><b>日期 : </b><span><!--{$volist.timeline|date_format:'%Y-%m-%d'}--></span></li>
				</ul>
				<div class="detail"><a href="<!--{$volist.url}-->" title="详细介绍" target="_blank"><img src="<!--{$skinpath}-->images/picd.gif" title="详细介绍" alt="详细介绍" /></a></div>
			  </dd>
			</dl>
			<!--{/foreach}-->
		  
		  </div><!-- #plug2 //-->
		  <div style="clear:both;"></div>
		</div>
		<!--{if $showpage!=""}-->
		<div class="clear"></div>
		<div class="pageController"><!--{$showpage}--></div> 
		<!--{/if}-->	  
	  </div><!-- $webcontent //-->
	
	</div><!-- #left //-->

    <div id="right">
	  <h3 class="title"><span>产品分类</span></h3>
	  <div class="webnav"> 
	  <!--{include file="<!--{$tplpath}-->block_productcat.tpl"}-->
	  </div><!-- $webnav -->
      <!--{include file="<!--{$tplpath}-->block_info.tpl"}--> 
      <!--{include file="<!--{$tplpath}-->block_contact.tpl"}-->  
	</div><!-- $right //-->

    <div style="clear:both;"></div>
  </div>

  <!--{include file="<!--{$tplpath}-->block_footer.tpl"}-->
</div>
<script type="text/javascript" src="<!--{$skinpath}-->js/screen.js"></script>
</body>
</html>