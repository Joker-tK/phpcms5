<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title><!--{$page_title}-->-<!--{$copyright_header}--></title>
<meta name="description" content="<!--{$page_description}-->" />
<meta name="keywords" content="<!--{$page_keyword}-->" />
<meta name="author" content="<!--{$copyright_author}-->" />
<link rel="stylesheet" type="text/css" href="<!--{$skinpath}-->style/main.css" />
<link rel="stylesheet" type="text/css" href="<!--{$skinpath}-->style/css.css" />
<script type='text/javascript' src='<!--{$skinpath}-->js/command.js'></script>
<script src="<!--{$skinpath}-->js/jquery-1.4.4.min.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/downnav.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/jquery.cross-slide.js" language="javascript" type="text/javascript"></script>
<!--[if IE 6]>
<script src="<!--{$skinpath}-->js/deletepng.js" language="javascript" type="text/javascript"></script>
<script type="text/javascript">
DD_belatedPNG.fix('.bg,img'); 
</script>
<![endif]-->
</head>
<body>
<div id="wrap">
  <!--{include file="<!--{$tplpath}-->block_head.tpl"}-->
  <!--{include file="<!--{$tplpath}-->block_banner.tpl"}-->
  <div id="web">
    <div id="left">
	  
	  <h3 class="title"><span><a href="<!--{$url_index}-->">首 页</a>&nbsp;&nbsp;>>&nbsp;&nbsp;<!--{$navigation}--></span></h3>
	  <div class="webcontent">
	    <div id="shownews">
		  <h1 class="title"><!--{$solution.title}--></h1>
		  <div class="text editor">
		    <div class="editor">
<!--{if $viewfalgcontent eq "yes"}-->
<!--{$solution.content|htmlencode}-->
<!--{else}-->
<div style="text-align:center"><img src="<!--{$skinpath}-->images/NoRight.jpg" width="179px" height="179px"/></div>
<!--{/if}-->  



			   <div style="height:90px;width:710px;clear:both;">
<script type="text/javascript"><!--
google_ad_client = "pub-3406709034811813";
/* 728x90, 创建于 08-8-28 */
google_ad_slot = "7097595396";
google_ad_width = 710;
google_ad_height = 90;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js"> 
</script>
</div>
			</div>
		  </div>
		  
		  <div class="hits">点击次数：<span><script src="<!--{$urlpath}-->data/include/solutionhits.php?id=<!--{$solution.solutionid}-->"></script></span>&nbsp;&nbsp;发布日期：<!--{$solution.timeline|date_format:"%Y/%m/%d"}-->&nbsp;&nbsp;【<a href='javascript:window.print()'>打印此页</a>】&nbsp;&nbsp;【<a href='javascript:self.close()'>关闭</a>】</div>
		  <div class="page">上一条：<!--{$previous_item}--><br>下一条：<!--{$next_item}--></div>
        </div>
	  </div><!--#webcontent //-->
	
	</div><!--$left-->

    <div id="right">
      <h3 class="title"><span>解决方案分类</span></h3>
	  <div class="webnav"> 
        <!--{include file="<!--{$tplpath}-->block_solutioncat.tpl"}-->
      </div>
      <!--{include file="<!--{$tplpath}-->block_info.tpl"}--> 
      <!--{include file="<!--{$tplpath}-->block_contact.tpl"}-->  
    </div><!--#right //-->



    <div style="clear:both;"></div>
  </div>

  <!--{include file="<!--{$tplpath}-->block_footer.tpl"}-->
</div>
<script type="text/javascript" src="<!--{$skinpath}-->js/screen.js"></script>
</body>
</html>