<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title><!--{$page_title}-->-<!--{$copyright_header}--></title>
<meta name="description" content="<!--{$page_description}-->" />
<meta name="keywords" content="<!--{$page_keyword}-->" />
<meta name="author" content="<!--{$copyright_author}-->" />
<link rel="stylesheet" type="text/css" href="<!--{$skinpath}-->style/main.css" />
<link rel="stylesheet" type="text/css" href="<!--{$skinpath}-->style/css.css" />
<script type='text/javascript' src='<!--{$skinpath}-->js/command.js'></script>
<script src="<!--{$skinpath}-->js/jquery-1.4.4.min.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/downnav.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/jquery.cross-slide.js" language="javascript" type="text/javascript"></script>
<!--[if IE 6]>
<script src="<!--{$skinpath}-->js/deletepng.js" language="javascript" type="text/javascript"></script>
<script type="text/javascript">
DD_belatedPNG.fix('.bg,img'); 
</script>
<![endif]-->
</head>
<body>
<div id="wrap">
  <!--{include file="<!--{$tplpath}-->block_head.tpl"}-->
  <!--{include file="<!--{$tplpath}-->block_banner.tpl"}-->
  <div id="web">
    <div id="left">
	  <h3 class="title"><span>会员中心</span></h3>
	  <div class="webcontent">
	    <div class="showtext editor">
		  <p>
		 
	<iframe id="iframe" height="280px" name="main" src="login.php" width="100%" frameborder="0" scrolling="no" allowtransparency="true"></iframe>	 
		 
		 </p>
		 
		</div>
      </div>
	</div><!---left //-->

    <div id="right">
	  <h3 class="title"><span>会员中心</span></h3>
	  <div class="webnav"> 
	    <div id="web-sidebar">



<!--{if $username eq ""}-->
		  <dl>
		    <dt class="part2"><a href="register.php" target="_blank" title='注册会员' style="margin-left: 5px;" >注册会员</a></dt>
		  </dl>	


<!--{else}-->
		  <dl>
		    <dt class="part2"><a href='index.php' target="_blank" title='会员中心首页' style="margin-left: 5px;">会员中心首页</a></dt>
		  </dl>	
		  <dl>
		    <dt class="part2"><a href='userinfo.php' target="_blank" title='修改基本信息' style="margin-left: 5px;">修改基本信息</a></dt>
		  </dl>	
		  		  <dl>
		    <dt class="part2"><a  href='mymessage.php' title='我的留言' style="margin-left: 5px;">我的留言</a></dt>
		  </dl>	
		  		  <dl>
		    <dt class="part2"><a href='myorder.php' title='我的订单' target="_blank" style="margin-left: 5px;">我的订单</a></dt>
		  </dl>	
		  		  <dl>
		    <dt class="part2"><a href='resume.php' title='我的简历' target="_blank" style="margin-left: 5px;">我的简历</a></dt>
		  </dl>	
		  		  <dl>
		    <dt class="part2"><a href='loginout.php' title='安全退出' style="margin-left: 5px;">安全退出</a></dt>
		  </dl>	
<!--{/if}-->




		</div>
	  
	  </div><!--$webnav //-->

      <!--{include file="<!--{$tplpath}-->block_info.tpl"}--> 
      <!--{include file="<!--{$tplpath}-->block_contact.tpl"}-->  
		
    </div><!-- $right //--->

    <div style="clear:both;"></div>
  </div><!--#web //-->
  <!--{include file="<!--{$tplpath}-->block_footer.tpl"}-->
</div>
<script type="text/javascript" src="<!--{$skinpath}-->js/screen.js"></script>
</body>
</html>


