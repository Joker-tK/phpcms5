<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title><!--{$page_title}-->-<!--{$copyright_header}--></title>
<meta name="description" content="<!--{$page_description}-->" />
<meta name="keywords" content="<!--{$page_keyword}-->" />
<meta name="author" content="<!--{$copyright_author}-->" />
<link rel="stylesheet" type="text/css" href="<!--{$skinpath}-->style/main.css" />
<link rel="stylesheet" type="text/css" href="<!--{$skinpath}-->style/css.css" />
<script type='text/javascript' src='<!--{$skinpath}-->js/command.js'></script>
<script src="<!--{$skinpath}-->js/jquery-1.4.4.min.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/downnav.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/jquery.cross-slide.js" language="javascript" type="text/javascript"></script>
<!--[if IE 6]>
<script src="<!--{$skinpath}-->js/deletepng.js" language="javascript" type="text/javascript"></script>
<script type="text/javascript">
DD_belatedPNG.fix('.bg,img'); 
</script>
<![endif]-->
</head>
<body>
<div id="wrap">
  <!--{include file="<!--{$tplpath}-->block_head.tpl"}-->
  <!--{include file="<!--{$tplpath}-->block_banner.tpl"}-->
  <div id="web">
  
    <div id="left">
	  <h3 class="title"><span><a href="<!--{$url_index}-->">首 页</a>&nbsp;&nbsp;>>&nbsp;&nbsp;<!--{$navigation}--></span></h3>
	  <div class="webcontent">
	    <div id="showjob">
		  <h1 class="title">招聘职位：<!--{$job.title}--></h1>
		  <div class="para">
		  		    <ul style="width:300px;float:left;">
			  <li class='info_deal'><b>职位编号 ：</b> #<!--{$job.jobid}--></li>
			  <li class='info_person'><b>招聘人数 ： </b> <!--{$job.number}--> 人</li>
			  <li class='info_address'><b>工作地点 ：</b> <!--{$job.workarea}--></li>
			  <li class='info_updatetime'><b>发布日期 ：</b> <!--{$job.timeline|date_format:'%Y-%m-%d'}--></li>
			  <li class='info_validity'><b>浏览次数 ：</b> <!--{$job.hits}--></li>
			</ul>
			<ul style="float:left;width:300px">
            <li style="height:30px;"></li>
            <li style="height:30px;"></li>
			<li>
			<div class="info_job">
			<a href="applyjob.php?jobid=<!--{$job.jobid}-->" target="_blank" title="在线应聘" class='hover-none'><span>在线应聘</span></a>
			</div>
			</li>
			</ul>
			<div style="clear:both;"></div>
		  </div>
		  <h3 class="hr"><span>岗位职责</span></h3>
		  <div class="text editor"> <!--{$job.jobdescription}--></div>

		  <h3 class="hr"><span>职位要求</span></h3>
		  <div class="text editor"><!--{$job.jobrequest}--></div>
          <!--{if $job.jobotherrequest!=""}-->
		  <h3 class="hr"><span>其他要求 </span></h3>
		  <div class="text editor"> <!--{$job.jobotherrequest}--></div>
		  <!--{/if}-->

		  <h3 class="hr"><span>招聘负责人联系方式 </span></h3>
		  <div class="text editor"><!--{$job.jobcontact}--></div>

		  <div class="hits"></div>
		  <div class="page">上一条：<!--{$previous_item}--><br>下一条：<!--{$next_item}--></div>
		</div>
	  </div>
	</div><!--#left //-->

    <div id="right">
      <h3 class="title"><span>招聘分类</span></h3>
	  <div class="webnav"> 
        <div id="web-sidebar">
		  <!--{foreach $volist_jobcategory as $volist}-->
          <dl>
		    <dt class="part2" id="part1-id6"><a href="<!--{$volist.url}-->"><!--{$volist.catename}--></a></dt>
		  </dl>	
		  <!--{/foreach}-->
        </div>
      </div>

      <!--{include file="<!--{$tplpath}-->block_info.tpl"}--> 
      <!--{include file="<!--{$tplpath}-->block_contact.tpl"}-->  

    </div><!--#right //-->
    <div style="clear:both;"></div>
  </div>

  <!--{include file="<!--{$tplpath}-->block_footer.tpl"}-->
</div>
<script type="text/javascript" src="<!--{$skinpath}-->js/screen.js"></script>
</body>
</html>