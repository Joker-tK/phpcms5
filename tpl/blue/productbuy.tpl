<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title><!--{$page_title}-->-<!--{$copyright_header}--></title>
<meta name="description" content="<!--{$page_description}-->" />
<meta name="keywords" content="<!--{$page_keyword}-->" />
<meta name="author" content="<!--{$copyright_author}-->" />
<link rel="stylesheet" type="text/css" href="<!--{$skinpath}-->style/main.css" />
<link rel="stylesheet" type="text/css" href="<!--{$skinpath}-->style/css.css" />
<script type='text/javascript' src='<!--{$skinpath}-->js/command.js'></script>
<script src="<!--{$skinpath}-->js/jquery-1.4.4.min.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/downnav.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/jquery.cross-slide.js" language="javascript" type="text/javascript"></script>
<!--[if IE 6]>
<script src="<!--{$skinpath}-->js/deletepng.js" language="javascript" type="text/javascript"></script>
<script type="text/javascript">
DD_belatedPNG.fix('.bg,img'); 
</script>
<![endif]-->
<script language="javascript">
function CheckJob(){
if (document.myform.username.value.length == 0) 
{
alert('姓名 不能为空');
document.myform.username.focus();
return false;
}
if (document.myform.email.value.length == 0)
{
alert('E–mail 不能为空');
document.myform.email.focus();
return false;
}
if (document.myform.telno.value.length == 0)
{
alert('联系电话不能为空');
document.myform.telno.focus();
return false;
}
}
</script>
</head>
<body>
<div id="wrap">
  <!--{include file="<!--{$tplpath}-->block_head.tpl"}-->
  <!--{include file="<!--{$tplpath}-->block_banner.tpl"}-->
  <div id="web">
    <div id="left">
	  <h3 class="title"><span><a href="<!--{$url_index}-->">首 页</a>&nbsp;&nbsp;>>在线订购</span></h3>
	  <div class="webcontent">

<form method='POST' onSubmit='return CheckJob();' name='myform' action='<!--{$url_productbuy}-->' >
<input type="hidden" name="action" value="saveadd" />
<table class='job_table' width="95%" border="0" align="center" cellpadding="5" cellspacing="3" style="margin-top:20px">
<!--{if $proid eq ""}-->
<tr><td class='text'>选择要订购的产品</td>
<td class='input'>
<select name='proid' id='proid'>
<!--{foreach $products as $productlist}-->	
<option value='<!--{$productlist.productid}-->' ><!--{$productlist.productname}--></option>
<!--{/foreach}-->
</select>
<span class='info'>*</span></td></tr>
<!--{else}-->
<input type="hidden" name="proid" value="<!--{$proid}-->" />
<!--{/if}-->
<tr><td class='text'>姓名</td>
<td class='input'><input name='username' type='text' class='input-text' value="<!--{$username}-->" size='40'><span class='info'>*</span></td></tr>
<tr><td class='text'>性别</td>
<td class='input'>
<input name='sex' type='radio' id='para12_1' value='1' checked='checked' />
<label for='para12_1'>男士</label> 
<input name='sex' type='radio' id='para12_2' value='0'  />
<label for='para12_2'>女士</label>  <span class='info'></span>
</td></tr>
<tr><td class='text'>公司名字</td>
<td class='input'><input name='company' type='text' class='input-text' size='40'><span class='info'></span></td></tr>
<tr><td class='text'>公司地址</td>
<td class='input'><input name='address' type='text' class='input-text' size='40'><span class='info'>*</span></td></tr>
<tr><td class='text'>邮政编码</td>
<td class='input'><input name='zipcode' type='text' class='input-text' size='40'><span class='info'></span></td></tr>
<tr><td class='text'>联系电话</td>
<td class='input'><input name='telephone' type='text' class='input-text' size='40'><span class='info'></span></td></tr>
<tr><td class='text'>传真：</td>
<td class='input'><input name='fax' type='text' class='input-text' size='40'><span class='info'></span></td></tr>
<tr><td class='text'>手机号</td>
<td class='input'><input name='mobile' type='text' class='input-text' size='40'><span class='info'>*</span></td></tr>
<tr><td class='text'>E–mail</td>
<td class='input'><input name='email' type='text' class='input-text' size='40'><span class='info'>*</span></td></tr>
<tr><td class='text'>订购详细说明</td>
<td class='input'><textarea name='remark' class='textarea-text' cols='60' rows='5'></textarea><span class='info'>*</span></td></tr>
<tr><td class='text'></td>
<td class='submint'><input type='submit' name='Submit' value='立即订购' class='submit' />&nbsp;&nbsp;<input type='reset' name='Submit' value='重新填写' class='reset' /></td>
</tr></table></form>
      </div>
    </div><!-- $left //-->
    
    <div id="right">
	  <h3 class="title"><span>产品分类</span></h3>
	  <div class="webnav"> 
	  <!--{include file="<!--{$tplpath}-->block_productcat.tpl"}-->
	  </div><!-- $webnav -->
      <!--{include file="<!--{$tplpath}-->block_info.tpl"}--> 
      <!--{include file="<!--{$tplpath}-->block_contact.tpl"}-->
	</div><!-- $right //-->
    <div style="clear:both;"></div>

  </div>

  <!--{include file="<!--{$tplpath}-->block_footer.tpl"}-->
</div>
<script type="text/javascript" src="<!--{$skinpath}-->js/screen.js"></script>
</body>
</html>


