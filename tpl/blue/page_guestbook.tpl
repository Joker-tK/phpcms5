<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$page_charset}-->" />
<title><!--{$page_title}-->-<!--{$copyright_header}--></title>
<meta name="description" content="<!--{$page_description}-->" />
<meta name="keywords" content="<!--{$page_keyword}-->" />
<meta name="author" content="<!--{$copyright_author}-->" />
<link rel="stylesheet" type="text/css" href="<!--{$skinpath}-->style/main.css" />
<link rel="stylesheet" type="text/css" href="<!--{$skinpath}-->style/css.css" />
<script type='text/javascript' src='<!--{$skinpath}-->js/command.js'></script>
<script src="<!--{$skinpath}-->js/jquery-1.4.4.min.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/downnav.js" language="javascript" type="text/javascript"></script>
<script src="<!--{$skinpath}-->js/jquery.cross-slide.js" language="javascript" type="text/javascript"></script>
<!--[if IE 6]>
<script src="<!--{$skinpath}-->js/deletepng.js" language="javascript" type="text/javascript"></script>
<script type="text/javascript">
DD_belatedPNG.fix('.bg,img'); 
</script>
<![endif]-->
</head>
<body>
<div id="wrap">
  <!--{include file="<!--{$tplpath}-->block_head.tpl"}-->
  <!--{include file="<!--{$tplpath}-->block_banner.tpl"}-->
 
  <div id="web">
    <div id="left">
	  <h3 class="title"><span>在线留言</span></h3>
	  <div class="webcontent">
	    <div class="showtext editor">
		  <p>

			<form method="post" action="<!--{$url_guestbook}-->" name="myform" id="myform" onsubmit="return checkform();" />
			<input type="hidden" name="action" value="saveadd" />
			<table cellpadding='8' cellspacing='8' border="0" style="line-height:25px;" width="80%" align="center">
			  <tr>
				<td colspan='2' class='topTd'></td>
			  </tr>
			  <tr>
				<td class='hback_1' width='15%'><b>姓 名：</b></td>
				<td class='hback' width='85%'><input name="bookuser" id="bookuser" type="text" size="15" />  <select name="gender" id="gender"><option value="1">先生</option><option value="2">女士</option></select></td>
			  </tr>
			  <tr>
				<td><b>公 司：</b></td>
				<td><input name="companyname" id="companyname" type="text" size="30" /> <font color='red'>*</font></td>
			  </tr>
			  <tr>
				<td><b>地 址：</b></td>
				<td><input name="address" id="address" type="text" size="30" /> <font color='red'>*</font></td>
			  </tr>
			  <tr>
				<td><b>行 业：</b></td>
				<td><input name="trade" id="trade" type="text" size="20" /> <font color='red'>*</font></td>
			  </tr>
			  <tr>
				<td><b>职 务：</b></td>
				<td><input name="jobs" id="jobs" type="text" size="20" /> <font color='red'>*</font></td>
			  </tr>
			  <tr>
				<td><b>电 话：</b></td>
				<td><input name="telephone" id="telephone" type="text" size="25" /> <font color='red'>*</font></td>
			  </tr>
			  <tr>
				<td><b>手 机：</b></td>
				<td><input name="mobile" id="mobile" type="text" size="25" /> <font color='red'></font></td>
			  </tr>
			  <tr>
				<td><b>传 真：</b></td>
				<td><input name="fax" id="fax" type="text" size="25" /> <font color='red'></font></td>
			  </tr>
			  <tr>
				<td><b>E-mail：</b></td>
				<td><input name="email" id="email" type="text" size="35" /> <font color='red'></font></td>
			  </tr>
			  <tr>
				<td><b>留言内容：</b></td>
				<td><textarea name="content" id="content" style="width:75%;height:100px;overflow:auto;"></textarea> <font color="red">*</font></td>
			  </tr>
			  <tr>
				<td colspan='2' align="center" height="30px;"><input type="submit" name="btn_save" value="提交保存" />&nbsp;&nbsp;<input type="reset" name="btn_reset" value="重 置" /></td>
			  </tr>
			</table>

			</form>


          </p>
	     <div style="height:90px;width:710px;clear:both;">
<script type="text/javascript"><!--
google_ad_client = "pub-3406709034811813";
/* 728x90, 创建于 08-8-28 */
google_ad_slot = "7097595396";
google_ad_width = 710;
google_ad_height = 90;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js"> 
</script>
</div>
		</div>
      </div>
	</div><!---left //-->

    <div id="right">
	  <h3 class="title"><span>关于我们</span></h3>
	  <div class="webnav"> 
	    <div id="web-sidebar">
		  <dl>
		    <dt class="part2"><a href="<!--{$url_about}-->"><!--{$lang_about}--></a></dt>
		  </dl>	
		  <!--{foreach $volist_page as $volist}-->
		  <dl>
		    <dt class="part2">
			  <a href="<!--{$volist.url}-->"><!--{$volist.title}--></a>
			</dt>
		  </dl>	
		  <!--{/foreach}-->
		  <dl>
		    <dt class="part2"><a href="<!--{$url_contact}-->"><!--{$lang_contact}--></a></dt>
		  </dl>	
		</div>
	  
	  </div><!--$webnav //-->

      <!--{include file="<!--{$tplpath}-->block_info.tpl"}--> 
      <!--{include file="<!--{$tplpath}-->block_contact.tpl"}-->  
		
    </div><!-- $right //--->

    <div style="clear:both;"></div>
  </div><!--#web //-->
  <!--{include file="<!--{$tplpath}-->block_footer.tpl"}-->
</div>
<script type="text/javascript" src="<!--{$skinpath}-->js/screen.js"></script>
</body>
</html>
<script language="javascript">
function checkform(){
	if($("#bookuser").val()==""){
		alert("姓名不能为空.");
		return false;
	}
	if($("#companyname").val()==""){
		alert("公司不能为空.");
		return false;
	}
	if($("#address").val()==""){
		alert("公司地址不能为空.");
		return false;
	}
	if($("#trade").val()==""){
		alert("所属行业不能为空.");
		return false;
	}
	if($("#jobs").val()==""){
		alert("职务不能为空.");
		return false;
	}
	if($("#telephone").val()==""){
		alert("联系电话不能为空.");
		return false;
	}
	if($("#content").val()==""){
		alert("留言内容不能为空.");
		return false;
	}
}
</script>