<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
**/
session_start();
ob_start();
require_once '../source/core/run.php';
loginuserout();
ob_end_flush();
function loginuserout(){
unset($_SESSION["USERID"]);
unset($_SESSION["USERNAME"]);
unset($_SESSION["usergroupname"]);
unset($_SESSION["gpurview"]);
unset($_SESSION["USERLEVEL"]);
unset($_SESSION["pointnum"]);
unset($_SESSION["lastlogindate"]);
Core_Fun::halt("退出成功","index.php",0);
}
?>