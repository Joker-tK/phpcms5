<?php
/**
 * @CopyRight  (C)2006-2011 LiangJing Development team Inc.
 * @WebSite    www.liangjing.org www.asp99.cn
 * @Author     Liangjing.org <asp3721@hotmail.com>
 * @Brief      liangjingcms v1.x
 * @Update     2011.09.11
 * @Id         单页
**/
session_start();
$mod = isset($_GET['mod']) ? $_GET['mod'] : "userinfo";
if(empty($mod)){
	$mod = "userinfo";
}
$modd = $mod;
define('ALLOWGUEST',true);
require_once '../source/core/run.php';
/* 指定允许访问的模块 */
$allowmod = array('about','contact','detail','sitemap','link','membercenter','userinfo');
if(!in_array($modd,$allowmod)) {
	Core_Fun::halt("对不起，不存在“mod=".$modd."”模块，请检查！","",1);
}
/* 判断是否存在文件 */
$tplfile = "../".INDEX_TEMPLATE.$modd.".".$tplext;
$widgetfile = "../source/widget/".$modd.".php";
if(!Core_Fun::fileexists($tplfile)){
	Core_Fun::halt("对不起，模板文件“".$tplfile."”不存在，请检查！","",1);
}
if(!Core_Fun::fileexists($widgetfile)){
	Core_Fun::halt("对不起，部件文件“".$widgetfile."”不存在，请检查！","",1);
}
/* 缓存,模板处理 */
if($config['cachstatus']==1){
	$cache_seconds = $config['cachtime']*60;
	$tpl->setCaching(true); 
	$tpl->setCacheLifetime($cache_seconds);	
}
$cacheid = md5($_SERVER["REQUEST_URI"]);
if(!$tpl->isCached($tplfile,$cacheid)){
	require_once '../source/module/app.php';
	require_once '../source/widget/'.$modd.'.php';
}

$tpl->assign("username",$_SESSION["USERNAME"]);
$tpl->assign("runtime",Core_Fun::runtime());
$tpl->display($tplfile,$cacheid);

?>




